




/* ****************** */
//      Appezzamenti
/* ****************** */








entity LottaIntegeregrata{
    tipologia String required,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob

}
relationship ManyToOne{
	Appezzamenti{lottaIntegeregrata} to LottaIntegeregrata
}






/* ****************** */
//      Operatore
/* ****************** */




	

/* ************** */
// Macchinari
/* ************** */




/* ****************** */
//  Pioggia
/* ****************** */
entity PioStorico{
	DataPioggia LocalDate required,
	QuantitaMl Integer required,
	Comune String  required
}


relationship ManyToOne { 
	PioStorico{appezzamenti}   to Appezzamenti
}


/* ****************** */
// Operazioni Colturali
/* ****************** */

entity OpcAnagrafica {
	descrizione String required,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob

}

entity RegOpeColturali{
	data LocalDate required,
	note TextBlob
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

entity OpcAnagraficaTipologia {
	descrizione String required,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}
relationship ManyToOne {
	OpcAnagrafica{opcanagraficatipologia} to OpcAnagraficaTipologia
}

relationship ManyToOne {
	RegOpeColturali{opcanagrafica} to OpcAnagrafica,  
	RegOpeColturali{macmodello}    to AtrAnagrafica,  
	RegOpeColturali{opeColtopeanagrafica} to OpeAnagrafica,
	RegOpeColturali{appezzamenti}  to Appezzamenti
}



/* ***********************/
// Registro Fitofarmaci
/* ***********************/
entity RegFito {
	Prg String required,
	DataRegistrazione LocalDate required,
	note TextBlob
	SuperficieTrattata Double required
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long,
	note TextBlob
}

relationship OneToMany{
   
}

relationship ManyToOne { 
	RegFito{fasefenologica} 	to FaseFenologica,
    RegFito{RegFitoToAtrAna} 	to AtrAnagrafica,
    RegFito{fitoOpeanagrafica} 	to OpeAnagrafica,
    RegFito{appezzamenti} 	to Appezzamenti,
	RegFito{avversita}   	to Avversita 
}


/* ***********************/
// Registro Trappole
/* ***********************/

entity RegTrappole {
	DataCollocamento LocalDate required,
	DataDismissione LocalDate required,
	quantita  Integer required,
	riconsegnaMagazzino Boolean required,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}
entity RegTrappoleDettaglio{
    DataControllo LocalDate required,
	DataSostFeromone LocalDate,
	parametroRilevato String required,
	note TextBlob
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}


relationship  OneToMany {
    RegTrappole{dettaglitrappole} to RegTrappoleDettaglio
}

relationship ManyToOne {
	RegTrappole{trappole} 	to Trappole,
    RegTrappole{appezzamenti} 	to Appezzamenti,
	RegTrappole{opeanagrafica} 	to OpeAnagrafica
}                                                  

/* ****************** */
//   Fertilizzante
/* ****************** */

relationship OneToMany {
	//FerFormulazione{feranagraficaff(denominazione)} to FerAnagrafica{ferformulazionefa(descrizione)},                     // MT non serve per fertilizzante
    // FerModalitaApplicazione{feranagraficafma(denominazione)} to FerAnagrafica{fermodalitaapplicazionefa(descrizione)},   // MT non serve per fertilizzante
	//FerGistificativo{feranagraficafg(denominazione)} to FerAnagrafica{fergistificativofa(descrizione)},                   // MT da spostare sul registro fertilizzazioni non su anagrafica
  
  /*  FerMateriaPrima to FerAnagrafica,
    FerTitolo to FerAnagrafica,
    FerMicroelementi to FerAnagrafica	*/

}

/*entity FerGistificativo {
	descrizione String   // (esempio carenza rilevata da analisi fogliare)
}

entity FerFormulazione {
	descrizione String //(polvere bagnante) , questo dati non è presente in sian,
}

entity FerModalitaApplicazione {
	descrizione String //(ferrigazione, fogliare, post-raccolta), dato non presente in sian, relativo comunque al registro.
}
	
entity FerMateriaPrima {
	descrizione String 
}	

entity FerTitolo {
	descrizione String,
	percentuale Double
}	

entity FerMicroelementi	{
	descrizione String,
	valore Double
}	
*/	

/*************************************

      REGISTRO FERTILIZZAZIONI

*************************************/

entity RegTrConcimi{
	dataRegistrazione LocalDate required,
	//tempoImpiegato String,
	//descProd TextBlob,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long,
	note TextBlob
}

// mt aggiunta oggetto relazione per articolo - quantita

entity ProdottoUiilizzatoFertilizzante{ // mt - per ogni prodotto utilizzato in una fertilizzazione indico la quantità
    TotaleProdottoUtilizzato Double
}

relationship OneToMany{
    RegTrConcimi to ProdottoUiilizzatoFertilizzante
    FerAnagrafica to ProdottoUiilizzatoFertilizzante
}

relationship ManyToMany { // MT - MODIFICATO IN MANYYOMAY 
    RegTrConcimi{macmodellorgc(descrizione)} to MacModello{regtrconcimimodello},                        // MT - indico macchina e attrezzature utilizzate
    RegTrConcimi{appezzamentirgc(nomeAppezzamento)} to ColturaAppezzamento{regtrconcimiappz},           // MT - IN RELAZIONE A COLTURAAPPEZZAMENTO, NON SOLO APPEZZAMENTO 
    RegTrConcimi{prodottoutilizzato} to ProdottoUiilizzatoFertilizzante{regtrconcimiprodotti}           // MT - per ogni prodotto utilizzato in una fertilizzazione indico la quantità
}

relationship ManyToOne {
    RegTrConcimi{regferticonttoer}  to Contoterzista,
    RegTrConcimi{opeanagraficargc(codiceFiscale)} to OpeAnagrafica{regtrconcimipean},                   // MT - indico OPERATORE
    RegTrConcimi{modalitadistribuzionergc(desMetodoDist)} to ModDistFertilizzanti{regtrconcimimdd},     // MT - indico modalità distibuzione
    RegTrConcimi to Contoterzista                                                                       // MT - indico cont oterzista che effettua operazione
    RegTrConcimi{fergiustificativo} to FerGistificativo{registrofertilizzanti}                          // MT - indico motivazione operazione
}



entity ModDistFertilizzanti{
    codMetodoDist String,	
    desMetodoDist String
    /*
    SPANDICONCIME,
    CENTRIFUGO,
    SPANDICONCIME PNEUMATICO,
    FERTIRRIGAZIONE,
    TRATTAMENTO FOGLIARE
    */
}

/**************************
    MT 1411

    PIANO FERTILIZZAZIONE 

**************************/

entity PianoFertilizzazione{
    dataInizio LocalDate,
    dataFine LocalDate,
    totaleNperiodo Double,
    totaleNoperazione Double,
    totalePperiodo Double,
    totalePoperazione Double,
    totaleKperiodo Double,
    totaleKoperazione Double,
    dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

relationship ManyToMany {
    PianoFertilizzazione to ColturaAppezzamento // MT - collego il piano di fertilizzazione alle colture sull'appezzamento.
}


/* ****************** */
//      Magazzino
/* ****************** */





/* ********************/
//      Fornitori
/* ********************/






/* ********************/
//      Dizionari
/* ********************/

entity DizQualita {
  codice String,
  descrizione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}
entity DizDestinazioni {
  codice String,
  descrizione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}
entity DizUsi {
  codice String,
  descrizione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}
entity DizVarieta {
  codice String,
  descrizione String,
  codOccupazione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}
entity DizMacrousi {
  codice String,
  descrizione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}
entity DizColture {
  codice String,
  descrizione String,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}

/* ********************/
//      Gestione BDF
/* ********************/













entity MeccanismiDiAzione {
  codice String,
  descrizione String,
  tipoImport String, // 'BDF' o 'MANUALE'
  operatore String, // 'BATCH' o 'MTONOLI'
  ts Instant,
  dataInizVali LocalDate, 
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}






entity Pittogrammi { // MT 1311 - PITOGRAMMI
  codice String,
  descrizione String,
  urlImmagine String,
  tipoImport String, // 'BDF' o 'MANUALE'
  operatore String, // 'BATCH' o 'MTONOLI'
  ts Instant,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}






entity Distribuzione{
  codMetodoDist String,
  desMetodoDist String,
  tipoImport String, // 'BDF' o 'MANUALE'
  operatore String, // 'BATCH' o 'MTONOLI'
  ts Instant,
  dataInizVali LocalDate,
  dataFineVali LocalDate,
  userIdCreator Long,
  userIdLastMod Long, 
  note TextBlob
}















relationship OneToMany {


  
  
    Pittogrammi{pittogrammietichetta} to EtichettaPittogrammi,    // MT 1311 - LEGAME TRA PITTOGRAMMI E ETICHETTAPITTROGRAMMI ( 1 PITTOGRAMMA N ETICHETTE )
  
  
  
   
 
}

/******
Registro Semine
MT 1411
*******/
entity SestoImpianto {
    sullaFila Double
    traLeFile Double
    dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

entity TipologiaSemina {
    descrizione String,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
    /*
    PRECISIONE
    SPAGLIO
    TRAPIANOT
    RATEO VARIABILE
    */
}

entity UnitaMisuraSemina {
    descrizione String,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
    /*
        GR/M2
        KG/M2
        DOSE/M2
        DOSE/METRO
    */
}

entity LottoSemente {
    numerolotto String,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
    // da dettagliare, potrebbe servire indicare anche l'anagrafica del fornitore produttore del seme se differente dal fornitore ddt?
}

entity DocumentoFornitore{
    dataddt LocalDate,
    numeroddt String,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

entity RegSemineTrapianti {
    dataInizio LocalDate,
    dataFine LocalDate,
    ddtFornitoreMr String,
    ddtFornitoreData String,
    nrPiante Integer,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

relationship ManyToOne { 
    RegSemineTrapianti{regraccoltope}  to OpeAnagrafica,
	RegSemineTrapianti{raccoltaForn}  to Fornitori,
    RegSemineTrapianti{raccoltaum}  to UnitaMisuraRaccolta,
    RegSemineTrapianti{raccoltatip}  to TipologiaSemina,
    RegSemineTrapianti{raccoltasest}  to SestoImpianto,
    RegSemineTrapianti{raccoltafornitore}  to Fornitori,
    DocumentoFornitore{fornit} to Fornitori, 
    Fornitori{fornitoreazienda} to AziAnagrafica // MT 1411 lo metto qui ma andrebbe nelle relazioni del fornitore su azienda
}

relationship ManyToMany {
    RegSemineTrapianti{regraccoltamac}  to AtrAnagrafica,
    RegSemineTrapianti{regraccoltaapp}  to ColturaAppezzamento,
    RegSemineTrapianti{regraccoltolotti}  to LottoSemente,
    RegSemineTrapianti{regraccoltadocumenti}  to DocumentoFornitore
}

/*******

Registro Raccolta
MT 1411

******/
entity RegRaccolta {
    quantita Double,
    locazioneConferimento TextBlob,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

entity UnitaMisuraRaccolta {
   descrizione String,
   dataInizVali LocalDate,
   dataFineVali LocalDate,
   userIdCreator Long,
   userIdLastMod Long, 
   note TextBlob
}


relationship ManyToOne { 
    RegRaccolta{regraccoltope}  to OpeAnagrafica,
	RegRaccolta{raccoltacontoter}  to Fornitori,
    RegRaccolta{raccoltaum}  to UnitaMisuraRaccolta
}

relationship ManyToMany {
    RegRaccolta{regraccoltamac}  to AtrAnagrafica,
    RegRaccolta{regraccoltaapp}  to ColturaAppezzamento
}


/* **************************/
//      Registro Irrigazioni
/* **************************/


entity RegIrrigazioni {
	dataInizio LocalDate,
    dataFine LocalDate,
	quantitamc Double,
	motivazione TextBlob,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

entity TipoIrrigazione {
    descrizione String,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
    /*
    SOMMERSIONE
    SCORRIMENTO
    INFILTRAZIONE LATERALE
    SUBIRRIGAZIONE
    ASPERSIONE
    GOCCIA
    */
}

relationship ManyToOne { 
    RegIrrigazioni{tipoirrigazioneirr(descrizione)} to   TipoIrrigazione{irrigazionitipo(data)} 
    RegIrrigazioni{regirrope}  to OpeAnagrafica
	RegIrrigazioni{regirrcontoter}  to Fornitori
}

relationship ManyToMany {
    RegIrrigazioni{regirrcoltaapp}  to ColturaAppezzamento
}



/* ****************** */
//  Pioggia
/* ****************** */
entity PioStorico{
	dataPioggia LocalDate
	quantitaMl Integer
	comune String
}

relationship OneToMany { 
	Appezzamenti{piostoricoappez(dataPioggia)}  to PioStorico{appezzamentipio(nomeAppezzamento)}
}

/**************************
    MT 1411

    PIANO IRRIGAZIONE 

**************************/

entity PianoIrrigazione{
    dataInizio LocalDate,
    dataFine LocalDate,
    totaleh20periodo Double,
    totaleh20operazione Double,
	dataInizVali LocalDate,
	dataFineVali LocalDate,
	userIdCreator Long,
	userIdLastMod Long, 
	note TextBlob
}

relationship ManyToMany {
    PianoIrrigazione to ColturaAppezzamento // MT - collego il piano di fertilizzazione alle colture sull'appezzamento.
}


paginate * with pager 
filter *
// dto * with mapstruct