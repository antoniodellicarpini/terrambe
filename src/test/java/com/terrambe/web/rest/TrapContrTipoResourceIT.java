package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TrapContrTipo;
import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.repository.TrapContrTipoRepository;
import com.terrambe.service.TrapContrTipoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TrapContrTipoCriteria;
import com.terrambe.service.TrapContrTipoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrapContrTipoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TrapContrTipoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TrapContrTipoRepository trapContrTipoRepository;

    @Autowired
    private TrapContrTipoService trapContrTipoService;

    @Autowired
    private TrapContrTipoQueryService trapContrTipoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTrapContrTipoMockMvc;

    private TrapContrTipo trapContrTipo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrapContrTipoResource trapContrTipoResource = new TrapContrTipoResource(trapContrTipoService, trapContrTipoQueryService);
        this.restTrapContrTipoMockMvc = MockMvcBuilders.standaloneSetup(trapContrTipoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrapContrTipo createEntity(EntityManager em) {
        TrapContrTipo trapContrTipo = new TrapContrTipo()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return trapContrTipo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrapContrTipo createUpdatedEntity(EntityManager em) {
        TrapContrTipo trapContrTipo = new TrapContrTipo()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return trapContrTipo;
    }

    @BeforeEach
    public void initTest() {
        trapContrTipo = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrapContrTipo() throws Exception {
        int databaseSizeBeforeCreate = trapContrTipoRepository.findAll().size();

        // Create the TrapContrTipo
        restTrapContrTipoMockMvc.perform(post("/api/trap-contr-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trapContrTipo)))
            .andExpect(status().isCreated());

        // Validate the TrapContrTipo in the database
        List<TrapContrTipo> trapContrTipoList = trapContrTipoRepository.findAll();
        assertThat(trapContrTipoList).hasSize(databaseSizeBeforeCreate + 1);
        TrapContrTipo testTrapContrTipo = trapContrTipoList.get(trapContrTipoList.size() - 1);
        assertThat(testTrapContrTipo.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTrapContrTipo.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTrapContrTipo.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTrapContrTipo.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTrapContrTipo.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTrapContrTipoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trapContrTipoRepository.findAll().size();

        // Create the TrapContrTipo with an existing ID
        trapContrTipo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrapContrTipoMockMvc.perform(post("/api/trap-contr-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trapContrTipo)))
            .andExpect(status().isBadRequest());

        // Validate the TrapContrTipo in the database
        List<TrapContrTipo> trapContrTipoList = trapContrTipoRepository.findAll();
        assertThat(trapContrTipoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTrapContrTipos() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trapContrTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTrapContrTipo() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get the trapContrTipo
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos/{id}", trapContrTipo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trapContrTipo.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTrapContrTipoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the trapContrTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTrapContrTipoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTrapContrTipoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the trapContrTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTrapContrTipoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where descrizione is not null
        defaultTrapContrTipoShouldBeFound("descrizione.specified=true");

        // Get all the trapContrTipoList where descrizione is null
        defaultTrapContrTipoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali is not null
        defaultTrapContrTipoShouldBeFound("dataInizVali.specified=true");

        // Get all the trapContrTipoList where dataInizVali is null
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTrapContrTipoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trapContrTipoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTrapContrTipoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali is not null
        defaultTrapContrTipoShouldBeFound("dataFineVali.specified=true");

        // Get all the trapContrTipoList where dataFineVali is null
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTrapContrTipoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trapContrTipoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTrapContrTipoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator is not null
        defaultTrapContrTipoShouldBeFound("userIdCreator.specified=true");

        // Get all the trapContrTipoList where userIdCreator is null
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTrapContrTipoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trapContrTipoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTrapContrTipoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod is not null
        defaultTrapContrTipoShouldBeFound("userIdLastMod.specified=true");

        // Get all the trapContrTipoList where userIdLastMod is null
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrapContrTiposByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);

        // Get all the trapContrTipoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trapContrTipoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTrapContrTipoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTrapContrTiposByTrapContrToTrapContrTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        trapContrTipoRepository.saveAndFlush(trapContrTipo);
        RegOpcTrapContr trapContrToTrapContrTipo = RegOpcTrapContrResourceIT.createEntity(em);
        em.persist(trapContrToTrapContrTipo);
        em.flush();
        trapContrTipo.addTrapContrToTrapContrTipo(trapContrToTrapContrTipo);
        trapContrTipoRepository.saveAndFlush(trapContrTipo);
        Long trapContrToTrapContrTipoId = trapContrToTrapContrTipo.getId();

        // Get all the trapContrTipoList where trapContrToTrapContrTipo equals to trapContrToTrapContrTipoId
        defaultTrapContrTipoShouldBeFound("trapContrToTrapContrTipoId.equals=" + trapContrToTrapContrTipoId);

        // Get all the trapContrTipoList where trapContrToTrapContrTipo equals to trapContrToTrapContrTipoId + 1
        defaultTrapContrTipoShouldNotBeFound("trapContrToTrapContrTipoId.equals=" + (trapContrToTrapContrTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTrapContrTipoShouldBeFound(String filter) throws Exception {
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trapContrTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTrapContrTipoShouldNotBeFound(String filter) throws Exception {
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTrapContrTipo() throws Exception {
        // Get the trapContrTipo
        restTrapContrTipoMockMvc.perform(get("/api/trap-contr-tipos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrapContrTipo() throws Exception {
        // Initialize the database
        trapContrTipoService.save(trapContrTipo);

        int databaseSizeBeforeUpdate = trapContrTipoRepository.findAll().size();

        // Update the trapContrTipo
        TrapContrTipo updatedTrapContrTipo = trapContrTipoRepository.findById(trapContrTipo.getId()).get();
        // Disconnect from session so that the updates on updatedTrapContrTipo are not directly saved in db
        em.detach(updatedTrapContrTipo);
        updatedTrapContrTipo
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTrapContrTipoMockMvc.perform(put("/api/trap-contr-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrapContrTipo)))
            .andExpect(status().isOk());

        // Validate the TrapContrTipo in the database
        List<TrapContrTipo> trapContrTipoList = trapContrTipoRepository.findAll();
        assertThat(trapContrTipoList).hasSize(databaseSizeBeforeUpdate);
        TrapContrTipo testTrapContrTipo = trapContrTipoList.get(trapContrTipoList.size() - 1);
        assertThat(testTrapContrTipo.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTrapContrTipo.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTrapContrTipo.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTrapContrTipo.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTrapContrTipo.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTrapContrTipo() throws Exception {
        int databaseSizeBeforeUpdate = trapContrTipoRepository.findAll().size();

        // Create the TrapContrTipo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrapContrTipoMockMvc.perform(put("/api/trap-contr-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trapContrTipo)))
            .andExpect(status().isBadRequest());

        // Validate the TrapContrTipo in the database
        List<TrapContrTipo> trapContrTipoList = trapContrTipoRepository.findAll();
        assertThat(trapContrTipoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrapContrTipo() throws Exception {
        // Initialize the database
        trapContrTipoService.save(trapContrTipo);

        int databaseSizeBeforeDelete = trapContrTipoRepository.findAll().size();

        // Delete the trapContrTipo
        restTrapContrTipoMockMvc.perform(delete("/api/trap-contr-tipos/{id}", trapContrTipo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TrapContrTipo> trapContrTipoList = trapContrTipoRepository.findAll();
        assertThat(trapContrTipoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrapContrTipo.class);
        TrapContrTipo trapContrTipo1 = new TrapContrTipo();
        trapContrTipo1.setId(1L);
        TrapContrTipo trapContrTipo2 = new TrapContrTipo();
        trapContrTipo2.setId(trapContrTipo1.getId());
        assertThat(trapContrTipo1).isEqualTo(trapContrTipo2);
        trapContrTipo2.setId(2L);
        assertThat(trapContrTipo1).isNotEqualTo(trapContrTipo2);
        trapContrTipo1.setId(null);
        assertThat(trapContrTipo1).isNotEqualTo(trapContrTipo2);
    }
}
