package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.PartAppez;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.repository.PartAppezRepository;
import com.terrambe.service.PartAppezService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.PartAppezCriteria;
import com.terrambe.service.PartAppezQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PartAppezResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class PartAppezResourceIT {

    private static final String DEFAULT_FOGLIO = "AAAAAAAAAA";
    private static final String UPDATED_FOGLIO = "BBBBBBBBBB";

    private static final String DEFAULT_PART = "AAAAAAAAAA";
    private static final String UPDATED_PART = "BBBBBBBBBB";

    private static final String DEFAULT_SUB = "AAAAAAAAAA";
    private static final String UPDATED_SUB = "BBBBBBBBBB";

    private static final String DEFAULT_SEZIONE = "AAAAAAAAAA";
    private static final String UPDATED_SEZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private PartAppezRepository partAppezRepository;

    @Autowired
    private PartAppezService partAppezService;

    @Autowired
    private PartAppezQueryService partAppezQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPartAppezMockMvc;

    private PartAppez partAppez;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PartAppezResource partAppezResource = new PartAppezResource(partAppezService, partAppezQueryService);
        this.restPartAppezMockMvc = MockMvcBuilders.standaloneSetup(partAppezResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PartAppez createEntity(EntityManager em) {
        PartAppez partAppez = new PartAppez()
            .foglio(DEFAULT_FOGLIO)
            .part(DEFAULT_PART)
            .sub(DEFAULT_SUB)
            .sezione(DEFAULT_SEZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return partAppez;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PartAppez createUpdatedEntity(EntityManager em) {
        PartAppez partAppez = new PartAppez()
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .sezione(UPDATED_SEZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return partAppez;
    }

    @BeforeEach
    public void initTest() {
        partAppez = createEntity(em);
    }

    @Test
    @Transactional
    public void createPartAppez() throws Exception {
        int databaseSizeBeforeCreate = partAppezRepository.findAll().size();

        // Create the PartAppez
        restPartAppezMockMvc.perform(post("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partAppez)))
            .andExpect(status().isCreated());

        // Validate the PartAppez in the database
        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeCreate + 1);
        PartAppez testPartAppez = partAppezList.get(partAppezList.size() - 1);
        assertThat(testPartAppez.getFoglio()).isEqualTo(DEFAULT_FOGLIO);
        assertThat(testPartAppez.getPart()).isEqualTo(DEFAULT_PART);
        assertThat(testPartAppez.getSub()).isEqualTo(DEFAULT_SUB);
        assertThat(testPartAppez.getSezione()).isEqualTo(DEFAULT_SEZIONE);
        assertThat(testPartAppez.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testPartAppez.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testPartAppez.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testPartAppez.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testPartAppez.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createPartAppezWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = partAppezRepository.findAll().size();

        // Create the PartAppez with an existing ID
        partAppez.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartAppezMockMvc.perform(post("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partAppez)))
            .andExpect(status().isBadRequest());

        // Validate the PartAppez in the database
        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFoglioIsRequired() throws Exception {
        int databaseSizeBeforeTest = partAppezRepository.findAll().size();
        // set the field null
        partAppez.setFoglio(null);

        // Create the PartAppez, which fails.

        restPartAppezMockMvc.perform(post("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partAppez)))
            .andExpect(status().isBadRequest());

        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPartIsRequired() throws Exception {
        int databaseSizeBeforeTest = partAppezRepository.findAll().size();
        // set the field null
        partAppez.setPart(null);

        // Create the PartAppez, which fails.

        restPartAppezMockMvc.perform(post("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partAppez)))
            .andExpect(status().isBadRequest());

        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPartAppezs() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList
        restPartAppezMockMvc.perform(get("/api/part-appezs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partAppez.getId().intValue())))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO.toString())))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART.toString())))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB.toString())))
            .andExpect(jsonPath("$.[*].sezione").value(hasItem(DEFAULT_SEZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getPartAppez() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get the partAppez
        restPartAppezMockMvc.perform(get("/api/part-appezs/{id}", partAppez.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(partAppez.getId().intValue()))
            .andExpect(jsonPath("$.foglio").value(DEFAULT_FOGLIO.toString()))
            .andExpect(jsonPath("$.part").value(DEFAULT_PART.toString()))
            .andExpect(jsonPath("$.sub").value(DEFAULT_SUB.toString()))
            .andExpect(jsonPath("$.sezione").value(DEFAULT_SEZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllPartAppezsByFoglioIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where foglio equals to DEFAULT_FOGLIO
        defaultPartAppezShouldBeFound("foglio.equals=" + DEFAULT_FOGLIO);

        // Get all the partAppezList where foglio equals to UPDATED_FOGLIO
        defaultPartAppezShouldNotBeFound("foglio.equals=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByFoglioIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where foglio in DEFAULT_FOGLIO or UPDATED_FOGLIO
        defaultPartAppezShouldBeFound("foglio.in=" + DEFAULT_FOGLIO + "," + UPDATED_FOGLIO);

        // Get all the partAppezList where foglio equals to UPDATED_FOGLIO
        defaultPartAppezShouldNotBeFound("foglio.in=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByFoglioIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where foglio is not null
        defaultPartAppezShouldBeFound("foglio.specified=true");

        // Get all the partAppezList where foglio is null
        defaultPartAppezShouldNotBeFound("foglio.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByPartIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where part equals to DEFAULT_PART
        defaultPartAppezShouldBeFound("part.equals=" + DEFAULT_PART);

        // Get all the partAppezList where part equals to UPDATED_PART
        defaultPartAppezShouldNotBeFound("part.equals=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByPartIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where part in DEFAULT_PART or UPDATED_PART
        defaultPartAppezShouldBeFound("part.in=" + DEFAULT_PART + "," + UPDATED_PART);

        // Get all the partAppezList where part equals to UPDATED_PART
        defaultPartAppezShouldNotBeFound("part.in=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByPartIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where part is not null
        defaultPartAppezShouldBeFound("part.specified=true");

        // Get all the partAppezList where part is null
        defaultPartAppezShouldNotBeFound("part.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySubIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sub equals to DEFAULT_SUB
        defaultPartAppezShouldBeFound("sub.equals=" + DEFAULT_SUB);

        // Get all the partAppezList where sub equals to UPDATED_SUB
        defaultPartAppezShouldNotBeFound("sub.equals=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySubIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sub in DEFAULT_SUB or UPDATED_SUB
        defaultPartAppezShouldBeFound("sub.in=" + DEFAULT_SUB + "," + UPDATED_SUB);

        // Get all the partAppezList where sub equals to UPDATED_SUB
        defaultPartAppezShouldNotBeFound("sub.in=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySubIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sub is not null
        defaultPartAppezShouldBeFound("sub.specified=true");

        // Get all the partAppezList where sub is null
        defaultPartAppezShouldNotBeFound("sub.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySezioneIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sezione equals to DEFAULT_SEZIONE
        defaultPartAppezShouldBeFound("sezione.equals=" + DEFAULT_SEZIONE);

        // Get all the partAppezList where sezione equals to UPDATED_SEZIONE
        defaultPartAppezShouldNotBeFound("sezione.equals=" + UPDATED_SEZIONE);
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySezioneIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sezione in DEFAULT_SEZIONE or UPDATED_SEZIONE
        defaultPartAppezShouldBeFound("sezione.in=" + DEFAULT_SEZIONE + "," + UPDATED_SEZIONE);

        // Get all the partAppezList where sezione equals to UPDATED_SEZIONE
        defaultPartAppezShouldNotBeFound("sezione.in=" + UPDATED_SEZIONE);
    }

    @Test
    @Transactional
    public void getAllPartAppezsBySezioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where sezione is not null
        defaultPartAppezShouldBeFound("sezione.specified=true");

        // Get all the partAppezList where sezione is null
        defaultPartAppezShouldNotBeFound("sezione.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali is not null
        defaultPartAppezShouldBeFound("dataInizVali.specified=true");

        // Get all the partAppezList where dataInizVali is null
        defaultPartAppezShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultPartAppezShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the partAppezList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultPartAppezShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali is not null
        defaultPartAppezShouldBeFound("dataFineVali.specified=true");

        // Get all the partAppezList where dataFineVali is null
        defaultPartAppezShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultPartAppezShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the partAppezList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultPartAppezShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator is not null
        defaultPartAppezShouldBeFound("userIdCreator.specified=true");

        // Get all the partAppezList where userIdCreator is null
        defaultPartAppezShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultPartAppezShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the partAppezList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultPartAppezShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod is not null
        defaultPartAppezShouldBeFound("userIdLastMod.specified=true");

        // Get all the partAppezList where userIdLastMod is null
        defaultPartAppezShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPartAppezsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);

        // Get all the partAppezList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultPartAppezShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the partAppezList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultPartAppezShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllPartAppezsByPartAppzToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);
        Appezzamenti partAppzToAppz = AppezzamentiResourceIT.createEntity(em);
        em.persist(partAppzToAppz);
        em.flush();
        partAppez.setPartAppzToAppz(partAppzToAppz);
        partAppezRepository.saveAndFlush(partAppez);
        Long partAppzToAppzId = partAppzToAppz.getId();

        // Get all the partAppezList where partAppzToAppz equals to partAppzToAppzId
        defaultPartAppezShouldBeFound("partAppzToAppzId.equals=" + partAppzToAppzId);

        // Get all the partAppezList where partAppzToAppz equals to partAppzToAppzId + 1
        defaultPartAppezShouldNotBeFound("partAppzToAppzId.equals=" + (partAppzToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllPartAppezsByPartAppzToPartIsEqualToSomething() throws Exception {
        // Initialize the database
        partAppezRepository.saveAndFlush(partAppez);
        AziSuperficieParticella partAppzToPart = AziSuperficieParticellaResourceIT.createEntity(em);
        em.persist(partAppzToPart);
        em.flush();
        partAppez.setPartAppzToPart(partAppzToPart);
        partAppezRepository.saveAndFlush(partAppez);
        Long partAppzToPartId = partAppzToPart.getId();

        // Get all the partAppezList where partAppzToPart equals to partAppzToPartId
        defaultPartAppezShouldBeFound("partAppzToPartId.equals=" + partAppzToPartId);

        // Get all the partAppezList where partAppzToPart equals to partAppzToPartId + 1
        defaultPartAppezShouldNotBeFound("partAppzToPartId.equals=" + (partAppzToPartId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPartAppezShouldBeFound(String filter) throws Exception {
        restPartAppezMockMvc.perform(get("/api/part-appezs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partAppez.getId().intValue())))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO)))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART)))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB)))
            .andExpect(jsonPath("$.[*].sezione").value(hasItem(DEFAULT_SEZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restPartAppezMockMvc.perform(get("/api/part-appezs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPartAppezShouldNotBeFound(String filter) throws Exception {
        restPartAppezMockMvc.perform(get("/api/part-appezs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPartAppezMockMvc.perform(get("/api/part-appezs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPartAppez() throws Exception {
        // Get the partAppez
        restPartAppezMockMvc.perform(get("/api/part-appezs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePartAppez() throws Exception {
        // Initialize the database
        partAppezService.save(partAppez);

        int databaseSizeBeforeUpdate = partAppezRepository.findAll().size();

        // Update the partAppez
        PartAppez updatedPartAppez = partAppezRepository.findById(partAppez.getId()).get();
        // Disconnect from session so that the updates on updatedPartAppez are not directly saved in db
        em.detach(updatedPartAppez);
        updatedPartAppez
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .sezione(UPDATED_SEZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restPartAppezMockMvc.perform(put("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPartAppez)))
            .andExpect(status().isOk());

        // Validate the PartAppez in the database
        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeUpdate);
        PartAppez testPartAppez = partAppezList.get(partAppezList.size() - 1);
        assertThat(testPartAppez.getFoglio()).isEqualTo(UPDATED_FOGLIO);
        assertThat(testPartAppez.getPart()).isEqualTo(UPDATED_PART);
        assertThat(testPartAppez.getSub()).isEqualTo(UPDATED_SUB);
        assertThat(testPartAppez.getSezione()).isEqualTo(UPDATED_SEZIONE);
        assertThat(testPartAppez.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testPartAppez.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testPartAppez.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testPartAppez.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testPartAppez.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingPartAppez() throws Exception {
        int databaseSizeBeforeUpdate = partAppezRepository.findAll().size();

        // Create the PartAppez

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartAppezMockMvc.perform(put("/api/part-appezs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partAppez)))
            .andExpect(status().isBadRequest());

        // Validate the PartAppez in the database
        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePartAppez() throws Exception {
        // Initialize the database
        partAppezService.save(partAppez);

        int databaseSizeBeforeDelete = partAppezRepository.findAll().size();

        // Delete the partAppez
        restPartAppezMockMvc.perform(delete("/api/part-appezs/{id}", partAppez.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PartAppez> partAppezList = partAppezRepository.findAll();
        assertThat(partAppezList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PartAppez.class);
        PartAppez partAppez1 = new PartAppez();
        partAppez1.setId(1L);
        PartAppez partAppez2 = new PartAppez();
        partAppez2.setId(partAppez1.getId());
        assertThat(partAppez1).isEqualTo(partAppez2);
        partAppez2.setId(2L);
        assertThat(partAppez1).isNotEqualTo(partAppez2);
        partAppez1.setId(null);
        assertThat(partAppez1).isNotEqualTo(partAppez2);
    }
}
