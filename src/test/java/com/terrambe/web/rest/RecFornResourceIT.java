package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecForn;
import com.terrambe.domain.Fornitori;
import com.terrambe.repository.RecFornRepository;
import com.terrambe.service.RecFornService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecFornCriteria;
import com.terrambe.service.RecFornQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecFornResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecFornResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecFornRepository recFornRepository;

    @Autowired
    private RecFornService recFornService;

    @Autowired
    private RecFornQueryService recFornQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecFornMockMvc;

    private RecForn recForn;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecFornResource recFornResource = new RecFornResource(recFornService, recFornQueryService);
        this.restRecFornMockMvc = MockMvcBuilders.standaloneSetup(recFornResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecForn createEntity(EntityManager em) {
        RecForn recForn = new RecForn()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recForn;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecForn createUpdatedEntity(EntityManager em) {
        RecForn recForn = new RecForn()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recForn;
    }

    @BeforeEach
    public void initTest() {
        recForn = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecForn() throws Exception {
        int databaseSizeBeforeCreate = recFornRepository.findAll().size();

        // Create the RecForn
        restRecFornMockMvc.perform(post("/api/rec-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recForn)))
            .andExpect(status().isCreated());

        // Validate the RecForn in the database
        List<RecForn> recFornList = recFornRepository.findAll();
        assertThat(recFornList).hasSize(databaseSizeBeforeCreate + 1);
        RecForn testRecForn = recFornList.get(recFornList.size() - 1);
        assertThat(testRecForn.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecForn.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecForn.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecForn.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecForn.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecForn.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecForn.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecForn.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecForn.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecForn.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecForn.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecFornWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recFornRepository.findAll().size();

        // Create the RecForn with an existing ID
        recForn.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecFornMockMvc.perform(post("/api/rec-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recForn)))
            .andExpect(status().isBadRequest());

        // Validate the RecForn in the database
        List<RecForn> recFornList = recFornRepository.findAll();
        assertThat(recFornList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecForns() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList
        restRecFornMockMvc.perform(get("/api/rec-forns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecForn() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get the recForn
        restRecFornMockMvc.perform(get("/api/rec-forns/{id}", recForn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recForn.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecFornsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where email equals to DEFAULT_EMAIL
        defaultRecFornShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recFornList where email equals to UPDATED_EMAIL
        defaultRecFornShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecFornsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecFornShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recFornList where email equals to UPDATED_EMAIL
        defaultRecFornShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecFornsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where email is not null
        defaultRecFornShouldBeFound("email.specified=true");

        // Get all the recFornList where email is null
        defaultRecFornShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where pec equals to DEFAULT_PEC
        defaultRecFornShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recFornList where pec equals to UPDATED_PEC
        defaultRecFornShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecFornsByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecFornShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recFornList where pec equals to UPDATED_PEC
        defaultRecFornShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecFornsByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where pec is not null
        defaultRecFornShouldBeFound("pec.specified=true");

        // Get all the recFornList where pec is null
        defaultRecFornShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where telefono equals to DEFAULT_TELEFONO
        defaultRecFornShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recFornList where telefono equals to UPDATED_TELEFONO
        defaultRecFornShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecFornsByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecFornShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recFornList where telefono equals to UPDATED_TELEFONO
        defaultRecFornShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecFornsByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where telefono is not null
        defaultRecFornShouldBeFound("telefono.specified=true");

        // Get all the recFornList where telefono is null
        defaultRecFornShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where fax equals to DEFAULT_FAX
        defaultRecFornShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recFornList where fax equals to UPDATED_FAX
        defaultRecFornShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecFornsByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecFornShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recFornList where fax equals to UPDATED_FAX
        defaultRecFornShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecFornsByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where fax is not null
        defaultRecFornShouldBeFound("fax.specified=true");

        // Get all the recFornList where fax is null
        defaultRecFornShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell equals to DEFAULT_CELL
        defaultRecFornShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recFornList where cell equals to UPDATED_CELL
        defaultRecFornShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecFornsByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecFornShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recFornList where cell equals to UPDATED_CELL
        defaultRecFornShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecFornsByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell is not null
        defaultRecFornShouldBeFound("cell.specified=true");

        // Get all the recFornList where cell is null
        defaultRecFornShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell2 equals to DEFAULT_CELL_2
        defaultRecFornShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recFornList where cell2 equals to UPDATED_CELL_2
        defaultRecFornShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecFornsByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecFornShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recFornList where cell2 equals to UPDATED_CELL_2
        defaultRecFornShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecFornsByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where cell2 is not null
        defaultRecFornShouldBeFound("cell2.specified=true");

        // Get all the recFornList where cell2 is null
        defaultRecFornShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali is not null
        defaultRecFornShouldBeFound("dataInizVali.specified=true");

        // Get all the recFornList where dataInizVali is null
        defaultRecFornShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecFornShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recFornList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecFornShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali is not null
        defaultRecFornShouldBeFound("dataFineVali.specified=true");

        // Get all the recFornList where dataFineVali is null
        defaultRecFornShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecFornsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecFornShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recFornList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecFornShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator is not null
        defaultRecFornShouldBeFound("userIdCreator.specified=true");

        // Get all the recFornList where userIdCreator is null
        defaultRecFornShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecFornShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recFornList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecFornShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod is not null
        defaultRecFornShouldBeFound("userIdLastMod.specified=true");

        // Get all the recFornList where userIdLastMod is null
        defaultRecFornShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecFornsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);

        // Get all the recFornList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecFornShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recFornList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecFornShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecFornsByRecToFornIsEqualToSomething() throws Exception {
        // Initialize the database
        recFornRepository.saveAndFlush(recForn);
        Fornitori recToForn = FornitoriResourceIT.createEntity(em);
        em.persist(recToForn);
        em.flush();
        recForn.setRecToForn(recToForn);
        recFornRepository.saveAndFlush(recForn);
        Long recToFornId = recToForn.getId();

        // Get all the recFornList where recToForn equals to recToFornId
        defaultRecFornShouldBeFound("recToFornId.equals=" + recToFornId);

        // Get all the recFornList where recToForn equals to recToFornId + 1
        defaultRecFornShouldNotBeFound("recToFornId.equals=" + (recToFornId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecFornShouldBeFound(String filter) throws Exception {
        restRecFornMockMvc.perform(get("/api/rec-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecFornMockMvc.perform(get("/api/rec-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecFornShouldNotBeFound(String filter) throws Exception {
        restRecFornMockMvc.perform(get("/api/rec-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecFornMockMvc.perform(get("/api/rec-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecForn() throws Exception {
        // Get the recForn
        restRecFornMockMvc.perform(get("/api/rec-forns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecForn() throws Exception {
        // Initialize the database
        recFornService.save(recForn);

        int databaseSizeBeforeUpdate = recFornRepository.findAll().size();

        // Update the recForn
        RecForn updatedRecForn = recFornRepository.findById(recForn.getId()).get();
        // Disconnect from session so that the updates on updatedRecForn are not directly saved in db
        em.detach(updatedRecForn);
        updatedRecForn
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecFornMockMvc.perform(put("/api/rec-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecForn)))
            .andExpect(status().isOk());

        // Validate the RecForn in the database
        List<RecForn> recFornList = recFornRepository.findAll();
        assertThat(recFornList).hasSize(databaseSizeBeforeUpdate);
        RecForn testRecForn = recFornList.get(recFornList.size() - 1);
        assertThat(testRecForn.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecForn.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecForn.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecForn.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecForn.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecForn.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecForn.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecForn.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecForn.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecForn.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecForn.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecForn() throws Exception {
        int databaseSizeBeforeUpdate = recFornRepository.findAll().size();

        // Create the RecForn

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecFornMockMvc.perform(put("/api/rec-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recForn)))
            .andExpect(status().isBadRequest());

        // Validate the RecForn in the database
        List<RecForn> recFornList = recFornRepository.findAll();
        assertThat(recFornList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecForn() throws Exception {
        // Initialize the database
        recFornService.save(recForn);

        int databaseSizeBeforeDelete = recFornRepository.findAll().size();

        // Delete the recForn
        restRecFornMockMvc.perform(delete("/api/rec-forns/{id}", recForn.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecForn> recFornList = recFornRepository.findAll();
        assertThat(recFornList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecForn.class);
        RecForn recForn1 = new RecForn();
        recForn1.setId(1L);
        RecForn recForn2 = new RecForn();
        recForn2.setId(recForn1.getId());
        assertThat(recForn1).isEqualTo(recForn2);
        recForn2.setId(2L);
        assertThat(recForn1).isNotEqualTo(recForn2);
        recForn1.setId(null);
        assertThat(recForn1).isNotEqualTo(recForn2);
    }
}
