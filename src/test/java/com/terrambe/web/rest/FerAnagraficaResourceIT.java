package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.FerAnagrafica;
import com.terrambe.domain.FertTipo;
import com.terrambe.repository.FerAnagraficaRepository;
import com.terrambe.service.FerAnagraficaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FerAnagraficaCriteria;
import com.terrambe.service.FerAnagraficaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FerAnagraficaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FerAnagraficaResourceIT {

    private static final String DEFAULT_DENOMINAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DENOMINAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_DITTA_PRODUTTRICE = "AAAAAAAAAA";
    private static final String UPDATED_DITTA_PRODUTTRICE = "BBBBBBBBBB";

    private static final Double DEFAULT_DATO_N = 1D;
    private static final Double UPDATED_DATO_N = 2D;
    private static final Double SMALLER_DATO_N = 1D - 1D;

    private static final Double DEFAULT_DATO_P = 1D;
    private static final Double UPDATED_DATO_P = 2D;
    private static final Double SMALLER_DATO_P = 1D - 1D;

    private static final Double DEFAULT_DATO_K = 1D;
    private static final Double UPDATED_DATO_K = 2D;
    private static final Double SMALLER_DATO_K = 1D - 1D;

    private static final String DEFAULT_TIPO_CONCIME = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_CONCIME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BIO = false;
    private static final Boolean UPDATED_BIO = true;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FerAnagraficaRepository ferAnagraficaRepository;

    @Autowired
    private FerAnagraficaService ferAnagraficaService;

    @Autowired
    private FerAnagraficaQueryService ferAnagraficaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFerAnagraficaMockMvc;

    private FerAnagrafica ferAnagrafica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FerAnagraficaResource ferAnagraficaResource = new FerAnagraficaResource(ferAnagraficaService, ferAnagraficaQueryService);
        this.restFerAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(ferAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FerAnagrafica createEntity(EntityManager em) {
        FerAnagrafica ferAnagrafica = new FerAnagrafica()
            .denominazione(DEFAULT_DENOMINAZIONE)
            .dittaProduttrice(DEFAULT_DITTA_PRODUTTRICE)
            .datoN(DEFAULT_DATO_N)
            .datoP(DEFAULT_DATO_P)
            .datoK(DEFAULT_DATO_K)
            .tipoConcime(DEFAULT_TIPO_CONCIME)
            .bio(DEFAULT_BIO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return ferAnagrafica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FerAnagrafica createUpdatedEntity(EntityManager em) {
        FerAnagrafica ferAnagrafica = new FerAnagrafica()
            .denominazione(UPDATED_DENOMINAZIONE)
            .dittaProduttrice(UPDATED_DITTA_PRODUTTRICE)
            .datoN(UPDATED_DATO_N)
            .datoP(UPDATED_DATO_P)
            .datoK(UPDATED_DATO_K)
            .tipoConcime(UPDATED_TIPO_CONCIME)
            .bio(UPDATED_BIO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return ferAnagrafica;
    }

    @BeforeEach
    public void initTest() {
        ferAnagrafica = createEntity(em);
    }

    @Test
    @Transactional
    public void createFerAnagrafica() throws Exception {
        int databaseSizeBeforeCreate = ferAnagraficaRepository.findAll().size();

        // Create the FerAnagrafica
        restFerAnagraficaMockMvc.perform(post("/api/fer-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ferAnagrafica)))
            .andExpect(status().isCreated());

        // Validate the FerAnagrafica in the database
        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeCreate + 1);
        FerAnagrafica testFerAnagrafica = ferAnagraficaList.get(ferAnagraficaList.size() - 1);
        assertThat(testFerAnagrafica.getDenominazione()).isEqualTo(DEFAULT_DENOMINAZIONE);
        assertThat(testFerAnagrafica.getDittaProduttrice()).isEqualTo(DEFAULT_DITTA_PRODUTTRICE);
        assertThat(testFerAnagrafica.getDatoN()).isEqualTo(DEFAULT_DATO_N);
        assertThat(testFerAnagrafica.getDatoP()).isEqualTo(DEFAULT_DATO_P);
        assertThat(testFerAnagrafica.getDatoK()).isEqualTo(DEFAULT_DATO_K);
        assertThat(testFerAnagrafica.getTipoConcime()).isEqualTo(DEFAULT_TIPO_CONCIME);
        assertThat(testFerAnagrafica.isBio()).isEqualTo(DEFAULT_BIO);
        assertThat(testFerAnagrafica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFerAnagrafica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFerAnagrafica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFerAnagrafica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFerAnagrafica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFerAnagraficaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ferAnagraficaRepository.findAll().size();

        // Create the FerAnagrafica with an existing ID
        ferAnagrafica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFerAnagraficaMockMvc.perform(post("/api/fer-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ferAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the FerAnagrafica in the database
        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDenominazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = ferAnagraficaRepository.findAll().size();
        // set the field null
        ferAnagrafica.setDenominazione(null);

        // Create the FerAnagrafica, which fails.

        restFerAnagraficaMockMvc.perform(post("/api/fer-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ferAnagrafica)))
            .andExpect(status().isBadRequest());

        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficas() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ferAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].denominazione").value(hasItem(DEFAULT_DENOMINAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dittaProduttrice").value(hasItem(DEFAULT_DITTA_PRODUTTRICE.toString())))
            .andExpect(jsonPath("$.[*].datoN").value(hasItem(DEFAULT_DATO_N.doubleValue())))
            .andExpect(jsonPath("$.[*].datoP").value(hasItem(DEFAULT_DATO_P.doubleValue())))
            .andExpect(jsonPath("$.[*].datoK").value(hasItem(DEFAULT_DATO_K.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoConcime").value(hasItem(DEFAULT_TIPO_CONCIME.toString())))
            .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getFerAnagrafica() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get the ferAnagrafica
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas/{id}", ferAnagrafica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ferAnagrafica.getId().intValue()))
            .andExpect(jsonPath("$.denominazione").value(DEFAULT_DENOMINAZIONE.toString()))
            .andExpect(jsonPath("$.dittaProduttrice").value(DEFAULT_DITTA_PRODUTTRICE.toString()))
            .andExpect(jsonPath("$.datoN").value(DEFAULT_DATO_N.doubleValue()))
            .andExpect(jsonPath("$.datoP").value(DEFAULT_DATO_P.doubleValue()))
            .andExpect(jsonPath("$.datoK").value(DEFAULT_DATO_K.doubleValue()))
            .andExpect(jsonPath("$.tipoConcime").value(DEFAULT_TIPO_CONCIME.toString()))
            .andExpect(jsonPath("$.bio").value(DEFAULT_BIO.booleanValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDenominazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where denominazione equals to DEFAULT_DENOMINAZIONE
        defaultFerAnagraficaShouldBeFound("denominazione.equals=" + DEFAULT_DENOMINAZIONE);

        // Get all the ferAnagraficaList where denominazione equals to UPDATED_DENOMINAZIONE
        defaultFerAnagraficaShouldNotBeFound("denominazione.equals=" + UPDATED_DENOMINAZIONE);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDenominazioneIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where denominazione in DEFAULT_DENOMINAZIONE or UPDATED_DENOMINAZIONE
        defaultFerAnagraficaShouldBeFound("denominazione.in=" + DEFAULT_DENOMINAZIONE + "," + UPDATED_DENOMINAZIONE);

        // Get all the ferAnagraficaList where denominazione equals to UPDATED_DENOMINAZIONE
        defaultFerAnagraficaShouldNotBeFound("denominazione.in=" + UPDATED_DENOMINAZIONE);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDenominazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where denominazione is not null
        defaultFerAnagraficaShouldBeFound("denominazione.specified=true");

        // Get all the ferAnagraficaList where denominazione is null
        defaultFerAnagraficaShouldNotBeFound("denominazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDittaProduttriceIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dittaProduttrice equals to DEFAULT_DITTA_PRODUTTRICE
        defaultFerAnagraficaShouldBeFound("dittaProduttrice.equals=" + DEFAULT_DITTA_PRODUTTRICE);

        // Get all the ferAnagraficaList where dittaProduttrice equals to UPDATED_DITTA_PRODUTTRICE
        defaultFerAnagraficaShouldNotBeFound("dittaProduttrice.equals=" + UPDATED_DITTA_PRODUTTRICE);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDittaProduttriceIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dittaProduttrice in DEFAULT_DITTA_PRODUTTRICE or UPDATED_DITTA_PRODUTTRICE
        defaultFerAnagraficaShouldBeFound("dittaProduttrice.in=" + DEFAULT_DITTA_PRODUTTRICE + "," + UPDATED_DITTA_PRODUTTRICE);

        // Get all the ferAnagraficaList where dittaProduttrice equals to UPDATED_DITTA_PRODUTTRICE
        defaultFerAnagraficaShouldNotBeFound("dittaProduttrice.in=" + UPDATED_DITTA_PRODUTTRICE);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDittaProduttriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dittaProduttrice is not null
        defaultFerAnagraficaShouldBeFound("dittaProduttrice.specified=true");

        // Get all the ferAnagraficaList where dittaProduttrice is null
        defaultFerAnagraficaShouldNotBeFound("dittaProduttrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN equals to DEFAULT_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.equals=" + DEFAULT_DATO_N);

        // Get all the ferAnagraficaList where datoN equals to UPDATED_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.equals=" + UPDATED_DATO_N);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN in DEFAULT_DATO_N or UPDATED_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.in=" + DEFAULT_DATO_N + "," + UPDATED_DATO_N);

        // Get all the ferAnagraficaList where datoN equals to UPDATED_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.in=" + UPDATED_DATO_N);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN is not null
        defaultFerAnagraficaShouldBeFound("datoN.specified=true");

        // Get all the ferAnagraficaList where datoN is null
        defaultFerAnagraficaShouldNotBeFound("datoN.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN is greater than or equal to DEFAULT_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.greaterThanOrEqual=" + DEFAULT_DATO_N);

        // Get all the ferAnagraficaList where datoN is greater than or equal to UPDATED_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.greaterThanOrEqual=" + UPDATED_DATO_N);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN is less than or equal to DEFAULT_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.lessThanOrEqual=" + DEFAULT_DATO_N);

        // Get all the ferAnagraficaList where datoN is less than or equal to SMALLER_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.lessThanOrEqual=" + SMALLER_DATO_N);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN is less than DEFAULT_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.lessThan=" + DEFAULT_DATO_N);

        // Get all the ferAnagraficaList where datoN is less than UPDATED_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.lessThan=" + UPDATED_DATO_N);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoNIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoN is greater than DEFAULT_DATO_N
        defaultFerAnagraficaShouldNotBeFound("datoN.greaterThan=" + DEFAULT_DATO_N);

        // Get all the ferAnagraficaList where datoN is greater than SMALLER_DATO_N
        defaultFerAnagraficaShouldBeFound("datoN.greaterThan=" + SMALLER_DATO_N);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP equals to DEFAULT_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.equals=" + DEFAULT_DATO_P);

        // Get all the ferAnagraficaList where datoP equals to UPDATED_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.equals=" + UPDATED_DATO_P);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP in DEFAULT_DATO_P or UPDATED_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.in=" + DEFAULT_DATO_P + "," + UPDATED_DATO_P);

        // Get all the ferAnagraficaList where datoP equals to UPDATED_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.in=" + UPDATED_DATO_P);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP is not null
        defaultFerAnagraficaShouldBeFound("datoP.specified=true");

        // Get all the ferAnagraficaList where datoP is null
        defaultFerAnagraficaShouldNotBeFound("datoP.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP is greater than or equal to DEFAULT_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.greaterThanOrEqual=" + DEFAULT_DATO_P);

        // Get all the ferAnagraficaList where datoP is greater than or equal to UPDATED_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.greaterThanOrEqual=" + UPDATED_DATO_P);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP is less than or equal to DEFAULT_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.lessThanOrEqual=" + DEFAULT_DATO_P);

        // Get all the ferAnagraficaList where datoP is less than or equal to SMALLER_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.lessThanOrEqual=" + SMALLER_DATO_P);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP is less than DEFAULT_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.lessThan=" + DEFAULT_DATO_P);

        // Get all the ferAnagraficaList where datoP is less than UPDATED_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.lessThan=" + UPDATED_DATO_P);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoPIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoP is greater than DEFAULT_DATO_P
        defaultFerAnagraficaShouldNotBeFound("datoP.greaterThan=" + DEFAULT_DATO_P);

        // Get all the ferAnagraficaList where datoP is greater than SMALLER_DATO_P
        defaultFerAnagraficaShouldBeFound("datoP.greaterThan=" + SMALLER_DATO_P);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK equals to DEFAULT_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.equals=" + DEFAULT_DATO_K);

        // Get all the ferAnagraficaList where datoK equals to UPDATED_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.equals=" + UPDATED_DATO_K);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK in DEFAULT_DATO_K or UPDATED_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.in=" + DEFAULT_DATO_K + "," + UPDATED_DATO_K);

        // Get all the ferAnagraficaList where datoK equals to UPDATED_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.in=" + UPDATED_DATO_K);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK is not null
        defaultFerAnagraficaShouldBeFound("datoK.specified=true");

        // Get all the ferAnagraficaList where datoK is null
        defaultFerAnagraficaShouldNotBeFound("datoK.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK is greater than or equal to DEFAULT_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.greaterThanOrEqual=" + DEFAULT_DATO_K);

        // Get all the ferAnagraficaList where datoK is greater than or equal to UPDATED_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.greaterThanOrEqual=" + UPDATED_DATO_K);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK is less than or equal to DEFAULT_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.lessThanOrEqual=" + DEFAULT_DATO_K);

        // Get all the ferAnagraficaList where datoK is less than or equal to SMALLER_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.lessThanOrEqual=" + SMALLER_DATO_K);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK is less than DEFAULT_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.lessThan=" + DEFAULT_DATO_K);

        // Get all the ferAnagraficaList where datoK is less than UPDATED_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.lessThan=" + UPDATED_DATO_K);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDatoKIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where datoK is greater than DEFAULT_DATO_K
        defaultFerAnagraficaShouldNotBeFound("datoK.greaterThan=" + DEFAULT_DATO_K);

        // Get all the ferAnagraficaList where datoK is greater than SMALLER_DATO_K
        defaultFerAnagraficaShouldBeFound("datoK.greaterThan=" + SMALLER_DATO_K);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByTipoConcimeIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where tipoConcime equals to DEFAULT_TIPO_CONCIME
        defaultFerAnagraficaShouldBeFound("tipoConcime.equals=" + DEFAULT_TIPO_CONCIME);

        // Get all the ferAnagraficaList where tipoConcime equals to UPDATED_TIPO_CONCIME
        defaultFerAnagraficaShouldNotBeFound("tipoConcime.equals=" + UPDATED_TIPO_CONCIME);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByTipoConcimeIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where tipoConcime in DEFAULT_TIPO_CONCIME or UPDATED_TIPO_CONCIME
        defaultFerAnagraficaShouldBeFound("tipoConcime.in=" + DEFAULT_TIPO_CONCIME + "," + UPDATED_TIPO_CONCIME);

        // Get all the ferAnagraficaList where tipoConcime equals to UPDATED_TIPO_CONCIME
        defaultFerAnagraficaShouldNotBeFound("tipoConcime.in=" + UPDATED_TIPO_CONCIME);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByTipoConcimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where tipoConcime is not null
        defaultFerAnagraficaShouldBeFound("tipoConcime.specified=true");

        // Get all the ferAnagraficaList where tipoConcime is null
        defaultFerAnagraficaShouldNotBeFound("tipoConcime.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByBioIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where bio equals to DEFAULT_BIO
        defaultFerAnagraficaShouldBeFound("bio.equals=" + DEFAULT_BIO);

        // Get all the ferAnagraficaList where bio equals to UPDATED_BIO
        defaultFerAnagraficaShouldNotBeFound("bio.equals=" + UPDATED_BIO);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByBioIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where bio in DEFAULT_BIO or UPDATED_BIO
        defaultFerAnagraficaShouldBeFound("bio.in=" + DEFAULT_BIO + "," + UPDATED_BIO);

        // Get all the ferAnagraficaList where bio equals to UPDATED_BIO
        defaultFerAnagraficaShouldNotBeFound("bio.in=" + UPDATED_BIO);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByBioIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where bio is not null
        defaultFerAnagraficaShouldBeFound("bio.specified=true");

        // Get all the ferAnagraficaList where bio is null
        defaultFerAnagraficaShouldNotBeFound("bio.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali is not null
        defaultFerAnagraficaShouldBeFound("dataInizVali.specified=true");

        // Get all the ferAnagraficaList where dataInizVali is null
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFerAnagraficaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the ferAnagraficaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFerAnagraficaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali is not null
        defaultFerAnagraficaShouldBeFound("dataFineVali.specified=true");

        // Get all the ferAnagraficaList where dataFineVali is null
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFerAnagraficaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the ferAnagraficaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFerAnagraficaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator is not null
        defaultFerAnagraficaShouldBeFound("userIdCreator.specified=true");

        // Get all the ferAnagraficaList where userIdCreator is null
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFerAnagraficaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the ferAnagraficaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFerAnagraficaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod is not null
        defaultFerAnagraficaShouldBeFound("userIdLastMod.specified=true");

        // Get all the ferAnagraficaList where userIdLastMod is null
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFerAnagraficasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);

        // Get all the ferAnagraficaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the ferAnagraficaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFerAnagraficaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFerAnagraficasByFertTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);
        FertTipo fertTipo = FertTipoResourceIT.createEntity(em);
        em.persist(fertTipo);
        em.flush();
        ferAnagrafica.setFertTipo(fertTipo);
        ferAnagraficaRepository.saveAndFlush(ferAnagrafica);
        Long fertTipoId = fertTipo.getId();

        // Get all the ferAnagraficaList where fertTipo equals to fertTipoId
        defaultFerAnagraficaShouldBeFound("fertTipoId.equals=" + fertTipoId);

        // Get all the ferAnagraficaList where fertTipo equals to fertTipoId + 1
        defaultFerAnagraficaShouldNotBeFound("fertTipoId.equals=" + (fertTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFerAnagraficaShouldBeFound(String filter) throws Exception {
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ferAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].denominazione").value(hasItem(DEFAULT_DENOMINAZIONE)))
            .andExpect(jsonPath("$.[*].dittaProduttrice").value(hasItem(DEFAULT_DITTA_PRODUTTRICE)))
            .andExpect(jsonPath("$.[*].datoN").value(hasItem(DEFAULT_DATO_N.doubleValue())))
            .andExpect(jsonPath("$.[*].datoP").value(hasItem(DEFAULT_DATO_P.doubleValue())))
            .andExpect(jsonPath("$.[*].datoK").value(hasItem(DEFAULT_DATO_K.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoConcime").value(hasItem(DEFAULT_TIPO_CONCIME)))
            .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFerAnagraficaShouldNotBeFound(String filter) throws Exception {
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFerAnagrafica() throws Exception {
        // Get the ferAnagrafica
        restFerAnagraficaMockMvc.perform(get("/api/fer-anagraficas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFerAnagrafica() throws Exception {
        // Initialize the database
        ferAnagraficaService.save(ferAnagrafica);

        int databaseSizeBeforeUpdate = ferAnagraficaRepository.findAll().size();

        // Update the ferAnagrafica
        FerAnagrafica updatedFerAnagrafica = ferAnagraficaRepository.findById(ferAnagrafica.getId()).get();
        // Disconnect from session so that the updates on updatedFerAnagrafica are not directly saved in db
        em.detach(updatedFerAnagrafica);
        updatedFerAnagrafica
            .denominazione(UPDATED_DENOMINAZIONE)
            .dittaProduttrice(UPDATED_DITTA_PRODUTTRICE)
            .datoN(UPDATED_DATO_N)
            .datoP(UPDATED_DATO_P)
            .datoK(UPDATED_DATO_K)
            .tipoConcime(UPDATED_TIPO_CONCIME)
            .bio(UPDATED_BIO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFerAnagraficaMockMvc.perform(put("/api/fer-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFerAnagrafica)))
            .andExpect(status().isOk());

        // Validate the FerAnagrafica in the database
        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeUpdate);
        FerAnagrafica testFerAnagrafica = ferAnagraficaList.get(ferAnagraficaList.size() - 1);
        assertThat(testFerAnagrafica.getDenominazione()).isEqualTo(UPDATED_DENOMINAZIONE);
        assertThat(testFerAnagrafica.getDittaProduttrice()).isEqualTo(UPDATED_DITTA_PRODUTTRICE);
        assertThat(testFerAnagrafica.getDatoN()).isEqualTo(UPDATED_DATO_N);
        assertThat(testFerAnagrafica.getDatoP()).isEqualTo(UPDATED_DATO_P);
        assertThat(testFerAnagrafica.getDatoK()).isEqualTo(UPDATED_DATO_K);
        assertThat(testFerAnagrafica.getTipoConcime()).isEqualTo(UPDATED_TIPO_CONCIME);
        assertThat(testFerAnagrafica.isBio()).isEqualTo(UPDATED_BIO);
        assertThat(testFerAnagrafica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFerAnagrafica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFerAnagrafica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFerAnagrafica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFerAnagrafica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFerAnagrafica() throws Exception {
        int databaseSizeBeforeUpdate = ferAnagraficaRepository.findAll().size();

        // Create the FerAnagrafica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFerAnagraficaMockMvc.perform(put("/api/fer-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ferAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the FerAnagrafica in the database
        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFerAnagrafica() throws Exception {
        // Initialize the database
        ferAnagraficaService.save(ferAnagrafica);

        int databaseSizeBeforeDelete = ferAnagraficaRepository.findAll().size();

        // Delete the ferAnagrafica
        restFerAnagraficaMockMvc.perform(delete("/api/fer-anagraficas/{id}", ferAnagrafica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FerAnagrafica> ferAnagraficaList = ferAnagraficaRepository.findAll();
        assertThat(ferAnagraficaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FerAnagrafica.class);
        FerAnagrafica ferAnagrafica1 = new FerAnagrafica();
        ferAnagrafica1.setId(1L);
        FerAnagrafica ferAnagrafica2 = new FerAnagrafica();
        ferAnagrafica2.setId(ferAnagrafica1.getId());
        assertThat(ferAnagrafica1).isEqualTo(ferAnagrafica2);
        ferAnagrafica2.setId(2L);
        assertThat(ferAnagrafica1).isNotEqualTo(ferAnagrafica2);
        ferAnagrafica1.setId(null);
        assertThat(ferAnagrafica1).isNotEqualTo(ferAnagrafica2);
    }
}
