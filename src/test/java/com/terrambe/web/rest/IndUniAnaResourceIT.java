package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndUniAna;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.repository.IndUniAnaRepository;
import com.terrambe.service.IndUniAnaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndUniAnaCriteria;
import com.terrambe.service.IndUniAnaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndUniAnaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndUniAnaResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndUniAnaRepository indUniAnaRepository;

    @Autowired
    private IndUniAnaService indUniAnaService;

    @Autowired
    private IndUniAnaQueryService indUniAnaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndUniAnaMockMvc;

    private IndUniAna indUniAna;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndUniAnaResource indUniAnaResource = new IndUniAnaResource(indUniAnaService, indUniAnaQueryService);
        this.restIndUniAnaMockMvc = MockMvcBuilders.standaloneSetup(indUniAnaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndUniAna createEntity(EntityManager em) {
        IndUniAna indUniAna = new IndUniAna()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indUniAna;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndUniAna createUpdatedEntity(EntityManager em) {
        IndUniAna indUniAna = new IndUniAna()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indUniAna;
    }

    @BeforeEach
    public void initTest() {
        indUniAna = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndUniAna() throws Exception {
        int databaseSizeBeforeCreate = indUniAnaRepository.findAll().size();

        // Create the IndUniAna
        restIndUniAnaMockMvc.perform(post("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indUniAna)))
            .andExpect(status().isCreated());

        // Validate the IndUniAna in the database
        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeCreate + 1);
        IndUniAna testIndUniAna = indUniAnaList.get(indUniAnaList.size() - 1);
        assertThat(testIndUniAna.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndUniAna.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndUniAna.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndUniAna.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndUniAna.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndUniAna.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndUniAna.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndUniAna.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndUniAna.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndUniAna.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndUniAna.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndUniAna.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndUniAna.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndUniAna.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndUniAnaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indUniAnaRepository.findAll().size();

        // Create the IndUniAna with an existing ID
        indUniAna.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndUniAnaMockMvc.perform(post("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indUniAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndUniAna in the database
        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indUniAnaRepository.findAll().size();
        // set the field null
        indUniAna.setIndirizzo(null);

        // Create the IndUniAna, which fails.

        restIndUniAnaMockMvc.perform(post("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indUniAna)))
            .andExpect(status().isBadRequest());

        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indUniAnaRepository.findAll().size();
        // set the field null
        indUniAna.setNazione(null);

        // Create the IndUniAna, which fails.

        restIndUniAnaMockMvc.perform(post("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indUniAna)))
            .andExpect(status().isBadRequest());

        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndUniAnas() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indUniAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndUniAna() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get the indUniAna
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas/{id}", indUniAna.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indUniAna.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndUniAnaShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indUniAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndUniAnaShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndUniAnaShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indUniAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndUniAnaShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where indirizzo is not null
        defaultIndUniAnaShouldBeFound("indirizzo.specified=true");

        // Get all the indUniAnaList where indirizzo is null
        defaultIndUniAnaShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where cap equals to DEFAULT_CAP
        defaultIndUniAnaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indUniAnaList where cap equals to UPDATED_CAP
        defaultIndUniAnaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndUniAnaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indUniAnaList where cap equals to UPDATED_CAP
        defaultIndUniAnaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where cap is not null
        defaultIndUniAnaShouldBeFound("cap.specified=true");

        // Get all the indUniAnaList where cap is null
        defaultIndUniAnaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where nazione equals to DEFAULT_NAZIONE
        defaultIndUniAnaShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indUniAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndUniAnaShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndUniAnaShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indUniAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndUniAnaShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where nazione is not null
        defaultIndUniAnaShouldBeFound("nazione.specified=true");

        // Get all the indUniAnaList where nazione is null
        defaultIndUniAnaShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where regione equals to DEFAULT_REGIONE
        defaultIndUniAnaShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indUniAnaList where regione equals to UPDATED_REGIONE
        defaultIndUniAnaShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndUniAnaShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indUniAnaList where regione equals to UPDATED_REGIONE
        defaultIndUniAnaShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where regione is not null
        defaultIndUniAnaShouldBeFound("regione.specified=true");

        // Get all the indUniAnaList where regione is null
        defaultIndUniAnaShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where provincia equals to DEFAULT_PROVINCIA
        defaultIndUniAnaShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indUniAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndUniAnaShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndUniAnaShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indUniAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndUniAnaShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where provincia is not null
        defaultIndUniAnaShouldBeFound("provincia.specified=true");

        // Get all the indUniAnaList where provincia is null
        defaultIndUniAnaShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where comune equals to DEFAULT_COMUNE
        defaultIndUniAnaShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indUniAnaList where comune equals to UPDATED_COMUNE
        defaultIndUniAnaShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndUniAnaShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indUniAnaList where comune equals to UPDATED_COMUNE
        defaultIndUniAnaShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where comune is not null
        defaultIndUniAnaShouldBeFound("comune.specified=true");

        // Get all the indUniAnaList where comune is null
        defaultIndUniAnaShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndUniAnaShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indUniAnaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndUniAnaShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndUniAnaShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indUniAnaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndUniAnaShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where numeroCiv is not null
        defaultIndUniAnaShouldBeFound("numeroCiv.specified=true");

        // Get all the indUniAnaList where numeroCiv is null
        defaultIndUniAnaShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude equals to DEFAULT_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indUniAnaList where latitude equals to UPDATED_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indUniAnaList where latitude equals to UPDATED_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude is not null
        defaultIndUniAnaShouldBeFound("latitude.specified=true");

        // Get all the indUniAnaList where latitude is null
        defaultIndUniAnaShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indUniAnaList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indUniAnaList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude is less than DEFAULT_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indUniAnaList where latitude is less than UPDATED_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where latitude is greater than DEFAULT_LATITUDE
        defaultIndUniAnaShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indUniAnaList where latitude is greater than SMALLER_LATITUDE
        defaultIndUniAnaShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude equals to DEFAULT_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indUniAnaList where longitude equals to UPDATED_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indUniAnaList where longitude equals to UPDATED_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude is not null
        defaultIndUniAnaShouldBeFound("longitude.specified=true");

        // Get all the indUniAnaList where longitude is null
        defaultIndUniAnaShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indUniAnaList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indUniAnaList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude is less than DEFAULT_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indUniAnaList where longitude is less than UPDATED_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndUniAnaShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indUniAnaList where longitude is greater than SMALLER_LONGITUDE
        defaultIndUniAnaShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali is not null
        defaultIndUniAnaShouldBeFound("dataInizVali.specified=true");

        // Get all the indUniAnaList where dataInizVali is null
        defaultIndUniAnaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndUniAnaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indUniAnaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndUniAnaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali is not null
        defaultIndUniAnaShouldBeFound("dataFineVali.specified=true");

        // Get all the indUniAnaList where dataFineVali is null
        defaultIndUniAnaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndUniAnaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indUniAnaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndUniAnaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator is not null
        defaultIndUniAnaShouldBeFound("userIdCreator.specified=true");

        // Get all the indUniAnaList where userIdCreator is null
        defaultIndUniAnaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndUniAnaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indUniAnaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndUniAnaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod is not null
        defaultIndUniAnaShouldBeFound("userIdLastMod.specified=true");

        // Get all the indUniAnaList where userIdLastMod is null
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndUniAnasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);

        // Get all the indUniAnaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndUniAnaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indUniAnaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndUniAnaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndUniAnasByIndToUniAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        indUniAnaRepository.saveAndFlush(indUniAna);
        UniAnagrafica indToUniAna = UniAnagraficaResourceIT.createEntity(em);
        em.persist(indToUniAna);
        em.flush();
        indUniAna.setIndToUniAna(indToUniAna);
        indUniAnaRepository.saveAndFlush(indUniAna);
        Long indToUniAnaId = indToUniAna.getId();

        // Get all the indUniAnaList where indToUniAna equals to indToUniAnaId
        defaultIndUniAnaShouldBeFound("indToUniAnaId.equals=" + indToUniAnaId);

        // Get all the indUniAnaList where indToUniAna equals to indToUniAnaId + 1
        defaultIndUniAnaShouldNotBeFound("indToUniAnaId.equals=" + (indToUniAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndUniAnaShouldBeFound(String filter) throws Exception {
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indUniAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndUniAnaShouldNotBeFound(String filter) throws Exception {
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndUniAna() throws Exception {
        // Get the indUniAna
        restIndUniAnaMockMvc.perform(get("/api/ind-uni-anas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndUniAna() throws Exception {
        // Initialize the database
        indUniAnaService.save(indUniAna);

        int databaseSizeBeforeUpdate = indUniAnaRepository.findAll().size();

        // Update the indUniAna
        IndUniAna updatedIndUniAna = indUniAnaRepository.findById(indUniAna.getId()).get();
        // Disconnect from session so that the updates on updatedIndUniAna are not directly saved in db
        em.detach(updatedIndUniAna);
        updatedIndUniAna
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndUniAnaMockMvc.perform(put("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndUniAna)))
            .andExpect(status().isOk());

        // Validate the IndUniAna in the database
        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeUpdate);
        IndUniAna testIndUniAna = indUniAnaList.get(indUniAnaList.size() - 1);
        assertThat(testIndUniAna.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndUniAna.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndUniAna.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndUniAna.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndUniAna.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndUniAna.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndUniAna.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndUniAna.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndUniAna.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndUniAna.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndUniAna.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndUniAna.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndUniAna.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndUniAna.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndUniAna() throws Exception {
        int databaseSizeBeforeUpdate = indUniAnaRepository.findAll().size();

        // Create the IndUniAna

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndUniAnaMockMvc.perform(put("/api/ind-uni-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indUniAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndUniAna in the database
        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndUniAna() throws Exception {
        // Initialize the database
        indUniAnaService.save(indUniAna);

        int databaseSizeBeforeDelete = indUniAnaRepository.findAll().size();

        // Delete the indUniAna
        restIndUniAnaMockMvc.perform(delete("/api/ind-uni-anas/{id}", indUniAna.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndUniAna> indUniAnaList = indUniAnaRepository.findAll();
        assertThat(indUniAnaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndUniAna.class);
        IndUniAna indUniAna1 = new IndUniAna();
        indUniAna1.setId(1L);
        IndUniAna indUniAna2 = new IndUniAna();
        indUniAna2.setId(indUniAna1.getId());
        assertThat(indUniAna1).isEqualTo(indUniAna2);
        indUniAna2.setId(2L);
        assertThat(indUniAna1).isNotEqualTo(indUniAna2);
        indUniAna1.setId(null);
        assertThat(indUniAna1).isNotEqualTo(indUniAna2);
    }
}
