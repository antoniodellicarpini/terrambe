package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.PermessiOperatore;
import com.terrambe.domain.TipologiaPermessoOperatore;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.PermessiOperatoreRepository;
import com.terrambe.service.PermessiOperatoreService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.PermessiOperatoreCriteria;
import com.terrambe.service.PermessiOperatoreQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PermessiOperatoreResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class PermessiOperatoreResourceIT {

    private static final Boolean DEFAULT_FLAG_MODIFICA = false;
    private static final Boolean UPDATED_FLAG_MODIFICA = true;

    private static final Boolean DEFAULT_FLAG_VISUALIZZA = false;
    private static final Boolean UPDATED_FLAG_VISUALIZZA = true;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private PermessiOperatoreRepository permessiOperatoreRepository;

    @Autowired
    private PermessiOperatoreService permessiOperatoreService;

    @Autowired
    private PermessiOperatoreQueryService permessiOperatoreQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPermessiOperatoreMockMvc;

    private PermessiOperatore permessiOperatore;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PermessiOperatoreResource permessiOperatoreResource = new PermessiOperatoreResource(permessiOperatoreService, permessiOperatoreQueryService);
        this.restPermessiOperatoreMockMvc = MockMvcBuilders.standaloneSetup(permessiOperatoreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermessiOperatore createEntity(EntityManager em) {
        PermessiOperatore permessiOperatore = new PermessiOperatore()
            .flagModifica(DEFAULT_FLAG_MODIFICA)
            .flagVisualizza(DEFAULT_FLAG_VISUALIZZA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return permessiOperatore;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermessiOperatore createUpdatedEntity(EntityManager em) {
        PermessiOperatore permessiOperatore = new PermessiOperatore()
            .flagModifica(UPDATED_FLAG_MODIFICA)
            .flagVisualizza(UPDATED_FLAG_VISUALIZZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return permessiOperatore;
    }

    @BeforeEach
    public void initTest() {
        permessiOperatore = createEntity(em);
    }

    @Test
    @Transactional
    public void createPermessiOperatore() throws Exception {
        int databaseSizeBeforeCreate = permessiOperatoreRepository.findAll().size();

        // Create the PermessiOperatore
        restPermessiOperatoreMockMvc.perform(post("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permessiOperatore)))
            .andExpect(status().isCreated());

        // Validate the PermessiOperatore in the database
        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeCreate + 1);
        PermessiOperatore testPermessiOperatore = permessiOperatoreList.get(permessiOperatoreList.size() - 1);
        assertThat(testPermessiOperatore.isFlagModifica()).isEqualTo(DEFAULT_FLAG_MODIFICA);
        assertThat(testPermessiOperatore.isFlagVisualizza()).isEqualTo(DEFAULT_FLAG_VISUALIZZA);
        assertThat(testPermessiOperatore.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testPermessiOperatore.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testPermessiOperatore.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testPermessiOperatore.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testPermessiOperatore.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createPermessiOperatoreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = permessiOperatoreRepository.findAll().size();

        // Create the PermessiOperatore with an existing ID
        permessiOperatore.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPermessiOperatoreMockMvc.perform(post("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permessiOperatore)))
            .andExpect(status().isBadRequest());

        // Validate the PermessiOperatore in the database
        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFlagModificaIsRequired() throws Exception {
        int databaseSizeBeforeTest = permessiOperatoreRepository.findAll().size();
        // set the field null
        permessiOperatore.setFlagModifica(null);

        // Create the PermessiOperatore, which fails.

        restPermessiOperatoreMockMvc.perform(post("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permessiOperatore)))
            .andExpect(status().isBadRequest());

        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFlagVisualizzaIsRequired() throws Exception {
        int databaseSizeBeforeTest = permessiOperatoreRepository.findAll().size();
        // set the field null
        permessiOperatore.setFlagVisualizza(null);

        // Create the PermessiOperatore, which fails.

        restPermessiOperatoreMockMvc.perform(post("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permessiOperatore)))
            .andExpect(status().isBadRequest());

        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatores() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permessiOperatore.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagModifica").value(hasItem(DEFAULT_FLAG_MODIFICA.booleanValue())))
            .andExpect(jsonPath("$.[*].flagVisualizza").value(hasItem(DEFAULT_FLAG_VISUALIZZA.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getPermessiOperatore() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get the permessiOperatore
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores/{id}", permessiOperatore.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(permessiOperatore.getId().intValue()))
            .andExpect(jsonPath("$.flagModifica").value(DEFAULT_FLAG_MODIFICA.booleanValue()))
            .andExpect(jsonPath("$.flagVisualizza").value(DEFAULT_FLAG_VISUALIZZA.booleanValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagModificaIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagModifica equals to DEFAULT_FLAG_MODIFICA
        defaultPermessiOperatoreShouldBeFound("flagModifica.equals=" + DEFAULT_FLAG_MODIFICA);

        // Get all the permessiOperatoreList where flagModifica equals to UPDATED_FLAG_MODIFICA
        defaultPermessiOperatoreShouldNotBeFound("flagModifica.equals=" + UPDATED_FLAG_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagModificaIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagModifica in DEFAULT_FLAG_MODIFICA or UPDATED_FLAG_MODIFICA
        defaultPermessiOperatoreShouldBeFound("flagModifica.in=" + DEFAULT_FLAG_MODIFICA + "," + UPDATED_FLAG_MODIFICA);

        // Get all the permessiOperatoreList where flagModifica equals to UPDATED_FLAG_MODIFICA
        defaultPermessiOperatoreShouldNotBeFound("flagModifica.in=" + UPDATED_FLAG_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagModificaIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagModifica is not null
        defaultPermessiOperatoreShouldBeFound("flagModifica.specified=true");

        // Get all the permessiOperatoreList where flagModifica is null
        defaultPermessiOperatoreShouldNotBeFound("flagModifica.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagVisualizzaIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagVisualizza equals to DEFAULT_FLAG_VISUALIZZA
        defaultPermessiOperatoreShouldBeFound("flagVisualizza.equals=" + DEFAULT_FLAG_VISUALIZZA);

        // Get all the permessiOperatoreList where flagVisualizza equals to UPDATED_FLAG_VISUALIZZA
        defaultPermessiOperatoreShouldNotBeFound("flagVisualizza.equals=" + UPDATED_FLAG_VISUALIZZA);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagVisualizzaIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagVisualizza in DEFAULT_FLAG_VISUALIZZA or UPDATED_FLAG_VISUALIZZA
        defaultPermessiOperatoreShouldBeFound("flagVisualizza.in=" + DEFAULT_FLAG_VISUALIZZA + "," + UPDATED_FLAG_VISUALIZZA);

        // Get all the permessiOperatoreList where flagVisualizza equals to UPDATED_FLAG_VISUALIZZA
        defaultPermessiOperatoreShouldNotBeFound("flagVisualizza.in=" + UPDATED_FLAG_VISUALIZZA);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByFlagVisualizzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where flagVisualizza is not null
        defaultPermessiOperatoreShouldBeFound("flagVisualizza.specified=true");

        // Get all the permessiOperatoreList where flagVisualizza is null
        defaultPermessiOperatoreShouldNotBeFound("flagVisualizza.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali is not null
        defaultPermessiOperatoreShouldBeFound("dataInizVali.specified=true");

        // Get all the permessiOperatoreList where dataInizVali is null
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the permessiOperatoreList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultPermessiOperatoreShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali is not null
        defaultPermessiOperatoreShouldBeFound("dataFineVali.specified=true");

        // Get all the permessiOperatoreList where dataFineVali is null
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultPermessiOperatoreShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the permessiOperatoreList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultPermessiOperatoreShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator is not null
        defaultPermessiOperatoreShouldBeFound("userIdCreator.specified=true");

        // Get all the permessiOperatoreList where userIdCreator is null
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultPermessiOperatoreShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the permessiOperatoreList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultPermessiOperatoreShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod is not null
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.specified=true");

        // Get all the permessiOperatoreList where userIdLastMod is null
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllPermessiOperatoresByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);

        // Get all the permessiOperatoreList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the permessiOperatoreList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultPermessiOperatoreShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllPermessiOperatoresByPermOperToTipoPermIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);
        TipologiaPermessoOperatore permOperToTipoPerm = TipologiaPermessoOperatoreResourceIT.createEntity(em);
        em.persist(permOperToTipoPerm);
        em.flush();
        permessiOperatore.setPermOperToTipoPerm(permOperToTipoPerm);
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);
        Long permOperToTipoPermId = permOperToTipoPerm.getId();

        // Get all the permessiOperatoreList where permOperToTipoPerm equals to permOperToTipoPermId
        defaultPermessiOperatoreShouldBeFound("permOperToTipoPermId.equals=" + permOperToTipoPermId);

        // Get all the permessiOperatoreList where permOperToTipoPerm equals to permOperToTipoPermId + 1
        defaultPermessiOperatoreShouldNotBeFound("permOperToTipoPermId.equals=" + (permOperToTipoPermId + 1));
    }


    @Test
    @Transactional
    public void getAllPermessiOperatoresByPerOperToOpeAnagIsEqualToSomething() throws Exception {
        // Initialize the database
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);
        OpeAnagrafica perOperToOpeAnag = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(perOperToOpeAnag);
        em.flush();
        permessiOperatore.setPerOperToOpeAnag(perOperToOpeAnag);
        permessiOperatoreRepository.saveAndFlush(permessiOperatore);
        Long perOperToOpeAnagId = perOperToOpeAnag.getId();

        // Get all the permessiOperatoreList where perOperToOpeAnag equals to perOperToOpeAnagId
        defaultPermessiOperatoreShouldBeFound("perOperToOpeAnagId.equals=" + perOperToOpeAnagId);

        // Get all the permessiOperatoreList where perOperToOpeAnag equals to perOperToOpeAnagId + 1
        defaultPermessiOperatoreShouldNotBeFound("perOperToOpeAnagId.equals=" + (perOperToOpeAnagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPermessiOperatoreShouldBeFound(String filter) throws Exception {
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permessiOperatore.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagModifica").value(hasItem(DEFAULT_FLAG_MODIFICA.booleanValue())))
            .andExpect(jsonPath("$.[*].flagVisualizza").value(hasItem(DEFAULT_FLAG_VISUALIZZA.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPermessiOperatoreShouldNotBeFound(String filter) throws Exception {
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPermessiOperatore() throws Exception {
        // Get the permessiOperatore
        restPermessiOperatoreMockMvc.perform(get("/api/permessi-operatores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePermessiOperatore() throws Exception {
        // Initialize the database
        permessiOperatoreService.save(permessiOperatore);

        int databaseSizeBeforeUpdate = permessiOperatoreRepository.findAll().size();

        // Update the permessiOperatore
        PermessiOperatore updatedPermessiOperatore = permessiOperatoreRepository.findById(permessiOperatore.getId()).get();
        // Disconnect from session so that the updates on updatedPermessiOperatore are not directly saved in db
        em.detach(updatedPermessiOperatore);
        updatedPermessiOperatore
            .flagModifica(UPDATED_FLAG_MODIFICA)
            .flagVisualizza(UPDATED_FLAG_VISUALIZZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restPermessiOperatoreMockMvc.perform(put("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPermessiOperatore)))
            .andExpect(status().isOk());

        // Validate the PermessiOperatore in the database
        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeUpdate);
        PermessiOperatore testPermessiOperatore = permessiOperatoreList.get(permessiOperatoreList.size() - 1);
        assertThat(testPermessiOperatore.isFlagModifica()).isEqualTo(UPDATED_FLAG_MODIFICA);
        assertThat(testPermessiOperatore.isFlagVisualizza()).isEqualTo(UPDATED_FLAG_VISUALIZZA);
        assertThat(testPermessiOperatore.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testPermessiOperatore.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testPermessiOperatore.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testPermessiOperatore.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testPermessiOperatore.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingPermessiOperatore() throws Exception {
        int databaseSizeBeforeUpdate = permessiOperatoreRepository.findAll().size();

        // Create the PermessiOperatore

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermessiOperatoreMockMvc.perform(put("/api/permessi-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permessiOperatore)))
            .andExpect(status().isBadRequest());

        // Validate the PermessiOperatore in the database
        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePermessiOperatore() throws Exception {
        // Initialize the database
        permessiOperatoreService.save(permessiOperatore);

        int databaseSizeBeforeDelete = permessiOperatoreRepository.findAll().size();

        // Delete the permessiOperatore
        restPermessiOperatoreMockMvc.perform(delete("/api/permessi-operatores/{id}", permessiOperatore.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PermessiOperatore> permessiOperatoreList = permessiOperatoreRepository.findAll();
        assertThat(permessiOperatoreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PermessiOperatore.class);
        PermessiOperatore permessiOperatore1 = new PermessiOperatore();
        permessiOperatore1.setId(1L);
        PermessiOperatore permessiOperatore2 = new PermessiOperatore();
        permessiOperatore2.setId(permessiOperatore1.getId());
        assertThat(permessiOperatore1).isEqualTo(permessiOperatore2);
        permessiOperatore2.setId(2L);
        assertThat(permessiOperatore1).isNotEqualTo(permessiOperatore2);
        permessiOperatore1.setId(null);
        assertThat(permessiOperatore1).isNotEqualTo(permessiOperatore2);
    }
}
