package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcConcProd;
import com.terrambe.domain.RegOpcConc;
import com.terrambe.repository.RegOpcConcProdRepository;
import com.terrambe.service.RegOpcConcProdService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcConcProdCriteria;
import com.terrambe.service.RegOpcConcProdQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcConcProdResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcConcProdResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Long DEFAULT_ID_CONCIME = 1L;
    private static final Long UPDATED_ID_CONCIME = 2L;
    private static final Long SMALLER_ID_CONCIME = 1L - 1L;

    private static final Float DEFAULT_QUANTITA = 1F;
    private static final Float UPDATED_QUANTITA = 2F;
    private static final Float SMALLER_QUANTITA = 1F - 1F;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcConcProdRepository regOpcConcProdRepository;

    @Autowired
    private RegOpcConcProdService regOpcConcProdService;

    @Autowired
    private RegOpcConcProdQueryService regOpcConcProdQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcConcProdMockMvc;

    private RegOpcConcProd regOpcConcProd;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcConcProdResource regOpcConcProdResource = new RegOpcConcProdResource(regOpcConcProdService, regOpcConcProdQueryService);
        this.restRegOpcConcProdMockMvc = MockMvcBuilders.standaloneSetup(regOpcConcProdResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConcProd createEntity(EntityManager em) {
        RegOpcConcProd regOpcConcProd = new RegOpcConcProd()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .idConcime(DEFAULT_ID_CONCIME)
            .quantita(DEFAULT_QUANTITA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcConcProd;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConcProd createUpdatedEntity(EntityManager em) {
        RegOpcConcProd regOpcConcProd = new RegOpcConcProd()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .idConcime(UPDATED_ID_CONCIME)
            .quantita(UPDATED_QUANTITA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcConcProd;
    }

    @BeforeEach
    public void initTest() {
        regOpcConcProd = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcConcProd() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcProdRepository.findAll().size();

        // Create the RegOpcConcProd
        restRegOpcConcProdMockMvc.perform(post("/api/reg-opc-conc-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcProd)))
            .andExpect(status().isCreated());

        // Validate the RegOpcConcProd in the database
        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcConcProd testRegOpcConcProd = regOpcConcProdList.get(regOpcConcProdList.size() - 1);
        assertThat(testRegOpcConcProd.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcConcProd.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcConcProd.getIdConcime()).isEqualTo(DEFAULT_ID_CONCIME);
        assertThat(testRegOpcConcProd.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegOpcConcProd.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcConcProd.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcConcProd.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcConcProd.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcConcProd.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcConcProdWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcProdRepository.findAll().size();

        // Create the RegOpcConcProd with an existing ID
        regOpcConcProd.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcConcProdMockMvc.perform(post("/api/reg-opc-conc-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcProd)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConcProd in the database
        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcConcProdRepository.findAll().size();
        // set the field null
        regOpcConcProd.setIdAzienda(null);

        // Create the RegOpcConcProd, which fails.

        restRegOpcConcProdMockMvc.perform(post("/api/reg-opc-conc-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcProd)))
            .andExpect(status().isBadRequest());

        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProds() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConcProd.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].idConcime").value(hasItem(DEFAULT_ID_CONCIME.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcConcProd() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get the regOpcConcProd
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods/{id}", regOpcConcProd.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcConcProd.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.idConcime").value(DEFAULT_ID_CONCIME.intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda is not null
        defaultRegOpcConcProdShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcConcProdList where idAzienda is null
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcConcProdShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcProdList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcConcProdShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd is not null
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcConcProdList where idUnitaProd is null
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcProdShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcProdList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcConcProdShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime equals to DEFAULT_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.equals=" + DEFAULT_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime equals to UPDATED_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.equals=" + UPDATED_ID_CONCIME);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime in DEFAULT_ID_CONCIME or UPDATED_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.in=" + DEFAULT_ID_CONCIME + "," + UPDATED_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime equals to UPDATED_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.in=" + UPDATED_ID_CONCIME);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime is not null
        defaultRegOpcConcProdShouldBeFound("idConcime.specified=true");

        // Get all the regOpcConcProdList where idConcime is null
        defaultRegOpcConcProdShouldNotBeFound("idConcime.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime is greater than or equal to DEFAULT_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.greaterThanOrEqual=" + DEFAULT_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime is greater than or equal to UPDATED_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.greaterThanOrEqual=" + UPDATED_ID_CONCIME);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime is less than or equal to DEFAULT_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.lessThanOrEqual=" + DEFAULT_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime is less than or equal to SMALLER_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.lessThanOrEqual=" + SMALLER_ID_CONCIME);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime is less than DEFAULT_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.lessThan=" + DEFAULT_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime is less than UPDATED_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.lessThan=" + UPDATED_ID_CONCIME);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByIdConcimeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where idConcime is greater than DEFAULT_ID_CONCIME
        defaultRegOpcConcProdShouldNotBeFound("idConcime.greaterThan=" + DEFAULT_ID_CONCIME);

        // Get all the regOpcConcProdList where idConcime is greater than SMALLER_ID_CONCIME
        defaultRegOpcConcProdShouldBeFound("idConcime.greaterThan=" + SMALLER_ID_CONCIME);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita equals to DEFAULT_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regOpcConcProdList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regOpcConcProdList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita is not null
        defaultRegOpcConcProdShouldBeFound("quantita.specified=true");

        // Get all the regOpcConcProdList where quantita is null
        defaultRegOpcConcProdShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcConcProdList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcConcProdList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita is less than DEFAULT_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcConcProdList where quantita is less than UPDATED_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where quantita is greater than DEFAULT_QUANTITA
        defaultRegOpcConcProdShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcConcProdList where quantita is greater than SMALLER_QUANTITA
        defaultRegOpcConcProdShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali is not null
        defaultRegOpcConcProdShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcConcProdList where dataInizVali is null
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcProdList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcProdShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali is not null
        defaultRegOpcConcProdShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcConcProdList where dataFineVali is null
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcProdShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcProdList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcConcProdShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator is not null
        defaultRegOpcConcProdShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcConcProdList where userIdCreator is null
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcProdShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcProdList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcConcProdShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod is not null
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcConcProdList where userIdLastMod is null
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcProdsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);

        // Get all the regOpcConcProdList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcProdList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcProdShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcProdsByProdToOpcConcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);
        RegOpcConc prodToOpcConc = RegOpcConcResourceIT.createEntity(em);
        em.persist(prodToOpcConc);
        em.flush();
        regOpcConcProd.setProdToOpcConc(prodToOpcConc);
        regOpcConcProdRepository.saveAndFlush(regOpcConcProd);
        Long prodToOpcConcId = prodToOpcConc.getId();

        // Get all the regOpcConcProdList where prodToOpcConc equals to prodToOpcConcId
        defaultRegOpcConcProdShouldBeFound("prodToOpcConcId.equals=" + prodToOpcConcId);

        // Get all the regOpcConcProdList where prodToOpcConc equals to prodToOpcConcId + 1
        defaultRegOpcConcProdShouldNotBeFound("prodToOpcConcId.equals=" + (prodToOpcConcId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcConcProdShouldBeFound(String filter) throws Exception {
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConcProd.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].idConcime").value(hasItem(DEFAULT_ID_CONCIME.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcConcProdShouldNotBeFound(String filter) throws Exception {
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcConcProd() throws Exception {
        // Get the regOpcConcProd
        restRegOpcConcProdMockMvc.perform(get("/api/reg-opc-conc-prods/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcConcProd() throws Exception {
        // Initialize the database
        regOpcConcProdService.save(regOpcConcProd);

        int databaseSizeBeforeUpdate = regOpcConcProdRepository.findAll().size();

        // Update the regOpcConcProd
        RegOpcConcProd updatedRegOpcConcProd = regOpcConcProdRepository.findById(regOpcConcProd.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcConcProd are not directly saved in db
        em.detach(updatedRegOpcConcProd);
        updatedRegOpcConcProd
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .idConcime(UPDATED_ID_CONCIME)
            .quantita(UPDATED_QUANTITA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcConcProdMockMvc.perform(put("/api/reg-opc-conc-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcConcProd)))
            .andExpect(status().isOk());

        // Validate the RegOpcConcProd in the database
        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeUpdate);
        RegOpcConcProd testRegOpcConcProd = regOpcConcProdList.get(regOpcConcProdList.size() - 1);
        assertThat(testRegOpcConcProd.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcConcProd.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcConcProd.getIdConcime()).isEqualTo(UPDATED_ID_CONCIME);
        assertThat(testRegOpcConcProd.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegOpcConcProd.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcConcProd.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcConcProd.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcConcProd.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcConcProd.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcConcProd() throws Exception {
        int databaseSizeBeforeUpdate = regOpcConcProdRepository.findAll().size();

        // Create the RegOpcConcProd

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcConcProdMockMvc.perform(put("/api/reg-opc-conc-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcProd)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConcProd in the database
        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcConcProd() throws Exception {
        // Initialize the database
        regOpcConcProdService.save(regOpcConcProd);

        int databaseSizeBeforeDelete = regOpcConcProdRepository.findAll().size();

        // Delete the regOpcConcProd
        restRegOpcConcProdMockMvc.perform(delete("/api/reg-opc-conc-prods/{id}", regOpcConcProd.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcConcProd> regOpcConcProdList = regOpcConcProdRepository.findAll();
        assertThat(regOpcConcProdList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcConcProd.class);
        RegOpcConcProd regOpcConcProd1 = new RegOpcConcProd();
        regOpcConcProd1.setId(1L);
        RegOpcConcProd regOpcConcProd2 = new RegOpcConcProd();
        regOpcConcProd2.setId(regOpcConcProd1.getId());
        assertThat(regOpcConcProd1).isEqualTo(regOpcConcProd2);
        regOpcConcProd2.setId(2L);
        assertThat(regOpcConcProd1).isNotEqualTo(regOpcConcProd2);
        regOpcConcProd1.setId(null);
        assertThat(regOpcConcProd1).isNotEqualTo(regOpcConcProd2);
    }
}
