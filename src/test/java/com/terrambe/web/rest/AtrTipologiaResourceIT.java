package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AtrTipologia;
import com.terrambe.domain.AtrMarchio;
import com.terrambe.repository.AtrTipologiaRepository;
import com.terrambe.service.AtrTipologiaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AtrTipologiaCriteria;
import com.terrambe.service.AtrTipologiaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AtrTipologiaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AtrTipologiaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AtrTipologiaRepository atrTipologiaRepository;

    @Autowired
    private AtrTipologiaService atrTipologiaService;

    @Autowired
    private AtrTipologiaQueryService atrTipologiaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAtrTipologiaMockMvc;

    private AtrTipologia atrTipologia;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtrTipologiaResource atrTipologiaResource = new AtrTipologiaResource(atrTipologiaService, atrTipologiaQueryService);
        this.restAtrTipologiaMockMvc = MockMvcBuilders.standaloneSetup(atrTipologiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrTipologia createEntity(EntityManager em) {
        AtrTipologia atrTipologia = new AtrTipologia()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return atrTipologia;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrTipologia createUpdatedEntity(EntityManager em) {
        AtrTipologia atrTipologia = new AtrTipologia()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return atrTipologia;
    }

    @BeforeEach
    public void initTest() {
        atrTipologia = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtrTipologia() throws Exception {
        int databaseSizeBeforeCreate = atrTipologiaRepository.findAll().size();

        // Create the AtrTipologia
        restAtrTipologiaMockMvc.perform(post("/api/atr-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrTipologia)))
            .andExpect(status().isCreated());

        // Validate the AtrTipologia in the database
        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeCreate + 1);
        AtrTipologia testAtrTipologia = atrTipologiaList.get(atrTipologiaList.size() - 1);
        assertThat(testAtrTipologia.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testAtrTipologia.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAtrTipologia.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAtrTipologia.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAtrTipologia.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAtrTipologia.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAtrTipologiaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atrTipologiaRepository.findAll().size();

        // Create the AtrTipologia with an existing ID
        atrTipologia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtrTipologiaMockMvc.perform(post("/api/atr-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the AtrTipologia in the database
        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = atrTipologiaRepository.findAll().size();
        // set the field null
        atrTipologia.setDescrizione(null);

        // Create the AtrTipologia, which fails.

        restAtrTipologiaMockMvc.perform(post("/api/atr-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrTipologia)))
            .andExpect(status().isBadRequest());

        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAtrTipologias() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAtrTipologia() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get the atrTipologia
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias/{id}", atrTipologia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(atrTipologia.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultAtrTipologiaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the atrTipologiaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAtrTipologiaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultAtrTipologiaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the atrTipologiaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAtrTipologiaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where descrizione is not null
        defaultAtrTipologiaShouldBeFound("descrizione.specified=true");

        // Get all the atrTipologiaList where descrizione is null
        defaultAtrTipologiaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali is not null
        defaultAtrTipologiaShouldBeFound("dataInizVali.specified=true");

        // Get all the atrTipologiaList where dataInizVali is null
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAtrTipologiaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrTipologiaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAtrTipologiaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali is not null
        defaultAtrTipologiaShouldBeFound("dataFineVali.specified=true");

        // Get all the atrTipologiaList where dataFineVali is null
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAtrTipologiaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrTipologiaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAtrTipologiaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator is not null
        defaultAtrTipologiaShouldBeFound("userIdCreator.specified=true");

        // Get all the atrTipologiaList where userIdCreator is null
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAtrTipologiaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrTipologiaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAtrTipologiaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod is not null
        defaultAtrTipologiaShouldBeFound("userIdLastMod.specified=true");

        // Get all the atrTipologiaList where userIdLastMod is null
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrTipologiasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);

        // Get all the atrTipologiaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrTipologiaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAtrTipologiaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAtrTipologiasByAtrMarchioIsEqualToSomething() throws Exception {
        // Initialize the database
        atrTipologiaRepository.saveAndFlush(atrTipologia);
        AtrMarchio atrMarchio = AtrMarchioResourceIT.createEntity(em);
        em.persist(atrMarchio);
        em.flush();
        atrTipologia.addAtrMarchio(atrMarchio);
        atrTipologiaRepository.saveAndFlush(atrTipologia);
        Long atrMarchioId = atrMarchio.getId();

        // Get all the atrTipologiaList where atrMarchio equals to atrMarchioId
        defaultAtrTipologiaShouldBeFound("atrMarchioId.equals=" + atrMarchioId);

        // Get all the atrTipologiaList where atrMarchio equals to atrMarchioId + 1
        defaultAtrTipologiaShouldNotBeFound("atrMarchioId.equals=" + (atrMarchioId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAtrTipologiaShouldBeFound(String filter) throws Exception {
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAtrTipologiaShouldNotBeFound(String filter) throws Exception {
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAtrTipologia() throws Exception {
        // Get the atrTipologia
        restAtrTipologiaMockMvc.perform(get("/api/atr-tipologias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtrTipologia() throws Exception {
        // Initialize the database
        atrTipologiaService.save(atrTipologia);

        int databaseSizeBeforeUpdate = atrTipologiaRepository.findAll().size();

        // Update the atrTipologia
        AtrTipologia updatedAtrTipologia = atrTipologiaRepository.findById(atrTipologia.getId()).get();
        // Disconnect from session so that the updates on updatedAtrTipologia are not directly saved in db
        em.detach(updatedAtrTipologia);
        updatedAtrTipologia
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAtrTipologiaMockMvc.perform(put("/api/atr-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAtrTipologia)))
            .andExpect(status().isOk());

        // Validate the AtrTipologia in the database
        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeUpdate);
        AtrTipologia testAtrTipologia = atrTipologiaList.get(atrTipologiaList.size() - 1);
        assertThat(testAtrTipologia.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testAtrTipologia.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAtrTipologia.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAtrTipologia.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAtrTipologia.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAtrTipologia.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAtrTipologia() throws Exception {
        int databaseSizeBeforeUpdate = atrTipologiaRepository.findAll().size();

        // Create the AtrTipologia

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtrTipologiaMockMvc.perform(put("/api/atr-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the AtrTipologia in the database
        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAtrTipologia() throws Exception {
        // Initialize the database
        atrTipologiaService.save(atrTipologia);

        int databaseSizeBeforeDelete = atrTipologiaRepository.findAll().size();

        // Delete the atrTipologia
        restAtrTipologiaMockMvc.perform(delete("/api/atr-tipologias/{id}", atrTipologia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AtrTipologia> atrTipologiaList = atrTipologiaRepository.findAll();
        assertThat(atrTipologiaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtrTipologia.class);
        AtrTipologia atrTipologia1 = new AtrTipologia();
        atrTipologia1.setId(1L);
        AtrTipologia atrTipologia2 = new AtrTipologia();
        atrTipologia2.setId(atrTipologia1.getId());
        assertThat(atrTipologia1).isEqualTo(atrTipologia2);
        atrTipologia2.setId(2L);
        assertThat(atrTipologia1).isNotEqualTo(atrTipologia2);
        atrTipologia1.setId(null);
        assertThat(atrTipologia1).isNotEqualTo(atrTipologia2);
    }
}
