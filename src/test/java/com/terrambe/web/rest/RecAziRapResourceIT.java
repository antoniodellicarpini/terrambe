package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecAziRap;
import com.terrambe.domain.AziRappresentante;
import com.terrambe.repository.RecAziRapRepository;
import com.terrambe.service.RecAziRapService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecAziRapCriteria;
import com.terrambe.service.RecAziRapQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecAziRapResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecAziRapResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecAziRapRepository recAziRapRepository;

    @Autowired
    private RecAziRapService recAziRapService;

    @Autowired
    private RecAziRapQueryService recAziRapQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecAziRapMockMvc;

    private RecAziRap recAziRap;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecAziRapResource recAziRapResource = new RecAziRapResource(recAziRapService, recAziRapQueryService);
        this.restRecAziRapMockMvc = MockMvcBuilders.standaloneSetup(recAziRapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziRap createEntity(EntityManager em) {
        RecAziRap recAziRap = new RecAziRap()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recAziRap;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziRap createUpdatedEntity(EntityManager em) {
        RecAziRap recAziRap = new RecAziRap()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recAziRap;
    }

    @BeforeEach
    public void initTest() {
        recAziRap = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecAziRap() throws Exception {
        int databaseSizeBeforeCreate = recAziRapRepository.findAll().size();

        // Create the RecAziRap
        restRecAziRapMockMvc.perform(post("/api/rec-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRap)))
            .andExpect(status().isCreated());

        // Validate the RecAziRap in the database
        List<RecAziRap> recAziRapList = recAziRapRepository.findAll();
        assertThat(recAziRapList).hasSize(databaseSizeBeforeCreate + 1);
        RecAziRap testRecAziRap = recAziRapList.get(recAziRapList.size() - 1);
        assertThat(testRecAziRap.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecAziRap.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecAziRap.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecAziRap.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecAziRap.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecAziRap.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecAziRap.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecAziRap.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecAziRap.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecAziRap.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecAziRap.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecAziRapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recAziRapRepository.findAll().size();

        // Create the RecAziRap with an existing ID
        recAziRap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecAziRapMockMvc.perform(post("/api/rec-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRap)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziRap in the database
        List<RecAziRap> recAziRapList = recAziRapRepository.findAll();
        assertThat(recAziRapList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecAziRaps() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecAziRap() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get the recAziRap
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps/{id}", recAziRap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recAziRap.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where email equals to DEFAULT_EMAIL
        defaultRecAziRapShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recAziRapList where email equals to UPDATED_EMAIL
        defaultRecAziRapShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecAziRapShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recAziRapList where email equals to UPDATED_EMAIL
        defaultRecAziRapShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where email is not null
        defaultRecAziRapShouldBeFound("email.specified=true");

        // Get all the recAziRapList where email is null
        defaultRecAziRapShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where pec equals to DEFAULT_PEC
        defaultRecAziRapShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recAziRapList where pec equals to UPDATED_PEC
        defaultRecAziRapShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecAziRapShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recAziRapList where pec equals to UPDATED_PEC
        defaultRecAziRapShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where pec is not null
        defaultRecAziRapShouldBeFound("pec.specified=true");

        // Get all the recAziRapList where pec is null
        defaultRecAziRapShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where telefono equals to DEFAULT_TELEFONO
        defaultRecAziRapShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recAziRapList where telefono equals to UPDATED_TELEFONO
        defaultRecAziRapShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecAziRapShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recAziRapList where telefono equals to UPDATED_TELEFONO
        defaultRecAziRapShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where telefono is not null
        defaultRecAziRapShouldBeFound("telefono.specified=true");

        // Get all the recAziRapList where telefono is null
        defaultRecAziRapShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where fax equals to DEFAULT_FAX
        defaultRecAziRapShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recAziRapList where fax equals to UPDATED_FAX
        defaultRecAziRapShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecAziRapShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recAziRapList where fax equals to UPDATED_FAX
        defaultRecAziRapShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where fax is not null
        defaultRecAziRapShouldBeFound("fax.specified=true");

        // Get all the recAziRapList where fax is null
        defaultRecAziRapShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell equals to DEFAULT_CELL
        defaultRecAziRapShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recAziRapList where cell equals to UPDATED_CELL
        defaultRecAziRapShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecAziRapShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recAziRapList where cell equals to UPDATED_CELL
        defaultRecAziRapShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell is not null
        defaultRecAziRapShouldBeFound("cell.specified=true");

        // Get all the recAziRapList where cell is null
        defaultRecAziRapShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell2 equals to DEFAULT_CELL_2
        defaultRecAziRapShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recAziRapList where cell2 equals to UPDATED_CELL_2
        defaultRecAziRapShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecAziRapShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recAziRapList where cell2 equals to UPDATED_CELL_2
        defaultRecAziRapShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where cell2 is not null
        defaultRecAziRapShouldBeFound("cell2.specified=true");

        // Get all the recAziRapList where cell2 is null
        defaultRecAziRapShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali is not null
        defaultRecAziRapShouldBeFound("dataInizVali.specified=true");

        // Get all the recAziRapList where dataInizVali is null
        defaultRecAziRapShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecAziRapShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali is not null
        defaultRecAziRapShouldBeFound("dataFineVali.specified=true");

        // Get all the recAziRapList where dataFineVali is null
        defaultRecAziRapShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecAziRapShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecAziRapShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator is not null
        defaultRecAziRapShouldBeFound("userIdCreator.specified=true");

        // Get all the recAziRapList where userIdCreator is null
        defaultRecAziRapShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecAziRapShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecAziRapShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod is not null
        defaultRecAziRapShouldBeFound("userIdLastMod.specified=true");

        // Get all the recAziRapList where userIdLastMod is null
        defaultRecAziRapShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);

        // Get all the recAziRapList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecAziRapShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecAziRapsByRecToAziRapIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapRepository.saveAndFlush(recAziRap);
        AziRappresentante recToAziRap = AziRappresentanteResourceIT.createEntity(em);
        em.persist(recToAziRap);
        em.flush();
        recAziRap.setRecToAziRap(recToAziRap);
        recAziRapRepository.saveAndFlush(recAziRap);
        Long recToAziRapId = recToAziRap.getId();

        // Get all the recAziRapList where recToAziRap equals to recToAziRapId
        defaultRecAziRapShouldBeFound("recToAziRapId.equals=" + recToAziRapId);

        // Get all the recAziRapList where recToAziRap equals to recToAziRapId + 1
        defaultRecAziRapShouldNotBeFound("recToAziRapId.equals=" + (recToAziRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecAziRapShouldBeFound(String filter) throws Exception {
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecAziRapShouldNotBeFound(String filter) throws Exception {
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecAziRap() throws Exception {
        // Get the recAziRap
        restRecAziRapMockMvc.perform(get("/api/rec-azi-raps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecAziRap() throws Exception {
        // Initialize the database
        recAziRapService.save(recAziRap);

        int databaseSizeBeforeUpdate = recAziRapRepository.findAll().size();

        // Update the recAziRap
        RecAziRap updatedRecAziRap = recAziRapRepository.findById(recAziRap.getId()).get();
        // Disconnect from session so that the updates on updatedRecAziRap are not directly saved in db
        em.detach(updatedRecAziRap);
        updatedRecAziRap
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecAziRapMockMvc.perform(put("/api/rec-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecAziRap)))
            .andExpect(status().isOk());

        // Validate the RecAziRap in the database
        List<RecAziRap> recAziRapList = recAziRapRepository.findAll();
        assertThat(recAziRapList).hasSize(databaseSizeBeforeUpdate);
        RecAziRap testRecAziRap = recAziRapList.get(recAziRapList.size() - 1);
        assertThat(testRecAziRap.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecAziRap.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecAziRap.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecAziRap.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecAziRap.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecAziRap.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecAziRap.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecAziRap.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecAziRap.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecAziRap.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecAziRap.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecAziRap() throws Exception {
        int databaseSizeBeforeUpdate = recAziRapRepository.findAll().size();

        // Create the RecAziRap

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecAziRapMockMvc.perform(put("/api/rec-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRap)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziRap in the database
        List<RecAziRap> recAziRapList = recAziRapRepository.findAll();
        assertThat(recAziRapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecAziRap() throws Exception {
        // Initialize the database
        recAziRapService.save(recAziRap);

        int databaseSizeBeforeDelete = recAziRapRepository.findAll().size();

        // Delete the recAziRap
        restRecAziRapMockMvc.perform(delete("/api/rec-azi-raps/{id}", recAziRap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecAziRap> recAziRapList = recAziRapRepository.findAll();
        assertThat(recAziRapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecAziRap.class);
        RecAziRap recAziRap1 = new RecAziRap();
        recAziRap1.setId(1L);
        RecAziRap recAziRap2 = new RecAziRap();
        recAziRap2.setId(recAziRap1.getId());
        assertThat(recAziRap1).isEqualTo(recAziRap2);
        recAziRap2.setId(2L);
        assertThat(recAziRap1).isNotEqualTo(recAziRap2);
        recAziRap1.setId(null);
        assertThat(recAziRap1).isNotEqualTo(recAziRap2);
    }
}
