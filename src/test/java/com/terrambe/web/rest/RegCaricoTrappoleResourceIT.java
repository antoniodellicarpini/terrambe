package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.domain.Trappole;
import com.terrambe.domain.Fornitori;
import com.terrambe.domain.TipoRegMag;
import com.terrambe.repository.RegCaricoTrappoleRepository;
import com.terrambe.service.RegCaricoTrappoleService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegCaricoTrappoleCriteria;
import com.terrambe.service.RegCaricoTrappoleQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegCaricoTrappoleResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegCaricoTrappoleResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Integer DEFAULT_QUANTITA = 1;
    private static final Integer UPDATED_QUANTITA = 2;
    private static final Integer SMALLER_QUANTITA = 1 - 1;

    private static final Float DEFAULT_PREZZO_UNITARIO = 1F;
    private static final Float UPDATED_PREZZO_UNITARIO = 2F;
    private static final Float SMALLER_PREZZO_UNITARIO = 1F - 1F;

    private static final LocalDate DEFAULT_DATA_CARICO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_CARICO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_CARICO = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_DDT = "AAAAAAAAAA";
    private static final String UPDATED_DDT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_SCADENZA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_SCADENZA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_SCADENZA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegCaricoTrappoleRepository regCaricoTrappoleRepository;

    @Autowired
    private RegCaricoTrappoleService regCaricoTrappoleService;

    @Autowired
    private RegCaricoTrappoleQueryService regCaricoTrappoleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegCaricoTrappoleMockMvc;

    private RegCaricoTrappole regCaricoTrappole;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegCaricoTrappoleResource regCaricoTrappoleResource = new RegCaricoTrappoleResource(regCaricoTrappoleService, regCaricoTrappoleQueryService);
        this.restRegCaricoTrappoleMockMvc = MockMvcBuilders.standaloneSetup(regCaricoTrappoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoTrappole createEntity(EntityManager em) {
        RegCaricoTrappole regCaricoTrappole = new RegCaricoTrappole()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .quantita(DEFAULT_QUANTITA)
            .prezzoUnitario(DEFAULT_PREZZO_UNITARIO)
            .dataCarico(DEFAULT_DATA_CARICO)
            .ddt(DEFAULT_DDT)
            .dataScadenza(DEFAULT_DATA_SCADENZA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regCaricoTrappole;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoTrappole createUpdatedEntity(EntityManager em) {
        RegCaricoTrappole regCaricoTrappole = new RegCaricoTrappole()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regCaricoTrappole;
    }

    @BeforeEach
    public void initTest() {
        regCaricoTrappole = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegCaricoTrappole() throws Exception {
        int databaseSizeBeforeCreate = regCaricoTrappoleRepository.findAll().size();

        // Create the RegCaricoTrappole
        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isCreated());

        // Validate the RegCaricoTrappole in the database
        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeCreate + 1);
        RegCaricoTrappole testRegCaricoTrappole = regCaricoTrappoleList.get(regCaricoTrappoleList.size() - 1);
        assertThat(testRegCaricoTrappole.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegCaricoTrappole.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegCaricoTrappole.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegCaricoTrappole.getPrezzoUnitario()).isEqualTo(DEFAULT_PREZZO_UNITARIO);
        assertThat(testRegCaricoTrappole.getDataCarico()).isEqualTo(DEFAULT_DATA_CARICO);
        assertThat(testRegCaricoTrappole.getDdt()).isEqualTo(DEFAULT_DDT);
        assertThat(testRegCaricoTrappole.getDataScadenza()).isEqualTo(DEFAULT_DATA_SCADENZA);
        assertThat(testRegCaricoTrappole.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegCaricoTrappole.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegCaricoTrappole.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegCaricoTrappole.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegCaricoTrappole.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegCaricoTrappoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regCaricoTrappoleRepository.findAll().size();

        // Create the RegCaricoTrappole with an existing ID
        regCaricoTrappole.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoTrappole in the database
        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setIdAzienda(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQuantitaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setQuantita(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrezzoUnitarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setPrezzoUnitario(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataCaricoIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setDataCarico(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDdtIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setDdt(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataScadenzaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoTrappoleRepository.findAll().size();
        // set the field null
        regCaricoTrappole.setDataScadenza(null);

        // Create the RegCaricoTrappole, which fails.

        restRegCaricoTrappoleMockMvc.perform(post("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappoles() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoTrappole.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA)))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT.toString())))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegCaricoTrappole() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get the regCaricoTrappole
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles/{id}", regCaricoTrappole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regCaricoTrappole.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA))
            .andExpect(jsonPath("$.prezzoUnitario").value(DEFAULT_PREZZO_UNITARIO.doubleValue()))
            .andExpect(jsonPath("$.dataCarico").value(DEFAULT_DATA_CARICO.toString()))
            .andExpect(jsonPath("$.ddt").value(DEFAULT_DDT.toString()))
            .andExpect(jsonPath("$.dataScadenza").value(DEFAULT_DATA_SCADENZA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda is not null
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.specified=true");

        // Get all the regCaricoTrappoleList where idAzienda is null
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegCaricoTrappoleShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoTrappoleList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegCaricoTrappoleShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd is not null
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.specified=true");

        // Get all the regCaricoTrappoleList where idUnitaProd is null
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoTrappoleList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegCaricoTrappoleShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita equals to DEFAULT_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita is not null
        defaultRegCaricoTrappoleShouldBeFound("quantita.specified=true");

        // Get all the regCaricoTrappoleList where quantita is null
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita is less than DEFAULT_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita is less than UPDATED_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where quantita is greater than DEFAULT_QUANTITA
        defaultRegCaricoTrappoleShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoTrappoleList where quantita is greater than SMALLER_QUANTITA
        defaultRegCaricoTrappoleShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario equals to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.equals=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.equals=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario in DEFAULT_PREZZO_UNITARIO or UPDATED_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.in=" + DEFAULT_PREZZO_UNITARIO + "," + UPDATED_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.in=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario is not null
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.specified=true");

        // Get all the regCaricoTrappoleList where prezzoUnitario is null
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario is greater than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.greaterThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario is greater than or equal to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.greaterThanOrEqual=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario is less than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.lessThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario is less than or equal to SMALLER_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.lessThanOrEqual=" + SMALLER_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario is less than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.lessThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario is less than UPDATED_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.lessThan=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByPrezzoUnitarioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where prezzoUnitario is greater than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldNotBeFound("prezzoUnitario.greaterThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoTrappoleList where prezzoUnitario is greater than SMALLER_PREZZO_UNITARIO
        defaultRegCaricoTrappoleShouldBeFound("prezzoUnitario.greaterThan=" + SMALLER_PREZZO_UNITARIO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico equals to DEFAULT_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.equals=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.equals=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico in DEFAULT_DATA_CARICO or UPDATED_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.in=" + DEFAULT_DATA_CARICO + "," + UPDATED_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.in=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico is not null
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.specified=true");

        // Get all the regCaricoTrappoleList where dataCarico is null
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico is greater than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.greaterThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico is greater than or equal to UPDATED_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.greaterThanOrEqual=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico is less than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.lessThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico is less than or equal to SMALLER_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.lessThanOrEqual=" + SMALLER_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico is less than DEFAULT_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.lessThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico is less than UPDATED_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.lessThan=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataCaricoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataCarico is greater than DEFAULT_DATA_CARICO
        defaultRegCaricoTrappoleShouldNotBeFound("dataCarico.greaterThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoTrappoleList where dataCarico is greater than SMALLER_DATA_CARICO
        defaultRegCaricoTrappoleShouldBeFound("dataCarico.greaterThan=" + SMALLER_DATA_CARICO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDdtIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where ddt equals to DEFAULT_DDT
        defaultRegCaricoTrappoleShouldBeFound("ddt.equals=" + DEFAULT_DDT);

        // Get all the regCaricoTrappoleList where ddt equals to UPDATED_DDT
        defaultRegCaricoTrappoleShouldNotBeFound("ddt.equals=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDdtIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where ddt in DEFAULT_DDT or UPDATED_DDT
        defaultRegCaricoTrappoleShouldBeFound("ddt.in=" + DEFAULT_DDT + "," + UPDATED_DDT);

        // Get all the regCaricoTrappoleList where ddt equals to UPDATED_DDT
        defaultRegCaricoTrappoleShouldNotBeFound("ddt.in=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDdtIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where ddt is not null
        defaultRegCaricoTrappoleShouldBeFound("ddt.specified=true");

        // Get all the regCaricoTrappoleList where ddt is null
        defaultRegCaricoTrappoleShouldNotBeFound("ddt.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza equals to DEFAULT_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.equals=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.equals=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza in DEFAULT_DATA_SCADENZA or UPDATED_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.in=" + DEFAULT_DATA_SCADENZA + "," + UPDATED_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.in=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza is not null
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.specified=true");

        // Get all the regCaricoTrappoleList where dataScadenza is null
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza is greater than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.greaterThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza is greater than or equal to UPDATED_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.greaterThanOrEqual=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza is less than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.lessThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza is less than or equal to SMALLER_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.lessThanOrEqual=" + SMALLER_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza is less than DEFAULT_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.lessThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza is less than UPDATED_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.lessThan=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataScadenzaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataScadenza is greater than DEFAULT_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldNotBeFound("dataScadenza.greaterThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoTrappoleList where dataScadenza is greater than SMALLER_DATA_SCADENZA
        defaultRegCaricoTrappoleShouldBeFound("dataScadenza.greaterThan=" + SMALLER_DATA_SCADENZA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali is not null
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.specified=true");

        // Get all the regCaricoTrappoleList where dataInizVali is null
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoTrappoleList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali is not null
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.specified=true");

        // Get all the regCaricoTrappoleList where dataFineVali is null
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoTrappoleList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegCaricoTrappoleShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator is not null
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.specified=true");

        // Get all the regCaricoTrappoleList where userIdCreator is null
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoTrappoleList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegCaricoTrappoleShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod is not null
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.specified=true");

        // Get all the regCaricoTrappoleList where userIdLastMod is null
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);

        // Get all the regCaricoTrappoleList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoTrappoleList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoTrappoleShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByMagUbicazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        MagUbicazione magUbicazione = MagUbicazioneResourceIT.createEntity(em);
        em.persist(magUbicazione);
        em.flush();
        regCaricoTrappole.setMagUbicazione(magUbicazione);
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Long magUbicazioneId = magUbicazione.getId();

        // Get all the regCaricoTrappoleList where magUbicazione equals to magUbicazioneId
        defaultRegCaricoTrappoleShouldBeFound("magUbicazioneId.equals=" + magUbicazioneId);

        // Get all the regCaricoTrappoleList where magUbicazione equals to magUbicazioneId + 1
        defaultRegCaricoTrappoleShouldNotBeFound("magUbicazioneId.equals=" + (magUbicazioneId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByTrappoleIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Trappole trappole = TrappoleResourceIT.createEntity(em);
        em.persist(trappole);
        em.flush();
        regCaricoTrappole.setTrappole(trappole);
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Long trappoleId = trappole.getId();

        // Get all the regCaricoTrappoleList where trappole equals to trappoleId
        defaultRegCaricoTrappoleShouldBeFound("trappoleId.equals=" + trappoleId);

        // Get all the regCaricoTrappoleList where trappole equals to trappoleId + 1
        defaultRegCaricoTrappoleShouldNotBeFound("trappoleId.equals=" + (trappoleId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByCaricoTrapToFornitoriIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Fornitori caricoTrapToFornitori = FornitoriResourceIT.createEntity(em);
        em.persist(caricoTrapToFornitori);
        em.flush();
        regCaricoTrappole.setCaricoTrapToFornitori(caricoTrapToFornitori);
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Long caricoTrapToFornitoriId = caricoTrapToFornitori.getId();

        // Get all the regCaricoTrappoleList where caricoTrapToFornitori equals to caricoTrapToFornitoriId
        defaultRegCaricoTrappoleShouldBeFound("caricoTrapToFornitoriId.equals=" + caricoTrapToFornitoriId);

        // Get all the regCaricoTrappoleList where caricoTrapToFornitori equals to caricoTrapToFornitoriId + 1
        defaultRegCaricoTrappoleShouldNotBeFound("caricoTrapToFornitoriId.equals=" + (caricoTrapToFornitoriId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoTrappolesByRegTrapToTipoRegIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        TipoRegMag regTrapToTipoReg = TipoRegMagResourceIT.createEntity(em);
        em.persist(regTrapToTipoReg);
        em.flush();
        regCaricoTrappole.setRegTrapToTipoReg(regTrapToTipoReg);
        regCaricoTrappoleRepository.saveAndFlush(regCaricoTrappole);
        Long regTrapToTipoRegId = regTrapToTipoReg.getId();

        // Get all the regCaricoTrappoleList where regTrapToTipoReg equals to regTrapToTipoRegId
        defaultRegCaricoTrappoleShouldBeFound("regTrapToTipoRegId.equals=" + regTrapToTipoRegId);

        // Get all the regCaricoTrappoleList where regTrapToTipoReg equals to regTrapToTipoRegId + 1
        defaultRegCaricoTrappoleShouldNotBeFound("regTrapToTipoRegId.equals=" + (regTrapToTipoRegId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegCaricoTrappoleShouldBeFound(String filter) throws Exception {
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoTrappole.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA)))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT)))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegCaricoTrappoleShouldNotBeFound(String filter) throws Exception {
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegCaricoTrappole() throws Exception {
        // Get the regCaricoTrappole
        restRegCaricoTrappoleMockMvc.perform(get("/api/reg-carico-trappoles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegCaricoTrappole() throws Exception {
        // Initialize the database
        regCaricoTrappoleService.save(regCaricoTrappole);

        int databaseSizeBeforeUpdate = regCaricoTrappoleRepository.findAll().size();

        // Update the regCaricoTrappole
        RegCaricoTrappole updatedRegCaricoTrappole = regCaricoTrappoleRepository.findById(regCaricoTrappole.getId()).get();
        // Disconnect from session so that the updates on updatedRegCaricoTrappole are not directly saved in db
        em.detach(updatedRegCaricoTrappole);
        updatedRegCaricoTrappole
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegCaricoTrappoleMockMvc.perform(put("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegCaricoTrappole)))
            .andExpect(status().isOk());

        // Validate the RegCaricoTrappole in the database
        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeUpdate);
        RegCaricoTrappole testRegCaricoTrappole = regCaricoTrappoleList.get(regCaricoTrappoleList.size() - 1);
        assertThat(testRegCaricoTrappole.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegCaricoTrappole.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegCaricoTrappole.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegCaricoTrappole.getPrezzoUnitario()).isEqualTo(UPDATED_PREZZO_UNITARIO);
        assertThat(testRegCaricoTrappole.getDataCarico()).isEqualTo(UPDATED_DATA_CARICO);
        assertThat(testRegCaricoTrappole.getDdt()).isEqualTo(UPDATED_DDT);
        assertThat(testRegCaricoTrappole.getDataScadenza()).isEqualTo(UPDATED_DATA_SCADENZA);
        assertThat(testRegCaricoTrappole.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegCaricoTrappole.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegCaricoTrappole.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegCaricoTrappole.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegCaricoTrappole.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegCaricoTrappole() throws Exception {
        int databaseSizeBeforeUpdate = regCaricoTrappoleRepository.findAll().size();

        // Create the RegCaricoTrappole

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegCaricoTrappoleMockMvc.perform(put("/api/reg-carico-trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoTrappole)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoTrappole in the database
        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegCaricoTrappole() throws Exception {
        // Initialize the database
        regCaricoTrappoleService.save(regCaricoTrappole);

        int databaseSizeBeforeDelete = regCaricoTrappoleRepository.findAll().size();

        // Delete the regCaricoTrappole
        restRegCaricoTrappoleMockMvc.perform(delete("/api/reg-carico-trappoles/{id}", regCaricoTrappole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegCaricoTrappole> regCaricoTrappoleList = regCaricoTrappoleRepository.findAll();
        assertThat(regCaricoTrappoleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegCaricoTrappole.class);
        RegCaricoTrappole regCaricoTrappole1 = new RegCaricoTrappole();
        regCaricoTrappole1.setId(1L);
        RegCaricoTrappole regCaricoTrappole2 = new RegCaricoTrappole();
        regCaricoTrappole2.setId(regCaricoTrappole1.getId());
        assertThat(regCaricoTrappole1).isEqualTo(regCaricoTrappole2);
        regCaricoTrappole2.setId(2L);
        assertThat(regCaricoTrappole1).isNotEqualTo(regCaricoTrappole2);
        regCaricoTrappole1.setId(null);
        assertThat(regCaricoTrappole1).isNotEqualTo(regCaricoTrappole2);
    }
}
