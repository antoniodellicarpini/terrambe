package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.ColtureBDF;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.domain.TerramColtureMascherate;
import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.repository.ColtureBDFRepository;
import com.terrambe.service.ColtureBDFService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ColtureBDFCriteria;
import com.terrambe.service.ColtureBDFQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ColtureBDFResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ColtureBDFResourceIT {

    private static final String DEFAULT_CODICE_PV = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_PV = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_COLTURA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_COLTURA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private ColtureBDFRepository coltureBDFRepository;

    @Autowired
    private ColtureBDFService coltureBDFService;

    @Autowired
    private ColtureBDFQueryService coltureBDFQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restColtureBDFMockMvc;

    private ColtureBDF coltureBDF;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ColtureBDFResource coltureBDFResource = new ColtureBDFResource(coltureBDFService, coltureBDFQueryService);
        this.restColtureBDFMockMvc = MockMvcBuilders.standaloneSetup(coltureBDFResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ColtureBDF createEntity(EntityManager em) {
        ColtureBDF coltureBDF = new ColtureBDF()
            .codicePv(DEFAULT_CODICE_PV)
            .nomeColtura(DEFAULT_NOME_COLTURA)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return coltureBDF;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ColtureBDF createUpdatedEntity(EntityManager em) {
        ColtureBDF coltureBDF = new ColtureBDF()
            .codicePv(UPDATED_CODICE_PV)
            .nomeColtura(UPDATED_NOME_COLTURA)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return coltureBDF;
    }

    @BeforeEach
    public void initTest() {
        coltureBDF = createEntity(em);
    }

    @Test
    @Transactional
    public void createColtureBDF() throws Exception {
        int databaseSizeBeforeCreate = coltureBDFRepository.findAll().size();

        // Create the ColtureBDF
        restColtureBDFMockMvc.perform(post("/api/colture-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coltureBDF)))
            .andExpect(status().isCreated());

        // Validate the ColtureBDF in the database
        List<ColtureBDF> coltureBDFList = coltureBDFRepository.findAll();
        assertThat(coltureBDFList).hasSize(databaseSizeBeforeCreate + 1);
        ColtureBDF testColtureBDF = coltureBDFList.get(coltureBDFList.size() - 1);
        assertThat(testColtureBDF.getCodicePv()).isEqualTo(DEFAULT_CODICE_PV);
        assertThat(testColtureBDF.getNomeColtura()).isEqualTo(DEFAULT_NOME_COLTURA);
        assertThat(testColtureBDF.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testColtureBDF.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testColtureBDF.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testColtureBDF.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testColtureBDF.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testColtureBDF.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testColtureBDF.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testColtureBDF.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createColtureBDFWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = coltureBDFRepository.findAll().size();

        // Create the ColtureBDF with an existing ID
        coltureBDF.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restColtureBDFMockMvc.perform(post("/api/colture-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coltureBDF)))
            .andExpect(status().isBadRequest());

        // Validate the ColtureBDF in the database
        List<ColtureBDF> coltureBDFList = coltureBDFRepository.findAll();
        assertThat(coltureBDFList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllColtureBDFS() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coltureBDF.getId().intValue())))
            .andExpect(jsonPath("$.[*].codicePv").value(hasItem(DEFAULT_CODICE_PV.toString())))
            .andExpect(jsonPath("$.[*].nomeColtura").value(hasItem(DEFAULT_NOME_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getColtureBDF() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get the coltureBDF
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs/{id}", coltureBDF.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(coltureBDF.getId().intValue()))
            .andExpect(jsonPath("$.codicePv").value(DEFAULT_CODICE_PV.toString()))
            .andExpect(jsonPath("$.nomeColtura").value(DEFAULT_NOME_COLTURA.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByCodicePvIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where codicePv equals to DEFAULT_CODICE_PV
        defaultColtureBDFShouldBeFound("codicePv.equals=" + DEFAULT_CODICE_PV);

        // Get all the coltureBDFList where codicePv equals to UPDATED_CODICE_PV
        defaultColtureBDFShouldNotBeFound("codicePv.equals=" + UPDATED_CODICE_PV);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByCodicePvIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where codicePv in DEFAULT_CODICE_PV or UPDATED_CODICE_PV
        defaultColtureBDFShouldBeFound("codicePv.in=" + DEFAULT_CODICE_PV + "," + UPDATED_CODICE_PV);

        // Get all the coltureBDFList where codicePv equals to UPDATED_CODICE_PV
        defaultColtureBDFShouldNotBeFound("codicePv.in=" + UPDATED_CODICE_PV);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByCodicePvIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where codicePv is not null
        defaultColtureBDFShouldBeFound("codicePv.specified=true");

        // Get all the coltureBDFList where codicePv is null
        defaultColtureBDFShouldNotBeFound("codicePv.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByNomeColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where nomeColtura equals to DEFAULT_NOME_COLTURA
        defaultColtureBDFShouldBeFound("nomeColtura.equals=" + DEFAULT_NOME_COLTURA);

        // Get all the coltureBDFList where nomeColtura equals to UPDATED_NOME_COLTURA
        defaultColtureBDFShouldNotBeFound("nomeColtura.equals=" + UPDATED_NOME_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByNomeColturaIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where nomeColtura in DEFAULT_NOME_COLTURA or UPDATED_NOME_COLTURA
        defaultColtureBDFShouldBeFound("nomeColtura.in=" + DEFAULT_NOME_COLTURA + "," + UPDATED_NOME_COLTURA);

        // Get all the coltureBDFList where nomeColtura equals to UPDATED_NOME_COLTURA
        defaultColtureBDFShouldNotBeFound("nomeColtura.in=" + UPDATED_NOME_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByNomeColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where nomeColtura is not null
        defaultColtureBDFShouldBeFound("nomeColtura.specified=true");

        // Get all the coltureBDFList where nomeColtura is null
        defaultColtureBDFShouldNotBeFound("nomeColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultColtureBDFShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the coltureBDFList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultColtureBDFShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultColtureBDFShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the coltureBDFList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultColtureBDFShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where tipoImport is not null
        defaultColtureBDFShouldBeFound("tipoImport.specified=true");

        // Get all the coltureBDFList where tipoImport is null
        defaultColtureBDFShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where operatore equals to DEFAULT_OPERATORE
        defaultColtureBDFShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the coltureBDFList where operatore equals to UPDATED_OPERATORE
        defaultColtureBDFShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultColtureBDFShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the coltureBDFList where operatore equals to UPDATED_OPERATORE
        defaultColtureBDFShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where operatore is not null
        defaultColtureBDFShouldBeFound("operatore.specified=true");

        // Get all the coltureBDFList where operatore is null
        defaultColtureBDFShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where ts equals to DEFAULT_TS
        defaultColtureBDFShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the coltureBDFList where ts equals to UPDATED_TS
        defaultColtureBDFShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTsIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where ts in DEFAULT_TS or UPDATED_TS
        defaultColtureBDFShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the coltureBDFList where ts equals to UPDATED_TS
        defaultColtureBDFShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where ts is not null
        defaultColtureBDFShouldBeFound("ts.specified=true");

        // Get all the coltureBDFList where ts is null
        defaultColtureBDFShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali is not null
        defaultColtureBDFShouldBeFound("dataInizVali.specified=true");

        // Get all the coltureBDFList where dataInizVali is null
        defaultColtureBDFShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultColtureBDFShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the coltureBDFList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultColtureBDFShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali is not null
        defaultColtureBDFShouldBeFound("dataFineVali.specified=true");

        // Get all the coltureBDFList where dataFineVali is null
        defaultColtureBDFShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultColtureBDFShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the coltureBDFList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultColtureBDFShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator is not null
        defaultColtureBDFShouldBeFound("userIdCreator.specified=true");

        // Get all the coltureBDFList where userIdCreator is null
        defaultColtureBDFShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultColtureBDFShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the coltureBDFList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultColtureBDFShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod is not null
        defaultColtureBDFShouldBeFound("userIdLastMod.specified=true");

        // Get all the coltureBDFList where userIdLastMod is null
        defaultColtureBDFShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColtureBDFSByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);

        // Get all the coltureBDFList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultColtureBDFShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the coltureBDFList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultColtureBDFShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByColtBdfToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);
        TabellaRaccordo coltBdfToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(coltBdfToRaccordo);
        em.flush();
        coltureBDF.addColtBdfToRaccordo(coltBdfToRaccordo);
        coltureBDFRepository.saveAndFlush(coltureBDF);
        Long coltBdfToRaccordoId = coltBdfToRaccordo.getId();

        // Get all the coltureBDFList where coltBdfToRaccordo equals to coltBdfToRaccordoId
        defaultColtureBDFShouldBeFound("coltBdfToRaccordoId.equals=" + coltBdfToRaccordoId);

        // Get all the coltureBDFList where coltBdfToRaccordo equals to coltBdfToRaccordoId + 1
        defaultColtureBDFShouldNotBeFound("coltBdfToRaccordoId.equals=" + (coltBdfToRaccordoId + 1));
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByColtBdfToColtMascIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);
        TerramColtureMascherate coltBdfToColtMasc = TerramColtureMascherateResourceIT.createEntity(em);
        em.persist(coltBdfToColtMasc);
        em.flush();
        coltureBDF.addColtBdfToColtMasc(coltBdfToColtMasc);
        coltureBDFRepository.saveAndFlush(coltureBDF);
        Long coltBdfToColtMascId = coltBdfToColtMasc.getId();

        // Get all the coltureBDFList where coltBdfToColtMasc equals to coltBdfToColtMascId
        defaultColtureBDFShouldBeFound("coltBdfToColtMascId.equals=" + coltBdfToColtMascId);

        // Get all the coltureBDFList where coltBdfToColtMasc equals to coltBdfToColtMascId + 1
        defaultColtureBDFShouldNotBeFound("coltBdfToColtMascId.equals=" + (coltBdfToColtMascId + 1));
    }


    @Test
    @Transactional
    public void getAllColtureBDFSByColtBdfToAgeaBdfIsEqualToSomething() throws Exception {
        // Initialize the database
        coltureBDFRepository.saveAndFlush(coltureBDF);
        Agea20152020BDF coltBdfToAgeaBdf = Agea20152020BDFResourceIT.createEntity(em);
        em.persist(coltBdfToAgeaBdf);
        em.flush();
        coltureBDF.addColtBdfToAgeaBdf(coltBdfToAgeaBdf);
        coltureBDFRepository.saveAndFlush(coltureBDF);
        Long coltBdfToAgeaBdfId = coltBdfToAgeaBdf.getId();

        // Get all the coltureBDFList where coltBdfToAgeaBdf equals to coltBdfToAgeaBdfId
        defaultColtureBDFShouldBeFound("coltBdfToAgeaBdfId.equals=" + coltBdfToAgeaBdfId);

        // Get all the coltureBDFList where coltBdfToAgeaBdf equals to coltBdfToAgeaBdfId + 1
        defaultColtureBDFShouldNotBeFound("coltBdfToAgeaBdfId.equals=" + (coltBdfToAgeaBdfId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultColtureBDFShouldBeFound(String filter) throws Exception {
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coltureBDF.getId().intValue())))
            .andExpect(jsonPath("$.[*].codicePv").value(hasItem(DEFAULT_CODICE_PV)))
            .andExpect(jsonPath("$.[*].nomeColtura").value(hasItem(DEFAULT_NOME_COLTURA)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultColtureBDFShouldNotBeFound(String filter) throws Exception {
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingColtureBDF() throws Exception {
        // Get the coltureBDF
        restColtureBDFMockMvc.perform(get("/api/colture-bdfs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateColtureBDF() throws Exception {
        // Initialize the database
        coltureBDFService.save(coltureBDF);

        int databaseSizeBeforeUpdate = coltureBDFRepository.findAll().size();

        // Update the coltureBDF
        ColtureBDF updatedColtureBDF = coltureBDFRepository.findById(coltureBDF.getId()).get();
        // Disconnect from session so that the updates on updatedColtureBDF are not directly saved in db
        em.detach(updatedColtureBDF);
        updatedColtureBDF
            .codicePv(UPDATED_CODICE_PV)
            .nomeColtura(UPDATED_NOME_COLTURA)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restColtureBDFMockMvc.perform(put("/api/colture-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedColtureBDF)))
            .andExpect(status().isOk());

        // Validate the ColtureBDF in the database
        List<ColtureBDF> coltureBDFList = coltureBDFRepository.findAll();
        assertThat(coltureBDFList).hasSize(databaseSizeBeforeUpdate);
        ColtureBDF testColtureBDF = coltureBDFList.get(coltureBDFList.size() - 1);
        assertThat(testColtureBDF.getCodicePv()).isEqualTo(UPDATED_CODICE_PV);
        assertThat(testColtureBDF.getNomeColtura()).isEqualTo(UPDATED_NOME_COLTURA);
        assertThat(testColtureBDF.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testColtureBDF.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testColtureBDF.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testColtureBDF.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testColtureBDF.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testColtureBDF.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testColtureBDF.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testColtureBDF.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingColtureBDF() throws Exception {
        int databaseSizeBeforeUpdate = coltureBDFRepository.findAll().size();

        // Create the ColtureBDF

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restColtureBDFMockMvc.perform(put("/api/colture-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coltureBDF)))
            .andExpect(status().isBadRequest());

        // Validate the ColtureBDF in the database
        List<ColtureBDF> coltureBDFList = coltureBDFRepository.findAll();
        assertThat(coltureBDFList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteColtureBDF() throws Exception {
        // Initialize the database
        coltureBDFService.save(coltureBDF);

        int databaseSizeBeforeDelete = coltureBDFRepository.findAll().size();

        // Delete the coltureBDF
        restColtureBDFMockMvc.perform(delete("/api/colture-bdfs/{id}", coltureBDF.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ColtureBDF> coltureBDFList = coltureBDFRepository.findAll();
        assertThat(coltureBDFList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ColtureBDF.class);
        ColtureBDF coltureBDF1 = new ColtureBDF();
        coltureBDF1.setId(1L);
        ColtureBDF coltureBDF2 = new ColtureBDF();
        coltureBDF2.setId(coltureBDF1.getId());
        assertThat(coltureBDF1).isEqualTo(coltureBDF2);
        coltureBDF2.setId(2L);
        assertThat(coltureBDF1).isNotEqualTo(coltureBDF2);
        coltureBDF1.setId(null);
        assertThat(coltureBDF1).isNotEqualTo(coltureBDF2);
    }
}
