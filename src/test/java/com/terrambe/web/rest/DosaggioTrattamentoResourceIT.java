package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DosaggioTrattamento;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.RegOpcFito;
import com.terrambe.repository.DosaggioTrattamentoRepository;
import com.terrambe.service.DosaggioTrattamentoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DosaggioTrattamentoCriteria;
import com.terrambe.service.DosaggioTrattamentoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DosaggioTrattamentoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DosaggioTrattamentoResourceIT {

    private static final Double DEFAULT_TOTALE_PRODOTTO_UTILIZZATO = 1D;
    private static final Double UPDATED_TOTALE_PRODOTTO_UTILIZZATO = 2D;
    private static final Double SMALLER_TOTALE_PRODOTTO_UTILIZZATO = 1D - 1D;

    @Autowired
    private DosaggioTrattamentoRepository dosaggioTrattamentoRepository;

    @Autowired
    private DosaggioTrattamentoService dosaggioTrattamentoService;

    @Autowired
    private DosaggioTrattamentoQueryService dosaggioTrattamentoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDosaggioTrattamentoMockMvc;

    private DosaggioTrattamento dosaggioTrattamento;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DosaggioTrattamentoResource dosaggioTrattamentoResource = new DosaggioTrattamentoResource(dosaggioTrattamentoService, dosaggioTrattamentoQueryService);
        this.restDosaggioTrattamentoMockMvc = MockMvcBuilders.standaloneSetup(dosaggioTrattamentoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DosaggioTrattamento createEntity(EntityManager em) {
        DosaggioTrattamento dosaggioTrattamento = new DosaggioTrattamento()
            .totaleProdottoUtilizzato(DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);
        return dosaggioTrattamento;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DosaggioTrattamento createUpdatedEntity(EntityManager em) {
        DosaggioTrattamento dosaggioTrattamento = new DosaggioTrattamento()
            .totaleProdottoUtilizzato(UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
        return dosaggioTrattamento;
    }

    @BeforeEach
    public void initTest() {
        dosaggioTrattamento = createEntity(em);
    }

    @Test
    @Transactional
    public void createDosaggioTrattamento() throws Exception {
        int databaseSizeBeforeCreate = dosaggioTrattamentoRepository.findAll().size();

        // Create the DosaggioTrattamento
        restDosaggioTrattamentoMockMvc.perform(post("/api/dosaggio-trattamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggioTrattamento)))
            .andExpect(status().isCreated());

        // Validate the DosaggioTrattamento in the database
        List<DosaggioTrattamento> dosaggioTrattamentoList = dosaggioTrattamentoRepository.findAll();
        assertThat(dosaggioTrattamentoList).hasSize(databaseSizeBeforeCreate + 1);
        DosaggioTrattamento testDosaggioTrattamento = dosaggioTrattamentoList.get(dosaggioTrattamentoList.size() - 1);
        assertThat(testDosaggioTrattamento.getTotaleProdottoUtilizzato()).isEqualTo(DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void createDosaggioTrattamentoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dosaggioTrattamentoRepository.findAll().size();

        // Create the DosaggioTrattamento with an existing ID
        dosaggioTrattamento.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDosaggioTrattamentoMockMvc.perform(post("/api/dosaggio-trattamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggioTrattamento)))
            .andExpect(status().isBadRequest());

        // Validate the DosaggioTrattamento in the database
        List<DosaggioTrattamento> dosaggioTrattamentoList = dosaggioTrattamentoRepository.findAll();
        assertThat(dosaggioTrattamentoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDosaggioTrattamentos() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosaggioTrattamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].totaleProdottoUtilizzato").value(hasItem(DEFAULT_TOTALE_PRODOTTO_UTILIZZATO.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getDosaggioTrattamento() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get the dosaggioTrattamento
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos/{id}", dosaggioTrattamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dosaggioTrattamento.getId().intValue()))
            .andExpect(jsonPath("$.totaleProdottoUtilizzato").value(DEFAULT_TOTALE_PRODOTTO_UTILIZZATO.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato equals to DEFAULT_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.equals=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato equals to UPDATED_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.equals=" + UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato in DEFAULT_TOTALE_PRODOTTO_UTILIZZATO or UPDATED_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.in=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO + "," + UPDATED_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato equals to UPDATED_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.in=" + UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is not null
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.specified=true");

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is null
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is greater than or equal to DEFAULT_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.greaterThanOrEqual=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is greater than or equal to UPDATED_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.greaterThanOrEqual=" + UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is less than or equal to DEFAULT_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.lessThanOrEqual=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is less than or equal to SMALLER_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.lessThanOrEqual=" + SMALLER_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsLessThanSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is less than DEFAULT_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.lessThan=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is less than UPDATED_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.lessThan=" + UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByTotaleProdottoUtilizzatoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is greater than DEFAULT_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldNotBeFound("totaleProdottoUtilizzato.greaterThan=" + DEFAULT_TOTALE_PRODOTTO_UTILIZZATO);

        // Get all the dosaggioTrattamentoList where totaleProdottoUtilizzato is greater than SMALLER_TOTALE_PRODOTTO_UTILIZZATO
        defaultDosaggioTrattamentoShouldBeFound("totaleProdottoUtilizzato.greaterThan=" + SMALLER_TOTALE_PRODOTTO_UTILIZZATO);
    }


    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByEtichettafitoIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);
        EtichettaFito etichettafito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettafito);
        em.flush();
        dosaggioTrattamento.setEtichettafito(etichettafito);
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);
        Long etichettafitoId = etichettafito.getId();

        // Get all the dosaggioTrattamentoList where etichettafito equals to etichettafitoId
        defaultDosaggioTrattamentoShouldBeFound("etichettafitoId.equals=" + etichettafitoId);

        // Get all the dosaggioTrattamentoList where etichettafito equals to etichettafitoId + 1
        defaultDosaggioTrattamentoShouldNotBeFound("etichettafitoId.equals=" + (etichettafitoId + 1));
    }


    @Test
    @Transactional
    public void getAllDosaggioTrattamentosByDosTrattToRegOpcFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);
        RegOpcFito dosTrattToRegOpcFito = RegOpcFitoResourceIT.createEntity(em);
        em.persist(dosTrattToRegOpcFito);
        em.flush();
        dosaggioTrattamento.addDosTrattToRegOpcFito(dosTrattToRegOpcFito);
        dosaggioTrattamentoRepository.saveAndFlush(dosaggioTrattamento);
        Long dosTrattToRegOpcFitoId = dosTrattToRegOpcFito.getId();

        // Get all the dosaggioTrattamentoList where dosTrattToRegOpcFito equals to dosTrattToRegOpcFitoId
        defaultDosaggioTrattamentoShouldBeFound("dosTrattToRegOpcFitoId.equals=" + dosTrattToRegOpcFitoId);

        // Get all the dosaggioTrattamentoList where dosTrattToRegOpcFito equals to dosTrattToRegOpcFitoId + 1
        defaultDosaggioTrattamentoShouldNotBeFound("dosTrattToRegOpcFitoId.equals=" + (dosTrattToRegOpcFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDosaggioTrattamentoShouldBeFound(String filter) throws Exception {
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosaggioTrattamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].totaleProdottoUtilizzato").value(hasItem(DEFAULT_TOTALE_PRODOTTO_UTILIZZATO.doubleValue())));

        // Check, that the count call also returns 1
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDosaggioTrattamentoShouldNotBeFound(String filter) throws Exception {
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDosaggioTrattamento() throws Exception {
        // Get the dosaggioTrattamento
        restDosaggioTrattamentoMockMvc.perform(get("/api/dosaggio-trattamentos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDosaggioTrattamento() throws Exception {
        // Initialize the database
        dosaggioTrattamentoService.save(dosaggioTrattamento);

        int databaseSizeBeforeUpdate = dosaggioTrattamentoRepository.findAll().size();

        // Update the dosaggioTrattamento
        DosaggioTrattamento updatedDosaggioTrattamento = dosaggioTrattamentoRepository.findById(dosaggioTrattamento.getId()).get();
        // Disconnect from session so that the updates on updatedDosaggioTrattamento are not directly saved in db
        em.detach(updatedDosaggioTrattamento);
        updatedDosaggioTrattamento
            .totaleProdottoUtilizzato(UPDATED_TOTALE_PRODOTTO_UTILIZZATO);

        restDosaggioTrattamentoMockMvc.perform(put("/api/dosaggio-trattamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDosaggioTrattamento)))
            .andExpect(status().isOk());

        // Validate the DosaggioTrattamento in the database
        List<DosaggioTrattamento> dosaggioTrattamentoList = dosaggioTrattamentoRepository.findAll();
        assertThat(dosaggioTrattamentoList).hasSize(databaseSizeBeforeUpdate);
        DosaggioTrattamento testDosaggioTrattamento = dosaggioTrattamentoList.get(dosaggioTrattamentoList.size() - 1);
        assertThat(testDosaggioTrattamento.getTotaleProdottoUtilizzato()).isEqualTo(UPDATED_TOTALE_PRODOTTO_UTILIZZATO);
    }

    @Test
    @Transactional
    public void updateNonExistingDosaggioTrattamento() throws Exception {
        int databaseSizeBeforeUpdate = dosaggioTrattamentoRepository.findAll().size();

        // Create the DosaggioTrattamento

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDosaggioTrattamentoMockMvc.perform(put("/api/dosaggio-trattamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggioTrattamento)))
            .andExpect(status().isBadRequest());

        // Validate the DosaggioTrattamento in the database
        List<DosaggioTrattamento> dosaggioTrattamentoList = dosaggioTrattamentoRepository.findAll();
        assertThat(dosaggioTrattamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDosaggioTrattamento() throws Exception {
        // Initialize the database
        dosaggioTrattamentoService.save(dosaggioTrattamento);

        int databaseSizeBeforeDelete = dosaggioTrattamentoRepository.findAll().size();

        // Delete the dosaggioTrattamento
        restDosaggioTrattamentoMockMvc.perform(delete("/api/dosaggio-trattamentos/{id}", dosaggioTrattamento.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DosaggioTrattamento> dosaggioTrattamentoList = dosaggioTrattamentoRepository.findAll();
        assertThat(dosaggioTrattamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DosaggioTrattamento.class);
        DosaggioTrattamento dosaggioTrattamento1 = new DosaggioTrattamento();
        dosaggioTrattamento1.setId(1L);
        DosaggioTrattamento dosaggioTrattamento2 = new DosaggioTrattamento();
        dosaggioTrattamento2.setId(dosaggioTrattamento1.getId());
        assertThat(dosaggioTrattamento1).isEqualTo(dosaggioTrattamento2);
        dosaggioTrattamento2.setId(2L);
        assertThat(dosaggioTrattamento1).isNotEqualTo(dosaggioTrattamento2);
        dosaggioTrattamento1.setId(null);
        assertThat(dosaggioTrattamento1).isNotEqualTo(dosaggioTrattamento2);
    }
}
