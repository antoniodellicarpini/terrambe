package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.EtichettaPittogrammi;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.EtichettaPittogrammiRepository;
import com.terrambe.service.EtichettaPittogrammiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.EtichettaPittogrammiCriteria;
import com.terrambe.service.EtichettaPittogrammiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtichettaPittogrammiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class EtichettaPittogrammiResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private EtichettaPittogrammiRepository etichettaPittogrammiRepository;

    @Autowired
    private EtichettaPittogrammiService etichettaPittogrammiService;

    @Autowired
    private EtichettaPittogrammiQueryService etichettaPittogrammiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEtichettaPittogrammiMockMvc;

    private EtichettaPittogrammi etichettaPittogrammi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EtichettaPittogrammiResource etichettaPittogrammiResource = new EtichettaPittogrammiResource(etichettaPittogrammiService, etichettaPittogrammiQueryService);
        this.restEtichettaPittogrammiMockMvc = MockMvcBuilders.standaloneSetup(etichettaPittogrammiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaPittogrammi createEntity(EntityManager em) {
        EtichettaPittogrammi etichettaPittogrammi = new EtichettaPittogrammi()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return etichettaPittogrammi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaPittogrammi createUpdatedEntity(EntityManager em) {
        EtichettaPittogrammi etichettaPittogrammi = new EtichettaPittogrammi()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return etichettaPittogrammi;
    }

    @BeforeEach
    public void initTest() {
        etichettaPittogrammi = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtichettaPittogrammi() throws Exception {
        int databaseSizeBeforeCreate = etichettaPittogrammiRepository.findAll().size();

        // Create the EtichettaPittogrammi
        restEtichettaPittogrammiMockMvc.perform(post("/api/etichetta-pittogrammis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaPittogrammi)))
            .andExpect(status().isCreated());

        // Validate the EtichettaPittogrammi in the database
        List<EtichettaPittogrammi> etichettaPittogrammiList = etichettaPittogrammiRepository.findAll();
        assertThat(etichettaPittogrammiList).hasSize(databaseSizeBeforeCreate + 1);
        EtichettaPittogrammi testEtichettaPittogrammi = etichettaPittogrammiList.get(etichettaPittogrammiList.size() - 1);
        assertThat(testEtichettaPittogrammi.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testEtichettaPittogrammi.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testEtichettaPittogrammi.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testEtichettaPittogrammi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testEtichettaPittogrammi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testEtichettaPittogrammi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testEtichettaPittogrammi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testEtichettaPittogrammi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createEtichettaPittogrammiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etichettaPittogrammiRepository.findAll().size();

        // Create the EtichettaPittogrammi with an existing ID
        etichettaPittogrammi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtichettaPittogrammiMockMvc.perform(post("/api/etichetta-pittogrammis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaPittogrammi)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaPittogrammi in the database
        List<EtichettaPittogrammi> etichettaPittogrammiList = etichettaPittogrammiRepository.findAll();
        assertThat(etichettaPittogrammiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEtichettaPittogrammis() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaPittogrammi.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtichettaPittogrammi() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get the etichettaPittogrammi
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis/{id}", etichettaPittogrammi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etichettaPittogrammi.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultEtichettaPittogrammiShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the etichettaPittogrammiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaPittogrammiShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultEtichettaPittogrammiShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the etichettaPittogrammiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaPittogrammiShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where tipoImport is not null
        defaultEtichettaPittogrammiShouldBeFound("tipoImport.specified=true");

        // Get all the etichettaPittogrammiList where tipoImport is null
        defaultEtichettaPittogrammiShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where operatore equals to DEFAULT_OPERATORE
        defaultEtichettaPittogrammiShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the etichettaPittogrammiList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaPittogrammiShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultEtichettaPittogrammiShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the etichettaPittogrammiList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaPittogrammiShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where operatore is not null
        defaultEtichettaPittogrammiShouldBeFound("operatore.specified=true");

        // Get all the etichettaPittogrammiList where operatore is null
        defaultEtichettaPittogrammiShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where ts equals to DEFAULT_TS
        defaultEtichettaPittogrammiShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the etichettaPittogrammiList where ts equals to UPDATED_TS
        defaultEtichettaPittogrammiShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTsIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where ts in DEFAULT_TS or UPDATED_TS
        defaultEtichettaPittogrammiShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the etichettaPittogrammiList where ts equals to UPDATED_TS
        defaultEtichettaPittogrammiShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where ts is not null
        defaultEtichettaPittogrammiShouldBeFound("ts.specified=true");

        // Get all the etichettaPittogrammiList where ts is null
        defaultEtichettaPittogrammiShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali is not null
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.specified=true");

        // Get all the etichettaPittogrammiList where dataInizVali is null
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaPittogrammiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali is not null
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.specified=true");

        // Get all the etichettaPittogrammiList where dataFineVali is null
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaPittogrammiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultEtichettaPittogrammiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator is not null
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.specified=true");

        // Get all the etichettaPittogrammiList where userIdCreator is null
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaPittogrammiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultEtichettaPittogrammiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod is not null
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.specified=true");

        // Get all the etichettaPittogrammiList where userIdLastMod is null
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);

        // Get all the etichettaPittogrammiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaPittogrammiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultEtichettaPittogrammiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllEtichettaPittogrammisByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        etichettaPittogrammi.setEtichettaFito(etichettaFito);
        etichettaPittogrammiRepository.saveAndFlush(etichettaPittogrammi);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the etichettaPittogrammiList where etichettaFito equals to etichettaFitoId
        defaultEtichettaPittogrammiShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the etichettaPittogrammiList where etichettaFito equals to etichettaFitoId + 1
        defaultEtichettaPittogrammiShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtichettaPittogrammiShouldBeFound(String filter) throws Exception {
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaPittogrammi.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtichettaPittogrammiShouldNotBeFound(String filter) throws Exception {
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtichettaPittogrammi() throws Exception {
        // Get the etichettaPittogrammi
        restEtichettaPittogrammiMockMvc.perform(get("/api/etichetta-pittogrammis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtichettaPittogrammi() throws Exception {
        // Initialize the database
        etichettaPittogrammiService.save(etichettaPittogrammi);

        int databaseSizeBeforeUpdate = etichettaPittogrammiRepository.findAll().size();

        // Update the etichettaPittogrammi
        EtichettaPittogrammi updatedEtichettaPittogrammi = etichettaPittogrammiRepository.findById(etichettaPittogrammi.getId()).get();
        // Disconnect from session so that the updates on updatedEtichettaPittogrammi are not directly saved in db
        em.detach(updatedEtichettaPittogrammi);
        updatedEtichettaPittogrammi
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restEtichettaPittogrammiMockMvc.perform(put("/api/etichetta-pittogrammis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtichettaPittogrammi)))
            .andExpect(status().isOk());

        // Validate the EtichettaPittogrammi in the database
        List<EtichettaPittogrammi> etichettaPittogrammiList = etichettaPittogrammiRepository.findAll();
        assertThat(etichettaPittogrammiList).hasSize(databaseSizeBeforeUpdate);
        EtichettaPittogrammi testEtichettaPittogrammi = etichettaPittogrammiList.get(etichettaPittogrammiList.size() - 1);
        assertThat(testEtichettaPittogrammi.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testEtichettaPittogrammi.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testEtichettaPittogrammi.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testEtichettaPittogrammi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testEtichettaPittogrammi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testEtichettaPittogrammi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testEtichettaPittogrammi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testEtichettaPittogrammi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtichettaPittogrammi() throws Exception {
        int databaseSizeBeforeUpdate = etichettaPittogrammiRepository.findAll().size();

        // Create the EtichettaPittogrammi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtichettaPittogrammiMockMvc.perform(put("/api/etichetta-pittogrammis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaPittogrammi)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaPittogrammi in the database
        List<EtichettaPittogrammi> etichettaPittogrammiList = etichettaPittogrammiRepository.findAll();
        assertThat(etichettaPittogrammiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtichettaPittogrammi() throws Exception {
        // Initialize the database
        etichettaPittogrammiService.save(etichettaPittogrammi);

        int databaseSizeBeforeDelete = etichettaPittogrammiRepository.findAll().size();

        // Delete the etichettaPittogrammi
        restEtichettaPittogrammiMockMvc.perform(delete("/api/etichetta-pittogrammis/{id}", etichettaPittogrammi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtichettaPittogrammi> etichettaPittogrammiList = etichettaPittogrammiRepository.findAll();
        assertThat(etichettaPittogrammiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtichettaPittogrammi.class);
        EtichettaPittogrammi etichettaPittogrammi1 = new EtichettaPittogrammi();
        etichettaPittogrammi1.setId(1L);
        EtichettaPittogrammi etichettaPittogrammi2 = new EtichettaPittogrammi();
        etichettaPittogrammi2.setId(etichettaPittogrammi1.getId());
        assertThat(etichettaPittogrammi1).isEqualTo(etichettaPittogrammi2);
        etichettaPittogrammi2.setId(2L);
        assertThat(etichettaPittogrammi1).isNotEqualTo(etichettaPittogrammi2);
        etichettaPittogrammi1.setId(null);
        assertThat(etichettaPittogrammi1).isNotEqualTo(etichettaPittogrammi2);
    }
}
