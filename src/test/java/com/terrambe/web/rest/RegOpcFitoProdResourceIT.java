package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcFitoProd;
import com.terrambe.domain.RegOpcFito;
import com.terrambe.repository.RegOpcFitoProdRepository;
import com.terrambe.service.RegOpcFitoProdService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcFitoProdCriteria;
import com.terrambe.service.RegOpcFitoProdQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcFitoProdResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcFitoProdResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_QUANTITA_FITO = 1F;
    private static final Float UPDATED_QUANTITA_FITO = 2F;
    private static final Float SMALLER_QUANTITA_FITO = 1F - 1F;

    private static final Long DEFAULT_ID_FITOFARMACO = 1L;
    private static final Long UPDATED_ID_FITOFARMACO = 2L;
    private static final Long SMALLER_ID_FITOFARMACO = 1L - 1L;

    private static final Long DEFAULT_ID_MAGAZZINO = 1L;
    private static final Long UPDATED_ID_MAGAZZINO = 2L;
    private static final Long SMALLER_ID_MAGAZZINO = 1L - 1L;

    private static final Long DEFAULT_ID_AVVERSITA = 1L;
    private static final Long UPDATED_ID_AVVERSITA = 2L;
    private static final Long SMALLER_ID_AVVERSITA = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcFitoProdRepository regOpcFitoProdRepository;

    @Autowired
    private RegOpcFitoProdService regOpcFitoProdService;

    @Autowired
    private RegOpcFitoProdQueryService regOpcFitoProdQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcFitoProdMockMvc;

    private RegOpcFitoProd regOpcFitoProd;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcFitoProdResource regOpcFitoProdResource = new RegOpcFitoProdResource(regOpcFitoProdService, regOpcFitoProdQueryService);
        this.restRegOpcFitoProdMockMvc = MockMvcBuilders.standaloneSetup(regOpcFitoProdResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFitoProd createEntity(EntityManager em) {
        RegOpcFitoProd regOpcFitoProd = new RegOpcFitoProd()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .quantitaFito(DEFAULT_QUANTITA_FITO)
            .idFitofarmaco(DEFAULT_ID_FITOFARMACO)
            .idMagazzino(DEFAULT_ID_MAGAZZINO)
            .idAvversita(DEFAULT_ID_AVVERSITA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcFitoProd;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFitoProd createUpdatedEntity(EntityManager em) {
        RegOpcFitoProd regOpcFitoProd = new RegOpcFitoProd()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantitaFito(UPDATED_QUANTITA_FITO)
            .idFitofarmaco(UPDATED_ID_FITOFARMACO)
            .idMagazzino(UPDATED_ID_MAGAZZINO)
            .idAvversita(UPDATED_ID_AVVERSITA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcFitoProd;
    }

    @BeforeEach
    public void initTest() {
        regOpcFitoProd = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcFitoProd() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoProdRepository.findAll().size();

        // Create the RegOpcFitoProd
        restRegOpcFitoProdMockMvc.perform(post("/api/reg-opc-fito-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoProd)))
            .andExpect(status().isCreated());

        // Validate the RegOpcFitoProd in the database
        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcFitoProd testRegOpcFitoProd = regOpcFitoProdList.get(regOpcFitoProdList.size() - 1);
        assertThat(testRegOpcFitoProd.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcFitoProd.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcFitoProd.getQuantitaFito()).isEqualTo(DEFAULT_QUANTITA_FITO);
        assertThat(testRegOpcFitoProd.getIdFitofarmaco()).isEqualTo(DEFAULT_ID_FITOFARMACO);
        assertThat(testRegOpcFitoProd.getIdMagazzino()).isEqualTo(DEFAULT_ID_MAGAZZINO);
        assertThat(testRegOpcFitoProd.getIdAvversita()).isEqualTo(DEFAULT_ID_AVVERSITA);
        assertThat(testRegOpcFitoProd.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcFitoProd.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcFitoProd.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcFitoProd.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcFitoProd.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcFitoProdWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoProdRepository.findAll().size();

        // Create the RegOpcFitoProd with an existing ID
        regOpcFitoProd.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcFitoProdMockMvc.perform(post("/api/reg-opc-fito-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoProd)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFitoProd in the database
        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcFitoProdRepository.findAll().size();
        // set the field null
        regOpcFitoProd.setIdAzienda(null);

        // Create the RegOpcFitoProd, which fails.

        restRegOpcFitoProdMockMvc.perform(post("/api/reg-opc-fito-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoProd)))
            .andExpect(status().isBadRequest());

        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProds() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFitoProd.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantitaFito").value(hasItem(DEFAULT_QUANTITA_FITO.doubleValue())))
            .andExpect(jsonPath("$.[*].idFitofarmaco").value(hasItem(DEFAULT_ID_FITOFARMACO.intValue())))
            .andExpect(jsonPath("$.[*].idMagazzino").value(hasItem(DEFAULT_ID_MAGAZZINO.intValue())))
            .andExpect(jsonPath("$.[*].idAvversita").value(hasItem(DEFAULT_ID_AVVERSITA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcFitoProd() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get the regOpcFitoProd
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods/{id}", regOpcFitoProd.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcFitoProd.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.quantitaFito").value(DEFAULT_QUANTITA_FITO.doubleValue()))
            .andExpect(jsonPath("$.idFitofarmaco").value(DEFAULT_ID_FITOFARMACO.intValue()))
            .andExpect(jsonPath("$.idMagazzino").value(DEFAULT_ID_MAGAZZINO.intValue()))
            .andExpect(jsonPath("$.idAvversita").value(DEFAULT_ID_AVVERSITA.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda is not null
        defaultRegOpcFitoProdShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcFitoProdList where idAzienda is null
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoProdShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoProdList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcFitoProdShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd is not null
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcFitoProdList where idUnitaProd is null
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoProdList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoProdShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito equals to DEFAULT_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.equals=" + DEFAULT_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito equals to UPDATED_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.equals=" + UPDATED_QUANTITA_FITO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito in DEFAULT_QUANTITA_FITO or UPDATED_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.in=" + DEFAULT_QUANTITA_FITO + "," + UPDATED_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito equals to UPDATED_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.in=" + UPDATED_QUANTITA_FITO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito is not null
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.specified=true");

        // Get all the regOpcFitoProdList where quantitaFito is null
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito is greater than or equal to DEFAULT_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.greaterThanOrEqual=" + DEFAULT_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito is greater than or equal to UPDATED_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.greaterThanOrEqual=" + UPDATED_QUANTITA_FITO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito is less than or equal to DEFAULT_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.lessThanOrEqual=" + DEFAULT_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito is less than or equal to SMALLER_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.lessThanOrEqual=" + SMALLER_QUANTITA_FITO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito is less than DEFAULT_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.lessThan=" + DEFAULT_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito is less than UPDATED_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.lessThan=" + UPDATED_QUANTITA_FITO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByQuantitaFitoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where quantitaFito is greater than DEFAULT_QUANTITA_FITO
        defaultRegOpcFitoProdShouldNotBeFound("quantitaFito.greaterThan=" + DEFAULT_QUANTITA_FITO);

        // Get all the regOpcFitoProdList where quantitaFito is greater than SMALLER_QUANTITA_FITO
        defaultRegOpcFitoProdShouldBeFound("quantitaFito.greaterThan=" + SMALLER_QUANTITA_FITO);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco equals to DEFAULT_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.equals=" + DEFAULT_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco equals to UPDATED_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.equals=" + UPDATED_ID_FITOFARMACO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco in DEFAULT_ID_FITOFARMACO or UPDATED_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.in=" + DEFAULT_ID_FITOFARMACO + "," + UPDATED_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco equals to UPDATED_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.in=" + UPDATED_ID_FITOFARMACO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco is not null
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.specified=true");

        // Get all the regOpcFitoProdList where idFitofarmaco is null
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco is greater than or equal to DEFAULT_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.greaterThanOrEqual=" + DEFAULT_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco is greater than or equal to UPDATED_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.greaterThanOrEqual=" + UPDATED_ID_FITOFARMACO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco is less than or equal to DEFAULT_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.lessThanOrEqual=" + DEFAULT_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco is less than or equal to SMALLER_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.lessThanOrEqual=" + SMALLER_ID_FITOFARMACO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco is less than DEFAULT_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.lessThan=" + DEFAULT_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco is less than UPDATED_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.lessThan=" + UPDATED_ID_FITOFARMACO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdFitofarmacoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idFitofarmaco is greater than DEFAULT_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldNotBeFound("idFitofarmaco.greaterThan=" + DEFAULT_ID_FITOFARMACO);

        // Get all the regOpcFitoProdList where idFitofarmaco is greater than SMALLER_ID_FITOFARMACO
        defaultRegOpcFitoProdShouldBeFound("idFitofarmaco.greaterThan=" + SMALLER_ID_FITOFARMACO);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino equals to DEFAULT_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.equals=" + DEFAULT_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino equals to UPDATED_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.equals=" + UPDATED_ID_MAGAZZINO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino in DEFAULT_ID_MAGAZZINO or UPDATED_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.in=" + DEFAULT_ID_MAGAZZINO + "," + UPDATED_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino equals to UPDATED_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.in=" + UPDATED_ID_MAGAZZINO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino is not null
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.specified=true");

        // Get all the regOpcFitoProdList where idMagazzino is null
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino is greater than or equal to DEFAULT_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.greaterThanOrEqual=" + DEFAULT_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino is greater than or equal to UPDATED_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.greaterThanOrEqual=" + UPDATED_ID_MAGAZZINO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino is less than or equal to DEFAULT_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.lessThanOrEqual=" + DEFAULT_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino is less than or equal to SMALLER_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.lessThanOrEqual=" + SMALLER_ID_MAGAZZINO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino is less than DEFAULT_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.lessThan=" + DEFAULT_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino is less than UPDATED_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.lessThan=" + UPDATED_ID_MAGAZZINO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdMagazzinoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idMagazzino is greater than DEFAULT_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldNotBeFound("idMagazzino.greaterThan=" + DEFAULT_ID_MAGAZZINO);

        // Get all the regOpcFitoProdList where idMagazzino is greater than SMALLER_ID_MAGAZZINO
        defaultRegOpcFitoProdShouldBeFound("idMagazzino.greaterThan=" + SMALLER_ID_MAGAZZINO);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita equals to DEFAULT_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.equals=" + DEFAULT_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita equals to UPDATED_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.equals=" + UPDATED_ID_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita in DEFAULT_ID_AVVERSITA or UPDATED_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.in=" + DEFAULT_ID_AVVERSITA + "," + UPDATED_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita equals to UPDATED_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.in=" + UPDATED_ID_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita is not null
        defaultRegOpcFitoProdShouldBeFound("idAvversita.specified=true");

        // Get all the regOpcFitoProdList where idAvversita is null
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita is greater than or equal to DEFAULT_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.greaterThanOrEqual=" + DEFAULT_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita is greater than or equal to UPDATED_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.greaterThanOrEqual=" + UPDATED_ID_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita is less than or equal to DEFAULT_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.lessThanOrEqual=" + DEFAULT_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita is less than or equal to SMALLER_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.lessThanOrEqual=" + SMALLER_ID_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita is less than DEFAULT_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.lessThan=" + DEFAULT_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita is less than UPDATED_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.lessThan=" + UPDATED_ID_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByIdAvversitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where idAvversita is greater than DEFAULT_ID_AVVERSITA
        defaultRegOpcFitoProdShouldNotBeFound("idAvversita.greaterThan=" + DEFAULT_ID_AVVERSITA);

        // Get all the regOpcFitoProdList where idAvversita is greater than SMALLER_ID_AVVERSITA
        defaultRegOpcFitoProdShouldBeFound("idAvversita.greaterThan=" + SMALLER_ID_AVVERSITA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali is not null
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcFitoProdList where dataInizVali is null
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoProdList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoProdShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali is not null
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcFitoProdList where dataFineVali is null
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoProdList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoProdShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator is not null
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcFitoProdList where userIdCreator is null
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoProdList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoProdShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod is not null
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcFitoProdList where userIdLastMod is null
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);

        // Get all the regOpcFitoProdList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoProdList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoProdShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoProdsByProdToOpcFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);
        RegOpcFito prodToOpcFito = RegOpcFitoResourceIT.createEntity(em);
        em.persist(prodToOpcFito);
        em.flush();
        regOpcFitoProd.setProdToOpcFito(prodToOpcFito);
        regOpcFitoProdRepository.saveAndFlush(regOpcFitoProd);
        Long prodToOpcFitoId = prodToOpcFito.getId();

        // Get all the regOpcFitoProdList where prodToOpcFito equals to prodToOpcFitoId
        defaultRegOpcFitoProdShouldBeFound("prodToOpcFitoId.equals=" + prodToOpcFitoId);

        // Get all the regOpcFitoProdList where prodToOpcFito equals to prodToOpcFitoId + 1
        defaultRegOpcFitoProdShouldNotBeFound("prodToOpcFitoId.equals=" + (prodToOpcFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcFitoProdShouldBeFound(String filter) throws Exception {
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFitoProd.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantitaFito").value(hasItem(DEFAULT_QUANTITA_FITO.doubleValue())))
            .andExpect(jsonPath("$.[*].idFitofarmaco").value(hasItem(DEFAULT_ID_FITOFARMACO.intValue())))
            .andExpect(jsonPath("$.[*].idMagazzino").value(hasItem(DEFAULT_ID_MAGAZZINO.intValue())))
            .andExpect(jsonPath("$.[*].idAvversita").value(hasItem(DEFAULT_ID_AVVERSITA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcFitoProdShouldNotBeFound(String filter) throws Exception {
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcFitoProd() throws Exception {
        // Get the regOpcFitoProd
        restRegOpcFitoProdMockMvc.perform(get("/api/reg-opc-fito-prods/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcFitoProd() throws Exception {
        // Initialize the database
        regOpcFitoProdService.save(regOpcFitoProd);

        int databaseSizeBeforeUpdate = regOpcFitoProdRepository.findAll().size();

        // Update the regOpcFitoProd
        RegOpcFitoProd updatedRegOpcFitoProd = regOpcFitoProdRepository.findById(regOpcFitoProd.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcFitoProd are not directly saved in db
        em.detach(updatedRegOpcFitoProd);
        updatedRegOpcFitoProd
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantitaFito(UPDATED_QUANTITA_FITO)
            .idFitofarmaco(UPDATED_ID_FITOFARMACO)
            .idMagazzino(UPDATED_ID_MAGAZZINO)
            .idAvversita(UPDATED_ID_AVVERSITA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcFitoProdMockMvc.perform(put("/api/reg-opc-fito-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcFitoProd)))
            .andExpect(status().isOk());

        // Validate the RegOpcFitoProd in the database
        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeUpdate);
        RegOpcFitoProd testRegOpcFitoProd = regOpcFitoProdList.get(regOpcFitoProdList.size() - 1);
        assertThat(testRegOpcFitoProd.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcFitoProd.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcFitoProd.getQuantitaFito()).isEqualTo(UPDATED_QUANTITA_FITO);
        assertThat(testRegOpcFitoProd.getIdFitofarmaco()).isEqualTo(UPDATED_ID_FITOFARMACO);
        assertThat(testRegOpcFitoProd.getIdMagazzino()).isEqualTo(UPDATED_ID_MAGAZZINO);
        assertThat(testRegOpcFitoProd.getIdAvversita()).isEqualTo(UPDATED_ID_AVVERSITA);
        assertThat(testRegOpcFitoProd.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcFitoProd.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcFitoProd.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcFitoProd.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcFitoProd.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcFitoProd() throws Exception {
        int databaseSizeBeforeUpdate = regOpcFitoProdRepository.findAll().size();

        // Create the RegOpcFitoProd

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcFitoProdMockMvc.perform(put("/api/reg-opc-fito-prods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoProd)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFitoProd in the database
        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcFitoProd() throws Exception {
        // Initialize the database
        regOpcFitoProdService.save(regOpcFitoProd);

        int databaseSizeBeforeDelete = regOpcFitoProdRepository.findAll().size();

        // Delete the regOpcFitoProd
        restRegOpcFitoProdMockMvc.perform(delete("/api/reg-opc-fito-prods/{id}", regOpcFitoProd.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcFitoProd> regOpcFitoProdList = regOpcFitoProdRepository.findAll();
        assertThat(regOpcFitoProdList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcFitoProd.class);
        RegOpcFitoProd regOpcFitoProd1 = new RegOpcFitoProd();
        regOpcFitoProd1.setId(1L);
        RegOpcFitoProd regOpcFitoProd2 = new RegOpcFitoProd();
        regOpcFitoProd2.setId(regOpcFitoProd1.getId());
        assertThat(regOpcFitoProd1).isEqualTo(regOpcFitoProd2);
        regOpcFitoProd2.setId(2L);
        assertThat(regOpcFitoProd1).isNotEqualTo(regOpcFitoProd2);
        regOpcFitoProd1.setId(null);
        assertThat(regOpcFitoProd1).isNotEqualTo(regOpcFitoProd2);
    }
}
