package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcConc;
import com.terrambe.domain.RegOpcConcAppz;
import com.terrambe.domain.RegOpcConcProd;
import com.terrambe.domain.MotivoConc;
import com.terrambe.domain.TipoDistConc;
import com.terrambe.repository.RegOpcConcRepository;
import com.terrambe.service.RegOpcConcService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcConcCriteria;
import com.terrambe.service.RegOpcConcQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcConcResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcConcResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Float DEFAULT_VOL_ACQUA_FERT_IRR = 1F;
    private static final Float UPDATED_VOL_ACQUA_FERT_IRR = 2F;
    private static final Float SMALLER_VOL_ACQUA_FERT_IRR = 1F - 1F;

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcConcRepository regOpcConcRepository;

    @Autowired
    private RegOpcConcService regOpcConcService;

    @Autowired
    private RegOpcConcQueryService regOpcConcQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcConcMockMvc;

    private RegOpcConc regOpcConc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcConcResource regOpcConcResource = new RegOpcConcResource(regOpcConcService, regOpcConcQueryService);
        this.restRegOpcConcMockMvc = MockMvcBuilders.standaloneSetup(regOpcConcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConc createEntity(EntityManager em) {
        RegOpcConc regOpcConc = new RegOpcConc()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .volAcquaFertIrr(DEFAULT_VOL_ACQUA_FERT_IRR)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcConc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConc createUpdatedEntity(EntityManager em) {
        RegOpcConc regOpcConc = new RegOpcConc()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volAcquaFertIrr(UPDATED_VOL_ACQUA_FERT_IRR)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcConc;
    }

    @BeforeEach
    public void initTest() {
        regOpcConc = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcConc() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcRepository.findAll().size();

        // Create the RegOpcConc
        restRegOpcConcMockMvc.perform(post("/api/reg-opc-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConc)))
            .andExpect(status().isCreated());

        // Validate the RegOpcConc in the database
        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcConc testRegOpcConc = regOpcConcList.get(regOpcConcList.size() - 1);
        assertThat(testRegOpcConc.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcConc.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcConc.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcConc.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcConc.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcConc.getVolAcquaFertIrr()).isEqualTo(DEFAULT_VOL_ACQUA_FERT_IRR);
        assertThat(testRegOpcConc.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcConc.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcConc.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcConc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcConc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcConc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcConc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcConc.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcConcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcRepository.findAll().size();

        // Create the RegOpcConc with an existing ID
        regOpcConc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcConcMockMvc.perform(post("/api/reg-opc-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConc)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConc in the database
        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcConcRepository.findAll().size();
        // set the field null
        regOpcConc.setIdAzienda(null);

        // Create the RegOpcConc, which fails.

        restRegOpcConcMockMvc.perform(post("/api/reg-opc-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConc)))
            .andExpect(status().isBadRequest());

        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcs() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].volAcquaFertIrr").value(hasItem(DEFAULT_VOL_ACQUA_FERT_IRR.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcConc() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get the regOpcConc
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs/{id}", regOpcConc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcConc.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.volAcquaFertIrr").value(DEFAULT_VOL_ACQUA_FERT_IRR.doubleValue()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda is not null
        defaultRegOpcConcShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcConcList where idAzienda is null
        defaultRegOpcConcShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcConcShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcConcShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd is not null
        defaultRegOpcConcShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcConcList where idUnitaProd is null
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcConcShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc is not null
        defaultRegOpcConcShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcConcList where dataInizOpc is null
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcConcShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcConcList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcConcShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc is not null
        defaultRegOpcConcShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcConcList where dataFineOpc is null
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcConcShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcConcList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcConcShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcConcShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcConcList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcConcShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcConcShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcConcList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcConcShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where tempoImpiegato is not null
        defaultRegOpcConcShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcConcList where tempoImpiegato is null
        defaultRegOpcConcShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr equals to DEFAULT_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.equals=" + DEFAULT_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr equals to UPDATED_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.equals=" + UPDATED_VOL_ACQUA_FERT_IRR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr in DEFAULT_VOL_ACQUA_FERT_IRR or UPDATED_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.in=" + DEFAULT_VOL_ACQUA_FERT_IRR + "," + UPDATED_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr equals to UPDATED_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.in=" + UPDATED_VOL_ACQUA_FERT_IRR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr is not null
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.specified=true");

        // Get all the regOpcConcList where volAcquaFertIrr is null
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr is greater than or equal to DEFAULT_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.greaterThanOrEqual=" + DEFAULT_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr is greater than or equal to UPDATED_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.greaterThanOrEqual=" + UPDATED_VOL_ACQUA_FERT_IRR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr is less than or equal to DEFAULT_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.lessThanOrEqual=" + DEFAULT_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr is less than or equal to SMALLER_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.lessThanOrEqual=" + SMALLER_VOL_ACQUA_FERT_IRR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr is less than DEFAULT_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.lessThan=" + DEFAULT_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr is less than UPDATED_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.lessThan=" + UPDATED_VOL_ACQUA_FERT_IRR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByVolAcquaFertIrrIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where volAcquaFertIrr is greater than DEFAULT_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldNotBeFound("volAcquaFertIrr.greaterThan=" + DEFAULT_VOL_ACQUA_FERT_IRR);

        // Get all the regOpcConcList where volAcquaFertIrr is greater than SMALLER_VOL_ACQUA_FERT_IRR
        defaultRegOpcConcShouldBeFound("volAcquaFertIrr.greaterThan=" + SMALLER_VOL_ACQUA_FERT_IRR);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcConcList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcConcList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura is not null
        defaultRegOpcConcShouldBeFound("idColtura.specified=true");

        // Get all the regOpcConcList where idColtura is null
        defaultRegOpcConcShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcConcList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcConcList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcConcList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcConcShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcConcList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcConcShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore is not null
        defaultRegOpcConcShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcConcList where idOperatore is null
        defaultRegOpcConcShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcConcShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcConcList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcConcShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo is not null
        defaultRegOpcConcShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcConcList where idMezzo is null
        defaultRegOpcConcShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcConcShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcConcList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcConcShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali is not null
        defaultRegOpcConcShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcConcList where dataInizVali is null
        defaultRegOpcConcShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali is not null
        defaultRegOpcConcShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcConcList where dataFineVali is null
        defaultRegOpcConcShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcConcShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator is not null
        defaultRegOpcConcShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcConcList where userIdCreator is null
        defaultRegOpcConcShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcConcShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod is not null
        defaultRegOpcConcShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcConcList where userIdLastMod is null
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);

        // Get all the regOpcConcList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByOpcConcToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);
        RegOpcConcAppz opcConcToAppz = RegOpcConcAppzResourceIT.createEntity(em);
        em.persist(opcConcToAppz);
        em.flush();
        regOpcConc.addOpcConcToAppz(opcConcToAppz);
        regOpcConcRepository.saveAndFlush(regOpcConc);
        Long opcConcToAppzId = opcConcToAppz.getId();

        // Get all the regOpcConcList where opcConcToAppz equals to opcConcToAppzId
        defaultRegOpcConcShouldBeFound("opcConcToAppzId.equals=" + opcConcToAppzId);

        // Get all the regOpcConcList where opcConcToAppz equals to opcConcToAppzId + 1
        defaultRegOpcConcShouldNotBeFound("opcConcToAppzId.equals=" + (opcConcToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByOpcConcToProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);
        RegOpcConcProd opcConcToProd = RegOpcConcProdResourceIT.createEntity(em);
        em.persist(opcConcToProd);
        em.flush();
        regOpcConc.addOpcConcToProd(opcConcToProd);
        regOpcConcRepository.saveAndFlush(regOpcConc);
        Long opcConcToProdId = opcConcToProd.getId();

        // Get all the regOpcConcList where opcConcToProd equals to opcConcToProdId
        defaultRegOpcConcShouldBeFound("opcConcToProdId.equals=" + opcConcToProdId);

        // Get all the regOpcConcList where opcConcToProd equals to opcConcToProdId + 1
        defaultRegOpcConcShouldNotBeFound("opcConcToProdId.equals=" + (opcConcToProdId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByOpcConcToMotivoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);
        MotivoConc opcConcToMotivo = MotivoConcResourceIT.createEntity(em);
        em.persist(opcConcToMotivo);
        em.flush();
        regOpcConc.setOpcConcToMotivo(opcConcToMotivo);
        regOpcConcRepository.saveAndFlush(regOpcConc);
        Long opcConcToMotivoId = opcConcToMotivo.getId();

        // Get all the regOpcConcList where opcConcToMotivo equals to opcConcToMotivoId
        defaultRegOpcConcShouldBeFound("opcConcToMotivoId.equals=" + opcConcToMotivoId);

        // Get all the regOpcConcList where opcConcToMotivo equals to opcConcToMotivoId + 1
        defaultRegOpcConcShouldNotBeFound("opcConcToMotivoId.equals=" + (opcConcToMotivoId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcConcsByOpcConcToTipoDistIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcRepository.saveAndFlush(regOpcConc);
        TipoDistConc opcConcToTipoDist = TipoDistConcResourceIT.createEntity(em);
        em.persist(opcConcToTipoDist);
        em.flush();
        regOpcConc.setOpcConcToTipoDist(opcConcToTipoDist);
        regOpcConcRepository.saveAndFlush(regOpcConc);
        Long opcConcToTipoDistId = opcConcToTipoDist.getId();

        // Get all the regOpcConcList where opcConcToTipoDist equals to opcConcToTipoDistId
        defaultRegOpcConcShouldBeFound("opcConcToTipoDistId.equals=" + opcConcToTipoDistId);

        // Get all the regOpcConcList where opcConcToTipoDist equals to opcConcToTipoDistId + 1
        defaultRegOpcConcShouldNotBeFound("opcConcToTipoDistId.equals=" + (opcConcToTipoDistId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcConcShouldBeFound(String filter) throws Exception {
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].volAcquaFertIrr").value(hasItem(DEFAULT_VOL_ACQUA_FERT_IRR.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcConcShouldNotBeFound(String filter) throws Exception {
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcConc() throws Exception {
        // Get the regOpcConc
        restRegOpcConcMockMvc.perform(get("/api/reg-opc-concs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcConc() throws Exception {
        // Initialize the database
        regOpcConcService.save(regOpcConc);

        int databaseSizeBeforeUpdate = regOpcConcRepository.findAll().size();

        // Update the regOpcConc
        RegOpcConc updatedRegOpcConc = regOpcConcRepository.findById(regOpcConc.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcConc are not directly saved in db
        em.detach(updatedRegOpcConc);
        updatedRegOpcConc
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volAcquaFertIrr(UPDATED_VOL_ACQUA_FERT_IRR)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcConcMockMvc.perform(put("/api/reg-opc-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcConc)))
            .andExpect(status().isOk());

        // Validate the RegOpcConc in the database
        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeUpdate);
        RegOpcConc testRegOpcConc = regOpcConcList.get(regOpcConcList.size() - 1);
        assertThat(testRegOpcConc.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcConc.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcConc.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcConc.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcConc.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcConc.getVolAcquaFertIrr()).isEqualTo(UPDATED_VOL_ACQUA_FERT_IRR);
        assertThat(testRegOpcConc.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcConc.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcConc.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcConc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcConc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcConc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcConc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcConc.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcConc() throws Exception {
        int databaseSizeBeforeUpdate = regOpcConcRepository.findAll().size();

        // Create the RegOpcConc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcConcMockMvc.perform(put("/api/reg-opc-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConc)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConc in the database
        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcConc() throws Exception {
        // Initialize the database
        regOpcConcService.save(regOpcConc);

        int databaseSizeBeforeDelete = regOpcConcRepository.findAll().size();

        // Delete the regOpcConc
        restRegOpcConcMockMvc.perform(delete("/api/reg-opc-concs/{id}", regOpcConc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcConc> regOpcConcList = regOpcConcRepository.findAll();
        assertThat(regOpcConcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcConc.class);
        RegOpcConc regOpcConc1 = new RegOpcConc();
        regOpcConc1.setId(1L);
        RegOpcConc regOpcConc2 = new RegOpcConc();
        regOpcConc2.setId(regOpcConc1.getId());
        assertThat(regOpcConc1).isEqualTo(regOpcConc2);
        regOpcConc2.setId(2L);
        assertThat(regOpcConc1).isNotEqualTo(regOpcConc2);
        regOpcConc1.setId(null);
        assertThat(regOpcConc1).isNotEqualTo(regOpcConc2);
    }
}
