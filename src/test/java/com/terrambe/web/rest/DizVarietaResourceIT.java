package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizVarieta;
import com.terrambe.repository.DizVarietaRepository;
import com.terrambe.service.DizVarietaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizVarietaCriteria;
import com.terrambe.service.DizVarietaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizVarietaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizVarietaResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_COD_OCCUPAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_COD_OCCUPAZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizVarietaRepository dizVarietaRepository;

    @Autowired
    private DizVarietaService dizVarietaService;

    @Autowired
    private DizVarietaQueryService dizVarietaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizVarietaMockMvc;

    private DizVarieta dizVarieta;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizVarietaResource dizVarietaResource = new DizVarietaResource(dizVarietaService, dizVarietaQueryService);
        this.restDizVarietaMockMvc = MockMvcBuilders.standaloneSetup(dizVarietaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizVarieta createEntity(EntityManager em) {
        DizVarieta dizVarieta = new DizVarieta()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .codOccupazione(DEFAULT_COD_OCCUPAZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizVarieta;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizVarieta createUpdatedEntity(EntityManager em) {
        DizVarieta dizVarieta = new DizVarieta()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .codOccupazione(UPDATED_COD_OCCUPAZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizVarieta;
    }

    @BeforeEach
    public void initTest() {
        dizVarieta = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizVarieta() throws Exception {
        int databaseSizeBeforeCreate = dizVarietaRepository.findAll().size();

        // Create the DizVarieta
        restDizVarietaMockMvc.perform(post("/api/diz-varietas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizVarieta)))
            .andExpect(status().isCreated());

        // Validate the DizVarieta in the database
        List<DizVarieta> dizVarietaList = dizVarietaRepository.findAll();
        assertThat(dizVarietaList).hasSize(databaseSizeBeforeCreate + 1);
        DizVarieta testDizVarieta = dizVarietaList.get(dizVarietaList.size() - 1);
        assertThat(testDizVarieta.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizVarieta.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizVarieta.getCodOccupazione()).isEqualTo(DEFAULT_COD_OCCUPAZIONE);
        assertThat(testDizVarieta.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizVarieta.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizVarieta.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizVarieta.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizVarieta.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizVarietaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizVarietaRepository.findAll().size();

        // Create the DizVarieta with an existing ID
        dizVarieta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizVarietaMockMvc.perform(post("/api/diz-varietas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizVarieta)))
            .andExpect(status().isBadRequest());

        // Validate the DizVarieta in the database
        List<DizVarieta> dizVarietaList = dizVarietaRepository.findAll();
        assertThat(dizVarietaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizVarietas() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList
        restDizVarietaMockMvc.perform(get("/api/diz-varietas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizVarieta.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].codOccupazione").value(hasItem(DEFAULT_COD_OCCUPAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizVarieta() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get the dizVarieta
        restDizVarietaMockMvc.perform(get("/api/diz-varietas/{id}", dizVarieta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizVarieta.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.codOccupazione").value(DEFAULT_COD_OCCUPAZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codice equals to DEFAULT_CODICE
        defaultDizVarietaShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizVarietaList where codice equals to UPDATED_CODICE
        defaultDizVarietaShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizVarietaShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizVarietaList where codice equals to UPDATED_CODICE
        defaultDizVarietaShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codice is not null
        defaultDizVarietaShouldBeFound("codice.specified=true");

        // Get all the dizVarietaList where codice is null
        defaultDizVarietaShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizVarietaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizVarietaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizVarietaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizVarietaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizVarietaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizVarietaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where descrizione is not null
        defaultDizVarietaShouldBeFound("descrizione.specified=true");

        // Get all the dizVarietaList where descrizione is null
        defaultDizVarietaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodOccupazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codOccupazione equals to DEFAULT_COD_OCCUPAZIONE
        defaultDizVarietaShouldBeFound("codOccupazione.equals=" + DEFAULT_COD_OCCUPAZIONE);

        // Get all the dizVarietaList where codOccupazione equals to UPDATED_COD_OCCUPAZIONE
        defaultDizVarietaShouldNotBeFound("codOccupazione.equals=" + UPDATED_COD_OCCUPAZIONE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodOccupazioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codOccupazione in DEFAULT_COD_OCCUPAZIONE or UPDATED_COD_OCCUPAZIONE
        defaultDizVarietaShouldBeFound("codOccupazione.in=" + DEFAULT_COD_OCCUPAZIONE + "," + UPDATED_COD_OCCUPAZIONE);

        // Get all the dizVarietaList where codOccupazione equals to UPDATED_COD_OCCUPAZIONE
        defaultDizVarietaShouldNotBeFound("codOccupazione.in=" + UPDATED_COD_OCCUPAZIONE);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByCodOccupazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where codOccupazione is not null
        defaultDizVarietaShouldBeFound("codOccupazione.specified=true");

        // Get all the dizVarietaList where codOccupazione is null
        defaultDizVarietaShouldNotBeFound("codOccupazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali is not null
        defaultDizVarietaShouldBeFound("dataInizVali.specified=true");

        // Get all the dizVarietaList where dataInizVali is null
        defaultDizVarietaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizVarietaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizVarietaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizVarietaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali is not null
        defaultDizVarietaShouldBeFound("dataFineVali.specified=true");

        // Get all the dizVarietaList where dataFineVali is null
        defaultDizVarietaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizVarietaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizVarietaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizVarietaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator is not null
        defaultDizVarietaShouldBeFound("userIdCreator.specified=true");

        // Get all the dizVarietaList where userIdCreator is null
        defaultDizVarietaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizVarietaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizVarietaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizVarietaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod is not null
        defaultDizVarietaShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizVarietaList where userIdLastMod is null
        defaultDizVarietaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizVarietasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizVarietaRepository.saveAndFlush(dizVarieta);

        // Get all the dizVarietaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizVarietaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizVarietaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizVarietaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizVarietaShouldBeFound(String filter) throws Exception {
        restDizVarietaMockMvc.perform(get("/api/diz-varietas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizVarieta.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].codOccupazione").value(hasItem(DEFAULT_COD_OCCUPAZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizVarietaMockMvc.perform(get("/api/diz-varietas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizVarietaShouldNotBeFound(String filter) throws Exception {
        restDizVarietaMockMvc.perform(get("/api/diz-varietas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizVarietaMockMvc.perform(get("/api/diz-varietas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizVarieta() throws Exception {
        // Get the dizVarieta
        restDizVarietaMockMvc.perform(get("/api/diz-varietas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizVarieta() throws Exception {
        // Initialize the database
        dizVarietaService.save(dizVarieta);

        int databaseSizeBeforeUpdate = dizVarietaRepository.findAll().size();

        // Update the dizVarieta
        DizVarieta updatedDizVarieta = dizVarietaRepository.findById(dizVarieta.getId()).get();
        // Disconnect from session so that the updates on updatedDizVarieta are not directly saved in db
        em.detach(updatedDizVarieta);
        updatedDizVarieta
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .codOccupazione(UPDATED_COD_OCCUPAZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizVarietaMockMvc.perform(put("/api/diz-varietas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizVarieta)))
            .andExpect(status().isOk());

        // Validate the DizVarieta in the database
        List<DizVarieta> dizVarietaList = dizVarietaRepository.findAll();
        assertThat(dizVarietaList).hasSize(databaseSizeBeforeUpdate);
        DizVarieta testDizVarieta = dizVarietaList.get(dizVarietaList.size() - 1);
        assertThat(testDizVarieta.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizVarieta.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizVarieta.getCodOccupazione()).isEqualTo(UPDATED_COD_OCCUPAZIONE);
        assertThat(testDizVarieta.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizVarieta.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizVarieta.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizVarieta.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizVarieta.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizVarieta() throws Exception {
        int databaseSizeBeforeUpdate = dizVarietaRepository.findAll().size();

        // Create the DizVarieta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizVarietaMockMvc.perform(put("/api/diz-varietas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizVarieta)))
            .andExpect(status().isBadRequest());

        // Validate the DizVarieta in the database
        List<DizVarieta> dizVarietaList = dizVarietaRepository.findAll();
        assertThat(dizVarietaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizVarieta() throws Exception {
        // Initialize the database
        dizVarietaService.save(dizVarieta);

        int databaseSizeBeforeDelete = dizVarietaRepository.findAll().size();

        // Delete the dizVarieta
        restDizVarietaMockMvc.perform(delete("/api/diz-varietas/{id}", dizVarieta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizVarieta> dizVarietaList = dizVarietaRepository.findAll();
        assertThat(dizVarietaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizVarieta.class);
        DizVarieta dizVarieta1 = new DizVarieta();
        dizVarieta1.setId(1L);
        DizVarieta dizVarieta2 = new DizVarieta();
        dizVarieta2.setId(dizVarieta1.getId());
        assertThat(dizVarieta1).isEqualTo(dizVarieta2);
        dizVarieta2.setId(2L);
        assertThat(dizVarieta1).isNotEqualTo(dizVarieta2);
        dizVarieta1.setId(null);
        assertThat(dizVarieta1).isNotEqualTo(dizVarieta2);
    }
}
