package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcFitoAppz;
import com.terrambe.domain.RegOpcFito;
import com.terrambe.repository.RegOpcFitoAppzRepository;
import com.terrambe.service.RegOpcFitoAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcFitoAppzCriteria;
import com.terrambe.service.RegOpcFitoAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcFitoAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcFitoAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcFitoAppzRepository regOpcFitoAppzRepository;

    @Autowired
    private RegOpcFitoAppzService regOpcFitoAppzService;

    @Autowired
    private RegOpcFitoAppzQueryService regOpcFitoAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcFitoAppzMockMvc;

    private RegOpcFitoAppz regOpcFitoAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcFitoAppzResource regOpcFitoAppzResource = new RegOpcFitoAppzResource(regOpcFitoAppzService, regOpcFitoAppzQueryService);
        this.restRegOpcFitoAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcFitoAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFitoAppz createEntity(EntityManager em) {
        RegOpcFitoAppz regOpcFitoAppz = new RegOpcFitoAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcFitoAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFitoAppz createUpdatedEntity(EntityManager em) {
        RegOpcFitoAppz regOpcFitoAppz = new RegOpcFitoAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcFitoAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcFitoAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcFitoAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoAppzRepository.findAll().size();

        // Create the RegOpcFitoAppz
        restRegOpcFitoAppzMockMvc.perform(post("/api/reg-opc-fito-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcFitoAppz in the database
        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcFitoAppz testRegOpcFitoAppz = regOpcFitoAppzList.get(regOpcFitoAppzList.size() - 1);
        assertThat(testRegOpcFitoAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcFitoAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcFitoAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcFitoAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcFitoAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcFitoAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcFitoAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcFitoAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcFitoAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoAppzRepository.findAll().size();

        // Create the RegOpcFitoAppz with an existing ID
        regOpcFitoAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcFitoAppzMockMvc.perform(post("/api/reg-opc-fito-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFitoAppz in the database
        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcFitoAppzRepository.findAll().size();
        // set the field null
        regOpcFitoAppz.setIdAzienda(null);

        // Create the RegOpcFitoAppz, which fails.

        restRegOpcFitoAppzMockMvc.perform(post("/api/reg-opc-fito-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzs() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFitoAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcFitoAppz() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get the regOpcFitoAppz
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs/{id}", regOpcFitoAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcFitoAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda is not null
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcFitoAppzList where idAzienda is null
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcFitoAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd is not null
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcFitoAppzList where idUnitaProd is null
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata is not null
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcFitoAppzList where percentLavorata is null
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcFitoAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcFitoAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento is not null
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcFitoAppzList where idAppezzamento is null
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcFitoAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcFitoAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali is not null
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcFitoAppzList where dataInizVali is null
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali is not null
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcFitoAppzList where dataFineVali is null
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator is not null
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcFitoAppzList where userIdCreator is null
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod is not null
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcFitoAppzList where userIdLastMod is null
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);

        // Get all the regOpcFitoAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitoAppzsByAppzToOpcFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);
        RegOpcFito appzToOpcFito = RegOpcFitoResourceIT.createEntity(em);
        em.persist(appzToOpcFito);
        em.flush();
        regOpcFitoAppz.setAppzToOpcFito(appzToOpcFito);
        regOpcFitoAppzRepository.saveAndFlush(regOpcFitoAppz);
        Long appzToOpcFitoId = appzToOpcFito.getId();

        // Get all the regOpcFitoAppzList where appzToOpcFito equals to appzToOpcFitoId
        defaultRegOpcFitoAppzShouldBeFound("appzToOpcFitoId.equals=" + appzToOpcFitoId);

        // Get all the regOpcFitoAppzList where appzToOpcFito equals to appzToOpcFitoId + 1
        defaultRegOpcFitoAppzShouldNotBeFound("appzToOpcFitoId.equals=" + (appzToOpcFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcFitoAppzShouldBeFound(String filter) throws Exception {
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFitoAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcFitoAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcFitoAppz() throws Exception {
        // Get the regOpcFitoAppz
        restRegOpcFitoAppzMockMvc.perform(get("/api/reg-opc-fito-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcFitoAppz() throws Exception {
        // Initialize the database
        regOpcFitoAppzService.save(regOpcFitoAppz);

        int databaseSizeBeforeUpdate = regOpcFitoAppzRepository.findAll().size();

        // Update the regOpcFitoAppz
        RegOpcFitoAppz updatedRegOpcFitoAppz = regOpcFitoAppzRepository.findById(regOpcFitoAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcFitoAppz are not directly saved in db
        em.detach(updatedRegOpcFitoAppz);
        updatedRegOpcFitoAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcFitoAppzMockMvc.perform(put("/api/reg-opc-fito-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcFitoAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcFitoAppz in the database
        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcFitoAppz testRegOpcFitoAppz = regOpcFitoAppzList.get(regOpcFitoAppzList.size() - 1);
        assertThat(testRegOpcFitoAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcFitoAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcFitoAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcFitoAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcFitoAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcFitoAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcFitoAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcFitoAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcFitoAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcFitoAppzRepository.findAll().size();

        // Create the RegOpcFitoAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcFitoAppzMockMvc.perform(put("/api/reg-opc-fito-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFitoAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFitoAppz in the database
        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcFitoAppz() throws Exception {
        // Initialize the database
        regOpcFitoAppzService.save(regOpcFitoAppz);

        int databaseSizeBeforeDelete = regOpcFitoAppzRepository.findAll().size();

        // Delete the regOpcFitoAppz
        restRegOpcFitoAppzMockMvc.perform(delete("/api/reg-opc-fito-appzs/{id}", regOpcFitoAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcFitoAppz> regOpcFitoAppzList = regOpcFitoAppzRepository.findAll();
        assertThat(regOpcFitoAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcFitoAppz.class);
        RegOpcFitoAppz regOpcFitoAppz1 = new RegOpcFitoAppz();
        regOpcFitoAppz1.setId(1L);
        RegOpcFitoAppz regOpcFitoAppz2 = new RegOpcFitoAppz();
        regOpcFitoAppz2.setId(regOpcFitoAppz1.getId());
        assertThat(regOpcFitoAppz1).isEqualTo(regOpcFitoAppz2);
        regOpcFitoAppz2.setId(2L);
        assertThat(regOpcFitoAppz1).isNotEqualTo(regOpcFitoAppz2);
        regOpcFitoAppz1.setId(null);
        assertThat(regOpcFitoAppz1).isNotEqualTo(regOpcFitoAppz2);
    }
}
