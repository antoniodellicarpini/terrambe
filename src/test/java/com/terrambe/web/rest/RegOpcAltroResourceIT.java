package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcAltro;
import com.terrambe.domain.RegOpcAltroAppz;
import com.terrambe.domain.TipoAltro;
import com.terrambe.repository.RegOpcAltroRepository;
import com.terrambe.service.RegOpcAltroService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcAltroCriteria;
import com.terrambe.service.RegOpcAltroQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcAltroResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcAltroResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcAltroRepository regOpcAltroRepository;

    @Autowired
    private RegOpcAltroService regOpcAltroService;

    @Autowired
    private RegOpcAltroQueryService regOpcAltroQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcAltroMockMvc;

    private RegOpcAltro regOpcAltro;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcAltroResource regOpcAltroResource = new RegOpcAltroResource(regOpcAltroService, regOpcAltroQueryService);
        this.restRegOpcAltroMockMvc = MockMvcBuilders.standaloneSetup(regOpcAltroResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcAltro createEntity(EntityManager em) {
        RegOpcAltro regOpcAltro = new RegOpcAltro()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcAltro;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcAltro createUpdatedEntity(EntityManager em) {
        RegOpcAltro regOpcAltro = new RegOpcAltro()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcAltro;
    }

    @BeforeEach
    public void initTest() {
        regOpcAltro = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcAltro() throws Exception {
        int databaseSizeBeforeCreate = regOpcAltroRepository.findAll().size();

        // Create the RegOpcAltro
        restRegOpcAltroMockMvc.perform(post("/api/reg-opc-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltro)))
            .andExpect(status().isCreated());

        // Validate the RegOpcAltro in the database
        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcAltro testRegOpcAltro = regOpcAltroList.get(regOpcAltroList.size() - 1);
        assertThat(testRegOpcAltro.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcAltro.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcAltro.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcAltro.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcAltro.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcAltro.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcAltro.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcAltro.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcAltro.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcAltro.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcAltro.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcAltro.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcAltro.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcAltroWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcAltroRepository.findAll().size();

        // Create the RegOpcAltro with an existing ID
        regOpcAltro.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcAltroMockMvc.perform(post("/api/reg-opc-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltro)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcAltro in the database
        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcAltroRepository.findAll().size();
        // set the field null
        regOpcAltro.setIdAzienda(null);

        // Create the RegOpcAltro, which fails.

        restRegOpcAltroMockMvc.perform(post("/api/reg-opc-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltro)))
            .andExpect(status().isBadRequest());

        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltros() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcAltro.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcAltro() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get the regOpcAltro
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros/{id}", regOpcAltro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcAltro.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda is not null
        defaultRegOpcAltroShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcAltroList where idAzienda is null
        defaultRegOpcAltroShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcAltroShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcAltroShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd is not null
        defaultRegOpcAltroShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcAltroList where idUnitaProd is null
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcAltroShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc is not null
        defaultRegOpcAltroShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcAltroList where dataInizOpc is null
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcAltroShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcAltroList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcAltroShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc is not null
        defaultRegOpcAltroShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcAltroList where dataFineOpc is null
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcAltroShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcAltroList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcAltroShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcAltroShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcAltroList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcAltroShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcAltroShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcAltroList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcAltroShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where tempoImpiegato is not null
        defaultRegOpcAltroShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcAltroList where tempoImpiegato is null
        defaultRegOpcAltroShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura is not null
        defaultRegOpcAltroShouldBeFound("idColtura.specified=true");

        // Get all the regOpcAltroList where idColtura is null
        defaultRegOpcAltroShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcAltroShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcAltroList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcAltroShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore is not null
        defaultRegOpcAltroShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcAltroList where idOperatore is null
        defaultRegOpcAltroShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcAltroShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcAltroList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcAltroShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo is not null
        defaultRegOpcAltroShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcAltroList where idMezzo is null
        defaultRegOpcAltroShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcAltroShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcAltroList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcAltroShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali is not null
        defaultRegOpcAltroShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcAltroList where dataInizVali is null
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcAltroShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali is not null
        defaultRegOpcAltroShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcAltroList where dataFineVali is null
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcAltroShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator is not null
        defaultRegOpcAltroShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcAltroList where userIdCreator is null
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcAltroShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod is not null
        defaultRegOpcAltroShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcAltroList where userIdLastMod is null
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltrosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);

        // Get all the regOpcAltroList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcAltroShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByOpcAltroToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);
        RegOpcAltroAppz opcAltroToAppz = RegOpcAltroAppzResourceIT.createEntity(em);
        em.persist(opcAltroToAppz);
        em.flush();
        regOpcAltro.addOpcAltroToAppz(opcAltroToAppz);
        regOpcAltroRepository.saveAndFlush(regOpcAltro);
        Long opcAltroToAppzId = opcAltroToAppz.getId();

        // Get all the regOpcAltroList where opcAltroToAppz equals to opcAltroToAppzId
        defaultRegOpcAltroShouldBeFound("opcAltroToAppzId.equals=" + opcAltroToAppzId);

        // Get all the regOpcAltroList where opcAltroToAppz equals to opcAltroToAppzId + 1
        defaultRegOpcAltroShouldNotBeFound("opcAltroToAppzId.equals=" + (opcAltroToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcAltrosByOpcAltroToTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroRepository.saveAndFlush(regOpcAltro);
        TipoAltro opcAltroToTipo = TipoAltroResourceIT.createEntity(em);
        em.persist(opcAltroToTipo);
        em.flush();
        regOpcAltro.setOpcAltroToTipo(opcAltroToTipo);
        regOpcAltroRepository.saveAndFlush(regOpcAltro);
        Long opcAltroToTipoId = opcAltroToTipo.getId();

        // Get all the regOpcAltroList where opcAltroToTipo equals to opcAltroToTipoId
        defaultRegOpcAltroShouldBeFound("opcAltroToTipoId.equals=" + opcAltroToTipoId);

        // Get all the regOpcAltroList where opcAltroToTipo equals to opcAltroToTipoId + 1
        defaultRegOpcAltroShouldNotBeFound("opcAltroToTipoId.equals=" + (opcAltroToTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcAltroShouldBeFound(String filter) throws Exception {
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcAltro.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcAltroShouldNotBeFound(String filter) throws Exception {
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcAltro() throws Exception {
        // Get the regOpcAltro
        restRegOpcAltroMockMvc.perform(get("/api/reg-opc-altros/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcAltro() throws Exception {
        // Initialize the database
        regOpcAltroService.save(regOpcAltro);

        int databaseSizeBeforeUpdate = regOpcAltroRepository.findAll().size();

        // Update the regOpcAltro
        RegOpcAltro updatedRegOpcAltro = regOpcAltroRepository.findById(regOpcAltro.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcAltro are not directly saved in db
        em.detach(updatedRegOpcAltro);
        updatedRegOpcAltro
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcAltroMockMvc.perform(put("/api/reg-opc-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcAltro)))
            .andExpect(status().isOk());

        // Validate the RegOpcAltro in the database
        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeUpdate);
        RegOpcAltro testRegOpcAltro = regOpcAltroList.get(regOpcAltroList.size() - 1);
        assertThat(testRegOpcAltro.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcAltro.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcAltro.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcAltro.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcAltro.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcAltro.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcAltro.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcAltro.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcAltro.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcAltro.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcAltro.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcAltro.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcAltro.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcAltro() throws Exception {
        int databaseSizeBeforeUpdate = regOpcAltroRepository.findAll().size();

        // Create the RegOpcAltro

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcAltroMockMvc.perform(put("/api/reg-opc-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltro)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcAltro in the database
        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcAltro() throws Exception {
        // Initialize the database
        regOpcAltroService.save(regOpcAltro);

        int databaseSizeBeforeDelete = regOpcAltroRepository.findAll().size();

        // Delete the regOpcAltro
        restRegOpcAltroMockMvc.perform(delete("/api/reg-opc-altros/{id}", regOpcAltro.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcAltro> regOpcAltroList = regOpcAltroRepository.findAll();
        assertThat(regOpcAltroList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcAltro.class);
        RegOpcAltro regOpcAltro1 = new RegOpcAltro();
        regOpcAltro1.setId(1L);
        RegOpcAltro regOpcAltro2 = new RegOpcAltro();
        regOpcAltro2.setId(regOpcAltro1.getId());
        assertThat(regOpcAltro1).isEqualTo(regOpcAltro2);
        regOpcAltro2.setId(2L);
        assertThat(regOpcAltro1).isNotEqualTo(regOpcAltro2);
        regOpcAltro1.setId(null);
        assertThat(regOpcAltro1).isNotEqualTo(regOpcAltro2);
    }
}
