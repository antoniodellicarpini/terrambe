package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipologieOpc;
import com.terrambe.repository.TipologieOpcRepository;
import com.terrambe.service.TipologieOpcService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipologieOpcCriteria;
import com.terrambe.service.TipologieOpcQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipologieOpcResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipologieOpcResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipologieOpcRepository tipologieOpcRepository;

    @Autowired
    private TipologieOpcService tipologieOpcService;

    @Autowired
    private TipologieOpcQueryService tipologieOpcQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipologieOpcMockMvc;

    private TipologieOpc tipologieOpc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipologieOpcResource tipologieOpcResource = new TipologieOpcResource(tipologieOpcService, tipologieOpcQueryService);
        this.restTipologieOpcMockMvc = MockMvcBuilders.standaloneSetup(tipologieOpcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologieOpc createEntity(EntityManager em) {
        TipologieOpc tipologieOpc = new TipologieOpc()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipologieOpc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologieOpc createUpdatedEntity(EntityManager em) {
        TipologieOpc tipologieOpc = new TipologieOpc()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipologieOpc;
    }

    @BeforeEach
    public void initTest() {
        tipologieOpc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipologieOpc() throws Exception {
        int databaseSizeBeforeCreate = tipologieOpcRepository.findAll().size();

        // Create the TipologieOpc
        restTipologieOpcMockMvc.perform(post("/api/tipologie-opcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologieOpc)))
            .andExpect(status().isCreated());

        // Validate the TipologieOpc in the database
        List<TipologieOpc> tipologieOpcList = tipologieOpcRepository.findAll();
        assertThat(tipologieOpcList).hasSize(databaseSizeBeforeCreate + 1);
        TipologieOpc testTipologieOpc = tipologieOpcList.get(tipologieOpcList.size() - 1);
        assertThat(testTipologieOpc.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipologieOpc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipologieOpc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipologieOpc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipologieOpc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipologieOpcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipologieOpcRepository.findAll().size();

        // Create the TipologieOpc with an existing ID
        tipologieOpc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipologieOpcMockMvc.perform(post("/api/tipologie-opcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologieOpc)))
            .andExpect(status().isBadRequest());

        // Validate the TipologieOpc in the database
        List<TipologieOpc> tipologieOpcList = tipologieOpcRepository.findAll();
        assertThat(tipologieOpcList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipologieOpcs() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologieOpc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipologieOpc() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get the tipologieOpc
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs/{id}", tipologieOpc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipologieOpc.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipologieOpcShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipologieOpcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologieOpcShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipologieOpcShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipologieOpcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologieOpcShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where descrizione is not null
        defaultTipologieOpcShouldBeFound("descrizione.specified=true");

        // Get all the tipologieOpcList where descrizione is null
        defaultTipologieOpcShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali is not null
        defaultTipologieOpcShouldBeFound("dataInizVali.specified=true");

        // Get all the tipologieOpcList where dataInizVali is null
        defaultTipologieOpcShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipologieOpcShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologieOpcList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipologieOpcShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali is not null
        defaultTipologieOpcShouldBeFound("dataFineVali.specified=true");

        // Get all the tipologieOpcList where dataFineVali is null
        defaultTipologieOpcShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipologieOpcShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologieOpcList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipologieOpcShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator is not null
        defaultTipologieOpcShouldBeFound("userIdCreator.specified=true");

        // Get all the tipologieOpcList where userIdCreator is null
        defaultTipologieOpcShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipologieOpcShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologieOpcList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipologieOpcShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod is not null
        defaultTipologieOpcShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipologieOpcList where userIdLastMod is null
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologieOpcsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologieOpcRepository.saveAndFlush(tipologieOpc);

        // Get all the tipologieOpcList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipologieOpcShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologieOpcList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipologieOpcShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipologieOpcShouldBeFound(String filter) throws Exception {
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologieOpc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipologieOpcShouldNotBeFound(String filter) throws Exception {
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipologieOpc() throws Exception {
        // Get the tipologieOpc
        restTipologieOpcMockMvc.perform(get("/api/tipologie-opcs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipologieOpc() throws Exception {
        // Initialize the database
        tipologieOpcService.save(tipologieOpc);

        int databaseSizeBeforeUpdate = tipologieOpcRepository.findAll().size();

        // Update the tipologieOpc
        TipologieOpc updatedTipologieOpc = tipologieOpcRepository.findById(tipologieOpc.getId()).get();
        // Disconnect from session so that the updates on updatedTipologieOpc are not directly saved in db
        em.detach(updatedTipologieOpc);
        updatedTipologieOpc
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipologieOpcMockMvc.perform(put("/api/tipologie-opcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipologieOpc)))
            .andExpect(status().isOk());

        // Validate the TipologieOpc in the database
        List<TipologieOpc> tipologieOpcList = tipologieOpcRepository.findAll();
        assertThat(tipologieOpcList).hasSize(databaseSizeBeforeUpdate);
        TipologieOpc testTipologieOpc = tipologieOpcList.get(tipologieOpcList.size() - 1);
        assertThat(testTipologieOpc.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipologieOpc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipologieOpc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipologieOpc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipologieOpc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipologieOpc() throws Exception {
        int databaseSizeBeforeUpdate = tipologieOpcRepository.findAll().size();

        // Create the TipologieOpc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipologieOpcMockMvc.perform(put("/api/tipologie-opcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologieOpc)))
            .andExpect(status().isBadRequest());

        // Validate the TipologieOpc in the database
        List<TipologieOpc> tipologieOpcList = tipologieOpcRepository.findAll();
        assertThat(tipologieOpcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipologieOpc() throws Exception {
        // Initialize the database
        tipologieOpcService.save(tipologieOpc);

        int databaseSizeBeforeDelete = tipologieOpcRepository.findAll().size();

        // Delete the tipologieOpc
        restTipologieOpcMockMvc.perform(delete("/api/tipologie-opcs/{id}", tipologieOpc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipologieOpc> tipologieOpcList = tipologieOpcRepository.findAll();
        assertThat(tipologieOpcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipologieOpc.class);
        TipologieOpc tipologieOpc1 = new TipologieOpc();
        tipologieOpc1.setId(1L);
        TipologieOpc tipologieOpc2 = new TipologieOpc();
        tipologieOpc2.setId(tipologieOpc1.getId());
        assertThat(tipologieOpc1).isEqualTo(tipologieOpc2);
        tipologieOpc2.setId(2L);
        assertThat(tipologieOpc1).isNotEqualTo(tipologieOpc2);
        tipologieOpc1.setId(null);
        assertThat(tipologieOpc1).isNotEqualTo(tipologieOpc2);
    }
}
