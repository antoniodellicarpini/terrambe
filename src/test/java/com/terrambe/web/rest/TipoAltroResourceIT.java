package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoAltro;
import com.terrambe.domain.RegOpcAltro;
import com.terrambe.repository.TipoAltroRepository;
import com.terrambe.service.TipoAltroService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoAltroCriteria;
import com.terrambe.service.TipoAltroQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoAltroResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoAltroResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoAltroRepository tipoAltroRepository;

    @Autowired
    private TipoAltroService tipoAltroService;

    @Autowired
    private TipoAltroQueryService tipoAltroQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoAltroMockMvc;

    private TipoAltro tipoAltro;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoAltroResource tipoAltroResource = new TipoAltroResource(tipoAltroService, tipoAltroQueryService);
        this.restTipoAltroMockMvc = MockMvcBuilders.standaloneSetup(tipoAltroResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoAltro createEntity(EntityManager em) {
        TipoAltro tipoAltro = new TipoAltro()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoAltro;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoAltro createUpdatedEntity(EntityManager em) {
        TipoAltro tipoAltro = new TipoAltro()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoAltro;
    }

    @BeforeEach
    public void initTest() {
        tipoAltro = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoAltro() throws Exception {
        int databaseSizeBeforeCreate = tipoAltroRepository.findAll().size();

        // Create the TipoAltro
        restTipoAltroMockMvc.perform(post("/api/tipo-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoAltro)))
            .andExpect(status().isCreated());

        // Validate the TipoAltro in the database
        List<TipoAltro> tipoAltroList = tipoAltroRepository.findAll();
        assertThat(tipoAltroList).hasSize(databaseSizeBeforeCreate + 1);
        TipoAltro testTipoAltro = tipoAltroList.get(tipoAltroList.size() - 1);
        assertThat(testTipoAltro.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoAltro.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoAltro.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoAltro.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoAltro.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoAltroWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoAltroRepository.findAll().size();

        // Create the TipoAltro with an existing ID
        tipoAltro.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoAltroMockMvc.perform(post("/api/tipo-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoAltro)))
            .andExpect(status().isBadRequest());

        // Validate the TipoAltro in the database
        List<TipoAltro> tipoAltroList = tipoAltroRepository.findAll();
        assertThat(tipoAltroList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoAltros() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList
        restTipoAltroMockMvc.perform(get("/api/tipo-altros?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoAltro.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoAltro() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get the tipoAltro
        restTipoAltroMockMvc.perform(get("/api/tipo-altros/{id}", tipoAltro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoAltro.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoAltroShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoAltroList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoAltroShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoAltroShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoAltroList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoAltroShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where descrizione is not null
        defaultTipoAltroShouldBeFound("descrizione.specified=true");

        // Get all the tipoAltroList where descrizione is null
        defaultTipoAltroShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali is not null
        defaultTipoAltroShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoAltroList where dataInizVali is null
        defaultTipoAltroShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoAltroShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoAltroList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoAltroShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali is not null
        defaultTipoAltroShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoAltroList where dataFineVali is null
        defaultTipoAltroShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoAltroShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoAltroList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoAltroShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator is not null
        defaultTipoAltroShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoAltroList where userIdCreator is null
        defaultTipoAltroShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoAltroShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoAltroList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoAltroShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod is not null
        defaultTipoAltroShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoAltroList where userIdLastMod is null
        defaultTipoAltroShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoAltrosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);

        // Get all the tipoAltroList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoAltroShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoAltroList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoAltroShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoAltrosByTipoToOpcAltroIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoAltroRepository.saveAndFlush(tipoAltro);
        RegOpcAltro tipoToOpcAltro = RegOpcAltroResourceIT.createEntity(em);
        em.persist(tipoToOpcAltro);
        em.flush();
        tipoAltro.addTipoToOpcAltro(tipoToOpcAltro);
        tipoAltroRepository.saveAndFlush(tipoAltro);
        Long tipoToOpcAltroId = tipoToOpcAltro.getId();

        // Get all the tipoAltroList where tipoToOpcAltro equals to tipoToOpcAltroId
        defaultTipoAltroShouldBeFound("tipoToOpcAltroId.equals=" + tipoToOpcAltroId);

        // Get all the tipoAltroList where tipoToOpcAltro equals to tipoToOpcAltroId + 1
        defaultTipoAltroShouldNotBeFound("tipoToOpcAltroId.equals=" + (tipoToOpcAltroId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoAltroShouldBeFound(String filter) throws Exception {
        restTipoAltroMockMvc.perform(get("/api/tipo-altros?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoAltro.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoAltroMockMvc.perform(get("/api/tipo-altros/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoAltroShouldNotBeFound(String filter) throws Exception {
        restTipoAltroMockMvc.perform(get("/api/tipo-altros?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoAltroMockMvc.perform(get("/api/tipo-altros/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoAltro() throws Exception {
        // Get the tipoAltro
        restTipoAltroMockMvc.perform(get("/api/tipo-altros/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoAltro() throws Exception {
        // Initialize the database
        tipoAltroService.save(tipoAltro);

        int databaseSizeBeforeUpdate = tipoAltroRepository.findAll().size();

        // Update the tipoAltro
        TipoAltro updatedTipoAltro = tipoAltroRepository.findById(tipoAltro.getId()).get();
        // Disconnect from session so that the updates on updatedTipoAltro are not directly saved in db
        em.detach(updatedTipoAltro);
        updatedTipoAltro
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoAltroMockMvc.perform(put("/api/tipo-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoAltro)))
            .andExpect(status().isOk());

        // Validate the TipoAltro in the database
        List<TipoAltro> tipoAltroList = tipoAltroRepository.findAll();
        assertThat(tipoAltroList).hasSize(databaseSizeBeforeUpdate);
        TipoAltro testTipoAltro = tipoAltroList.get(tipoAltroList.size() - 1);
        assertThat(testTipoAltro.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoAltro.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoAltro.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoAltro.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoAltro.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoAltro() throws Exception {
        int databaseSizeBeforeUpdate = tipoAltroRepository.findAll().size();

        // Create the TipoAltro

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoAltroMockMvc.perform(put("/api/tipo-altros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoAltro)))
            .andExpect(status().isBadRequest());

        // Validate the TipoAltro in the database
        List<TipoAltro> tipoAltroList = tipoAltroRepository.findAll();
        assertThat(tipoAltroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoAltro() throws Exception {
        // Initialize the database
        tipoAltroService.save(tipoAltro);

        int databaseSizeBeforeDelete = tipoAltroRepository.findAll().size();

        // Delete the tipoAltro
        restTipoAltroMockMvc.perform(delete("/api/tipo-altros/{id}", tipoAltro.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoAltro> tipoAltroList = tipoAltroRepository.findAll();
        assertThat(tipoAltroList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoAltro.class);
        TipoAltro tipoAltro1 = new TipoAltro();
        tipoAltro1.setId(1L);
        TipoAltro tipoAltro2 = new TipoAltro();
        tipoAltro2.setId(tipoAltro1.getId());
        assertThat(tipoAltro1).isEqualTo(tipoAltro2);
        tipoAltro2.setId(2L);
        assertThat(tipoAltro1).isNotEqualTo(tipoAltro2);
        tipoAltro1.setId(null);
        assertThat(tipoAltro1).isNotEqualTo(tipoAltro2);
    }
}
