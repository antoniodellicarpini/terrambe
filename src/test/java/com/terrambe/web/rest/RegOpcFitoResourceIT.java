package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcFito;
import com.terrambe.domain.RegOpcFitoAppz;
import com.terrambe.domain.RegOpcFitoProd;
import com.terrambe.domain.DosaggioTrattamento;
import com.terrambe.domain.MotivoTrattFito;
import com.terrambe.repository.RegOpcFitoRepository;
import com.terrambe.service.RegOpcFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcFitoCriteria;
import com.terrambe.service.RegOpcFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcFitoResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Float DEFAULT_VOLUME_ACQUA = 1F;
    private static final Float UPDATED_VOLUME_ACQUA = 2F;
    private static final Float SMALLER_VOLUME_ACQUA = 1F - 1F;

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_FASE_FENOLOGICA = 1L;
    private static final Long UPDATED_ID_FASE_FENOLOGICA = 2L;
    private static final Long SMALLER_ID_FASE_FENOLOGICA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcFitoRepository regOpcFitoRepository;

    @Autowired
    private RegOpcFitoService regOpcFitoService;

    @Autowired
    private RegOpcFitoQueryService regOpcFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcFitoMockMvc;

    private RegOpcFito regOpcFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcFitoResource regOpcFitoResource = new RegOpcFitoResource(regOpcFitoService, regOpcFitoQueryService);
        this.restRegOpcFitoMockMvc = MockMvcBuilders.standaloneSetup(regOpcFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFito createEntity(EntityManager em) {
        RegOpcFito regOpcFito = new RegOpcFito()
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .volumeAcqua(DEFAULT_VOLUME_ACQUA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcFito createUpdatedEntity(EntityManager em) {
        RegOpcFito regOpcFito = new RegOpcFito()
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volumeAcqua(UPDATED_VOLUME_ACQUA)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcFito;
    }

    @BeforeEach
    public void initTest() {
        regOpcFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcFito() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoRepository.findAll().size();

        // Create the RegOpcFito
        restRegOpcFitoMockMvc.perform(post("/api/reg-opc-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFito)))
            .andExpect(status().isCreated());

        // Validate the RegOpcFito in the database
        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcFito testRegOpcFito = regOpcFitoList.get(regOpcFitoList.size() - 1);
        assertThat(testRegOpcFito.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcFito.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcFito.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcFito.getVolumeAcqua()).isEqualTo(DEFAULT_VOLUME_ACQUA);
        assertThat(testRegOpcFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcFito.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcFitoRepository.findAll().size();

        // Create the RegOpcFito with an existing ID
        regOpcFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcFitoMockMvc.perform(post("/api/reg-opc-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFito)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFito in the database
        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcFitoRepository.findAll().size();
        // set the field null

        // Create the RegOpcFito, which fails.

        restRegOpcFitoMockMvc.perform(post("/api/reg-opc-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFito)))
            .andExpect(status().isBadRequest());

        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitos() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].volumeAcqua").value(hasItem(DEFAULT_VOLUME_ACQUA.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idFaseFenologica").value(hasItem(DEFAULT_ID_FASE_FENOLOGICA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcFito() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get the regOpcFito
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos/{id}", regOpcFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcFito.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.volumeAcqua").value(DEFAULT_VOLUME_ACQUA.doubleValue()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idFaseFenologica").value(DEFAULT_ID_FASE_FENOLOGICA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda is not null
        defaultRegOpcFitoShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcFitoList where idAzienda is null
        defaultRegOpcFitoShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcFitoShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcFitoList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcFitoShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd is not null
        defaultRegOpcFitoShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcFitoList where idUnitaProd is null
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcFitoShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcFitoList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcFitoShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc is not null
        defaultRegOpcFitoShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcFitoList where dataInizOpc is null
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcFitoShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcFitoList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcFitoShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc is not null
        defaultRegOpcFitoShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcFitoList where dataFineOpc is null
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcFitoShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcFitoList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcFitoShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcFitoShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcFitoList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcFitoShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcFitoShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcFitoList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcFitoShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where tempoImpiegato is not null
        defaultRegOpcFitoShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcFitoList where tempoImpiegato is null
        defaultRegOpcFitoShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua equals to DEFAULT_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.equals=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua equals to UPDATED_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.equals=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua in DEFAULT_VOLUME_ACQUA or UPDATED_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.in=" + DEFAULT_VOLUME_ACQUA + "," + UPDATED_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua equals to UPDATED_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.in=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua is not null
        defaultRegOpcFitoShouldBeFound("volumeAcqua.specified=true");

        // Get all the regOpcFitoList where volumeAcqua is null
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua is greater than or equal to DEFAULT_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.greaterThanOrEqual=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua is greater than or equal to UPDATED_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.greaterThanOrEqual=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua is less than or equal to DEFAULT_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.lessThanOrEqual=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua is less than or equal to SMALLER_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.lessThanOrEqual=" + SMALLER_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua is less than DEFAULT_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.lessThan=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua is less than UPDATED_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.lessThan=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByVolumeAcquaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where volumeAcqua is greater than DEFAULT_VOLUME_ACQUA
        defaultRegOpcFitoShouldNotBeFound("volumeAcqua.greaterThan=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcFitoList where volumeAcqua is greater than SMALLER_VOLUME_ACQUA
        defaultRegOpcFitoShouldBeFound("volumeAcqua.greaterThan=" + SMALLER_VOLUME_ACQUA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura is not null
        defaultRegOpcFitoShouldBeFound("idColtura.specified=true");

        // Get all the regOpcFitoList where idColtura is null
        defaultRegOpcFitoShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcFitoShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcFitoList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcFitoShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica equals to DEFAULT_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.equals=" + DEFAULT_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica equals to UPDATED_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.equals=" + UPDATED_ID_FASE_FENOLOGICA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica in DEFAULT_ID_FASE_FENOLOGICA or UPDATED_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.in=" + DEFAULT_ID_FASE_FENOLOGICA + "," + UPDATED_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica equals to UPDATED_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.in=" + UPDATED_ID_FASE_FENOLOGICA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica is not null
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.specified=true");

        // Get all the regOpcFitoList where idFaseFenologica is null
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica is greater than or equal to DEFAULT_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.greaterThanOrEqual=" + DEFAULT_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica is greater than or equal to UPDATED_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.greaterThanOrEqual=" + UPDATED_ID_FASE_FENOLOGICA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica is less than or equal to DEFAULT_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.lessThanOrEqual=" + DEFAULT_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica is less than or equal to SMALLER_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.lessThanOrEqual=" + SMALLER_ID_FASE_FENOLOGICA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica is less than DEFAULT_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.lessThan=" + DEFAULT_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica is less than UPDATED_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.lessThan=" + UPDATED_ID_FASE_FENOLOGICA);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdFaseFenologicaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idFaseFenologica is greater than DEFAULT_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldNotBeFound("idFaseFenologica.greaterThan=" + DEFAULT_ID_FASE_FENOLOGICA);

        // Get all the regOpcFitoList where idFaseFenologica is greater than SMALLER_ID_FASE_FENOLOGICA
        defaultRegOpcFitoShouldBeFound("idFaseFenologica.greaterThan=" + SMALLER_ID_FASE_FENOLOGICA);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore is not null
        defaultRegOpcFitoShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcFitoList where idOperatore is null
        defaultRegOpcFitoShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcFitoShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcFitoList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcFitoShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo is not null
        defaultRegOpcFitoShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcFitoList where idMezzo is null
        defaultRegOpcFitoShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcFitoShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcFitoList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcFitoShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali is not null
        defaultRegOpcFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcFitoList where dataInizVali is null
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali is not null
        defaultRegOpcFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcFitoList where dataFineVali is null
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator is not null
        defaultRegOpcFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcFitoList where userIdCreator is null
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod is not null
        defaultRegOpcFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcFitoList where userIdLastMod is null
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);

        // Get all the regOpcFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByOpcFitoToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        RegOpcFitoAppz opcFitoToAppz = RegOpcFitoAppzResourceIT.createEntity(em);
        em.persist(opcFitoToAppz);
        em.flush();
        regOpcFito.addOpcFitoToAppz(opcFitoToAppz);
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        Long opcFitoToAppzId = opcFitoToAppz.getId();

        // Get all the regOpcFitoList where opcFitoToAppz equals to opcFitoToAppzId
        defaultRegOpcFitoShouldBeFound("opcFitoToAppzId.equals=" + opcFitoToAppzId);

        // Get all the regOpcFitoList where opcFitoToAppz equals to opcFitoToAppzId + 1
        defaultRegOpcFitoShouldNotBeFound("opcFitoToAppzId.equals=" + (opcFitoToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByOpcFitoToProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        RegOpcFitoProd opcFitoToProd = RegOpcFitoProdResourceIT.createEntity(em);
        em.persist(opcFitoToProd);
        em.flush();
        regOpcFito.addOpcFitoToProd(opcFitoToProd);
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        Long opcFitoToProdId = opcFitoToProd.getId();

        // Get all the regOpcFitoList where opcFitoToProd equals to opcFitoToProdId
        defaultRegOpcFitoShouldBeFound("opcFitoToProdId.equals=" + opcFitoToProdId);

        // Get all the regOpcFitoList where opcFitoToProd equals to opcFitoToProdId + 1
        defaultRegOpcFitoShouldNotBeFound("opcFitoToProdId.equals=" + (opcFitoToProdId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByRegOpcFitoToDosTrattIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        DosaggioTrattamento regOpcFitoToDosTratt = DosaggioTrattamentoResourceIT.createEntity(em);
        em.persist(regOpcFitoToDosTratt);
        em.flush();
        regOpcFito.setRegOpcFitoToDosTratt(regOpcFitoToDosTratt);
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        Long regOpcFitoToDosTrattId = regOpcFitoToDosTratt.getId();

        // Get all the regOpcFitoList where regOpcFitoToDosTratt equals to regOpcFitoToDosTrattId
        defaultRegOpcFitoShouldBeFound("regOpcFitoToDosTrattId.equals=" + regOpcFitoToDosTrattId);

        // Get all the regOpcFitoList where regOpcFitoToDosTratt equals to regOpcFitoToDosTrattId + 1
        defaultRegOpcFitoShouldNotBeFound("regOpcFitoToDosTrattId.equals=" + (regOpcFitoToDosTrattId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcFitosByTrattFitoToMotivoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        MotivoTrattFito trattFitoToMotivo = MotivoTrattFitoResourceIT.createEntity(em);
        em.persist(trattFitoToMotivo);
        em.flush();
        regOpcFito.setTrattFitoToMotivo(trattFitoToMotivo);
        regOpcFitoRepository.saveAndFlush(regOpcFito);
        Long trattFitoToMotivoId = trattFitoToMotivo.getId();

        // Get all the regOpcFitoList where trattFitoToMotivo equals to trattFitoToMotivoId
        defaultRegOpcFitoShouldBeFound("trattFitoToMotivoId.equals=" + trattFitoToMotivoId);

        // Get all the regOpcFitoList where trattFitoToMotivo equals to trattFitoToMotivoId + 1
        defaultRegOpcFitoShouldNotBeFound("trattFitoToMotivoId.equals=" + (trattFitoToMotivoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcFitoShouldBeFound(String filter) throws Exception {
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].volumeAcqua").value(hasItem(DEFAULT_VOLUME_ACQUA.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idFaseFenologica").value(hasItem(DEFAULT_ID_FASE_FENOLOGICA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcFitoShouldNotBeFound(String filter) throws Exception {
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcFito() throws Exception {
        // Get the regOpcFito
        restRegOpcFitoMockMvc.perform(get("/api/reg-opc-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcFito() throws Exception {
        // Initialize the database
        regOpcFitoService.save(regOpcFito);

        int databaseSizeBeforeUpdate = regOpcFitoRepository.findAll().size();

        // Update the regOpcFito
        RegOpcFito updatedRegOpcFito = regOpcFitoRepository.findById(regOpcFito.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcFito are not directly saved in db
        em.detach(updatedRegOpcFito);
        updatedRegOpcFito
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volumeAcqua(UPDATED_VOLUME_ACQUA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcFitoMockMvc.perform(put("/api/reg-opc-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcFito)))
            .andExpect(status().isOk());

        // Validate the RegOpcFito in the database
        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeUpdate);
        RegOpcFito testRegOpcFito = regOpcFitoList.get(regOpcFitoList.size() - 1);
        assertThat(testRegOpcFito.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcFito.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcFito.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcFito.getVolumeAcqua()).isEqualTo(UPDATED_VOLUME_ACQUA);
        assertThat(testRegOpcFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcFito.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcFito() throws Exception {
        int databaseSizeBeforeUpdate = regOpcFitoRepository.findAll().size();

        // Create the RegOpcFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcFitoMockMvc.perform(put("/api/reg-opc-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcFito)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcFito in the database
        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcFito() throws Exception {
        // Initialize the database
        regOpcFitoService.save(regOpcFito);

        int databaseSizeBeforeDelete = regOpcFitoRepository.findAll().size();

        // Delete the regOpcFito
        restRegOpcFitoMockMvc.perform(delete("/api/reg-opc-fitos/{id}", regOpcFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcFito> regOpcFitoList = regOpcFitoRepository.findAll();
        assertThat(regOpcFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcFito.class);
        RegOpcFito regOpcFito1 = new RegOpcFito();
        regOpcFito1.setId(1L);
        RegOpcFito regOpcFito2 = new RegOpcFito();
        regOpcFito2.setId(regOpcFito1.getId());
        assertThat(regOpcFito1).isEqualTo(regOpcFito2);
        regOpcFito2.setId(2L);
        assertThat(regOpcFito1).isNotEqualTo(regOpcFito2);
        regOpcFito1.setId(null);
        assertThat(regOpcFito1).isNotEqualTo(regOpcFito2);
    }
}
