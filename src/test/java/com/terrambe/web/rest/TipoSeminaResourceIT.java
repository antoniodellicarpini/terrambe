package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoSemina;
import com.terrambe.domain.RegOpcSemina;
import com.terrambe.repository.TipoSeminaRepository;
import com.terrambe.service.TipoSeminaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoSeminaCriteria;
import com.terrambe.service.TipoSeminaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoSeminaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoSeminaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoSeminaRepository tipoSeminaRepository;

    @Autowired
    private TipoSeminaService tipoSeminaService;

    @Autowired
    private TipoSeminaQueryService tipoSeminaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoSeminaMockMvc;

    private TipoSemina tipoSemina;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoSeminaResource tipoSeminaResource = new TipoSeminaResource(tipoSeminaService, tipoSeminaQueryService);
        this.restTipoSeminaMockMvc = MockMvcBuilders.standaloneSetup(tipoSeminaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoSemina createEntity(EntityManager em) {
        TipoSemina tipoSemina = new TipoSemina()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoSemina;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoSemina createUpdatedEntity(EntityManager em) {
        TipoSemina tipoSemina = new TipoSemina()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoSemina;
    }

    @BeforeEach
    public void initTest() {
        tipoSemina = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoSemina() throws Exception {
        int databaseSizeBeforeCreate = tipoSeminaRepository.findAll().size();

        // Create the TipoSemina
        restTipoSeminaMockMvc.perform(post("/api/tipo-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoSemina)))
            .andExpect(status().isCreated());

        // Validate the TipoSemina in the database
        List<TipoSemina> tipoSeminaList = tipoSeminaRepository.findAll();
        assertThat(tipoSeminaList).hasSize(databaseSizeBeforeCreate + 1);
        TipoSemina testTipoSemina = tipoSeminaList.get(tipoSeminaList.size() - 1);
        assertThat(testTipoSemina.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoSemina.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoSemina.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoSemina.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoSemina.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoSeminaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoSeminaRepository.findAll().size();

        // Create the TipoSemina with an existing ID
        tipoSemina.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoSeminaMockMvc.perform(post("/api/tipo-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoSemina)))
            .andExpect(status().isBadRequest());

        // Validate the TipoSemina in the database
        List<TipoSemina> tipoSeminaList = tipoSeminaRepository.findAll();
        assertThat(tipoSeminaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoSeminas() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoSemina.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoSemina() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get the tipoSemina
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas/{id}", tipoSemina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoSemina.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoSeminaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoSeminaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoSeminaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoSeminaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoSeminaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoSeminaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where descrizione is not null
        defaultTipoSeminaShouldBeFound("descrizione.specified=true");

        // Get all the tipoSeminaList where descrizione is null
        defaultTipoSeminaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali is not null
        defaultTipoSeminaShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoSeminaList where dataInizVali is null
        defaultTipoSeminaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoSeminaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoSeminaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoSeminaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali is not null
        defaultTipoSeminaShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoSeminaList where dataFineVali is null
        defaultTipoSeminaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoSeminaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoSeminaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoSeminaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator is not null
        defaultTipoSeminaShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoSeminaList where userIdCreator is null
        defaultTipoSeminaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoSeminaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoSeminaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoSeminaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod is not null
        defaultTipoSeminaShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoSeminaList where userIdLastMod is null
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoSeminasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);

        // Get all the tipoSeminaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoSeminaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoSeminaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoSeminaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoSeminasByTipoToOpcSeminaIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoSeminaRepository.saveAndFlush(tipoSemina);
        RegOpcSemina tipoToOpcSemina = RegOpcSeminaResourceIT.createEntity(em);
        em.persist(tipoToOpcSemina);
        em.flush();
        tipoSemina.addTipoToOpcSemina(tipoToOpcSemina);
        tipoSeminaRepository.saveAndFlush(tipoSemina);
        Long tipoToOpcSeminaId = tipoToOpcSemina.getId();

        // Get all the tipoSeminaList where tipoToOpcSemina equals to tipoToOpcSeminaId
        defaultTipoSeminaShouldBeFound("tipoToOpcSeminaId.equals=" + tipoToOpcSeminaId);

        // Get all the tipoSeminaList where tipoToOpcSemina equals to tipoToOpcSeminaId + 1
        defaultTipoSeminaShouldNotBeFound("tipoToOpcSeminaId.equals=" + (tipoToOpcSeminaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoSeminaShouldBeFound(String filter) throws Exception {
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoSemina.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoSeminaShouldNotBeFound(String filter) throws Exception {
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoSemina() throws Exception {
        // Get the tipoSemina
        restTipoSeminaMockMvc.perform(get("/api/tipo-seminas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoSemina() throws Exception {
        // Initialize the database
        tipoSeminaService.save(tipoSemina);

        int databaseSizeBeforeUpdate = tipoSeminaRepository.findAll().size();

        // Update the tipoSemina
        TipoSemina updatedTipoSemina = tipoSeminaRepository.findById(tipoSemina.getId()).get();
        // Disconnect from session so that the updates on updatedTipoSemina are not directly saved in db
        em.detach(updatedTipoSemina);
        updatedTipoSemina
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoSeminaMockMvc.perform(put("/api/tipo-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoSemina)))
            .andExpect(status().isOk());

        // Validate the TipoSemina in the database
        List<TipoSemina> tipoSeminaList = tipoSeminaRepository.findAll();
        assertThat(tipoSeminaList).hasSize(databaseSizeBeforeUpdate);
        TipoSemina testTipoSemina = tipoSeminaList.get(tipoSeminaList.size() - 1);
        assertThat(testTipoSemina.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoSemina.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoSemina.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoSemina.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoSemina.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoSemina() throws Exception {
        int databaseSizeBeforeUpdate = tipoSeminaRepository.findAll().size();

        // Create the TipoSemina

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoSeminaMockMvc.perform(put("/api/tipo-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoSemina)))
            .andExpect(status().isBadRequest());

        // Validate the TipoSemina in the database
        List<TipoSemina> tipoSeminaList = tipoSeminaRepository.findAll();
        assertThat(tipoSeminaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoSemina() throws Exception {
        // Initialize the database
        tipoSeminaService.save(tipoSemina);

        int databaseSizeBeforeDelete = tipoSeminaRepository.findAll().size();

        // Delete the tipoSemina
        restTipoSeminaMockMvc.perform(delete("/api/tipo-seminas/{id}", tipoSemina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoSemina> tipoSeminaList = tipoSeminaRepository.findAll();
        assertThat(tipoSeminaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoSemina.class);
        TipoSemina tipoSemina1 = new TipoSemina();
        tipoSemina1.setId(1L);
        TipoSemina tipoSemina2 = new TipoSemina();
        tipoSemina2.setId(tipoSemina1.getId());
        assertThat(tipoSemina1).isEqualTo(tipoSemina2);
        tipoSemina2.setId(2L);
        assertThat(tipoSemina1).isNotEqualTo(tipoSemina2);
        tipoSemina1.setId(null);
        assertThat(tipoSemina1).isNotEqualTo(tipoSemina2);
    }
}
