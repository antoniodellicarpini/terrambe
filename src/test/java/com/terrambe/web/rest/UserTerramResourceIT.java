package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.UserTerram;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.UserTerramRepository;
import com.terrambe.service.UserTerramService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.UserTerramCriteria;
import com.terrambe.service.UserTerramQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.terrambe.domain.enumeration.TipologiaUtente;
/**
 * Integration tests for the {@link UserTerramResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class UserTerramResourceIT {

    private static final TipologiaUtente DEFAULT_TIPO_UTENTE = TipologiaUtente.RAPPRESENTANTE;
    private static final TipologiaUtente UPDATED_TIPO_UTENTE = TipologiaUtente.TERZISTA;

    private static final Integer DEFAULT_ID_JUSER = 1;
    private static final Integer UPDATED_ID_JUSER = 2;
    private static final Integer SMALLER_ID_JUSER = 1 - 1;

    @Autowired
    private UserTerramRepository userTerramRepository;

    @Mock
    private UserTerramRepository userTerramRepositoryMock;

    @Mock
    private UserTerramService userTerramServiceMock;

    @Autowired
    private UserTerramService userTerramService;

    @Autowired
    private UserTerramQueryService userTerramQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserTerramMockMvc;

    private UserTerram userTerram;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserTerramResource userTerramResource = new UserTerramResource(userTerramService, userTerramQueryService);
        this.restUserTerramMockMvc = MockMvcBuilders.standaloneSetup(userTerramResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTerram createEntity(EntityManager em) {
        UserTerram userTerram = new UserTerram()
            .tipoUtente(DEFAULT_TIPO_UTENTE)
            .idJuser(DEFAULT_ID_JUSER);
        return userTerram;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTerram createUpdatedEntity(EntityManager em) {
        UserTerram userTerram = new UserTerram()
            .tipoUtente(UPDATED_TIPO_UTENTE)
            .idJuser(UPDATED_ID_JUSER);
        return userTerram;
    }

    @BeforeEach
    public void initTest() {
        userTerram = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserTerram() throws Exception {
        int databaseSizeBeforeCreate = userTerramRepository.findAll().size();

        // Create the UserTerram
        restUserTerramMockMvc.perform(post("/api/user-terrams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTerram)))
            .andExpect(status().isCreated());

        // Validate the UserTerram in the database
        List<UserTerram> userTerramList = userTerramRepository.findAll();
        assertThat(userTerramList).hasSize(databaseSizeBeforeCreate + 1);
        UserTerram testUserTerram = userTerramList.get(userTerramList.size() - 1);
        assertThat(testUserTerram.getTipoUtente()).isEqualTo(DEFAULT_TIPO_UTENTE);
        assertThat(testUserTerram.getIdJuser()).isEqualTo(DEFAULT_ID_JUSER);
    }

    @Test
    @Transactional
    public void createUserTerramWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userTerramRepository.findAll().size();

        // Create the UserTerram with an existing ID
        userTerram.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserTerramMockMvc.perform(post("/api/user-terrams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTerram)))
            .andExpect(status().isBadRequest());

        // Validate the UserTerram in the database
        List<UserTerram> userTerramList = userTerramRepository.findAll();
        assertThat(userTerramList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserTerrams() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList
        restUserTerramMockMvc.perform(get("/api/user-terrams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userTerram.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoUtente").value(hasItem(DEFAULT_TIPO_UTENTE.toString())))
            .andExpect(jsonPath("$.[*].idJuser").value(hasItem(DEFAULT_ID_JUSER)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllUserTerramsWithEagerRelationshipsIsEnabled() throws Exception {
        UserTerramResource userTerramResource = new UserTerramResource(userTerramServiceMock, userTerramQueryService);
        when(userTerramServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restUserTerramMockMvc = MockMvcBuilders.standaloneSetup(userTerramResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUserTerramMockMvc.perform(get("/api/user-terrams?eagerload=true"))
        .andExpect(status().isOk());

        verify(userTerramServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllUserTerramsWithEagerRelationshipsIsNotEnabled() throws Exception {
        UserTerramResource userTerramResource = new UserTerramResource(userTerramServiceMock, userTerramQueryService);
            when(userTerramServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restUserTerramMockMvc = MockMvcBuilders.standaloneSetup(userTerramResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUserTerramMockMvc.perform(get("/api/user-terrams?eagerload=true"))
        .andExpect(status().isOk());

            verify(userTerramServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getUserTerram() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get the userTerram
        restUserTerramMockMvc.perform(get("/api/user-terrams/{id}", userTerram.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userTerram.getId().intValue()))
            .andExpect(jsonPath("$.tipoUtente").value(DEFAULT_TIPO_UTENTE.toString()))
            .andExpect(jsonPath("$.idJuser").value(DEFAULT_ID_JUSER));
    }

    @Test
    @Transactional
    public void getAllUserTerramsByTipoUtenteIsEqualToSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where tipoUtente equals to DEFAULT_TIPO_UTENTE
        defaultUserTerramShouldBeFound("tipoUtente.equals=" + DEFAULT_TIPO_UTENTE);

        // Get all the userTerramList where tipoUtente equals to UPDATED_TIPO_UTENTE
        defaultUserTerramShouldNotBeFound("tipoUtente.equals=" + UPDATED_TIPO_UTENTE);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByTipoUtenteIsInShouldWork() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where tipoUtente in DEFAULT_TIPO_UTENTE or UPDATED_TIPO_UTENTE
        defaultUserTerramShouldBeFound("tipoUtente.in=" + DEFAULT_TIPO_UTENTE + "," + UPDATED_TIPO_UTENTE);

        // Get all the userTerramList where tipoUtente equals to UPDATED_TIPO_UTENTE
        defaultUserTerramShouldNotBeFound("tipoUtente.in=" + UPDATED_TIPO_UTENTE);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByTipoUtenteIsNullOrNotNull() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where tipoUtente is not null
        defaultUserTerramShouldBeFound("tipoUtente.specified=true");

        // Get all the userTerramList where tipoUtente is null
        defaultUserTerramShouldNotBeFound("tipoUtente.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsEqualToSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser equals to DEFAULT_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.equals=" + DEFAULT_ID_JUSER);

        // Get all the userTerramList where idJuser equals to UPDATED_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.equals=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsInShouldWork() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser in DEFAULT_ID_JUSER or UPDATED_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.in=" + DEFAULT_ID_JUSER + "," + UPDATED_ID_JUSER);

        // Get all the userTerramList where idJuser equals to UPDATED_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.in=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsNullOrNotNull() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser is not null
        defaultUserTerramShouldBeFound("idJuser.specified=true");

        // Get all the userTerramList where idJuser is null
        defaultUserTerramShouldNotBeFound("idJuser.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser is greater than or equal to DEFAULT_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.greaterThanOrEqual=" + DEFAULT_ID_JUSER);

        // Get all the userTerramList where idJuser is greater than or equal to UPDATED_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.greaterThanOrEqual=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser is less than or equal to DEFAULT_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.lessThanOrEqual=" + DEFAULT_ID_JUSER);

        // Get all the userTerramList where idJuser is less than or equal to SMALLER_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.lessThanOrEqual=" + SMALLER_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsLessThanSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser is less than DEFAULT_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.lessThan=" + DEFAULT_ID_JUSER);

        // Get all the userTerramList where idJuser is less than UPDATED_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.lessThan=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllUserTerramsByIdJuserIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);

        // Get all the userTerramList where idJuser is greater than DEFAULT_ID_JUSER
        defaultUserTerramShouldNotBeFound("idJuser.greaterThan=" + DEFAULT_ID_JUSER);

        // Get all the userTerramList where idJuser is greater than SMALLER_ID_JUSER
        defaultUserTerramShouldBeFound("idJuser.greaterThan=" + SMALLER_ID_JUSER);
    }


    @Test
    @Transactional
    public void getAllUserTerramsByUsrTerToAziAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        userTerramRepository.saveAndFlush(userTerram);
        AziAnagrafica usrTerToAziAna = AziAnagraficaResourceIT.createEntity(em);
        em.persist(usrTerToAziAna);
        em.flush();
        userTerram.addUsrTerToAziAna(usrTerToAziAna);
        userTerramRepository.saveAndFlush(userTerram);
        Long usrTerToAziAnaId = usrTerToAziAna.getId();

        // Get all the userTerramList where usrTerToAziAna equals to usrTerToAziAnaId
        defaultUserTerramShouldBeFound("usrTerToAziAnaId.equals=" + usrTerToAziAnaId);

        // Get all the userTerramList where usrTerToAziAna equals to usrTerToAziAnaId + 1
        defaultUserTerramShouldNotBeFound("usrTerToAziAnaId.equals=" + (usrTerToAziAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserTerramShouldBeFound(String filter) throws Exception {
        restUserTerramMockMvc.perform(get("/api/user-terrams?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userTerram.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoUtente").value(hasItem(DEFAULT_TIPO_UTENTE.toString())))
            .andExpect(jsonPath("$.[*].idJuser").value(hasItem(DEFAULT_ID_JUSER)));

        // Check, that the count call also returns 1
        restUserTerramMockMvc.perform(get("/api/user-terrams/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserTerramShouldNotBeFound(String filter) throws Exception {
        restUserTerramMockMvc.perform(get("/api/user-terrams?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserTerramMockMvc.perform(get("/api/user-terrams/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserTerram() throws Exception {
        // Get the userTerram
        restUserTerramMockMvc.perform(get("/api/user-terrams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserTerram() throws Exception {
        // Initialize the database
        userTerramService.save(userTerram);

        int databaseSizeBeforeUpdate = userTerramRepository.findAll().size();

        // Update the userTerram
        UserTerram updatedUserTerram = userTerramRepository.findById(userTerram.getId()).get();
        // Disconnect from session so that the updates on updatedUserTerram are not directly saved in db
        em.detach(updatedUserTerram);
        updatedUserTerram
            .tipoUtente(UPDATED_TIPO_UTENTE)
            .idJuser(UPDATED_ID_JUSER);

        restUserTerramMockMvc.perform(put("/api/user-terrams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserTerram)))
            .andExpect(status().isOk());

        // Validate the UserTerram in the database
        List<UserTerram> userTerramList = userTerramRepository.findAll();
        assertThat(userTerramList).hasSize(databaseSizeBeforeUpdate);
        UserTerram testUserTerram = userTerramList.get(userTerramList.size() - 1);
        assertThat(testUserTerram.getTipoUtente()).isEqualTo(UPDATED_TIPO_UTENTE);
        assertThat(testUserTerram.getIdJuser()).isEqualTo(UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void updateNonExistingUserTerram() throws Exception {
        int databaseSizeBeforeUpdate = userTerramRepository.findAll().size();

        // Create the UserTerram

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserTerramMockMvc.perform(put("/api/user-terrams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTerram)))
            .andExpect(status().isBadRequest());

        // Validate the UserTerram in the database
        List<UserTerram> userTerramList = userTerramRepository.findAll();
        assertThat(userTerramList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserTerram() throws Exception {
        // Initialize the database
        userTerramService.save(userTerram);

        int databaseSizeBeforeDelete = userTerramRepository.findAll().size();

        // Delete the userTerram
        restUserTerramMockMvc.perform(delete("/api/user-terrams/{id}", userTerram.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserTerram> userTerramList = userTerramRepository.findAll();
        assertThat(userTerramList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTerram.class);
        UserTerram userTerram1 = new UserTerram();
        userTerram1.setId(1L);
        UserTerram userTerram2 = new UserTerram();
        userTerram2.setId(userTerram1.getId());
        assertThat(userTerram1).isEqualTo(userTerram2);
        userTerram2.setId(2L);
        assertThat(userTerram1).isNotEqualTo(userTerram2);
        userTerram1.setId(null);
        assertThat(userTerram1).isNotEqualTo(userTerram2);
    }
}
