package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoDistConc;
import com.terrambe.domain.RegOpcConc;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.TipoDistConcRepository;
import com.terrambe.service.TipoDistConcService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoDistConcCriteria;
import com.terrambe.service.TipoDistConcQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoDistConcResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoDistConcResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoDistConcRepository tipoDistConcRepository;

    @Autowired
    private TipoDistConcService tipoDistConcService;

    @Autowired
    private TipoDistConcQueryService tipoDistConcQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoDistConcMockMvc;

    private TipoDistConc tipoDistConc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoDistConcResource tipoDistConcResource = new TipoDistConcResource(tipoDistConcService, tipoDistConcQueryService);
        this.restTipoDistConcMockMvc = MockMvcBuilders.standaloneSetup(tipoDistConcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoDistConc createEntity(EntityManager em) {
        TipoDistConc tipoDistConc = new TipoDistConc()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoDistConc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoDistConc createUpdatedEntity(EntityManager em) {
        TipoDistConc tipoDistConc = new TipoDistConc()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoDistConc;
    }

    @BeforeEach
    public void initTest() {
        tipoDistConc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoDistConc() throws Exception {
        int databaseSizeBeforeCreate = tipoDistConcRepository.findAll().size();

        // Create the TipoDistConc
        restTipoDistConcMockMvc.perform(post("/api/tipo-dist-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistConc)))
            .andExpect(status().isCreated());

        // Validate the TipoDistConc in the database
        List<TipoDistConc> tipoDistConcList = tipoDistConcRepository.findAll();
        assertThat(tipoDistConcList).hasSize(databaseSizeBeforeCreate + 1);
        TipoDistConc testTipoDistConc = tipoDistConcList.get(tipoDistConcList.size() - 1);
        assertThat(testTipoDistConc.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoDistConc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoDistConc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoDistConc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoDistConc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoDistConcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoDistConcRepository.findAll().size();

        // Create the TipoDistConc with an existing ID
        tipoDistConc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoDistConcMockMvc.perform(post("/api/tipo-dist-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistConc)))
            .andExpect(status().isBadRequest());

        // Validate the TipoDistConc in the database
        List<TipoDistConc> tipoDistConcList = tipoDistConcRepository.findAll();
        assertThat(tipoDistConcList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoDistConcs() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoDistConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoDistConc() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get the tipoDistConc
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs/{id}", tipoDistConc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoDistConc.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoDistConcShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoDistConcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoDistConcShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoDistConcShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoDistConcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoDistConcShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where descrizione is not null
        defaultTipoDistConcShouldBeFound("descrizione.specified=true");

        // Get all the tipoDistConcList where descrizione is null
        defaultTipoDistConcShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali is not null
        defaultTipoDistConcShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoDistConcList where dataInizVali is null
        defaultTipoDistConcShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoDistConcShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistConcList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoDistConcShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali is not null
        defaultTipoDistConcShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoDistConcList where dataFineVali is null
        defaultTipoDistConcShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoDistConcShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistConcList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoDistConcShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator is not null
        defaultTipoDistConcShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoDistConcList where userIdCreator is null
        defaultTipoDistConcShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoDistConcShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistConcList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoDistConcShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod is not null
        defaultTipoDistConcShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoDistConcList where userIdLastMod is null
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistConcsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);

        // Get all the tipoDistConcList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistConcShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistConcList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoDistConcShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoDistConcsByTipoDistToOpcConcIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);
        RegOpcConc tipoDistToOpcConc = RegOpcConcResourceIT.createEntity(em);
        em.persist(tipoDistToOpcConc);
        em.flush();
        tipoDistConc.addTipoDistToOpcConc(tipoDistToOpcConc);
        tipoDistConcRepository.saveAndFlush(tipoDistConc);
        Long tipoDistToOpcConcId = tipoDistToOpcConc.getId();

        // Get all the tipoDistConcList where tipoDistToOpcConc equals to tipoDistToOpcConcId
        defaultTipoDistConcShouldBeFound("tipoDistToOpcConcId.equals=" + tipoDistToOpcConcId);

        // Get all the tipoDistConcList where tipoDistToOpcConc equals to tipoDistToOpcConcId + 1
        defaultTipoDistConcShouldNotBeFound("tipoDistToOpcConcId.equals=" + (tipoDistToOpcConcId + 1));
    }


    @Test
    @Transactional
    public void getAllTipoDistConcsByTipoDistrToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistConcRepository.saveAndFlush(tipoDistConc);
        TabellaRaccordo tipoDistrToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(tipoDistrToRaccordo);
        em.flush();
        tipoDistConc.addTipoDistrToRaccordo(tipoDistrToRaccordo);
        tipoDistConcRepository.saveAndFlush(tipoDistConc);
        Long tipoDistrToRaccordoId = tipoDistrToRaccordo.getId();

        // Get all the tipoDistConcList where tipoDistrToRaccordo equals to tipoDistrToRaccordoId
        defaultTipoDistConcShouldBeFound("tipoDistrToRaccordoId.equals=" + tipoDistrToRaccordoId);

        // Get all the tipoDistConcList where tipoDistrToRaccordo equals to tipoDistrToRaccordoId + 1
        defaultTipoDistConcShouldNotBeFound("tipoDistrToRaccordoId.equals=" + (tipoDistrToRaccordoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoDistConcShouldBeFound(String filter) throws Exception {
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoDistConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoDistConcShouldNotBeFound(String filter) throws Exception {
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoDistConc() throws Exception {
        // Get the tipoDistConc
        restTipoDistConcMockMvc.perform(get("/api/tipo-dist-concs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoDistConc() throws Exception {
        // Initialize the database
        tipoDistConcService.save(tipoDistConc);

        int databaseSizeBeforeUpdate = tipoDistConcRepository.findAll().size();

        // Update the tipoDistConc
        TipoDistConc updatedTipoDistConc = tipoDistConcRepository.findById(tipoDistConc.getId()).get();
        // Disconnect from session so that the updates on updatedTipoDistConc are not directly saved in db
        em.detach(updatedTipoDistConc);
        updatedTipoDistConc
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoDistConcMockMvc.perform(put("/api/tipo-dist-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoDistConc)))
            .andExpect(status().isOk());

        // Validate the TipoDistConc in the database
        List<TipoDistConc> tipoDistConcList = tipoDistConcRepository.findAll();
        assertThat(tipoDistConcList).hasSize(databaseSizeBeforeUpdate);
        TipoDistConc testTipoDistConc = tipoDistConcList.get(tipoDistConcList.size() - 1);
        assertThat(testTipoDistConc.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoDistConc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoDistConc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoDistConc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoDistConc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoDistConc() throws Exception {
        int databaseSizeBeforeUpdate = tipoDistConcRepository.findAll().size();

        // Create the TipoDistConc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoDistConcMockMvc.perform(put("/api/tipo-dist-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistConc)))
            .andExpect(status().isBadRequest());

        // Validate the TipoDistConc in the database
        List<TipoDistConc> tipoDistConcList = tipoDistConcRepository.findAll();
        assertThat(tipoDistConcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoDistConc() throws Exception {
        // Initialize the database
        tipoDistConcService.save(tipoDistConc);

        int databaseSizeBeforeDelete = tipoDistConcRepository.findAll().size();

        // Delete the tipoDistConc
        restTipoDistConcMockMvc.perform(delete("/api/tipo-dist-concs/{id}", tipoDistConc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoDistConc> tipoDistConcList = tipoDistConcRepository.findAll();
        assertThat(tipoDistConcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoDistConc.class);
        TipoDistConc tipoDistConc1 = new TipoDistConc();
        tipoDistConc1.setId(1L);
        TipoDistConc tipoDistConc2 = new TipoDistConc();
        tipoDistConc2.setId(tipoDistConc1.getId());
        assertThat(tipoDistConc1).isEqualTo(tipoDistConc2);
        tipoDistConc2.setId(2L);
        assertThat(tipoDistConc1).isNotEqualTo(tipoDistConc2);
        tipoDistConc1.setId(null);
        assertThat(tipoDistConc1).isNotEqualTo(tipoDistConc2);
    }
}
