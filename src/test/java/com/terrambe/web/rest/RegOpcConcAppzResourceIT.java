package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcConcAppz;
import com.terrambe.domain.RegOpcConc;
import com.terrambe.repository.RegOpcConcAppzRepository;
import com.terrambe.service.RegOpcConcAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcConcAppzCriteria;
import com.terrambe.service.RegOpcConcAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcConcAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcConcAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Float DEFAULT_VAL_N = 1F;
    private static final Float UPDATED_VAL_N = 2F;
    private static final Float SMALLER_VAL_N = 1F - 1F;

    private static final Float DEFAULT_VAL_P = 1F;
    private static final Float UPDATED_VAL_P = 2F;
    private static final Float SMALLER_VAL_P = 1F - 1F;

    private static final Float DEFAULT_VAL_K = 1F;
    private static final Float UPDATED_VAL_K = 2F;
    private static final Float SMALLER_VAL_K = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcConcAppzRepository regOpcConcAppzRepository;

    @Autowired
    private RegOpcConcAppzService regOpcConcAppzService;

    @Autowired
    private RegOpcConcAppzQueryService regOpcConcAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcConcAppzMockMvc;

    private RegOpcConcAppz regOpcConcAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcConcAppzResource regOpcConcAppzResource = new RegOpcConcAppzResource(regOpcConcAppzService, regOpcConcAppzQueryService);
        this.restRegOpcConcAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcConcAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConcAppz createEntity(EntityManager em) {
        RegOpcConcAppz regOpcConcAppz = new RegOpcConcAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .valN(DEFAULT_VAL_N)
            .valP(DEFAULT_VAL_P)
            .valK(DEFAULT_VAL_K)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcConcAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcConcAppz createUpdatedEntity(EntityManager em) {
        RegOpcConcAppz regOpcConcAppz = new RegOpcConcAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .valN(UPDATED_VAL_N)
            .valP(UPDATED_VAL_P)
            .valK(UPDATED_VAL_K)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcConcAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcConcAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcConcAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcAppzRepository.findAll().size();

        // Create the RegOpcConcAppz
        restRegOpcConcAppzMockMvc.perform(post("/api/reg-opc-conc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcConcAppz in the database
        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcConcAppz testRegOpcConcAppz = regOpcConcAppzList.get(regOpcConcAppzList.size() - 1);
        assertThat(testRegOpcConcAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcConcAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcConcAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcConcAppz.getValN()).isEqualTo(DEFAULT_VAL_N);
        assertThat(testRegOpcConcAppz.getValP()).isEqualTo(DEFAULT_VAL_P);
        assertThat(testRegOpcConcAppz.getValK()).isEqualTo(DEFAULT_VAL_K);
        assertThat(testRegOpcConcAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcConcAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcConcAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcConcAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcConcAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcConcAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcConcAppzRepository.findAll().size();

        // Create the RegOpcConcAppz with an existing ID
        regOpcConcAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcConcAppzMockMvc.perform(post("/api/reg-opc-conc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConcAppz in the database
        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcConcAppzRepository.findAll().size();
        // set the field null
        regOpcConcAppz.setIdAzienda(null);

        // Create the RegOpcConcAppz, which fails.

        restRegOpcConcAppzMockMvc.perform(post("/api/reg-opc-conc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzs() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConcAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].valN").value(hasItem(DEFAULT_VAL_N.doubleValue())))
            .andExpect(jsonPath("$.[*].valP").value(hasItem(DEFAULT_VAL_P.doubleValue())))
            .andExpect(jsonPath("$.[*].valK").value(hasItem(DEFAULT_VAL_K.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcConcAppz() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get the regOpcConcAppz
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs/{id}", regOpcConcAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcConcAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.valN").value(DEFAULT_VAL_N.doubleValue()))
            .andExpect(jsonPath("$.valP").value(DEFAULT_VAL_P.doubleValue()))
            .andExpect(jsonPath("$.valK").value(DEFAULT_VAL_K.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda is not null
        defaultRegOpcConcAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcConcAppzList where idAzienda is null
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcConcAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcConcAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcConcAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd is not null
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcConcAppzList where idUnitaProd is null
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcConcAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcConcAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata is not null
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcConcAppzList where percentLavorata is null
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcConcAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcConcAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN equals to DEFAULT_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.equals=" + DEFAULT_VAL_N);

        // Get all the regOpcConcAppzList where valN equals to UPDATED_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.equals=" + UPDATED_VAL_N);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN in DEFAULT_VAL_N or UPDATED_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.in=" + DEFAULT_VAL_N + "," + UPDATED_VAL_N);

        // Get all the regOpcConcAppzList where valN equals to UPDATED_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.in=" + UPDATED_VAL_N);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN is not null
        defaultRegOpcConcAppzShouldBeFound("valN.specified=true");

        // Get all the regOpcConcAppzList where valN is null
        defaultRegOpcConcAppzShouldNotBeFound("valN.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN is greater than or equal to DEFAULT_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.greaterThanOrEqual=" + DEFAULT_VAL_N);

        // Get all the regOpcConcAppzList where valN is greater than or equal to UPDATED_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.greaterThanOrEqual=" + UPDATED_VAL_N);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN is less than or equal to DEFAULT_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.lessThanOrEqual=" + DEFAULT_VAL_N);

        // Get all the regOpcConcAppzList where valN is less than or equal to SMALLER_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.lessThanOrEqual=" + SMALLER_VAL_N);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN is less than DEFAULT_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.lessThan=" + DEFAULT_VAL_N);

        // Get all the regOpcConcAppzList where valN is less than UPDATED_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.lessThan=" + UPDATED_VAL_N);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValNIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valN is greater than DEFAULT_VAL_N
        defaultRegOpcConcAppzShouldNotBeFound("valN.greaterThan=" + DEFAULT_VAL_N);

        // Get all the regOpcConcAppzList where valN is greater than SMALLER_VAL_N
        defaultRegOpcConcAppzShouldBeFound("valN.greaterThan=" + SMALLER_VAL_N);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP equals to DEFAULT_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.equals=" + DEFAULT_VAL_P);

        // Get all the regOpcConcAppzList where valP equals to UPDATED_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.equals=" + UPDATED_VAL_P);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP in DEFAULT_VAL_P or UPDATED_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.in=" + DEFAULT_VAL_P + "," + UPDATED_VAL_P);

        // Get all the regOpcConcAppzList where valP equals to UPDATED_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.in=" + UPDATED_VAL_P);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP is not null
        defaultRegOpcConcAppzShouldBeFound("valP.specified=true");

        // Get all the regOpcConcAppzList where valP is null
        defaultRegOpcConcAppzShouldNotBeFound("valP.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP is greater than or equal to DEFAULT_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.greaterThanOrEqual=" + DEFAULT_VAL_P);

        // Get all the regOpcConcAppzList where valP is greater than or equal to UPDATED_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.greaterThanOrEqual=" + UPDATED_VAL_P);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP is less than or equal to DEFAULT_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.lessThanOrEqual=" + DEFAULT_VAL_P);

        // Get all the regOpcConcAppzList where valP is less than or equal to SMALLER_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.lessThanOrEqual=" + SMALLER_VAL_P);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP is less than DEFAULT_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.lessThan=" + DEFAULT_VAL_P);

        // Get all the regOpcConcAppzList where valP is less than UPDATED_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.lessThan=" + UPDATED_VAL_P);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValPIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valP is greater than DEFAULT_VAL_P
        defaultRegOpcConcAppzShouldNotBeFound("valP.greaterThan=" + DEFAULT_VAL_P);

        // Get all the regOpcConcAppzList where valP is greater than SMALLER_VAL_P
        defaultRegOpcConcAppzShouldBeFound("valP.greaterThan=" + SMALLER_VAL_P);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK equals to DEFAULT_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.equals=" + DEFAULT_VAL_K);

        // Get all the regOpcConcAppzList where valK equals to UPDATED_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.equals=" + UPDATED_VAL_K);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK in DEFAULT_VAL_K or UPDATED_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.in=" + DEFAULT_VAL_K + "," + UPDATED_VAL_K);

        // Get all the regOpcConcAppzList where valK equals to UPDATED_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.in=" + UPDATED_VAL_K);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK is not null
        defaultRegOpcConcAppzShouldBeFound("valK.specified=true");

        // Get all the regOpcConcAppzList where valK is null
        defaultRegOpcConcAppzShouldNotBeFound("valK.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK is greater than or equal to DEFAULT_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.greaterThanOrEqual=" + DEFAULT_VAL_K);

        // Get all the regOpcConcAppzList where valK is greater than or equal to UPDATED_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.greaterThanOrEqual=" + UPDATED_VAL_K);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK is less than or equal to DEFAULT_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.lessThanOrEqual=" + DEFAULT_VAL_K);

        // Get all the regOpcConcAppzList where valK is less than or equal to SMALLER_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.lessThanOrEqual=" + SMALLER_VAL_K);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK is less than DEFAULT_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.lessThan=" + DEFAULT_VAL_K);

        // Get all the regOpcConcAppzList where valK is less than UPDATED_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.lessThan=" + UPDATED_VAL_K);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByValKIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where valK is greater than DEFAULT_VAL_K
        defaultRegOpcConcAppzShouldNotBeFound("valK.greaterThan=" + DEFAULT_VAL_K);

        // Get all the regOpcConcAppzList where valK is greater than SMALLER_VAL_K
        defaultRegOpcConcAppzShouldBeFound("valK.greaterThan=" + SMALLER_VAL_K);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento is not null
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcConcAppzList where idAppezzamento is null
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcConcAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcConcAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali is not null
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcConcAppzList where dataInizVali is null
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcConcAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcConcAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali is not null
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcConcAppzList where dataFineVali is null
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcConcAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcConcAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator is not null
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcConcAppzList where userIdCreator is null
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcConcAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcConcAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod is not null
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcConcAppzList where userIdLastMod is null
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);

        // Get all the regOpcConcAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcConcAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcConcAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcConcAppzsByAppzToOpcConcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);
        RegOpcConc appzToOpcConc = RegOpcConcResourceIT.createEntity(em);
        em.persist(appzToOpcConc);
        em.flush();
        regOpcConcAppz.setAppzToOpcConc(appzToOpcConc);
        regOpcConcAppzRepository.saveAndFlush(regOpcConcAppz);
        Long appzToOpcConcId = appzToOpcConc.getId();

        // Get all the regOpcConcAppzList where appzToOpcConc equals to appzToOpcConcId
        defaultRegOpcConcAppzShouldBeFound("appzToOpcConcId.equals=" + appzToOpcConcId);

        // Get all the regOpcConcAppzList where appzToOpcConc equals to appzToOpcConcId + 1
        defaultRegOpcConcAppzShouldNotBeFound("appzToOpcConcId.equals=" + (appzToOpcConcId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcConcAppzShouldBeFound(String filter) throws Exception {
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcConcAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].valN").value(hasItem(DEFAULT_VAL_N.doubleValue())))
            .andExpect(jsonPath("$.[*].valP").value(hasItem(DEFAULT_VAL_P.doubleValue())))
            .andExpect(jsonPath("$.[*].valK").value(hasItem(DEFAULT_VAL_K.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcConcAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcConcAppz() throws Exception {
        // Get the regOpcConcAppz
        restRegOpcConcAppzMockMvc.perform(get("/api/reg-opc-conc-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcConcAppz() throws Exception {
        // Initialize the database
        regOpcConcAppzService.save(regOpcConcAppz);

        int databaseSizeBeforeUpdate = regOpcConcAppzRepository.findAll().size();

        // Update the regOpcConcAppz
        RegOpcConcAppz updatedRegOpcConcAppz = regOpcConcAppzRepository.findById(regOpcConcAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcConcAppz are not directly saved in db
        em.detach(updatedRegOpcConcAppz);
        updatedRegOpcConcAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .valN(UPDATED_VAL_N)
            .valP(UPDATED_VAL_P)
            .valK(UPDATED_VAL_K)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcConcAppzMockMvc.perform(put("/api/reg-opc-conc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcConcAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcConcAppz in the database
        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcConcAppz testRegOpcConcAppz = regOpcConcAppzList.get(regOpcConcAppzList.size() - 1);
        assertThat(testRegOpcConcAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcConcAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcConcAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcConcAppz.getValN()).isEqualTo(UPDATED_VAL_N);
        assertThat(testRegOpcConcAppz.getValP()).isEqualTo(UPDATED_VAL_P);
        assertThat(testRegOpcConcAppz.getValK()).isEqualTo(UPDATED_VAL_K);
        assertThat(testRegOpcConcAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcConcAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcConcAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcConcAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcConcAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcConcAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcConcAppzRepository.findAll().size();

        // Create the RegOpcConcAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcConcAppzMockMvc.perform(put("/api/reg-opc-conc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcConcAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcConcAppz in the database
        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcConcAppz() throws Exception {
        // Initialize the database
        regOpcConcAppzService.save(regOpcConcAppz);

        int databaseSizeBeforeDelete = regOpcConcAppzRepository.findAll().size();

        // Delete the regOpcConcAppz
        restRegOpcConcAppzMockMvc.perform(delete("/api/reg-opc-conc-appzs/{id}", regOpcConcAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcConcAppz> regOpcConcAppzList = regOpcConcAppzRepository.findAll();
        assertThat(regOpcConcAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcConcAppz.class);
        RegOpcConcAppz regOpcConcAppz1 = new RegOpcConcAppz();
        regOpcConcAppz1.setId(1L);
        RegOpcConcAppz regOpcConcAppz2 = new RegOpcConcAppz();
        regOpcConcAppz2.setId(regOpcConcAppz1.getId());
        assertThat(regOpcConcAppz1).isEqualTo(regOpcConcAppz2);
        regOpcConcAppz2.setId(2L);
        assertThat(regOpcConcAppz1).isNotEqualTo(regOpcConcAppz2);
        regOpcConcAppz1.setId(null);
        assertThat(regOpcConcAppz1).isNotEqualTo(regOpcConcAppz2);
    }
}
