package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcRacc;
import com.terrambe.domain.RegOpcRaccAppz;
import com.terrambe.domain.TipoRacc;
import com.terrambe.repository.RegOpcRaccRepository;
import com.terrambe.service.RegOpcRaccService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcRaccCriteria;
import com.terrambe.service.RegOpcRaccQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcRaccResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcRaccResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final String DEFAULT_LOCAZIONE_CONFERIMENTO = "AAAAAAAAAA";
    private static final String UPDATED_LOCAZIONE_CONFERIMENTO = "BBBBBBBBBB";

    private static final Float DEFAULT_QUANTITA = 1F;
    private static final Float UPDATED_QUANTITA = 2F;
    private static final Float SMALLER_QUANTITA = 1F - 1F;

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcRaccRepository regOpcRaccRepository;

    @Autowired
    private RegOpcRaccService regOpcRaccService;

    @Autowired
    private RegOpcRaccQueryService regOpcRaccQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcRaccMockMvc;

    private RegOpcRacc regOpcRacc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcRaccResource regOpcRaccResource = new RegOpcRaccResource(regOpcRaccService, regOpcRaccQueryService);
        this.restRegOpcRaccMockMvc = MockMvcBuilders.standaloneSetup(regOpcRaccResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcRacc createEntity(EntityManager em) {
        RegOpcRacc regOpcRacc = new RegOpcRacc()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .locazioneConferimento(DEFAULT_LOCAZIONE_CONFERIMENTO)
            .quantita(DEFAULT_QUANTITA)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcRacc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcRacc createUpdatedEntity(EntityManager em) {
        RegOpcRacc regOpcRacc = new RegOpcRacc()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .locazioneConferimento(UPDATED_LOCAZIONE_CONFERIMENTO)
            .quantita(UPDATED_QUANTITA)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcRacc;
    }

    @BeforeEach
    public void initTest() {
        regOpcRacc = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcRacc() throws Exception {
        int databaseSizeBeforeCreate = regOpcRaccRepository.findAll().size();

        // Create the RegOpcRacc
        restRegOpcRaccMockMvc.perform(post("/api/reg-opc-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRacc)))
            .andExpect(status().isCreated());

        // Validate the RegOpcRacc in the database
        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcRacc testRegOpcRacc = regOpcRaccList.get(regOpcRaccList.size() - 1);
        assertThat(testRegOpcRacc.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcRacc.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcRacc.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcRacc.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcRacc.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcRacc.getLocazioneConferimento()).isEqualTo(DEFAULT_LOCAZIONE_CONFERIMENTO);
        assertThat(testRegOpcRacc.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegOpcRacc.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcRacc.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcRacc.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcRacc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcRacc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcRacc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcRacc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcRacc.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcRaccWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcRaccRepository.findAll().size();

        // Create the RegOpcRacc with an existing ID
        regOpcRacc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcRaccMockMvc.perform(post("/api/reg-opc-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRacc)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcRacc in the database
        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcRaccRepository.findAll().size();
        // set the field null
        regOpcRacc.setIdAzienda(null);

        // Create the RegOpcRacc, which fails.

        restRegOpcRaccMockMvc.perform(post("/api/reg-opc-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRacc)))
            .andExpect(status().isBadRequest());

        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccs() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcRacc.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].locazioneConferimento").value(hasItem(DEFAULT_LOCAZIONE_CONFERIMENTO.toString())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcRacc() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get the regOpcRacc
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs/{id}", regOpcRacc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcRacc.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.locazioneConferimento").value(DEFAULT_LOCAZIONE_CONFERIMENTO.toString()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA.doubleValue()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda is not null
        defaultRegOpcRaccShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcRaccList where idAzienda is null
        defaultRegOpcRaccShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcRaccShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcRaccShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd is not null
        defaultRegOpcRaccShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcRaccList where idUnitaProd is null
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcRaccShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc is not null
        defaultRegOpcRaccShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcRaccList where dataInizOpc is null
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcRaccShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcRaccList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcRaccShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc is not null
        defaultRegOpcRaccShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcRaccList where dataFineOpc is null
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcRaccShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcRaccList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcRaccShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcRaccShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcRaccList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcRaccShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcRaccShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcRaccList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcRaccShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where tempoImpiegato is not null
        defaultRegOpcRaccShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcRaccList where tempoImpiegato is null
        defaultRegOpcRaccShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByLocazioneConferimentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where locazioneConferimento equals to DEFAULT_LOCAZIONE_CONFERIMENTO
        defaultRegOpcRaccShouldBeFound("locazioneConferimento.equals=" + DEFAULT_LOCAZIONE_CONFERIMENTO);

        // Get all the regOpcRaccList where locazioneConferimento equals to UPDATED_LOCAZIONE_CONFERIMENTO
        defaultRegOpcRaccShouldNotBeFound("locazioneConferimento.equals=" + UPDATED_LOCAZIONE_CONFERIMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByLocazioneConferimentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where locazioneConferimento in DEFAULT_LOCAZIONE_CONFERIMENTO or UPDATED_LOCAZIONE_CONFERIMENTO
        defaultRegOpcRaccShouldBeFound("locazioneConferimento.in=" + DEFAULT_LOCAZIONE_CONFERIMENTO + "," + UPDATED_LOCAZIONE_CONFERIMENTO);

        // Get all the regOpcRaccList where locazioneConferimento equals to UPDATED_LOCAZIONE_CONFERIMENTO
        defaultRegOpcRaccShouldNotBeFound("locazioneConferimento.in=" + UPDATED_LOCAZIONE_CONFERIMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByLocazioneConferimentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where locazioneConferimento is not null
        defaultRegOpcRaccShouldBeFound("locazioneConferimento.specified=true");

        // Get all the regOpcRaccList where locazioneConferimento is null
        defaultRegOpcRaccShouldNotBeFound("locazioneConferimento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita equals to DEFAULT_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regOpcRaccList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regOpcRaccList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita is not null
        defaultRegOpcRaccShouldBeFound("quantita.specified=true");

        // Get all the regOpcRaccList where quantita is null
        defaultRegOpcRaccShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcRaccList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcRaccList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita is less than DEFAULT_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcRaccList where quantita is less than UPDATED_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where quantita is greater than DEFAULT_QUANTITA
        defaultRegOpcRaccShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcRaccList where quantita is greater than SMALLER_QUANTITA
        defaultRegOpcRaccShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura is not null
        defaultRegOpcRaccShouldBeFound("idColtura.specified=true");

        // Get all the regOpcRaccList where idColtura is null
        defaultRegOpcRaccShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcRaccShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcRaccList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcRaccShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore is not null
        defaultRegOpcRaccShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcRaccList where idOperatore is null
        defaultRegOpcRaccShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcRaccShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcRaccList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcRaccShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo is not null
        defaultRegOpcRaccShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcRaccList where idMezzo is null
        defaultRegOpcRaccShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcRaccShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcRaccList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcRaccShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali is not null
        defaultRegOpcRaccShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcRaccList where dataInizVali is null
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcRaccShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali is not null
        defaultRegOpcRaccShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcRaccList where dataFineVali is null
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcRaccShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator is not null
        defaultRegOpcRaccShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcRaccList where userIdCreator is null
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcRaccShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod is not null
        defaultRegOpcRaccShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcRaccList where userIdLastMod is null
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);

        // Get all the regOpcRaccList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcRaccShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByOpcRaccToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);
        RegOpcRaccAppz opcRaccToAppz = RegOpcRaccAppzResourceIT.createEntity(em);
        em.persist(opcRaccToAppz);
        em.flush();
        regOpcRacc.addOpcRaccToAppz(opcRaccToAppz);
        regOpcRaccRepository.saveAndFlush(regOpcRacc);
        Long opcRaccToAppzId = opcRaccToAppz.getId();

        // Get all the regOpcRaccList where opcRaccToAppz equals to opcRaccToAppzId
        defaultRegOpcRaccShouldBeFound("opcRaccToAppzId.equals=" + opcRaccToAppzId);

        // Get all the regOpcRaccList where opcRaccToAppz equals to opcRaccToAppzId + 1
        defaultRegOpcRaccShouldNotBeFound("opcRaccToAppzId.equals=" + (opcRaccToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccsByOpcRaccToTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccRepository.saveAndFlush(regOpcRacc);
        TipoRacc opcRaccToTipo = TipoRaccResourceIT.createEntity(em);
        em.persist(opcRaccToTipo);
        em.flush();
        regOpcRacc.setOpcRaccToTipo(opcRaccToTipo);
        regOpcRaccRepository.saveAndFlush(regOpcRacc);
        Long opcRaccToTipoId = opcRaccToTipo.getId();

        // Get all the regOpcRaccList where opcRaccToTipo equals to opcRaccToTipoId
        defaultRegOpcRaccShouldBeFound("opcRaccToTipoId.equals=" + opcRaccToTipoId);

        // Get all the regOpcRaccList where opcRaccToTipo equals to opcRaccToTipoId + 1
        defaultRegOpcRaccShouldNotBeFound("opcRaccToTipoId.equals=" + (opcRaccToTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcRaccShouldBeFound(String filter) throws Exception {
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcRacc.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].locazioneConferimento").value(hasItem(DEFAULT_LOCAZIONE_CONFERIMENTO)))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcRaccShouldNotBeFound(String filter) throws Exception {
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcRacc() throws Exception {
        // Get the regOpcRacc
        restRegOpcRaccMockMvc.perform(get("/api/reg-opc-raccs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcRacc() throws Exception {
        // Initialize the database
        regOpcRaccService.save(regOpcRacc);

        int databaseSizeBeforeUpdate = regOpcRaccRepository.findAll().size();

        // Update the regOpcRacc
        RegOpcRacc updatedRegOpcRacc = regOpcRaccRepository.findById(regOpcRacc.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcRacc are not directly saved in db
        em.detach(updatedRegOpcRacc);
        updatedRegOpcRacc
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .locazioneConferimento(UPDATED_LOCAZIONE_CONFERIMENTO)
            .quantita(UPDATED_QUANTITA)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcRaccMockMvc.perform(put("/api/reg-opc-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcRacc)))
            .andExpect(status().isOk());

        // Validate the RegOpcRacc in the database
        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeUpdate);
        RegOpcRacc testRegOpcRacc = regOpcRaccList.get(regOpcRaccList.size() - 1);
        assertThat(testRegOpcRacc.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcRacc.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcRacc.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcRacc.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcRacc.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcRacc.getLocazioneConferimento()).isEqualTo(UPDATED_LOCAZIONE_CONFERIMENTO);
        assertThat(testRegOpcRacc.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegOpcRacc.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcRacc.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcRacc.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcRacc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcRacc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcRacc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcRacc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcRacc.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcRacc() throws Exception {
        int databaseSizeBeforeUpdate = regOpcRaccRepository.findAll().size();

        // Create the RegOpcRacc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcRaccMockMvc.perform(put("/api/reg-opc-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRacc)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcRacc in the database
        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcRacc() throws Exception {
        // Initialize the database
        regOpcRaccService.save(regOpcRacc);

        int databaseSizeBeforeDelete = regOpcRaccRepository.findAll().size();

        // Delete the regOpcRacc
        restRegOpcRaccMockMvc.perform(delete("/api/reg-opc-raccs/{id}", regOpcRacc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcRacc> regOpcRaccList = regOpcRaccRepository.findAll();
        assertThat(regOpcRaccList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcRacc.class);
        RegOpcRacc regOpcRacc1 = new RegOpcRacc();
        regOpcRacc1.setId(1L);
        RegOpcRacc regOpcRacc2 = new RegOpcRacc();
        regOpcRacc2.setId(regOpcRacc1.getId());
        assertThat(regOpcRacc1).isEqualTo(regOpcRacc2);
        regOpcRacc2.setId(2L);
        assertThat(regOpcRacc1).isNotEqualTo(regOpcRacc2);
        regOpcRacc1.setId(null);
        assertThat(regOpcRacc1).isNotEqualTo(regOpcRacc2);
    }
}
