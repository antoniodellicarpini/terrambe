package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcPio;
import com.terrambe.domain.RegOpcPioAppz;
import com.terrambe.repository.RegOpcPioRepository;
import com.terrambe.service.RegOpcPioService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcPioCriteria;
import com.terrambe.service.RegOpcPioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcPioResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcPioResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_OPERAZIONE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_OPERAZIONE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_OPERAZIONE = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcPioRepository regOpcPioRepository;

    @Autowired
    private RegOpcPioService regOpcPioService;

    @Autowired
    private RegOpcPioQueryService regOpcPioQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcPioMockMvc;

    private RegOpcPio regOpcPio;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcPioResource regOpcPioResource = new RegOpcPioResource(regOpcPioService, regOpcPioQueryService);
        this.restRegOpcPioMockMvc = MockMvcBuilders.standaloneSetup(regOpcPioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPio createEntity(EntityManager em) {
        RegOpcPio regOpcPio = new RegOpcPio()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataOperazione(DEFAULT_DATA_OPERAZIONE)
            .idColtura(DEFAULT_ID_COLTURA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcPio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPio createUpdatedEntity(EntityManager em) {
        RegOpcPio regOpcPio = new RegOpcPio()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .idColtura(UPDATED_ID_COLTURA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcPio;
    }

    @BeforeEach
    public void initTest() {
        regOpcPio = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcPio() throws Exception {
        int databaseSizeBeforeCreate = regOpcPioRepository.findAll().size();

        // Create the RegOpcPio
        restRegOpcPioMockMvc.perform(post("/api/reg-opc-pios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPio)))
            .andExpect(status().isCreated());

        // Validate the RegOpcPio in the database
        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcPio testRegOpcPio = regOpcPioList.get(regOpcPioList.size() - 1);
        assertThat(testRegOpcPio.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcPio.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcPio.getDataOperazione()).isEqualTo(DEFAULT_DATA_OPERAZIONE);
        assertThat(testRegOpcPio.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcPio.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcPio.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcPio.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcPio.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcPio.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcPioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcPioRepository.findAll().size();

        // Create the RegOpcPio with an existing ID
        regOpcPio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcPioMockMvc.perform(post("/api/reg-opc-pios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPio)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPio in the database
        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcPioRepository.findAll().size();
        // set the field null
        regOpcPio.setIdAzienda(null);

        // Create the RegOpcPio, which fails.

        restRegOpcPioMockMvc.perform(post("/api/reg-opc-pios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPio)))
            .andExpect(status().isBadRequest());

        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcPios() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPio.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcPio() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get the regOpcPio
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios/{id}", regOpcPio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcPio.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataOperazione").value(DEFAULT_DATA_OPERAZIONE.toString()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda is not null
        defaultRegOpcPioShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcPioList where idAzienda is null
        defaultRegOpcPioShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcPioShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcPioShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd is not null
        defaultRegOpcPioShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcPioList where idUnitaProd is null
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcPioShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione equals to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.equals=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.equals=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione in DEFAULT_DATA_OPERAZIONE or UPDATED_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.in=" + DEFAULT_DATA_OPERAZIONE + "," + UPDATED_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.in=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione is not null
        defaultRegOpcPioShouldBeFound("dataOperazione.specified=true");

        // Get all the regOpcPioList where dataOperazione is null
        defaultRegOpcPioShouldNotBeFound("dataOperazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione is greater than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.greaterThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione is greater than or equal to UPDATED_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.greaterThanOrEqual=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione is less than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.lessThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione is less than or equal to SMALLER_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.lessThanOrEqual=" + SMALLER_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione is less than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.lessThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione is less than UPDATED_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.lessThan=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataOperazioneIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataOperazione is greater than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcPioShouldNotBeFound("dataOperazione.greaterThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcPioList where dataOperazione is greater than SMALLER_DATA_OPERAZIONE
        defaultRegOpcPioShouldBeFound("dataOperazione.greaterThan=" + SMALLER_DATA_OPERAZIONE);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPioList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcPioList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura is not null
        defaultRegOpcPioShouldBeFound("idColtura.specified=true");

        // Get all the regOpcPioList where idColtura is null
        defaultRegOpcPioShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPioList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPioList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPioList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcPioShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPioList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcPioShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali is not null
        defaultRegOpcPioShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcPioList where dataInizVali is null
        defaultRegOpcPioShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcPioShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali is not null
        defaultRegOpcPioShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcPioList where dataFineVali is null
        defaultRegOpcPioShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcPioShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator is not null
        defaultRegOpcPioShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcPioList where userIdCreator is null
        defaultRegOpcPioShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcPioShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod is not null
        defaultRegOpcPioShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcPioList where userIdLastMod is null
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPiosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);

        // Get all the regOpcPioList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPioShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPiosByOpcPioToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioRepository.saveAndFlush(regOpcPio);
        RegOpcPioAppz opcPioToAppz = RegOpcPioAppzResourceIT.createEntity(em);
        em.persist(opcPioToAppz);
        em.flush();
        regOpcPio.addOpcPioToAppz(opcPioToAppz);
        regOpcPioRepository.saveAndFlush(regOpcPio);
        Long opcPioToAppzId = opcPioToAppz.getId();

        // Get all the regOpcPioList where opcPioToAppz equals to opcPioToAppzId
        defaultRegOpcPioShouldBeFound("opcPioToAppzId.equals=" + opcPioToAppzId);

        // Get all the regOpcPioList where opcPioToAppz equals to opcPioToAppzId + 1
        defaultRegOpcPioShouldNotBeFound("opcPioToAppzId.equals=" + (opcPioToAppzId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcPioShouldBeFound(String filter) throws Exception {
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPio.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcPioShouldNotBeFound(String filter) throws Exception {
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcPio() throws Exception {
        // Get the regOpcPio
        restRegOpcPioMockMvc.perform(get("/api/reg-opc-pios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcPio() throws Exception {
        // Initialize the database
        regOpcPioService.save(regOpcPio);

        int databaseSizeBeforeUpdate = regOpcPioRepository.findAll().size();

        // Update the regOpcPio
        RegOpcPio updatedRegOpcPio = regOpcPioRepository.findById(regOpcPio.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcPio are not directly saved in db
        em.detach(updatedRegOpcPio);
        updatedRegOpcPio
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .idColtura(UPDATED_ID_COLTURA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcPioMockMvc.perform(put("/api/reg-opc-pios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcPio)))
            .andExpect(status().isOk());

        // Validate the RegOpcPio in the database
        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeUpdate);
        RegOpcPio testRegOpcPio = regOpcPioList.get(regOpcPioList.size() - 1);
        assertThat(testRegOpcPio.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcPio.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcPio.getDataOperazione()).isEqualTo(UPDATED_DATA_OPERAZIONE);
        assertThat(testRegOpcPio.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcPio.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcPio.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcPio.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcPio.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcPio.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcPio() throws Exception {
        int databaseSizeBeforeUpdate = regOpcPioRepository.findAll().size();

        // Create the RegOpcPio

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcPioMockMvc.perform(put("/api/reg-opc-pios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPio)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPio in the database
        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcPio() throws Exception {
        // Initialize the database
        regOpcPioService.save(regOpcPio);

        int databaseSizeBeforeDelete = regOpcPioRepository.findAll().size();

        // Delete the regOpcPio
        restRegOpcPioMockMvc.perform(delete("/api/reg-opc-pios/{id}", regOpcPio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcPio> regOpcPioList = regOpcPioRepository.findAll();
        assertThat(regOpcPioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcPio.class);
        RegOpcPio regOpcPio1 = new RegOpcPio();
        regOpcPio1.setId(1L);
        RegOpcPio regOpcPio2 = new RegOpcPio();
        regOpcPio2.setId(regOpcPio1.getId());
        assertThat(regOpcPio1).isEqualTo(regOpcPio2);
        regOpcPio2.setId(2L);
        assertThat(regOpcPio1).isNotEqualTo(regOpcPio2);
        regOpcPio1.setId(null);
        assertThat(regOpcPio1).isNotEqualTo(regOpcPio2);
    }
}
