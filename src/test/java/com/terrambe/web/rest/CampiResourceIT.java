package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Campi;
import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.repository.CampiRepository;
import com.terrambe.service.CampiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.CampiCriteria;
import com.terrambe.service.CampiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CampiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class CampiResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final String DEFAULT_NOME_CAMPO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_CAMPO = "BBBBBBBBBB";

    private static final Float DEFAULT_SUPERFICIE_UTILIZZATA = 1F;
    private static final Float UPDATED_SUPERFICIE_UTILIZZATA = 2F;
    private static final Float SMALLER_SUPERFICIE_UTILIZZATA = 1F - 1F;

    private static final Float DEFAULT_SUP_CATASTALE = 1F;
    private static final Float UPDATED_SUP_CATASTALE = 2F;
    private static final Float SMALLER_SUP_CATASTALE = 1F - 1F;

    private static final Float DEFAULT_SUP_UTILIZZABILE = 1F;
    private static final Float UPDATED_SUP_UTILIZZABILE = 2F;
    private static final Float SMALLER_SUP_UTILIZZABILE = 1F - 1F;

    private static final String DEFAULT_UUID_ESRI = "AAAAAAAAAA";
    private static final String UPDATED_UUID_ESRI = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private CampiRepository campiRepository;

    @Autowired
    private CampiService campiService;

    @Autowired
    private CampiQueryService campiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCampiMockMvc;

    private Campi campi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampiResource campiResource = new CampiResource(campiService, campiQueryService);
        this.restCampiMockMvc = MockMvcBuilders.standaloneSetup(campiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campi createEntity(EntityManager em) {
        Campi campi = new Campi()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .nomeCampo(DEFAULT_NOME_CAMPO)
            .superficieUtilizzata(DEFAULT_SUPERFICIE_UTILIZZATA)
            .supCatastale(DEFAULT_SUP_CATASTALE)
            .supUtilizzabile(DEFAULT_SUP_UTILIZZABILE)
            .uuidEsri(DEFAULT_UUID_ESRI)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return campi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campi createUpdatedEntity(EntityManager em) {
        Campi campi = new Campi()
            .idAzienda(UPDATED_ID_AZIENDA)
            .nomeCampo(UPDATED_NOME_CAMPO)
            .superficieUtilizzata(UPDATED_SUPERFICIE_UTILIZZATA)
            .supCatastale(UPDATED_SUP_CATASTALE)
            .supUtilizzabile(UPDATED_SUP_UTILIZZABILE)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return campi;
    }

    @BeforeEach
    public void initTest() {
        campi = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampi() throws Exception {
        int databaseSizeBeforeCreate = campiRepository.findAll().size();

        // Create the Campi
        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isCreated());

        // Validate the Campi in the database
        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeCreate + 1);
        Campi testCampi = campiList.get(campiList.size() - 1);
        assertThat(testCampi.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testCampi.getNomeCampo()).isEqualTo(DEFAULT_NOME_CAMPO);
        assertThat(testCampi.getSuperficieUtilizzata()).isEqualTo(DEFAULT_SUPERFICIE_UTILIZZATA);
        assertThat(testCampi.getSupCatastale()).isEqualTo(DEFAULT_SUP_CATASTALE);
        assertThat(testCampi.getSupUtilizzabile()).isEqualTo(DEFAULT_SUP_UTILIZZABILE);
        assertThat(testCampi.getUuidEsri()).isEqualTo(DEFAULT_UUID_ESRI);
        assertThat(testCampi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testCampi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testCampi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testCampi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testCampi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createCampiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campiRepository.findAll().size();

        // Create the Campi with an existing ID
        campi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        // Validate the Campi in the database
        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setIdAzienda(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeCampoIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setNomeCampo(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSuperficieUtilizzataIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setSuperficieUtilizzata(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupCatastaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setSupCatastale(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupUtilizzabileIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setSupUtilizzabile(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUuidEsriIsRequired() throws Exception {
        int databaseSizeBeforeTest = campiRepository.findAll().size();
        // set the field null
        campi.setUuidEsri(null);

        // Create the Campi, which fails.

        restCampiMockMvc.perform(post("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCampis() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList
        restCampiMockMvc.perform(get("/api/campis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campi.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].nomeCampo").value(hasItem(DEFAULT_NOME_CAMPO.toString())))
            .andExpect(jsonPath("$.[*].superficieUtilizzata").value(hasItem(DEFAULT_SUPERFICIE_UTILIZZATA.doubleValue())))
            .andExpect(jsonPath("$.[*].supCatastale").value(hasItem(DEFAULT_SUP_CATASTALE.doubleValue())))
            .andExpect(jsonPath("$.[*].supUtilizzabile").value(hasItem(DEFAULT_SUP_UTILIZZABILE.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getCampi() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get the campi
        restCampiMockMvc.perform(get("/api/campis/{id}", campi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campi.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.nomeCampo").value(DEFAULT_NOME_CAMPO.toString()))
            .andExpect(jsonPath("$.superficieUtilizzata").value(DEFAULT_SUPERFICIE_UTILIZZATA.doubleValue()))
            .andExpect(jsonPath("$.supCatastale").value(DEFAULT_SUP_CATASTALE.doubleValue()))
            .andExpect(jsonPath("$.supUtilizzabile").value(DEFAULT_SUP_UTILIZZABILE.doubleValue()))
            .andExpect(jsonPath("$.uuidEsri").value(DEFAULT_UUID_ESRI.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the campiList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the campiList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda is not null
        defaultCampiShouldBeFound("idAzienda.specified=true");

        // Get all the campiList where idAzienda is null
        defaultCampiShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the campiList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the campiList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the campiList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllCampisByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultCampiShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the campiList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultCampiShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllCampisByNomeCampoIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where nomeCampo equals to DEFAULT_NOME_CAMPO
        defaultCampiShouldBeFound("nomeCampo.equals=" + DEFAULT_NOME_CAMPO);

        // Get all the campiList where nomeCampo equals to UPDATED_NOME_CAMPO
        defaultCampiShouldNotBeFound("nomeCampo.equals=" + UPDATED_NOME_CAMPO);
    }

    @Test
    @Transactional
    public void getAllCampisByNomeCampoIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where nomeCampo in DEFAULT_NOME_CAMPO or UPDATED_NOME_CAMPO
        defaultCampiShouldBeFound("nomeCampo.in=" + DEFAULT_NOME_CAMPO + "," + UPDATED_NOME_CAMPO);

        // Get all the campiList where nomeCampo equals to UPDATED_NOME_CAMPO
        defaultCampiShouldNotBeFound("nomeCampo.in=" + UPDATED_NOME_CAMPO);
    }

    @Test
    @Transactional
    public void getAllCampisByNomeCampoIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where nomeCampo is not null
        defaultCampiShouldBeFound("nomeCampo.specified=true");

        // Get all the campiList where nomeCampo is null
        defaultCampiShouldNotBeFound("nomeCampo.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata equals to DEFAULT_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.equals=" + DEFAULT_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata equals to UPDATED_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.equals=" + UPDATED_SUPERFICIE_UTILIZZATA);
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata in DEFAULT_SUPERFICIE_UTILIZZATA or UPDATED_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.in=" + DEFAULT_SUPERFICIE_UTILIZZATA + "," + UPDATED_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata equals to UPDATED_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.in=" + UPDATED_SUPERFICIE_UTILIZZATA);
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata is not null
        defaultCampiShouldBeFound("superficieUtilizzata.specified=true");

        // Get all the campiList where superficieUtilizzata is null
        defaultCampiShouldNotBeFound("superficieUtilizzata.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata is greater than or equal to DEFAULT_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.greaterThanOrEqual=" + DEFAULT_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata is greater than or equal to UPDATED_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.greaterThanOrEqual=" + UPDATED_SUPERFICIE_UTILIZZATA);
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata is less than or equal to DEFAULT_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.lessThanOrEqual=" + DEFAULT_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata is less than or equal to SMALLER_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.lessThanOrEqual=" + SMALLER_SUPERFICIE_UTILIZZATA);
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata is less than DEFAULT_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.lessThan=" + DEFAULT_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata is less than UPDATED_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.lessThan=" + UPDATED_SUPERFICIE_UTILIZZATA);
    }

    @Test
    @Transactional
    public void getAllCampisBySuperficieUtilizzataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where superficieUtilizzata is greater than DEFAULT_SUPERFICIE_UTILIZZATA
        defaultCampiShouldNotBeFound("superficieUtilizzata.greaterThan=" + DEFAULT_SUPERFICIE_UTILIZZATA);

        // Get all the campiList where superficieUtilizzata is greater than SMALLER_SUPERFICIE_UTILIZZATA
        defaultCampiShouldBeFound("superficieUtilizzata.greaterThan=" + SMALLER_SUPERFICIE_UTILIZZATA);
    }


    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale equals to DEFAULT_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.equals=" + DEFAULT_SUP_CATASTALE);

        // Get all the campiList where supCatastale equals to UPDATED_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.equals=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale in DEFAULT_SUP_CATASTALE or UPDATED_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.in=" + DEFAULT_SUP_CATASTALE + "," + UPDATED_SUP_CATASTALE);

        // Get all the campiList where supCatastale equals to UPDATED_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.in=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale is not null
        defaultCampiShouldBeFound("supCatastale.specified=true");

        // Get all the campiList where supCatastale is null
        defaultCampiShouldNotBeFound("supCatastale.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale is greater than or equal to DEFAULT_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.greaterThanOrEqual=" + DEFAULT_SUP_CATASTALE);

        // Get all the campiList where supCatastale is greater than or equal to UPDATED_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.greaterThanOrEqual=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale is less than or equal to DEFAULT_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.lessThanOrEqual=" + DEFAULT_SUP_CATASTALE);

        // Get all the campiList where supCatastale is less than or equal to SMALLER_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.lessThanOrEqual=" + SMALLER_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale is less than DEFAULT_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.lessThan=" + DEFAULT_SUP_CATASTALE);

        // Get all the campiList where supCatastale is less than UPDATED_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.lessThan=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupCatastaleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supCatastale is greater than DEFAULT_SUP_CATASTALE
        defaultCampiShouldNotBeFound("supCatastale.greaterThan=" + DEFAULT_SUP_CATASTALE);

        // Get all the campiList where supCatastale is greater than SMALLER_SUP_CATASTALE
        defaultCampiShouldBeFound("supCatastale.greaterThan=" + SMALLER_SUP_CATASTALE);
    }


    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile equals to DEFAULT_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.equals=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile equals to UPDATED_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.equals=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile in DEFAULT_SUP_UTILIZZABILE or UPDATED_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.in=" + DEFAULT_SUP_UTILIZZABILE + "," + UPDATED_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile equals to UPDATED_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.in=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile is not null
        defaultCampiShouldBeFound("supUtilizzabile.specified=true");

        // Get all the campiList where supUtilizzabile is null
        defaultCampiShouldNotBeFound("supUtilizzabile.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile is greater than or equal to DEFAULT_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.greaterThanOrEqual=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile is greater than or equal to UPDATED_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.greaterThanOrEqual=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile is less than or equal to DEFAULT_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.lessThanOrEqual=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile is less than or equal to SMALLER_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.lessThanOrEqual=" + SMALLER_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile is less than DEFAULT_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.lessThan=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile is less than UPDATED_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.lessThan=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllCampisBySupUtilizzabileIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where supUtilizzabile is greater than DEFAULT_SUP_UTILIZZABILE
        defaultCampiShouldNotBeFound("supUtilizzabile.greaterThan=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the campiList where supUtilizzabile is greater than SMALLER_SUP_UTILIZZABILE
        defaultCampiShouldBeFound("supUtilizzabile.greaterThan=" + SMALLER_SUP_UTILIZZABILE);
    }


    @Test
    @Transactional
    public void getAllCampisByUuidEsriIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where uuidEsri equals to DEFAULT_UUID_ESRI
        defaultCampiShouldBeFound("uuidEsri.equals=" + DEFAULT_UUID_ESRI);

        // Get all the campiList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultCampiShouldNotBeFound("uuidEsri.equals=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllCampisByUuidEsriIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where uuidEsri in DEFAULT_UUID_ESRI or UPDATED_UUID_ESRI
        defaultCampiShouldBeFound("uuidEsri.in=" + DEFAULT_UUID_ESRI + "," + UPDATED_UUID_ESRI);

        // Get all the campiList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultCampiShouldNotBeFound("uuidEsri.in=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllCampisByUuidEsriIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where uuidEsri is not null
        defaultCampiShouldBeFound("uuidEsri.specified=true");

        // Get all the campiList where uuidEsri is null
        defaultCampiShouldNotBeFound("uuidEsri.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali is not null
        defaultCampiShouldBeFound("dataInizVali.specified=true");

        // Get all the campiList where dataInizVali is null
        defaultCampiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultCampiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the campiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultCampiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali is not null
        defaultCampiShouldBeFound("dataFineVali.specified=true");

        // Get all the campiList where dataFineVali is null
        defaultCampiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllCampisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultCampiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the campiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultCampiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator is not null
        defaultCampiShouldBeFound("userIdCreator.specified=true");

        // Get all the campiList where userIdCreator is null
        defaultCampiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultCampiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the campiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultCampiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod is not null
        defaultCampiShouldBeFound("userIdLastMod.specified=true");

        // Get all the campiList where userIdLastMod is null
        defaultCampiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllCampisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);

        // Get all the campiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultCampiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the campiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultCampiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllCampisByCampiToAziSupIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);
        AziSuperficieParticella campiToAziSup = AziSuperficieParticellaResourceIT.createEntity(em);
        em.persist(campiToAziSup);
        em.flush();
        campi.addCampiToAziSup(campiToAziSup);
        campiRepository.saveAndFlush(campi);
        Long campiToAziSupId = campiToAziSup.getId();

        // Get all the campiList where campiToAziSup equals to campiToAziSupId
        defaultCampiShouldBeFound("campiToAziSupId.equals=" + campiToAziSupId);

        // Get all the campiList where campiToAziSup equals to campiToAziSupId + 1
        defaultCampiShouldNotBeFound("campiToAziSupId.equals=" + (campiToAziSupId + 1));
    }


    @Test
    @Transactional
    public void getAllCampisByCampiToAppezIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);
        Appezzamenti campiToAppez = AppezzamentiResourceIT.createEntity(em);
        em.persist(campiToAppez);
        em.flush();
        campi.addCampiToAppez(campiToAppez);
        campiRepository.saveAndFlush(campi);
        Long campiToAppezId = campiToAppez.getId();

        // Get all the campiList where campiToAppez equals to campiToAppezId
        defaultCampiShouldBeFound("campiToAppezId.equals=" + campiToAppezId);

        // Get all the campiList where campiToAppez equals to campiToAppezId + 1
        defaultCampiShouldNotBeFound("campiToAppezId.equals=" + (campiToAppezId + 1));
    }


    @Test
    @Transactional
    public void getAllCampisByCampiuniIsEqualToSomething() throws Exception {
        // Initialize the database
        campiRepository.saveAndFlush(campi);
        UniAnagrafica campiuni = UniAnagraficaResourceIT.createEntity(em);
        em.persist(campiuni);
        em.flush();
        campi.setCampiuni(campiuni);
        campiRepository.saveAndFlush(campi);
        Long campiuniId = campiuni.getId();

        // Get all the campiList where campiuni equals to campiuniId
        defaultCampiShouldBeFound("campiuniId.equals=" + campiuniId);

        // Get all the campiList where campiuni equals to campiuniId + 1
        defaultCampiShouldNotBeFound("campiuniId.equals=" + (campiuniId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCampiShouldBeFound(String filter) throws Exception {
        restCampiMockMvc.perform(get("/api/campis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campi.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].nomeCampo").value(hasItem(DEFAULT_NOME_CAMPO)))
            .andExpect(jsonPath("$.[*].superficieUtilizzata").value(hasItem(DEFAULT_SUPERFICIE_UTILIZZATA.doubleValue())))
            .andExpect(jsonPath("$.[*].supCatastale").value(hasItem(DEFAULT_SUP_CATASTALE.doubleValue())))
            .andExpect(jsonPath("$.[*].supUtilizzabile").value(hasItem(DEFAULT_SUP_UTILIZZABILE.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restCampiMockMvc.perform(get("/api/campis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCampiShouldNotBeFound(String filter) throws Exception {
        restCampiMockMvc.perform(get("/api/campis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCampiMockMvc.perform(get("/api/campis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCampi() throws Exception {
        // Get the campi
        restCampiMockMvc.perform(get("/api/campis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampi() throws Exception {
        // Initialize the database
        campiService.save(campi);

        int databaseSizeBeforeUpdate = campiRepository.findAll().size();

        // Update the campi
        Campi updatedCampi = campiRepository.findById(campi.getId()).get();
        // Disconnect from session so that the updates on updatedCampi are not directly saved in db
        em.detach(updatedCampi);
        updatedCampi
            .idAzienda(UPDATED_ID_AZIENDA)
            .nomeCampo(UPDATED_NOME_CAMPO)
            .superficieUtilizzata(UPDATED_SUPERFICIE_UTILIZZATA)
            .supCatastale(UPDATED_SUP_CATASTALE)
            .supUtilizzabile(UPDATED_SUP_UTILIZZABILE)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restCampiMockMvc.perform(put("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCampi)))
            .andExpect(status().isOk());

        // Validate the Campi in the database
        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeUpdate);
        Campi testCampi = campiList.get(campiList.size() - 1);
        assertThat(testCampi.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testCampi.getNomeCampo()).isEqualTo(UPDATED_NOME_CAMPO);
        assertThat(testCampi.getSuperficieUtilizzata()).isEqualTo(UPDATED_SUPERFICIE_UTILIZZATA);
        assertThat(testCampi.getSupCatastale()).isEqualTo(UPDATED_SUP_CATASTALE);
        assertThat(testCampi.getSupUtilizzabile()).isEqualTo(UPDATED_SUP_UTILIZZABILE);
        assertThat(testCampi.getUuidEsri()).isEqualTo(UPDATED_UUID_ESRI);
        assertThat(testCampi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testCampi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testCampi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testCampi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testCampi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingCampi() throws Exception {
        int databaseSizeBeforeUpdate = campiRepository.findAll().size();

        // Create the Campi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampiMockMvc.perform(put("/api/campis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campi)))
            .andExpect(status().isBadRequest());

        // Validate the Campi in the database
        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampi() throws Exception {
        // Initialize the database
        campiService.save(campi);

        int databaseSizeBeforeDelete = campiRepository.findAll().size();

        // Delete the campi
        restCampiMockMvc.perform(delete("/api/campis/{id}", campi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Campi> campiList = campiRepository.findAll();
        assertThat(campiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Campi.class);
        Campi campi1 = new Campi();
        campi1.setId(1L);
        Campi campi2 = new Campi();
        campi2.setId(campi1.getId());
        assertThat(campi1).isEqualTo(campi2);
        campi2.setId(2L);
        assertThat(campi1).isNotEqualTo(campi2);
        campi1.setId(null);
        assertThat(campi1).isNotEqualTo(campi2);
    }
}
