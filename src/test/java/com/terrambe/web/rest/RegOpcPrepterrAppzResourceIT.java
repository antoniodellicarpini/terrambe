package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcPrepterrAppz;
import com.terrambe.domain.RegOpcPrepterr;
import com.terrambe.repository.RegOpcPrepterrAppzRepository;
import com.terrambe.service.RegOpcPrepterrAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcPrepterrAppzCriteria;
import com.terrambe.service.RegOpcPrepterrAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcPrepterrAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcPrepterrAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcPrepterrAppzRepository regOpcPrepterrAppzRepository;

    @Autowired
    private RegOpcPrepterrAppzService regOpcPrepterrAppzService;

    @Autowired
    private RegOpcPrepterrAppzQueryService regOpcPrepterrAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcPrepterrAppzMockMvc;

    private RegOpcPrepterrAppz regOpcPrepterrAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcPrepterrAppzResource regOpcPrepterrAppzResource = new RegOpcPrepterrAppzResource(regOpcPrepterrAppzService, regOpcPrepterrAppzQueryService);
        this.restRegOpcPrepterrAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcPrepterrAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPrepterrAppz createEntity(EntityManager em) {
        RegOpcPrepterrAppz regOpcPrepterrAppz = new RegOpcPrepterrAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcPrepterrAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPrepterrAppz createUpdatedEntity(EntityManager em) {
        RegOpcPrepterrAppz regOpcPrepterrAppz = new RegOpcPrepterrAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcPrepterrAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcPrepterrAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcPrepterrAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcPrepterrAppzRepository.findAll().size();

        // Create the RegOpcPrepterrAppz
        restRegOpcPrepterrAppzMockMvc.perform(post("/api/reg-opc-prepterr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterrAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcPrepterrAppz in the database
        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcPrepterrAppz testRegOpcPrepterrAppz = regOpcPrepterrAppzList.get(regOpcPrepterrAppzList.size() - 1);
        assertThat(testRegOpcPrepterrAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcPrepterrAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcPrepterrAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcPrepterrAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcPrepterrAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcPrepterrAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcPrepterrAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcPrepterrAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcPrepterrAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcPrepterrAppzRepository.findAll().size();

        // Create the RegOpcPrepterrAppz with an existing ID
        regOpcPrepterrAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcPrepterrAppzMockMvc.perform(post("/api/reg-opc-prepterr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterrAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPrepterrAppz in the database
        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcPrepterrAppzRepository.findAll().size();
        // set the field null
        regOpcPrepterrAppz.setIdAzienda(null);

        // Create the RegOpcPrepterrAppz, which fails.

        restRegOpcPrepterrAppzMockMvc.perform(post("/api/reg-opc-prepterr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterrAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzs() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPrepterrAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcPrepterrAppz() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get the regOpcPrepterrAppz
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs/{id}", regOpcPrepterrAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcPrepterrAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda is not null
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcPrepterrAppzList where idAzienda is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcPrepterrAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is not null
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcPrepterrAppzList where idUnitaProd is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcPrepterrAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata is not null
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcPrepterrAppzList where percentLavorata is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcPrepterrAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcPrepterrAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is not null
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcPrepterrAppzList where idAppezzamento is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPrepterrAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcPrepterrAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali is not null
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcPrepterrAppzList where dataInizVali is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali is not null
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcPrepterrAppzList where dataFineVali is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcPrepterrAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator is not null
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcPrepterrAppzList where userIdCreator is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcPrepterrAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is not null
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcPrepterrAppzList where userIdLastMod is null
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPrepterrAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrAppzsByAppzToOpcPrepterrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);
        RegOpcPrepterr appzToOpcPrepterr = RegOpcPrepterrResourceIT.createEntity(em);
        em.persist(appzToOpcPrepterr);
        em.flush();
        regOpcPrepterrAppz.setAppzToOpcPrepterr(appzToOpcPrepterr);
        regOpcPrepterrAppzRepository.saveAndFlush(regOpcPrepterrAppz);
        Long appzToOpcPrepterrId = appzToOpcPrepterr.getId();

        // Get all the regOpcPrepterrAppzList where appzToOpcPrepterr equals to appzToOpcPrepterrId
        defaultRegOpcPrepterrAppzShouldBeFound("appzToOpcPrepterrId.equals=" + appzToOpcPrepterrId);

        // Get all the regOpcPrepterrAppzList where appzToOpcPrepterr equals to appzToOpcPrepterrId + 1
        defaultRegOpcPrepterrAppzShouldNotBeFound("appzToOpcPrepterrId.equals=" + (appzToOpcPrepterrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcPrepterrAppzShouldBeFound(String filter) throws Exception {
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPrepterrAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcPrepterrAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcPrepterrAppz() throws Exception {
        // Get the regOpcPrepterrAppz
        restRegOpcPrepterrAppzMockMvc.perform(get("/api/reg-opc-prepterr-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcPrepterrAppz() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzService.save(regOpcPrepterrAppz);

        int databaseSizeBeforeUpdate = regOpcPrepterrAppzRepository.findAll().size();

        // Update the regOpcPrepterrAppz
        RegOpcPrepterrAppz updatedRegOpcPrepterrAppz = regOpcPrepterrAppzRepository.findById(regOpcPrepterrAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcPrepterrAppz are not directly saved in db
        em.detach(updatedRegOpcPrepterrAppz);
        updatedRegOpcPrepterrAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcPrepterrAppzMockMvc.perform(put("/api/reg-opc-prepterr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcPrepterrAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcPrepterrAppz in the database
        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcPrepterrAppz testRegOpcPrepterrAppz = regOpcPrepterrAppzList.get(regOpcPrepterrAppzList.size() - 1);
        assertThat(testRegOpcPrepterrAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcPrepterrAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcPrepterrAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcPrepterrAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcPrepterrAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcPrepterrAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcPrepterrAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcPrepterrAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcPrepterrAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcPrepterrAppzRepository.findAll().size();

        // Create the RegOpcPrepterrAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcPrepterrAppzMockMvc.perform(put("/api/reg-opc-prepterr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterrAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPrepterrAppz in the database
        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcPrepterrAppz() throws Exception {
        // Initialize the database
        regOpcPrepterrAppzService.save(regOpcPrepterrAppz);

        int databaseSizeBeforeDelete = regOpcPrepterrAppzRepository.findAll().size();

        // Delete the regOpcPrepterrAppz
        restRegOpcPrepterrAppzMockMvc.perform(delete("/api/reg-opc-prepterr-appzs/{id}", regOpcPrepterrAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcPrepterrAppz> regOpcPrepterrAppzList = regOpcPrepterrAppzRepository.findAll();
        assertThat(regOpcPrepterrAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcPrepterrAppz.class);
        RegOpcPrepterrAppz regOpcPrepterrAppz1 = new RegOpcPrepterrAppz();
        regOpcPrepterrAppz1.setId(1L);
        RegOpcPrepterrAppz regOpcPrepterrAppz2 = new RegOpcPrepterrAppz();
        regOpcPrepterrAppz2.setId(regOpcPrepterrAppz1.getId());
        assertThat(regOpcPrepterrAppz1).isEqualTo(regOpcPrepterrAppz2);
        regOpcPrepterrAppz2.setId(2L);
        assertThat(regOpcPrepterrAppz1).isNotEqualTo(regOpcPrepterrAppz2);
        regOpcPrepterrAppz1.setId(null);
        assertThat(regOpcPrepterrAppz1).isNotEqualTo(regOpcPrepterrAppz2);
    }
}
