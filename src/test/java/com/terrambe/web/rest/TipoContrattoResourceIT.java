package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoContratto;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.TipoContrattoRepository;
import com.terrambe.service.TipoContrattoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoContrattoCriteria;
import com.terrambe.service.TipoContrattoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoContrattoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoContrattoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipoContrattoRepository tipoContrattoRepository;

    @Autowired
    private TipoContrattoService tipoContrattoService;

    @Autowired
    private TipoContrattoQueryService tipoContrattoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoContrattoMockMvc;

    private TipoContratto tipoContratto;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoContrattoResource tipoContrattoResource = new TipoContrattoResource(tipoContrattoService, tipoContrattoQueryService);
        this.restTipoContrattoMockMvc = MockMvcBuilders.standaloneSetup(tipoContrattoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoContratto createEntity(EntityManager em) {
        TipoContratto tipoContratto = new TipoContratto()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipoContratto;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoContratto createUpdatedEntity(EntityManager em) {
        TipoContratto tipoContratto = new TipoContratto()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipoContratto;
    }

    @BeforeEach
    public void initTest() {
        tipoContratto = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoContratto() throws Exception {
        int databaseSizeBeforeCreate = tipoContrattoRepository.findAll().size();

        // Create the TipoContratto
        restTipoContrattoMockMvc.perform(post("/api/tipo-contrattos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoContratto)))
            .andExpect(status().isCreated());

        // Validate the TipoContratto in the database
        List<TipoContratto> tipoContrattoList = tipoContrattoRepository.findAll();
        assertThat(tipoContrattoList).hasSize(databaseSizeBeforeCreate + 1);
        TipoContratto testTipoContratto = tipoContrattoList.get(tipoContrattoList.size() - 1);
        assertThat(testTipoContratto.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoContratto.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoContratto.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoContratto.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoContratto.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipoContratto.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipoContrattoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoContrattoRepository.findAll().size();

        // Create the TipoContratto with an existing ID
        tipoContratto.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoContrattoMockMvc.perform(post("/api/tipo-contrattos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoContratto)))
            .andExpect(status().isBadRequest());

        // Validate the TipoContratto in the database
        List<TipoContratto> tipoContrattoList = tipoContrattoRepository.findAll();
        assertThat(tipoContrattoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoContrattos() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoContratto.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipoContratto() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get the tipoContratto
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos/{id}", tipoContratto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoContratto.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoContrattoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoContrattoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoContrattoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoContrattoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoContrattoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoContrattoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where descrizione is not null
        defaultTipoContrattoShouldBeFound("descrizione.specified=true");

        // Get all the tipoContrattoList where descrizione is null
        defaultTipoContrattoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali is not null
        defaultTipoContrattoShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoContrattoList where dataInizVali is null
        defaultTipoContrattoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoContrattoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoContrattoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoContrattoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali is not null
        defaultTipoContrattoShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoContrattoList where dataFineVali is null
        defaultTipoContrattoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoContrattoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoContrattoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoContrattoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator is not null
        defaultTipoContrattoShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoContrattoList where userIdCreator is null
        defaultTipoContrattoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoContrattoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoContrattoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoContrattoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod is not null
        defaultTipoContrattoShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoContrattoList where userIdLastMod is null
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoContrattosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);

        // Get all the tipoContrattoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoContrattoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoContrattoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoContrattoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoContrattosByTipoContratToOpeIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoContrattoRepository.saveAndFlush(tipoContratto);
        OpeAnagrafica tipoContratToOpe = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(tipoContratToOpe);
        em.flush();
        tipoContratto.addTipoContratToOpe(tipoContratToOpe);
        tipoContrattoRepository.saveAndFlush(tipoContratto);
        Long tipoContratToOpeId = tipoContratToOpe.getId();

        // Get all the tipoContrattoList where tipoContratToOpe equals to tipoContratToOpeId
        defaultTipoContrattoShouldBeFound("tipoContratToOpeId.equals=" + tipoContratToOpeId);

        // Get all the tipoContrattoList where tipoContratToOpe equals to tipoContratToOpeId + 1
        defaultTipoContrattoShouldNotBeFound("tipoContratToOpeId.equals=" + (tipoContratToOpeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoContrattoShouldBeFound(String filter) throws Exception {
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoContratto.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoContrattoShouldNotBeFound(String filter) throws Exception {
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoContratto() throws Exception {
        // Get the tipoContratto
        restTipoContrattoMockMvc.perform(get("/api/tipo-contrattos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoContratto() throws Exception {
        // Initialize the database
        tipoContrattoService.save(tipoContratto);

        int databaseSizeBeforeUpdate = tipoContrattoRepository.findAll().size();

        // Update the tipoContratto
        TipoContratto updatedTipoContratto = tipoContrattoRepository.findById(tipoContratto.getId()).get();
        // Disconnect from session so that the updates on updatedTipoContratto are not directly saved in db
        em.detach(updatedTipoContratto);
        updatedTipoContratto
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipoContrattoMockMvc.perform(put("/api/tipo-contrattos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoContratto)))
            .andExpect(status().isOk());

        // Validate the TipoContratto in the database
        List<TipoContratto> tipoContrattoList = tipoContrattoRepository.findAll();
        assertThat(tipoContrattoList).hasSize(databaseSizeBeforeUpdate);
        TipoContratto testTipoContratto = tipoContrattoList.get(tipoContrattoList.size() - 1);
        assertThat(testTipoContratto.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoContratto.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoContratto.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoContratto.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoContratto.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipoContratto.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoContratto() throws Exception {
        int databaseSizeBeforeUpdate = tipoContrattoRepository.findAll().size();

        // Create the TipoContratto

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoContrattoMockMvc.perform(put("/api/tipo-contrattos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoContratto)))
            .andExpect(status().isBadRequest());

        // Validate the TipoContratto in the database
        List<TipoContratto> tipoContrattoList = tipoContrattoRepository.findAll();
        assertThat(tipoContrattoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoContratto() throws Exception {
        // Initialize the database
        tipoContrattoService.save(tipoContratto);

        int databaseSizeBeforeDelete = tipoContrattoRepository.findAll().size();

        // Delete the tipoContratto
        restTipoContrattoMockMvc.perform(delete("/api/tipo-contrattos/{id}", tipoContratto.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoContratto> tipoContrattoList = tipoContrattoRepository.findAll();
        assertThat(tipoContrattoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoContratto.class);
        TipoContratto tipoContratto1 = new TipoContratto();
        tipoContratto1.setId(1L);
        TipoContratto tipoContratto2 = new TipoContratto();
        tipoContratto2.setId(tipoContratto1.getId());
        assertThat(tipoContratto1).isEqualTo(tipoContratto2);
        tipoContratto2.setId(2L);
        assertThat(tipoContratto1).isNotEqualTo(tipoContratto2);
        tipoContratto1.setId(null);
        assertThat(tipoContratto1).isNotEqualTo(tipoContratto2);
    }
}
