package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Cliente;
import com.terrambe.domain.IndAziendaCli;
import com.terrambe.domain.RecAziendaCli;
import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.repository.ClienteRepository;
import com.terrambe.service.ClienteService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ClienteCriteria;
import com.terrambe.service.ClienteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ClienteResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ClienteResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final String DEFAULT_CUAA = "AAAAAAAAAAA";
    private static final String UPDATED_CUAA = "BBBBBBBBBBB";

    private static final String DEFAULT_PARTITAIVA = "AAAAAAAAAAA";
    private static final String UPDATED_PARTITAIVA = "BBBBBBBBBBB";

    private static final String DEFAULT_CODICEFISCALE = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODICEFISCALE = "BBBBBBBBBBBBBBBB";

    private static final String DEFAULT_CODICE_ATECO = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_ATECO = "BBBBBBBBBB";

    private static final String DEFAULT_RAGIONE_SOCIALE = "AAAAAAAAAA";
    private static final String UPDATED_RAGIONE_SOCIALE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_ATTIVO = false;
    private static final Boolean UPDATED_ATTIVO = true;

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteQueryService clienteQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClienteMockMvc;

    private Cliente cliente;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClienteResource clienteResource = new ClienteResource(clienteService, clienteQueryService);
        this.restClienteMockMvc = MockMvcBuilders.standaloneSetup(clienteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cliente createEntity(EntityManager em) {
        Cliente cliente = new Cliente()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .cuaa(DEFAULT_CUAA)
            .partitaiva(DEFAULT_PARTITAIVA)
            .codicefiscale(DEFAULT_CODICEFISCALE)
            .codiceAteco(DEFAULT_CODICE_ATECO)
            .ragioneSociale(DEFAULT_RAGIONE_SOCIALE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .attivo(DEFAULT_ATTIVO)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return cliente;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cliente createUpdatedEntity(EntityManager em) {
        Cliente cliente = new Cliente()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .cuaa(UPDATED_CUAA)
            .partitaiva(UPDATED_PARTITAIVA)
            .codicefiscale(UPDATED_CODICEFISCALE)
            .codiceAteco(UPDATED_CODICE_ATECO)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .attivo(UPDATED_ATTIVO)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return cliente;
    }

    @BeforeEach
    public void initTest() {
        cliente = createEntity(em);
    }

    @Test
    @Transactional
    public void createCliente() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isCreated());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate + 1);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testCliente.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testCliente.getCuaa()).isEqualTo(DEFAULT_CUAA);
        assertThat(testCliente.getPartitaiva()).isEqualTo(DEFAULT_PARTITAIVA);
        assertThat(testCliente.getCodicefiscale()).isEqualTo(DEFAULT_CODICEFISCALE);
        assertThat(testCliente.getCodiceAteco()).isEqualTo(DEFAULT_CODICE_ATECO);
        assertThat(testCliente.getRagioneSociale()).isEqualTo(DEFAULT_RAGIONE_SOCIALE);
        assertThat(testCliente.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testCliente.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testCliente.isAttivo()).isEqualTo(DEFAULT_ATTIVO);
        assertThat(testCliente.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testCliente.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testCliente.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createClienteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente with an existing ID
        cliente.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setIdAzienda(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCuaaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setCuaa(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPartitaivaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setPartitaiva(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodicefiscaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setCodicefiscale(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodiceAtecoIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setCodiceAteco(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRagioneSocialeIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setRagioneSociale(null);

        // Create the Cliente, which fails.

        restClienteMockMvc.perform(post("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientes() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].cuaa").value(hasItem(DEFAULT_CUAA.toString())))
            .andExpect(jsonPath("$.[*].partitaiva").value(hasItem(DEFAULT_PARTITAIVA.toString())))
            .andExpect(jsonPath("$.[*].codicefiscale").value(hasItem(DEFAULT_CODICEFISCALE.toString())))
            .andExpect(jsonPath("$.[*].codiceAteco").value(hasItem(DEFAULT_CODICE_ATECO.toString())))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", cliente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cliente.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.cuaa").value(DEFAULT_CUAA.toString()))
            .andExpect(jsonPath("$.partitaiva").value(DEFAULT_PARTITAIVA.toString()))
            .andExpect(jsonPath("$.codicefiscale").value(DEFAULT_CODICEFISCALE.toString()))
            .andExpect(jsonPath("$.codiceAteco").value(DEFAULT_CODICE_ATECO.toString()))
            .andExpect(jsonPath("$.ragioneSociale").value(DEFAULT_RAGIONE_SOCIALE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.attivo").value(DEFAULT_ATTIVO.booleanValue()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the clienteList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the clienteList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda is not null
        defaultClienteShouldBeFound("idAzienda.specified=true");

        // Get all the clienteList where idAzienda is null
        defaultClienteShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the clienteList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the clienteList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the clienteList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllClientesByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultClienteShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the clienteList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultClienteShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd is not null
        defaultClienteShouldBeFound("idUnitaProd.specified=true");

        // Get all the clienteList where idUnitaProd is null
        defaultClienteShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllClientesByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultClienteShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the clienteList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultClienteShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllClientesByCuaaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where cuaa equals to DEFAULT_CUAA
        defaultClienteShouldBeFound("cuaa.equals=" + DEFAULT_CUAA);

        // Get all the clienteList where cuaa equals to UPDATED_CUAA
        defaultClienteShouldNotBeFound("cuaa.equals=" + UPDATED_CUAA);
    }

    @Test
    @Transactional
    public void getAllClientesByCuaaIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where cuaa in DEFAULT_CUAA or UPDATED_CUAA
        defaultClienteShouldBeFound("cuaa.in=" + DEFAULT_CUAA + "," + UPDATED_CUAA);

        // Get all the clienteList where cuaa equals to UPDATED_CUAA
        defaultClienteShouldNotBeFound("cuaa.in=" + UPDATED_CUAA);
    }

    @Test
    @Transactional
    public void getAllClientesByCuaaIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where cuaa is not null
        defaultClienteShouldBeFound("cuaa.specified=true");

        // Get all the clienteList where cuaa is null
        defaultClienteShouldNotBeFound("cuaa.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByPartitaivaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where partitaiva equals to DEFAULT_PARTITAIVA
        defaultClienteShouldBeFound("partitaiva.equals=" + DEFAULT_PARTITAIVA);

        // Get all the clienteList where partitaiva equals to UPDATED_PARTITAIVA
        defaultClienteShouldNotBeFound("partitaiva.equals=" + UPDATED_PARTITAIVA);
    }

    @Test
    @Transactional
    public void getAllClientesByPartitaivaIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where partitaiva in DEFAULT_PARTITAIVA or UPDATED_PARTITAIVA
        defaultClienteShouldBeFound("partitaiva.in=" + DEFAULT_PARTITAIVA + "," + UPDATED_PARTITAIVA);

        // Get all the clienteList where partitaiva equals to UPDATED_PARTITAIVA
        defaultClienteShouldNotBeFound("partitaiva.in=" + UPDATED_PARTITAIVA);
    }

    @Test
    @Transactional
    public void getAllClientesByPartitaivaIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where partitaiva is not null
        defaultClienteShouldBeFound("partitaiva.specified=true");

        // Get all the clienteList where partitaiva is null
        defaultClienteShouldNotBeFound("partitaiva.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByCodicefiscaleIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codicefiscale equals to DEFAULT_CODICEFISCALE
        defaultClienteShouldBeFound("codicefiscale.equals=" + DEFAULT_CODICEFISCALE);

        // Get all the clienteList where codicefiscale equals to UPDATED_CODICEFISCALE
        defaultClienteShouldNotBeFound("codicefiscale.equals=" + UPDATED_CODICEFISCALE);
    }

    @Test
    @Transactional
    public void getAllClientesByCodicefiscaleIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codicefiscale in DEFAULT_CODICEFISCALE or UPDATED_CODICEFISCALE
        defaultClienteShouldBeFound("codicefiscale.in=" + DEFAULT_CODICEFISCALE + "," + UPDATED_CODICEFISCALE);

        // Get all the clienteList where codicefiscale equals to UPDATED_CODICEFISCALE
        defaultClienteShouldNotBeFound("codicefiscale.in=" + UPDATED_CODICEFISCALE);
    }

    @Test
    @Transactional
    public void getAllClientesByCodicefiscaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codicefiscale is not null
        defaultClienteShouldBeFound("codicefiscale.specified=true");

        // Get all the clienteList where codicefiscale is null
        defaultClienteShouldNotBeFound("codicefiscale.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByCodiceAtecoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codiceAteco equals to DEFAULT_CODICE_ATECO
        defaultClienteShouldBeFound("codiceAteco.equals=" + DEFAULT_CODICE_ATECO);

        // Get all the clienteList where codiceAteco equals to UPDATED_CODICE_ATECO
        defaultClienteShouldNotBeFound("codiceAteco.equals=" + UPDATED_CODICE_ATECO);
    }

    @Test
    @Transactional
    public void getAllClientesByCodiceAtecoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codiceAteco in DEFAULT_CODICE_ATECO or UPDATED_CODICE_ATECO
        defaultClienteShouldBeFound("codiceAteco.in=" + DEFAULT_CODICE_ATECO + "," + UPDATED_CODICE_ATECO);

        // Get all the clienteList where codiceAteco equals to UPDATED_CODICE_ATECO
        defaultClienteShouldNotBeFound("codiceAteco.in=" + UPDATED_CODICE_ATECO);
    }

    @Test
    @Transactional
    public void getAllClientesByCodiceAtecoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where codiceAteco is not null
        defaultClienteShouldBeFound("codiceAteco.specified=true");

        // Get all the clienteList where codiceAteco is null
        defaultClienteShouldNotBeFound("codiceAteco.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByRagioneSocialeIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where ragioneSociale equals to DEFAULT_RAGIONE_SOCIALE
        defaultClienteShouldBeFound("ragioneSociale.equals=" + DEFAULT_RAGIONE_SOCIALE);

        // Get all the clienteList where ragioneSociale equals to UPDATED_RAGIONE_SOCIALE
        defaultClienteShouldNotBeFound("ragioneSociale.equals=" + UPDATED_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void getAllClientesByRagioneSocialeIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where ragioneSociale in DEFAULT_RAGIONE_SOCIALE or UPDATED_RAGIONE_SOCIALE
        defaultClienteShouldBeFound("ragioneSociale.in=" + DEFAULT_RAGIONE_SOCIALE + "," + UPDATED_RAGIONE_SOCIALE);

        // Get all the clienteList where ragioneSociale equals to UPDATED_RAGIONE_SOCIALE
        defaultClienteShouldNotBeFound("ragioneSociale.in=" + UPDATED_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void getAllClientesByRagioneSocialeIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where ragioneSociale is not null
        defaultClienteShouldBeFound("ragioneSociale.specified=true");

        // Get all the clienteList where ragioneSociale is null
        defaultClienteShouldNotBeFound("ragioneSociale.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali is not null
        defaultClienteShouldBeFound("dataInizVali.specified=true");

        // Get all the clienteList where dataInizVali is null
        defaultClienteShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultClienteShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the clienteList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultClienteShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali is not null
        defaultClienteShouldBeFound("dataFineVali.specified=true");

        // Get all the clienteList where dataFineVali is null
        defaultClienteShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllClientesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultClienteShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the clienteList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultClienteShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllClientesByAttivoIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where attivo equals to DEFAULT_ATTIVO
        defaultClienteShouldBeFound("attivo.equals=" + DEFAULT_ATTIVO);

        // Get all the clienteList where attivo equals to UPDATED_ATTIVO
        defaultClienteShouldNotBeFound("attivo.equals=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllClientesByAttivoIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where attivo in DEFAULT_ATTIVO or UPDATED_ATTIVO
        defaultClienteShouldBeFound("attivo.in=" + DEFAULT_ATTIVO + "," + UPDATED_ATTIVO);

        // Get all the clienteList where attivo equals to UPDATED_ATTIVO
        defaultClienteShouldNotBeFound("attivo.in=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllClientesByAttivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where attivo is not null
        defaultClienteShouldBeFound("attivo.specified=true");

        // Get all the clienteList where attivo is null
        defaultClienteShouldNotBeFound("attivo.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator is not null
        defaultClienteShouldBeFound("userIdCreator.specified=true");

        // Get all the clienteList where userIdCreator is null
        defaultClienteShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultClienteShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the clienteList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultClienteShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod is not null
        defaultClienteShouldBeFound("userIdLastMod.specified=true");

        // Get all the clienteList where userIdLastMod is null
        defaultClienteShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllClientesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clienteList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultClienteShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the clienteList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultClienteShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllClientesByCliToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);
        IndAziendaCli cliToInd = IndAziendaCliResourceIT.createEntity(em);
        em.persist(cliToInd);
        em.flush();
        cliente.addCliToInd(cliToInd);
        clienteRepository.saveAndFlush(cliente);
        Long cliToIndId = cliToInd.getId();

        // Get all the clienteList where cliToInd equals to cliToIndId
        defaultClienteShouldBeFound("cliToIndId.equals=" + cliToIndId);

        // Get all the clienteList where cliToInd equals to cliToIndId + 1
        defaultClienteShouldNotBeFound("cliToIndId.equals=" + (cliToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllClientesByCliToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);
        RecAziendaCli cliToRec = RecAziendaCliResourceIT.createEntity(em);
        em.persist(cliToRec);
        em.flush();
        cliente.addCliToRec(cliToRec);
        clienteRepository.saveAndFlush(cliente);
        Long cliToRecId = cliToRec.getId();

        // Get all the clienteList where cliToRec equals to cliToRecId
        defaultClienteShouldBeFound("cliToRecId.equals=" + cliToRecId);

        // Get all the clienteList where cliToRec equals to cliToRecId + 1
        defaultClienteShouldNotBeFound("cliToRecId.equals=" + (cliToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllClientesByNaturaGiuridicaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);
        AziFormaGiuridica naturaGiuridica = AziFormaGiuridicaResourceIT.createEntity(em);
        em.persist(naturaGiuridica);
        em.flush();
        cliente.setNaturaGiuridica(naturaGiuridica);
        clienteRepository.saveAndFlush(cliente);
        Long naturaGiuridicaId = naturaGiuridica.getId();

        // Get all the clienteList where naturaGiuridica equals to naturaGiuridicaId
        defaultClienteShouldBeFound("naturaGiuridicaId.equals=" + naturaGiuridicaId);

        // Get all the clienteList where naturaGiuridica equals to naturaGiuridicaId + 1
        defaultClienteShouldNotBeFound("naturaGiuridicaId.equals=" + (naturaGiuridicaId + 1));
    }


    @Test
    @Transactional
    public void getAllClientesByCliToAziAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);
        AziAnagrafica cliToAziAna = AziAnagraficaResourceIT.createEntity(em);
        em.persist(cliToAziAna);
        em.flush();
        cliente.setCliToAziAna(cliToAziAna);
        clienteRepository.saveAndFlush(cliente);
        Long cliToAziAnaId = cliToAziAna.getId();

        // Get all the clienteList where cliToAziAna equals to cliToAziAnaId
        defaultClienteShouldBeFound("cliToAziAnaId.equals=" + cliToAziAnaId);

        // Get all the clienteList where cliToAziAna equals to cliToAziAnaId + 1
        defaultClienteShouldNotBeFound("cliToAziAnaId.equals=" + (cliToAziAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllClientesByCliAnaToCliRapIsEqualToSomething() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);
        AziRappresentanteCli cliAnaToCliRap = AziRappresentanteCliResourceIT.createEntity(em);
        em.persist(cliAnaToCliRap);
        em.flush();
        cliente.setCliAnaToCliRap(cliAnaToCliRap);
        clienteRepository.saveAndFlush(cliente);
        Long cliAnaToCliRapId = cliAnaToCliRap.getId();

        // Get all the clienteList where cliAnaToCliRap equals to cliAnaToCliRapId
        defaultClienteShouldBeFound("cliAnaToCliRapId.equals=" + cliAnaToCliRapId);

        // Get all the clienteList where cliAnaToCliRap equals to cliAnaToCliRapId + 1
        defaultClienteShouldNotBeFound("cliAnaToCliRapId.equals=" + (cliAnaToCliRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClienteShouldBeFound(String filter) throws Exception {
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].cuaa").value(hasItem(DEFAULT_CUAA)))
            .andExpect(jsonPath("$.[*].partitaiva").value(hasItem(DEFAULT_PARTITAIVA)))
            .andExpect(jsonPath("$.[*].codicefiscale").value(hasItem(DEFAULT_CODICEFISCALE)))
            .andExpect(jsonPath("$.[*].codiceAteco").value(hasItem(DEFAULT_CODICE_ATECO)))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restClienteMockMvc.perform(get("/api/clientes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClienteShouldNotBeFound(String filter) throws Exception {
        restClienteMockMvc.perform(get("/api/clientes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClienteMockMvc.perform(get("/api/clientes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCliente() throws Exception {
        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCliente() throws Exception {
        // Initialize the database
        clienteService.save(cliente);

        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Update the cliente
        Cliente updatedCliente = clienteRepository.findById(cliente.getId()).get();
        // Disconnect from session so that the updates on updatedCliente are not directly saved in db
        em.detach(updatedCliente);
        updatedCliente
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .cuaa(UPDATED_CUAA)
            .partitaiva(UPDATED_PARTITAIVA)
            .codicefiscale(UPDATED_CODICEFISCALE)
            .codiceAteco(UPDATED_CODICE_ATECO)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .attivo(UPDATED_ATTIVO)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCliente)))
            .andExpect(status().isOk());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
        Cliente testCliente = clienteList.get(clienteList.size() - 1);
        assertThat(testCliente.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testCliente.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testCliente.getCuaa()).isEqualTo(UPDATED_CUAA);
        assertThat(testCliente.getPartitaiva()).isEqualTo(UPDATED_PARTITAIVA);
        assertThat(testCliente.getCodicefiscale()).isEqualTo(UPDATED_CODICEFISCALE);
        assertThat(testCliente.getCodiceAteco()).isEqualTo(UPDATED_CODICE_ATECO);
        assertThat(testCliente.getRagioneSociale()).isEqualTo(UPDATED_RAGIONE_SOCIALE);
        assertThat(testCliente.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testCliente.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testCliente.isAttivo()).isEqualTo(UPDATED_ATTIVO);
        assertThat(testCliente.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testCliente.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testCliente.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingCliente() throws Exception {
        int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Create the Cliente

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClienteMockMvc.perform(put("/api/clientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cliente)))
            .andExpect(status().isBadRequest());

        // Validate the Cliente in the database
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCliente() throws Exception {
        // Initialize the database
        clienteService.save(cliente);

        int databaseSizeBeforeDelete = clienteRepository.findAll().size();

        // Delete the cliente
        restClienteMockMvc.perform(delete("/api/clientes/{id}", cliente.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cliente> clienteList = clienteRepository.findAll();
        assertThat(clienteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cliente.class);
        Cliente cliente1 = new Cliente();
        cliente1.setId(1L);
        Cliente cliente2 = new Cliente();
        cliente2.setId(cliente1.getId());
        assertThat(cliente1).isEqualTo(cliente2);
        cliente2.setId(2L);
        assertThat(cliente1).isNotEqualTo(cliente2);
        cliente1.setId(null);
        assertThat(cliente1).isNotEqualTo(cliente2);
    }
}
