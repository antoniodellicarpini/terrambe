package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.repository.AziFormaGiuridicaRepository;
import com.terrambe.service.AziFormaGiuridicaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziFormaGiuridicaCriteria;
import com.terrambe.service.AziFormaGiuridicaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziFormaGiuridicaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziFormaGiuridicaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziFormaGiuridicaRepository aziFormaGiuridicaRepository;

    @Autowired
    private AziFormaGiuridicaService aziFormaGiuridicaService;

    @Autowired
    private AziFormaGiuridicaQueryService aziFormaGiuridicaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziFormaGiuridicaMockMvc;

    private AziFormaGiuridica aziFormaGiuridica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziFormaGiuridicaResource aziFormaGiuridicaResource = new AziFormaGiuridicaResource(aziFormaGiuridicaService, aziFormaGiuridicaQueryService);
        this.restAziFormaGiuridicaMockMvc = MockMvcBuilders.standaloneSetup(aziFormaGiuridicaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziFormaGiuridica createEntity(EntityManager em) {
        AziFormaGiuridica aziFormaGiuridica = new AziFormaGiuridica()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziFormaGiuridica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziFormaGiuridica createUpdatedEntity(EntityManager em) {
        AziFormaGiuridica aziFormaGiuridica = new AziFormaGiuridica()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziFormaGiuridica;
    }

    @BeforeEach
    public void initTest() {
        aziFormaGiuridica = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziFormaGiuridica() throws Exception {
        int databaseSizeBeforeCreate = aziFormaGiuridicaRepository.findAll().size();

        // Create the AziFormaGiuridica
        restAziFormaGiuridicaMockMvc.perform(post("/api/azi-forma-giuridicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziFormaGiuridica)))
            .andExpect(status().isCreated());

        // Validate the AziFormaGiuridica in the database
        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeCreate + 1);
        AziFormaGiuridica testAziFormaGiuridica = aziFormaGiuridicaList.get(aziFormaGiuridicaList.size() - 1);
        assertThat(testAziFormaGiuridica.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testAziFormaGiuridica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziFormaGiuridica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziFormaGiuridica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziFormaGiuridica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziFormaGiuridica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziFormaGiuridicaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziFormaGiuridicaRepository.findAll().size();

        // Create the AziFormaGiuridica with an existing ID
        aziFormaGiuridica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziFormaGiuridicaMockMvc.perform(post("/api/azi-forma-giuridicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziFormaGiuridica)))
            .andExpect(status().isBadRequest());

        // Validate the AziFormaGiuridica in the database
        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziFormaGiuridicaRepository.findAll().size();
        // set the field null
        aziFormaGiuridica.setDescrizione(null);

        // Create the AziFormaGiuridica, which fails.

        restAziFormaGiuridicaMockMvc.perform(post("/api/azi-forma-giuridicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziFormaGiuridica)))
            .andExpect(status().isBadRequest());

        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicas() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziFormaGiuridica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziFormaGiuridica() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get the aziFormaGiuridica
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas/{id}", aziFormaGiuridica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziFormaGiuridica.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultAziFormaGiuridicaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the aziFormaGiuridicaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAziFormaGiuridicaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultAziFormaGiuridicaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the aziFormaGiuridicaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAziFormaGiuridicaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where descrizione is not null
        defaultAziFormaGiuridicaShouldBeFound("descrizione.specified=true");

        // Get all the aziFormaGiuridicaList where descrizione is null
        defaultAziFormaGiuridicaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali is not null
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.specified=true");

        // Get all the aziFormaGiuridicaList where dataInizVali is null
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziFormaGiuridicaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali is not null
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.specified=true");

        // Get all the aziFormaGiuridicaList where dataFineVali is null
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziFormaGiuridicaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziFormaGiuridicaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator is not null
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.specified=true");

        // Get all the aziFormaGiuridicaList where userIdCreator is null
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziFormaGiuridicaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziFormaGiuridicaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod is not null
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziFormaGiuridicaList where userIdLastMod is null
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziFormaGiuridicasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziFormaGiuridicaRepository.saveAndFlush(aziFormaGiuridica);

        // Get all the aziFormaGiuridicaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziFormaGiuridicaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziFormaGiuridicaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziFormaGiuridicaShouldBeFound(String filter) throws Exception {
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziFormaGiuridica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziFormaGiuridicaShouldNotBeFound(String filter) throws Exception {
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziFormaGiuridica() throws Exception {
        // Get the aziFormaGiuridica
        restAziFormaGiuridicaMockMvc.perform(get("/api/azi-forma-giuridicas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziFormaGiuridica() throws Exception {
        // Initialize the database
        aziFormaGiuridicaService.save(aziFormaGiuridica);

        int databaseSizeBeforeUpdate = aziFormaGiuridicaRepository.findAll().size();

        // Update the aziFormaGiuridica
        AziFormaGiuridica updatedAziFormaGiuridica = aziFormaGiuridicaRepository.findById(aziFormaGiuridica.getId()).get();
        // Disconnect from session so that the updates on updatedAziFormaGiuridica are not directly saved in db
        em.detach(updatedAziFormaGiuridica);
        updatedAziFormaGiuridica
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziFormaGiuridicaMockMvc.perform(put("/api/azi-forma-giuridicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziFormaGiuridica)))
            .andExpect(status().isOk());

        // Validate the AziFormaGiuridica in the database
        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeUpdate);
        AziFormaGiuridica testAziFormaGiuridica = aziFormaGiuridicaList.get(aziFormaGiuridicaList.size() - 1);
        assertThat(testAziFormaGiuridica.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testAziFormaGiuridica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziFormaGiuridica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziFormaGiuridica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziFormaGiuridica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziFormaGiuridica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziFormaGiuridica() throws Exception {
        int databaseSizeBeforeUpdate = aziFormaGiuridicaRepository.findAll().size();

        // Create the AziFormaGiuridica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziFormaGiuridicaMockMvc.perform(put("/api/azi-forma-giuridicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziFormaGiuridica)))
            .andExpect(status().isBadRequest());

        // Validate the AziFormaGiuridica in the database
        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziFormaGiuridica() throws Exception {
        // Initialize the database
        aziFormaGiuridicaService.save(aziFormaGiuridica);

        int databaseSizeBeforeDelete = aziFormaGiuridicaRepository.findAll().size();

        // Delete the aziFormaGiuridica
        restAziFormaGiuridicaMockMvc.perform(delete("/api/azi-forma-giuridicas/{id}", aziFormaGiuridica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziFormaGiuridica> aziFormaGiuridicaList = aziFormaGiuridicaRepository.findAll();
        assertThat(aziFormaGiuridicaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziFormaGiuridica.class);
        AziFormaGiuridica aziFormaGiuridica1 = new AziFormaGiuridica();
        aziFormaGiuridica1.setId(1L);
        AziFormaGiuridica aziFormaGiuridica2 = new AziFormaGiuridica();
        aziFormaGiuridica2.setId(aziFormaGiuridica1.getId());
        assertThat(aziFormaGiuridica1).isEqualTo(aziFormaGiuridica2);
        aziFormaGiuridica2.setId(2L);
        assertThat(aziFormaGiuridica1).isNotEqualTo(aziFormaGiuridica2);
        aziFormaGiuridica1.setId(null);
        assertThat(aziFormaGiuridica1).isNotEqualTo(aziFormaGiuridica2);
    }
}
