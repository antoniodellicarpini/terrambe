package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcTrap;
import com.terrambe.domain.RegOpcTrapAppz;
import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.repository.RegOpcTrapRepository;
import com.terrambe.service.RegOpcTrapService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcTrapCriteria;
import com.terrambe.service.RegOpcTrapQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcTrapResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcTrapResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_OPERAZIONE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_OPERAZIONE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_OPERAZIONE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final Long DEFAULT_ID_TRAPPOLA = 1L;
    private static final Long UPDATED_ID_TRAPPOLA = 2L;
    private static final Long SMALLER_ID_TRAPPOLA = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcTrapRepository regOpcTrapRepository;

    @Autowired
    private RegOpcTrapService regOpcTrapService;

    @Autowired
    private RegOpcTrapQueryService regOpcTrapQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcTrapMockMvc;

    private RegOpcTrap regOpcTrap;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcTrapResource regOpcTrapResource = new RegOpcTrapResource(regOpcTrapService, regOpcTrapQueryService);
        this.restRegOpcTrapMockMvc = MockMvcBuilders.standaloneSetup(regOpcTrapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrap createEntity(EntityManager em) {
        RegOpcTrap regOpcTrap = new RegOpcTrap()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataOperazione(DEFAULT_DATA_OPERAZIONE)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .idTrappola(DEFAULT_ID_TRAPPOLA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcTrap;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrap createUpdatedEntity(EntityManager em) {
        RegOpcTrap regOpcTrap = new RegOpcTrap()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .idTrappola(UPDATED_ID_TRAPPOLA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcTrap;
    }

    @BeforeEach
    public void initTest() {
        regOpcTrap = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcTrap() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapRepository.findAll().size();

        // Create the RegOpcTrap
        restRegOpcTrapMockMvc.perform(post("/api/reg-opc-traps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrap)))
            .andExpect(status().isCreated());

        // Validate the RegOpcTrap in the database
        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcTrap testRegOpcTrap = regOpcTrapList.get(regOpcTrapList.size() - 1);
        assertThat(testRegOpcTrap.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcTrap.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcTrap.getDataOperazione()).isEqualTo(DEFAULT_DATA_OPERAZIONE);
        assertThat(testRegOpcTrap.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcTrap.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcTrap.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcTrap.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcTrap.getIdTrappola()).isEqualTo(DEFAULT_ID_TRAPPOLA);
        assertThat(testRegOpcTrap.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcTrap.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcTrap.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcTrap.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcTrap.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcTrapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapRepository.findAll().size();

        // Create the RegOpcTrap with an existing ID
        regOpcTrap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcTrapMockMvc.perform(post("/api/reg-opc-traps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrap)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrap in the database
        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcTrapRepository.findAll().size();
        // set the field null
        regOpcTrap.setIdAzienda(null);

        // Create the RegOpcTrap, which fails.

        restRegOpcTrapMockMvc.perform(post("/api/reg-opc-traps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrap)))
            .andExpect(status().isBadRequest());

        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcTraps() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrap.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].idTrappola").value(hasItem(DEFAULT_ID_TRAPPOLA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcTrap() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get the regOpcTrap
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps/{id}", regOpcTrap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcTrap.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataOperazione").value(DEFAULT_DATA_OPERAZIONE.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.idTrappola").value(DEFAULT_ID_TRAPPOLA.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda is not null
        defaultRegOpcTrapShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcTrapList where idAzienda is null
        defaultRegOpcTrapShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcTrapShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd is not null
        defaultRegOpcTrapShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcTrapList where idUnitaProd is null
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione equals to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.equals=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.equals=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione in DEFAULT_DATA_OPERAZIONE or UPDATED_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.in=" + DEFAULT_DATA_OPERAZIONE + "," + UPDATED_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.in=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione is not null
        defaultRegOpcTrapShouldBeFound("dataOperazione.specified=true");

        // Get all the regOpcTrapList where dataOperazione is null
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione is greater than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.greaterThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione is greater than or equal to UPDATED_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.greaterThanOrEqual=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione is less than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.lessThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione is less than or equal to SMALLER_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.lessThanOrEqual=" + SMALLER_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione is less than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.lessThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione is less than UPDATED_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.lessThan=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataOperazioneIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataOperazione is greater than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcTrapShouldNotBeFound("dataOperazione.greaterThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcTrapList where dataOperazione is greater than SMALLER_DATA_OPERAZIONE
        defaultRegOpcTrapShouldBeFound("dataOperazione.greaterThan=" + SMALLER_DATA_OPERAZIONE);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcTrapShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcTrapList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcTrapShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcTrapShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcTrapList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcTrapShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where tempoImpiegato is not null
        defaultRegOpcTrapShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcTrapList where tempoImpiegato is null
        defaultRegOpcTrapShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura is not null
        defaultRegOpcTrapShouldBeFound("idColtura.specified=true");

        // Get all the regOpcTrapList where idColtura is null
        defaultRegOpcTrapShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcTrapShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcTrapList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcTrapShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore is not null
        defaultRegOpcTrapShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcTrapList where idOperatore is null
        defaultRegOpcTrapShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcTrapShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcTrapList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcTrapShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo is not null
        defaultRegOpcTrapShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcTrapList where idMezzo is null
        defaultRegOpcTrapShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcTrapShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcTrapList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcTrapShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola equals to DEFAULT_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.equals=" + DEFAULT_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola equals to UPDATED_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.equals=" + UPDATED_ID_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola in DEFAULT_ID_TRAPPOLA or UPDATED_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.in=" + DEFAULT_ID_TRAPPOLA + "," + UPDATED_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola equals to UPDATED_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.in=" + UPDATED_ID_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola is not null
        defaultRegOpcTrapShouldBeFound("idTrappola.specified=true");

        // Get all the regOpcTrapList where idTrappola is null
        defaultRegOpcTrapShouldNotBeFound("idTrappola.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola is greater than or equal to DEFAULT_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.greaterThanOrEqual=" + DEFAULT_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola is greater than or equal to UPDATED_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.greaterThanOrEqual=" + UPDATED_ID_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola is less than or equal to DEFAULT_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.lessThanOrEqual=" + DEFAULT_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola is less than or equal to SMALLER_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.lessThanOrEqual=" + SMALLER_ID_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola is less than DEFAULT_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.lessThan=" + DEFAULT_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola is less than UPDATED_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.lessThan=" + UPDATED_ID_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByIdTrappolaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where idTrappola is greater than DEFAULT_ID_TRAPPOLA
        defaultRegOpcTrapShouldNotBeFound("idTrappola.greaterThan=" + DEFAULT_ID_TRAPPOLA);

        // Get all the regOpcTrapList where idTrappola is greater than SMALLER_ID_TRAPPOLA
        defaultRegOpcTrapShouldBeFound("idTrappola.greaterThan=" + SMALLER_ID_TRAPPOLA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali is not null
        defaultRegOpcTrapShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcTrapList where dataInizVali is null
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali is not null
        defaultRegOpcTrapShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcTrapList where dataFineVali is null
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator is not null
        defaultRegOpcTrapShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcTrapList where userIdCreator is null
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod is not null
        defaultRegOpcTrapShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcTrapList where userIdLastMod is null
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);

        // Get all the regOpcTrapList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByOpcTrapToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);
        RegOpcTrapAppz opcTrapToAppz = RegOpcTrapAppzResourceIT.createEntity(em);
        em.persist(opcTrapToAppz);
        em.flush();
        regOpcTrap.addOpcTrapToAppz(opcTrapToAppz);
        regOpcTrapRepository.saveAndFlush(regOpcTrap);
        Long opcTrapToAppzId = opcTrapToAppz.getId();

        // Get all the regOpcTrapList where opcTrapToAppz equals to opcTrapToAppzId
        defaultRegOpcTrapShouldBeFound("opcTrapToAppzId.equals=" + opcTrapToAppzId);

        // Get all the regOpcTrapList where opcTrapToAppz equals to opcTrapToAppzId + 1
        defaultRegOpcTrapShouldNotBeFound("opcTrapToAppzId.equals=" + (opcTrapToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapsByOpcTrapToTrapContrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapRepository.saveAndFlush(regOpcTrap);
        RegOpcTrapContr opcTrapToTrapContr = RegOpcTrapContrResourceIT.createEntity(em);
        em.persist(opcTrapToTrapContr);
        em.flush();
        regOpcTrap.addOpcTrapToTrapContr(opcTrapToTrapContr);
        regOpcTrapRepository.saveAndFlush(regOpcTrap);
        Long opcTrapToTrapContrId = opcTrapToTrapContr.getId();

        // Get all the regOpcTrapList where opcTrapToTrapContr equals to opcTrapToTrapContrId
        defaultRegOpcTrapShouldBeFound("opcTrapToTrapContrId.equals=" + opcTrapToTrapContrId);

        // Get all the regOpcTrapList where opcTrapToTrapContr equals to opcTrapToTrapContrId + 1
        defaultRegOpcTrapShouldNotBeFound("opcTrapToTrapContrId.equals=" + (opcTrapToTrapContrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcTrapShouldBeFound(String filter) throws Exception {
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrap.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].idTrappola").value(hasItem(DEFAULT_ID_TRAPPOLA.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcTrapShouldNotBeFound(String filter) throws Exception {
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcTrap() throws Exception {
        // Get the regOpcTrap
        restRegOpcTrapMockMvc.perform(get("/api/reg-opc-traps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcTrap() throws Exception {
        // Initialize the database
        regOpcTrapService.save(regOpcTrap);

        int databaseSizeBeforeUpdate = regOpcTrapRepository.findAll().size();

        // Update the regOpcTrap
        RegOpcTrap updatedRegOpcTrap = regOpcTrapRepository.findById(regOpcTrap.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcTrap are not directly saved in db
        em.detach(updatedRegOpcTrap);
        updatedRegOpcTrap
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .idTrappola(UPDATED_ID_TRAPPOLA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcTrapMockMvc.perform(put("/api/reg-opc-traps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcTrap)))
            .andExpect(status().isOk());

        // Validate the RegOpcTrap in the database
        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeUpdate);
        RegOpcTrap testRegOpcTrap = regOpcTrapList.get(regOpcTrapList.size() - 1);
        assertThat(testRegOpcTrap.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcTrap.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcTrap.getDataOperazione()).isEqualTo(UPDATED_DATA_OPERAZIONE);
        assertThat(testRegOpcTrap.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcTrap.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcTrap.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcTrap.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcTrap.getIdTrappola()).isEqualTo(UPDATED_ID_TRAPPOLA);
        assertThat(testRegOpcTrap.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcTrap.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcTrap.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcTrap.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcTrap.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcTrap() throws Exception {
        int databaseSizeBeforeUpdate = regOpcTrapRepository.findAll().size();

        // Create the RegOpcTrap

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcTrapMockMvc.perform(put("/api/reg-opc-traps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrap)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrap in the database
        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcTrap() throws Exception {
        // Initialize the database
        regOpcTrapService.save(regOpcTrap);

        int databaseSizeBeforeDelete = regOpcTrapRepository.findAll().size();

        // Delete the regOpcTrap
        restRegOpcTrapMockMvc.perform(delete("/api/reg-opc-traps/{id}", regOpcTrap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcTrap> regOpcTrapList = regOpcTrapRepository.findAll();
        assertThat(regOpcTrapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcTrap.class);
        RegOpcTrap regOpcTrap1 = new RegOpcTrap();
        regOpcTrap1.setId(1L);
        RegOpcTrap regOpcTrap2 = new RegOpcTrap();
        regOpcTrap2.setId(regOpcTrap1.getId());
        assertThat(regOpcTrap1).isEqualTo(regOpcTrap2);
        regOpcTrap2.setId(2L);
        assertThat(regOpcTrap1).isNotEqualTo(regOpcTrap2);
        regOpcTrap1.setId(null);
        assertThat(regOpcTrap1).isNotEqualTo(regOpcTrap2);
    }
}
