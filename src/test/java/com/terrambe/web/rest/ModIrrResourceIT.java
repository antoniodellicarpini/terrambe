package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.ModIrr;
import com.terrambe.repository.ModIrrRepository;
import com.terrambe.service.ModIrrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ModIrrCriteria;
import com.terrambe.service.ModIrrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ModIrrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ModIrrResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private ModIrrRepository modIrrRepository;

    @Autowired
    private ModIrrService modIrrService;

    @Autowired
    private ModIrrQueryService modIrrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restModIrrMockMvc;

    private ModIrr modIrr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModIrrResource modIrrResource = new ModIrrResource(modIrrService, modIrrQueryService);
        this.restModIrrMockMvc = MockMvcBuilders.standaloneSetup(modIrrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModIrr createEntity(EntityManager em) {
        ModIrr modIrr = new ModIrr()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return modIrr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModIrr createUpdatedEntity(EntityManager em) {
        ModIrr modIrr = new ModIrr()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return modIrr;
    }

    @BeforeEach
    public void initTest() {
        modIrr = createEntity(em);
    }

    @Test
    @Transactional
    public void createModIrr() throws Exception {
        int databaseSizeBeforeCreate = modIrrRepository.findAll().size();

        // Create the ModIrr
        restModIrrMockMvc.perform(post("/api/mod-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modIrr)))
            .andExpect(status().isCreated());

        // Validate the ModIrr in the database
        List<ModIrr> modIrrList = modIrrRepository.findAll();
        assertThat(modIrrList).hasSize(databaseSizeBeforeCreate + 1);
        ModIrr testModIrr = modIrrList.get(modIrrList.size() - 1);
        assertThat(testModIrr.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testModIrr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testModIrr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testModIrr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testModIrr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createModIrrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modIrrRepository.findAll().size();

        // Create the ModIrr with an existing ID
        modIrr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModIrrMockMvc.perform(post("/api/mod-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modIrr)))
            .andExpect(status().isBadRequest());

        // Validate the ModIrr in the database
        List<ModIrr> modIrrList = modIrrRepository.findAll();
        assertThat(modIrrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllModIrrs() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList
        restModIrrMockMvc.perform(get("/api/mod-irrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getModIrr() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get the modIrr
        restModIrrMockMvc.perform(get("/api/mod-irrs/{id}", modIrr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modIrr.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllModIrrsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultModIrrShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the modIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultModIrrShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultModIrrShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the modIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultModIrrShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where descrizione is not null
        defaultModIrrShouldBeFound("descrizione.specified=true");

        // Get all the modIrrList where descrizione is null
        defaultModIrrShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali is not null
        defaultModIrrShouldBeFound("dataInizVali.specified=true");

        // Get all the modIrrList where dataInizVali is null
        defaultModIrrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultModIrrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the modIrrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultModIrrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali is not null
        defaultModIrrShouldBeFound("dataFineVali.specified=true");

        // Get all the modIrrList where dataFineVali is null
        defaultModIrrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllModIrrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultModIrrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the modIrrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultModIrrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator is not null
        defaultModIrrShouldBeFound("userIdCreator.specified=true");

        // Get all the modIrrList where userIdCreator is null
        defaultModIrrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultModIrrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the modIrrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultModIrrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod is not null
        defaultModIrrShouldBeFound("userIdLastMod.specified=true");

        // Get all the modIrrList where userIdLastMod is null
        defaultModIrrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllModIrrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        modIrrRepository.saveAndFlush(modIrr);

        // Get all the modIrrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultModIrrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the modIrrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultModIrrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultModIrrShouldBeFound(String filter) throws Exception {
        restModIrrMockMvc.perform(get("/api/mod-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restModIrrMockMvc.perform(get("/api/mod-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultModIrrShouldNotBeFound(String filter) throws Exception {
        restModIrrMockMvc.perform(get("/api/mod-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restModIrrMockMvc.perform(get("/api/mod-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingModIrr() throws Exception {
        // Get the modIrr
        restModIrrMockMvc.perform(get("/api/mod-irrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModIrr() throws Exception {
        // Initialize the database
        modIrrService.save(modIrr);

        int databaseSizeBeforeUpdate = modIrrRepository.findAll().size();

        // Update the modIrr
        ModIrr updatedModIrr = modIrrRepository.findById(modIrr.getId()).get();
        // Disconnect from session so that the updates on updatedModIrr are not directly saved in db
        em.detach(updatedModIrr);
        updatedModIrr
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restModIrrMockMvc.perform(put("/api/mod-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedModIrr)))
            .andExpect(status().isOk());

        // Validate the ModIrr in the database
        List<ModIrr> modIrrList = modIrrRepository.findAll();
        assertThat(modIrrList).hasSize(databaseSizeBeforeUpdate);
        ModIrr testModIrr = modIrrList.get(modIrrList.size() - 1);
        assertThat(testModIrr.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testModIrr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testModIrr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testModIrr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testModIrr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingModIrr() throws Exception {
        int databaseSizeBeforeUpdate = modIrrRepository.findAll().size();

        // Create the ModIrr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModIrrMockMvc.perform(put("/api/mod-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modIrr)))
            .andExpect(status().isBadRequest());

        // Validate the ModIrr in the database
        List<ModIrr> modIrrList = modIrrRepository.findAll();
        assertThat(modIrrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModIrr() throws Exception {
        // Initialize the database
        modIrrService.save(modIrr);

        int databaseSizeBeforeDelete = modIrrRepository.findAll().size();

        // Delete the modIrr
        restModIrrMockMvc.perform(delete("/api/mod-irrs/{id}", modIrr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ModIrr> modIrrList = modIrrRepository.findAll();
        assertThat(modIrrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModIrr.class);
        ModIrr modIrr1 = new ModIrr();
        modIrr1.setId(1L);
        ModIrr modIrr2 = new ModIrr();
        modIrr2.setId(modIrr1.getId());
        assertThat(modIrr1).isEqualTo(modIrr2);
        modIrr2.setId(2L);
        assertThat(modIrr1).isNotEqualTo(modIrr2);
        modIrr1.setId(null);
        assertThat(modIrr1).isNotEqualTo(modIrr2);
    }
}
