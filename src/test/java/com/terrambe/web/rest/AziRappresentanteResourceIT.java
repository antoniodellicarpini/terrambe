package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziRappresentante;
import com.terrambe.domain.IndNascRap;
import com.terrambe.domain.IndAziRap;
import com.terrambe.domain.RecAziRap;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.AziRappresentanteRepository;
import com.terrambe.service.AziRappresentanteService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziRappresentanteCriteria;
import com.terrambe.service.AziRappresentanteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziRappresentanteResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziRappresentanteResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME = "BBBBBBBBBB";

    private static final String DEFAULT_SESSO = "AAAAAAAAAA";
    private static final String UPDATED_SESSO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_NASCITA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_NASCITA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_NASCITA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CODICE_FISCALE = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODICE_FISCALE = "BBBBBBBBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziRappresentanteRepository aziRappresentanteRepository;

    @Autowired
    private AziRappresentanteService aziRappresentanteService;

    @Autowired
    private AziRappresentanteQueryService aziRappresentanteQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziRappresentanteMockMvc;

    private AziRappresentante aziRappresentante;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziRappresentanteResource aziRappresentanteResource = new AziRappresentanteResource(aziRappresentanteService, aziRappresentanteQueryService);
        this.restAziRappresentanteMockMvc = MockMvcBuilders.standaloneSetup(aziRappresentanteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziRappresentante createEntity(EntityManager em) {
        AziRappresentante aziRappresentante = new AziRappresentante()
            .nome(DEFAULT_NOME)
            .cognome(DEFAULT_COGNOME)
            .sesso(DEFAULT_SESSO)
            .dataNascita(DEFAULT_DATA_NASCITA)
            .codiceFiscale(DEFAULT_CODICE_FISCALE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziRappresentante;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziRappresentante createUpdatedEntity(EntityManager em) {
        AziRappresentante aziRappresentante = new AziRappresentante()
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziRappresentante;
    }

    @BeforeEach
    public void initTest() {
        aziRappresentante = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziRappresentante() throws Exception {
        int databaseSizeBeforeCreate = aziRappresentanteRepository.findAll().size();

        // Create the AziRappresentante
        restAziRappresentanteMockMvc.perform(post("/api/azi-rappresentantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentante)))
            .andExpect(status().isCreated());

        // Validate the AziRappresentante in the database
        List<AziRappresentante> aziRappresentanteList = aziRappresentanteRepository.findAll();
        assertThat(aziRappresentanteList).hasSize(databaseSizeBeforeCreate + 1);
        AziRappresentante testAziRappresentante = aziRappresentanteList.get(aziRappresentanteList.size() - 1);
        assertThat(testAziRappresentante.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testAziRappresentante.getCognome()).isEqualTo(DEFAULT_COGNOME);
        assertThat(testAziRappresentante.getSesso()).isEqualTo(DEFAULT_SESSO);
        assertThat(testAziRappresentante.getDataNascita()).isEqualTo(DEFAULT_DATA_NASCITA);
        assertThat(testAziRappresentante.getCodiceFiscale()).isEqualTo(DEFAULT_CODICE_FISCALE);
        assertThat(testAziRappresentante.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziRappresentante.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziRappresentante.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziRappresentante.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziRappresentante.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziRappresentanteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziRappresentanteRepository.findAll().size();

        // Create the AziRappresentante with an existing ID
        aziRappresentante.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziRappresentanteMockMvc.perform(post("/api/azi-rappresentantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentante)))
            .andExpect(status().isBadRequest());

        // Validate the AziRappresentante in the database
        List<AziRappresentante> aziRappresentanteList = aziRappresentanteRepository.findAll();
        assertThat(aziRappresentanteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantes() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziRappresentante.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME.toString())))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO.toString())))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziRappresentante() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get the aziRappresentante
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes/{id}", aziRappresentante.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziRappresentante.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME.toString()))
            .andExpect(jsonPath("$.sesso").value(DEFAULT_SESSO.toString()))
            .andExpect(jsonPath("$.dataNascita").value(DEFAULT_DATA_NASCITA.toString()))
            .andExpect(jsonPath("$.codiceFiscale").value(DEFAULT_CODICE_FISCALE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where nome equals to DEFAULT_NOME
        defaultAziRappresentanteShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the aziRappresentanteList where nome equals to UPDATED_NOME
        defaultAziRappresentanteShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultAziRappresentanteShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the aziRappresentanteList where nome equals to UPDATED_NOME
        defaultAziRappresentanteShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where nome is not null
        defaultAziRappresentanteShouldBeFound("nome.specified=true");

        // Get all the aziRappresentanteList where nome is null
        defaultAziRappresentanteShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByCognomeIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where cognome equals to DEFAULT_COGNOME
        defaultAziRappresentanteShouldBeFound("cognome.equals=" + DEFAULT_COGNOME);

        // Get all the aziRappresentanteList where cognome equals to UPDATED_COGNOME
        defaultAziRappresentanteShouldNotBeFound("cognome.equals=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByCognomeIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where cognome in DEFAULT_COGNOME or UPDATED_COGNOME
        defaultAziRappresentanteShouldBeFound("cognome.in=" + DEFAULT_COGNOME + "," + UPDATED_COGNOME);

        // Get all the aziRappresentanteList where cognome equals to UPDATED_COGNOME
        defaultAziRappresentanteShouldNotBeFound("cognome.in=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByCognomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where cognome is not null
        defaultAziRappresentanteShouldBeFound("cognome.specified=true");

        // Get all the aziRappresentanteList where cognome is null
        defaultAziRappresentanteShouldNotBeFound("cognome.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesBySessoIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where sesso equals to DEFAULT_SESSO
        defaultAziRappresentanteShouldBeFound("sesso.equals=" + DEFAULT_SESSO);

        // Get all the aziRappresentanteList where sesso equals to UPDATED_SESSO
        defaultAziRappresentanteShouldNotBeFound("sesso.equals=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesBySessoIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where sesso in DEFAULT_SESSO or UPDATED_SESSO
        defaultAziRappresentanteShouldBeFound("sesso.in=" + DEFAULT_SESSO + "," + UPDATED_SESSO);

        // Get all the aziRappresentanteList where sesso equals to UPDATED_SESSO
        defaultAziRappresentanteShouldNotBeFound("sesso.in=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesBySessoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where sesso is not null
        defaultAziRappresentanteShouldBeFound("sesso.specified=true");

        // Get all the aziRappresentanteList where sesso is null
        defaultAziRappresentanteShouldNotBeFound("sesso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita equals to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.equals=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.equals=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita in DEFAULT_DATA_NASCITA or UPDATED_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.in=" + DEFAULT_DATA_NASCITA + "," + UPDATED_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.in=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita is not null
        defaultAziRappresentanteShouldBeFound("dataNascita.specified=true");

        // Get all the aziRappresentanteList where dataNascita is null
        defaultAziRappresentanteShouldNotBeFound("dataNascita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita is greater than or equal to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.greaterThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita is greater than or equal to UPDATED_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.greaterThanOrEqual=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita is less than or equal to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.lessThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita is less than or equal to SMALLER_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.lessThanOrEqual=" + SMALLER_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita is less than DEFAULT_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.lessThan=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita is less than UPDATED_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.lessThan=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataNascitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataNascita is greater than DEFAULT_DATA_NASCITA
        defaultAziRappresentanteShouldNotBeFound("dataNascita.greaterThan=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteList where dataNascita is greater than SMALLER_DATA_NASCITA
        defaultAziRappresentanteShouldBeFound("dataNascita.greaterThan=" + SMALLER_DATA_NASCITA);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByCodiceFiscaleIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where codiceFiscale equals to DEFAULT_CODICE_FISCALE
        defaultAziRappresentanteShouldBeFound("codiceFiscale.equals=" + DEFAULT_CODICE_FISCALE);

        // Get all the aziRappresentanteList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultAziRappresentanteShouldNotBeFound("codiceFiscale.equals=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByCodiceFiscaleIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where codiceFiscale in DEFAULT_CODICE_FISCALE or UPDATED_CODICE_FISCALE
        defaultAziRappresentanteShouldBeFound("codiceFiscale.in=" + DEFAULT_CODICE_FISCALE + "," + UPDATED_CODICE_FISCALE);

        // Get all the aziRappresentanteList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultAziRappresentanteShouldNotBeFound("codiceFiscale.in=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByCodiceFiscaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where codiceFiscale is not null
        defaultAziRappresentanteShouldBeFound("codiceFiscale.specified=true");

        // Get all the aziRappresentanteList where codiceFiscale is null
        defaultAziRappresentanteShouldNotBeFound("codiceFiscale.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali is not null
        defaultAziRappresentanteShouldBeFound("dataInizVali.specified=true");

        // Get all the aziRappresentanteList where dataInizVali is null
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziRappresentanteShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali is not null
        defaultAziRappresentanteShouldBeFound("dataFineVali.specified=true");

        // Get all the aziRappresentanteList where dataFineVali is null
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziRappresentanteShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator is not null
        defaultAziRappresentanteShouldBeFound("userIdCreator.specified=true");

        // Get all the aziRappresentanteList where userIdCreator is null
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziRappresentanteShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod is not null
        defaultAziRappresentanteShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziRappresentanteList where userIdLastMod is null
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentantesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);

        // Get all the aziRappresentanteList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziRappresentanteShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByAziRapToIndNascIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        IndNascRap aziRapToIndNasc = IndNascRapResourceIT.createEntity(em);
        em.persist(aziRapToIndNasc);
        em.flush();
        aziRappresentante.setAziRapToIndNasc(aziRapToIndNasc);
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        Long aziRapToIndNascId = aziRapToIndNasc.getId();

        // Get all the aziRappresentanteList where aziRapToIndNasc equals to aziRapToIndNascId
        defaultAziRappresentanteShouldBeFound("aziRapToIndNascId.equals=" + aziRapToIndNascId);

        // Get all the aziRappresentanteList where aziRapToIndNasc equals to aziRapToIndNascId + 1
        defaultAziRappresentanteShouldNotBeFound("aziRapToIndNascId.equals=" + (aziRapToIndNascId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByAziRapToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        IndAziRap aziRapToInd = IndAziRapResourceIT.createEntity(em);
        em.persist(aziRapToInd);
        em.flush();
        aziRappresentante.addAziRapToInd(aziRapToInd);
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        Long aziRapToIndId = aziRapToInd.getId();

        // Get all the aziRappresentanteList where aziRapToInd equals to aziRapToIndId
        defaultAziRappresentanteShouldBeFound("aziRapToIndId.equals=" + aziRapToIndId);

        // Get all the aziRappresentanteList where aziRapToInd equals to aziRapToIndId + 1
        defaultAziRappresentanteShouldNotBeFound("aziRapToIndId.equals=" + (aziRapToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByAziRapToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        RecAziRap aziRapToRec = RecAziRapResourceIT.createEntity(em);
        em.persist(aziRapToRec);
        em.flush();
        aziRappresentante.addAziRapToRec(aziRapToRec);
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        Long aziRapToRecId = aziRapToRec.getId();

        // Get all the aziRappresentanteList where aziRapToRec equals to aziRapToRecId
        defaultAziRappresentanteShouldBeFound("aziRapToRecId.equals=" + aziRapToRecId);

        // Get all the aziRappresentanteList where aziRapToRec equals to aziRapToRecId + 1
        defaultAziRappresentanteShouldNotBeFound("aziRapToRecId.equals=" + (aziRapToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentantesByAziRapToAziAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        AziAnagrafica aziRapToAziAna = AziAnagraficaResourceIT.createEntity(em);
        em.persist(aziRapToAziAna);
        em.flush();
        aziRappresentante.addAziRapToAziAna(aziRapToAziAna);
        aziRappresentanteRepository.saveAndFlush(aziRappresentante);
        Long aziRapToAziAnaId = aziRapToAziAna.getId();

        // Get all the aziRappresentanteList where aziRapToAziAna equals to aziRapToAziAnaId
        defaultAziRappresentanteShouldBeFound("aziRapToAziAnaId.equals=" + aziRapToAziAnaId);

        // Get all the aziRappresentanteList where aziRapToAziAna equals to aziRapToAziAnaId + 1
        defaultAziRappresentanteShouldNotBeFound("aziRapToAziAnaId.equals=" + (aziRapToAziAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziRappresentanteShouldBeFound(String filter) throws Exception {
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziRappresentante.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME)))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO)))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziRappresentanteShouldNotBeFound(String filter) throws Exception {
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziRappresentante() throws Exception {
        // Get the aziRappresentante
        restAziRappresentanteMockMvc.perform(get("/api/azi-rappresentantes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziRappresentante() throws Exception {
        // Initialize the database
        aziRappresentanteService.save(aziRappresentante);

        int databaseSizeBeforeUpdate = aziRappresentanteRepository.findAll().size();

        // Update the aziRappresentante
        AziRappresentante updatedAziRappresentante = aziRappresentanteRepository.findById(aziRappresentante.getId()).get();
        // Disconnect from session so that the updates on updatedAziRappresentante are not directly saved in db
        em.detach(updatedAziRappresentante);
        updatedAziRappresentante
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziRappresentanteMockMvc.perform(put("/api/azi-rappresentantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziRappresentante)))
            .andExpect(status().isOk());

        // Validate the AziRappresentante in the database
        List<AziRappresentante> aziRappresentanteList = aziRappresentanteRepository.findAll();
        assertThat(aziRappresentanteList).hasSize(databaseSizeBeforeUpdate);
        AziRappresentante testAziRappresentante = aziRappresentanteList.get(aziRappresentanteList.size() - 1);
        assertThat(testAziRappresentante.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testAziRappresentante.getCognome()).isEqualTo(UPDATED_COGNOME);
        assertThat(testAziRappresentante.getSesso()).isEqualTo(UPDATED_SESSO);
        assertThat(testAziRappresentante.getDataNascita()).isEqualTo(UPDATED_DATA_NASCITA);
        assertThat(testAziRappresentante.getCodiceFiscale()).isEqualTo(UPDATED_CODICE_FISCALE);
        assertThat(testAziRappresentante.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziRappresentante.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziRappresentante.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziRappresentante.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziRappresentante.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziRappresentante() throws Exception {
        int databaseSizeBeforeUpdate = aziRappresentanteRepository.findAll().size();

        // Create the AziRappresentante

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziRappresentanteMockMvc.perform(put("/api/azi-rappresentantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentante)))
            .andExpect(status().isBadRequest());

        // Validate the AziRappresentante in the database
        List<AziRappresentante> aziRappresentanteList = aziRappresentanteRepository.findAll();
        assertThat(aziRappresentanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziRappresentante() throws Exception {
        // Initialize the database
        aziRappresentanteService.save(aziRappresentante);

        int databaseSizeBeforeDelete = aziRappresentanteRepository.findAll().size();

        // Delete the aziRappresentante
        restAziRappresentanteMockMvc.perform(delete("/api/azi-rappresentantes/{id}", aziRappresentante.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziRappresentante> aziRappresentanteList = aziRappresentanteRepository.findAll();
        assertThat(aziRappresentanteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziRappresentante.class);
        AziRappresentante aziRappresentante1 = new AziRappresentante();
        aziRappresentante1.setId(1L);
        AziRappresentante aziRappresentante2 = new AziRappresentante();
        aziRappresentante2.setId(aziRappresentante1.getId());
        assertThat(aziRappresentante1).isEqualTo(aziRappresentante2);
        aziRappresentante2.setId(2L);
        assertThat(aziRappresentante1).isNotEqualTo(aziRappresentante2);
        aziRappresentante1.setId(null);
        assertThat(aziRappresentante1).isNotEqualTo(aziRappresentante2);
    }
}
