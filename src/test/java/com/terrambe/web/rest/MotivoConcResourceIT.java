package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.MotivoConc;
import com.terrambe.domain.RegOpcConc;
import com.terrambe.repository.MotivoConcRepository;
import com.terrambe.service.MotivoConcService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.MotivoConcCriteria;
import com.terrambe.service.MotivoConcQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MotivoConcResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class MotivoConcResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private MotivoConcRepository motivoConcRepository;

    @Autowired
    private MotivoConcService motivoConcService;

    @Autowired
    private MotivoConcQueryService motivoConcQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMotivoConcMockMvc;

    private MotivoConc motivoConc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotivoConcResource motivoConcResource = new MotivoConcResource(motivoConcService, motivoConcQueryService);
        this.restMotivoConcMockMvc = MockMvcBuilders.standaloneSetup(motivoConcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoConc createEntity(EntityManager em) {
        MotivoConc motivoConc = new MotivoConc()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return motivoConc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoConc createUpdatedEntity(EntityManager em) {
        MotivoConc motivoConc = new MotivoConc()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return motivoConc;
    }

    @BeforeEach
    public void initTest() {
        motivoConc = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotivoConc() throws Exception {
        int databaseSizeBeforeCreate = motivoConcRepository.findAll().size();

        // Create the MotivoConc
        restMotivoConcMockMvc.perform(post("/api/motivo-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoConc)))
            .andExpect(status().isCreated());

        // Validate the MotivoConc in the database
        List<MotivoConc> motivoConcList = motivoConcRepository.findAll();
        assertThat(motivoConcList).hasSize(databaseSizeBeforeCreate + 1);
        MotivoConc testMotivoConc = motivoConcList.get(motivoConcList.size() - 1);
        assertThat(testMotivoConc.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testMotivoConc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testMotivoConc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testMotivoConc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testMotivoConc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createMotivoConcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motivoConcRepository.findAll().size();

        // Create the MotivoConc with an existing ID
        motivoConc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotivoConcMockMvc.perform(post("/api/motivo-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoConc)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoConc in the database
        List<MotivoConc> motivoConcList = motivoConcRepository.findAll();
        assertThat(motivoConcList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMotivoConcs() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList
        restMotivoConcMockMvc.perform(get("/api/motivo-concs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getMotivoConc() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get the motivoConc
        restMotivoConcMockMvc.perform(get("/api/motivo-concs/{id}", motivoConc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motivoConc.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultMotivoConcShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the motivoConcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoConcShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultMotivoConcShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the motivoConcList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoConcShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where descrizione is not null
        defaultMotivoConcShouldBeFound("descrizione.specified=true");

        // Get all the motivoConcList where descrizione is null
        defaultMotivoConcShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali is not null
        defaultMotivoConcShouldBeFound("dataInizVali.specified=true");

        // Get all the motivoConcList where dataInizVali is null
        defaultMotivoConcShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultMotivoConcShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoConcList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultMotivoConcShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali is not null
        defaultMotivoConcShouldBeFound("dataFineVali.specified=true");

        // Get all the motivoConcList where dataFineVali is null
        defaultMotivoConcShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultMotivoConcShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoConcList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultMotivoConcShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator is not null
        defaultMotivoConcShouldBeFound("userIdCreator.specified=true");

        // Get all the motivoConcList where userIdCreator is null
        defaultMotivoConcShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultMotivoConcShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoConcList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultMotivoConcShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod is not null
        defaultMotivoConcShouldBeFound("userIdLastMod.specified=true");

        // Get all the motivoConcList where userIdLastMod is null
        defaultMotivoConcShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoConcsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);

        // Get all the motivoConcList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoConcShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoConcList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultMotivoConcShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllMotivoConcsByMotivoToOpcConcIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoConcRepository.saveAndFlush(motivoConc);
        RegOpcConc motivoToOpcConc = RegOpcConcResourceIT.createEntity(em);
        em.persist(motivoToOpcConc);
        em.flush();
        motivoConc.addMotivoToOpcConc(motivoToOpcConc);
        motivoConcRepository.saveAndFlush(motivoConc);
        Long motivoToOpcConcId = motivoToOpcConc.getId();

        // Get all the motivoConcList where motivoToOpcConc equals to motivoToOpcConcId
        defaultMotivoConcShouldBeFound("motivoToOpcConcId.equals=" + motivoToOpcConcId);

        // Get all the motivoConcList where motivoToOpcConc equals to motivoToOpcConcId + 1
        defaultMotivoConcShouldNotBeFound("motivoToOpcConcId.equals=" + (motivoToOpcConcId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMotivoConcShouldBeFound(String filter) throws Exception {
        restMotivoConcMockMvc.perform(get("/api/motivo-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoConc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restMotivoConcMockMvc.perform(get("/api/motivo-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMotivoConcShouldNotBeFound(String filter) throws Exception {
        restMotivoConcMockMvc.perform(get("/api/motivo-concs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMotivoConcMockMvc.perform(get("/api/motivo-concs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMotivoConc() throws Exception {
        // Get the motivoConc
        restMotivoConcMockMvc.perform(get("/api/motivo-concs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotivoConc() throws Exception {
        // Initialize the database
        motivoConcService.save(motivoConc);

        int databaseSizeBeforeUpdate = motivoConcRepository.findAll().size();

        // Update the motivoConc
        MotivoConc updatedMotivoConc = motivoConcRepository.findById(motivoConc.getId()).get();
        // Disconnect from session so that the updates on updatedMotivoConc are not directly saved in db
        em.detach(updatedMotivoConc);
        updatedMotivoConc
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restMotivoConcMockMvc.perform(put("/api/motivo-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotivoConc)))
            .andExpect(status().isOk());

        // Validate the MotivoConc in the database
        List<MotivoConc> motivoConcList = motivoConcRepository.findAll();
        assertThat(motivoConcList).hasSize(databaseSizeBeforeUpdate);
        MotivoConc testMotivoConc = motivoConcList.get(motivoConcList.size() - 1);
        assertThat(testMotivoConc.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testMotivoConc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testMotivoConc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testMotivoConc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testMotivoConc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingMotivoConc() throws Exception {
        int databaseSizeBeforeUpdate = motivoConcRepository.findAll().size();

        // Create the MotivoConc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMotivoConcMockMvc.perform(put("/api/motivo-concs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoConc)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoConc in the database
        List<MotivoConc> motivoConcList = motivoConcRepository.findAll();
        assertThat(motivoConcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMotivoConc() throws Exception {
        // Initialize the database
        motivoConcService.save(motivoConc);

        int databaseSizeBeforeDelete = motivoConcRepository.findAll().size();

        // Delete the motivoConc
        restMotivoConcMockMvc.perform(delete("/api/motivo-concs/{id}", motivoConc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MotivoConc> motivoConcList = motivoConcRepository.findAll();
        assertThat(motivoConcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotivoConc.class);
        MotivoConc motivoConc1 = new MotivoConc();
        motivoConc1.setId(1L);
        MotivoConc motivoConc2 = new MotivoConc();
        motivoConc2.setId(motivoConc1.getId());
        assertThat(motivoConc1).isEqualTo(motivoConc2);
        motivoConc2.setId(2L);
        assertThat(motivoConc1).isNotEqualTo(motivoConc2);
        motivoConc1.setId(null);
        assertThat(motivoConc1).isNotEqualTo(motivoConc2);
    }
}
