package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.domain.AziSupPartConduzione;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.Campi;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.repository.AziSuperficieParticellaRepository;
import com.terrambe.service.AziSuperficieParticellaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziSuperficieParticellaCriteria;
import com.terrambe.service.AziSuperficieParticellaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziSuperficieParticellaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziSuperficieParticellaResourceIT {

    private static final String DEFAULT_FOGLIO = "AAAAAAAAAA";
    private static final String UPDATED_FOGLIO = "BBBBBBBBBB";

    private static final String DEFAULT_PART = "AAAAAAAAAA";
    private static final String UPDATED_PART = "BBBBBBBBBB";

    private static final String DEFAULT_SUB = "AAAAAAAAAA";
    private static final String UPDATED_SUB = "BBBBBBBBBB";

    private static final String DEFAULT_SEZIONE = "AAAAAAAAAA";
    private static final String UPDATED_SEZIONE = "BBBBBBBBBB";

    private static final Float DEFAULT_SUPERFICIE = 1F;
    private static final Float UPDATED_SUPERFICIE = 2F;
    private static final Float SMALLER_SUPERFICIE = 1F - 1F;

    private static final Float DEFAULT_SUP_CATASTALE = 1F;
    private static final Float UPDATED_SUP_CATASTALE = 2F;
    private static final Float SMALLER_SUP_CATASTALE = 1F - 1F;

    private static final Float DEFAULT_SUP_UTILIZZABILE = 1F;
    private static final Float UPDATED_SUP_UTILIZZABILE = 2F;
    private static final Float SMALLER_SUP_UTILIZZABILE = 1F - 1F;

    private static final Float DEFAULT_SUP_TARA = 1F;
    private static final Float UPDATED_SUP_TARA = 2F;
    private static final Float SMALLER_SUP_TARA = 1F - 1F;

    private static final String DEFAULT_UUID_ESRI = "AAAAAAAAAA";
    private static final String UPDATED_UUID_ESRI = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziSuperficieParticellaRepository aziSuperficieParticellaRepository;

    @Autowired
    private AziSuperficieParticellaService aziSuperficieParticellaService;

    @Autowired
    private AziSuperficieParticellaQueryService aziSuperficieParticellaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziSuperficieParticellaMockMvc;

    private AziSuperficieParticella aziSuperficieParticella;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziSuperficieParticellaResource aziSuperficieParticellaResource = new AziSuperficieParticellaResource(aziSuperficieParticellaService, aziSuperficieParticellaQueryService);
        this.restAziSuperficieParticellaMockMvc = MockMvcBuilders.standaloneSetup(aziSuperficieParticellaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSuperficieParticella createEntity(EntityManager em) {
        AziSuperficieParticella aziSuperficieParticella = new AziSuperficieParticella()
            .foglio(DEFAULT_FOGLIO)
            .part(DEFAULT_PART)
            .sub(DEFAULT_SUB)
            .sezione(DEFAULT_SEZIONE)
            .superficie(DEFAULT_SUPERFICIE)
            .supCatastale(DEFAULT_SUP_CATASTALE)
            .supUtilizzabile(DEFAULT_SUP_UTILIZZABILE)
            .supTara(DEFAULT_SUP_TARA)
            .uuidEsri(DEFAULT_UUID_ESRI)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziSuperficieParticella;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSuperficieParticella createUpdatedEntity(EntityManager em) {
        AziSuperficieParticella aziSuperficieParticella = new AziSuperficieParticella()
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .sezione(UPDATED_SEZIONE)
            .superficie(UPDATED_SUPERFICIE)
            .supCatastale(UPDATED_SUP_CATASTALE)
            .supUtilizzabile(UPDATED_SUP_UTILIZZABILE)
            .supTara(UPDATED_SUP_TARA)
            .uuidEsri(UPDATED_UUID_ESRI)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziSuperficieParticella;
    }

    @BeforeEach
    public void initTest() {
        aziSuperficieParticella = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziSuperficieParticella() throws Exception {
        int databaseSizeBeforeCreate = aziSuperficieParticellaRepository.findAll().size();

        // Create the AziSuperficieParticella
        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isCreated());

        // Validate the AziSuperficieParticella in the database
        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeCreate + 1);
        AziSuperficieParticella testAziSuperficieParticella = aziSuperficieParticellaList.get(aziSuperficieParticellaList.size() - 1);
        assertThat(testAziSuperficieParticella.getFoglio()).isEqualTo(DEFAULT_FOGLIO);
        assertThat(testAziSuperficieParticella.getPart()).isEqualTo(DEFAULT_PART);
        assertThat(testAziSuperficieParticella.getSub()).isEqualTo(DEFAULT_SUB);
        assertThat(testAziSuperficieParticella.getSezione()).isEqualTo(DEFAULT_SEZIONE);
        assertThat(testAziSuperficieParticella.getSuperficie()).isEqualTo(DEFAULT_SUPERFICIE);
        assertThat(testAziSuperficieParticella.getSupCatastale()).isEqualTo(DEFAULT_SUP_CATASTALE);
        assertThat(testAziSuperficieParticella.getSupUtilizzabile()).isEqualTo(DEFAULT_SUP_UTILIZZABILE);
        assertThat(testAziSuperficieParticella.getSupTara()).isEqualTo(DEFAULT_SUP_TARA);
        assertThat(testAziSuperficieParticella.getUuidEsri()).isEqualTo(DEFAULT_UUID_ESRI);
        assertThat(testAziSuperficieParticella.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testAziSuperficieParticella.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testAziSuperficieParticella.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testAziSuperficieParticella.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testAziSuperficieParticella.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziSuperficieParticella.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziSuperficieParticella.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziSuperficieParticella.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziSuperficieParticella.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziSuperficieParticellaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziSuperficieParticellaRepository.findAll().size();

        // Create the AziSuperficieParticella with an existing ID
        aziSuperficieParticella.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        // Validate the AziSuperficieParticella in the database
        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFoglioIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setFoglio(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPartIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setPart(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSuperficieIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setSuperficie(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupCatastaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setSupCatastale(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupUtilizzabileIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setSupUtilizzabile(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupTaraIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setSupTara(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUuidEsriIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setUuidEsri(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setNazione(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRegioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setRegione(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProvinciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setProvincia(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkComuneIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSuperficieParticellaRepository.findAll().size();
        // set the field null
        aziSuperficieParticella.setComune(null);

        // Create the AziSuperficieParticella, which fails.

        restAziSuperficieParticellaMockMvc.perform(post("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellas() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSuperficieParticella.getId().intValue())))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO.toString())))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART.toString())))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB.toString())))
            .andExpect(jsonPath("$.[*].sezione").value(hasItem(DEFAULT_SEZIONE.toString())))
            .andExpect(jsonPath("$.[*].superficie").value(hasItem(DEFAULT_SUPERFICIE.doubleValue())))
            .andExpect(jsonPath("$.[*].supCatastale").value(hasItem(DEFAULT_SUP_CATASTALE.doubleValue())))
            .andExpect(jsonPath("$.[*].supUtilizzabile").value(hasItem(DEFAULT_SUP_UTILIZZABILE.doubleValue())))
            .andExpect(jsonPath("$.[*].supTara").value(hasItem(DEFAULT_SUP_TARA.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziSuperficieParticella() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get the aziSuperficieParticella
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas/{id}", aziSuperficieParticella.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziSuperficieParticella.getId().intValue()))
            .andExpect(jsonPath("$.foglio").value(DEFAULT_FOGLIO.toString()))
            .andExpect(jsonPath("$.part").value(DEFAULT_PART.toString()))
            .andExpect(jsonPath("$.sub").value(DEFAULT_SUB.toString()))
            .andExpect(jsonPath("$.sezione").value(DEFAULT_SEZIONE.toString()))
            .andExpect(jsonPath("$.superficie").value(DEFAULT_SUPERFICIE.doubleValue()))
            .andExpect(jsonPath("$.supCatastale").value(DEFAULT_SUP_CATASTALE.doubleValue()))
            .andExpect(jsonPath("$.supUtilizzabile").value(DEFAULT_SUP_UTILIZZABILE.doubleValue()))
            .andExpect(jsonPath("$.supTara").value(DEFAULT_SUP_TARA.doubleValue()))
            .andExpect(jsonPath("$.uuidEsri").value(DEFAULT_UUID_ESRI.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByFoglioIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where foglio equals to DEFAULT_FOGLIO
        defaultAziSuperficieParticellaShouldBeFound("foglio.equals=" + DEFAULT_FOGLIO);

        // Get all the aziSuperficieParticellaList where foglio equals to UPDATED_FOGLIO
        defaultAziSuperficieParticellaShouldNotBeFound("foglio.equals=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByFoglioIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where foglio in DEFAULT_FOGLIO or UPDATED_FOGLIO
        defaultAziSuperficieParticellaShouldBeFound("foglio.in=" + DEFAULT_FOGLIO + "," + UPDATED_FOGLIO);

        // Get all the aziSuperficieParticellaList where foglio equals to UPDATED_FOGLIO
        defaultAziSuperficieParticellaShouldNotBeFound("foglio.in=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByFoglioIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where foglio is not null
        defaultAziSuperficieParticellaShouldBeFound("foglio.specified=true");

        // Get all the aziSuperficieParticellaList where foglio is null
        defaultAziSuperficieParticellaShouldNotBeFound("foglio.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where part equals to DEFAULT_PART
        defaultAziSuperficieParticellaShouldBeFound("part.equals=" + DEFAULT_PART);

        // Get all the aziSuperficieParticellaList where part equals to UPDATED_PART
        defaultAziSuperficieParticellaShouldNotBeFound("part.equals=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where part in DEFAULT_PART or UPDATED_PART
        defaultAziSuperficieParticellaShouldBeFound("part.in=" + DEFAULT_PART + "," + UPDATED_PART);

        // Get all the aziSuperficieParticellaList where part equals to UPDATED_PART
        defaultAziSuperficieParticellaShouldNotBeFound("part.in=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where part is not null
        defaultAziSuperficieParticellaShouldBeFound("part.specified=true");

        // Get all the aziSuperficieParticellaList where part is null
        defaultAziSuperficieParticellaShouldNotBeFound("part.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySubIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sub equals to DEFAULT_SUB
        defaultAziSuperficieParticellaShouldBeFound("sub.equals=" + DEFAULT_SUB);

        // Get all the aziSuperficieParticellaList where sub equals to UPDATED_SUB
        defaultAziSuperficieParticellaShouldNotBeFound("sub.equals=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySubIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sub in DEFAULT_SUB or UPDATED_SUB
        defaultAziSuperficieParticellaShouldBeFound("sub.in=" + DEFAULT_SUB + "," + UPDATED_SUB);

        // Get all the aziSuperficieParticellaList where sub equals to UPDATED_SUB
        defaultAziSuperficieParticellaShouldNotBeFound("sub.in=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySubIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sub is not null
        defaultAziSuperficieParticellaShouldBeFound("sub.specified=true");

        // Get all the aziSuperficieParticellaList where sub is null
        defaultAziSuperficieParticellaShouldNotBeFound("sub.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySezioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sezione equals to DEFAULT_SEZIONE
        defaultAziSuperficieParticellaShouldBeFound("sezione.equals=" + DEFAULT_SEZIONE);

        // Get all the aziSuperficieParticellaList where sezione equals to UPDATED_SEZIONE
        defaultAziSuperficieParticellaShouldNotBeFound("sezione.equals=" + UPDATED_SEZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySezioneIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sezione in DEFAULT_SEZIONE or UPDATED_SEZIONE
        defaultAziSuperficieParticellaShouldBeFound("sezione.in=" + DEFAULT_SEZIONE + "," + UPDATED_SEZIONE);

        // Get all the aziSuperficieParticellaList where sezione equals to UPDATED_SEZIONE
        defaultAziSuperficieParticellaShouldNotBeFound("sezione.in=" + UPDATED_SEZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySezioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where sezione is not null
        defaultAziSuperficieParticellaShouldBeFound("sezione.specified=true");

        // Get all the aziSuperficieParticellaList where sezione is null
        defaultAziSuperficieParticellaShouldNotBeFound("sezione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie equals to DEFAULT_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.equals=" + DEFAULT_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie equals to UPDATED_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.equals=" + UPDATED_SUPERFICIE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie in DEFAULT_SUPERFICIE or UPDATED_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.in=" + DEFAULT_SUPERFICIE + "," + UPDATED_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie equals to UPDATED_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.in=" + UPDATED_SUPERFICIE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie is not null
        defaultAziSuperficieParticellaShouldBeFound("superficie.specified=true");

        // Get all the aziSuperficieParticellaList where superficie is null
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie is greater than or equal to DEFAULT_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.greaterThanOrEqual=" + DEFAULT_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie is greater than or equal to UPDATED_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.greaterThanOrEqual=" + UPDATED_SUPERFICIE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie is less than or equal to DEFAULT_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.lessThanOrEqual=" + DEFAULT_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie is less than or equal to SMALLER_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.lessThanOrEqual=" + SMALLER_SUPERFICIE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie is less than DEFAULT_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.lessThan=" + DEFAULT_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie is less than UPDATED_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.lessThan=" + UPDATED_SUPERFICIE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySuperficieIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where superficie is greater than DEFAULT_SUPERFICIE
        defaultAziSuperficieParticellaShouldNotBeFound("superficie.greaterThan=" + DEFAULT_SUPERFICIE);

        // Get all the aziSuperficieParticellaList where superficie is greater than SMALLER_SUPERFICIE
        defaultAziSuperficieParticellaShouldBeFound("superficie.greaterThan=" + SMALLER_SUPERFICIE);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale equals to DEFAULT_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.equals=" + DEFAULT_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale equals to UPDATED_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.equals=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale in DEFAULT_SUP_CATASTALE or UPDATED_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.in=" + DEFAULT_SUP_CATASTALE + "," + UPDATED_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale equals to UPDATED_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.in=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale is not null
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.specified=true");

        // Get all the aziSuperficieParticellaList where supCatastale is null
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale is greater than or equal to DEFAULT_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.greaterThanOrEqual=" + DEFAULT_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale is greater than or equal to UPDATED_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.greaterThanOrEqual=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale is less than or equal to DEFAULT_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.lessThanOrEqual=" + DEFAULT_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale is less than or equal to SMALLER_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.lessThanOrEqual=" + SMALLER_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale is less than DEFAULT_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.lessThan=" + DEFAULT_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale is less than UPDATED_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.lessThan=" + UPDATED_SUP_CATASTALE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupCatastaleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supCatastale is greater than DEFAULT_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldNotBeFound("supCatastale.greaterThan=" + DEFAULT_SUP_CATASTALE);

        // Get all the aziSuperficieParticellaList where supCatastale is greater than SMALLER_SUP_CATASTALE
        defaultAziSuperficieParticellaShouldBeFound("supCatastale.greaterThan=" + SMALLER_SUP_CATASTALE);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile equals to DEFAULT_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.equals=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile equals to UPDATED_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.equals=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile in DEFAULT_SUP_UTILIZZABILE or UPDATED_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.in=" + DEFAULT_SUP_UTILIZZABILE + "," + UPDATED_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile equals to UPDATED_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.in=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is not null
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.specified=true");

        // Get all the aziSuperficieParticellaList where supUtilizzabile is null
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is greater than or equal to DEFAULT_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.greaterThanOrEqual=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is greater than or equal to UPDATED_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.greaterThanOrEqual=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is less than or equal to DEFAULT_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.lessThanOrEqual=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is less than or equal to SMALLER_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.lessThanOrEqual=" + SMALLER_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is less than DEFAULT_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.lessThan=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is less than UPDATED_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.lessThan=" + UPDATED_SUP_UTILIZZABILE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupUtilizzabileIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is greater than DEFAULT_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldNotBeFound("supUtilizzabile.greaterThan=" + DEFAULT_SUP_UTILIZZABILE);

        // Get all the aziSuperficieParticellaList where supUtilizzabile is greater than SMALLER_SUP_UTILIZZABILE
        defaultAziSuperficieParticellaShouldBeFound("supUtilizzabile.greaterThan=" + SMALLER_SUP_UTILIZZABILE);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara equals to DEFAULT_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.equals=" + DEFAULT_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara equals to UPDATED_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.equals=" + UPDATED_SUP_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara in DEFAULT_SUP_TARA or UPDATED_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.in=" + DEFAULT_SUP_TARA + "," + UPDATED_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara equals to UPDATED_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.in=" + UPDATED_SUP_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara is not null
        defaultAziSuperficieParticellaShouldBeFound("supTara.specified=true");

        // Get all the aziSuperficieParticellaList where supTara is null
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara is greater than or equal to DEFAULT_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.greaterThanOrEqual=" + DEFAULT_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara is greater than or equal to UPDATED_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.greaterThanOrEqual=" + UPDATED_SUP_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara is less than or equal to DEFAULT_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.lessThanOrEqual=" + DEFAULT_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara is less than or equal to SMALLER_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.lessThanOrEqual=" + SMALLER_SUP_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara is less than DEFAULT_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.lessThan=" + DEFAULT_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara is less than UPDATED_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.lessThan=" + UPDATED_SUP_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasBySupTaraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where supTara is greater than DEFAULT_SUP_TARA
        defaultAziSuperficieParticellaShouldNotBeFound("supTara.greaterThan=" + DEFAULT_SUP_TARA);

        // Get all the aziSuperficieParticellaList where supTara is greater than SMALLER_SUP_TARA
        defaultAziSuperficieParticellaShouldBeFound("supTara.greaterThan=" + SMALLER_SUP_TARA);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUuidEsriIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where uuidEsri equals to DEFAULT_UUID_ESRI
        defaultAziSuperficieParticellaShouldBeFound("uuidEsri.equals=" + DEFAULT_UUID_ESRI);

        // Get all the aziSuperficieParticellaList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAziSuperficieParticellaShouldNotBeFound("uuidEsri.equals=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUuidEsriIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where uuidEsri in DEFAULT_UUID_ESRI or UPDATED_UUID_ESRI
        defaultAziSuperficieParticellaShouldBeFound("uuidEsri.in=" + DEFAULT_UUID_ESRI + "," + UPDATED_UUID_ESRI);

        // Get all the aziSuperficieParticellaList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAziSuperficieParticellaShouldNotBeFound("uuidEsri.in=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUuidEsriIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where uuidEsri is not null
        defaultAziSuperficieParticellaShouldBeFound("uuidEsri.specified=true");

        // Get all the aziSuperficieParticellaList where uuidEsri is null
        defaultAziSuperficieParticellaShouldNotBeFound("uuidEsri.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where nazione equals to DEFAULT_NAZIONE
        defaultAziSuperficieParticellaShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the aziSuperficieParticellaList where nazione equals to UPDATED_NAZIONE
        defaultAziSuperficieParticellaShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultAziSuperficieParticellaShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the aziSuperficieParticellaList where nazione equals to UPDATED_NAZIONE
        defaultAziSuperficieParticellaShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where nazione is not null
        defaultAziSuperficieParticellaShouldBeFound("nazione.specified=true");

        // Get all the aziSuperficieParticellaList where nazione is null
        defaultAziSuperficieParticellaShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where regione equals to DEFAULT_REGIONE
        defaultAziSuperficieParticellaShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the aziSuperficieParticellaList where regione equals to UPDATED_REGIONE
        defaultAziSuperficieParticellaShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultAziSuperficieParticellaShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the aziSuperficieParticellaList where regione equals to UPDATED_REGIONE
        defaultAziSuperficieParticellaShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where regione is not null
        defaultAziSuperficieParticellaShouldBeFound("regione.specified=true");

        // Get all the aziSuperficieParticellaList where regione is null
        defaultAziSuperficieParticellaShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where provincia equals to DEFAULT_PROVINCIA
        defaultAziSuperficieParticellaShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the aziSuperficieParticellaList where provincia equals to UPDATED_PROVINCIA
        defaultAziSuperficieParticellaShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultAziSuperficieParticellaShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the aziSuperficieParticellaList where provincia equals to UPDATED_PROVINCIA
        defaultAziSuperficieParticellaShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where provincia is not null
        defaultAziSuperficieParticellaShouldBeFound("provincia.specified=true");

        // Get all the aziSuperficieParticellaList where provincia is null
        defaultAziSuperficieParticellaShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where comune equals to DEFAULT_COMUNE
        defaultAziSuperficieParticellaShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the aziSuperficieParticellaList where comune equals to UPDATED_COMUNE
        defaultAziSuperficieParticellaShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultAziSuperficieParticellaShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the aziSuperficieParticellaList where comune equals to UPDATED_COMUNE
        defaultAziSuperficieParticellaShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where comune is not null
        defaultAziSuperficieParticellaShouldBeFound("comune.specified=true");

        // Get all the aziSuperficieParticellaList where comune is null
        defaultAziSuperficieParticellaShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali is not null
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.specified=true");

        // Get all the aziSuperficieParticellaList where dataInizVali is null
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSuperficieParticellaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali is not null
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.specified=true");

        // Get all the aziSuperficieParticellaList where dataFineVali is null
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSuperficieParticellaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziSuperficieParticellaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator is not null
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.specified=true");

        // Get all the aziSuperficieParticellaList where userIdCreator is null
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSuperficieParticellaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziSuperficieParticellaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod is not null
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziSuperficieParticellaList where userIdLastMod is null
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);

        // Get all the aziSuperficieParticellaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSuperficieParticellaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziSuperficieParticellaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartToDettaglioIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        AziSupPartDettaglio partToDettaglio = AziSupPartDettaglioResourceIT.createEntity(em);
        em.persist(partToDettaglio);
        em.flush();
        aziSuperficieParticella.addPartToDettaglio(partToDettaglio);
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Long partToDettaglioId = partToDettaglio.getId();

        // Get all the aziSuperficieParticellaList where partToDettaglio equals to partToDettaglioId
        defaultAziSuperficieParticellaShouldBeFound("partToDettaglioId.equals=" + partToDettaglioId);

        // Get all the aziSuperficieParticellaList where partToDettaglio equals to partToDettaglioId + 1
        defaultAziSuperficieParticellaShouldNotBeFound("partToDettaglioId.equals=" + (partToDettaglioId + 1));
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByConduzioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        AziSupPartConduzione conduzione = AziSupPartConduzioneResourceIT.createEntity(em);
        em.persist(conduzione);
        em.flush();
        aziSuperficieParticella.setConduzione(conduzione);
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Long conduzioneId = conduzione.getId();

        // Get all the aziSuperficieParticellaList where conduzione equals to conduzioneId
        defaultAziSuperficieParticellaShouldBeFound("conduzioneId.equals=" + conduzioneId);

        // Get all the aziSuperficieParticellaList where conduzione equals to conduzioneId + 1
        defaultAziSuperficieParticellaShouldNotBeFound("conduzioneId.equals=" + (conduzioneId + 1));
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartToAziAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        AziAnagrafica partToAziAna = AziAnagraficaResourceIT.createEntity(em);
        em.persist(partToAziAna);
        em.flush();
        aziSuperficieParticella.setPartToAziAna(partToAziAna);
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Long partToAziAnaId = partToAziAna.getId();

        // Get all the aziSuperficieParticellaList where partToAziAna equals to partToAziAnaId
        defaultAziSuperficieParticellaShouldBeFound("partToAziAnaId.equals=" + partToAziAnaId);

        // Get all the aziSuperficieParticellaList where partToAziAna equals to partToAziAnaId + 1
        defaultAziSuperficieParticellaShouldNotBeFound("partToAziAnaId.equals=" + (partToAziAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByAziSupToCampiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Campi aziSupToCampi = CampiResourceIT.createEntity(em);
        em.persist(aziSupToCampi);
        em.flush();
        aziSuperficieParticella.setAziSupToCampi(aziSupToCampi);
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Long aziSupToCampiId = aziSupToCampi.getId();

        // Get all the aziSuperficieParticellaList where aziSupToCampi equals to aziSupToCampiId
        defaultAziSuperficieParticellaShouldBeFound("aziSupToCampiId.equals=" + aziSupToCampiId);

        // Get all the aziSuperficieParticellaList where aziSupToCampi equals to aziSupToCampiId + 1
        defaultAziSuperficieParticellaShouldNotBeFound("aziSupToCampiId.equals=" + (aziSupToCampiId + 1));
    }


    @Test
    @Transactional
    public void getAllAziSuperficieParticellasByPartToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Appezzamenti partToAppz = AppezzamentiResourceIT.createEntity(em);
        em.persist(partToAppz);
        em.flush();
        aziSuperficieParticella.addPartToAppz(partToAppz);
        aziSuperficieParticellaRepository.saveAndFlush(aziSuperficieParticella);
        Long partToAppzId = partToAppz.getId();

        // Get all the aziSuperficieParticellaList where partToAppz equals to partToAppzId
        defaultAziSuperficieParticellaShouldBeFound("partToAppzId.equals=" + partToAppzId);

        // Get all the aziSuperficieParticellaList where partToAppz equals to partToAppzId + 1
        defaultAziSuperficieParticellaShouldNotBeFound("partToAppzId.equals=" + (partToAppzId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziSuperficieParticellaShouldBeFound(String filter) throws Exception {
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSuperficieParticella.getId().intValue())))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO)))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART)))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB)))
            .andExpect(jsonPath("$.[*].sezione").value(hasItem(DEFAULT_SEZIONE)))
            .andExpect(jsonPath("$.[*].superficie").value(hasItem(DEFAULT_SUPERFICIE.doubleValue())))
            .andExpect(jsonPath("$.[*].supCatastale").value(hasItem(DEFAULT_SUP_CATASTALE.doubleValue())))
            .andExpect(jsonPath("$.[*].supUtilizzabile").value(hasItem(DEFAULT_SUP_UTILIZZABILE.doubleValue())))
            .andExpect(jsonPath("$.[*].supTara").value(hasItem(DEFAULT_SUP_TARA.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziSuperficieParticellaShouldNotBeFound(String filter) throws Exception {
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziSuperficieParticella() throws Exception {
        // Get the aziSuperficieParticella
        restAziSuperficieParticellaMockMvc.perform(get("/api/azi-superficie-particellas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziSuperficieParticella() throws Exception {
        // Initialize the database
        aziSuperficieParticellaService.save(aziSuperficieParticella);

        int databaseSizeBeforeUpdate = aziSuperficieParticellaRepository.findAll().size();

        // Update the aziSuperficieParticella
        AziSuperficieParticella updatedAziSuperficieParticella = aziSuperficieParticellaRepository.findById(aziSuperficieParticella.getId()).get();
        // Disconnect from session so that the updates on updatedAziSuperficieParticella are not directly saved in db
        em.detach(updatedAziSuperficieParticella);
        updatedAziSuperficieParticella
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .sezione(UPDATED_SEZIONE)
            .superficie(UPDATED_SUPERFICIE)
            .supCatastale(UPDATED_SUP_CATASTALE)
            .supUtilizzabile(UPDATED_SUP_UTILIZZABILE)
            .supTara(UPDATED_SUP_TARA)
            .uuidEsri(UPDATED_UUID_ESRI)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziSuperficieParticellaMockMvc.perform(put("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziSuperficieParticella)))
            .andExpect(status().isOk());

        // Validate the AziSuperficieParticella in the database
        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeUpdate);
        AziSuperficieParticella testAziSuperficieParticella = aziSuperficieParticellaList.get(aziSuperficieParticellaList.size() - 1);
        assertThat(testAziSuperficieParticella.getFoglio()).isEqualTo(UPDATED_FOGLIO);
        assertThat(testAziSuperficieParticella.getPart()).isEqualTo(UPDATED_PART);
        assertThat(testAziSuperficieParticella.getSub()).isEqualTo(UPDATED_SUB);
        assertThat(testAziSuperficieParticella.getSezione()).isEqualTo(UPDATED_SEZIONE);
        assertThat(testAziSuperficieParticella.getSuperficie()).isEqualTo(UPDATED_SUPERFICIE);
        assertThat(testAziSuperficieParticella.getSupCatastale()).isEqualTo(UPDATED_SUP_CATASTALE);
        assertThat(testAziSuperficieParticella.getSupUtilizzabile()).isEqualTo(UPDATED_SUP_UTILIZZABILE);
        assertThat(testAziSuperficieParticella.getSupTara()).isEqualTo(UPDATED_SUP_TARA);
        assertThat(testAziSuperficieParticella.getUuidEsri()).isEqualTo(UPDATED_UUID_ESRI);
        assertThat(testAziSuperficieParticella.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testAziSuperficieParticella.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testAziSuperficieParticella.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testAziSuperficieParticella.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testAziSuperficieParticella.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziSuperficieParticella.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziSuperficieParticella.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziSuperficieParticella.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziSuperficieParticella.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziSuperficieParticella() throws Exception {
        int databaseSizeBeforeUpdate = aziSuperficieParticellaRepository.findAll().size();

        // Create the AziSuperficieParticella

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziSuperficieParticellaMockMvc.perform(put("/api/azi-superficie-particellas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSuperficieParticella)))
            .andExpect(status().isBadRequest());

        // Validate the AziSuperficieParticella in the database
        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziSuperficieParticella() throws Exception {
        // Initialize the database
        aziSuperficieParticellaService.save(aziSuperficieParticella);

        int databaseSizeBeforeDelete = aziSuperficieParticellaRepository.findAll().size();

        // Delete the aziSuperficieParticella
        restAziSuperficieParticellaMockMvc.perform(delete("/api/azi-superficie-particellas/{id}", aziSuperficieParticella.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziSuperficieParticella> aziSuperficieParticellaList = aziSuperficieParticellaRepository.findAll();
        assertThat(aziSuperficieParticellaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziSuperficieParticella.class);
        AziSuperficieParticella aziSuperficieParticella1 = new AziSuperficieParticella();
        aziSuperficieParticella1.setId(1L);
        AziSuperficieParticella aziSuperficieParticella2 = new AziSuperficieParticella();
        aziSuperficieParticella2.setId(aziSuperficieParticella1.getId());
        assertThat(aziSuperficieParticella1).isEqualTo(aziSuperficieParticella2);
        aziSuperficieParticella2.setId(2L);
        assertThat(aziSuperficieParticella1).isNotEqualTo(aziSuperficieParticella2);
        aziSuperficieParticella1.setId(null);
        assertThat(aziSuperficieParticella1).isNotEqualTo(aziSuperficieParticella2);
    }
}
