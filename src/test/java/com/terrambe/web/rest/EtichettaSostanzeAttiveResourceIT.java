package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.SostanzeAttive;
import com.terrambe.repository.EtichettaSostanzeAttiveRepository;
import com.terrambe.service.EtichettaSostanzeAttiveService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.EtichettaSostanzeAttiveCriteria;
import com.terrambe.service.EtichettaSostanzeAttiveQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtichettaSostanzeAttiveResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class EtichettaSostanzeAttiveResourceIT {

    private static final Float DEFAULT_GRAMMI_LITRO = 1F;
    private static final Float UPDATED_GRAMMI_LITRO = 2F;
    private static final Float SMALLER_GRAMMI_LITRO = 1F - 1F;

    private static final Float DEFAULT_PERCENTUALE = 1F;
    private static final Float UPDATED_PERCENTUALE = 2F;
    private static final Float SMALLER_PERCENTUALE = 1F - 1F;

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private EtichettaSostanzeAttiveRepository etichettaSostanzeAttiveRepository;

    @Autowired
    private EtichettaSostanzeAttiveService etichettaSostanzeAttiveService;

    @Autowired
    private EtichettaSostanzeAttiveQueryService etichettaSostanzeAttiveQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEtichettaSostanzeAttiveMockMvc;

    private EtichettaSostanzeAttive etichettaSostanzeAttive;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EtichettaSostanzeAttiveResource etichettaSostanzeAttiveResource = new EtichettaSostanzeAttiveResource(etichettaSostanzeAttiveService, etichettaSostanzeAttiveQueryService);
        this.restEtichettaSostanzeAttiveMockMvc = MockMvcBuilders.standaloneSetup(etichettaSostanzeAttiveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaSostanzeAttive createEntity(EntityManager em) {
        EtichettaSostanzeAttive etichettaSostanzeAttive = new EtichettaSostanzeAttive()
            .grammiLitro(DEFAULT_GRAMMI_LITRO)
            .percentuale(DEFAULT_PERCENTUALE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return etichettaSostanzeAttive;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaSostanzeAttive createUpdatedEntity(EntityManager em) {
        EtichettaSostanzeAttive etichettaSostanzeAttive = new EtichettaSostanzeAttive()
            .grammiLitro(UPDATED_GRAMMI_LITRO)
            .percentuale(UPDATED_PERCENTUALE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return etichettaSostanzeAttive;
    }

    @BeforeEach
    public void initTest() {
        etichettaSostanzeAttive = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtichettaSostanzeAttive() throws Exception {
        int databaseSizeBeforeCreate = etichettaSostanzeAttiveRepository.findAll().size();

        // Create the EtichettaSostanzeAttive
        restEtichettaSostanzeAttiveMockMvc.perform(post("/api/etichetta-sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaSostanzeAttive)))
            .andExpect(status().isCreated());

        // Validate the EtichettaSostanzeAttive in the database
        List<EtichettaSostanzeAttive> etichettaSostanzeAttiveList = etichettaSostanzeAttiveRepository.findAll();
        assertThat(etichettaSostanzeAttiveList).hasSize(databaseSizeBeforeCreate + 1);
        EtichettaSostanzeAttive testEtichettaSostanzeAttive = etichettaSostanzeAttiveList.get(etichettaSostanzeAttiveList.size() - 1);
        assertThat(testEtichettaSostanzeAttive.getGrammiLitro()).isEqualTo(DEFAULT_GRAMMI_LITRO);
        assertThat(testEtichettaSostanzeAttive.getPercentuale()).isEqualTo(DEFAULT_PERCENTUALE);
        assertThat(testEtichettaSostanzeAttive.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testEtichettaSostanzeAttive.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testEtichettaSostanzeAttive.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testEtichettaSostanzeAttive.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testEtichettaSostanzeAttive.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testEtichettaSostanzeAttive.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testEtichettaSostanzeAttive.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testEtichettaSostanzeAttive.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createEtichettaSostanzeAttiveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etichettaSostanzeAttiveRepository.findAll().size();

        // Create the EtichettaSostanzeAttive with an existing ID
        etichettaSostanzeAttive.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtichettaSostanzeAttiveMockMvc.perform(post("/api/etichetta-sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaSostanzeAttive)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaSostanzeAttive in the database
        List<EtichettaSostanzeAttive> etichettaSostanzeAttiveList = etichettaSostanzeAttiveRepository.findAll();
        assertThat(etichettaSostanzeAttiveList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttives() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaSostanzeAttive.getId().intValue())))
            .andExpect(jsonPath("$.[*].grammiLitro").value(hasItem(DEFAULT_GRAMMI_LITRO.doubleValue())))
            .andExpect(jsonPath("$.[*].percentuale").value(hasItem(DEFAULT_PERCENTUALE.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtichettaSostanzeAttive() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get the etichettaSostanzeAttive
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives/{id}", etichettaSostanzeAttive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etichettaSostanzeAttive.getId().intValue()))
            .andExpect(jsonPath("$.grammiLitro").value(DEFAULT_GRAMMI_LITRO.doubleValue()))
            .andExpect(jsonPath("$.percentuale").value(DEFAULT_PERCENTUALE.doubleValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro equals to DEFAULT_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.equals=" + DEFAULT_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro equals to UPDATED_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.equals=" + UPDATED_GRAMMI_LITRO);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro in DEFAULT_GRAMMI_LITRO or UPDATED_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.in=" + DEFAULT_GRAMMI_LITRO + "," + UPDATED_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro equals to UPDATED_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.in=" + UPDATED_GRAMMI_LITRO);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.specified=true");

        // Get all the etichettaSostanzeAttiveList where grammiLitro is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is greater than or equal to DEFAULT_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.greaterThanOrEqual=" + DEFAULT_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is greater than or equal to UPDATED_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.greaterThanOrEqual=" + UPDATED_GRAMMI_LITRO);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is less than or equal to DEFAULT_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.lessThanOrEqual=" + DEFAULT_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is less than or equal to SMALLER_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.lessThanOrEqual=" + SMALLER_GRAMMI_LITRO);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is less than DEFAULT_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.lessThan=" + DEFAULT_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is less than UPDATED_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.lessThan=" + UPDATED_GRAMMI_LITRO);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByGrammiLitroIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is greater than DEFAULT_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldNotBeFound("grammiLitro.greaterThan=" + DEFAULT_GRAMMI_LITRO);

        // Get all the etichettaSostanzeAttiveList where grammiLitro is greater than SMALLER_GRAMMI_LITRO
        defaultEtichettaSostanzeAttiveShouldBeFound("grammiLitro.greaterThan=" + SMALLER_GRAMMI_LITRO);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale equals to DEFAULT_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.equals=" + DEFAULT_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale equals to UPDATED_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.equals=" + UPDATED_PERCENTUALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale in DEFAULT_PERCENTUALE or UPDATED_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.in=" + DEFAULT_PERCENTUALE + "," + UPDATED_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale equals to UPDATED_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.in=" + UPDATED_PERCENTUALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.specified=true");

        // Get all the etichettaSostanzeAttiveList where percentuale is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale is greater than or equal to DEFAULT_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.greaterThanOrEqual=" + DEFAULT_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale is greater than or equal to UPDATED_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.greaterThanOrEqual=" + UPDATED_PERCENTUALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale is less than or equal to DEFAULT_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.lessThanOrEqual=" + DEFAULT_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale is less than or equal to SMALLER_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.lessThanOrEqual=" + SMALLER_PERCENTUALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale is less than DEFAULT_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.lessThan=" + DEFAULT_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale is less than UPDATED_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.lessThan=" + UPDATED_PERCENTUALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByPercentualeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where percentuale is greater than DEFAULT_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("percentuale.greaterThan=" + DEFAULT_PERCENTUALE);

        // Get all the etichettaSostanzeAttiveList where percentuale is greater than SMALLER_PERCENTUALE
        defaultEtichettaSostanzeAttiveShouldBeFound("percentuale.greaterThan=" + SMALLER_PERCENTUALE);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultEtichettaSostanzeAttiveShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the etichettaSostanzeAttiveList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaSostanzeAttiveShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultEtichettaSostanzeAttiveShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the etichettaSostanzeAttiveList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaSostanzeAttiveShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where tipoImport is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("tipoImport.specified=true");

        // Get all the etichettaSostanzeAttiveList where tipoImport is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where operatore equals to DEFAULT_OPERATORE
        defaultEtichettaSostanzeAttiveShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the etichettaSostanzeAttiveList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultEtichettaSostanzeAttiveShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the etichettaSostanzeAttiveList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaSostanzeAttiveShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where operatore is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("operatore.specified=true");

        // Get all the etichettaSostanzeAttiveList where operatore is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where ts equals to DEFAULT_TS
        defaultEtichettaSostanzeAttiveShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the etichettaSostanzeAttiveList where ts equals to UPDATED_TS
        defaultEtichettaSostanzeAttiveShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTsIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where ts in DEFAULT_TS or UPDATED_TS
        defaultEtichettaSostanzeAttiveShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the etichettaSostanzeAttiveList where ts equals to UPDATED_TS
        defaultEtichettaSostanzeAttiveShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where ts is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("ts.specified=true");

        // Get all the etichettaSostanzeAttiveList where ts is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.specified=true");

        // Get all the etichettaSostanzeAttiveList where dataInizVali is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaSostanzeAttiveList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.specified=true");

        // Get all the etichettaSostanzeAttiveList where dataFineVali is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaSostanzeAttiveList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultEtichettaSostanzeAttiveShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.specified=true");

        // Get all the etichettaSostanzeAttiveList where userIdCreator is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaSostanzeAttiveList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is not null
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.specified=true");

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is null
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaSostanzeAttiveList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultEtichettaSostanzeAttiveShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        etichettaSostanzeAttive.setEtichettaFito(etichettaFito);
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the etichettaSostanzeAttiveList where etichettaFito equals to etichettaFitoId
        defaultEtichettaSostanzeAttiveShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the etichettaSostanzeAttiveList where etichettaFito equals to etichettaFitoId + 1
        defaultEtichettaSostanzeAttiveShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaSostanzeAttivesByEtiSostAttToSostAttIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);
        SostanzeAttive etiSostAttToSostAtt = SostanzeAttiveResourceIT.createEntity(em);
        em.persist(etiSostAttToSostAtt);
        em.flush();
        etichettaSostanzeAttive.setEtiSostAttToSostAtt(etiSostAttToSostAtt);
        etichettaSostanzeAttiveRepository.saveAndFlush(etichettaSostanzeAttive);
        Long etiSostAttToSostAttId = etiSostAttToSostAtt.getId();

        // Get all the etichettaSostanzeAttiveList where etiSostAttToSostAtt equals to etiSostAttToSostAttId
        defaultEtichettaSostanzeAttiveShouldBeFound("etiSostAttToSostAttId.equals=" + etiSostAttToSostAttId);

        // Get all the etichettaSostanzeAttiveList where etiSostAttToSostAtt equals to etiSostAttToSostAttId + 1
        defaultEtichettaSostanzeAttiveShouldNotBeFound("etiSostAttToSostAttId.equals=" + (etiSostAttToSostAttId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtichettaSostanzeAttiveShouldBeFound(String filter) throws Exception {
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaSostanzeAttive.getId().intValue())))
            .andExpect(jsonPath("$.[*].grammiLitro").value(hasItem(DEFAULT_GRAMMI_LITRO.doubleValue())))
            .andExpect(jsonPath("$.[*].percentuale").value(hasItem(DEFAULT_PERCENTUALE.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtichettaSostanzeAttiveShouldNotBeFound(String filter) throws Exception {
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtichettaSostanzeAttive() throws Exception {
        // Get the etichettaSostanzeAttive
        restEtichettaSostanzeAttiveMockMvc.perform(get("/api/etichetta-sostanze-attives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtichettaSostanzeAttive() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveService.save(etichettaSostanzeAttive);

        int databaseSizeBeforeUpdate = etichettaSostanzeAttiveRepository.findAll().size();

        // Update the etichettaSostanzeAttive
        EtichettaSostanzeAttive updatedEtichettaSostanzeAttive = etichettaSostanzeAttiveRepository.findById(etichettaSostanzeAttive.getId()).get();
        // Disconnect from session so that the updates on updatedEtichettaSostanzeAttive are not directly saved in db
        em.detach(updatedEtichettaSostanzeAttive);
        updatedEtichettaSostanzeAttive
            .grammiLitro(UPDATED_GRAMMI_LITRO)
            .percentuale(UPDATED_PERCENTUALE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restEtichettaSostanzeAttiveMockMvc.perform(put("/api/etichetta-sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtichettaSostanzeAttive)))
            .andExpect(status().isOk());

        // Validate the EtichettaSostanzeAttive in the database
        List<EtichettaSostanzeAttive> etichettaSostanzeAttiveList = etichettaSostanzeAttiveRepository.findAll();
        assertThat(etichettaSostanzeAttiveList).hasSize(databaseSizeBeforeUpdate);
        EtichettaSostanzeAttive testEtichettaSostanzeAttive = etichettaSostanzeAttiveList.get(etichettaSostanzeAttiveList.size() - 1);
        assertThat(testEtichettaSostanzeAttive.getGrammiLitro()).isEqualTo(UPDATED_GRAMMI_LITRO);
        assertThat(testEtichettaSostanzeAttive.getPercentuale()).isEqualTo(UPDATED_PERCENTUALE);
        assertThat(testEtichettaSostanzeAttive.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testEtichettaSostanzeAttive.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testEtichettaSostanzeAttive.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testEtichettaSostanzeAttive.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testEtichettaSostanzeAttive.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testEtichettaSostanzeAttive.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testEtichettaSostanzeAttive.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testEtichettaSostanzeAttive.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtichettaSostanzeAttive() throws Exception {
        int databaseSizeBeforeUpdate = etichettaSostanzeAttiveRepository.findAll().size();

        // Create the EtichettaSostanzeAttive

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtichettaSostanzeAttiveMockMvc.perform(put("/api/etichetta-sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaSostanzeAttive)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaSostanzeAttive in the database
        List<EtichettaSostanzeAttive> etichettaSostanzeAttiveList = etichettaSostanzeAttiveRepository.findAll();
        assertThat(etichettaSostanzeAttiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtichettaSostanzeAttive() throws Exception {
        // Initialize the database
        etichettaSostanzeAttiveService.save(etichettaSostanzeAttive);

        int databaseSizeBeforeDelete = etichettaSostanzeAttiveRepository.findAll().size();

        // Delete the etichettaSostanzeAttive
        restEtichettaSostanzeAttiveMockMvc.perform(delete("/api/etichetta-sostanze-attives/{id}", etichettaSostanzeAttive.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtichettaSostanzeAttive> etichettaSostanzeAttiveList = etichettaSostanzeAttiveRepository.findAll();
        assertThat(etichettaSostanzeAttiveList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtichettaSostanzeAttive.class);
        EtichettaSostanzeAttive etichettaSostanzeAttive1 = new EtichettaSostanzeAttive();
        etichettaSostanzeAttive1.setId(1L);
        EtichettaSostanzeAttive etichettaSostanzeAttive2 = new EtichettaSostanzeAttive();
        etichettaSostanzeAttive2.setId(etichettaSostanzeAttive1.getId());
        assertThat(etichettaSostanzeAttive1).isEqualTo(etichettaSostanzeAttive2);
        etichettaSostanzeAttive2.setId(2L);
        assertThat(etichettaSostanzeAttive1).isNotEqualTo(etichettaSostanzeAttive2);
        etichettaSostanzeAttive1.setId(null);
        assertThat(etichettaSostanzeAttive1).isNotEqualTo(etichettaSostanzeAttive2);
    }
}
