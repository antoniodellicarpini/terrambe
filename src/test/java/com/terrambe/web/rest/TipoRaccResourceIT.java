package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoRacc;
import com.terrambe.domain.RegOpcRacc;
import com.terrambe.repository.TipoRaccRepository;
import com.terrambe.service.TipoRaccService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoRaccCriteria;
import com.terrambe.service.TipoRaccQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoRaccResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoRaccResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoRaccRepository tipoRaccRepository;

    @Autowired
    private TipoRaccService tipoRaccService;

    @Autowired
    private TipoRaccQueryService tipoRaccQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoRaccMockMvc;

    private TipoRacc tipoRacc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoRaccResource tipoRaccResource = new TipoRaccResource(tipoRaccService, tipoRaccQueryService);
        this.restTipoRaccMockMvc = MockMvcBuilders.standaloneSetup(tipoRaccResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoRacc createEntity(EntityManager em) {
        TipoRacc tipoRacc = new TipoRacc()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoRacc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoRacc createUpdatedEntity(EntityManager em) {
        TipoRacc tipoRacc = new TipoRacc()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoRacc;
    }

    @BeforeEach
    public void initTest() {
        tipoRacc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoRacc() throws Exception {
        int databaseSizeBeforeCreate = tipoRaccRepository.findAll().size();

        // Create the TipoRacc
        restTipoRaccMockMvc.perform(post("/api/tipo-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRacc)))
            .andExpect(status().isCreated());

        // Validate the TipoRacc in the database
        List<TipoRacc> tipoRaccList = tipoRaccRepository.findAll();
        assertThat(tipoRaccList).hasSize(databaseSizeBeforeCreate + 1);
        TipoRacc testTipoRacc = tipoRaccList.get(tipoRaccList.size() - 1);
        assertThat(testTipoRacc.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoRacc.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoRacc.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoRacc.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoRacc.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoRaccWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoRaccRepository.findAll().size();

        // Create the TipoRacc with an existing ID
        tipoRacc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoRaccMockMvc.perform(post("/api/tipo-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRacc)))
            .andExpect(status().isBadRequest());

        // Validate the TipoRacc in the database
        List<TipoRacc> tipoRaccList = tipoRaccRepository.findAll();
        assertThat(tipoRaccList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoRaccs() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoRacc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoRacc() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get the tipoRacc
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs/{id}", tipoRacc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoRacc.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoRaccShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoRaccList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoRaccShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoRaccShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoRaccList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoRaccShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where descrizione is not null
        defaultTipoRaccShouldBeFound("descrizione.specified=true");

        // Get all the tipoRaccList where descrizione is null
        defaultTipoRaccShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali is not null
        defaultTipoRaccShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoRaccList where dataInizVali is null
        defaultTipoRaccShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoRaccShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoRaccList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoRaccShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali is not null
        defaultTipoRaccShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoRaccList where dataFineVali is null
        defaultTipoRaccShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoRaccShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoRaccList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoRaccShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator is not null
        defaultTipoRaccShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoRaccList where userIdCreator is null
        defaultTipoRaccShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoRaccShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRaccList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoRaccShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod is not null
        defaultTipoRaccShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoRaccList where userIdLastMod is null
        defaultTipoRaccShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRaccsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);

        // Get all the tipoRaccList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoRaccShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRaccList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoRaccShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoRaccsByTipoToOpcRaccIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRaccRepository.saveAndFlush(tipoRacc);
        RegOpcRacc tipoToOpcRacc = RegOpcRaccResourceIT.createEntity(em);
        em.persist(tipoToOpcRacc);
        em.flush();
        tipoRacc.addTipoToOpcRacc(tipoToOpcRacc);
        tipoRaccRepository.saveAndFlush(tipoRacc);
        Long tipoToOpcRaccId = tipoToOpcRacc.getId();

        // Get all the tipoRaccList where tipoToOpcRacc equals to tipoToOpcRaccId
        defaultTipoRaccShouldBeFound("tipoToOpcRaccId.equals=" + tipoToOpcRaccId);

        // Get all the tipoRaccList where tipoToOpcRacc equals to tipoToOpcRaccId + 1
        defaultTipoRaccShouldNotBeFound("tipoToOpcRaccId.equals=" + (tipoToOpcRaccId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoRaccShouldBeFound(String filter) throws Exception {
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoRacc.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoRaccShouldNotBeFound(String filter) throws Exception {
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoRacc() throws Exception {
        // Get the tipoRacc
        restTipoRaccMockMvc.perform(get("/api/tipo-raccs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoRacc() throws Exception {
        // Initialize the database
        tipoRaccService.save(tipoRacc);

        int databaseSizeBeforeUpdate = tipoRaccRepository.findAll().size();

        // Update the tipoRacc
        TipoRacc updatedTipoRacc = tipoRaccRepository.findById(tipoRacc.getId()).get();
        // Disconnect from session so that the updates on updatedTipoRacc are not directly saved in db
        em.detach(updatedTipoRacc);
        updatedTipoRacc
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoRaccMockMvc.perform(put("/api/tipo-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoRacc)))
            .andExpect(status().isOk());

        // Validate the TipoRacc in the database
        List<TipoRacc> tipoRaccList = tipoRaccRepository.findAll();
        assertThat(tipoRaccList).hasSize(databaseSizeBeforeUpdate);
        TipoRacc testTipoRacc = tipoRaccList.get(tipoRaccList.size() - 1);
        assertThat(testTipoRacc.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoRacc.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoRacc.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoRacc.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoRacc.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoRacc() throws Exception {
        int databaseSizeBeforeUpdate = tipoRaccRepository.findAll().size();

        // Create the TipoRacc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoRaccMockMvc.perform(put("/api/tipo-raccs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRacc)))
            .andExpect(status().isBadRequest());

        // Validate the TipoRacc in the database
        List<TipoRacc> tipoRaccList = tipoRaccRepository.findAll();
        assertThat(tipoRaccList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoRacc() throws Exception {
        // Initialize the database
        tipoRaccService.save(tipoRacc);

        int databaseSizeBeforeDelete = tipoRaccRepository.findAll().size();

        // Delete the tipoRacc
        restTipoRaccMockMvc.perform(delete("/api/tipo-raccs/{id}", tipoRacc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoRacc> tipoRaccList = tipoRaccRepository.findAll();
        assertThat(tipoRaccList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoRacc.class);
        TipoRacc tipoRacc1 = new TipoRacc();
        tipoRacc1.setId(1L);
        TipoRacc tipoRacc2 = new TipoRacc();
        tipoRacc2.setId(tipoRacc1.getId());
        assertThat(tipoRacc1).isEqualTo(tipoRacc2);
        tipoRacc2.setId(2L);
        assertThat(tipoRacc1).isNotEqualTo(tipoRacc2);
        tipoRacc1.setId(null);
        assertThat(tipoRacc1).isNotEqualTo(tipoRacc2);
    }
}
