package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndNascOpeAna;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.IndNascOpeAnaRepository;
import com.terrambe.service.IndNascOpeAnaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndNascOpeAnaCriteria;
import com.terrambe.service.IndNascOpeAnaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndNascOpeAnaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndNascOpeAnaResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private IndNascOpeAnaRepository indNascOpeAnaRepository;

    @Autowired
    private IndNascOpeAnaService indNascOpeAnaService;

    @Autowired
    private IndNascOpeAnaQueryService indNascOpeAnaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndNascOpeAnaMockMvc;

    private IndNascOpeAna indNascOpeAna;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndNascOpeAnaResource indNascOpeAnaResource = new IndNascOpeAnaResource(indNascOpeAnaService, indNascOpeAnaQueryService);
        this.restIndNascOpeAnaMockMvc = MockMvcBuilders.standaloneSetup(indNascOpeAnaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascOpeAna createEntity(EntityManager em) {
        IndNascOpeAna indNascOpeAna = new IndNascOpeAna()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return indNascOpeAna;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascOpeAna createUpdatedEntity(EntityManager em) {
        IndNascOpeAna indNascOpeAna = new IndNascOpeAna()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return indNascOpeAna;
    }

    @BeforeEach
    public void initTest() {
        indNascOpeAna = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndNascOpeAna() throws Exception {
        int databaseSizeBeforeCreate = indNascOpeAnaRepository.findAll().size();

        // Create the IndNascOpeAna
        restIndNascOpeAnaMockMvc.perform(post("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascOpeAna)))
            .andExpect(status().isCreated());

        // Validate the IndNascOpeAna in the database
        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeCreate + 1);
        IndNascOpeAna testIndNascOpeAna = indNascOpeAnaList.get(indNascOpeAnaList.size() - 1);
        assertThat(testIndNascOpeAna.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndNascOpeAna.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndNascOpeAna.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndNascOpeAna.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndNascOpeAna.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndNascOpeAna.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndNascOpeAna.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndNascOpeAna.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndNascOpeAna.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndNascOpeAna.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createIndNascOpeAnaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indNascOpeAnaRepository.findAll().size();

        // Create the IndNascOpeAna with an existing ID
        indNascOpeAna.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndNascOpeAnaMockMvc.perform(post("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascOpeAna in the database
        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascOpeAnaRepository.findAll().size();
        // set the field null
        indNascOpeAna.setIndirizzo(null);

        // Create the IndNascOpeAna, which fails.

        restIndNascOpeAnaMockMvc.perform(post("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascOpeAna)))
            .andExpect(status().isBadRequest());

        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascOpeAnaRepository.findAll().size();
        // set the field null
        indNascOpeAna.setNazione(null);

        // Create the IndNascOpeAna, which fails.

        restIndNascOpeAnaMockMvc.perform(post("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascOpeAna)))
            .andExpect(status().isBadRequest());

        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnas() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getIndNascOpeAna() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get the indNascOpeAna
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas/{id}", indNascOpeAna.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indNascOpeAna.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndNascOpeAnaShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indNascOpeAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascOpeAnaShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndNascOpeAnaShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indNascOpeAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascOpeAnaShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where indirizzo is not null
        defaultIndNascOpeAnaShouldBeFound("indirizzo.specified=true");

        // Get all the indNascOpeAnaList where indirizzo is null
        defaultIndNascOpeAnaShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where cap equals to DEFAULT_CAP
        defaultIndNascOpeAnaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indNascOpeAnaList where cap equals to UPDATED_CAP
        defaultIndNascOpeAnaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndNascOpeAnaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indNascOpeAnaList where cap equals to UPDATED_CAP
        defaultIndNascOpeAnaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where cap is not null
        defaultIndNascOpeAnaShouldBeFound("cap.specified=true");

        // Get all the indNascOpeAnaList where cap is null
        defaultIndNascOpeAnaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where nazione equals to DEFAULT_NAZIONE
        defaultIndNascOpeAnaShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indNascOpeAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndNascOpeAnaShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndNascOpeAnaShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indNascOpeAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndNascOpeAnaShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where nazione is not null
        defaultIndNascOpeAnaShouldBeFound("nazione.specified=true");

        // Get all the indNascOpeAnaList where nazione is null
        defaultIndNascOpeAnaShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where regione equals to DEFAULT_REGIONE
        defaultIndNascOpeAnaShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indNascOpeAnaList where regione equals to UPDATED_REGIONE
        defaultIndNascOpeAnaShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndNascOpeAnaShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indNascOpeAnaList where regione equals to UPDATED_REGIONE
        defaultIndNascOpeAnaShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where regione is not null
        defaultIndNascOpeAnaShouldBeFound("regione.specified=true");

        // Get all the indNascOpeAnaList where regione is null
        defaultIndNascOpeAnaShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where provincia equals to DEFAULT_PROVINCIA
        defaultIndNascOpeAnaShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indNascOpeAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascOpeAnaShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndNascOpeAnaShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indNascOpeAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascOpeAnaShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where provincia is not null
        defaultIndNascOpeAnaShouldBeFound("provincia.specified=true");

        // Get all the indNascOpeAnaList where provincia is null
        defaultIndNascOpeAnaShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where comune equals to DEFAULT_COMUNE
        defaultIndNascOpeAnaShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indNascOpeAnaList where comune equals to UPDATED_COMUNE
        defaultIndNascOpeAnaShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndNascOpeAnaShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indNascOpeAnaList where comune equals to UPDATED_COMUNE
        defaultIndNascOpeAnaShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where comune is not null
        defaultIndNascOpeAnaShouldBeFound("comune.specified=true");

        // Get all the indNascOpeAnaList where comune is null
        defaultIndNascOpeAnaShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali is not null
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.specified=true");

        // Get all the indNascOpeAnaList where dataInizVali is null
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascOpeAnaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndNascOpeAnaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali is not null
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.specified=true");

        // Get all the indNascOpeAnaList where dataFineVali is null
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascOpeAnaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndNascOpeAnaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator is not null
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.specified=true");

        // Get all the indNascOpeAnaList where userIdCreator is null
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascOpeAnaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndNascOpeAnaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod is not null
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.specified=true");

        // Get all the indNascOpeAnaList where userIdLastMod is null
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascOpeAnasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);

        // Get all the indNascOpeAnaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascOpeAnaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndNascOpeAnaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndNascOpeAnasByIndNascToOpeAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);
        OpeAnagrafica indNascToOpeAna = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(indNascToOpeAna);
        em.flush();
        indNascOpeAna.setIndNascToOpeAna(indNascToOpeAna);
        indNascToOpeAna.setOpeAnaToIndNasc(indNascOpeAna);
        indNascOpeAnaRepository.saveAndFlush(indNascOpeAna);
        Long indNascToOpeAnaId = indNascToOpeAna.getId();

        // Get all the indNascOpeAnaList where indNascToOpeAna equals to indNascToOpeAnaId
        defaultIndNascOpeAnaShouldBeFound("indNascToOpeAnaId.equals=" + indNascToOpeAnaId);

        // Get all the indNascOpeAnaList where indNascToOpeAna equals to indNascToOpeAnaId + 1
        defaultIndNascOpeAnaShouldNotBeFound("indNascToOpeAnaId.equals=" + (indNascToOpeAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndNascOpeAnaShouldBeFound(String filter) throws Exception {
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndNascOpeAnaShouldNotBeFound(String filter) throws Exception {
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndNascOpeAna() throws Exception {
        // Get the indNascOpeAna
        restIndNascOpeAnaMockMvc.perform(get("/api/ind-nasc-ope-anas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndNascOpeAna() throws Exception {
        // Initialize the database
        indNascOpeAnaService.save(indNascOpeAna);

        int databaseSizeBeforeUpdate = indNascOpeAnaRepository.findAll().size();

        // Update the indNascOpeAna
        IndNascOpeAna updatedIndNascOpeAna = indNascOpeAnaRepository.findById(indNascOpeAna.getId()).get();
        // Disconnect from session so that the updates on updatedIndNascOpeAna are not directly saved in db
        em.detach(updatedIndNascOpeAna);
        updatedIndNascOpeAna
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restIndNascOpeAnaMockMvc.perform(put("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndNascOpeAna)))
            .andExpect(status().isOk());

        // Validate the IndNascOpeAna in the database
        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeUpdate);
        IndNascOpeAna testIndNascOpeAna = indNascOpeAnaList.get(indNascOpeAnaList.size() - 1);
        assertThat(testIndNascOpeAna.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndNascOpeAna.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndNascOpeAna.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndNascOpeAna.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndNascOpeAna.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndNascOpeAna.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndNascOpeAna.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndNascOpeAna.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndNascOpeAna.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndNascOpeAna.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingIndNascOpeAna() throws Exception {
        int databaseSizeBeforeUpdate = indNascOpeAnaRepository.findAll().size();

        // Create the IndNascOpeAna

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndNascOpeAnaMockMvc.perform(put("/api/ind-nasc-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascOpeAna in the database
        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndNascOpeAna() throws Exception {
        // Initialize the database
        indNascOpeAnaService.save(indNascOpeAna);

        int databaseSizeBeforeDelete = indNascOpeAnaRepository.findAll().size();

        // Delete the indNascOpeAna
        restIndNascOpeAnaMockMvc.perform(delete("/api/ind-nasc-ope-anas/{id}", indNascOpeAna.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndNascOpeAna> indNascOpeAnaList = indNascOpeAnaRepository.findAll();
        assertThat(indNascOpeAnaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndNascOpeAna.class);
        IndNascOpeAna indNascOpeAna1 = new IndNascOpeAna();
        indNascOpeAna1.setId(1L);
        IndNascOpeAna indNascOpeAna2 = new IndNascOpeAna();
        indNascOpeAna2.setId(indNascOpeAna1.getId());
        assertThat(indNascOpeAna1).isEqualTo(indNascOpeAna2);
        indNascOpeAna2.setId(2L);
        assertThat(indNascOpeAna1).isNotEqualTo(indNascOpeAna2);
        indNascOpeAna1.setId(null);
        assertThat(indNascOpeAna1).isNotEqualTo(indNascOpeAna2);
    }
}
