package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.Fornitori;
import com.terrambe.domain.TipoRegMag;
import com.terrambe.repository.RegCaricoFitoFarmacoRepository;
import com.terrambe.service.RegCaricoFitoFarmacoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegCaricoFitoFarmacoCriteria;
import com.terrambe.service.RegCaricoFitoFarmacoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegCaricoFitoFarmacoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegCaricoFitoFarmacoResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Double DEFAULT_QUANTITA = 1D;
    private static final Double UPDATED_QUANTITA = 2D;
    private static final Double SMALLER_QUANTITA = 1D - 1D;

    private static final Float DEFAULT_PREZZO_UNITARIO = 1F;
    private static final Float UPDATED_PREZZO_UNITARIO = 2F;
    private static final Float SMALLER_PREZZO_UNITARIO = 1F - 1F;

    private static final LocalDate DEFAULT_DATA_CARICO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_CARICO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_CARICO = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_DDT = "AAAAAAAAAA";
    private static final String UPDATED_DDT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_SCADENZA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_SCADENZA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_SCADENZA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegCaricoFitoFarmacoRepository regCaricoFitoFarmacoRepository;

    @Autowired
    private RegCaricoFitoFarmacoService regCaricoFitoFarmacoService;

    @Autowired
    private RegCaricoFitoFarmacoQueryService regCaricoFitoFarmacoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegCaricoFitoFarmacoMockMvc;

    private RegCaricoFitoFarmaco regCaricoFitoFarmaco;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegCaricoFitoFarmacoResource regCaricoFitoFarmacoResource = new RegCaricoFitoFarmacoResource(regCaricoFitoFarmacoService, regCaricoFitoFarmacoQueryService);
        this.restRegCaricoFitoFarmacoMockMvc = MockMvcBuilders.standaloneSetup(regCaricoFitoFarmacoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoFitoFarmaco createEntity(EntityManager em) {
        RegCaricoFitoFarmaco regCaricoFitoFarmaco = new RegCaricoFitoFarmaco()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .quantita(DEFAULT_QUANTITA)
            .prezzoUnitario(DEFAULT_PREZZO_UNITARIO)
            .dataCarico(DEFAULT_DATA_CARICO)
            .ddt(DEFAULT_DDT)
            .dataScadenza(DEFAULT_DATA_SCADENZA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regCaricoFitoFarmaco;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoFitoFarmaco createUpdatedEntity(EntityManager em) {
        RegCaricoFitoFarmaco regCaricoFitoFarmaco = new RegCaricoFitoFarmaco()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regCaricoFitoFarmaco;
    }

    @BeforeEach
    public void initTest() {
        regCaricoFitoFarmaco = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegCaricoFitoFarmaco() throws Exception {
        int databaseSizeBeforeCreate = regCaricoFitoFarmacoRepository.findAll().size();

        // Create the RegCaricoFitoFarmaco
        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isCreated());

        // Validate the RegCaricoFitoFarmaco in the database
        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeCreate + 1);
        RegCaricoFitoFarmaco testRegCaricoFitoFarmaco = regCaricoFitoFarmacoList.get(regCaricoFitoFarmacoList.size() - 1);
        assertThat(testRegCaricoFitoFarmaco.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegCaricoFitoFarmaco.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegCaricoFitoFarmaco.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegCaricoFitoFarmaco.getPrezzoUnitario()).isEqualTo(DEFAULT_PREZZO_UNITARIO);
        assertThat(testRegCaricoFitoFarmaco.getDataCarico()).isEqualTo(DEFAULT_DATA_CARICO);
        assertThat(testRegCaricoFitoFarmaco.getDdt()).isEqualTo(DEFAULT_DDT);
        assertThat(testRegCaricoFitoFarmaco.getDataScadenza()).isEqualTo(DEFAULT_DATA_SCADENZA);
        assertThat(testRegCaricoFitoFarmaco.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegCaricoFitoFarmaco.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegCaricoFitoFarmaco.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegCaricoFitoFarmaco.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegCaricoFitoFarmaco.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegCaricoFitoFarmacoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regCaricoFitoFarmacoRepository.findAll().size();

        // Create the RegCaricoFitoFarmaco with an existing ID
        regCaricoFitoFarmaco.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoFitoFarmaco in the database
        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setIdAzienda(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQuantitaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setQuantita(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrezzoUnitarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setPrezzoUnitario(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataCaricoIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setDataCarico(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDdtIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setDdt(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataScadenzaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFitoFarmacoRepository.findAll().size();
        // set the field null
        regCaricoFitoFarmaco.setDataScadenza(null);

        // Create the RegCaricoFitoFarmaco, which fails.

        restRegCaricoFitoFarmacoMockMvc.perform(post("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacos() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoFitoFarmaco.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT.toString())))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegCaricoFitoFarmaco() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get the regCaricoFitoFarmaco
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos/{id}", regCaricoFitoFarmaco.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regCaricoFitoFarmaco.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA.doubleValue()))
            .andExpect(jsonPath("$.prezzoUnitario").value(DEFAULT_PREZZO_UNITARIO.doubleValue()))
            .andExpect(jsonPath("$.dataCarico").value(DEFAULT_DATA_CARICO.toString()))
            .andExpect(jsonPath("$.ddt").value(DEFAULT_DDT.toString()))
            .andExpect(jsonPath("$.dataScadenza").value(DEFAULT_DATA_SCADENZA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.specified=true");

        // Get all the regCaricoFitoFarmacoList where idAzienda is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFitoFarmacoList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegCaricoFitoFarmacoShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.specified=true");

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFitoFarmacoList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegCaricoFitoFarmacoShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita equals to DEFAULT_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.specified=true");

        // Get all the regCaricoFitoFarmacoList where quantita is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita is less than DEFAULT_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita is less than UPDATED_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where quantita is greater than DEFAULT_QUANTITA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFitoFarmacoList where quantita is greater than SMALLER_QUANTITA
        defaultRegCaricoFitoFarmacoShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario equals to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.equals=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.equals=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario in DEFAULT_PREZZO_UNITARIO or UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.in=" + DEFAULT_PREZZO_UNITARIO + "," + UPDATED_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.in=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.specified=true");

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is greater than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.greaterThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is greater than or equal to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.greaterThanOrEqual=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is less than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.lessThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is less than or equal to SMALLER_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.lessThanOrEqual=" + SMALLER_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is less than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.lessThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is less than UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.lessThan=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByPrezzoUnitarioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is greater than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("prezzoUnitario.greaterThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFitoFarmacoList where prezzoUnitario is greater than SMALLER_PREZZO_UNITARIO
        defaultRegCaricoFitoFarmacoShouldBeFound("prezzoUnitario.greaterThan=" + SMALLER_PREZZO_UNITARIO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico equals to DEFAULT_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.equals=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.equals=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico in DEFAULT_DATA_CARICO or UPDATED_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.in=" + DEFAULT_DATA_CARICO + "," + UPDATED_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.in=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.specified=true");

        // Get all the regCaricoFitoFarmacoList where dataCarico is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico is greater than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.greaterThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico is greater than or equal to UPDATED_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.greaterThanOrEqual=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico is less than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.lessThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico is less than or equal to SMALLER_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.lessThanOrEqual=" + SMALLER_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico is less than DEFAULT_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.lessThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico is less than UPDATED_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.lessThan=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataCaricoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataCarico is greater than DEFAULT_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataCarico.greaterThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFitoFarmacoList where dataCarico is greater than SMALLER_DATA_CARICO
        defaultRegCaricoFitoFarmacoShouldBeFound("dataCarico.greaterThan=" + SMALLER_DATA_CARICO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDdtIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where ddt equals to DEFAULT_DDT
        defaultRegCaricoFitoFarmacoShouldBeFound("ddt.equals=" + DEFAULT_DDT);

        // Get all the regCaricoFitoFarmacoList where ddt equals to UPDATED_DDT
        defaultRegCaricoFitoFarmacoShouldNotBeFound("ddt.equals=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDdtIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where ddt in DEFAULT_DDT or UPDATED_DDT
        defaultRegCaricoFitoFarmacoShouldBeFound("ddt.in=" + DEFAULT_DDT + "," + UPDATED_DDT);

        // Get all the regCaricoFitoFarmacoList where ddt equals to UPDATED_DDT
        defaultRegCaricoFitoFarmacoShouldNotBeFound("ddt.in=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDdtIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where ddt is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("ddt.specified=true");

        // Get all the regCaricoFitoFarmacoList where ddt is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("ddt.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza equals to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.equals=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.equals=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza in DEFAULT_DATA_SCADENZA or UPDATED_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.in=" + DEFAULT_DATA_SCADENZA + "," + UPDATED_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.in=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.specified=true");

        // Get all the regCaricoFitoFarmacoList where dataScadenza is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is greater than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.greaterThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is greater than or equal to UPDATED_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.greaterThanOrEqual=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is less than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.lessThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is less than or equal to SMALLER_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.lessThanOrEqual=" + SMALLER_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is less than DEFAULT_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.lessThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is less than UPDATED_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.lessThan=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataScadenzaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is greater than DEFAULT_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataScadenza.greaterThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFitoFarmacoList where dataScadenza is greater than SMALLER_DATA_SCADENZA
        defaultRegCaricoFitoFarmacoShouldBeFound("dataScadenza.greaterThan=" + SMALLER_DATA_SCADENZA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.specified=true");

        // Get all the regCaricoFitoFarmacoList where dataInizVali is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFitoFarmacoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.specified=true");

        // Get all the regCaricoFitoFarmacoList where dataFineVali is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFitoFarmacoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegCaricoFitoFarmacoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.specified=true");

        // Get all the regCaricoFitoFarmacoList where userIdCreator is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFitoFarmacoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is not null
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.specified=true");

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is null
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFitoFarmacoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoFitoFarmacoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByMagUbicazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        MagUbicazione magUbicazione = MagUbicazioneResourceIT.createEntity(em);
        em.persist(magUbicazione);
        em.flush();
        regCaricoFitoFarmaco.setMagUbicazione(magUbicazione);
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        Long magUbicazioneId = magUbicazione.getId();

        // Get all the regCaricoFitoFarmacoList where magUbicazione equals to magUbicazioneId
        defaultRegCaricoFitoFarmacoShouldBeFound("magUbicazioneId.equals=" + magUbicazioneId);

        // Get all the regCaricoFitoFarmacoList where magUbicazione equals to magUbicazioneId + 1
        defaultRegCaricoFitoFarmacoShouldNotBeFound("magUbicazioneId.equals=" + (magUbicazioneId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByEtichettafitoIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        EtichettaFito etichettafito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettafito);
        em.flush();
        regCaricoFitoFarmaco.setEtichettafito(etichettafito);
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        Long etichettafitoId = etichettafito.getId();

        // Get all the regCaricoFitoFarmacoList where etichettafito equals to etichettafitoId
        defaultRegCaricoFitoFarmacoShouldBeFound("etichettafitoId.equals=" + etichettafitoId);

        // Get all the regCaricoFitoFarmacoList where etichettafito equals to etichettafitoId + 1
        defaultRegCaricoFitoFarmacoShouldNotBeFound("etichettafitoId.equals=" + (etichettafitoId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByCaricoFitoToFornitoriIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        Fornitori caricoFitoToFornitori = FornitoriResourceIT.createEntity(em);
        em.persist(caricoFitoToFornitori);
        em.flush();
        regCaricoFitoFarmaco.setCaricoFitoToFornitori(caricoFitoToFornitori);
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        Long caricoFitoToFornitoriId = caricoFitoToFornitori.getId();

        // Get all the regCaricoFitoFarmacoList where caricoFitoToFornitori equals to caricoFitoToFornitoriId
        defaultRegCaricoFitoFarmacoShouldBeFound("caricoFitoToFornitoriId.equals=" + caricoFitoToFornitoriId);

        // Get all the regCaricoFitoFarmacoList where caricoFitoToFornitori equals to caricoFitoToFornitoriId + 1
        defaultRegCaricoFitoFarmacoShouldNotBeFound("caricoFitoToFornitoriId.equals=" + (caricoFitoToFornitoriId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFitoFarmacosByRegFitoToTipoRegIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        TipoRegMag regFitoToTipoReg = TipoRegMagResourceIT.createEntity(em);
        em.persist(regFitoToTipoReg);
        em.flush();
        regCaricoFitoFarmaco.setRegFitoToTipoReg(regFitoToTipoReg);
        regCaricoFitoFarmacoRepository.saveAndFlush(regCaricoFitoFarmaco);
        Long regFitoToTipoRegId = regFitoToTipoReg.getId();

        // Get all the regCaricoFitoFarmacoList where regFitoToTipoReg equals to regFitoToTipoRegId
        defaultRegCaricoFitoFarmacoShouldBeFound("regFitoToTipoRegId.equals=" + regFitoToTipoRegId);

        // Get all the regCaricoFitoFarmacoList where regFitoToTipoReg equals to regFitoToTipoRegId + 1
        defaultRegCaricoFitoFarmacoShouldNotBeFound("regFitoToTipoRegId.equals=" + (regFitoToTipoRegId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegCaricoFitoFarmacoShouldBeFound(String filter) throws Exception {
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoFitoFarmaco.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT)))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegCaricoFitoFarmacoShouldNotBeFound(String filter) throws Exception {
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegCaricoFitoFarmaco() throws Exception {
        // Get the regCaricoFitoFarmaco
        restRegCaricoFitoFarmacoMockMvc.perform(get("/api/reg-carico-fito-farmacos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegCaricoFitoFarmaco() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoService.save(regCaricoFitoFarmaco);

        int databaseSizeBeforeUpdate = regCaricoFitoFarmacoRepository.findAll().size();

        // Update the regCaricoFitoFarmaco
        RegCaricoFitoFarmaco updatedRegCaricoFitoFarmaco = regCaricoFitoFarmacoRepository.findById(regCaricoFitoFarmaco.getId()).get();
        // Disconnect from session so that the updates on updatedRegCaricoFitoFarmaco are not directly saved in db
        em.detach(updatedRegCaricoFitoFarmaco);
        updatedRegCaricoFitoFarmaco
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegCaricoFitoFarmacoMockMvc.perform(put("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegCaricoFitoFarmaco)))
            .andExpect(status().isOk());

        // Validate the RegCaricoFitoFarmaco in the database
        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeUpdate);
        RegCaricoFitoFarmaco testRegCaricoFitoFarmaco = regCaricoFitoFarmacoList.get(regCaricoFitoFarmacoList.size() - 1);
        assertThat(testRegCaricoFitoFarmaco.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegCaricoFitoFarmaco.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegCaricoFitoFarmaco.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegCaricoFitoFarmaco.getPrezzoUnitario()).isEqualTo(UPDATED_PREZZO_UNITARIO);
        assertThat(testRegCaricoFitoFarmaco.getDataCarico()).isEqualTo(UPDATED_DATA_CARICO);
        assertThat(testRegCaricoFitoFarmaco.getDdt()).isEqualTo(UPDATED_DDT);
        assertThat(testRegCaricoFitoFarmaco.getDataScadenza()).isEqualTo(UPDATED_DATA_SCADENZA);
        assertThat(testRegCaricoFitoFarmaco.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegCaricoFitoFarmaco.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegCaricoFitoFarmaco.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegCaricoFitoFarmaco.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegCaricoFitoFarmaco.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegCaricoFitoFarmaco() throws Exception {
        int databaseSizeBeforeUpdate = regCaricoFitoFarmacoRepository.findAll().size();

        // Create the RegCaricoFitoFarmaco

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegCaricoFitoFarmacoMockMvc.perform(put("/api/reg-carico-fito-farmacos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFitoFarmaco)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoFitoFarmaco in the database
        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegCaricoFitoFarmaco() throws Exception {
        // Initialize the database
        regCaricoFitoFarmacoService.save(regCaricoFitoFarmaco);

        int databaseSizeBeforeDelete = regCaricoFitoFarmacoRepository.findAll().size();

        // Delete the regCaricoFitoFarmaco
        restRegCaricoFitoFarmacoMockMvc.perform(delete("/api/reg-carico-fito-farmacos/{id}", regCaricoFitoFarmaco.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegCaricoFitoFarmaco> regCaricoFitoFarmacoList = regCaricoFitoFarmacoRepository.findAll();
        assertThat(regCaricoFitoFarmacoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegCaricoFitoFarmaco.class);
        RegCaricoFitoFarmaco regCaricoFitoFarmaco1 = new RegCaricoFitoFarmaco();
        regCaricoFitoFarmaco1.setId(1L);
        RegCaricoFitoFarmaco regCaricoFitoFarmaco2 = new RegCaricoFitoFarmaco();
        regCaricoFitoFarmaco2.setId(regCaricoFitoFarmaco1.getId());
        assertThat(regCaricoFitoFarmaco1).isEqualTo(regCaricoFitoFarmaco2);
        regCaricoFitoFarmaco2.setId(2L);
        assertThat(regCaricoFitoFarmaco1).isNotEqualTo(regCaricoFitoFarmaco2);
        regCaricoFitoFarmaco1.setId(null);
        assertThat(regCaricoFitoFarmaco1).isNotEqualTo(regCaricoFitoFarmaco2);
    }
}
