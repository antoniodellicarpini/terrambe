package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndAzienda;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.IndAziendaRepository;
import com.terrambe.service.IndAziendaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndAziendaCriteria;
import com.terrambe.service.IndAziendaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndAziendaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndAziendaResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndAziendaRepository indAziendaRepository;

    @Autowired
    private IndAziendaService indAziendaService;

    @Autowired
    private IndAziendaQueryService indAziendaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndAziendaMockMvc;

    private IndAzienda indAzienda;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndAziendaResource indAziendaResource = new IndAziendaResource(indAziendaService, indAziendaQueryService);
        this.restIndAziendaMockMvc = MockMvcBuilders.standaloneSetup(indAziendaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAzienda createEntity(EntityManager em) {
        IndAzienda indAzienda = new IndAzienda()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indAzienda;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAzienda createUpdatedEntity(EntityManager em) {
        IndAzienda indAzienda = new IndAzienda()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indAzienda;
    }

    @BeforeEach
    public void initTest() {
        indAzienda = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndAzienda() throws Exception {
        int databaseSizeBeforeCreate = indAziendaRepository.findAll().size();

        // Create the IndAzienda
        restIndAziendaMockMvc.perform(post("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAzienda)))
            .andExpect(status().isCreated());

        // Validate the IndAzienda in the database
        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeCreate + 1);
        IndAzienda testIndAzienda = indAziendaList.get(indAziendaList.size() - 1);
        assertThat(testIndAzienda.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndAzienda.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndAzienda.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndAzienda.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndAzienda.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndAzienda.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndAzienda.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndAzienda.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndAzienda.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndAzienda.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndAzienda.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndAzienda.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndAzienda.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndAzienda.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndAziendaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indAziendaRepository.findAll().size();

        // Create the IndAzienda with an existing ID
        indAzienda.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndAziendaMockMvc.perform(post("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAzienda)))
            .andExpect(status().isBadRequest());

        // Validate the IndAzienda in the database
        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziendaRepository.findAll().size();
        // set the field null
        indAzienda.setIndirizzo(null);

        // Create the IndAzienda, which fails.

        restIndAziendaMockMvc.perform(post("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAzienda)))
            .andExpect(status().isBadRequest());

        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziendaRepository.findAll().size();
        // set the field null
        indAzienda.setNazione(null);

        // Create the IndAzienda, which fails.

        restIndAziendaMockMvc.perform(post("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAzienda)))
            .andExpect(status().isBadRequest());

        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndAziendas() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAzienda.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndAzienda() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get the indAzienda
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas/{id}", indAzienda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indAzienda.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndAziendasByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndAziendaShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indAziendaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziendaShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndAziendaShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indAziendaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziendaShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where indirizzo is not null
        defaultIndAziendaShouldBeFound("indirizzo.specified=true");

        // Get all the indAziendaList where indirizzo is null
        defaultIndAziendaShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where cap equals to DEFAULT_CAP
        defaultIndAziendaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indAziendaList where cap equals to UPDATED_CAP
        defaultIndAziendaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndAziendaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indAziendaList where cap equals to UPDATED_CAP
        defaultIndAziendaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where cap is not null
        defaultIndAziendaShouldBeFound("cap.specified=true");

        // Get all the indAziendaList where cap is null
        defaultIndAziendaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where nazione equals to DEFAULT_NAZIONE
        defaultIndAziendaShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indAziendaList where nazione equals to UPDATED_NAZIONE
        defaultIndAziendaShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndAziendaShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indAziendaList where nazione equals to UPDATED_NAZIONE
        defaultIndAziendaShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where nazione is not null
        defaultIndAziendaShouldBeFound("nazione.specified=true");

        // Get all the indAziendaList where nazione is null
        defaultIndAziendaShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where regione equals to DEFAULT_REGIONE
        defaultIndAziendaShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indAziendaList where regione equals to UPDATED_REGIONE
        defaultIndAziendaShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndAziendaShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indAziendaList where regione equals to UPDATED_REGIONE
        defaultIndAziendaShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where regione is not null
        defaultIndAziendaShouldBeFound("regione.specified=true");

        // Get all the indAziendaList where regione is null
        defaultIndAziendaShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where provincia equals to DEFAULT_PROVINCIA
        defaultIndAziendaShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indAziendaList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziendaShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndAziendaShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indAziendaList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziendaShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where provincia is not null
        defaultIndAziendaShouldBeFound("provincia.specified=true");

        // Get all the indAziendaList where provincia is null
        defaultIndAziendaShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where comune equals to DEFAULT_COMUNE
        defaultIndAziendaShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indAziendaList where comune equals to UPDATED_COMUNE
        defaultIndAziendaShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndAziendaShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indAziendaList where comune equals to UPDATED_COMUNE
        defaultIndAziendaShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where comune is not null
        defaultIndAziendaShouldBeFound("comune.specified=true");

        // Get all the indAziendaList where comune is null
        defaultIndAziendaShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndAziendaShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indAziendaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziendaShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndAziendaShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indAziendaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziendaShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where numeroCiv is not null
        defaultIndAziendaShouldBeFound("numeroCiv.specified=true");

        // Get all the indAziendaList where numeroCiv is null
        defaultIndAziendaShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude equals to DEFAULT_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indAziendaList where latitude equals to UPDATED_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indAziendaList where latitude equals to UPDATED_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude is not null
        defaultIndAziendaShouldBeFound("latitude.specified=true");

        // Get all the indAziendaList where latitude is null
        defaultIndAziendaShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziendaList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziendaList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude is less than DEFAULT_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indAziendaList where latitude is less than UPDATED_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where latitude is greater than DEFAULT_LATITUDE
        defaultIndAziendaShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indAziendaList where latitude is greater than SMALLER_LATITUDE
        defaultIndAziendaShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude equals to DEFAULT_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indAziendaList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude is not null
        defaultIndAziendaShouldBeFound("longitude.specified=true");

        // Get all the indAziendaList where longitude is null
        defaultIndAziendaShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude is less than DEFAULT_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaList where longitude is less than UPDATED_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndAziendaShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaList where longitude is greater than SMALLER_LONGITUDE
        defaultIndAziendaShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali is not null
        defaultIndAziendaShouldBeFound("dataInizVali.specified=true");

        // Get all the indAziendaList where dataInizVali is null
        defaultIndAziendaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndAziendaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali is not null
        defaultIndAziendaShouldBeFound("dataFineVali.specified=true");

        // Get all the indAziendaList where dataFineVali is null
        defaultIndAziendaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndAziendaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndAziendaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator is not null
        defaultIndAziendaShouldBeFound("userIdCreator.specified=true");

        // Get all the indAziendaList where userIdCreator is null
        defaultIndAziendaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndAziendaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndAziendaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod is not null
        defaultIndAziendaShouldBeFound("userIdLastMod.specified=true");

        // Get all the indAziendaList where userIdLastMod is null
        defaultIndAziendaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);

        // Get all the indAziendaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndAziendaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndAziendasByIndToAziIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaRepository.saveAndFlush(indAzienda);
        AziAnagrafica indToAzi = AziAnagraficaResourceIT.createEntity(em);
        em.persist(indToAzi);
        em.flush();
        indAzienda.setIndToAzi(indToAzi);
        indAziendaRepository.saveAndFlush(indAzienda);
        Long indToAziId = indToAzi.getId();

        // Get all the indAziendaList where indToAzi equals to indToAziId
        defaultIndAziendaShouldBeFound("indToAziId.equals=" + indToAziId);

        // Get all the indAziendaList where indToAzi equals to indToAziId + 1
        defaultIndAziendaShouldNotBeFound("indToAziId.equals=" + (indToAziId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndAziendaShouldBeFound(String filter) throws Exception {
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAzienda.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndAziendaShouldNotBeFound(String filter) throws Exception {
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndAzienda() throws Exception {
        // Get the indAzienda
        restIndAziendaMockMvc.perform(get("/api/ind-aziendas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndAzienda() throws Exception {
        // Initialize the database
        indAziendaService.save(indAzienda);

        int databaseSizeBeforeUpdate = indAziendaRepository.findAll().size();

        // Update the indAzienda
        IndAzienda updatedIndAzienda = indAziendaRepository.findById(indAzienda.getId()).get();
        // Disconnect from session so that the updates on updatedIndAzienda are not directly saved in db
        em.detach(updatedIndAzienda);
        updatedIndAzienda
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndAziendaMockMvc.perform(put("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndAzienda)))
            .andExpect(status().isOk());

        // Validate the IndAzienda in the database
        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeUpdate);
        IndAzienda testIndAzienda = indAziendaList.get(indAziendaList.size() - 1);
        assertThat(testIndAzienda.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndAzienda.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndAzienda.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndAzienda.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndAzienda.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndAzienda.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndAzienda.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndAzienda.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndAzienda.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndAzienda.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndAzienda.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndAzienda.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndAzienda.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndAzienda.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndAzienda() throws Exception {
        int databaseSizeBeforeUpdate = indAziendaRepository.findAll().size();

        // Create the IndAzienda

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndAziendaMockMvc.perform(put("/api/ind-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAzienda)))
            .andExpect(status().isBadRequest());

        // Validate the IndAzienda in the database
        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndAzienda() throws Exception {
        // Initialize the database
        indAziendaService.save(indAzienda);

        int databaseSizeBeforeDelete = indAziendaRepository.findAll().size();

        // Delete the indAzienda
        restIndAziendaMockMvc.perform(delete("/api/ind-aziendas/{id}", indAzienda.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndAzienda> indAziendaList = indAziendaRepository.findAll();
        assertThat(indAziendaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndAzienda.class);
        IndAzienda indAzienda1 = new IndAzienda();
        indAzienda1.setId(1L);
        IndAzienda indAzienda2 = new IndAzienda();
        indAzienda2.setId(indAzienda1.getId());
        assertThat(indAzienda1).isEqualTo(indAzienda2);
        indAzienda2.setId(2L);
        assertThat(indAzienda1).isNotEqualTo(indAzienda2);
        indAzienda1.setId(null);
        assertThat(indAzienda1).isNotEqualTo(indAzienda2);
    }
}
