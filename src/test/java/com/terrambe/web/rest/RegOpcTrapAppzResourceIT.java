package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcTrapAppz;
import com.terrambe.domain.RegOpcTrap;
import com.terrambe.repository.RegOpcTrapAppzRepository;
import com.terrambe.service.RegOpcTrapAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcTrapAppzCriteria;
import com.terrambe.service.RegOpcTrapAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcTrapAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcTrapAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Integer DEFAULT_QUANTITA = 1;
    private static final Integer UPDATED_QUANTITA = 2;
    private static final Integer SMALLER_QUANTITA = 1 - 1;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcTrapAppzRepository regOpcTrapAppzRepository;

    @Autowired
    private RegOpcTrapAppzService regOpcTrapAppzService;

    @Autowired
    private RegOpcTrapAppzQueryService regOpcTrapAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcTrapAppzMockMvc;

    private RegOpcTrapAppz regOpcTrapAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcTrapAppzResource regOpcTrapAppzResource = new RegOpcTrapAppzResource(regOpcTrapAppzService, regOpcTrapAppzQueryService);
        this.restRegOpcTrapAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcTrapAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrapAppz createEntity(EntityManager em) {
        RegOpcTrapAppz regOpcTrapAppz = new RegOpcTrapAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .quantita(DEFAULT_QUANTITA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcTrapAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrapAppz createUpdatedEntity(EntityManager em) {
        RegOpcTrapAppz regOpcTrapAppz = new RegOpcTrapAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcTrapAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcTrapAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcTrapAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapAppzRepository.findAll().size();

        // Create the RegOpcTrapAppz
        restRegOpcTrapAppzMockMvc.perform(post("/api/reg-opc-trap-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcTrapAppz in the database
        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcTrapAppz testRegOpcTrapAppz = regOpcTrapAppzList.get(regOpcTrapAppzList.size() - 1);
        assertThat(testRegOpcTrapAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcTrapAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcTrapAppz.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegOpcTrapAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcTrapAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcTrapAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcTrapAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcTrapAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcTrapAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapAppzRepository.findAll().size();

        // Create the RegOpcTrapAppz with an existing ID
        regOpcTrapAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcTrapAppzMockMvc.perform(post("/api/reg-opc-trap-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrapAppz in the database
        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcTrapAppzRepository.findAll().size();
        // set the field null
        regOpcTrapAppz.setIdAzienda(null);

        // Create the RegOpcTrapAppz, which fails.

        restRegOpcTrapAppzMockMvc.perform(post("/api/reg-opc-trap-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzs() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrapAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA)))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcTrapAppz() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get the regOpcTrapAppz
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs/{id}", regOpcTrapAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcTrapAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda is not null
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcTrapAppzList where idAzienda is null
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcTrapAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd is not null
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcTrapAppzList where idUnitaProd is null
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita equals to DEFAULT_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita equals to UPDATED_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita is not null
        defaultRegOpcTrapAppzShouldBeFound("quantita.specified=true");

        // Get all the regOpcTrapAppzList where quantita is null
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita is less than DEFAULT_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita is less than UPDATED_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where quantita is greater than DEFAULT_QUANTITA
        defaultRegOpcTrapAppzShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regOpcTrapAppzList where quantita is greater than SMALLER_QUANTITA
        defaultRegOpcTrapAppzShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento is not null
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcTrapAppzList where idAppezzamento is null
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcTrapAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcTrapAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali is not null
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcTrapAppzList where dataInizVali is null
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali is not null
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcTrapAppzList where dataFineVali is null
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator is not null
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcTrapAppzList where userIdCreator is null
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod is not null
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcTrapAppzList where userIdLastMod is null
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);

        // Get all the regOpcTrapAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapAppzsByAppzToOpcTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);
        RegOpcTrap appzToOpcTrap = RegOpcTrapResourceIT.createEntity(em);
        em.persist(appzToOpcTrap);
        em.flush();
        regOpcTrapAppz.setAppzToOpcTrap(appzToOpcTrap);
        regOpcTrapAppzRepository.saveAndFlush(regOpcTrapAppz);
        Long appzToOpcTrapId = appzToOpcTrap.getId();

        // Get all the regOpcTrapAppzList where appzToOpcTrap equals to appzToOpcTrapId
        defaultRegOpcTrapAppzShouldBeFound("appzToOpcTrapId.equals=" + appzToOpcTrapId);

        // Get all the regOpcTrapAppzList where appzToOpcTrap equals to appzToOpcTrapId + 1
        defaultRegOpcTrapAppzShouldNotBeFound("appzToOpcTrapId.equals=" + (appzToOpcTrapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcTrapAppzShouldBeFound(String filter) throws Exception {
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrapAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA)))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcTrapAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcTrapAppz() throws Exception {
        // Get the regOpcTrapAppz
        restRegOpcTrapAppzMockMvc.perform(get("/api/reg-opc-trap-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcTrapAppz() throws Exception {
        // Initialize the database
        regOpcTrapAppzService.save(regOpcTrapAppz);

        int databaseSizeBeforeUpdate = regOpcTrapAppzRepository.findAll().size();

        // Update the regOpcTrapAppz
        RegOpcTrapAppz updatedRegOpcTrapAppz = regOpcTrapAppzRepository.findById(regOpcTrapAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcTrapAppz are not directly saved in db
        em.detach(updatedRegOpcTrapAppz);
        updatedRegOpcTrapAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcTrapAppzMockMvc.perform(put("/api/reg-opc-trap-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcTrapAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcTrapAppz in the database
        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcTrapAppz testRegOpcTrapAppz = regOpcTrapAppzList.get(regOpcTrapAppzList.size() - 1);
        assertThat(testRegOpcTrapAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcTrapAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcTrapAppz.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegOpcTrapAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcTrapAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcTrapAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcTrapAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcTrapAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcTrapAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcTrapAppzRepository.findAll().size();

        // Create the RegOpcTrapAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcTrapAppzMockMvc.perform(put("/api/reg-opc-trap-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrapAppz in the database
        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcTrapAppz() throws Exception {
        // Initialize the database
        regOpcTrapAppzService.save(regOpcTrapAppz);

        int databaseSizeBeforeDelete = regOpcTrapAppzRepository.findAll().size();

        // Delete the regOpcTrapAppz
        restRegOpcTrapAppzMockMvc.perform(delete("/api/reg-opc-trap-appzs/{id}", regOpcTrapAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcTrapAppz> regOpcTrapAppzList = regOpcTrapAppzRepository.findAll();
        assertThat(regOpcTrapAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcTrapAppz.class);
        RegOpcTrapAppz regOpcTrapAppz1 = new RegOpcTrapAppz();
        regOpcTrapAppz1.setId(1L);
        RegOpcTrapAppz regOpcTrapAppz2 = new RegOpcTrapAppz();
        regOpcTrapAppz2.setId(regOpcTrapAppz1.getId());
        assertThat(regOpcTrapAppz1).isEqualTo(regOpcTrapAppz2);
        regOpcTrapAppz2.setId(2L);
        assertThat(regOpcTrapAppz1).isNotEqualTo(regOpcTrapAppz2);
        regOpcTrapAppz1.setId(null);
        assertThat(regOpcTrapAppz1).isNotEqualTo(regOpcTrapAppz2);
    }
}
