package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndForn;
import com.terrambe.domain.Fornitori;
import com.terrambe.repository.IndFornRepository;
import com.terrambe.service.IndFornService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndFornCriteria;
import com.terrambe.service.IndFornQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndFornResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndFornResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndFornRepository indFornRepository;

    @Autowired
    private IndFornService indFornService;

    @Autowired
    private IndFornQueryService indFornQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndFornMockMvc;

    private IndForn indForn;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndFornResource indFornResource = new IndFornResource(indFornService, indFornQueryService);
        this.restIndFornMockMvc = MockMvcBuilders.standaloneSetup(indFornResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndForn createEntity(EntityManager em) {
        IndForn indForn = new IndForn()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indForn;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndForn createUpdatedEntity(EntityManager em) {
        IndForn indForn = new IndForn()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indForn;
    }

    @BeforeEach
    public void initTest() {
        indForn = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndForn() throws Exception {
        int databaseSizeBeforeCreate = indFornRepository.findAll().size();

        // Create the IndForn
        restIndFornMockMvc.perform(post("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indForn)))
            .andExpect(status().isCreated());

        // Validate the IndForn in the database
        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeCreate + 1);
        IndForn testIndForn = indFornList.get(indFornList.size() - 1);
        assertThat(testIndForn.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndForn.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndForn.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndForn.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndForn.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndForn.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndForn.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndForn.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndForn.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndForn.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndForn.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndForn.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndForn.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndForn.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndFornWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indFornRepository.findAll().size();

        // Create the IndForn with an existing ID
        indForn.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndFornMockMvc.perform(post("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indForn)))
            .andExpect(status().isBadRequest());

        // Validate the IndForn in the database
        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indFornRepository.findAll().size();
        // set the field null
        indForn.setIndirizzo(null);

        // Create the IndForn, which fails.

        restIndFornMockMvc.perform(post("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indForn)))
            .andExpect(status().isBadRequest());

        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indFornRepository.findAll().size();
        // set the field null
        indForn.setNazione(null);

        // Create the IndForn, which fails.

        restIndFornMockMvc.perform(post("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indForn)))
            .andExpect(status().isBadRequest());

        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndForns() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList
        restIndFornMockMvc.perform(get("/api/ind-forns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndForn() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get the indForn
        restIndFornMockMvc.perform(get("/api/ind-forns/{id}", indForn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indForn.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndFornsByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndFornShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indFornList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndFornShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndFornsByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndFornShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indFornList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndFornShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndFornsByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where indirizzo is not null
        defaultIndFornShouldBeFound("indirizzo.specified=true");

        // Get all the indFornList where indirizzo is null
        defaultIndFornShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where cap equals to DEFAULT_CAP
        defaultIndFornShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indFornList where cap equals to UPDATED_CAP
        defaultIndFornShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndFornsByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndFornShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indFornList where cap equals to UPDATED_CAP
        defaultIndFornShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndFornsByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where cap is not null
        defaultIndFornShouldBeFound("cap.specified=true");

        // Get all the indFornList where cap is null
        defaultIndFornShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where nazione equals to DEFAULT_NAZIONE
        defaultIndFornShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indFornList where nazione equals to UPDATED_NAZIONE
        defaultIndFornShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndFornShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indFornList where nazione equals to UPDATED_NAZIONE
        defaultIndFornShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where nazione is not null
        defaultIndFornShouldBeFound("nazione.specified=true");

        // Get all the indFornList where nazione is null
        defaultIndFornShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where regione equals to DEFAULT_REGIONE
        defaultIndFornShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indFornList where regione equals to UPDATED_REGIONE
        defaultIndFornShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndFornShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indFornList where regione equals to UPDATED_REGIONE
        defaultIndFornShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where regione is not null
        defaultIndFornShouldBeFound("regione.specified=true");

        // Get all the indFornList where regione is null
        defaultIndFornShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where provincia equals to DEFAULT_PROVINCIA
        defaultIndFornShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indFornList where provincia equals to UPDATED_PROVINCIA
        defaultIndFornShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndFornsByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndFornShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indFornList where provincia equals to UPDATED_PROVINCIA
        defaultIndFornShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndFornsByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where provincia is not null
        defaultIndFornShouldBeFound("provincia.specified=true");

        // Get all the indFornList where provincia is null
        defaultIndFornShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where comune equals to DEFAULT_COMUNE
        defaultIndFornShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indFornList where comune equals to UPDATED_COMUNE
        defaultIndFornShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndFornShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indFornList where comune equals to UPDATED_COMUNE
        defaultIndFornShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where comune is not null
        defaultIndFornShouldBeFound("comune.specified=true");

        // Get all the indFornList where comune is null
        defaultIndFornShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndFornShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indFornList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndFornShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndFornsByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndFornShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indFornList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndFornShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndFornsByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where numeroCiv is not null
        defaultIndFornShouldBeFound("numeroCiv.specified=true");

        // Get all the indFornList where numeroCiv is null
        defaultIndFornShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude equals to DEFAULT_LATITUDE
        defaultIndFornShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indFornList where latitude equals to UPDATED_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndFornShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indFornList where latitude equals to UPDATED_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude is not null
        defaultIndFornShouldBeFound("latitude.specified=true");

        // Get all the indFornList where latitude is null
        defaultIndFornShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndFornShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indFornList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndFornShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indFornList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude is less than DEFAULT_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indFornList where latitude is less than UPDATED_LATITUDE
        defaultIndFornShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where latitude is greater than DEFAULT_LATITUDE
        defaultIndFornShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indFornList where latitude is greater than SMALLER_LATITUDE
        defaultIndFornShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude equals to DEFAULT_LONGITUDE
        defaultIndFornShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indFornList where longitude equals to UPDATED_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndFornShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indFornList where longitude equals to UPDATED_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude is not null
        defaultIndFornShouldBeFound("longitude.specified=true");

        // Get all the indFornList where longitude is null
        defaultIndFornShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndFornShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indFornList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndFornShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indFornList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude is less than DEFAULT_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indFornList where longitude is less than UPDATED_LONGITUDE
        defaultIndFornShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndFornsByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndFornShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indFornList where longitude is greater than SMALLER_LONGITUDE
        defaultIndFornShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali is not null
        defaultIndFornShouldBeFound("dataInizVali.specified=true");

        // Get all the indFornList where dataInizVali is null
        defaultIndFornShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndFornShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indFornList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndFornShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali is not null
        defaultIndFornShouldBeFound("dataFineVali.specified=true");

        // Get all the indFornList where dataFineVali is null
        defaultIndFornShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndFornsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndFornShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indFornList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndFornShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator is not null
        defaultIndFornShouldBeFound("userIdCreator.specified=true");

        // Get all the indFornList where userIdCreator is null
        defaultIndFornShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndFornShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indFornList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndFornShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod is not null
        defaultIndFornShouldBeFound("userIdLastMod.specified=true");

        // Get all the indFornList where userIdLastMod is null
        defaultIndFornShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndFornsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);

        // Get all the indFornList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndFornShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indFornList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndFornShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndFornsByIndToFornIsEqualToSomething() throws Exception {
        // Initialize the database
        indFornRepository.saveAndFlush(indForn);
        Fornitori indToForn = FornitoriResourceIT.createEntity(em);
        em.persist(indToForn);
        em.flush();
        indForn.setIndToForn(indToForn);
        indFornRepository.saveAndFlush(indForn);
        Long indToFornId = indToForn.getId();

        // Get all the indFornList where indToForn equals to indToFornId
        defaultIndFornShouldBeFound("indToFornId.equals=" + indToFornId);

        // Get all the indFornList where indToForn equals to indToFornId + 1
        defaultIndFornShouldNotBeFound("indToFornId.equals=" + (indToFornId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndFornShouldBeFound(String filter) throws Exception {
        restIndFornMockMvc.perform(get("/api/ind-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndFornMockMvc.perform(get("/api/ind-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndFornShouldNotBeFound(String filter) throws Exception {
        restIndFornMockMvc.perform(get("/api/ind-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndFornMockMvc.perform(get("/api/ind-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndForn() throws Exception {
        // Get the indForn
        restIndFornMockMvc.perform(get("/api/ind-forns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndForn() throws Exception {
        // Initialize the database
        indFornService.save(indForn);

        int databaseSizeBeforeUpdate = indFornRepository.findAll().size();

        // Update the indForn
        IndForn updatedIndForn = indFornRepository.findById(indForn.getId()).get();
        // Disconnect from session so that the updates on updatedIndForn are not directly saved in db
        em.detach(updatedIndForn);
        updatedIndForn
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndFornMockMvc.perform(put("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndForn)))
            .andExpect(status().isOk());

        // Validate the IndForn in the database
        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeUpdate);
        IndForn testIndForn = indFornList.get(indFornList.size() - 1);
        assertThat(testIndForn.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndForn.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndForn.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndForn.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndForn.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndForn.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndForn.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndForn.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndForn.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndForn.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndForn.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndForn.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndForn.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndForn.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndForn() throws Exception {
        int databaseSizeBeforeUpdate = indFornRepository.findAll().size();

        // Create the IndForn

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndFornMockMvc.perform(put("/api/ind-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indForn)))
            .andExpect(status().isBadRequest());

        // Validate the IndForn in the database
        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndForn() throws Exception {
        // Initialize the database
        indFornService.save(indForn);

        int databaseSizeBeforeDelete = indFornRepository.findAll().size();

        // Delete the indForn
        restIndFornMockMvc.perform(delete("/api/ind-forns/{id}", indForn.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndForn> indFornList = indFornRepository.findAll();
        assertThat(indFornList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndForn.class);
        IndForn indForn1 = new IndForn();
        indForn1.setId(1L);
        IndForn indForn2 = new IndForn();
        indForn2.setId(indForn1.getId());
        assertThat(indForn1).isEqualTo(indForn2);
        indForn2.setId(2L);
        assertThat(indForn1).isNotEqualTo(indForn2);
        indForn1.setId(null);
        assertThat(indForn1).isNotEqualTo(indForn2);
    }
}
