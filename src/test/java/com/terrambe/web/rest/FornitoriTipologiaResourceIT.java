package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.FornitoriTipologia;
import com.terrambe.domain.Fornitori;
import com.terrambe.repository.FornitoriTipologiaRepository;
import com.terrambe.service.FornitoriTipologiaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FornitoriTipologiaCriteria;
import com.terrambe.service.FornitoriTipologiaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FornitoriTipologiaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FornitoriTipologiaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FornitoriTipologiaRepository fornitoriTipologiaRepository;

    @Autowired
    private FornitoriTipologiaService fornitoriTipologiaService;

    @Autowired
    private FornitoriTipologiaQueryService fornitoriTipologiaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFornitoriTipologiaMockMvc;

    private FornitoriTipologia fornitoriTipologia;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FornitoriTipologiaResource fornitoriTipologiaResource = new FornitoriTipologiaResource(fornitoriTipologiaService, fornitoriTipologiaQueryService);
        this.restFornitoriTipologiaMockMvc = MockMvcBuilders.standaloneSetup(fornitoriTipologiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FornitoriTipologia createEntity(EntityManager em) {
        FornitoriTipologia fornitoriTipologia = new FornitoriTipologia()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return fornitoriTipologia;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FornitoriTipologia createUpdatedEntity(EntityManager em) {
        FornitoriTipologia fornitoriTipologia = new FornitoriTipologia()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return fornitoriTipologia;
    }

    @BeforeEach
    public void initTest() {
        fornitoriTipologia = createEntity(em);
    }

    @Test
    @Transactional
    public void createFornitoriTipologia() throws Exception {
        int databaseSizeBeforeCreate = fornitoriTipologiaRepository.findAll().size();

        // Create the FornitoriTipologia
        restFornitoriTipologiaMockMvc.perform(post("/api/fornitori-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitoriTipologia)))
            .andExpect(status().isCreated());

        // Validate the FornitoriTipologia in the database
        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeCreate + 1);
        FornitoriTipologia testFornitoriTipologia = fornitoriTipologiaList.get(fornitoriTipologiaList.size() - 1);
        assertThat(testFornitoriTipologia.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testFornitoriTipologia.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFornitoriTipologia.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFornitoriTipologia.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFornitoriTipologia.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFornitoriTipologia.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFornitoriTipologiaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fornitoriTipologiaRepository.findAll().size();

        // Create the FornitoriTipologia with an existing ID
        fornitoriTipologia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFornitoriTipologiaMockMvc.perform(post("/api/fornitori-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitoriTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the FornitoriTipologia in the database
        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornitoriTipologiaRepository.findAll().size();
        // set the field null
        fornitoriTipologia.setDescrizione(null);

        // Create the FornitoriTipologia, which fails.

        restFornitoriTipologiaMockMvc.perform(post("/api/fornitori-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitoriTipologia)))
            .andExpect(status().isBadRequest());

        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologias() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornitoriTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getFornitoriTipologia() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get the fornitoriTipologia
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias/{id}", fornitoriTipologia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fornitoriTipologia.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultFornitoriTipologiaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the fornitoriTipologiaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFornitoriTipologiaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultFornitoriTipologiaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the fornitoriTipologiaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFornitoriTipologiaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where descrizione is not null
        defaultFornitoriTipologiaShouldBeFound("descrizione.specified=true");

        // Get all the fornitoriTipologiaList where descrizione is null
        defaultFornitoriTipologiaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali is not null
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.specified=true");

        // Get all the fornitoriTipologiaList where dataInizVali is null
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriTipologiaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFornitoriTipologiaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali is not null
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.specified=true");

        // Get all the fornitoriTipologiaList where dataFineVali is null
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriTipologiaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFornitoriTipologiaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator is not null
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.specified=true");

        // Get all the fornitoriTipologiaList where userIdCreator is null
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriTipologiaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFornitoriTipologiaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod is not null
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.specified=true");

        // Get all the fornitoriTipologiaList where userIdLastMod is null
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitoriTipologiasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);

        // Get all the fornitoriTipologiaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriTipologiaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFornitoriTipologiaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFornitoriTipologiasByFornitoriIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);
        Fornitori fornitori = FornitoriResourceIT.createEntity(em);
        em.persist(fornitori);
        em.flush();
        fornitoriTipologia.addFornitori(fornitori);
        fornitoriTipologiaRepository.saveAndFlush(fornitoriTipologia);
        Long fornitoriId = fornitori.getId();

        // Get all the fornitoriTipologiaList where fornitori equals to fornitoriId
        defaultFornitoriTipologiaShouldBeFound("fornitoriId.equals=" + fornitoriId);

        // Get all the fornitoriTipologiaList where fornitori equals to fornitoriId + 1
        defaultFornitoriTipologiaShouldNotBeFound("fornitoriId.equals=" + (fornitoriId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFornitoriTipologiaShouldBeFound(String filter) throws Exception {
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornitoriTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFornitoriTipologiaShouldNotBeFound(String filter) throws Exception {
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFornitoriTipologia() throws Exception {
        // Get the fornitoriTipologia
        restFornitoriTipologiaMockMvc.perform(get("/api/fornitori-tipologias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFornitoriTipologia() throws Exception {
        // Initialize the database
        fornitoriTipologiaService.save(fornitoriTipologia);

        int databaseSizeBeforeUpdate = fornitoriTipologiaRepository.findAll().size();

        // Update the fornitoriTipologia
        FornitoriTipologia updatedFornitoriTipologia = fornitoriTipologiaRepository.findById(fornitoriTipologia.getId()).get();
        // Disconnect from session so that the updates on updatedFornitoriTipologia are not directly saved in db
        em.detach(updatedFornitoriTipologia);
        updatedFornitoriTipologia
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFornitoriTipologiaMockMvc.perform(put("/api/fornitori-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFornitoriTipologia)))
            .andExpect(status().isOk());

        // Validate the FornitoriTipologia in the database
        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeUpdate);
        FornitoriTipologia testFornitoriTipologia = fornitoriTipologiaList.get(fornitoriTipologiaList.size() - 1);
        assertThat(testFornitoriTipologia.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testFornitoriTipologia.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFornitoriTipologia.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFornitoriTipologia.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFornitoriTipologia.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFornitoriTipologia.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFornitoriTipologia() throws Exception {
        int databaseSizeBeforeUpdate = fornitoriTipologiaRepository.findAll().size();

        // Create the FornitoriTipologia

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFornitoriTipologiaMockMvc.perform(put("/api/fornitori-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitoriTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the FornitoriTipologia in the database
        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFornitoriTipologia() throws Exception {
        // Initialize the database
        fornitoriTipologiaService.save(fornitoriTipologia);

        int databaseSizeBeforeDelete = fornitoriTipologiaRepository.findAll().size();

        // Delete the fornitoriTipologia
        restFornitoriTipologiaMockMvc.perform(delete("/api/fornitori-tipologias/{id}", fornitoriTipologia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FornitoriTipologia> fornitoriTipologiaList = fornitoriTipologiaRepository.findAll();
        assertThat(fornitoriTipologiaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FornitoriTipologia.class);
        FornitoriTipologia fornitoriTipologia1 = new FornitoriTipologia();
        fornitoriTipologia1.setId(1L);
        FornitoriTipologia fornitoriTipologia2 = new FornitoriTipologia();
        fornitoriTipologia2.setId(fornitoriTipologia1.getId());
        assertThat(fornitoriTipologia1).isEqualTo(fornitoriTipologia2);
        fornitoriTipologia2.setId(2L);
        assertThat(fornitoriTipologia1).isNotEqualTo(fornitoriTipologia2);
        fornitoriTipologia1.setId(null);
        assertThat(fornitoriTipologia1).isNotEqualTo(fornitoriTipologia2);
    }
}
