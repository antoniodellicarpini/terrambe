package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Fornitori;
import com.terrambe.domain.IndForn;
import com.terrambe.domain.RecForn;
import com.terrambe.domain.FornitoriTipologia;
import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.FornitoriRepository;
import com.terrambe.service.FornitoriService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FornitoriCriteria;
import com.terrambe.service.FornitoriQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FornitoriResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FornitoriResourceIT {

    private static final String DEFAULT_REGISTRO_IMPRESE = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRO_IMPRESE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_ISCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NUM_ISCRIZIONE = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final String DEFAULT_NOME_FORNITORE = "AAAAAAAAAA";
    private static final String UPDATED_NOME_FORNITORE = "BBBBBBBBBB";

    private static final String DEFAULT_PIVA = "AAAAAAAAAAA";
    private static final String UPDATED_PIVA = "BBBBBBBBBBB";

    private static final String DEFAULT_CODI_FISC = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODI_FISC = "BBBBBBBBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FornitoriRepository fornitoriRepository;

    @Mock
    private FornitoriRepository fornitoriRepositoryMock;

    @Mock
    private FornitoriService fornitoriServiceMock;

    @Autowired
    private FornitoriService fornitoriService;

    @Autowired
    private FornitoriQueryService fornitoriQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFornitoriMockMvc;

    private Fornitori fornitori;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FornitoriResource fornitoriResource = new FornitoriResource(fornitoriService, fornitoriQueryService);
        this.restFornitoriMockMvc = MockMvcBuilders.standaloneSetup(fornitoriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fornitori createEntity(EntityManager em) {
        Fornitori fornitori = new Fornitori()
            .registroImprese(DEFAULT_REGISTRO_IMPRESE)
            .numIscrizione(DEFAULT_NUM_ISCRIZIONE)
            .idAzienda(DEFAULT_ID_AZIENDA)
            .nomeFornitore(DEFAULT_NOME_FORNITORE)
            .piva(DEFAULT_PIVA)
            .codiFisc(DEFAULT_CODI_FISC)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return fornitori;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fornitori createUpdatedEntity(EntityManager em) {
        Fornitori fornitori = new Fornitori()
            .registroImprese(UPDATED_REGISTRO_IMPRESE)
            .numIscrizione(UPDATED_NUM_ISCRIZIONE)
            .idAzienda(UPDATED_ID_AZIENDA)
            .nomeFornitore(UPDATED_NOME_FORNITORE)
            .piva(UPDATED_PIVA)
            .codiFisc(UPDATED_CODI_FISC)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return fornitori;
    }

    @BeforeEach
    public void initTest() {
        fornitori = createEntity(em);
    }

    @Test
    @Transactional
    public void createFornitori() throws Exception {
        int databaseSizeBeforeCreate = fornitoriRepository.findAll().size();

        // Create the Fornitori
        restFornitoriMockMvc.perform(post("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isCreated());

        // Validate the Fornitori in the database
        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeCreate + 1);
        Fornitori testFornitori = fornitoriList.get(fornitoriList.size() - 1);
        assertThat(testFornitori.getRegistroImprese()).isEqualTo(DEFAULT_REGISTRO_IMPRESE);
        assertThat(testFornitori.getNumIscrizione()).isEqualTo(DEFAULT_NUM_ISCRIZIONE);
        assertThat(testFornitori.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testFornitori.getNomeFornitore()).isEqualTo(DEFAULT_NOME_FORNITORE);
        assertThat(testFornitori.getPiva()).isEqualTo(DEFAULT_PIVA);
        assertThat(testFornitori.getCodiFisc()).isEqualTo(DEFAULT_CODI_FISC);
        assertThat(testFornitori.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFornitori.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFornitori.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFornitori.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFornitori.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFornitoriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fornitoriRepository.findAll().size();

        // Create the Fornitori with an existing ID
        fornitori.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFornitoriMockMvc.perform(post("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isBadRequest());

        // Validate the Fornitori in the database
        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornitoriRepository.findAll().size();
        // set the field null
        fornitori.setIdAzienda(null);

        // Create the Fornitori, which fails.

        restFornitoriMockMvc.perform(post("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isBadRequest());

        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeFornitoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornitoriRepository.findAll().size();
        // set the field null
        fornitori.setNomeFornitore(null);

        // Create the Fornitori, which fails.

        restFornitoriMockMvc.perform(post("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isBadRequest());

        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPivaIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornitoriRepository.findAll().size();
        // set the field null
        fornitori.setPiva(null);

        // Create the Fornitori, which fails.

        restFornitoriMockMvc.perform(post("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isBadRequest());

        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFornitoris() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList
        restFornitoriMockMvc.perform(get("/api/fornitoris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornitori.getId().intValue())))
            .andExpect(jsonPath("$.[*].registroImprese").value(hasItem(DEFAULT_REGISTRO_IMPRESE.toString())))
            .andExpect(jsonPath("$.[*].numIscrizione").value(hasItem(DEFAULT_NUM_ISCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].nomeFornitore").value(hasItem(DEFAULT_NOME_FORNITORE.toString())))
            .andExpect(jsonPath("$.[*].piva").value(hasItem(DEFAULT_PIVA.toString())))
            .andExpect(jsonPath("$.[*].codiFisc").value(hasItem(DEFAULT_CODI_FISC.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllFornitorisWithEagerRelationshipsIsEnabled() throws Exception {
        FornitoriResource fornitoriResource = new FornitoriResource(fornitoriServiceMock, fornitoriQueryService);
        when(fornitoriServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restFornitoriMockMvc = MockMvcBuilders.standaloneSetup(fornitoriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restFornitoriMockMvc.perform(get("/api/fornitoris?eagerload=true"))
        .andExpect(status().isOk());

        verify(fornitoriServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllFornitorisWithEagerRelationshipsIsNotEnabled() throws Exception {
        FornitoriResource fornitoriResource = new FornitoriResource(fornitoriServiceMock, fornitoriQueryService);
            when(fornitoriServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restFornitoriMockMvc = MockMvcBuilders.standaloneSetup(fornitoriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restFornitoriMockMvc.perform(get("/api/fornitoris?eagerload=true"))
        .andExpect(status().isOk());

            verify(fornitoriServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getFornitori() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get the fornitori
        restFornitoriMockMvc.perform(get("/api/fornitoris/{id}", fornitori.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fornitori.getId().intValue()))
            .andExpect(jsonPath("$.registroImprese").value(DEFAULT_REGISTRO_IMPRESE.toString()))
            .andExpect(jsonPath("$.numIscrizione").value(DEFAULT_NUM_ISCRIZIONE.toString()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.nomeFornitore").value(DEFAULT_NOME_FORNITORE.toString()))
            .andExpect(jsonPath("$.piva").value(DEFAULT_PIVA.toString()))
            .andExpect(jsonPath("$.codiFisc").value(DEFAULT_CODI_FISC.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFornitorisByRegistroImpreseIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where registroImprese equals to DEFAULT_REGISTRO_IMPRESE
        defaultFornitoriShouldBeFound("registroImprese.equals=" + DEFAULT_REGISTRO_IMPRESE);

        // Get all the fornitoriList where registroImprese equals to UPDATED_REGISTRO_IMPRESE
        defaultFornitoriShouldNotBeFound("registroImprese.equals=" + UPDATED_REGISTRO_IMPRESE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByRegistroImpreseIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where registroImprese in DEFAULT_REGISTRO_IMPRESE or UPDATED_REGISTRO_IMPRESE
        defaultFornitoriShouldBeFound("registroImprese.in=" + DEFAULT_REGISTRO_IMPRESE + "," + UPDATED_REGISTRO_IMPRESE);

        // Get all the fornitoriList where registroImprese equals to UPDATED_REGISTRO_IMPRESE
        defaultFornitoriShouldNotBeFound("registroImprese.in=" + UPDATED_REGISTRO_IMPRESE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByRegistroImpreseIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where registroImprese is not null
        defaultFornitoriShouldBeFound("registroImprese.specified=true");

        // Get all the fornitoriList where registroImprese is null
        defaultFornitoriShouldNotBeFound("registroImprese.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByNumIscrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where numIscrizione equals to DEFAULT_NUM_ISCRIZIONE
        defaultFornitoriShouldBeFound("numIscrizione.equals=" + DEFAULT_NUM_ISCRIZIONE);

        // Get all the fornitoriList where numIscrizione equals to UPDATED_NUM_ISCRIZIONE
        defaultFornitoriShouldNotBeFound("numIscrizione.equals=" + UPDATED_NUM_ISCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByNumIscrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where numIscrizione in DEFAULT_NUM_ISCRIZIONE or UPDATED_NUM_ISCRIZIONE
        defaultFornitoriShouldBeFound("numIscrizione.in=" + DEFAULT_NUM_ISCRIZIONE + "," + UPDATED_NUM_ISCRIZIONE);

        // Get all the fornitoriList where numIscrizione equals to UPDATED_NUM_ISCRIZIONE
        defaultFornitoriShouldNotBeFound("numIscrizione.in=" + UPDATED_NUM_ISCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByNumIscrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where numIscrizione is not null
        defaultFornitoriShouldBeFound("numIscrizione.specified=true");

        // Get all the fornitoriList where numIscrizione is null
        defaultFornitoriShouldNotBeFound("numIscrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda is not null
        defaultFornitoriShouldBeFound("idAzienda.specified=true");

        // Get all the fornitoriList where idAzienda is null
        defaultFornitoriShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultFornitoriShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the fornitoriList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultFornitoriShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllFornitorisByNomeFornitoreIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where nomeFornitore equals to DEFAULT_NOME_FORNITORE
        defaultFornitoriShouldBeFound("nomeFornitore.equals=" + DEFAULT_NOME_FORNITORE);

        // Get all the fornitoriList where nomeFornitore equals to UPDATED_NOME_FORNITORE
        defaultFornitoriShouldNotBeFound("nomeFornitore.equals=" + UPDATED_NOME_FORNITORE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByNomeFornitoreIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where nomeFornitore in DEFAULT_NOME_FORNITORE or UPDATED_NOME_FORNITORE
        defaultFornitoriShouldBeFound("nomeFornitore.in=" + DEFAULT_NOME_FORNITORE + "," + UPDATED_NOME_FORNITORE);

        // Get all the fornitoriList where nomeFornitore equals to UPDATED_NOME_FORNITORE
        defaultFornitoriShouldNotBeFound("nomeFornitore.in=" + UPDATED_NOME_FORNITORE);
    }

    @Test
    @Transactional
    public void getAllFornitorisByNomeFornitoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where nomeFornitore is not null
        defaultFornitoriShouldBeFound("nomeFornitore.specified=true");

        // Get all the fornitoriList where nomeFornitore is null
        defaultFornitoriShouldNotBeFound("nomeFornitore.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByPivaIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where piva equals to DEFAULT_PIVA
        defaultFornitoriShouldBeFound("piva.equals=" + DEFAULT_PIVA);

        // Get all the fornitoriList where piva equals to UPDATED_PIVA
        defaultFornitoriShouldNotBeFound("piva.equals=" + UPDATED_PIVA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByPivaIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where piva in DEFAULT_PIVA or UPDATED_PIVA
        defaultFornitoriShouldBeFound("piva.in=" + DEFAULT_PIVA + "," + UPDATED_PIVA);

        // Get all the fornitoriList where piva equals to UPDATED_PIVA
        defaultFornitoriShouldNotBeFound("piva.in=" + UPDATED_PIVA);
    }

    @Test
    @Transactional
    public void getAllFornitorisByPivaIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where piva is not null
        defaultFornitoriShouldBeFound("piva.specified=true");

        // Get all the fornitoriList where piva is null
        defaultFornitoriShouldNotBeFound("piva.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByCodiFiscIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where codiFisc equals to DEFAULT_CODI_FISC
        defaultFornitoriShouldBeFound("codiFisc.equals=" + DEFAULT_CODI_FISC);

        // Get all the fornitoriList where codiFisc equals to UPDATED_CODI_FISC
        defaultFornitoriShouldNotBeFound("codiFisc.equals=" + UPDATED_CODI_FISC);
    }

    @Test
    @Transactional
    public void getAllFornitorisByCodiFiscIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where codiFisc in DEFAULT_CODI_FISC or UPDATED_CODI_FISC
        defaultFornitoriShouldBeFound("codiFisc.in=" + DEFAULT_CODI_FISC + "," + UPDATED_CODI_FISC);

        // Get all the fornitoriList where codiFisc equals to UPDATED_CODI_FISC
        defaultFornitoriShouldNotBeFound("codiFisc.in=" + UPDATED_CODI_FISC);
    }

    @Test
    @Transactional
    public void getAllFornitorisByCodiFiscIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where codiFisc is not null
        defaultFornitoriShouldBeFound("codiFisc.specified=true");

        // Get all the fornitoriList where codiFisc is null
        defaultFornitoriShouldNotBeFound("codiFisc.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali is not null
        defaultFornitoriShouldBeFound("dataInizVali.specified=true");

        // Get all the fornitoriList where dataInizVali is null
        defaultFornitoriShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFornitoriShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the fornitoriList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFornitoriShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali is not null
        defaultFornitoriShouldBeFound("dataFineVali.specified=true");

        // Get all the fornitoriList where dataFineVali is null
        defaultFornitoriShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFornitorisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFornitoriShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the fornitoriList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFornitoriShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator is not null
        defaultFornitoriShouldBeFound("userIdCreator.specified=true");

        // Get all the fornitoriList where userIdCreator is null
        defaultFornitoriShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFornitoriShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the fornitoriList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFornitoriShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod is not null
        defaultFornitoriShouldBeFound("userIdLastMod.specified=true");

        // Get all the fornitoriList where userIdLastMod is null
        defaultFornitoriShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFornitorisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);

        // Get all the fornitoriList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFornitoriShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the fornitoriList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFornitoriShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        IndForn fornToInd = IndFornResourceIT.createEntity(em);
        em.persist(fornToInd);
        em.flush();
        fornitori.addFornToInd(fornToInd);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornToIndId = fornToInd.getId();

        // Get all the fornitoriList where fornToInd equals to fornToIndId
        defaultFornitoriShouldBeFound("fornToIndId.equals=" + fornToIndId);

        // Get all the fornitoriList where fornToInd equals to fornToIndId + 1
        defaultFornitoriShouldNotBeFound("fornToIndId.equals=" + (fornToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        RecForn fornToRec = RecFornResourceIT.createEntity(em);
        em.persist(fornToRec);
        em.flush();
        fornitori.addFornToRec(fornToRec);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornToRecId = fornToRec.getId();

        // Get all the fornitoriList where fornToRec equals to fornToRecId
        defaultFornitoriShouldBeFound("fornToRecId.equals=" + fornToRecId);

        // Get all the fornitoriList where fornToRec equals to fornToRecId + 1
        defaultFornitoriShouldNotBeFound("fornToRecId.equals=" + (fornToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornitoritipologiaIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        FornitoriTipologia fornitoritipologia = FornitoriTipologiaResourceIT.createEntity(em);
        em.persist(fornitoritipologia);
        em.flush();
        fornitori.addFornitoritipologia(fornitoritipologia);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornitoritipologiaId = fornitoritipologia.getId();

        // Get all the fornitoriList where fornitoritipologia equals to fornitoritipologiaId
        defaultFornitoriShouldBeFound("fornitoritipologiaId.equals=" + fornitoritipologiaId);

        // Get all the fornitoriList where fornitoritipologia equals to fornitoritipologiaId + 1
        defaultFornitoriShouldNotBeFound("fornitoritipologiaId.equals=" + (fornitoritipologiaId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornitoriToCaricoFertIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        RegCaricoFertilizzante fornitoriToCaricoFert = RegCaricoFertilizzanteResourceIT.createEntity(em);
        em.persist(fornitoriToCaricoFert);
        em.flush();
        fornitori.addFornitoriToCaricoFert(fornitoriToCaricoFert);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornitoriToCaricoFertId = fornitoriToCaricoFert.getId();

        // Get all the fornitoriList where fornitoriToCaricoFert equals to fornitoriToCaricoFertId
        defaultFornitoriShouldBeFound("fornitoriToCaricoFertId.equals=" + fornitoriToCaricoFertId);

        // Get all the fornitoriList where fornitoriToCaricoFert equals to fornitoriToCaricoFertId + 1
        defaultFornitoriShouldNotBeFound("fornitoriToCaricoFertId.equals=" + (fornitoriToCaricoFertId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornitoriToCaricoFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        RegCaricoFitoFarmaco fornitoriToCaricoFito = RegCaricoFitoFarmacoResourceIT.createEntity(em);
        em.persist(fornitoriToCaricoFito);
        em.flush();
        fornitori.addFornitoriToCaricoFito(fornitoriToCaricoFito);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornitoriToCaricoFitoId = fornitoriToCaricoFito.getId();

        // Get all the fornitoriList where fornitoriToCaricoFito equals to fornitoriToCaricoFitoId
        defaultFornitoriShouldBeFound("fornitoriToCaricoFitoId.equals=" + fornitoriToCaricoFitoId);

        // Get all the fornitoriList where fornitoriToCaricoFito equals to fornitoriToCaricoFitoId + 1
        defaultFornitoriShouldNotBeFound("fornitoriToCaricoFitoId.equals=" + (fornitoriToCaricoFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornitoriToCaricoTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        RegCaricoTrappole fornitoriToCaricoTrap = RegCaricoTrappoleResourceIT.createEntity(em);
        em.persist(fornitoriToCaricoTrap);
        em.flush();
        fornitori.addFornitoriToCaricoTrap(fornitoriToCaricoTrap);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornitoriToCaricoTrapId = fornitoriToCaricoTrap.getId();

        // Get all the fornitoriList where fornitoriToCaricoTrap equals to fornitoriToCaricoTrapId
        defaultFornitoriShouldBeFound("fornitoriToCaricoTrapId.equals=" + fornitoriToCaricoTrapId);

        // Get all the fornitoriList where fornitoriToCaricoTrap equals to fornitoriToCaricoTrapId + 1
        defaultFornitoriShouldNotBeFound("fornitoriToCaricoTrapId.equals=" + (fornitoriToCaricoTrapId + 1));
    }


    @Test
    @Transactional
    public void getAllFornitorisByFornToOpeAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        fornitoriRepository.saveAndFlush(fornitori);
        OpeAnagrafica fornToOpeAna = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(fornToOpeAna);
        em.flush();
        fornitori.addFornToOpeAna(fornToOpeAna);
        fornitoriRepository.saveAndFlush(fornitori);
        Long fornToOpeAnaId = fornToOpeAna.getId();

        // Get all the fornitoriList where fornToOpeAna equals to fornToOpeAnaId
        defaultFornitoriShouldBeFound("fornToOpeAnaId.equals=" + fornToOpeAnaId);

        // Get all the fornitoriList where fornToOpeAna equals to fornToOpeAnaId + 1
        defaultFornitoriShouldNotBeFound("fornToOpeAnaId.equals=" + (fornToOpeAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFornitoriShouldBeFound(String filter) throws Exception {
        restFornitoriMockMvc.perform(get("/api/fornitoris?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornitori.getId().intValue())))
            .andExpect(jsonPath("$.[*].registroImprese").value(hasItem(DEFAULT_REGISTRO_IMPRESE)))
            .andExpect(jsonPath("$.[*].numIscrizione").value(hasItem(DEFAULT_NUM_ISCRIZIONE)))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].nomeFornitore").value(hasItem(DEFAULT_NOME_FORNITORE)))
            .andExpect(jsonPath("$.[*].piva").value(hasItem(DEFAULT_PIVA)))
            .andExpect(jsonPath("$.[*].codiFisc").value(hasItem(DEFAULT_CODI_FISC)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFornitoriMockMvc.perform(get("/api/fornitoris/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFornitoriShouldNotBeFound(String filter) throws Exception {
        restFornitoriMockMvc.perform(get("/api/fornitoris?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFornitoriMockMvc.perform(get("/api/fornitoris/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFornitori() throws Exception {
        // Get the fornitori
        restFornitoriMockMvc.perform(get("/api/fornitoris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFornitori() throws Exception {
        // Initialize the database
        fornitoriService.save(fornitori);

        int databaseSizeBeforeUpdate = fornitoriRepository.findAll().size();

        // Update the fornitori
        Fornitori updatedFornitori = fornitoriRepository.findById(fornitori.getId()).get();
        // Disconnect from session so that the updates on updatedFornitori are not directly saved in db
        em.detach(updatedFornitori);
        updatedFornitori
            .registroImprese(UPDATED_REGISTRO_IMPRESE)
            .numIscrizione(UPDATED_NUM_ISCRIZIONE)
            .idAzienda(UPDATED_ID_AZIENDA)
            .nomeFornitore(UPDATED_NOME_FORNITORE)
            .piva(UPDATED_PIVA)
            .codiFisc(UPDATED_CODI_FISC)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFornitoriMockMvc.perform(put("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFornitori)))
            .andExpect(status().isOk());

        // Validate the Fornitori in the database
        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeUpdate);
        Fornitori testFornitori = fornitoriList.get(fornitoriList.size() - 1);
        assertThat(testFornitori.getRegistroImprese()).isEqualTo(UPDATED_REGISTRO_IMPRESE);
        assertThat(testFornitori.getNumIscrizione()).isEqualTo(UPDATED_NUM_ISCRIZIONE);
        assertThat(testFornitori.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testFornitori.getNomeFornitore()).isEqualTo(UPDATED_NOME_FORNITORE);
        assertThat(testFornitori.getPiva()).isEqualTo(UPDATED_PIVA);
        assertThat(testFornitori.getCodiFisc()).isEqualTo(UPDATED_CODI_FISC);
        assertThat(testFornitori.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFornitori.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFornitori.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFornitori.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFornitori.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFornitori() throws Exception {
        int databaseSizeBeforeUpdate = fornitoriRepository.findAll().size();

        // Create the Fornitori

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFornitoriMockMvc.perform(put("/api/fornitoris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fornitori)))
            .andExpect(status().isBadRequest());

        // Validate the Fornitori in the database
        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFornitori() throws Exception {
        // Initialize the database
        fornitoriService.save(fornitori);

        int databaseSizeBeforeDelete = fornitoriRepository.findAll().size();

        // Delete the fornitori
        restFornitoriMockMvc.perform(delete("/api/fornitoris/{id}", fornitori.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fornitori> fornitoriList = fornitoriRepository.findAll();
        assertThat(fornitoriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fornitori.class);
        Fornitori fornitori1 = new Fornitori();
        fornitori1.setId(1L);
        Fornitori fornitori2 = new Fornitori();
        fornitori2.setId(fornitori1.getId());
        assertThat(fornitori1).isEqualTo(fornitori2);
        fornitori2.setId(2L);
        assertThat(fornitori1).isNotEqualTo(fornitori2);
        fornitori1.setId(null);
        assertThat(fornitori1).isNotEqualTo(fornitori2);
    }
}
