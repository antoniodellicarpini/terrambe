package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipologiaFito;
import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.repository.TipologiaFitoRepository;
import com.terrambe.service.TipologiaFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipologiaFitoCriteria;
import com.terrambe.service.TipologiaFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipologiaFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipologiaFitoResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPOLOGIA = "AAAAAAAAAA";
    private static final String UPDATED_TIPOLOGIA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipologiaFitoRepository tipologiaFitoRepository;

    @Autowired
    private TipologiaFitoService tipologiaFitoService;

    @Autowired
    private TipologiaFitoQueryService tipologiaFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipologiaFitoMockMvc;

    private TipologiaFito tipologiaFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipologiaFitoResource tipologiaFitoResource = new TipologiaFitoResource(tipologiaFitoService, tipologiaFitoQueryService);
        this.restTipologiaFitoMockMvc = MockMvcBuilders.standaloneSetup(tipologiaFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaFito createEntity(EntityManager em) {
        TipologiaFito tipologiaFito = new TipologiaFito()
            .codice(DEFAULT_CODICE)
            .tipologia(DEFAULT_TIPOLOGIA)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipologiaFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaFito createUpdatedEntity(EntityManager em) {
        TipologiaFito tipologiaFito = new TipologiaFito()
            .codice(UPDATED_CODICE)
            .tipologia(UPDATED_TIPOLOGIA)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipologiaFito;
    }

    @BeforeEach
    public void initTest() {
        tipologiaFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipologiaFito() throws Exception {
        int databaseSizeBeforeCreate = tipologiaFitoRepository.findAll().size();

        // Create the TipologiaFito
        restTipologiaFitoMockMvc.perform(post("/api/tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaFito)))
            .andExpect(status().isCreated());

        // Validate the TipologiaFito in the database
        List<TipologiaFito> tipologiaFitoList = tipologiaFitoRepository.findAll();
        assertThat(tipologiaFitoList).hasSize(databaseSizeBeforeCreate + 1);
        TipologiaFito testTipologiaFito = tipologiaFitoList.get(tipologiaFitoList.size() - 1);
        assertThat(testTipologiaFito.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testTipologiaFito.getTipologia()).isEqualTo(DEFAULT_TIPOLOGIA);
        assertThat(testTipologiaFito.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testTipologiaFito.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testTipologiaFito.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testTipologiaFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipologiaFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipologiaFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipologiaFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipologiaFito.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipologiaFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipologiaFitoRepository.findAll().size();

        // Create the TipologiaFito with an existing ID
        tipologiaFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipologiaFitoMockMvc.perform(post("/api/tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaFito)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaFito in the database
        List<TipologiaFito> tipologiaFitoList = tipologiaFitoRepository.findAll();
        assertThat(tipologiaFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipologiaFitos() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].tipologia").value(hasItem(DEFAULT_TIPOLOGIA.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipologiaFito() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get the tipologiaFito
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos/{id}", tipologiaFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipologiaFito.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.tipologia").value(DEFAULT_TIPOLOGIA.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where codice equals to DEFAULT_CODICE
        defaultTipologiaFitoShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the tipologiaFitoList where codice equals to UPDATED_CODICE
        defaultTipologiaFitoShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultTipologiaFitoShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the tipologiaFitoList where codice equals to UPDATED_CODICE
        defaultTipologiaFitoShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where codice is not null
        defaultTipologiaFitoShouldBeFound("codice.specified=true");

        // Get all the tipologiaFitoList where codice is null
        defaultTipologiaFitoShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipologiaIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipologia equals to DEFAULT_TIPOLOGIA
        defaultTipologiaFitoShouldBeFound("tipologia.equals=" + DEFAULT_TIPOLOGIA);

        // Get all the tipologiaFitoList where tipologia equals to UPDATED_TIPOLOGIA
        defaultTipologiaFitoShouldNotBeFound("tipologia.equals=" + UPDATED_TIPOLOGIA);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipologiaIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipologia in DEFAULT_TIPOLOGIA or UPDATED_TIPOLOGIA
        defaultTipologiaFitoShouldBeFound("tipologia.in=" + DEFAULT_TIPOLOGIA + "," + UPDATED_TIPOLOGIA);

        // Get all the tipologiaFitoList where tipologia equals to UPDATED_TIPOLOGIA
        defaultTipologiaFitoShouldNotBeFound("tipologia.in=" + UPDATED_TIPOLOGIA);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipologiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipologia is not null
        defaultTipologiaFitoShouldBeFound("tipologia.specified=true");

        // Get all the tipologiaFitoList where tipologia is null
        defaultTipologiaFitoShouldNotBeFound("tipologia.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultTipologiaFitoShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the tipologiaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTipologiaFitoShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultTipologiaFitoShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the tipologiaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTipologiaFitoShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where tipoImport is not null
        defaultTipologiaFitoShouldBeFound("tipoImport.specified=true");

        // Get all the tipologiaFitoList where tipoImport is null
        defaultTipologiaFitoShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where operatore equals to DEFAULT_OPERATORE
        defaultTipologiaFitoShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the tipologiaFitoList where operatore equals to UPDATED_OPERATORE
        defaultTipologiaFitoShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultTipologiaFitoShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the tipologiaFitoList where operatore equals to UPDATED_OPERATORE
        defaultTipologiaFitoShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where operatore is not null
        defaultTipologiaFitoShouldBeFound("operatore.specified=true");

        // Get all the tipologiaFitoList where operatore is null
        defaultTipologiaFitoShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where ts equals to DEFAULT_TS
        defaultTipologiaFitoShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the tipologiaFitoList where ts equals to UPDATED_TS
        defaultTipologiaFitoShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where ts in DEFAULT_TS or UPDATED_TS
        defaultTipologiaFitoShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the tipologiaFitoList where ts equals to UPDATED_TS
        defaultTipologiaFitoShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where ts is not null
        defaultTipologiaFitoShouldBeFound("ts.specified=true");

        // Get all the tipologiaFitoList where ts is null
        defaultTipologiaFitoShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali is not null
        defaultTipologiaFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the tipologiaFitoList where dataInizVali is null
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipologiaFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali is not null
        defaultTipologiaFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the tipologiaFitoList where dataFineVali is null
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipologiaFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipologiaFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator is not null
        defaultTipologiaFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the tipologiaFitoList where userIdCreator is null
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipologiaFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipologiaFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod is not null
        defaultTipologiaFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipologiaFitoList where userIdLastMod is null
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);

        // Get all the tipologiaFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipologiaFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipologiaFitosByTipofitofarmacoetichettaIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);
        EtiTipologiaFito tipofitofarmacoetichetta = EtiTipologiaFitoResourceIT.createEntity(em);
        em.persist(tipofitofarmacoetichetta);
        em.flush();
        tipologiaFito.addTipofitofarmacoetichetta(tipofitofarmacoetichetta);
        tipologiaFitoRepository.saveAndFlush(tipologiaFito);
        Long tipofitofarmacoetichettaId = tipofitofarmacoetichetta.getId();

        // Get all the tipologiaFitoList where tipofitofarmacoetichetta equals to tipofitofarmacoetichettaId
        defaultTipologiaFitoShouldBeFound("tipofitofarmacoetichettaId.equals=" + tipofitofarmacoetichettaId);

        // Get all the tipologiaFitoList where tipofitofarmacoetichetta equals to tipofitofarmacoetichettaId + 1
        defaultTipologiaFitoShouldNotBeFound("tipofitofarmacoetichettaId.equals=" + (tipofitofarmacoetichettaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipologiaFitoShouldBeFound(String filter) throws Exception {
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].tipologia").value(hasItem(DEFAULT_TIPOLOGIA)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipologiaFitoShouldNotBeFound(String filter) throws Exception {
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipologiaFito() throws Exception {
        // Get the tipologiaFito
        restTipologiaFitoMockMvc.perform(get("/api/tipologia-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipologiaFito() throws Exception {
        // Initialize the database
        tipologiaFitoService.save(tipologiaFito);

        int databaseSizeBeforeUpdate = tipologiaFitoRepository.findAll().size();

        // Update the tipologiaFito
        TipologiaFito updatedTipologiaFito = tipologiaFitoRepository.findById(tipologiaFito.getId()).get();
        // Disconnect from session so that the updates on updatedTipologiaFito are not directly saved in db
        em.detach(updatedTipologiaFito);
        updatedTipologiaFito
            .codice(UPDATED_CODICE)
            .tipologia(UPDATED_TIPOLOGIA)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipologiaFitoMockMvc.perform(put("/api/tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipologiaFito)))
            .andExpect(status().isOk());

        // Validate the TipologiaFito in the database
        List<TipologiaFito> tipologiaFitoList = tipologiaFitoRepository.findAll();
        assertThat(tipologiaFitoList).hasSize(databaseSizeBeforeUpdate);
        TipologiaFito testTipologiaFito = tipologiaFitoList.get(tipologiaFitoList.size() - 1);
        assertThat(testTipologiaFito.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testTipologiaFito.getTipologia()).isEqualTo(UPDATED_TIPOLOGIA);
        assertThat(testTipologiaFito.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testTipologiaFito.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testTipologiaFito.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testTipologiaFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipologiaFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipologiaFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipologiaFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipologiaFito.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipologiaFito() throws Exception {
        int databaseSizeBeforeUpdate = tipologiaFitoRepository.findAll().size();

        // Create the TipologiaFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipologiaFitoMockMvc.perform(put("/api/tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaFito)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaFito in the database
        List<TipologiaFito> tipologiaFitoList = tipologiaFitoRepository.findAll();
        assertThat(tipologiaFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipologiaFito() throws Exception {
        // Initialize the database
        tipologiaFitoService.save(tipologiaFito);

        int databaseSizeBeforeDelete = tipologiaFitoRepository.findAll().size();

        // Delete the tipologiaFito
        restTipologiaFitoMockMvc.perform(delete("/api/tipologia-fitos/{id}", tipologiaFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipologiaFito> tipologiaFitoList = tipologiaFitoRepository.findAll();
        assertThat(tipologiaFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipologiaFito.class);
        TipologiaFito tipologiaFito1 = new TipologiaFito();
        tipologiaFito1.setId(1L);
        TipologiaFito tipologiaFito2 = new TipologiaFito();
        tipologiaFito2.setId(tipologiaFito1.getId());
        assertThat(tipologiaFito1).isEqualTo(tipologiaFito2);
        tipologiaFito2.setId(2L);
        assertThat(tipologiaFito1).isNotEqualTo(tipologiaFito2);
        tipologiaFito1.setId(null);
        assertThat(tipologiaFito1).isNotEqualTo(tipologiaFito2);
    }
}
