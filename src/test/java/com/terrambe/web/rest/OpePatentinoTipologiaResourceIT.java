package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.domain.OpePatentino;
import com.terrambe.repository.OpePatentinoTipologiaRepository;
import com.terrambe.service.OpePatentinoTipologiaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.OpePatentinoTipologiaCriteria;
import com.terrambe.service.OpePatentinoTipologiaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OpePatentinoTipologiaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class OpePatentinoTipologiaResourceIT {

    private static final String DEFAULT_TPOLOGIA = "AAAAAAAAAA";
    private static final String UPDATED_TPOLOGIA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private OpePatentinoTipologiaRepository opePatentinoTipologiaRepository;

    @Autowired
    private OpePatentinoTipologiaService opePatentinoTipologiaService;

    @Autowired
    private OpePatentinoTipologiaQueryService opePatentinoTipologiaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOpePatentinoTipologiaMockMvc;

    private OpePatentinoTipologia opePatentinoTipologia;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OpePatentinoTipologiaResource opePatentinoTipologiaResource = new OpePatentinoTipologiaResource(opePatentinoTipologiaService, opePatentinoTipologiaQueryService);
        this.restOpePatentinoTipologiaMockMvc = MockMvcBuilders.standaloneSetup(opePatentinoTipologiaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpePatentinoTipologia createEntity(EntityManager em) {
        OpePatentinoTipologia opePatentinoTipologia = new OpePatentinoTipologia()
            .tpologia(DEFAULT_TPOLOGIA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return opePatentinoTipologia;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpePatentinoTipologia createUpdatedEntity(EntityManager em) {
        OpePatentinoTipologia opePatentinoTipologia = new OpePatentinoTipologia()
            .tpologia(UPDATED_TPOLOGIA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return opePatentinoTipologia;
    }

    @BeforeEach
    public void initTest() {
        opePatentinoTipologia = createEntity(em);
    }

    @Test
    @Transactional
    public void createOpePatentinoTipologia() throws Exception {
        int databaseSizeBeforeCreate = opePatentinoTipologiaRepository.findAll().size();

        // Create the OpePatentinoTipologia
        restOpePatentinoTipologiaMockMvc.perform(post("/api/ope-patentino-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentinoTipologia)))
            .andExpect(status().isCreated());

        // Validate the OpePatentinoTipologia in the database
        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeCreate + 1);
        OpePatentinoTipologia testOpePatentinoTipologia = opePatentinoTipologiaList.get(opePatentinoTipologiaList.size() - 1);
        assertThat(testOpePatentinoTipologia.getTpologia()).isEqualTo(DEFAULT_TPOLOGIA);
        assertThat(testOpePatentinoTipologia.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testOpePatentinoTipologia.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testOpePatentinoTipologia.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testOpePatentinoTipologia.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testOpePatentinoTipologia.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createOpePatentinoTipologiaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = opePatentinoTipologiaRepository.findAll().size();

        // Create the OpePatentinoTipologia with an existing ID
        opePatentinoTipologia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOpePatentinoTipologiaMockMvc.perform(post("/api/ope-patentino-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentinoTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the OpePatentinoTipologia in the database
        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTpologiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = opePatentinoTipologiaRepository.findAll().size();
        // set the field null
        opePatentinoTipologia.setTpologia(null);

        // Create the OpePatentinoTipologia, which fails.

        restOpePatentinoTipologiaMockMvc.perform(post("/api/ope-patentino-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentinoTipologia)))
            .andExpect(status().isBadRequest());

        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologias() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opePatentinoTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].tpologia").value(hasItem(DEFAULT_TPOLOGIA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getOpePatentinoTipologia() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get the opePatentinoTipologia
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias/{id}", opePatentinoTipologia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(opePatentinoTipologia.getId().intValue()))
            .andExpect(jsonPath("$.tpologia").value(DEFAULT_TPOLOGIA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByTpologiaIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where tpologia equals to DEFAULT_TPOLOGIA
        defaultOpePatentinoTipologiaShouldBeFound("tpologia.equals=" + DEFAULT_TPOLOGIA);

        // Get all the opePatentinoTipologiaList where tpologia equals to UPDATED_TPOLOGIA
        defaultOpePatentinoTipologiaShouldNotBeFound("tpologia.equals=" + UPDATED_TPOLOGIA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByTpologiaIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where tpologia in DEFAULT_TPOLOGIA or UPDATED_TPOLOGIA
        defaultOpePatentinoTipologiaShouldBeFound("tpologia.in=" + DEFAULT_TPOLOGIA + "," + UPDATED_TPOLOGIA);

        // Get all the opePatentinoTipologiaList where tpologia equals to UPDATED_TPOLOGIA
        defaultOpePatentinoTipologiaShouldNotBeFound("tpologia.in=" + UPDATED_TPOLOGIA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByTpologiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where tpologia is not null
        defaultOpePatentinoTipologiaShouldBeFound("tpologia.specified=true");

        // Get all the opePatentinoTipologiaList where tpologia is null
        defaultOpePatentinoTipologiaShouldNotBeFound("tpologia.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali is not null
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.specified=true");

        // Get all the opePatentinoTipologiaList where dataInizVali is null
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoTipologiaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali is not null
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.specified=true");

        // Get all the opePatentinoTipologiaList where dataFineVali is null
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoTipologiaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultOpePatentinoTipologiaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator is not null
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.specified=true");

        // Get all the opePatentinoTipologiaList where userIdCreator is null
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoTipologiaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultOpePatentinoTipologiaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod is not null
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.specified=true");

        // Get all the opePatentinoTipologiaList where userIdLastMod is null
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);

        // Get all the opePatentinoTipologiaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoTipologiaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultOpePatentinoTipologiaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllOpePatentinoTipologiasByOpePatTipToOpePateIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);
        OpePatentino opePatTipToOpePate = OpePatentinoResourceIT.createEntity(em);
        em.persist(opePatTipToOpePate);
        em.flush();
        opePatentinoTipologia.addOpePatTipToOpePate(opePatTipToOpePate);
        opePatentinoTipologiaRepository.saveAndFlush(opePatentinoTipologia);
        Long opePatTipToOpePateId = opePatTipToOpePate.getId();

        // Get all the opePatentinoTipologiaList where opePatTipToOpePate equals to opePatTipToOpePateId
        defaultOpePatentinoTipologiaShouldBeFound("opePatTipToOpePateId.equals=" + opePatTipToOpePateId);

        // Get all the opePatentinoTipologiaList where opePatTipToOpePate equals to opePatTipToOpePateId + 1
        defaultOpePatentinoTipologiaShouldNotBeFound("opePatTipToOpePateId.equals=" + (opePatTipToOpePateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOpePatentinoTipologiaShouldBeFound(String filter) throws Exception {
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opePatentinoTipologia.getId().intValue())))
            .andExpect(jsonPath("$.[*].tpologia").value(hasItem(DEFAULT_TPOLOGIA)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOpePatentinoTipologiaShouldNotBeFound(String filter) throws Exception {
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOpePatentinoTipologia() throws Exception {
        // Get the opePatentinoTipologia
        restOpePatentinoTipologiaMockMvc.perform(get("/api/ope-patentino-tipologias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOpePatentinoTipologia() throws Exception {
        // Initialize the database
        opePatentinoTipologiaService.save(opePatentinoTipologia);

        int databaseSizeBeforeUpdate = opePatentinoTipologiaRepository.findAll().size();

        // Update the opePatentinoTipologia
        OpePatentinoTipologia updatedOpePatentinoTipologia = opePatentinoTipologiaRepository.findById(opePatentinoTipologia.getId()).get();
        // Disconnect from session so that the updates on updatedOpePatentinoTipologia are not directly saved in db
        em.detach(updatedOpePatentinoTipologia);
        updatedOpePatentinoTipologia
            .tpologia(UPDATED_TPOLOGIA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restOpePatentinoTipologiaMockMvc.perform(put("/api/ope-patentino-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOpePatentinoTipologia)))
            .andExpect(status().isOk());

        // Validate the OpePatentinoTipologia in the database
        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeUpdate);
        OpePatentinoTipologia testOpePatentinoTipologia = opePatentinoTipologiaList.get(opePatentinoTipologiaList.size() - 1);
        assertThat(testOpePatentinoTipologia.getTpologia()).isEqualTo(UPDATED_TPOLOGIA);
        assertThat(testOpePatentinoTipologia.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testOpePatentinoTipologia.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testOpePatentinoTipologia.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testOpePatentinoTipologia.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testOpePatentinoTipologia.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingOpePatentinoTipologia() throws Exception {
        int databaseSizeBeforeUpdate = opePatentinoTipologiaRepository.findAll().size();

        // Create the OpePatentinoTipologia

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOpePatentinoTipologiaMockMvc.perform(put("/api/ope-patentino-tipologias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentinoTipologia)))
            .andExpect(status().isBadRequest());

        // Validate the OpePatentinoTipologia in the database
        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOpePatentinoTipologia() throws Exception {
        // Initialize the database
        opePatentinoTipologiaService.save(opePatentinoTipologia);

        int databaseSizeBeforeDelete = opePatentinoTipologiaRepository.findAll().size();

        // Delete the opePatentinoTipologia
        restOpePatentinoTipologiaMockMvc.perform(delete("/api/ope-patentino-tipologias/{id}", opePatentinoTipologia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OpePatentinoTipologia> opePatentinoTipologiaList = opePatentinoTipologiaRepository.findAll();
        assertThat(opePatentinoTipologiaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpePatentinoTipologia.class);
        OpePatentinoTipologia opePatentinoTipologia1 = new OpePatentinoTipologia();
        opePatentinoTipologia1.setId(1L);
        OpePatentinoTipologia opePatentinoTipologia2 = new OpePatentinoTipologia();
        opePatentinoTipologia2.setId(opePatentinoTipologia1.getId());
        assertThat(opePatentinoTipologia1).isEqualTo(opePatentinoTipologia2);
        opePatentinoTipologia2.setId(2L);
        assertThat(opePatentinoTipologia1).isNotEqualTo(opePatentinoTipologia2);
        opePatentinoTipologia1.setId(null);
        assertThat(opePatentinoTipologia1).isNotEqualTo(opePatentinoTipologia2);
    }
}
