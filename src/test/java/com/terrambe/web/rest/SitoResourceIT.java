package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Sito;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.SitoRepository;
import com.terrambe.service.SitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.SitoCriteria;
import com.terrambe.service.SitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class SitoResourceIT {

    private static final String DEFAULT_COD_SITO = "AAAAAAAAAA";
    private static final String UPDATED_COD_SITO = "BBBBBBBBBB";

    private static final String DEFAULT_DES_SITO = "AAAAAAAAAA";
    private static final String UPDATED_DES_SITO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private SitoRepository sitoRepository;

    @Autowired
    private SitoService sitoService;

    @Autowired
    private SitoQueryService sitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSitoMockMvc;

    private Sito sito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SitoResource sitoResource = new SitoResource(sitoService, sitoQueryService);
        this.restSitoMockMvc = MockMvcBuilders.standaloneSetup(sitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sito createEntity(EntityManager em) {
        Sito sito = new Sito()
            .codSito(DEFAULT_COD_SITO)
            .desSito(DEFAULT_DES_SITO)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return sito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sito createUpdatedEntity(EntityManager em) {
        Sito sito = new Sito()
            .codSito(UPDATED_COD_SITO)
            .desSito(UPDATED_DES_SITO)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return sito;
    }

    @BeforeEach
    public void initTest() {
        sito = createEntity(em);
    }

    @Test
    @Transactional
    public void createSito() throws Exception {
        int databaseSizeBeforeCreate = sitoRepository.findAll().size();

        // Create the Sito
        restSitoMockMvc.perform(post("/api/sitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sito)))
            .andExpect(status().isCreated());

        // Validate the Sito in the database
        List<Sito> sitoList = sitoRepository.findAll();
        assertThat(sitoList).hasSize(databaseSizeBeforeCreate + 1);
        Sito testSito = sitoList.get(sitoList.size() - 1);
        assertThat(testSito.getCodSito()).isEqualTo(DEFAULT_COD_SITO);
        assertThat(testSito.getDesSito()).isEqualTo(DEFAULT_DES_SITO);
        assertThat(testSito.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testSito.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testSito.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testSito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testSito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testSito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testSito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testSito.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createSitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sitoRepository.findAll().size();

        // Create the Sito with an existing ID
        sito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSitoMockMvc.perform(post("/api/sitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sito)))
            .andExpect(status().isBadRequest());

        // Validate the Sito in the database
        List<Sito> sitoList = sitoRepository.findAll();
        assertThat(sitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSitos() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList
        restSitoMockMvc.perform(get("/api/sitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sito.getId().intValue())))
            .andExpect(jsonPath("$.[*].codSito").value(hasItem(DEFAULT_COD_SITO.toString())))
            .andExpect(jsonPath("$.[*].desSito").value(hasItem(DEFAULT_DES_SITO.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getSito() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get the sito
        restSitoMockMvc.perform(get("/api/sitos/{id}", sito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sito.getId().intValue()))
            .andExpect(jsonPath("$.codSito").value(DEFAULT_COD_SITO.toString()))
            .andExpect(jsonPath("$.desSito").value(DEFAULT_DES_SITO.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllSitosByCodSitoIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where codSito equals to DEFAULT_COD_SITO
        defaultSitoShouldBeFound("codSito.equals=" + DEFAULT_COD_SITO);

        // Get all the sitoList where codSito equals to UPDATED_COD_SITO
        defaultSitoShouldNotBeFound("codSito.equals=" + UPDATED_COD_SITO);
    }

    @Test
    @Transactional
    public void getAllSitosByCodSitoIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where codSito in DEFAULT_COD_SITO or UPDATED_COD_SITO
        defaultSitoShouldBeFound("codSito.in=" + DEFAULT_COD_SITO + "," + UPDATED_COD_SITO);

        // Get all the sitoList where codSito equals to UPDATED_COD_SITO
        defaultSitoShouldNotBeFound("codSito.in=" + UPDATED_COD_SITO);
    }

    @Test
    @Transactional
    public void getAllSitosByCodSitoIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where codSito is not null
        defaultSitoShouldBeFound("codSito.specified=true");

        // Get all the sitoList where codSito is null
        defaultSitoShouldNotBeFound("codSito.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByDesSitoIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where desSito equals to DEFAULT_DES_SITO
        defaultSitoShouldBeFound("desSito.equals=" + DEFAULT_DES_SITO);

        // Get all the sitoList where desSito equals to UPDATED_DES_SITO
        defaultSitoShouldNotBeFound("desSito.equals=" + UPDATED_DES_SITO);
    }

    @Test
    @Transactional
    public void getAllSitosByDesSitoIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where desSito in DEFAULT_DES_SITO or UPDATED_DES_SITO
        defaultSitoShouldBeFound("desSito.in=" + DEFAULT_DES_SITO + "," + UPDATED_DES_SITO);

        // Get all the sitoList where desSito equals to UPDATED_DES_SITO
        defaultSitoShouldNotBeFound("desSito.in=" + UPDATED_DES_SITO);
    }

    @Test
    @Transactional
    public void getAllSitosByDesSitoIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where desSito is not null
        defaultSitoShouldBeFound("desSito.specified=true");

        // Get all the sitoList where desSito is null
        defaultSitoShouldNotBeFound("desSito.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultSitoShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the sitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultSitoShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllSitosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultSitoShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the sitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultSitoShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllSitosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where tipoImport is not null
        defaultSitoShouldBeFound("tipoImport.specified=true");

        // Get all the sitoList where tipoImport is null
        defaultSitoShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where operatore equals to DEFAULT_OPERATORE
        defaultSitoShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the sitoList where operatore equals to UPDATED_OPERATORE
        defaultSitoShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllSitosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultSitoShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the sitoList where operatore equals to UPDATED_OPERATORE
        defaultSitoShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllSitosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where operatore is not null
        defaultSitoShouldBeFound("operatore.specified=true");

        // Get all the sitoList where operatore is null
        defaultSitoShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where ts equals to DEFAULT_TS
        defaultSitoShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the sitoList where ts equals to UPDATED_TS
        defaultSitoShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllSitosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where ts in DEFAULT_TS or UPDATED_TS
        defaultSitoShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the sitoList where ts equals to UPDATED_TS
        defaultSitoShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllSitosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where ts is not null
        defaultSitoShouldBeFound("ts.specified=true");

        // Get all the sitoList where ts is null
        defaultSitoShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali is not null
        defaultSitoShouldBeFound("dataInizVali.specified=true");

        // Get all the sitoList where dataInizVali is null
        defaultSitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultSitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultSitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali is not null
        defaultSitoShouldBeFound("dataFineVali.specified=true");

        // Get all the sitoList where dataFineVali is null
        defaultSitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultSitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultSitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator is not null
        defaultSitoShouldBeFound("userIdCreator.specified=true");

        // Get all the sitoList where userIdCreator is null
        defaultSitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultSitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultSitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod is not null
        defaultSitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the sitoList where userIdLastMod is null
        defaultSitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);

        // Get all the sitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultSitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultSitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllSitosBySitoToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        sitoRepository.saveAndFlush(sito);
        TabellaRaccordo sitoToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(sitoToRaccordo);
        em.flush();
        sito.addSitoToRaccordo(sitoToRaccordo);
        sitoRepository.saveAndFlush(sito);
        Long sitoToRaccordoId = sitoToRaccordo.getId();

        // Get all the sitoList where sitoToRaccordo equals to sitoToRaccordoId
        defaultSitoShouldBeFound("sitoToRaccordoId.equals=" + sitoToRaccordoId);

        // Get all the sitoList where sitoToRaccordo equals to sitoToRaccordoId + 1
        defaultSitoShouldNotBeFound("sitoToRaccordoId.equals=" + (sitoToRaccordoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSitoShouldBeFound(String filter) throws Exception {
        restSitoMockMvc.perform(get("/api/sitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sito.getId().intValue())))
            .andExpect(jsonPath("$.[*].codSito").value(hasItem(DEFAULT_COD_SITO)))
            .andExpect(jsonPath("$.[*].desSito").value(hasItem(DEFAULT_DES_SITO)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restSitoMockMvc.perform(get("/api/sitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSitoShouldNotBeFound(String filter) throws Exception {
        restSitoMockMvc.perform(get("/api/sitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSitoMockMvc.perform(get("/api/sitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSito() throws Exception {
        // Get the sito
        restSitoMockMvc.perform(get("/api/sitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSito() throws Exception {
        // Initialize the database
        sitoService.save(sito);

        int databaseSizeBeforeUpdate = sitoRepository.findAll().size();

        // Update the sito
        Sito updatedSito = sitoRepository.findById(sito.getId()).get();
        // Disconnect from session so that the updates on updatedSito are not directly saved in db
        em.detach(updatedSito);
        updatedSito
            .codSito(UPDATED_COD_SITO)
            .desSito(UPDATED_DES_SITO)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restSitoMockMvc.perform(put("/api/sitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSito)))
            .andExpect(status().isOk());

        // Validate the Sito in the database
        List<Sito> sitoList = sitoRepository.findAll();
        assertThat(sitoList).hasSize(databaseSizeBeforeUpdate);
        Sito testSito = sitoList.get(sitoList.size() - 1);
        assertThat(testSito.getCodSito()).isEqualTo(UPDATED_COD_SITO);
        assertThat(testSito.getDesSito()).isEqualTo(UPDATED_DES_SITO);
        assertThat(testSito.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testSito.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testSito.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testSito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testSito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testSito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testSito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testSito.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingSito() throws Exception {
        int databaseSizeBeforeUpdate = sitoRepository.findAll().size();

        // Create the Sito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSitoMockMvc.perform(put("/api/sitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sito)))
            .andExpect(status().isBadRequest());

        // Validate the Sito in the database
        List<Sito> sitoList = sitoRepository.findAll();
        assertThat(sitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSito() throws Exception {
        // Initialize the database
        sitoService.save(sito);

        int databaseSizeBeforeDelete = sitoRepository.findAll().size();

        // Delete the sito
        restSitoMockMvc.perform(delete("/api/sitos/{id}", sito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sito> sitoList = sitoRepository.findAll();
        assertThat(sitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sito.class);
        Sito sito1 = new Sito();
        sito1.setId(1L);
        Sito sito2 = new Sito();
        sito2.setId(sito1.getId());
        assertThat(sito1).isEqualTo(sito2);
        sito2.setId(2L);
        assertThat(sito1).isNotEqualTo(sito2);
        sito1.setId(null);
        assertThat(sito1).isNotEqualTo(sito2);
    }
}
