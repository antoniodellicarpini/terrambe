package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcRaccAppz;
import com.terrambe.domain.RegOpcRacc;
import com.terrambe.repository.RegOpcRaccAppzRepository;
import com.terrambe.service.RegOpcRaccAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcRaccAppzCriteria;
import com.terrambe.service.RegOpcRaccAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcRaccAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcRaccAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcRaccAppzRepository regOpcRaccAppzRepository;

    @Autowired
    private RegOpcRaccAppzService regOpcRaccAppzService;

    @Autowired
    private RegOpcRaccAppzQueryService regOpcRaccAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcRaccAppzMockMvc;

    private RegOpcRaccAppz regOpcRaccAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcRaccAppzResource regOpcRaccAppzResource = new RegOpcRaccAppzResource(regOpcRaccAppzService, regOpcRaccAppzQueryService);
        this.restRegOpcRaccAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcRaccAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcRaccAppz createEntity(EntityManager em) {
        RegOpcRaccAppz regOpcRaccAppz = new RegOpcRaccAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcRaccAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcRaccAppz createUpdatedEntity(EntityManager em) {
        RegOpcRaccAppz regOpcRaccAppz = new RegOpcRaccAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcRaccAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcRaccAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcRaccAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcRaccAppzRepository.findAll().size();

        // Create the RegOpcRaccAppz
        restRegOpcRaccAppzMockMvc.perform(post("/api/reg-opc-racc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRaccAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcRaccAppz in the database
        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcRaccAppz testRegOpcRaccAppz = regOpcRaccAppzList.get(regOpcRaccAppzList.size() - 1);
        assertThat(testRegOpcRaccAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcRaccAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcRaccAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcRaccAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcRaccAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcRaccAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcRaccAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcRaccAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcRaccAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcRaccAppzRepository.findAll().size();

        // Create the RegOpcRaccAppz with an existing ID
        regOpcRaccAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcRaccAppzMockMvc.perform(post("/api/reg-opc-racc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRaccAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcRaccAppz in the database
        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcRaccAppzRepository.findAll().size();
        // set the field null
        regOpcRaccAppz.setIdAzienda(null);

        // Create the RegOpcRaccAppz, which fails.

        restRegOpcRaccAppzMockMvc.perform(post("/api/reg-opc-racc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRaccAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzs() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcRaccAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcRaccAppz() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get the regOpcRaccAppz
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs/{id}", regOpcRaccAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcRaccAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda is not null
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcRaccAppzList where idAzienda is null
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcRaccAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcRaccAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcRaccAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd is not null
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcRaccAppzList where idUnitaProd is null
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcRaccAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcRaccAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata is not null
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcRaccAppzList where percentLavorata is null
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcRaccAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcRaccAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento is not null
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcRaccAppzList where idAppezzamento is null
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcRaccAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcRaccAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali is not null
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcRaccAppzList where dataInizVali is null
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcRaccAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali is not null
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcRaccAppzList where dataFineVali is null
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcRaccAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcRaccAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator is not null
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcRaccAppzList where userIdCreator is null
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcRaccAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcRaccAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod is not null
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcRaccAppzList where userIdLastMod is null
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);

        // Get all the regOpcRaccAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcRaccAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcRaccAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcRaccAppzsByAppzToOpcRaccIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);
        RegOpcRacc appzToOpcRacc = RegOpcRaccResourceIT.createEntity(em);
        em.persist(appzToOpcRacc);
        em.flush();
        regOpcRaccAppz.setAppzToOpcRacc(appzToOpcRacc);
        regOpcRaccAppzRepository.saveAndFlush(regOpcRaccAppz);
        Long appzToOpcRaccId = appzToOpcRacc.getId();

        // Get all the regOpcRaccAppzList where appzToOpcRacc equals to appzToOpcRaccId
        defaultRegOpcRaccAppzShouldBeFound("appzToOpcRaccId.equals=" + appzToOpcRaccId);

        // Get all the regOpcRaccAppzList where appzToOpcRacc equals to appzToOpcRaccId + 1
        defaultRegOpcRaccAppzShouldNotBeFound("appzToOpcRaccId.equals=" + (appzToOpcRaccId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcRaccAppzShouldBeFound(String filter) throws Exception {
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcRaccAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcRaccAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcRaccAppz() throws Exception {
        // Get the regOpcRaccAppz
        restRegOpcRaccAppzMockMvc.perform(get("/api/reg-opc-racc-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcRaccAppz() throws Exception {
        // Initialize the database
        regOpcRaccAppzService.save(regOpcRaccAppz);

        int databaseSizeBeforeUpdate = regOpcRaccAppzRepository.findAll().size();

        // Update the regOpcRaccAppz
        RegOpcRaccAppz updatedRegOpcRaccAppz = regOpcRaccAppzRepository.findById(regOpcRaccAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcRaccAppz are not directly saved in db
        em.detach(updatedRegOpcRaccAppz);
        updatedRegOpcRaccAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcRaccAppzMockMvc.perform(put("/api/reg-opc-racc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcRaccAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcRaccAppz in the database
        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcRaccAppz testRegOpcRaccAppz = regOpcRaccAppzList.get(regOpcRaccAppzList.size() - 1);
        assertThat(testRegOpcRaccAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcRaccAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcRaccAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcRaccAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcRaccAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcRaccAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcRaccAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcRaccAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcRaccAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcRaccAppzRepository.findAll().size();

        // Create the RegOpcRaccAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcRaccAppzMockMvc.perform(put("/api/reg-opc-racc-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcRaccAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcRaccAppz in the database
        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcRaccAppz() throws Exception {
        // Initialize the database
        regOpcRaccAppzService.save(regOpcRaccAppz);

        int databaseSizeBeforeDelete = regOpcRaccAppzRepository.findAll().size();

        // Delete the regOpcRaccAppz
        restRegOpcRaccAppzMockMvc.perform(delete("/api/reg-opc-racc-appzs/{id}", regOpcRaccAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcRaccAppz> regOpcRaccAppzList = regOpcRaccAppzRepository.findAll();
        assertThat(regOpcRaccAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcRaccAppz.class);
        RegOpcRaccAppz regOpcRaccAppz1 = new RegOpcRaccAppz();
        regOpcRaccAppz1.setId(1L);
        RegOpcRaccAppz regOpcRaccAppz2 = new RegOpcRaccAppz();
        regOpcRaccAppz2.setId(regOpcRaccAppz1.getId());
        assertThat(regOpcRaccAppz1).isEqualTo(regOpcRaccAppz2);
        regOpcRaccAppz2.setId(2L);
        assertThat(regOpcRaccAppz1).isNotEqualTo(regOpcRaccAppz2);
        regOpcRaccAppz1.setId(null);
        assertThat(regOpcRaccAppz1).isNotEqualTo(regOpcRaccAppz2);
    }
}
