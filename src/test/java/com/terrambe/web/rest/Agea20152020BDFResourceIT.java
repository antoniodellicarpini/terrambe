package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.domain.ColtureBDF;
import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.repository.Agea20152020BDFRepository;
import com.terrambe.service.Agea20152020BDFService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.Agea20152020BDFCriteria;
import com.terrambe.service.Agea20152020BDFQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link Agea20152020BDFResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class Agea20152020BDFResourceIT {

    @Autowired
    private Agea20152020BDFRepository agea20152020BDFRepository;

    @Autowired
    private Agea20152020BDFService agea20152020BDFService;

    @Autowired
    private Agea20152020BDFQueryService agea20152020BDFQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAgea20152020BDFMockMvc;

    private Agea20152020BDF agea20152020BDF;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Agea20152020BDFResource agea20152020BDFResource = new Agea20152020BDFResource(agea20152020BDFService, agea20152020BDFQueryService);
        this.restAgea20152020BDFMockMvc = MockMvcBuilders.standaloneSetup(agea20152020BDFResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agea20152020BDF createEntity(EntityManager em) {
        Agea20152020BDF agea20152020BDF = new Agea20152020BDF();
        return agea20152020BDF;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agea20152020BDF createUpdatedEntity(EntityManager em) {
        Agea20152020BDF agea20152020BDF = new Agea20152020BDF();
        return agea20152020BDF;
    }

    @BeforeEach
    public void initTest() {
        agea20152020BDF = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgea20152020BDF() throws Exception {
        int databaseSizeBeforeCreate = agea20152020BDFRepository.findAll().size();

        // Create the Agea20152020BDF
        restAgea20152020BDFMockMvc.perform(post("/api/agea-20152020-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020BDF)))
            .andExpect(status().isCreated());

        // Validate the Agea20152020BDF in the database
        List<Agea20152020BDF> agea20152020BDFList = agea20152020BDFRepository.findAll();
        assertThat(agea20152020BDFList).hasSize(databaseSizeBeforeCreate + 1);
        Agea20152020BDF testAgea20152020BDF = agea20152020BDFList.get(agea20152020BDFList.size() - 1);
    }

    @Test
    @Transactional
    public void createAgea20152020BDFWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agea20152020BDFRepository.findAll().size();

        // Create the Agea20152020BDF with an existing ID
        agea20152020BDF.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgea20152020BDFMockMvc.perform(post("/api/agea-20152020-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020BDF)))
            .andExpect(status().isBadRequest());

        // Validate the Agea20152020BDF in the database
        List<Agea20152020BDF> agea20152020BDFList = agea20152020BDFRepository.findAll();
        assertThat(agea20152020BDFList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAgea20152020BDFS() throws Exception {
        // Initialize the database
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);

        // Get all the agea20152020BDFList
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agea20152020BDF.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getAgea20152020BDF() throws Exception {
        // Initialize the database
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);

        // Get the agea20152020BDF
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs/{id}", agea20152020BDF.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agea20152020BDF.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllAgea20152020BDFSByAgeaBdfToColtBdfIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);
        ColtureBDF ageaBdfToColtBdf = ColtureBDFResourceIT.createEntity(em);
        em.persist(ageaBdfToColtBdf);
        em.flush();
        agea20152020BDF.setAgeaBdfToColtBdf(ageaBdfToColtBdf);
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);
        Long ageaBdfToColtBdfId = ageaBdfToColtBdf.getId();

        // Get all the agea20152020BDFList where ageaBdfToColtBdf equals to ageaBdfToColtBdfId
        defaultAgea20152020BDFShouldBeFound("ageaBdfToColtBdfId.equals=" + ageaBdfToColtBdfId);

        // Get all the agea20152020BDFList where ageaBdfToColtBdf equals to ageaBdfToColtBdfId + 1
        defaultAgea20152020BDFShouldNotBeFound("ageaBdfToColtBdfId.equals=" + (ageaBdfToColtBdfId + 1));
    }


    @Test
    @Transactional
    public void getAllAgea20152020BDFSByBdfToMAtriceIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);
        Agea20152020Matrice bdfToMAtrice = Agea20152020MatriceResourceIT.createEntity(em);
        em.persist(bdfToMAtrice);
        em.flush();
        agea20152020BDF.setBdfToMAtrice(bdfToMAtrice);
        agea20152020BDFRepository.saveAndFlush(agea20152020BDF);
        Long bdfToMAtriceId = bdfToMAtrice.getId();

        // Get all the agea20152020BDFList where bdfToMAtrice equals to bdfToMAtriceId
        defaultAgea20152020BDFShouldBeFound("bdfToMAtriceId.equals=" + bdfToMAtriceId);

        // Get all the agea20152020BDFList where bdfToMAtrice equals to bdfToMAtriceId + 1
        defaultAgea20152020BDFShouldNotBeFound("bdfToMAtriceId.equals=" + (bdfToMAtriceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAgea20152020BDFShouldBeFound(String filter) throws Exception {
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agea20152020BDF.getId().intValue())));

        // Check, that the count call also returns 1
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAgea20152020BDFShouldNotBeFound(String filter) throws Exception {
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAgea20152020BDF() throws Exception {
        // Get the agea20152020BDF
        restAgea20152020BDFMockMvc.perform(get("/api/agea-20152020-bdfs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgea20152020BDF() throws Exception {
        // Initialize the database
        agea20152020BDFService.save(agea20152020BDF);

        int databaseSizeBeforeUpdate = agea20152020BDFRepository.findAll().size();

        // Update the agea20152020BDF
        Agea20152020BDF updatedAgea20152020BDF = agea20152020BDFRepository.findById(agea20152020BDF.getId()).get();
        // Disconnect from session so that the updates on updatedAgea20152020BDF are not directly saved in db
        em.detach(updatedAgea20152020BDF);

        restAgea20152020BDFMockMvc.perform(put("/api/agea-20152020-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgea20152020BDF)))
            .andExpect(status().isOk());

        // Validate the Agea20152020BDF in the database
        List<Agea20152020BDF> agea20152020BDFList = agea20152020BDFRepository.findAll();
        assertThat(agea20152020BDFList).hasSize(databaseSizeBeforeUpdate);
        Agea20152020BDF testAgea20152020BDF = agea20152020BDFList.get(agea20152020BDFList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingAgea20152020BDF() throws Exception {
        int databaseSizeBeforeUpdate = agea20152020BDFRepository.findAll().size();

        // Create the Agea20152020BDF

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgea20152020BDFMockMvc.perform(put("/api/agea-20152020-bdfs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020BDF)))
            .andExpect(status().isBadRequest());

        // Validate the Agea20152020BDF in the database
        List<Agea20152020BDF> agea20152020BDFList = agea20152020BDFRepository.findAll();
        assertThat(agea20152020BDFList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgea20152020BDF() throws Exception {
        // Initialize the database
        agea20152020BDFService.save(agea20152020BDF);

        int databaseSizeBeforeDelete = agea20152020BDFRepository.findAll().size();

        // Delete the agea20152020BDF
        restAgea20152020BDFMockMvc.perform(delete("/api/agea-20152020-bdfs/{id}", agea20152020BDF.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Agea20152020BDF> agea20152020BDFList = agea20152020BDFRepository.findAll();
        assertThat(agea20152020BDFList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Agea20152020BDF.class);
        Agea20152020BDF agea20152020BDF1 = new Agea20152020BDF();
        agea20152020BDF1.setId(1L);
        Agea20152020BDF agea20152020BDF2 = new Agea20152020BDF();
        agea20152020BDF2.setId(agea20152020BDF1.getId());
        assertThat(agea20152020BDF1).isEqualTo(agea20152020BDF2);
        agea20152020BDF2.setId(2L);
        assertThat(agea20152020BDF1).isNotEqualTo(agea20152020BDF2);
        agea20152020BDF1.setId(null);
        assertThat(agea20152020BDF1).isNotEqualTo(agea20152020BDF2);
    }
}
