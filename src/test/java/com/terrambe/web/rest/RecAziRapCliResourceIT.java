package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecAziRapCli;
import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.repository.RecAziRapCliRepository;
import com.terrambe.service.RecAziRapCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecAziRapCliCriteria;
import com.terrambe.service.RecAziRapCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecAziRapCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecAziRapCliResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecAziRapCliRepository recAziRapCliRepository;

    @Autowired
    private RecAziRapCliService recAziRapCliService;

    @Autowired
    private RecAziRapCliQueryService recAziRapCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecAziRapCliMockMvc;

    private RecAziRapCli recAziRapCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecAziRapCliResource recAziRapCliResource = new RecAziRapCliResource(recAziRapCliService, recAziRapCliQueryService);
        this.restRecAziRapCliMockMvc = MockMvcBuilders.standaloneSetup(recAziRapCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziRapCli createEntity(EntityManager em) {
        RecAziRapCli recAziRapCli = new RecAziRapCli()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recAziRapCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziRapCli createUpdatedEntity(EntityManager em) {
        RecAziRapCli recAziRapCli = new RecAziRapCli()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recAziRapCli;
    }

    @BeforeEach
    public void initTest() {
        recAziRapCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecAziRapCli() throws Exception {
        int databaseSizeBeforeCreate = recAziRapCliRepository.findAll().size();

        // Create the RecAziRapCli
        restRecAziRapCliMockMvc.perform(post("/api/rec-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRapCli)))
            .andExpect(status().isCreated());

        // Validate the RecAziRapCli in the database
        List<RecAziRapCli> recAziRapCliList = recAziRapCliRepository.findAll();
        assertThat(recAziRapCliList).hasSize(databaseSizeBeforeCreate + 1);
        RecAziRapCli testRecAziRapCli = recAziRapCliList.get(recAziRapCliList.size() - 1);
        assertThat(testRecAziRapCli.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecAziRapCli.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecAziRapCli.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecAziRapCli.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecAziRapCli.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecAziRapCli.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecAziRapCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecAziRapCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecAziRapCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecAziRapCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecAziRapCli.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecAziRapCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recAziRapCliRepository.findAll().size();

        // Create the RecAziRapCli with an existing ID
        recAziRapCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecAziRapCliMockMvc.perform(post("/api/rec-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziRapCli in the database
        List<RecAziRapCli> recAziRapCliList = recAziRapCliRepository.findAll();
        assertThat(recAziRapCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecAziRapClis() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecAziRapCli() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get the recAziRapCli
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis/{id}", recAziRapCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recAziRapCli.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where email equals to DEFAULT_EMAIL
        defaultRecAziRapCliShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recAziRapCliList where email equals to UPDATED_EMAIL
        defaultRecAziRapCliShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecAziRapCliShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recAziRapCliList where email equals to UPDATED_EMAIL
        defaultRecAziRapCliShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where email is not null
        defaultRecAziRapCliShouldBeFound("email.specified=true");

        // Get all the recAziRapCliList where email is null
        defaultRecAziRapCliShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where pec equals to DEFAULT_PEC
        defaultRecAziRapCliShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recAziRapCliList where pec equals to UPDATED_PEC
        defaultRecAziRapCliShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecAziRapCliShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recAziRapCliList where pec equals to UPDATED_PEC
        defaultRecAziRapCliShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where pec is not null
        defaultRecAziRapCliShouldBeFound("pec.specified=true");

        // Get all the recAziRapCliList where pec is null
        defaultRecAziRapCliShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where telefono equals to DEFAULT_TELEFONO
        defaultRecAziRapCliShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recAziRapCliList where telefono equals to UPDATED_TELEFONO
        defaultRecAziRapCliShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecAziRapCliShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recAziRapCliList where telefono equals to UPDATED_TELEFONO
        defaultRecAziRapCliShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where telefono is not null
        defaultRecAziRapCliShouldBeFound("telefono.specified=true");

        // Get all the recAziRapCliList where telefono is null
        defaultRecAziRapCliShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where fax equals to DEFAULT_FAX
        defaultRecAziRapCliShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recAziRapCliList where fax equals to UPDATED_FAX
        defaultRecAziRapCliShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecAziRapCliShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recAziRapCliList where fax equals to UPDATED_FAX
        defaultRecAziRapCliShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where fax is not null
        defaultRecAziRapCliShouldBeFound("fax.specified=true");

        // Get all the recAziRapCliList where fax is null
        defaultRecAziRapCliShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell equals to DEFAULT_CELL
        defaultRecAziRapCliShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recAziRapCliList where cell equals to UPDATED_CELL
        defaultRecAziRapCliShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecAziRapCliShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recAziRapCliList where cell equals to UPDATED_CELL
        defaultRecAziRapCliShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell is not null
        defaultRecAziRapCliShouldBeFound("cell.specified=true");

        // Get all the recAziRapCliList where cell is null
        defaultRecAziRapCliShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell2 equals to DEFAULT_CELL_2
        defaultRecAziRapCliShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recAziRapCliList where cell2 equals to UPDATED_CELL_2
        defaultRecAziRapCliShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecAziRapCliShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recAziRapCliList where cell2 equals to UPDATED_CELL_2
        defaultRecAziRapCliShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where cell2 is not null
        defaultRecAziRapCliShouldBeFound("cell2.specified=true");

        // Get all the recAziRapCliList where cell2 is null
        defaultRecAziRapCliShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali is not null
        defaultRecAziRapCliShouldBeFound("dataInizVali.specified=true");

        // Get all the recAziRapCliList where dataInizVali is null
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecAziRapCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziRapCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecAziRapCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali is not null
        defaultRecAziRapCliShouldBeFound("dataFineVali.specified=true");

        // Get all the recAziRapCliList where dataFineVali is null
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecAziRapCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziRapCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecAziRapCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator is not null
        defaultRecAziRapCliShouldBeFound("userIdCreator.specified=true");

        // Get all the recAziRapCliList where userIdCreator is null
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecAziRapCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziRapCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecAziRapCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod is not null
        defaultRecAziRapCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the recAziRapCliList where userIdLastMod is null
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziRapClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);

        // Get all the recAziRapCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziRapCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecAziRapCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecAziRapClisByRecToCliRapIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziRapCliRepository.saveAndFlush(recAziRapCli);
        AziRappresentanteCli recToCliRap = AziRappresentanteCliResourceIT.createEntity(em);
        em.persist(recToCliRap);
        em.flush();
        recAziRapCli.setRecToCliRap(recToCliRap);
        recAziRapCliRepository.saveAndFlush(recAziRapCli);
        Long recToCliRapId = recToCliRap.getId();

        // Get all the recAziRapCliList where recToCliRap equals to recToCliRapId
        defaultRecAziRapCliShouldBeFound("recToCliRapId.equals=" + recToCliRapId);

        // Get all the recAziRapCliList where recToCliRap equals to recToCliRapId + 1
        defaultRecAziRapCliShouldNotBeFound("recToCliRapId.equals=" + (recToCliRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecAziRapCliShouldBeFound(String filter) throws Exception {
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecAziRapCliShouldNotBeFound(String filter) throws Exception {
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecAziRapCli() throws Exception {
        // Get the recAziRapCli
        restRecAziRapCliMockMvc.perform(get("/api/rec-azi-rap-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecAziRapCli() throws Exception {
        // Initialize the database
        recAziRapCliService.save(recAziRapCli);

        int databaseSizeBeforeUpdate = recAziRapCliRepository.findAll().size();

        // Update the recAziRapCli
        RecAziRapCli updatedRecAziRapCli = recAziRapCliRepository.findById(recAziRapCli.getId()).get();
        // Disconnect from session so that the updates on updatedRecAziRapCli are not directly saved in db
        em.detach(updatedRecAziRapCli);
        updatedRecAziRapCli
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecAziRapCliMockMvc.perform(put("/api/rec-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecAziRapCli)))
            .andExpect(status().isOk());

        // Validate the RecAziRapCli in the database
        List<RecAziRapCli> recAziRapCliList = recAziRapCliRepository.findAll();
        assertThat(recAziRapCliList).hasSize(databaseSizeBeforeUpdate);
        RecAziRapCli testRecAziRapCli = recAziRapCliList.get(recAziRapCliList.size() - 1);
        assertThat(testRecAziRapCli.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecAziRapCli.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecAziRapCli.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecAziRapCli.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecAziRapCli.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecAziRapCli.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecAziRapCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecAziRapCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecAziRapCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecAziRapCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecAziRapCli.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecAziRapCli() throws Exception {
        int databaseSizeBeforeUpdate = recAziRapCliRepository.findAll().size();

        // Create the RecAziRapCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecAziRapCliMockMvc.perform(put("/api/rec-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziRapCli in the database
        List<RecAziRapCli> recAziRapCliList = recAziRapCliRepository.findAll();
        assertThat(recAziRapCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecAziRapCli() throws Exception {
        // Initialize the database
        recAziRapCliService.save(recAziRapCli);

        int databaseSizeBeforeDelete = recAziRapCliRepository.findAll().size();

        // Delete the recAziRapCli
        restRecAziRapCliMockMvc.perform(delete("/api/rec-azi-rap-clis/{id}", recAziRapCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecAziRapCli> recAziRapCliList = recAziRapCliRepository.findAll();
        assertThat(recAziRapCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecAziRapCli.class);
        RecAziRapCli recAziRapCli1 = new RecAziRapCli();
        recAziRapCli1.setId(1L);
        RecAziRapCli recAziRapCli2 = new RecAziRapCli();
        recAziRapCli2.setId(recAziRapCli1.getId());
        assertThat(recAziRapCli1).isEqualTo(recAziRapCli2);
        recAziRapCli2.setId(2L);
        assertThat(recAziRapCli1).isNotEqualTo(recAziRapCli2);
        recAziRapCli1.setId(null);
        assertThat(recAziRapCli1).isNotEqualTo(recAziRapCli2);
    }
}
