package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AtrMarchio;
import com.terrambe.domain.AtrTipologia;
import com.terrambe.repository.AtrMarchioRepository;
import com.terrambe.service.AtrMarchioService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AtrMarchioCriteria;
import com.terrambe.service.AtrMarchioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AtrMarchioResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AtrMarchioResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AtrMarchioRepository atrMarchioRepository;

    @Mock
    private AtrMarchioRepository atrMarchioRepositoryMock;

    @Mock
    private AtrMarchioService atrMarchioServiceMock;

    @Autowired
    private AtrMarchioService atrMarchioService;

    @Autowired
    private AtrMarchioQueryService atrMarchioQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAtrMarchioMockMvc;

    private AtrMarchio atrMarchio;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtrMarchioResource atrMarchioResource = new AtrMarchioResource(atrMarchioService, atrMarchioQueryService);
        this.restAtrMarchioMockMvc = MockMvcBuilders.standaloneSetup(atrMarchioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrMarchio createEntity(EntityManager em) {
        AtrMarchio atrMarchio = new AtrMarchio()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return atrMarchio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrMarchio createUpdatedEntity(EntityManager em) {
        AtrMarchio atrMarchio = new AtrMarchio()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return atrMarchio;
    }

    @BeforeEach
    public void initTest() {
        atrMarchio = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtrMarchio() throws Exception {
        int databaseSizeBeforeCreate = atrMarchioRepository.findAll().size();

        // Create the AtrMarchio
        restAtrMarchioMockMvc.perform(post("/api/atr-marchios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrMarchio)))
            .andExpect(status().isCreated());

        // Validate the AtrMarchio in the database
        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeCreate + 1);
        AtrMarchio testAtrMarchio = atrMarchioList.get(atrMarchioList.size() - 1);
        assertThat(testAtrMarchio.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testAtrMarchio.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAtrMarchio.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAtrMarchio.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAtrMarchio.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAtrMarchio.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAtrMarchioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atrMarchioRepository.findAll().size();

        // Create the AtrMarchio with an existing ID
        atrMarchio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtrMarchioMockMvc.perform(post("/api/atr-marchios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrMarchio)))
            .andExpect(status().isBadRequest());

        // Validate the AtrMarchio in the database
        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = atrMarchioRepository.findAll().size();
        // set the field null
        atrMarchio.setDescrizione(null);

        // Create the AtrMarchio, which fails.

        restAtrMarchioMockMvc.perform(post("/api/atr-marchios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrMarchio)))
            .andExpect(status().isBadRequest());

        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAtrMarchios() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrMarchio.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAtrMarchiosWithEagerRelationshipsIsEnabled() throws Exception {
        AtrMarchioResource atrMarchioResource = new AtrMarchioResource(atrMarchioServiceMock, atrMarchioQueryService);
        when(atrMarchioServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restAtrMarchioMockMvc = MockMvcBuilders.standaloneSetup(atrMarchioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAtrMarchioMockMvc.perform(get("/api/atr-marchios?eagerload=true"))
        .andExpect(status().isOk());

        verify(atrMarchioServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAtrMarchiosWithEagerRelationshipsIsNotEnabled() throws Exception {
        AtrMarchioResource atrMarchioResource = new AtrMarchioResource(atrMarchioServiceMock, atrMarchioQueryService);
            when(atrMarchioServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restAtrMarchioMockMvc = MockMvcBuilders.standaloneSetup(atrMarchioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAtrMarchioMockMvc.perform(get("/api/atr-marchios?eagerload=true"))
        .andExpect(status().isOk());

            verify(atrMarchioServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAtrMarchio() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get the atrMarchio
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios/{id}", atrMarchio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(atrMarchio.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultAtrMarchioShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the atrMarchioList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAtrMarchioShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultAtrMarchioShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the atrMarchioList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAtrMarchioShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where descrizione is not null
        defaultAtrMarchioShouldBeFound("descrizione.specified=true");

        // Get all the atrMarchioList where descrizione is null
        defaultAtrMarchioShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali is not null
        defaultAtrMarchioShouldBeFound("dataInizVali.specified=true");

        // Get all the atrMarchioList where dataInizVali is null
        defaultAtrMarchioShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAtrMarchioShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrMarchioList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAtrMarchioShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali is not null
        defaultAtrMarchioShouldBeFound("dataFineVali.specified=true");

        // Get all the atrMarchioList where dataFineVali is null
        defaultAtrMarchioShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAtrMarchioShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrMarchioList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAtrMarchioShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator is not null
        defaultAtrMarchioShouldBeFound("userIdCreator.specified=true");

        // Get all the atrMarchioList where userIdCreator is null
        defaultAtrMarchioShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAtrMarchioShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrMarchioList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAtrMarchioShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod is not null
        defaultAtrMarchioShouldBeFound("userIdLastMod.specified=true");

        // Get all the atrMarchioList where userIdLastMod is null
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrMarchiosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);

        // Get all the atrMarchioList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAtrMarchioShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrMarchioList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAtrMarchioShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAtrMarchiosByAtrTipologiaIsEqualToSomething() throws Exception {
        // Initialize the database
        atrMarchioRepository.saveAndFlush(atrMarchio);
        AtrTipologia atrTipologia = AtrTipologiaResourceIT.createEntity(em);
        em.persist(atrTipologia);
        em.flush();
        atrMarchio.addAtrTipologia(atrTipologia);
        atrMarchioRepository.saveAndFlush(atrMarchio);
        Long atrTipologiaId = atrTipologia.getId();

        // Get all the atrMarchioList where atrTipologia equals to atrTipologiaId
        defaultAtrMarchioShouldBeFound("atrTipologiaId.equals=" + atrTipologiaId);

        // Get all the atrMarchioList where atrTipologia equals to atrTipologiaId + 1
        defaultAtrMarchioShouldNotBeFound("atrTipologiaId.equals=" + (atrTipologiaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAtrMarchioShouldBeFound(String filter) throws Exception {
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrMarchio.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAtrMarchioShouldNotBeFound(String filter) throws Exception {
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAtrMarchio() throws Exception {
        // Get the atrMarchio
        restAtrMarchioMockMvc.perform(get("/api/atr-marchios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtrMarchio() throws Exception {
        // Initialize the database
        atrMarchioService.save(atrMarchio);

        int databaseSizeBeforeUpdate = atrMarchioRepository.findAll().size();

        // Update the atrMarchio
        AtrMarchio updatedAtrMarchio = atrMarchioRepository.findById(atrMarchio.getId()).get();
        // Disconnect from session so that the updates on updatedAtrMarchio are not directly saved in db
        em.detach(updatedAtrMarchio);
        updatedAtrMarchio
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAtrMarchioMockMvc.perform(put("/api/atr-marchios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAtrMarchio)))
            .andExpect(status().isOk());

        // Validate the AtrMarchio in the database
        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeUpdate);
        AtrMarchio testAtrMarchio = atrMarchioList.get(atrMarchioList.size() - 1);
        assertThat(testAtrMarchio.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testAtrMarchio.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAtrMarchio.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAtrMarchio.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAtrMarchio.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAtrMarchio.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAtrMarchio() throws Exception {
        int databaseSizeBeforeUpdate = atrMarchioRepository.findAll().size();

        // Create the AtrMarchio

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtrMarchioMockMvc.perform(put("/api/atr-marchios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrMarchio)))
            .andExpect(status().isBadRequest());

        // Validate the AtrMarchio in the database
        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAtrMarchio() throws Exception {
        // Initialize the database
        atrMarchioService.save(atrMarchio);

        int databaseSizeBeforeDelete = atrMarchioRepository.findAll().size();

        // Delete the atrMarchio
        restAtrMarchioMockMvc.perform(delete("/api/atr-marchios/{id}", atrMarchio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AtrMarchio> atrMarchioList = atrMarchioRepository.findAll();
        assertThat(atrMarchioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtrMarchio.class);
        AtrMarchio atrMarchio1 = new AtrMarchio();
        atrMarchio1.setId(1L);
        AtrMarchio atrMarchio2 = new AtrMarchio();
        atrMarchio2.setId(atrMarchio1.getId());
        assertThat(atrMarchio1).isEqualTo(atrMarchio2);
        atrMarchio2.setId(2L);
        assertThat(atrMarchio1).isNotEqualTo(atrMarchio2);
        atrMarchio1.setId(null);
        assertThat(atrMarchio1).isNotEqualTo(atrMarchio2);
    }
}
