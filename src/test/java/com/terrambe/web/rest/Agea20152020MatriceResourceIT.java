package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.repository.Agea20152020MatriceRepository;
import com.terrambe.service.Agea20152020MatriceService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.Agea20152020MatriceCriteria;
import com.terrambe.service.Agea20152020MatriceQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link Agea20152020MatriceResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class Agea20152020MatriceResourceIT {

    private static final String DEFAULT_DESC_COLTURA = "AAAAAAAAAA";
    private static final String UPDATED_DESC_COLTURA = "BBBBBBBBBB";

    private static final String DEFAULT_COD_MACRO_USO = "AAAAAAAAAA";
    private static final String UPDATED_COD_MACRO_USO = "BBBBBBBBBB";

    private static final String DEFAULT_MACRO_USO = "AAAAAAAAAA";
    private static final String UPDATED_MACRO_USO = "BBBBBBBBBB";

    private static final String DEFAULT_COD_OCCUPAZIONE_SUOLO = "AAAAAAAAAA";
    private static final String UPDATED_COD_OCCUPAZIONE_SUOLO = "BBBBBBBBBB";

    private static final String DEFAULT_OCCUPAZIONE_SUOLO = "AAAAAAAAAA";
    private static final String UPDATED_OCCUPAZIONE_SUOLO = "BBBBBBBBBB";

    private static final String DEFAULT_COD_DESTINAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_COD_DESTINAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_DESTINAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESTINAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_COD_USO = "AAAAAAAAAA";
    private static final String UPDATED_COD_USO = "BBBBBBBBBB";

    private static final String DEFAULT_USO = "AAAAAAAAAA";
    private static final String UPDATED_USO = "BBBBBBBBBB";

    private static final String DEFAULT_COD_QUALITA = "AAAAAAAAAA";
    private static final String UPDATED_COD_QUALITA = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITA = "AAAAAAAAAA";
    private static final String UPDATED_QUALITA = "BBBBBBBBBB";

    private static final String DEFAULT_COD_VARIETA = "AAAAAAAAAA";
    private static final String UPDATED_COD_VARIETA = "BBBBBBBBBB";

    private static final String DEFAULT_VARIETA = "AAAAAAAAAA";
    private static final String UPDATED_VARIETA = "BBBBBBBBBB";

    private static final String DEFAULT_COD_FAMIGLIA = "AAAAAAAAAA";
    private static final String UPDATED_COD_FAMIGLIA = "BBBBBBBBBB";

    private static final String DEFAULT_FAMIGLIA = "AAAAAAAAAA";
    private static final String UPDATED_FAMIGLIA = "BBBBBBBBBB";

    private static final String DEFAULT_COD_GENERE = "AAAAAAAAAA";
    private static final String UPDATED_COD_GENERE = "BBBBBBBBBB";

    private static final String DEFAULT_GENERE = "AAAAAAAAAA";
    private static final String UPDATED_GENERE = "BBBBBBBBBB";

    private static final String DEFAULT_COD_SPECIE = "AAAAAAAAAA";
    private static final String UPDATED_COD_SPECIE = "BBBBBBBBBB";

    private static final String DEFAULT_SPECIE = "AAAAAAAAAA";
    private static final String UPDATED_SPECIE = "BBBBBBBBBB";

    private static final String DEFAULT_VERSIONE_INSERIMENTO = "AAAAAAAAAA";
    private static final String UPDATED_VERSIONE_INSERIMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_COMPOSTO_AGEA = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_COMPOSTO_AGEA = "BBBBBBBBBB";

    @Autowired
    private Agea20152020MatriceRepository agea20152020MatriceRepository;

    @Autowired
    private Agea20152020MatriceService agea20152020MatriceService;

    @Autowired
    private Agea20152020MatriceQueryService agea20152020MatriceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAgea20152020MatriceMockMvc;

    private Agea20152020Matrice agea20152020Matrice;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Agea20152020MatriceResource agea20152020MatriceResource = new Agea20152020MatriceResource(agea20152020MatriceService, agea20152020MatriceQueryService);
        this.restAgea20152020MatriceMockMvc = MockMvcBuilders.standaloneSetup(agea20152020MatriceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agea20152020Matrice createEntity(EntityManager em) {
        Agea20152020Matrice agea20152020Matrice = new Agea20152020Matrice()
            .descColtura(DEFAULT_DESC_COLTURA)
            .codMacroUso(DEFAULT_COD_MACRO_USO)
            .macroUso(DEFAULT_MACRO_USO)
            .codOccupazioneSuolo(DEFAULT_COD_OCCUPAZIONE_SUOLO)
            .occupazioneSuolo(DEFAULT_OCCUPAZIONE_SUOLO)
            .codDestinazione(DEFAULT_COD_DESTINAZIONE)
            .destinazione(DEFAULT_DESTINAZIONE)
            .codUso(DEFAULT_COD_USO)
            .uso(DEFAULT_USO)
            .codQualita(DEFAULT_COD_QUALITA)
            .qualita(DEFAULT_QUALITA)
            .codVarieta(DEFAULT_COD_VARIETA)
            .varieta(DEFAULT_VARIETA)
            .codFamiglia(DEFAULT_COD_FAMIGLIA)
            .famiglia(DEFAULT_FAMIGLIA)
            .codGenere(DEFAULT_COD_GENERE)
            .genere(DEFAULT_GENERE)
            .codSpecie(DEFAULT_COD_SPECIE)
            .specie(DEFAULT_SPECIE)
            .versioneInserimento(DEFAULT_VERSIONE_INSERIMENTO)
            .codiceCompostoAgea(DEFAULT_CODICE_COMPOSTO_AGEA);
        return agea20152020Matrice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agea20152020Matrice createUpdatedEntity(EntityManager em) {
        Agea20152020Matrice agea20152020Matrice = new Agea20152020Matrice()
            .descColtura(UPDATED_DESC_COLTURA)
            .codMacroUso(UPDATED_COD_MACRO_USO)
            .macroUso(UPDATED_MACRO_USO)
            .codOccupazioneSuolo(UPDATED_COD_OCCUPAZIONE_SUOLO)
            .occupazioneSuolo(UPDATED_OCCUPAZIONE_SUOLO)
            .codDestinazione(UPDATED_COD_DESTINAZIONE)
            .destinazione(UPDATED_DESTINAZIONE)
            .codUso(UPDATED_COD_USO)
            .uso(UPDATED_USO)
            .codQualita(UPDATED_COD_QUALITA)
            .qualita(UPDATED_QUALITA)
            .codVarieta(UPDATED_COD_VARIETA)
            .varieta(UPDATED_VARIETA)
            .codFamiglia(UPDATED_COD_FAMIGLIA)
            .famiglia(UPDATED_FAMIGLIA)
            .codGenere(UPDATED_COD_GENERE)
            .genere(UPDATED_GENERE)
            .codSpecie(UPDATED_COD_SPECIE)
            .specie(UPDATED_SPECIE)
            .versioneInserimento(UPDATED_VERSIONE_INSERIMENTO)
            .codiceCompostoAgea(UPDATED_CODICE_COMPOSTO_AGEA);
        return agea20152020Matrice;
    }

    @BeforeEach
    public void initTest() {
        agea20152020Matrice = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgea20152020Matrice() throws Exception {
        int databaseSizeBeforeCreate = agea20152020MatriceRepository.findAll().size();

        // Create the Agea20152020Matrice
        restAgea20152020MatriceMockMvc.perform(post("/api/agea-20152020-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020Matrice)))
            .andExpect(status().isCreated());

        // Validate the Agea20152020Matrice in the database
        List<Agea20152020Matrice> agea20152020MatriceList = agea20152020MatriceRepository.findAll();
        assertThat(agea20152020MatriceList).hasSize(databaseSizeBeforeCreate + 1);
        Agea20152020Matrice testAgea20152020Matrice = agea20152020MatriceList.get(agea20152020MatriceList.size() - 1);
        assertThat(testAgea20152020Matrice.getDescColtura()).isEqualTo(DEFAULT_DESC_COLTURA);
        assertThat(testAgea20152020Matrice.getCodMacroUso()).isEqualTo(DEFAULT_COD_MACRO_USO);
        assertThat(testAgea20152020Matrice.getMacroUso()).isEqualTo(DEFAULT_MACRO_USO);
        assertThat(testAgea20152020Matrice.getCodOccupazioneSuolo()).isEqualTo(DEFAULT_COD_OCCUPAZIONE_SUOLO);
        assertThat(testAgea20152020Matrice.getOccupazioneSuolo()).isEqualTo(DEFAULT_OCCUPAZIONE_SUOLO);
        assertThat(testAgea20152020Matrice.getCodDestinazione()).isEqualTo(DEFAULT_COD_DESTINAZIONE);
        assertThat(testAgea20152020Matrice.getDestinazione()).isEqualTo(DEFAULT_DESTINAZIONE);
        assertThat(testAgea20152020Matrice.getCodUso()).isEqualTo(DEFAULT_COD_USO);
        assertThat(testAgea20152020Matrice.getUso()).isEqualTo(DEFAULT_USO);
        assertThat(testAgea20152020Matrice.getCodQualita()).isEqualTo(DEFAULT_COD_QUALITA);
        assertThat(testAgea20152020Matrice.getQualita()).isEqualTo(DEFAULT_QUALITA);
        assertThat(testAgea20152020Matrice.getCodVarieta()).isEqualTo(DEFAULT_COD_VARIETA);
        assertThat(testAgea20152020Matrice.getVarieta()).isEqualTo(DEFAULT_VARIETA);
        assertThat(testAgea20152020Matrice.getCodFamiglia()).isEqualTo(DEFAULT_COD_FAMIGLIA);
        assertThat(testAgea20152020Matrice.getFamiglia()).isEqualTo(DEFAULT_FAMIGLIA);
        assertThat(testAgea20152020Matrice.getCodGenere()).isEqualTo(DEFAULT_COD_GENERE);
        assertThat(testAgea20152020Matrice.getGenere()).isEqualTo(DEFAULT_GENERE);
        assertThat(testAgea20152020Matrice.getCodSpecie()).isEqualTo(DEFAULT_COD_SPECIE);
        assertThat(testAgea20152020Matrice.getSpecie()).isEqualTo(DEFAULT_SPECIE);
        assertThat(testAgea20152020Matrice.getVersioneInserimento()).isEqualTo(DEFAULT_VERSIONE_INSERIMENTO);
        assertThat(testAgea20152020Matrice.getCodiceCompostoAgea()).isEqualTo(DEFAULT_CODICE_COMPOSTO_AGEA);
    }

    @Test
    @Transactional
    public void createAgea20152020MatriceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agea20152020MatriceRepository.findAll().size();

        // Create the Agea20152020Matrice with an existing ID
        agea20152020Matrice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgea20152020MatriceMockMvc.perform(post("/api/agea-20152020-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020Matrice)))
            .andExpect(status().isBadRequest());

        // Validate the Agea20152020Matrice in the database
        List<Agea20152020Matrice> agea20152020MatriceList = agea20152020MatriceRepository.findAll();
        assertThat(agea20152020MatriceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAgea20152020Matrices() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agea20152020Matrice.getId().intValue())))
            .andExpect(jsonPath("$.[*].descColtura").value(hasItem(DEFAULT_DESC_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].codMacroUso").value(hasItem(DEFAULT_COD_MACRO_USO.toString())))
            .andExpect(jsonPath("$.[*].macroUso").value(hasItem(DEFAULT_MACRO_USO.toString())))
            .andExpect(jsonPath("$.[*].codOccupazioneSuolo").value(hasItem(DEFAULT_COD_OCCUPAZIONE_SUOLO.toString())))
            .andExpect(jsonPath("$.[*].occupazioneSuolo").value(hasItem(DEFAULT_OCCUPAZIONE_SUOLO.toString())))
            .andExpect(jsonPath("$.[*].codDestinazione").value(hasItem(DEFAULT_COD_DESTINAZIONE.toString())))
            .andExpect(jsonPath("$.[*].destinazione").value(hasItem(DEFAULT_DESTINAZIONE.toString())))
            .andExpect(jsonPath("$.[*].codUso").value(hasItem(DEFAULT_COD_USO.toString())))
            .andExpect(jsonPath("$.[*].uso").value(hasItem(DEFAULT_USO.toString())))
            .andExpect(jsonPath("$.[*].codQualita").value(hasItem(DEFAULT_COD_QUALITA.toString())))
            .andExpect(jsonPath("$.[*].qualita").value(hasItem(DEFAULT_QUALITA.toString())))
            .andExpect(jsonPath("$.[*].codVarieta").value(hasItem(DEFAULT_COD_VARIETA.toString())))
            .andExpect(jsonPath("$.[*].varieta").value(hasItem(DEFAULT_VARIETA.toString())))
            .andExpect(jsonPath("$.[*].codFamiglia").value(hasItem(DEFAULT_COD_FAMIGLIA.toString())))
            .andExpect(jsonPath("$.[*].famiglia").value(hasItem(DEFAULT_FAMIGLIA.toString())))
            .andExpect(jsonPath("$.[*].codGenere").value(hasItem(DEFAULT_COD_GENERE.toString())))
            .andExpect(jsonPath("$.[*].genere").value(hasItem(DEFAULT_GENERE.toString())))
            .andExpect(jsonPath("$.[*].codSpecie").value(hasItem(DEFAULT_COD_SPECIE.toString())))
            .andExpect(jsonPath("$.[*].specie").value(hasItem(DEFAULT_SPECIE.toString())))
            .andExpect(jsonPath("$.[*].versioneInserimento").value(hasItem(DEFAULT_VERSIONE_INSERIMENTO.toString())))
            .andExpect(jsonPath("$.[*].codiceCompostoAgea").value(hasItem(DEFAULT_CODICE_COMPOSTO_AGEA.toString())));
    }
    
    @Test
    @Transactional
    public void getAgea20152020Matrice() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get the agea20152020Matrice
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices/{id}", agea20152020Matrice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agea20152020Matrice.getId().intValue()))
            .andExpect(jsonPath("$.descColtura").value(DEFAULT_DESC_COLTURA.toString()))
            .andExpect(jsonPath("$.codMacroUso").value(DEFAULT_COD_MACRO_USO.toString()))
            .andExpect(jsonPath("$.macroUso").value(DEFAULT_MACRO_USO.toString()))
            .andExpect(jsonPath("$.codOccupazioneSuolo").value(DEFAULT_COD_OCCUPAZIONE_SUOLO.toString()))
            .andExpect(jsonPath("$.occupazioneSuolo").value(DEFAULT_OCCUPAZIONE_SUOLO.toString()))
            .andExpect(jsonPath("$.codDestinazione").value(DEFAULT_COD_DESTINAZIONE.toString()))
            .andExpect(jsonPath("$.destinazione").value(DEFAULT_DESTINAZIONE.toString()))
            .andExpect(jsonPath("$.codUso").value(DEFAULT_COD_USO.toString()))
            .andExpect(jsonPath("$.uso").value(DEFAULT_USO.toString()))
            .andExpect(jsonPath("$.codQualita").value(DEFAULT_COD_QUALITA.toString()))
            .andExpect(jsonPath("$.qualita").value(DEFAULT_QUALITA.toString()))
            .andExpect(jsonPath("$.codVarieta").value(DEFAULT_COD_VARIETA.toString()))
            .andExpect(jsonPath("$.varieta").value(DEFAULT_VARIETA.toString()))
            .andExpect(jsonPath("$.codFamiglia").value(DEFAULT_COD_FAMIGLIA.toString()))
            .andExpect(jsonPath("$.famiglia").value(DEFAULT_FAMIGLIA.toString()))
            .andExpect(jsonPath("$.codGenere").value(DEFAULT_COD_GENERE.toString()))
            .andExpect(jsonPath("$.genere").value(DEFAULT_GENERE.toString()))
            .andExpect(jsonPath("$.codSpecie").value(DEFAULT_COD_SPECIE.toString()))
            .andExpect(jsonPath("$.specie").value(DEFAULT_SPECIE.toString()))
            .andExpect(jsonPath("$.versioneInserimento").value(DEFAULT_VERSIONE_INSERIMENTO.toString()))
            .andExpect(jsonPath("$.codiceCompostoAgea").value(DEFAULT_CODICE_COMPOSTO_AGEA.toString()));
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDescColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where descColtura equals to DEFAULT_DESC_COLTURA
        defaultAgea20152020MatriceShouldBeFound("descColtura.equals=" + DEFAULT_DESC_COLTURA);

        // Get all the agea20152020MatriceList where descColtura equals to UPDATED_DESC_COLTURA
        defaultAgea20152020MatriceShouldNotBeFound("descColtura.equals=" + UPDATED_DESC_COLTURA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDescColturaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where descColtura in DEFAULT_DESC_COLTURA or UPDATED_DESC_COLTURA
        defaultAgea20152020MatriceShouldBeFound("descColtura.in=" + DEFAULT_DESC_COLTURA + "," + UPDATED_DESC_COLTURA);

        // Get all the agea20152020MatriceList where descColtura equals to UPDATED_DESC_COLTURA
        defaultAgea20152020MatriceShouldNotBeFound("descColtura.in=" + UPDATED_DESC_COLTURA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDescColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where descColtura is not null
        defaultAgea20152020MatriceShouldBeFound("descColtura.specified=true");

        // Get all the agea20152020MatriceList where descColtura is null
        defaultAgea20152020MatriceShouldNotBeFound("descColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodMacroUsoIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codMacroUso equals to DEFAULT_COD_MACRO_USO
        defaultAgea20152020MatriceShouldBeFound("codMacroUso.equals=" + DEFAULT_COD_MACRO_USO);

        // Get all the agea20152020MatriceList where codMacroUso equals to UPDATED_COD_MACRO_USO
        defaultAgea20152020MatriceShouldNotBeFound("codMacroUso.equals=" + UPDATED_COD_MACRO_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodMacroUsoIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codMacroUso in DEFAULT_COD_MACRO_USO or UPDATED_COD_MACRO_USO
        defaultAgea20152020MatriceShouldBeFound("codMacroUso.in=" + DEFAULT_COD_MACRO_USO + "," + UPDATED_COD_MACRO_USO);

        // Get all the agea20152020MatriceList where codMacroUso equals to UPDATED_COD_MACRO_USO
        defaultAgea20152020MatriceShouldNotBeFound("codMacroUso.in=" + UPDATED_COD_MACRO_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodMacroUsoIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codMacroUso is not null
        defaultAgea20152020MatriceShouldBeFound("codMacroUso.specified=true");

        // Get all the agea20152020MatriceList where codMacroUso is null
        defaultAgea20152020MatriceShouldNotBeFound("codMacroUso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByMacroUsoIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where macroUso equals to DEFAULT_MACRO_USO
        defaultAgea20152020MatriceShouldBeFound("macroUso.equals=" + DEFAULT_MACRO_USO);

        // Get all the agea20152020MatriceList where macroUso equals to UPDATED_MACRO_USO
        defaultAgea20152020MatriceShouldNotBeFound("macroUso.equals=" + UPDATED_MACRO_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByMacroUsoIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where macroUso in DEFAULT_MACRO_USO or UPDATED_MACRO_USO
        defaultAgea20152020MatriceShouldBeFound("macroUso.in=" + DEFAULT_MACRO_USO + "," + UPDATED_MACRO_USO);

        // Get all the agea20152020MatriceList where macroUso equals to UPDATED_MACRO_USO
        defaultAgea20152020MatriceShouldNotBeFound("macroUso.in=" + UPDATED_MACRO_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByMacroUsoIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where macroUso is not null
        defaultAgea20152020MatriceShouldBeFound("macroUso.specified=true");

        // Get all the agea20152020MatriceList where macroUso is null
        defaultAgea20152020MatriceShouldNotBeFound("macroUso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodOccupazioneSuoloIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codOccupazioneSuolo equals to DEFAULT_COD_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldBeFound("codOccupazioneSuolo.equals=" + DEFAULT_COD_OCCUPAZIONE_SUOLO);

        // Get all the agea20152020MatriceList where codOccupazioneSuolo equals to UPDATED_COD_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldNotBeFound("codOccupazioneSuolo.equals=" + UPDATED_COD_OCCUPAZIONE_SUOLO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodOccupazioneSuoloIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codOccupazioneSuolo in DEFAULT_COD_OCCUPAZIONE_SUOLO or UPDATED_COD_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldBeFound("codOccupazioneSuolo.in=" + DEFAULT_COD_OCCUPAZIONE_SUOLO + "," + UPDATED_COD_OCCUPAZIONE_SUOLO);

        // Get all the agea20152020MatriceList where codOccupazioneSuolo equals to UPDATED_COD_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldNotBeFound("codOccupazioneSuolo.in=" + UPDATED_COD_OCCUPAZIONE_SUOLO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodOccupazioneSuoloIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codOccupazioneSuolo is not null
        defaultAgea20152020MatriceShouldBeFound("codOccupazioneSuolo.specified=true");

        // Get all the agea20152020MatriceList where codOccupazioneSuolo is null
        defaultAgea20152020MatriceShouldNotBeFound("codOccupazioneSuolo.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByOccupazioneSuoloIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where occupazioneSuolo equals to DEFAULT_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldBeFound("occupazioneSuolo.equals=" + DEFAULT_OCCUPAZIONE_SUOLO);

        // Get all the agea20152020MatriceList where occupazioneSuolo equals to UPDATED_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldNotBeFound("occupazioneSuolo.equals=" + UPDATED_OCCUPAZIONE_SUOLO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByOccupazioneSuoloIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where occupazioneSuolo in DEFAULT_OCCUPAZIONE_SUOLO or UPDATED_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldBeFound("occupazioneSuolo.in=" + DEFAULT_OCCUPAZIONE_SUOLO + "," + UPDATED_OCCUPAZIONE_SUOLO);

        // Get all the agea20152020MatriceList where occupazioneSuolo equals to UPDATED_OCCUPAZIONE_SUOLO
        defaultAgea20152020MatriceShouldNotBeFound("occupazioneSuolo.in=" + UPDATED_OCCUPAZIONE_SUOLO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByOccupazioneSuoloIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where occupazioneSuolo is not null
        defaultAgea20152020MatriceShouldBeFound("occupazioneSuolo.specified=true");

        // Get all the agea20152020MatriceList where occupazioneSuolo is null
        defaultAgea20152020MatriceShouldNotBeFound("occupazioneSuolo.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodDestinazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codDestinazione equals to DEFAULT_COD_DESTINAZIONE
        defaultAgea20152020MatriceShouldBeFound("codDestinazione.equals=" + DEFAULT_COD_DESTINAZIONE);

        // Get all the agea20152020MatriceList where codDestinazione equals to UPDATED_COD_DESTINAZIONE
        defaultAgea20152020MatriceShouldNotBeFound("codDestinazione.equals=" + UPDATED_COD_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodDestinazioneIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codDestinazione in DEFAULT_COD_DESTINAZIONE or UPDATED_COD_DESTINAZIONE
        defaultAgea20152020MatriceShouldBeFound("codDestinazione.in=" + DEFAULT_COD_DESTINAZIONE + "," + UPDATED_COD_DESTINAZIONE);

        // Get all the agea20152020MatriceList where codDestinazione equals to UPDATED_COD_DESTINAZIONE
        defaultAgea20152020MatriceShouldNotBeFound("codDestinazione.in=" + UPDATED_COD_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodDestinazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codDestinazione is not null
        defaultAgea20152020MatriceShouldBeFound("codDestinazione.specified=true");

        // Get all the agea20152020MatriceList where codDestinazione is null
        defaultAgea20152020MatriceShouldNotBeFound("codDestinazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDestinazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where destinazione equals to DEFAULT_DESTINAZIONE
        defaultAgea20152020MatriceShouldBeFound("destinazione.equals=" + DEFAULT_DESTINAZIONE);

        // Get all the agea20152020MatriceList where destinazione equals to UPDATED_DESTINAZIONE
        defaultAgea20152020MatriceShouldNotBeFound("destinazione.equals=" + UPDATED_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDestinazioneIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where destinazione in DEFAULT_DESTINAZIONE or UPDATED_DESTINAZIONE
        defaultAgea20152020MatriceShouldBeFound("destinazione.in=" + DEFAULT_DESTINAZIONE + "," + UPDATED_DESTINAZIONE);

        // Get all the agea20152020MatriceList where destinazione equals to UPDATED_DESTINAZIONE
        defaultAgea20152020MatriceShouldNotBeFound("destinazione.in=" + UPDATED_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByDestinazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where destinazione is not null
        defaultAgea20152020MatriceShouldBeFound("destinazione.specified=true");

        // Get all the agea20152020MatriceList where destinazione is null
        defaultAgea20152020MatriceShouldNotBeFound("destinazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodUsoIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codUso equals to DEFAULT_COD_USO
        defaultAgea20152020MatriceShouldBeFound("codUso.equals=" + DEFAULT_COD_USO);

        // Get all the agea20152020MatriceList where codUso equals to UPDATED_COD_USO
        defaultAgea20152020MatriceShouldNotBeFound("codUso.equals=" + UPDATED_COD_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodUsoIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codUso in DEFAULT_COD_USO or UPDATED_COD_USO
        defaultAgea20152020MatriceShouldBeFound("codUso.in=" + DEFAULT_COD_USO + "," + UPDATED_COD_USO);

        // Get all the agea20152020MatriceList where codUso equals to UPDATED_COD_USO
        defaultAgea20152020MatriceShouldNotBeFound("codUso.in=" + UPDATED_COD_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodUsoIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codUso is not null
        defaultAgea20152020MatriceShouldBeFound("codUso.specified=true");

        // Get all the agea20152020MatriceList where codUso is null
        defaultAgea20152020MatriceShouldNotBeFound("codUso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByUsoIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where uso equals to DEFAULT_USO
        defaultAgea20152020MatriceShouldBeFound("uso.equals=" + DEFAULT_USO);

        // Get all the agea20152020MatriceList where uso equals to UPDATED_USO
        defaultAgea20152020MatriceShouldNotBeFound("uso.equals=" + UPDATED_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByUsoIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where uso in DEFAULT_USO or UPDATED_USO
        defaultAgea20152020MatriceShouldBeFound("uso.in=" + DEFAULT_USO + "," + UPDATED_USO);

        // Get all the agea20152020MatriceList where uso equals to UPDATED_USO
        defaultAgea20152020MatriceShouldNotBeFound("uso.in=" + UPDATED_USO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByUsoIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where uso is not null
        defaultAgea20152020MatriceShouldBeFound("uso.specified=true");

        // Get all the agea20152020MatriceList where uso is null
        defaultAgea20152020MatriceShouldNotBeFound("uso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodQualitaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codQualita equals to DEFAULT_COD_QUALITA
        defaultAgea20152020MatriceShouldBeFound("codQualita.equals=" + DEFAULT_COD_QUALITA);

        // Get all the agea20152020MatriceList where codQualita equals to UPDATED_COD_QUALITA
        defaultAgea20152020MatriceShouldNotBeFound("codQualita.equals=" + UPDATED_COD_QUALITA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodQualitaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codQualita in DEFAULT_COD_QUALITA or UPDATED_COD_QUALITA
        defaultAgea20152020MatriceShouldBeFound("codQualita.in=" + DEFAULT_COD_QUALITA + "," + UPDATED_COD_QUALITA);

        // Get all the agea20152020MatriceList where codQualita equals to UPDATED_COD_QUALITA
        defaultAgea20152020MatriceShouldNotBeFound("codQualita.in=" + UPDATED_COD_QUALITA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodQualitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codQualita is not null
        defaultAgea20152020MatriceShouldBeFound("codQualita.specified=true");

        // Get all the agea20152020MatriceList where codQualita is null
        defaultAgea20152020MatriceShouldNotBeFound("codQualita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByQualitaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where qualita equals to DEFAULT_QUALITA
        defaultAgea20152020MatriceShouldBeFound("qualita.equals=" + DEFAULT_QUALITA);

        // Get all the agea20152020MatriceList where qualita equals to UPDATED_QUALITA
        defaultAgea20152020MatriceShouldNotBeFound("qualita.equals=" + UPDATED_QUALITA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByQualitaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where qualita in DEFAULT_QUALITA or UPDATED_QUALITA
        defaultAgea20152020MatriceShouldBeFound("qualita.in=" + DEFAULT_QUALITA + "," + UPDATED_QUALITA);

        // Get all the agea20152020MatriceList where qualita equals to UPDATED_QUALITA
        defaultAgea20152020MatriceShouldNotBeFound("qualita.in=" + UPDATED_QUALITA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByQualitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where qualita is not null
        defaultAgea20152020MatriceShouldBeFound("qualita.specified=true");

        // Get all the agea20152020MatriceList where qualita is null
        defaultAgea20152020MatriceShouldNotBeFound("qualita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodVarietaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codVarieta equals to DEFAULT_COD_VARIETA
        defaultAgea20152020MatriceShouldBeFound("codVarieta.equals=" + DEFAULT_COD_VARIETA);

        // Get all the agea20152020MatriceList where codVarieta equals to UPDATED_COD_VARIETA
        defaultAgea20152020MatriceShouldNotBeFound("codVarieta.equals=" + UPDATED_COD_VARIETA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodVarietaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codVarieta in DEFAULT_COD_VARIETA or UPDATED_COD_VARIETA
        defaultAgea20152020MatriceShouldBeFound("codVarieta.in=" + DEFAULT_COD_VARIETA + "," + UPDATED_COD_VARIETA);

        // Get all the agea20152020MatriceList where codVarieta equals to UPDATED_COD_VARIETA
        defaultAgea20152020MatriceShouldNotBeFound("codVarieta.in=" + UPDATED_COD_VARIETA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodVarietaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codVarieta is not null
        defaultAgea20152020MatriceShouldBeFound("codVarieta.specified=true");

        // Get all the agea20152020MatriceList where codVarieta is null
        defaultAgea20152020MatriceShouldNotBeFound("codVarieta.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVarietaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where varieta equals to DEFAULT_VARIETA
        defaultAgea20152020MatriceShouldBeFound("varieta.equals=" + DEFAULT_VARIETA);

        // Get all the agea20152020MatriceList where varieta equals to UPDATED_VARIETA
        defaultAgea20152020MatriceShouldNotBeFound("varieta.equals=" + UPDATED_VARIETA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVarietaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where varieta in DEFAULT_VARIETA or UPDATED_VARIETA
        defaultAgea20152020MatriceShouldBeFound("varieta.in=" + DEFAULT_VARIETA + "," + UPDATED_VARIETA);

        // Get all the agea20152020MatriceList where varieta equals to UPDATED_VARIETA
        defaultAgea20152020MatriceShouldNotBeFound("varieta.in=" + UPDATED_VARIETA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVarietaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where varieta is not null
        defaultAgea20152020MatriceShouldBeFound("varieta.specified=true");

        // Get all the agea20152020MatriceList where varieta is null
        defaultAgea20152020MatriceShouldNotBeFound("varieta.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodFamigliaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codFamiglia equals to DEFAULT_COD_FAMIGLIA
        defaultAgea20152020MatriceShouldBeFound("codFamiglia.equals=" + DEFAULT_COD_FAMIGLIA);

        // Get all the agea20152020MatriceList where codFamiglia equals to UPDATED_COD_FAMIGLIA
        defaultAgea20152020MatriceShouldNotBeFound("codFamiglia.equals=" + UPDATED_COD_FAMIGLIA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodFamigliaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codFamiglia in DEFAULT_COD_FAMIGLIA or UPDATED_COD_FAMIGLIA
        defaultAgea20152020MatriceShouldBeFound("codFamiglia.in=" + DEFAULT_COD_FAMIGLIA + "," + UPDATED_COD_FAMIGLIA);

        // Get all the agea20152020MatriceList where codFamiglia equals to UPDATED_COD_FAMIGLIA
        defaultAgea20152020MatriceShouldNotBeFound("codFamiglia.in=" + UPDATED_COD_FAMIGLIA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodFamigliaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codFamiglia is not null
        defaultAgea20152020MatriceShouldBeFound("codFamiglia.specified=true");

        // Get all the agea20152020MatriceList where codFamiglia is null
        defaultAgea20152020MatriceShouldNotBeFound("codFamiglia.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByFamigliaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where famiglia equals to DEFAULT_FAMIGLIA
        defaultAgea20152020MatriceShouldBeFound("famiglia.equals=" + DEFAULT_FAMIGLIA);

        // Get all the agea20152020MatriceList where famiglia equals to UPDATED_FAMIGLIA
        defaultAgea20152020MatriceShouldNotBeFound("famiglia.equals=" + UPDATED_FAMIGLIA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByFamigliaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where famiglia in DEFAULT_FAMIGLIA or UPDATED_FAMIGLIA
        defaultAgea20152020MatriceShouldBeFound("famiglia.in=" + DEFAULT_FAMIGLIA + "," + UPDATED_FAMIGLIA);

        // Get all the agea20152020MatriceList where famiglia equals to UPDATED_FAMIGLIA
        defaultAgea20152020MatriceShouldNotBeFound("famiglia.in=" + UPDATED_FAMIGLIA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByFamigliaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where famiglia is not null
        defaultAgea20152020MatriceShouldBeFound("famiglia.specified=true");

        // Get all the agea20152020MatriceList where famiglia is null
        defaultAgea20152020MatriceShouldNotBeFound("famiglia.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodGenereIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codGenere equals to DEFAULT_COD_GENERE
        defaultAgea20152020MatriceShouldBeFound("codGenere.equals=" + DEFAULT_COD_GENERE);

        // Get all the agea20152020MatriceList where codGenere equals to UPDATED_COD_GENERE
        defaultAgea20152020MatriceShouldNotBeFound("codGenere.equals=" + UPDATED_COD_GENERE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodGenereIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codGenere in DEFAULT_COD_GENERE or UPDATED_COD_GENERE
        defaultAgea20152020MatriceShouldBeFound("codGenere.in=" + DEFAULT_COD_GENERE + "," + UPDATED_COD_GENERE);

        // Get all the agea20152020MatriceList where codGenere equals to UPDATED_COD_GENERE
        defaultAgea20152020MatriceShouldNotBeFound("codGenere.in=" + UPDATED_COD_GENERE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodGenereIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codGenere is not null
        defaultAgea20152020MatriceShouldBeFound("codGenere.specified=true");

        // Get all the agea20152020MatriceList where codGenere is null
        defaultAgea20152020MatriceShouldNotBeFound("codGenere.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByGenereIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where genere equals to DEFAULT_GENERE
        defaultAgea20152020MatriceShouldBeFound("genere.equals=" + DEFAULT_GENERE);

        // Get all the agea20152020MatriceList where genere equals to UPDATED_GENERE
        defaultAgea20152020MatriceShouldNotBeFound("genere.equals=" + UPDATED_GENERE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByGenereIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where genere in DEFAULT_GENERE or UPDATED_GENERE
        defaultAgea20152020MatriceShouldBeFound("genere.in=" + DEFAULT_GENERE + "," + UPDATED_GENERE);

        // Get all the agea20152020MatriceList where genere equals to UPDATED_GENERE
        defaultAgea20152020MatriceShouldNotBeFound("genere.in=" + UPDATED_GENERE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByGenereIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where genere is not null
        defaultAgea20152020MatriceShouldBeFound("genere.specified=true");

        // Get all the agea20152020MatriceList where genere is null
        defaultAgea20152020MatriceShouldNotBeFound("genere.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodSpecieIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codSpecie equals to DEFAULT_COD_SPECIE
        defaultAgea20152020MatriceShouldBeFound("codSpecie.equals=" + DEFAULT_COD_SPECIE);

        // Get all the agea20152020MatriceList where codSpecie equals to UPDATED_COD_SPECIE
        defaultAgea20152020MatriceShouldNotBeFound("codSpecie.equals=" + UPDATED_COD_SPECIE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodSpecieIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codSpecie in DEFAULT_COD_SPECIE or UPDATED_COD_SPECIE
        defaultAgea20152020MatriceShouldBeFound("codSpecie.in=" + DEFAULT_COD_SPECIE + "," + UPDATED_COD_SPECIE);

        // Get all the agea20152020MatriceList where codSpecie equals to UPDATED_COD_SPECIE
        defaultAgea20152020MatriceShouldNotBeFound("codSpecie.in=" + UPDATED_COD_SPECIE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodSpecieIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codSpecie is not null
        defaultAgea20152020MatriceShouldBeFound("codSpecie.specified=true");

        // Get all the agea20152020MatriceList where codSpecie is null
        defaultAgea20152020MatriceShouldNotBeFound("codSpecie.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesBySpecieIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where specie equals to DEFAULT_SPECIE
        defaultAgea20152020MatriceShouldBeFound("specie.equals=" + DEFAULT_SPECIE);

        // Get all the agea20152020MatriceList where specie equals to UPDATED_SPECIE
        defaultAgea20152020MatriceShouldNotBeFound("specie.equals=" + UPDATED_SPECIE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesBySpecieIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where specie in DEFAULT_SPECIE or UPDATED_SPECIE
        defaultAgea20152020MatriceShouldBeFound("specie.in=" + DEFAULT_SPECIE + "," + UPDATED_SPECIE);

        // Get all the agea20152020MatriceList where specie equals to UPDATED_SPECIE
        defaultAgea20152020MatriceShouldNotBeFound("specie.in=" + UPDATED_SPECIE);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesBySpecieIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where specie is not null
        defaultAgea20152020MatriceShouldBeFound("specie.specified=true");

        // Get all the agea20152020MatriceList where specie is null
        defaultAgea20152020MatriceShouldNotBeFound("specie.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVersioneInserimentoIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where versioneInserimento equals to DEFAULT_VERSIONE_INSERIMENTO
        defaultAgea20152020MatriceShouldBeFound("versioneInserimento.equals=" + DEFAULT_VERSIONE_INSERIMENTO);

        // Get all the agea20152020MatriceList where versioneInserimento equals to UPDATED_VERSIONE_INSERIMENTO
        defaultAgea20152020MatriceShouldNotBeFound("versioneInserimento.equals=" + UPDATED_VERSIONE_INSERIMENTO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVersioneInserimentoIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where versioneInserimento in DEFAULT_VERSIONE_INSERIMENTO or UPDATED_VERSIONE_INSERIMENTO
        defaultAgea20152020MatriceShouldBeFound("versioneInserimento.in=" + DEFAULT_VERSIONE_INSERIMENTO + "," + UPDATED_VERSIONE_INSERIMENTO);

        // Get all the agea20152020MatriceList where versioneInserimento equals to UPDATED_VERSIONE_INSERIMENTO
        defaultAgea20152020MatriceShouldNotBeFound("versioneInserimento.in=" + UPDATED_VERSIONE_INSERIMENTO);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByVersioneInserimentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where versioneInserimento is not null
        defaultAgea20152020MatriceShouldBeFound("versioneInserimento.specified=true");

        // Get all the agea20152020MatriceList where versioneInserimento is null
        defaultAgea20152020MatriceShouldNotBeFound("versioneInserimento.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodiceCompostoAgeaIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codiceCompostoAgea equals to DEFAULT_CODICE_COMPOSTO_AGEA
        defaultAgea20152020MatriceShouldBeFound("codiceCompostoAgea.equals=" + DEFAULT_CODICE_COMPOSTO_AGEA);

        // Get all the agea20152020MatriceList where codiceCompostoAgea equals to UPDATED_CODICE_COMPOSTO_AGEA
        defaultAgea20152020MatriceShouldNotBeFound("codiceCompostoAgea.equals=" + UPDATED_CODICE_COMPOSTO_AGEA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodiceCompostoAgeaIsInShouldWork() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codiceCompostoAgea in DEFAULT_CODICE_COMPOSTO_AGEA or UPDATED_CODICE_COMPOSTO_AGEA
        defaultAgea20152020MatriceShouldBeFound("codiceCompostoAgea.in=" + DEFAULT_CODICE_COMPOSTO_AGEA + "," + UPDATED_CODICE_COMPOSTO_AGEA);

        // Get all the agea20152020MatriceList where codiceCompostoAgea equals to UPDATED_CODICE_COMPOSTO_AGEA
        defaultAgea20152020MatriceShouldNotBeFound("codiceCompostoAgea.in=" + UPDATED_CODICE_COMPOSTO_AGEA);
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByCodiceCompostoAgeaIsNullOrNotNull() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);

        // Get all the agea20152020MatriceList where codiceCompostoAgea is not null
        defaultAgea20152020MatriceShouldBeFound("codiceCompostoAgea.specified=true");

        // Get all the agea20152020MatriceList where codiceCompostoAgea is null
        defaultAgea20152020MatriceShouldNotBeFound("codiceCompostoAgea.specified=false");
    }

    @Test
    @Transactional
    public void getAllAgea20152020MatricesByAgeaToDettIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        AziSupPartDettaglio ageaToDett = AziSupPartDettaglioResourceIT.createEntity(em);
        em.persist(ageaToDett);
        em.flush();
        agea20152020Matrice.addAgeaToDett(ageaToDett);
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        Long ageaToDettId = ageaToDett.getId();

        // Get all the agea20152020MatriceList where ageaToDett equals to ageaToDettId
        defaultAgea20152020MatriceShouldBeFound("ageaToDettId.equals=" + ageaToDettId);

        // Get all the agea20152020MatriceList where ageaToDett equals to ageaToDettId + 1
        defaultAgea20152020MatriceShouldNotBeFound("ageaToDettId.equals=" + (ageaToDettId + 1));
    }


    @Test
    @Transactional
    public void getAllAgea20152020MatricesByAgeaToColtIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        ColturaAppezzamento ageaToColt = ColturaAppezzamentoResourceIT.createEntity(em);
        em.persist(ageaToColt);
        em.flush();
        agea20152020Matrice.addAgeaToColt(ageaToColt);
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        Long ageaToColtId = ageaToColt.getId();

        // Get all the agea20152020MatriceList where ageaToColt equals to ageaToColtId
        defaultAgea20152020MatriceShouldBeFound("ageaToColtId.equals=" + ageaToColtId);

        // Get all the agea20152020MatriceList where ageaToColt equals to ageaToColtId + 1
        defaultAgea20152020MatriceShouldNotBeFound("ageaToColtId.equals=" + (ageaToColtId + 1));
    }


    @Test
    @Transactional
    public void getAllAgea20152020MatricesByMatriceToBdfIsEqualToSomething() throws Exception {
        // Initialize the database
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        Agea20152020BDF matriceToBdf = Agea20152020BDFResourceIT.createEntity(em);
        em.persist(matriceToBdf);
        em.flush();
        agea20152020Matrice.addMatriceToBdf(matriceToBdf);
        agea20152020MatriceRepository.saveAndFlush(agea20152020Matrice);
        Long matriceToBdfId = matriceToBdf.getId();

        // Get all the agea20152020MatriceList where matriceToBdf equals to matriceToBdfId
        defaultAgea20152020MatriceShouldBeFound("matriceToBdfId.equals=" + matriceToBdfId);

        // Get all the agea20152020MatriceList where matriceToBdf equals to matriceToBdfId + 1
        defaultAgea20152020MatriceShouldNotBeFound("matriceToBdfId.equals=" + (matriceToBdfId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAgea20152020MatriceShouldBeFound(String filter) throws Exception {
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agea20152020Matrice.getId().intValue())))
            .andExpect(jsonPath("$.[*].descColtura").value(hasItem(DEFAULT_DESC_COLTURA)))
            .andExpect(jsonPath("$.[*].codMacroUso").value(hasItem(DEFAULT_COD_MACRO_USO)))
            .andExpect(jsonPath("$.[*].macroUso").value(hasItem(DEFAULT_MACRO_USO)))
            .andExpect(jsonPath("$.[*].codOccupazioneSuolo").value(hasItem(DEFAULT_COD_OCCUPAZIONE_SUOLO)))
            .andExpect(jsonPath("$.[*].occupazioneSuolo").value(hasItem(DEFAULT_OCCUPAZIONE_SUOLO)))
            .andExpect(jsonPath("$.[*].codDestinazione").value(hasItem(DEFAULT_COD_DESTINAZIONE)))
            .andExpect(jsonPath("$.[*].destinazione").value(hasItem(DEFAULT_DESTINAZIONE)))
            .andExpect(jsonPath("$.[*].codUso").value(hasItem(DEFAULT_COD_USO)))
            .andExpect(jsonPath("$.[*].uso").value(hasItem(DEFAULT_USO)))
            .andExpect(jsonPath("$.[*].codQualita").value(hasItem(DEFAULT_COD_QUALITA)))
            .andExpect(jsonPath("$.[*].qualita").value(hasItem(DEFAULT_QUALITA)))
            .andExpect(jsonPath("$.[*].codVarieta").value(hasItem(DEFAULT_COD_VARIETA)))
            .andExpect(jsonPath("$.[*].varieta").value(hasItem(DEFAULT_VARIETA)))
            .andExpect(jsonPath("$.[*].codFamiglia").value(hasItem(DEFAULT_COD_FAMIGLIA)))
            .andExpect(jsonPath("$.[*].famiglia").value(hasItem(DEFAULT_FAMIGLIA)))
            .andExpect(jsonPath("$.[*].codGenere").value(hasItem(DEFAULT_COD_GENERE)))
            .andExpect(jsonPath("$.[*].genere").value(hasItem(DEFAULT_GENERE)))
            .andExpect(jsonPath("$.[*].codSpecie").value(hasItem(DEFAULT_COD_SPECIE)))
            .andExpect(jsonPath("$.[*].specie").value(hasItem(DEFAULT_SPECIE)))
            .andExpect(jsonPath("$.[*].versioneInserimento").value(hasItem(DEFAULT_VERSIONE_INSERIMENTO)))
            .andExpect(jsonPath("$.[*].codiceCompostoAgea").value(hasItem(DEFAULT_CODICE_COMPOSTO_AGEA)));

        // Check, that the count call also returns 1
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAgea20152020MatriceShouldNotBeFound(String filter) throws Exception {
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAgea20152020Matrice() throws Exception {
        // Get the agea20152020Matrice
        restAgea20152020MatriceMockMvc.perform(get("/api/agea-20152020-matrices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgea20152020Matrice() throws Exception {
        // Initialize the database
        agea20152020MatriceService.save(agea20152020Matrice);

        int databaseSizeBeforeUpdate = agea20152020MatriceRepository.findAll().size();

        // Update the agea20152020Matrice
        Agea20152020Matrice updatedAgea20152020Matrice = agea20152020MatriceRepository.findById(agea20152020Matrice.getId()).get();
        // Disconnect from session so that the updates on updatedAgea20152020Matrice are not directly saved in db
        em.detach(updatedAgea20152020Matrice);
        updatedAgea20152020Matrice
            .descColtura(UPDATED_DESC_COLTURA)
            .codMacroUso(UPDATED_COD_MACRO_USO)
            .macroUso(UPDATED_MACRO_USO)
            .codOccupazioneSuolo(UPDATED_COD_OCCUPAZIONE_SUOLO)
            .occupazioneSuolo(UPDATED_OCCUPAZIONE_SUOLO)
            .codDestinazione(UPDATED_COD_DESTINAZIONE)
            .destinazione(UPDATED_DESTINAZIONE)
            .codUso(UPDATED_COD_USO)
            .uso(UPDATED_USO)
            .codQualita(UPDATED_COD_QUALITA)
            .qualita(UPDATED_QUALITA)
            .codVarieta(UPDATED_COD_VARIETA)
            .varieta(UPDATED_VARIETA)
            .codFamiglia(UPDATED_COD_FAMIGLIA)
            .famiglia(UPDATED_FAMIGLIA)
            .codGenere(UPDATED_COD_GENERE)
            .genere(UPDATED_GENERE)
            .codSpecie(UPDATED_COD_SPECIE)
            .specie(UPDATED_SPECIE)
            .versioneInserimento(UPDATED_VERSIONE_INSERIMENTO)
            .codiceCompostoAgea(UPDATED_CODICE_COMPOSTO_AGEA);

        restAgea20152020MatriceMockMvc.perform(put("/api/agea-20152020-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgea20152020Matrice)))
            .andExpect(status().isOk());

        // Validate the Agea20152020Matrice in the database
        List<Agea20152020Matrice> agea20152020MatriceList = agea20152020MatriceRepository.findAll();
        assertThat(agea20152020MatriceList).hasSize(databaseSizeBeforeUpdate);
        Agea20152020Matrice testAgea20152020Matrice = agea20152020MatriceList.get(agea20152020MatriceList.size() - 1);
        assertThat(testAgea20152020Matrice.getDescColtura()).isEqualTo(UPDATED_DESC_COLTURA);
        assertThat(testAgea20152020Matrice.getCodMacroUso()).isEqualTo(UPDATED_COD_MACRO_USO);
        assertThat(testAgea20152020Matrice.getMacroUso()).isEqualTo(UPDATED_MACRO_USO);
        assertThat(testAgea20152020Matrice.getCodOccupazioneSuolo()).isEqualTo(UPDATED_COD_OCCUPAZIONE_SUOLO);
        assertThat(testAgea20152020Matrice.getOccupazioneSuolo()).isEqualTo(UPDATED_OCCUPAZIONE_SUOLO);
        assertThat(testAgea20152020Matrice.getCodDestinazione()).isEqualTo(UPDATED_COD_DESTINAZIONE);
        assertThat(testAgea20152020Matrice.getDestinazione()).isEqualTo(UPDATED_DESTINAZIONE);
        assertThat(testAgea20152020Matrice.getCodUso()).isEqualTo(UPDATED_COD_USO);
        assertThat(testAgea20152020Matrice.getUso()).isEqualTo(UPDATED_USO);
        assertThat(testAgea20152020Matrice.getCodQualita()).isEqualTo(UPDATED_COD_QUALITA);
        assertThat(testAgea20152020Matrice.getQualita()).isEqualTo(UPDATED_QUALITA);
        assertThat(testAgea20152020Matrice.getCodVarieta()).isEqualTo(UPDATED_COD_VARIETA);
        assertThat(testAgea20152020Matrice.getVarieta()).isEqualTo(UPDATED_VARIETA);
        assertThat(testAgea20152020Matrice.getCodFamiglia()).isEqualTo(UPDATED_COD_FAMIGLIA);
        assertThat(testAgea20152020Matrice.getFamiglia()).isEqualTo(UPDATED_FAMIGLIA);
        assertThat(testAgea20152020Matrice.getCodGenere()).isEqualTo(UPDATED_COD_GENERE);
        assertThat(testAgea20152020Matrice.getGenere()).isEqualTo(UPDATED_GENERE);
        assertThat(testAgea20152020Matrice.getCodSpecie()).isEqualTo(UPDATED_COD_SPECIE);
        assertThat(testAgea20152020Matrice.getSpecie()).isEqualTo(UPDATED_SPECIE);
        assertThat(testAgea20152020Matrice.getVersioneInserimento()).isEqualTo(UPDATED_VERSIONE_INSERIMENTO);
        assertThat(testAgea20152020Matrice.getCodiceCompostoAgea()).isEqualTo(UPDATED_CODICE_COMPOSTO_AGEA);
    }

    @Test
    @Transactional
    public void updateNonExistingAgea20152020Matrice() throws Exception {
        int databaseSizeBeforeUpdate = agea20152020MatriceRepository.findAll().size();

        // Create the Agea20152020Matrice

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgea20152020MatriceMockMvc.perform(put("/api/agea-20152020-matrices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agea20152020Matrice)))
            .andExpect(status().isBadRequest());

        // Validate the Agea20152020Matrice in the database
        List<Agea20152020Matrice> agea20152020MatriceList = agea20152020MatriceRepository.findAll();
        assertThat(agea20152020MatriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgea20152020Matrice() throws Exception {
        // Initialize the database
        agea20152020MatriceService.save(agea20152020Matrice);

        int databaseSizeBeforeDelete = agea20152020MatriceRepository.findAll().size();

        // Delete the agea20152020Matrice
        restAgea20152020MatriceMockMvc.perform(delete("/api/agea-20152020-matrices/{id}", agea20152020Matrice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Agea20152020Matrice> agea20152020MatriceList = agea20152020MatriceRepository.findAll();
        assertThat(agea20152020MatriceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Agea20152020Matrice.class);
        Agea20152020Matrice agea20152020Matrice1 = new Agea20152020Matrice();
        agea20152020Matrice1.setId(1L);
        Agea20152020Matrice agea20152020Matrice2 = new Agea20152020Matrice();
        agea20152020Matrice2.setId(agea20152020Matrice1.getId());
        assertThat(agea20152020Matrice1).isEqualTo(agea20152020Matrice2);
        agea20152020Matrice2.setId(2L);
        assertThat(agea20152020Matrice1).isNotEqualTo(agea20152020Matrice2);
        agea20152020Matrice1.setId(null);
        assertThat(agea20152020Matrice1).isNotEqualTo(agea20152020Matrice2);
    }
}
