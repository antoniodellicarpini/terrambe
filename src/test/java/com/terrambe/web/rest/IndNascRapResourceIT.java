package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndNascRap;
import com.terrambe.domain.AziRappresentante;
import com.terrambe.repository.IndNascRapRepository;
import com.terrambe.service.IndNascRapService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndNascRapCriteria;
import com.terrambe.service.IndNascRapQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndNascRapResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndNascRapResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private IndNascRapRepository indNascRapRepository;

    @Autowired
    private IndNascRapService indNascRapService;

    @Autowired
    private IndNascRapQueryService indNascRapQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndNascRapMockMvc;

    private IndNascRap indNascRap;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndNascRapResource indNascRapResource = new IndNascRapResource(indNascRapService, indNascRapQueryService);
        this.restIndNascRapMockMvc = MockMvcBuilders.standaloneSetup(indNascRapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascRap createEntity(EntityManager em) {
        IndNascRap indNascRap = new IndNascRap()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return indNascRap;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascRap createUpdatedEntity(EntityManager em) {
        IndNascRap indNascRap = new IndNascRap()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return indNascRap;
    }

    @BeforeEach
    public void initTest() {
        indNascRap = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndNascRap() throws Exception {
        int databaseSizeBeforeCreate = indNascRapRepository.findAll().size();

        // Create the IndNascRap
        restIndNascRapMockMvc.perform(post("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRap)))
            .andExpect(status().isCreated());

        // Validate the IndNascRap in the database
        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeCreate + 1);
        IndNascRap testIndNascRap = indNascRapList.get(indNascRapList.size() - 1);
        assertThat(testIndNascRap.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndNascRap.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndNascRap.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndNascRap.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndNascRap.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndNascRap.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndNascRap.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndNascRap.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndNascRap.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndNascRap.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createIndNascRapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indNascRapRepository.findAll().size();

        // Create the IndNascRap with an existing ID
        indNascRap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndNascRapMockMvc.perform(post("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRap)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascRap in the database
        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascRapRepository.findAll().size();
        // set the field null
        indNascRap.setIndirizzo(null);

        // Create the IndNascRap, which fails.

        restIndNascRapMockMvc.perform(post("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRap)))
            .andExpect(status().isBadRequest());

        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascRapRepository.findAll().size();
        // set the field null
        indNascRap.setNazione(null);

        // Create the IndNascRap, which fails.

        restIndNascRapMockMvc.perform(post("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRap)))
            .andExpect(status().isBadRequest());

        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndNascRaps() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getIndNascRap() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get the indNascRap
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps/{id}", indNascRap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indNascRap.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndNascRapShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indNascRapList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascRapShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndNascRapShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indNascRapList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascRapShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where indirizzo is not null
        defaultIndNascRapShouldBeFound("indirizzo.specified=true");

        // Get all the indNascRapList where indirizzo is null
        defaultIndNascRapShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where cap equals to DEFAULT_CAP
        defaultIndNascRapShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indNascRapList where cap equals to UPDATED_CAP
        defaultIndNascRapShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndNascRapShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indNascRapList where cap equals to UPDATED_CAP
        defaultIndNascRapShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where cap is not null
        defaultIndNascRapShouldBeFound("cap.specified=true");

        // Get all the indNascRapList where cap is null
        defaultIndNascRapShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where nazione equals to DEFAULT_NAZIONE
        defaultIndNascRapShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indNascRapList where nazione equals to UPDATED_NAZIONE
        defaultIndNascRapShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndNascRapShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indNascRapList where nazione equals to UPDATED_NAZIONE
        defaultIndNascRapShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where nazione is not null
        defaultIndNascRapShouldBeFound("nazione.specified=true");

        // Get all the indNascRapList where nazione is null
        defaultIndNascRapShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where regione equals to DEFAULT_REGIONE
        defaultIndNascRapShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indNascRapList where regione equals to UPDATED_REGIONE
        defaultIndNascRapShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndNascRapShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indNascRapList where regione equals to UPDATED_REGIONE
        defaultIndNascRapShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where regione is not null
        defaultIndNascRapShouldBeFound("regione.specified=true");

        // Get all the indNascRapList where regione is null
        defaultIndNascRapShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where provincia equals to DEFAULT_PROVINCIA
        defaultIndNascRapShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indNascRapList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascRapShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndNascRapShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indNascRapList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascRapShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where provincia is not null
        defaultIndNascRapShouldBeFound("provincia.specified=true");

        // Get all the indNascRapList where provincia is null
        defaultIndNascRapShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where comune equals to DEFAULT_COMUNE
        defaultIndNascRapShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indNascRapList where comune equals to UPDATED_COMUNE
        defaultIndNascRapShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndNascRapShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indNascRapList where comune equals to UPDATED_COMUNE
        defaultIndNascRapShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where comune is not null
        defaultIndNascRapShouldBeFound("comune.specified=true");

        // Get all the indNascRapList where comune is null
        defaultIndNascRapShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali is not null
        defaultIndNascRapShouldBeFound("dataInizVali.specified=true");

        // Get all the indNascRapList where dataInizVali is null
        defaultIndNascRapShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndNascRapShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali is not null
        defaultIndNascRapShouldBeFound("dataFineVali.specified=true");

        // Get all the indNascRapList where dataFineVali is null
        defaultIndNascRapShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndNascRapShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndNascRapShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator is not null
        defaultIndNascRapShouldBeFound("userIdCreator.specified=true");

        // Get all the indNascRapList where userIdCreator is null
        defaultIndNascRapShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndNascRapShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndNascRapShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod is not null
        defaultIndNascRapShouldBeFound("userIdLastMod.specified=true");

        // Get all the indNascRapList where userIdLastMod is null
        defaultIndNascRapShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);

        // Get all the indNascRapList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndNascRapShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndNascRapsByIndNascToAziRapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapRepository.saveAndFlush(indNascRap);
        AziRappresentante indNascToAziRap = AziRappresentanteResourceIT.createEntity(em);
        em.persist(indNascToAziRap);
        em.flush();
        indNascRap.setIndNascToAziRap(indNascToAziRap);
        indNascToAziRap.setAziRapToIndNasc(indNascRap);
        indNascRapRepository.saveAndFlush(indNascRap);
        Long indNascToAziRapId = indNascToAziRap.getId();

        // Get all the indNascRapList where indNascToAziRap equals to indNascToAziRapId
        defaultIndNascRapShouldBeFound("indNascToAziRapId.equals=" + indNascToAziRapId);

        // Get all the indNascRapList where indNascToAziRap equals to indNascToAziRapId + 1
        defaultIndNascRapShouldNotBeFound("indNascToAziRapId.equals=" + (indNascToAziRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndNascRapShouldBeFound(String filter) throws Exception {
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndNascRapShouldNotBeFound(String filter) throws Exception {
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndNascRap() throws Exception {
        // Get the indNascRap
        restIndNascRapMockMvc.perform(get("/api/ind-nasc-raps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndNascRap() throws Exception {
        // Initialize the database
        indNascRapService.save(indNascRap);

        int databaseSizeBeforeUpdate = indNascRapRepository.findAll().size();

        // Update the indNascRap
        IndNascRap updatedIndNascRap = indNascRapRepository.findById(indNascRap.getId()).get();
        // Disconnect from session so that the updates on updatedIndNascRap are not directly saved in db
        em.detach(updatedIndNascRap);
        updatedIndNascRap
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restIndNascRapMockMvc.perform(put("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndNascRap)))
            .andExpect(status().isOk());

        // Validate the IndNascRap in the database
        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeUpdate);
        IndNascRap testIndNascRap = indNascRapList.get(indNascRapList.size() - 1);
        assertThat(testIndNascRap.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndNascRap.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndNascRap.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndNascRap.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndNascRap.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndNascRap.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndNascRap.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndNascRap.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndNascRap.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndNascRap.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingIndNascRap() throws Exception {
        int databaseSizeBeforeUpdate = indNascRapRepository.findAll().size();

        // Create the IndNascRap

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndNascRapMockMvc.perform(put("/api/ind-nasc-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRap)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascRap in the database
        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndNascRap() throws Exception {
        // Initialize the database
        indNascRapService.save(indNascRap);

        int databaseSizeBeforeDelete = indNascRapRepository.findAll().size();

        // Delete the indNascRap
        restIndNascRapMockMvc.perform(delete("/api/ind-nasc-raps/{id}", indNascRap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndNascRap> indNascRapList = indNascRapRepository.findAll();
        assertThat(indNascRapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndNascRap.class);
        IndNascRap indNascRap1 = new IndNascRap();
        indNascRap1.setId(1L);
        IndNascRap indNascRap2 = new IndNascRap();
        indNascRap2.setId(indNascRap1.getId());
        assertThat(indNascRap1).isEqualTo(indNascRap2);
        indNascRap2.setId(2L);
        assertThat(indNascRap1).isNotEqualTo(indNascRap2);
        indNascRap1.setId(null);
        assertThat(indNascRap1).isNotEqualTo(indNascRap2);
    }
}
