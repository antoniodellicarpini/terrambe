package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoPrepterr;
import com.terrambe.domain.RegOpcPrepterr;
import com.terrambe.repository.TipoPrepterrRepository;
import com.terrambe.service.TipoPrepterrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoPrepterrCriteria;
import com.terrambe.service.TipoPrepterrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoPrepterrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoPrepterrResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoPrepterrRepository tipoPrepterrRepository;

    @Autowired
    private TipoPrepterrService tipoPrepterrService;

    @Autowired
    private TipoPrepterrQueryService tipoPrepterrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoPrepterrMockMvc;

    private TipoPrepterr tipoPrepterr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoPrepterrResource tipoPrepterrResource = new TipoPrepterrResource(tipoPrepterrService, tipoPrepterrQueryService);
        this.restTipoPrepterrMockMvc = MockMvcBuilders.standaloneSetup(tipoPrepterrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoPrepterr createEntity(EntityManager em) {
        TipoPrepterr tipoPrepterr = new TipoPrepterr()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoPrepterr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoPrepterr createUpdatedEntity(EntityManager em) {
        TipoPrepterr tipoPrepterr = new TipoPrepterr()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoPrepterr;
    }

    @BeforeEach
    public void initTest() {
        tipoPrepterr = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoPrepterr() throws Exception {
        int databaseSizeBeforeCreate = tipoPrepterrRepository.findAll().size();

        // Create the TipoPrepterr
        restTipoPrepterrMockMvc.perform(post("/api/tipo-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoPrepterr)))
            .andExpect(status().isCreated());

        // Validate the TipoPrepterr in the database
        List<TipoPrepterr> tipoPrepterrList = tipoPrepterrRepository.findAll();
        assertThat(tipoPrepterrList).hasSize(databaseSizeBeforeCreate + 1);
        TipoPrepterr testTipoPrepterr = tipoPrepterrList.get(tipoPrepterrList.size() - 1);
        assertThat(testTipoPrepterr.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoPrepterr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoPrepterr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoPrepterr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoPrepterr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoPrepterrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoPrepterrRepository.findAll().size();

        // Create the TipoPrepterr with an existing ID
        tipoPrepterr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoPrepterrMockMvc.perform(post("/api/tipo-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoPrepterr)))
            .andExpect(status().isBadRequest());

        // Validate the TipoPrepterr in the database
        List<TipoPrepterr> tipoPrepterrList = tipoPrepterrRepository.findAll();
        assertThat(tipoPrepterrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoPrepterrs() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoPrepterr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoPrepterr() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get the tipoPrepterr
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs/{id}", tipoPrepterr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoPrepterr.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoPrepterrShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoPrepterrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoPrepterrShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoPrepterrShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoPrepterrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoPrepterrShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where descrizione is not null
        defaultTipoPrepterrShouldBeFound("descrizione.specified=true");

        // Get all the tipoPrepterrList where descrizione is null
        defaultTipoPrepterrShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali is not null
        defaultTipoPrepterrShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoPrepterrList where dataInizVali is null
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoPrepterrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoPrepterrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoPrepterrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali is not null
        defaultTipoPrepterrShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoPrepterrList where dataFineVali is null
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoPrepterrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoPrepterrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoPrepterrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator is not null
        defaultTipoPrepterrShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoPrepterrList where userIdCreator is null
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoPrepterrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoPrepterrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoPrepterrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod is not null
        defaultTipoPrepterrShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoPrepterrList where userIdLastMod is null
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoPrepterrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);

        // Get all the tipoPrepterrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoPrepterrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoPrepterrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoPrepterrsByTipoToOpcPrepterrIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);
        RegOpcPrepterr tipoToOpcPrepterr = RegOpcPrepterrResourceIT.createEntity(em);
        em.persist(tipoToOpcPrepterr);
        em.flush();
        tipoPrepterr.addTipoToOpcPrepterr(tipoToOpcPrepterr);
        tipoPrepterrRepository.saveAndFlush(tipoPrepterr);
        Long tipoToOpcPrepterrId = tipoToOpcPrepterr.getId();

        // Get all the tipoPrepterrList where tipoToOpcPrepterr equals to tipoToOpcPrepterrId
        defaultTipoPrepterrShouldBeFound("tipoToOpcPrepterrId.equals=" + tipoToOpcPrepterrId);

        // Get all the tipoPrepterrList where tipoToOpcPrepterr equals to tipoToOpcPrepterrId + 1
        defaultTipoPrepterrShouldNotBeFound("tipoToOpcPrepterrId.equals=" + (tipoToOpcPrepterrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoPrepterrShouldBeFound(String filter) throws Exception {
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoPrepterr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoPrepterrShouldNotBeFound(String filter) throws Exception {
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoPrepterr() throws Exception {
        // Get the tipoPrepterr
        restTipoPrepterrMockMvc.perform(get("/api/tipo-prepterrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoPrepterr() throws Exception {
        // Initialize the database
        tipoPrepterrService.save(tipoPrepterr);

        int databaseSizeBeforeUpdate = tipoPrepterrRepository.findAll().size();

        // Update the tipoPrepterr
        TipoPrepterr updatedTipoPrepterr = tipoPrepterrRepository.findById(tipoPrepterr.getId()).get();
        // Disconnect from session so that the updates on updatedTipoPrepterr are not directly saved in db
        em.detach(updatedTipoPrepterr);
        updatedTipoPrepterr
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoPrepterrMockMvc.perform(put("/api/tipo-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoPrepterr)))
            .andExpect(status().isOk());

        // Validate the TipoPrepterr in the database
        List<TipoPrepterr> tipoPrepterrList = tipoPrepterrRepository.findAll();
        assertThat(tipoPrepterrList).hasSize(databaseSizeBeforeUpdate);
        TipoPrepterr testTipoPrepterr = tipoPrepterrList.get(tipoPrepterrList.size() - 1);
        assertThat(testTipoPrepterr.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoPrepterr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoPrepterr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoPrepterr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoPrepterr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoPrepterr() throws Exception {
        int databaseSizeBeforeUpdate = tipoPrepterrRepository.findAll().size();

        // Create the TipoPrepterr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoPrepterrMockMvc.perform(put("/api/tipo-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoPrepterr)))
            .andExpect(status().isBadRequest());

        // Validate the TipoPrepterr in the database
        List<TipoPrepterr> tipoPrepterrList = tipoPrepterrRepository.findAll();
        assertThat(tipoPrepterrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoPrepterr() throws Exception {
        // Initialize the database
        tipoPrepterrService.save(tipoPrepterr);

        int databaseSizeBeforeDelete = tipoPrepterrRepository.findAll().size();

        // Delete the tipoPrepterr
        restTipoPrepterrMockMvc.perform(delete("/api/tipo-prepterrs/{id}", tipoPrepterr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoPrepterr> tipoPrepterrList = tipoPrepterrRepository.findAll();
        assertThat(tipoPrepterrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoPrepterr.class);
        TipoPrepterr tipoPrepterr1 = new TipoPrepterr();
        tipoPrepterr1.setId(1L);
        TipoPrepterr tipoPrepterr2 = new TipoPrepterr();
        tipoPrepterr2.setId(tipoPrepterr1.getId());
        assertThat(tipoPrepterr1).isEqualTo(tipoPrepterr2);
        tipoPrepterr2.setId(2L);
        assertThat(tipoPrepterr1).isNotEqualTo(tipoPrepterr2);
        tipoPrepterr1.setId(null);
        assertThat(tipoPrepterr1).isNotEqualTo(tipoPrepterr2);
    }
}
