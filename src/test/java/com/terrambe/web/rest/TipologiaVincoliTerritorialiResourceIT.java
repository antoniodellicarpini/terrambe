package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipologiaVincoliTerritoriali;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.repository.TipologiaVincoliTerritorialiRepository;
import com.terrambe.service.TipologiaVincoliTerritorialiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipologiaVincoliTerritorialiCriteria;
import com.terrambe.service.TipologiaVincoliTerritorialiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipologiaVincoliTerritorialiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipologiaVincoliTerritorialiResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipologiaVincoliTerritorialiRepository tipologiaVincoliTerritorialiRepository;

    @Autowired
    private TipologiaVincoliTerritorialiService tipologiaVincoliTerritorialiService;

    @Autowired
    private TipologiaVincoliTerritorialiQueryService tipologiaVincoliTerritorialiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipologiaVincoliTerritorialiMockMvc;

    private TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipologiaVincoliTerritorialiResource tipologiaVincoliTerritorialiResource = new TipologiaVincoliTerritorialiResource(tipologiaVincoliTerritorialiService, tipologiaVincoliTerritorialiQueryService);
        this.restTipologiaVincoliTerritorialiMockMvc = MockMvcBuilders.standaloneSetup(tipologiaVincoliTerritorialiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaVincoliTerritoriali createEntity(EntityManager em) {
        TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali = new TipologiaVincoliTerritoriali()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipologiaVincoliTerritoriali;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaVincoliTerritoriali createUpdatedEntity(EntityManager em) {
        TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali = new TipologiaVincoliTerritoriali()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipologiaVincoliTerritoriali;
    }

    @BeforeEach
    public void initTest() {
        tipologiaVincoliTerritoriali = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipologiaVincoliTerritoriali() throws Exception {
        int databaseSizeBeforeCreate = tipologiaVincoliTerritorialiRepository.findAll().size();

        // Create the TipologiaVincoliTerritoriali
        restTipologiaVincoliTerritorialiMockMvc.perform(post("/api/tipologia-vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaVincoliTerritoriali)))
            .andExpect(status().isCreated());

        // Validate the TipologiaVincoliTerritoriali in the database
        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeCreate + 1);
        TipologiaVincoliTerritoriali testTipologiaVincoliTerritoriali = tipologiaVincoliTerritorialiList.get(tipologiaVincoliTerritorialiList.size() - 1);
        assertThat(testTipologiaVincoliTerritoriali.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipologiaVincoliTerritoriali.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipologiaVincoliTerritoriali.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipologiaVincoliTerritoriali.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipologiaVincoliTerritoriali.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipologiaVincoliTerritoriali.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipologiaVincoliTerritorialiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipologiaVincoliTerritorialiRepository.findAll().size();

        // Create the TipologiaVincoliTerritoriali with an existing ID
        tipologiaVincoliTerritoriali.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipologiaVincoliTerritorialiMockMvc.perform(post("/api/tipologia-vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaVincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaVincoliTerritoriali in the database
        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipologiaVincoliTerritorialiRepository.findAll().size();
        // set the field null
        tipologiaVincoliTerritoriali.setDescrizione(null);

        // Create the TipologiaVincoliTerritoriali, which fails.

        restTipologiaVincoliTerritorialiMockMvc.perform(post("/api/tipologia-vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaVincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialis() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaVincoliTerritoriali.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipologiaVincoliTerritoriali() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get the tipologiaVincoliTerritoriali
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis/{id}", tipologiaVincoliTerritoriali.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipologiaVincoliTerritoriali.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipologiaVincoliTerritorialiShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipologiaVincoliTerritorialiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipologiaVincoliTerritorialiShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipologiaVincoliTerritorialiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where descrizione is not null
        defaultTipologiaVincoliTerritorialiShouldBeFound("descrizione.specified=true");

        // Get all the tipologiaVincoliTerritorialiList where descrizione is null
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is not null
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.specified=true");

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is null
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is not null
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.specified=true");

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is null
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaVincoliTerritorialiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipologiaVincoliTerritorialiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is not null
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.specified=true");

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is null
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaVincoliTerritorialiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is not null
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is null
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaVincoliTerritorialiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipologiaVincoliTerritorialiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipologiaVincoliTerritorialisByVincoliTerToAppezIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);
        Appezzamenti vincoliTerToAppez = AppezzamentiResourceIT.createEntity(em);
        em.persist(vincoliTerToAppez);
        em.flush();
        tipologiaVincoliTerritoriali.addVincoliTerToAppez(vincoliTerToAppez);
        tipologiaVincoliTerritorialiRepository.saveAndFlush(tipologiaVincoliTerritoriali);
        Long vincoliTerToAppezId = vincoliTerToAppez.getId();

        // Get all the tipologiaVincoliTerritorialiList where vincoliTerToAppez equals to vincoliTerToAppezId
        defaultTipologiaVincoliTerritorialiShouldBeFound("vincoliTerToAppezId.equals=" + vincoliTerToAppezId);

        // Get all the tipologiaVincoliTerritorialiList where vincoliTerToAppez equals to vincoliTerToAppezId + 1
        defaultTipologiaVincoliTerritorialiShouldNotBeFound("vincoliTerToAppezId.equals=" + (vincoliTerToAppezId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipologiaVincoliTerritorialiShouldBeFound(String filter) throws Exception {
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaVincoliTerritoriali.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipologiaVincoliTerritorialiShouldNotBeFound(String filter) throws Exception {
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipologiaVincoliTerritoriali() throws Exception {
        // Get the tipologiaVincoliTerritoriali
        restTipologiaVincoliTerritorialiMockMvc.perform(get("/api/tipologia-vincoli-territorialis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipologiaVincoliTerritoriali() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiService.save(tipologiaVincoliTerritoriali);

        int databaseSizeBeforeUpdate = tipologiaVincoliTerritorialiRepository.findAll().size();

        // Update the tipologiaVincoliTerritoriali
        TipologiaVincoliTerritoriali updatedTipologiaVincoliTerritoriali = tipologiaVincoliTerritorialiRepository.findById(tipologiaVincoliTerritoriali.getId()).get();
        // Disconnect from session so that the updates on updatedTipologiaVincoliTerritoriali are not directly saved in db
        em.detach(updatedTipologiaVincoliTerritoriali);
        updatedTipologiaVincoliTerritoriali
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipologiaVincoliTerritorialiMockMvc.perform(put("/api/tipologia-vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipologiaVincoliTerritoriali)))
            .andExpect(status().isOk());

        // Validate the TipologiaVincoliTerritoriali in the database
        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeUpdate);
        TipologiaVincoliTerritoriali testTipologiaVincoliTerritoriali = tipologiaVincoliTerritorialiList.get(tipologiaVincoliTerritorialiList.size() - 1);
        assertThat(testTipologiaVincoliTerritoriali.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipologiaVincoliTerritoriali.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipologiaVincoliTerritoriali.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipologiaVincoliTerritoriali.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipologiaVincoliTerritoriali.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipologiaVincoliTerritoriali.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipologiaVincoliTerritoriali() throws Exception {
        int databaseSizeBeforeUpdate = tipologiaVincoliTerritorialiRepository.findAll().size();

        // Create the TipologiaVincoliTerritoriali

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipologiaVincoliTerritorialiMockMvc.perform(put("/api/tipologia-vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaVincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaVincoliTerritoriali in the database
        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipologiaVincoliTerritoriali() throws Exception {
        // Initialize the database
        tipologiaVincoliTerritorialiService.save(tipologiaVincoliTerritoriali);

        int databaseSizeBeforeDelete = tipologiaVincoliTerritorialiRepository.findAll().size();

        // Delete the tipologiaVincoliTerritoriali
        restTipologiaVincoliTerritorialiMockMvc.perform(delete("/api/tipologia-vincoli-territorialis/{id}", tipologiaVincoliTerritoriali.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialiList = tipologiaVincoliTerritorialiRepository.findAll();
        assertThat(tipologiaVincoliTerritorialiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipologiaVincoliTerritoriali.class);
        TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali1 = new TipologiaVincoliTerritoriali();
        tipologiaVincoliTerritoriali1.setId(1L);
        TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali2 = new TipologiaVincoliTerritoriali();
        tipologiaVincoliTerritoriali2.setId(tipologiaVincoliTerritoriali1.getId());
        assertThat(tipologiaVincoliTerritoriali1).isEqualTo(tipologiaVincoliTerritoriali2);
        tipologiaVincoliTerritoriali2.setId(2L);
        assertThat(tipologiaVincoliTerritoriali1).isNotEqualTo(tipologiaVincoliTerritoriali2);
        tipologiaVincoliTerritoriali1.setId(null);
        assertThat(tipologiaVincoliTerritoriali1).isNotEqualTo(tipologiaVincoliTerritoriali2);
    }
}
