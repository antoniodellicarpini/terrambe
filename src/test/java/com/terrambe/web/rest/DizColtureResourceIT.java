package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizColture;
import com.terrambe.repository.DizColtureRepository;
import com.terrambe.service.DizColtureService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizColtureCriteria;
import com.terrambe.service.DizColtureQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizColtureResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizColtureResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizColtureRepository dizColtureRepository;

    @Autowired
    private DizColtureService dizColtureService;

    @Autowired
    private DizColtureQueryService dizColtureQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizColtureMockMvc;

    private DizColture dizColture;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizColtureResource dizColtureResource = new DizColtureResource(dizColtureService, dizColtureQueryService);
        this.restDizColtureMockMvc = MockMvcBuilders.standaloneSetup(dizColtureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizColture createEntity(EntityManager em) {
        DizColture dizColture = new DizColture()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizColture;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizColture createUpdatedEntity(EntityManager em) {
        DizColture dizColture = new DizColture()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizColture;
    }

    @BeforeEach
    public void initTest() {
        dizColture = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizColture() throws Exception {
        int databaseSizeBeforeCreate = dizColtureRepository.findAll().size();

        // Create the DizColture
        restDizColtureMockMvc.perform(post("/api/diz-coltures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizColture)))
            .andExpect(status().isCreated());

        // Validate the DizColture in the database
        List<DizColture> dizColtureList = dizColtureRepository.findAll();
        assertThat(dizColtureList).hasSize(databaseSizeBeforeCreate + 1);
        DizColture testDizColture = dizColtureList.get(dizColtureList.size() - 1);
        assertThat(testDizColture.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizColture.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizColture.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizColture.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizColture.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizColture.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizColture.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizColtureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizColtureRepository.findAll().size();

        // Create the DizColture with an existing ID
        dizColture.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizColtureMockMvc.perform(post("/api/diz-coltures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizColture)))
            .andExpect(status().isBadRequest());

        // Validate the DizColture in the database
        List<DizColture> dizColtureList = dizColtureRepository.findAll();
        assertThat(dizColtureList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizColtures() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList
        restDizColtureMockMvc.perform(get("/api/diz-coltures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizColture.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizColture() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get the dizColture
        restDizColtureMockMvc.perform(get("/api/diz-coltures/{id}", dizColture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizColture.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizColturesByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where codice equals to DEFAULT_CODICE
        defaultDizColtureShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizColtureList where codice equals to UPDATED_CODICE
        defaultDizColtureShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizColturesByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizColtureShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizColtureList where codice equals to UPDATED_CODICE
        defaultDizColtureShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizColturesByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where codice is not null
        defaultDizColtureShouldBeFound("codice.specified=true");

        // Get all the dizColtureList where codice is null
        defaultDizColtureShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizColtureShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizColtureList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizColtureShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizColtureShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizColtureList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizColtureShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where descrizione is not null
        defaultDizColtureShouldBeFound("descrizione.specified=true");

        // Get all the dizColtureList where descrizione is null
        defaultDizColtureShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali is not null
        defaultDizColtureShouldBeFound("dataInizVali.specified=true");

        // Get all the dizColtureList where dataInizVali is null
        defaultDizColtureShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizColtureShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizColtureList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizColtureShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali is not null
        defaultDizColtureShouldBeFound("dataFineVali.specified=true");

        // Get all the dizColtureList where dataFineVali is null
        defaultDizColtureShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizColturesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizColtureShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizColtureList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizColtureShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator is not null
        defaultDizColtureShouldBeFound("userIdCreator.specified=true");

        // Get all the dizColtureList where userIdCreator is null
        defaultDizColtureShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizColtureShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizColtureList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizColtureShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod is not null
        defaultDizColtureShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizColtureList where userIdLastMod is null
        defaultDizColtureShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizColturesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizColtureRepository.saveAndFlush(dizColture);

        // Get all the dizColtureList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizColtureShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizColtureList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizColtureShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizColtureShouldBeFound(String filter) throws Exception {
        restDizColtureMockMvc.perform(get("/api/diz-coltures?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizColture.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizColtureMockMvc.perform(get("/api/diz-coltures/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizColtureShouldNotBeFound(String filter) throws Exception {
        restDizColtureMockMvc.perform(get("/api/diz-coltures?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizColtureMockMvc.perform(get("/api/diz-coltures/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizColture() throws Exception {
        // Get the dizColture
        restDizColtureMockMvc.perform(get("/api/diz-coltures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizColture() throws Exception {
        // Initialize the database
        dizColtureService.save(dizColture);

        int databaseSizeBeforeUpdate = dizColtureRepository.findAll().size();

        // Update the dizColture
        DizColture updatedDizColture = dizColtureRepository.findById(dizColture.getId()).get();
        // Disconnect from session so that the updates on updatedDizColture are not directly saved in db
        em.detach(updatedDizColture);
        updatedDizColture
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizColtureMockMvc.perform(put("/api/diz-coltures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizColture)))
            .andExpect(status().isOk());

        // Validate the DizColture in the database
        List<DizColture> dizColtureList = dizColtureRepository.findAll();
        assertThat(dizColtureList).hasSize(databaseSizeBeforeUpdate);
        DizColture testDizColture = dizColtureList.get(dizColtureList.size() - 1);
        assertThat(testDizColture.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizColture.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizColture.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizColture.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizColture.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizColture.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizColture.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizColture() throws Exception {
        int databaseSizeBeforeUpdate = dizColtureRepository.findAll().size();

        // Create the DizColture

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizColtureMockMvc.perform(put("/api/diz-coltures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizColture)))
            .andExpect(status().isBadRequest());

        // Validate the DizColture in the database
        List<DizColture> dizColtureList = dizColtureRepository.findAll();
        assertThat(dizColtureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizColture() throws Exception {
        // Initialize the database
        dizColtureService.save(dizColture);

        int databaseSizeBeforeDelete = dizColtureRepository.findAll().size();

        // Delete the dizColture
        restDizColtureMockMvc.perform(delete("/api/diz-coltures/{id}", dizColture.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizColture> dizColtureList = dizColtureRepository.findAll();
        assertThat(dizColtureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizColture.class);
        DizColture dizColture1 = new DizColture();
        dizColture1.setId(1L);
        DizColture dizColture2 = new DizColture();
        dizColture2.setId(dizColture1.getId());
        assertThat(dizColture1).isEqualTo(dizColture2);
        dizColture2.setId(2L);
        assertThat(dizColture1).isNotEqualTo(dizColture2);
        dizColture1.setId(null);
        assertThat(dizColture1).isNotEqualTo(dizColture2);
    }
}
