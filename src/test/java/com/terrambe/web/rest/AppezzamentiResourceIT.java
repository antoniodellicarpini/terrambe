package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.domain.TipologiaVincoliTerritoriali;
import com.terrambe.domain.Campi;
import com.terrambe.domain.BioAppzTipo;
import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.repository.AppezzamentiRepository;
import com.terrambe.service.AppezzamentiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AppezzamentiCriteria;
import com.terrambe.service.AppezzamentiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AppezzamentiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AppezzamentiResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final String DEFAULT_NOME_APPEZZAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_APPEZZAMENTO = "BBBBBBBBBB";

    private static final Float DEFAULT_SUPERFICE_HA = 1F;
    private static final Float UPDATED_SUPERFICE_HA = 2F;
    private static final Float SMALLER_SUPERFICE_HA = 1F - 1F;

    private static final Boolean DEFAULT_ROTAZIONE = false;
    private static final Boolean UPDATED_ROTAZIONE = true;

    private static final Boolean DEFAULT_SERRA = false;
    private static final Boolean UPDATED_SERRA = true;

    private static final Boolean DEFAULT_IRRIGABILE = false;
    private static final Boolean UPDATED_IRRIGABILE = true;

    private static final String DEFAULT_UUID_ESRI = "AAAAAAAAAA";
    private static final String UPDATED_UUID_ESRI = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AppezzamentiRepository appezzamentiRepository;

    @Mock
    private AppezzamentiRepository appezzamentiRepositoryMock;

    @Mock
    private AppezzamentiService appezzamentiServiceMock;

    @Autowired
    private AppezzamentiService appezzamentiService;

    @Autowired
    private AppezzamentiQueryService appezzamentiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAppezzamentiMockMvc;

    private Appezzamenti appezzamenti;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AppezzamentiResource appezzamentiResource = new AppezzamentiResource(appezzamentiService, appezzamentiQueryService);
        this.restAppezzamentiMockMvc = MockMvcBuilders.standaloneSetup(appezzamentiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Appezzamenti createEntity(EntityManager em) {
        Appezzamenti appezzamenti = new Appezzamenti()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .nomeAppezzamento(DEFAULT_NOME_APPEZZAMENTO)
            .superficeHa(DEFAULT_SUPERFICE_HA)
            .rotazione(DEFAULT_ROTAZIONE)
            .serra(DEFAULT_SERRA)
            .irrigabile(DEFAULT_IRRIGABILE)
            .uuidEsri(DEFAULT_UUID_ESRI)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return appezzamenti;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Appezzamenti createUpdatedEntity(EntityManager em) {
        Appezzamenti appezzamenti = new Appezzamenti()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .nomeAppezzamento(UPDATED_NOME_APPEZZAMENTO)
            .superficeHa(UPDATED_SUPERFICE_HA)
            .rotazione(UPDATED_ROTAZIONE)
            .serra(UPDATED_SERRA)
            .irrigabile(UPDATED_IRRIGABILE)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return appezzamenti;
    }

    @BeforeEach
    public void initTest() {
        appezzamenti = createEntity(em);
    }

    @Test
    @Transactional
    public void createAppezzamenti() throws Exception {
        int databaseSizeBeforeCreate = appezzamentiRepository.findAll().size();

        // Create the Appezzamenti
        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isCreated());

        // Validate the Appezzamenti in the database
        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeCreate + 1);
        Appezzamenti testAppezzamenti = appezzamentiList.get(appezzamentiList.size() - 1);
        assertThat(testAppezzamenti.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testAppezzamenti.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testAppezzamenti.getNomeAppezzamento()).isEqualTo(DEFAULT_NOME_APPEZZAMENTO);
        assertThat(testAppezzamenti.getSuperficeHa()).isEqualTo(DEFAULT_SUPERFICE_HA);
        assertThat(testAppezzamenti.isRotazione()).isEqualTo(DEFAULT_ROTAZIONE);
        assertThat(testAppezzamenti.isSerra()).isEqualTo(DEFAULT_SERRA);
        assertThat(testAppezzamenti.isIrrigabile()).isEqualTo(DEFAULT_IRRIGABILE);
        assertThat(testAppezzamenti.getUuidEsri()).isEqualTo(DEFAULT_UUID_ESRI);
        assertThat(testAppezzamenti.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAppezzamenti.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAppezzamenti.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAppezzamenti.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAppezzamenti.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAppezzamentiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = appezzamentiRepository.findAll().size();

        // Create the Appezzamenti with an existing ID
        appezzamenti.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        // Validate the Appezzamenti in the database
        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setIdAzienda(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeAppezzamentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setNomeAppezzamento(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSuperficeHaIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setSuperficeHa(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRotazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setRotazione(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSerraIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setSerra(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIrrigabileIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setIrrigabile(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUuidEsriIsRequired() throws Exception {
        int databaseSizeBeforeTest = appezzamentiRepository.findAll().size();
        // set the field null
        appezzamenti.setUuidEsri(null);

        // Create the Appezzamenti, which fails.

        restAppezzamentiMockMvc.perform(post("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAppezzamentis() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(appezzamenti.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].nomeAppezzamento").value(hasItem(DEFAULT_NOME_APPEZZAMENTO.toString())))
            .andExpect(jsonPath("$.[*].superficeHa").value(hasItem(DEFAULT_SUPERFICE_HA.doubleValue())))
            .andExpect(jsonPath("$.[*].rotazione").value(hasItem(DEFAULT_ROTAZIONE.booleanValue())))
            .andExpect(jsonPath("$.[*].serra").value(hasItem(DEFAULT_SERRA.booleanValue())))
            .andExpect(jsonPath("$.[*].irrigabile").value(hasItem(DEFAULT_IRRIGABILE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAppezzamentisWithEagerRelationshipsIsEnabled() throws Exception {
        AppezzamentiResource appezzamentiResource = new AppezzamentiResource(appezzamentiServiceMock, appezzamentiQueryService);
        when(appezzamentiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restAppezzamentiMockMvc = MockMvcBuilders.standaloneSetup(appezzamentiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAppezzamentiMockMvc.perform(get("/api/appezzamentis?eagerload=true"))
        .andExpect(status().isOk());

        verify(appezzamentiServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAppezzamentisWithEagerRelationshipsIsNotEnabled() throws Exception {
        AppezzamentiResource appezzamentiResource = new AppezzamentiResource(appezzamentiServiceMock, appezzamentiQueryService);
            when(appezzamentiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restAppezzamentiMockMvc = MockMvcBuilders.standaloneSetup(appezzamentiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAppezzamentiMockMvc.perform(get("/api/appezzamentis?eagerload=true"))
        .andExpect(status().isOk());

            verify(appezzamentiServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAppezzamenti() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get the appezzamenti
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis/{id}", appezzamenti.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(appezzamenti.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.nomeAppezzamento").value(DEFAULT_NOME_APPEZZAMENTO.toString()))
            .andExpect(jsonPath("$.superficeHa").value(DEFAULT_SUPERFICE_HA.doubleValue()))
            .andExpect(jsonPath("$.rotazione").value(DEFAULT_ROTAZIONE.booleanValue()))
            .andExpect(jsonPath("$.serra").value(DEFAULT_SERRA.booleanValue()))
            .andExpect(jsonPath("$.irrigabile").value(DEFAULT_IRRIGABILE.booleanValue()))
            .andExpect(jsonPath("$.uuidEsri").value(DEFAULT_UUID_ESRI.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda is not null
        defaultAppezzamentiShouldBeFound("idAzienda.specified=true");

        // Get all the appezzamentiList where idAzienda is null
        defaultAppezzamentiShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultAppezzamentiShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the appezzamentiList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultAppezzamentiShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd is not null
        defaultAppezzamentiShouldBeFound("idUnitaProd.specified=true");

        // Get all the appezzamentiList where idUnitaProd is null
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultAppezzamentiShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the appezzamentiList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultAppezzamentiShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByNomeAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where nomeAppezzamento equals to DEFAULT_NOME_APPEZZAMENTO
        defaultAppezzamentiShouldBeFound("nomeAppezzamento.equals=" + DEFAULT_NOME_APPEZZAMENTO);

        // Get all the appezzamentiList where nomeAppezzamento equals to UPDATED_NOME_APPEZZAMENTO
        defaultAppezzamentiShouldNotBeFound("nomeAppezzamento.equals=" + UPDATED_NOME_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByNomeAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where nomeAppezzamento in DEFAULT_NOME_APPEZZAMENTO or UPDATED_NOME_APPEZZAMENTO
        defaultAppezzamentiShouldBeFound("nomeAppezzamento.in=" + DEFAULT_NOME_APPEZZAMENTO + "," + UPDATED_NOME_APPEZZAMENTO);

        // Get all the appezzamentiList where nomeAppezzamento equals to UPDATED_NOME_APPEZZAMENTO
        defaultAppezzamentiShouldNotBeFound("nomeAppezzamento.in=" + UPDATED_NOME_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByNomeAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where nomeAppezzamento is not null
        defaultAppezzamentiShouldBeFound("nomeAppezzamento.specified=true");

        // Get all the appezzamentiList where nomeAppezzamento is null
        defaultAppezzamentiShouldNotBeFound("nomeAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa equals to DEFAULT_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.equals=" + DEFAULT_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa equals to UPDATED_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.equals=" + UPDATED_SUPERFICE_HA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa in DEFAULT_SUPERFICE_HA or UPDATED_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.in=" + DEFAULT_SUPERFICE_HA + "," + UPDATED_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa equals to UPDATED_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.in=" + UPDATED_SUPERFICE_HA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa is not null
        defaultAppezzamentiShouldBeFound("superficeHa.specified=true");

        // Get all the appezzamentiList where superficeHa is null
        defaultAppezzamentiShouldNotBeFound("superficeHa.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa is greater than or equal to DEFAULT_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.greaterThanOrEqual=" + DEFAULT_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa is greater than or equal to UPDATED_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.greaterThanOrEqual=" + UPDATED_SUPERFICE_HA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa is less than or equal to DEFAULT_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.lessThanOrEqual=" + DEFAULT_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa is less than or equal to SMALLER_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.lessThanOrEqual=" + SMALLER_SUPERFICE_HA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa is less than DEFAULT_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.lessThan=" + DEFAULT_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa is less than UPDATED_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.lessThan=" + UPDATED_SUPERFICE_HA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySuperficeHaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where superficeHa is greater than DEFAULT_SUPERFICE_HA
        defaultAppezzamentiShouldNotBeFound("superficeHa.greaterThan=" + DEFAULT_SUPERFICE_HA);

        // Get all the appezzamentiList where superficeHa is greater than SMALLER_SUPERFICE_HA
        defaultAppezzamentiShouldBeFound("superficeHa.greaterThan=" + SMALLER_SUPERFICE_HA);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByRotazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where rotazione equals to DEFAULT_ROTAZIONE
        defaultAppezzamentiShouldBeFound("rotazione.equals=" + DEFAULT_ROTAZIONE);

        // Get all the appezzamentiList where rotazione equals to UPDATED_ROTAZIONE
        defaultAppezzamentiShouldNotBeFound("rotazione.equals=" + UPDATED_ROTAZIONE);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByRotazioneIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where rotazione in DEFAULT_ROTAZIONE or UPDATED_ROTAZIONE
        defaultAppezzamentiShouldBeFound("rotazione.in=" + DEFAULT_ROTAZIONE + "," + UPDATED_ROTAZIONE);

        // Get all the appezzamentiList where rotazione equals to UPDATED_ROTAZIONE
        defaultAppezzamentiShouldNotBeFound("rotazione.in=" + UPDATED_ROTAZIONE);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByRotazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where rotazione is not null
        defaultAppezzamentiShouldBeFound("rotazione.specified=true");

        // Get all the appezzamentiList where rotazione is null
        defaultAppezzamentiShouldNotBeFound("rotazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySerraIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where serra equals to DEFAULT_SERRA
        defaultAppezzamentiShouldBeFound("serra.equals=" + DEFAULT_SERRA);

        // Get all the appezzamentiList where serra equals to UPDATED_SERRA
        defaultAppezzamentiShouldNotBeFound("serra.equals=" + UPDATED_SERRA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySerraIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where serra in DEFAULT_SERRA or UPDATED_SERRA
        defaultAppezzamentiShouldBeFound("serra.in=" + DEFAULT_SERRA + "," + UPDATED_SERRA);

        // Get all the appezzamentiList where serra equals to UPDATED_SERRA
        defaultAppezzamentiShouldNotBeFound("serra.in=" + UPDATED_SERRA);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisBySerraIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where serra is not null
        defaultAppezzamentiShouldBeFound("serra.specified=true");

        // Get all the appezzamentiList where serra is null
        defaultAppezzamentiShouldNotBeFound("serra.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIrrigabileIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where irrigabile equals to DEFAULT_IRRIGABILE
        defaultAppezzamentiShouldBeFound("irrigabile.equals=" + DEFAULT_IRRIGABILE);

        // Get all the appezzamentiList where irrigabile equals to UPDATED_IRRIGABILE
        defaultAppezzamentiShouldNotBeFound("irrigabile.equals=" + UPDATED_IRRIGABILE);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIrrigabileIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where irrigabile in DEFAULT_IRRIGABILE or UPDATED_IRRIGABILE
        defaultAppezzamentiShouldBeFound("irrigabile.in=" + DEFAULT_IRRIGABILE + "," + UPDATED_IRRIGABILE);

        // Get all the appezzamentiList where irrigabile equals to UPDATED_IRRIGABILE
        defaultAppezzamentiShouldNotBeFound("irrigabile.in=" + UPDATED_IRRIGABILE);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByIrrigabileIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where irrigabile is not null
        defaultAppezzamentiShouldBeFound("irrigabile.specified=true");

        // Get all the appezzamentiList where irrigabile is null
        defaultAppezzamentiShouldNotBeFound("irrigabile.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUuidEsriIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where uuidEsri equals to DEFAULT_UUID_ESRI
        defaultAppezzamentiShouldBeFound("uuidEsri.equals=" + DEFAULT_UUID_ESRI);

        // Get all the appezzamentiList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAppezzamentiShouldNotBeFound("uuidEsri.equals=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUuidEsriIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where uuidEsri in DEFAULT_UUID_ESRI or UPDATED_UUID_ESRI
        defaultAppezzamentiShouldBeFound("uuidEsri.in=" + DEFAULT_UUID_ESRI + "," + UPDATED_UUID_ESRI);

        // Get all the appezzamentiList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAppezzamentiShouldNotBeFound("uuidEsri.in=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUuidEsriIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where uuidEsri is not null
        defaultAppezzamentiShouldBeFound("uuidEsri.specified=true");

        // Get all the appezzamentiList where uuidEsri is null
        defaultAppezzamentiShouldNotBeFound("uuidEsri.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali is not null
        defaultAppezzamentiShouldBeFound("dataInizVali.specified=true");

        // Get all the appezzamentiList where dataInizVali is null
        defaultAppezzamentiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAppezzamentiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the appezzamentiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAppezzamentiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali is not null
        defaultAppezzamentiShouldBeFound("dataFineVali.specified=true");

        // Get all the appezzamentiList where dataFineVali is null
        defaultAppezzamentiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAppezzamentiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the appezzamentiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAppezzamentiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator is not null
        defaultAppezzamentiShouldBeFound("userIdCreator.specified=true");

        // Get all the appezzamentiList where userIdCreator is null
        defaultAppezzamentiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAppezzamentiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the appezzamentiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAppezzamentiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod is not null
        defaultAppezzamentiShouldBeFound("userIdLastMod.specified=true");

        // Get all the appezzamentiList where userIdLastMod is null
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAppezzamentisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);

        // Get all the appezzamentiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAppezzamentiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the appezzamentiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAppezzamentiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByAppzToPartIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);
        AziSuperficieParticella appzToPart = AziSuperficieParticellaResourceIT.createEntity(em);
        em.persist(appzToPart);
        em.flush();
        appezzamenti.addAppzToPart(appzToPart);
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Long appzToPartId = appzToPart.getId();

        // Get all the appezzamentiList where appzToPart equals to appzToPartId
        defaultAppezzamentiShouldBeFound("appzToPartId.equals=" + appzToPartId);

        // Get all the appezzamentiList where appzToPart equals to appzToPartId + 1
        defaultAppezzamentiShouldNotBeFound("appzToPartId.equals=" + (appzToPartId + 1));
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByAppezToVincoliTerIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);
        TipologiaVincoliTerritoriali appezToVincoliTer = TipologiaVincoliTerritorialiResourceIT.createEntity(em);
        em.persist(appezToVincoliTer);
        em.flush();
        appezzamenti.addAppezToVincoliTer(appezToVincoliTer);
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Long appezToVincoliTerId = appezToVincoliTer.getId();

        // Get all the appezzamentiList where appezToVincoliTer equals to appezToVincoliTerId
        defaultAppezzamentiShouldBeFound("appezToVincoliTerId.equals=" + appezToVincoliTerId);

        // Get all the appezzamentiList where appezToVincoliTer equals to appezToVincoliTerId + 1
        defaultAppezzamentiShouldNotBeFound("appezToVincoliTerId.equals=" + (appezToVincoliTerId + 1));
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByAppezToCampiIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Campi appezToCampi = CampiResourceIT.createEntity(em);
        em.persist(appezToCampi);
        em.flush();
        appezzamenti.setAppezToCampi(appezToCampi);
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Long appezToCampiId = appezToCampi.getId();

        // Get all the appezzamentiList where appezToCampi equals to appezToCampiId
        defaultAppezzamentiShouldBeFound("appezToCampiId.equals=" + appezToCampiId);

        // Get all the appezzamentiList where appezToCampi equals to appezToCampiId + 1
        defaultAppezzamentiShouldNotBeFound("appezToCampiId.equals=" + (appezToCampiId + 1));
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByAppzToBioAppzTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);
        BioAppzTipo appzToBioAppzTipo = BioAppzTipoResourceIT.createEntity(em);
        em.persist(appzToBioAppzTipo);
        em.flush();
        appezzamenti.setAppzToBioAppzTipo(appzToBioAppzTipo);
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Long appzToBioAppzTipoId = appzToBioAppzTipo.getId();

        // Get all the appezzamentiList where appzToBioAppzTipo equals to appzToBioAppzTipoId
        defaultAppezzamentiShouldBeFound("appzToBioAppzTipoId.equals=" + appzToBioAppzTipoId);

        // Get all the appezzamentiList where appzToBioAppzTipo equals to appzToBioAppzTipoId + 1
        defaultAppezzamentiShouldNotBeFound("appzToBioAppzTipoId.equals=" + (appzToBioAppzTipoId + 1));
    }


    @Test
    @Transactional
    public void getAllAppezzamentisByAppeToColtIsEqualToSomething() throws Exception {
        // Initialize the database
        appezzamentiRepository.saveAndFlush(appezzamenti);
        ColturaAppezzamento appeToColt = ColturaAppezzamentoResourceIT.createEntity(em);
        em.persist(appeToColt);
        em.flush();
        appezzamenti.addAppeToColt(appeToColt);
        appezzamentiRepository.saveAndFlush(appezzamenti);
        Long appeToColtId = appeToColt.getId();

        // Get all the appezzamentiList where appeToColt equals to appeToColtId
        defaultAppezzamentiShouldBeFound("appeToColtId.equals=" + appeToColtId);

        // Get all the appezzamentiList where appeToColt equals to appeToColtId + 1
        defaultAppezzamentiShouldNotBeFound("appeToColtId.equals=" + (appeToColtId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAppezzamentiShouldBeFound(String filter) throws Exception {
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(appezzamenti.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].nomeAppezzamento").value(hasItem(DEFAULT_NOME_APPEZZAMENTO)))
            .andExpect(jsonPath("$.[*].superficeHa").value(hasItem(DEFAULT_SUPERFICE_HA.doubleValue())))
            .andExpect(jsonPath("$.[*].rotazione").value(hasItem(DEFAULT_ROTAZIONE.booleanValue())))
            .andExpect(jsonPath("$.[*].serra").value(hasItem(DEFAULT_SERRA.booleanValue())))
            .andExpect(jsonPath("$.[*].irrigabile").value(hasItem(DEFAULT_IRRIGABILE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAppezzamentiShouldNotBeFound(String filter) throws Exception {
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAppezzamenti() throws Exception {
        // Get the appezzamenti
        restAppezzamentiMockMvc.perform(get("/api/appezzamentis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAppezzamenti() throws Exception {
        // Initialize the database
        appezzamentiService.save(appezzamenti);

        int databaseSizeBeforeUpdate = appezzamentiRepository.findAll().size();

        // Update the appezzamenti
        Appezzamenti updatedAppezzamenti = appezzamentiRepository.findById(appezzamenti.getId()).get();
        // Disconnect from session so that the updates on updatedAppezzamenti are not directly saved in db
        em.detach(updatedAppezzamenti);
        updatedAppezzamenti
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .nomeAppezzamento(UPDATED_NOME_APPEZZAMENTO)
            .superficeHa(UPDATED_SUPERFICE_HA)
            .rotazione(UPDATED_ROTAZIONE)
            .serra(UPDATED_SERRA)
            .irrigabile(UPDATED_IRRIGABILE)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAppezzamentiMockMvc.perform(put("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAppezzamenti)))
            .andExpect(status().isOk());

        // Validate the Appezzamenti in the database
        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeUpdate);
        Appezzamenti testAppezzamenti = appezzamentiList.get(appezzamentiList.size() - 1);
        assertThat(testAppezzamenti.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testAppezzamenti.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testAppezzamenti.getNomeAppezzamento()).isEqualTo(UPDATED_NOME_APPEZZAMENTO);
        assertThat(testAppezzamenti.getSuperficeHa()).isEqualTo(UPDATED_SUPERFICE_HA);
        assertThat(testAppezzamenti.isRotazione()).isEqualTo(UPDATED_ROTAZIONE);
        assertThat(testAppezzamenti.isSerra()).isEqualTo(UPDATED_SERRA);
        assertThat(testAppezzamenti.isIrrigabile()).isEqualTo(UPDATED_IRRIGABILE);
        assertThat(testAppezzamenti.getUuidEsri()).isEqualTo(UPDATED_UUID_ESRI);
        assertThat(testAppezzamenti.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAppezzamenti.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAppezzamenti.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAppezzamenti.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAppezzamenti.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAppezzamenti() throws Exception {
        int databaseSizeBeforeUpdate = appezzamentiRepository.findAll().size();

        // Create the Appezzamenti

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAppezzamentiMockMvc.perform(put("/api/appezzamentis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appezzamenti)))
            .andExpect(status().isBadRequest());

        // Validate the Appezzamenti in the database
        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAppezzamenti() throws Exception {
        // Initialize the database
        appezzamentiService.save(appezzamenti);

        int databaseSizeBeforeDelete = appezzamentiRepository.findAll().size();

        // Delete the appezzamenti
        restAppezzamentiMockMvc.perform(delete("/api/appezzamentis/{id}", appezzamenti.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Appezzamenti> appezzamentiList = appezzamentiRepository.findAll();
        assertThat(appezzamentiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Appezzamenti.class);
        Appezzamenti appezzamenti1 = new Appezzamenti();
        appezzamenti1.setId(1L);
        Appezzamenti appezzamenti2 = new Appezzamenti();
        appezzamenti2.setId(appezzamenti1.getId());
        assertThat(appezzamenti1).isEqualTo(appezzamenti2);
        appezzamenti2.setId(2L);
        assertThat(appezzamenti1).isNotEqualTo(appezzamenti2);
        appezzamenti1.setId(null);
        assertThat(appezzamenti1).isNotEqualTo(appezzamenti2);
    }
}
