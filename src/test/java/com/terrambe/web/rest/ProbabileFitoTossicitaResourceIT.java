package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.ProbabileFitoTossicita;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.ProbabileFitoTossicitaRepository;
import com.terrambe.service.ProbabileFitoTossicitaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ProbabileFitoTossicitaCriteria;
import com.terrambe.service.ProbabileFitoTossicitaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProbabileFitoTossicitaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ProbabileFitoTossicitaResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private ProbabileFitoTossicitaRepository probabileFitoTossicitaRepository;

    @Autowired
    private ProbabileFitoTossicitaService probabileFitoTossicitaService;

    @Autowired
    private ProbabileFitoTossicitaQueryService probabileFitoTossicitaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProbabileFitoTossicitaMockMvc;

    private ProbabileFitoTossicita probabileFitoTossicita;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProbabileFitoTossicitaResource probabileFitoTossicitaResource = new ProbabileFitoTossicitaResource(probabileFitoTossicitaService, probabileFitoTossicitaQueryService);
        this.restProbabileFitoTossicitaMockMvc = MockMvcBuilders.standaloneSetup(probabileFitoTossicitaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProbabileFitoTossicita createEntity(EntityManager em) {
        ProbabileFitoTossicita probabileFitoTossicita = new ProbabileFitoTossicita()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return probabileFitoTossicita;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProbabileFitoTossicita createUpdatedEntity(EntityManager em) {
        ProbabileFitoTossicita probabileFitoTossicita = new ProbabileFitoTossicita()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return probabileFitoTossicita;
    }

    @BeforeEach
    public void initTest() {
        probabileFitoTossicita = createEntity(em);
    }

    @Test
    @Transactional
    public void createProbabileFitoTossicita() throws Exception {
        int databaseSizeBeforeCreate = probabileFitoTossicitaRepository.findAll().size();

        // Create the ProbabileFitoTossicita
        restProbabileFitoTossicitaMockMvc.perform(post("/api/probabile-fito-tossicitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probabileFitoTossicita)))
            .andExpect(status().isCreated());

        // Validate the ProbabileFitoTossicita in the database
        List<ProbabileFitoTossicita> probabileFitoTossicitaList = probabileFitoTossicitaRepository.findAll();
        assertThat(probabileFitoTossicitaList).hasSize(databaseSizeBeforeCreate + 1);
        ProbabileFitoTossicita testProbabileFitoTossicita = probabileFitoTossicitaList.get(probabileFitoTossicitaList.size() - 1);
        assertThat(testProbabileFitoTossicita.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testProbabileFitoTossicita.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testProbabileFitoTossicita.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testProbabileFitoTossicita.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testProbabileFitoTossicita.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testProbabileFitoTossicita.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testProbabileFitoTossicita.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testProbabileFitoTossicita.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createProbabileFitoTossicitaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = probabileFitoTossicitaRepository.findAll().size();

        // Create the ProbabileFitoTossicita with an existing ID
        probabileFitoTossicita.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProbabileFitoTossicitaMockMvc.perform(post("/api/probabile-fito-tossicitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probabileFitoTossicita)))
            .andExpect(status().isBadRequest());

        // Validate the ProbabileFitoTossicita in the database
        List<ProbabileFitoTossicita> probabileFitoTossicitaList = probabileFitoTossicitaRepository.findAll();
        assertThat(probabileFitoTossicitaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProbabileFitoTossicitas() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(probabileFitoTossicita.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getProbabileFitoTossicita() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get the probabileFitoTossicita
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas/{id}", probabileFitoTossicita.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(probabileFitoTossicita.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultProbabileFitoTossicitaShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the probabileFitoTossicitaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultProbabileFitoTossicitaShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultProbabileFitoTossicitaShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the probabileFitoTossicitaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultProbabileFitoTossicitaShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where tipoImport is not null
        defaultProbabileFitoTossicitaShouldBeFound("tipoImport.specified=true");

        // Get all the probabileFitoTossicitaList where tipoImport is null
        defaultProbabileFitoTossicitaShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where operatore equals to DEFAULT_OPERATORE
        defaultProbabileFitoTossicitaShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the probabileFitoTossicitaList where operatore equals to UPDATED_OPERATORE
        defaultProbabileFitoTossicitaShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultProbabileFitoTossicitaShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the probabileFitoTossicitaList where operatore equals to UPDATED_OPERATORE
        defaultProbabileFitoTossicitaShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where operatore is not null
        defaultProbabileFitoTossicitaShouldBeFound("operatore.specified=true");

        // Get all the probabileFitoTossicitaList where operatore is null
        defaultProbabileFitoTossicitaShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where ts equals to DEFAULT_TS
        defaultProbabileFitoTossicitaShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the probabileFitoTossicitaList where ts equals to UPDATED_TS
        defaultProbabileFitoTossicitaShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTsIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where ts in DEFAULT_TS or UPDATED_TS
        defaultProbabileFitoTossicitaShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the probabileFitoTossicitaList where ts equals to UPDATED_TS
        defaultProbabileFitoTossicitaShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where ts is not null
        defaultProbabileFitoTossicitaShouldBeFound("ts.specified=true");

        // Get all the probabileFitoTossicitaList where ts is null
        defaultProbabileFitoTossicitaShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali is not null
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.specified=true");

        // Get all the probabileFitoTossicitaList where dataInizVali is null
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the probabileFitoTossicitaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali is not null
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.specified=true");

        // Get all the probabileFitoTossicitaList where dataFineVali is null
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the probabileFitoTossicitaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultProbabileFitoTossicitaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator is not null
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.specified=true");

        // Get all the probabileFitoTossicitaList where userIdCreator is null
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the probabileFitoTossicitaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultProbabileFitoTossicitaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod is not null
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.specified=true");

        // Get all the probabileFitoTossicitaList where userIdLastMod is null
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);

        // Get all the probabileFitoTossicitaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the probabileFitoTossicitaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultProbabileFitoTossicitaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllProbabileFitoTossicitasByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        probabileFitoTossicita.setEtichettaFito(etichettaFito);
        probabileFitoTossicitaRepository.saveAndFlush(probabileFitoTossicita);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the probabileFitoTossicitaList where etichettaFito equals to etichettaFitoId
        defaultProbabileFitoTossicitaShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the probabileFitoTossicitaList where etichettaFito equals to etichettaFitoId + 1
        defaultProbabileFitoTossicitaShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProbabileFitoTossicitaShouldBeFound(String filter) throws Exception {
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(probabileFitoTossicita.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProbabileFitoTossicitaShouldNotBeFound(String filter) throws Exception {
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProbabileFitoTossicita() throws Exception {
        // Get the probabileFitoTossicita
        restProbabileFitoTossicitaMockMvc.perform(get("/api/probabile-fito-tossicitas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProbabileFitoTossicita() throws Exception {
        // Initialize the database
        probabileFitoTossicitaService.save(probabileFitoTossicita);

        int databaseSizeBeforeUpdate = probabileFitoTossicitaRepository.findAll().size();

        // Update the probabileFitoTossicita
        ProbabileFitoTossicita updatedProbabileFitoTossicita = probabileFitoTossicitaRepository.findById(probabileFitoTossicita.getId()).get();
        // Disconnect from session so that the updates on updatedProbabileFitoTossicita are not directly saved in db
        em.detach(updatedProbabileFitoTossicita);
        updatedProbabileFitoTossicita
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restProbabileFitoTossicitaMockMvc.perform(put("/api/probabile-fito-tossicitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProbabileFitoTossicita)))
            .andExpect(status().isOk());

        // Validate the ProbabileFitoTossicita in the database
        List<ProbabileFitoTossicita> probabileFitoTossicitaList = probabileFitoTossicitaRepository.findAll();
        assertThat(probabileFitoTossicitaList).hasSize(databaseSizeBeforeUpdate);
        ProbabileFitoTossicita testProbabileFitoTossicita = probabileFitoTossicitaList.get(probabileFitoTossicitaList.size() - 1);
        assertThat(testProbabileFitoTossicita.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testProbabileFitoTossicita.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testProbabileFitoTossicita.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testProbabileFitoTossicita.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testProbabileFitoTossicita.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testProbabileFitoTossicita.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testProbabileFitoTossicita.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testProbabileFitoTossicita.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingProbabileFitoTossicita() throws Exception {
        int databaseSizeBeforeUpdate = probabileFitoTossicitaRepository.findAll().size();

        // Create the ProbabileFitoTossicita

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProbabileFitoTossicitaMockMvc.perform(put("/api/probabile-fito-tossicitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probabileFitoTossicita)))
            .andExpect(status().isBadRequest());

        // Validate the ProbabileFitoTossicita in the database
        List<ProbabileFitoTossicita> probabileFitoTossicitaList = probabileFitoTossicitaRepository.findAll();
        assertThat(probabileFitoTossicitaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProbabileFitoTossicita() throws Exception {
        // Initialize the database
        probabileFitoTossicitaService.save(probabileFitoTossicita);

        int databaseSizeBeforeDelete = probabileFitoTossicitaRepository.findAll().size();

        // Delete the probabileFitoTossicita
        restProbabileFitoTossicitaMockMvc.perform(delete("/api/probabile-fito-tossicitas/{id}", probabileFitoTossicita.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProbabileFitoTossicita> probabileFitoTossicitaList = probabileFitoTossicitaRepository.findAll();
        assertThat(probabileFitoTossicitaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProbabileFitoTossicita.class);
        ProbabileFitoTossicita probabileFitoTossicita1 = new ProbabileFitoTossicita();
        probabileFitoTossicita1.setId(1L);
        ProbabileFitoTossicita probabileFitoTossicita2 = new ProbabileFitoTossicita();
        probabileFitoTossicita2.setId(probabileFitoTossicita1.getId());
        assertThat(probabileFitoTossicita1).isEqualTo(probabileFitoTossicita2);
        probabileFitoTossicita2.setId(2L);
        assertThat(probabileFitoTossicita1).isNotEqualTo(probabileFitoTossicita2);
        probabileFitoTossicita1.setId(null);
        assertThat(probabileFitoTossicita1).isNotEqualTo(probabileFitoTossicita2);
    }
}
