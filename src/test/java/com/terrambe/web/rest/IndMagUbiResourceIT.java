package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndMagUbi;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.repository.IndMagUbiRepository;
import com.terrambe.service.IndMagUbiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndMagUbiCriteria;
import com.terrambe.service.IndMagUbiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndMagUbiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndMagUbiResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndMagUbiRepository indMagUbiRepository;

    @Autowired
    private IndMagUbiService indMagUbiService;

    @Autowired
    private IndMagUbiQueryService indMagUbiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndMagUbiMockMvc;

    private IndMagUbi indMagUbi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndMagUbiResource indMagUbiResource = new IndMagUbiResource(indMagUbiService, indMagUbiQueryService);
        this.restIndMagUbiMockMvc = MockMvcBuilders.standaloneSetup(indMagUbiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndMagUbi createEntity(EntityManager em) {
        IndMagUbi indMagUbi = new IndMagUbi()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indMagUbi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndMagUbi createUpdatedEntity(EntityManager em) {
        IndMagUbi indMagUbi = new IndMagUbi()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indMagUbi;
    }

    @BeforeEach
    public void initTest() {
        indMagUbi = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndMagUbi() throws Exception {
        int databaseSizeBeforeCreate = indMagUbiRepository.findAll().size();

        // Create the IndMagUbi
        restIndMagUbiMockMvc.perform(post("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indMagUbi)))
            .andExpect(status().isCreated());

        // Validate the IndMagUbi in the database
        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeCreate + 1);
        IndMagUbi testIndMagUbi = indMagUbiList.get(indMagUbiList.size() - 1);
        assertThat(testIndMagUbi.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndMagUbi.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndMagUbi.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndMagUbi.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndMagUbi.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndMagUbi.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndMagUbi.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndMagUbi.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndMagUbi.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndMagUbi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndMagUbi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndMagUbi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndMagUbi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndMagUbi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndMagUbiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indMagUbiRepository.findAll().size();

        // Create the IndMagUbi with an existing ID
        indMagUbi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndMagUbiMockMvc.perform(post("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indMagUbi)))
            .andExpect(status().isBadRequest());

        // Validate the IndMagUbi in the database
        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indMagUbiRepository.findAll().size();
        // set the field null
        indMagUbi.setIndirizzo(null);

        // Create the IndMagUbi, which fails.

        restIndMagUbiMockMvc.perform(post("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indMagUbi)))
            .andExpect(status().isBadRequest());

        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indMagUbiRepository.findAll().size();
        // set the field null
        indMagUbi.setNazione(null);

        // Create the IndMagUbi, which fails.

        restIndMagUbiMockMvc.perform(post("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indMagUbi)))
            .andExpect(status().isBadRequest());

        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndMagUbis() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indMagUbi.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndMagUbi() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get the indMagUbi
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis/{id}", indMagUbi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indMagUbi.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndMagUbiShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indMagUbiList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndMagUbiShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndMagUbiShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indMagUbiList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndMagUbiShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where indirizzo is not null
        defaultIndMagUbiShouldBeFound("indirizzo.specified=true");

        // Get all the indMagUbiList where indirizzo is null
        defaultIndMagUbiShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where cap equals to DEFAULT_CAP
        defaultIndMagUbiShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indMagUbiList where cap equals to UPDATED_CAP
        defaultIndMagUbiShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndMagUbiShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indMagUbiList where cap equals to UPDATED_CAP
        defaultIndMagUbiShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where cap is not null
        defaultIndMagUbiShouldBeFound("cap.specified=true");

        // Get all the indMagUbiList where cap is null
        defaultIndMagUbiShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where nazione equals to DEFAULT_NAZIONE
        defaultIndMagUbiShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indMagUbiList where nazione equals to UPDATED_NAZIONE
        defaultIndMagUbiShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndMagUbiShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indMagUbiList where nazione equals to UPDATED_NAZIONE
        defaultIndMagUbiShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where nazione is not null
        defaultIndMagUbiShouldBeFound("nazione.specified=true");

        // Get all the indMagUbiList where nazione is null
        defaultIndMagUbiShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where regione equals to DEFAULT_REGIONE
        defaultIndMagUbiShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indMagUbiList where regione equals to UPDATED_REGIONE
        defaultIndMagUbiShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndMagUbiShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indMagUbiList where regione equals to UPDATED_REGIONE
        defaultIndMagUbiShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where regione is not null
        defaultIndMagUbiShouldBeFound("regione.specified=true");

        // Get all the indMagUbiList where regione is null
        defaultIndMagUbiShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where provincia equals to DEFAULT_PROVINCIA
        defaultIndMagUbiShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indMagUbiList where provincia equals to UPDATED_PROVINCIA
        defaultIndMagUbiShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndMagUbiShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indMagUbiList where provincia equals to UPDATED_PROVINCIA
        defaultIndMagUbiShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where provincia is not null
        defaultIndMagUbiShouldBeFound("provincia.specified=true");

        // Get all the indMagUbiList where provincia is null
        defaultIndMagUbiShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where comune equals to DEFAULT_COMUNE
        defaultIndMagUbiShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indMagUbiList where comune equals to UPDATED_COMUNE
        defaultIndMagUbiShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndMagUbiShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indMagUbiList where comune equals to UPDATED_COMUNE
        defaultIndMagUbiShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where comune is not null
        defaultIndMagUbiShouldBeFound("comune.specified=true");

        // Get all the indMagUbiList where comune is null
        defaultIndMagUbiShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndMagUbiShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indMagUbiList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndMagUbiShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndMagUbiShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indMagUbiList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndMagUbiShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where numeroCiv is not null
        defaultIndMagUbiShouldBeFound("numeroCiv.specified=true");

        // Get all the indMagUbiList where numeroCiv is null
        defaultIndMagUbiShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude equals to DEFAULT_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indMagUbiList where latitude equals to UPDATED_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indMagUbiList where latitude equals to UPDATED_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude is not null
        defaultIndMagUbiShouldBeFound("latitude.specified=true");

        // Get all the indMagUbiList where latitude is null
        defaultIndMagUbiShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indMagUbiList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indMagUbiList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude is less than DEFAULT_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indMagUbiList where latitude is less than UPDATED_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where latitude is greater than DEFAULT_LATITUDE
        defaultIndMagUbiShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indMagUbiList where latitude is greater than SMALLER_LATITUDE
        defaultIndMagUbiShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude equals to DEFAULT_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indMagUbiList where longitude equals to UPDATED_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indMagUbiList where longitude equals to UPDATED_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude is not null
        defaultIndMagUbiShouldBeFound("longitude.specified=true");

        // Get all the indMagUbiList where longitude is null
        defaultIndMagUbiShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indMagUbiList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indMagUbiList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude is less than DEFAULT_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indMagUbiList where longitude is less than UPDATED_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndMagUbiShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indMagUbiList where longitude is greater than SMALLER_LONGITUDE
        defaultIndMagUbiShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali is not null
        defaultIndMagUbiShouldBeFound("dataInizVali.specified=true");

        // Get all the indMagUbiList where dataInizVali is null
        defaultIndMagUbiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndMagUbiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indMagUbiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndMagUbiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali is not null
        defaultIndMagUbiShouldBeFound("dataFineVali.specified=true");

        // Get all the indMagUbiList where dataFineVali is null
        defaultIndMagUbiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndMagUbiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indMagUbiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndMagUbiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator is not null
        defaultIndMagUbiShouldBeFound("userIdCreator.specified=true");

        // Get all the indMagUbiList where userIdCreator is null
        defaultIndMagUbiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndMagUbiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indMagUbiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndMagUbiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod is not null
        defaultIndMagUbiShouldBeFound("userIdLastMod.specified=true");

        // Get all the indMagUbiList where userIdLastMod is null
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndMagUbisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);

        // Get all the indMagUbiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndMagUbiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indMagUbiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndMagUbiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndMagUbisByIndToMagUbiIsEqualToSomething() throws Exception {
        // Initialize the database
        indMagUbiRepository.saveAndFlush(indMagUbi);
        MagUbicazione indToMagUbi = MagUbicazioneResourceIT.createEntity(em);
        em.persist(indToMagUbi);
        em.flush();
        indMagUbi.setIndToMagUbi(indToMagUbi);
        indMagUbiRepository.saveAndFlush(indMagUbi);
        Long indToMagUbiId = indToMagUbi.getId();

        // Get all the indMagUbiList where indToMagUbi equals to indToMagUbiId
        defaultIndMagUbiShouldBeFound("indToMagUbiId.equals=" + indToMagUbiId);

        // Get all the indMagUbiList where indToMagUbi equals to indToMagUbiId + 1
        defaultIndMagUbiShouldNotBeFound("indToMagUbiId.equals=" + (indToMagUbiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndMagUbiShouldBeFound(String filter) throws Exception {
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indMagUbi.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndMagUbiShouldNotBeFound(String filter) throws Exception {
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndMagUbi() throws Exception {
        // Get the indMagUbi
        restIndMagUbiMockMvc.perform(get("/api/ind-mag-ubis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndMagUbi() throws Exception {
        // Initialize the database
        indMagUbiService.save(indMagUbi);

        int databaseSizeBeforeUpdate = indMagUbiRepository.findAll().size();

        // Update the indMagUbi
        IndMagUbi updatedIndMagUbi = indMagUbiRepository.findById(indMagUbi.getId()).get();
        // Disconnect from session so that the updates on updatedIndMagUbi are not directly saved in db
        em.detach(updatedIndMagUbi);
        updatedIndMagUbi
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndMagUbiMockMvc.perform(put("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndMagUbi)))
            .andExpect(status().isOk());

        // Validate the IndMagUbi in the database
        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeUpdate);
        IndMagUbi testIndMagUbi = indMagUbiList.get(indMagUbiList.size() - 1);
        assertThat(testIndMagUbi.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndMagUbi.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndMagUbi.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndMagUbi.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndMagUbi.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndMagUbi.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndMagUbi.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndMagUbi.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndMagUbi.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndMagUbi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndMagUbi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndMagUbi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndMagUbi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndMagUbi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndMagUbi() throws Exception {
        int databaseSizeBeforeUpdate = indMagUbiRepository.findAll().size();

        // Create the IndMagUbi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndMagUbiMockMvc.perform(put("/api/ind-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indMagUbi)))
            .andExpect(status().isBadRequest());

        // Validate the IndMagUbi in the database
        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndMagUbi() throws Exception {
        // Initialize the database
        indMagUbiService.save(indMagUbi);

        int databaseSizeBeforeDelete = indMagUbiRepository.findAll().size();

        // Delete the indMagUbi
        restIndMagUbiMockMvc.perform(delete("/api/ind-mag-ubis/{id}", indMagUbi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndMagUbi> indMagUbiList = indMagUbiRepository.findAll();
        assertThat(indMagUbiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndMagUbi.class);
        IndMagUbi indMagUbi1 = new IndMagUbi();
        indMagUbi1.setId(1L);
        IndMagUbi indMagUbi2 = new IndMagUbi();
        indMagUbi2.setId(indMagUbi1.getId());
        assertThat(indMagUbi1).isEqualTo(indMagUbi2);
        indMagUbi2.setId(2L);
        assertThat(indMagUbi1).isNotEqualTo(indMagUbi2);
        indMagUbi1.setId(null);
        assertThat(indMagUbi1).isNotEqualTo(indMagUbi2);
    }
}
