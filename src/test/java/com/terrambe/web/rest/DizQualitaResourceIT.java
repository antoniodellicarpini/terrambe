package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizQualita;
import com.terrambe.repository.DizQualitaRepository;
import com.terrambe.service.DizQualitaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizQualitaCriteria;
import com.terrambe.service.DizQualitaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizQualitaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizQualitaResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizQualitaRepository dizQualitaRepository;

    @Autowired
    private DizQualitaService dizQualitaService;

    @Autowired
    private DizQualitaQueryService dizQualitaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizQualitaMockMvc;

    private DizQualita dizQualita;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizQualitaResource dizQualitaResource = new DizQualitaResource(dizQualitaService, dizQualitaQueryService);
        this.restDizQualitaMockMvc = MockMvcBuilders.standaloneSetup(dizQualitaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizQualita createEntity(EntityManager em) {
        DizQualita dizQualita = new DizQualita()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizQualita;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizQualita createUpdatedEntity(EntityManager em) {
        DizQualita dizQualita = new DizQualita()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizQualita;
    }

    @BeforeEach
    public void initTest() {
        dizQualita = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizQualita() throws Exception {
        int databaseSizeBeforeCreate = dizQualitaRepository.findAll().size();

        // Create the DizQualita
        restDizQualitaMockMvc.perform(post("/api/diz-qualitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizQualita)))
            .andExpect(status().isCreated());

        // Validate the DizQualita in the database
        List<DizQualita> dizQualitaList = dizQualitaRepository.findAll();
        assertThat(dizQualitaList).hasSize(databaseSizeBeforeCreate + 1);
        DizQualita testDizQualita = dizQualitaList.get(dizQualitaList.size() - 1);
        assertThat(testDizQualita.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizQualita.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizQualita.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizQualita.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizQualita.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizQualita.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizQualita.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizQualitaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizQualitaRepository.findAll().size();

        // Create the DizQualita with an existing ID
        dizQualita.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizQualitaMockMvc.perform(post("/api/diz-qualitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizQualita)))
            .andExpect(status().isBadRequest());

        // Validate the DizQualita in the database
        List<DizQualita> dizQualitaList = dizQualitaRepository.findAll();
        assertThat(dizQualitaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizQualitas() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizQualita.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizQualita() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get the dizQualita
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas/{id}", dizQualita.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizQualita.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizQualitasByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where codice equals to DEFAULT_CODICE
        defaultDizQualitaShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizQualitaList where codice equals to UPDATED_CODICE
        defaultDizQualitaShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizQualitaShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizQualitaList where codice equals to UPDATED_CODICE
        defaultDizQualitaShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where codice is not null
        defaultDizQualitaShouldBeFound("codice.specified=true");

        // Get all the dizQualitaList where codice is null
        defaultDizQualitaShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizQualitaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizQualitaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizQualitaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizQualitaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizQualitaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizQualitaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where descrizione is not null
        defaultDizQualitaShouldBeFound("descrizione.specified=true");

        // Get all the dizQualitaList where descrizione is null
        defaultDizQualitaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali is not null
        defaultDizQualitaShouldBeFound("dataInizVali.specified=true");

        // Get all the dizQualitaList where dataInizVali is null
        defaultDizQualitaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizQualitaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizQualitaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizQualitaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali is not null
        defaultDizQualitaShouldBeFound("dataFineVali.specified=true");

        // Get all the dizQualitaList where dataFineVali is null
        defaultDizQualitaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizQualitaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizQualitaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizQualitaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator is not null
        defaultDizQualitaShouldBeFound("userIdCreator.specified=true");

        // Get all the dizQualitaList where userIdCreator is null
        defaultDizQualitaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizQualitaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizQualitaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizQualitaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod is not null
        defaultDizQualitaShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizQualitaList where userIdLastMod is null
        defaultDizQualitaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizQualitasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizQualitaRepository.saveAndFlush(dizQualita);

        // Get all the dizQualitaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizQualitaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizQualitaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizQualitaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizQualitaShouldBeFound(String filter) throws Exception {
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizQualita.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizQualitaShouldNotBeFound(String filter) throws Exception {
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizQualita() throws Exception {
        // Get the dizQualita
        restDizQualitaMockMvc.perform(get("/api/diz-qualitas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizQualita() throws Exception {
        // Initialize the database
        dizQualitaService.save(dizQualita);

        int databaseSizeBeforeUpdate = dizQualitaRepository.findAll().size();

        // Update the dizQualita
        DizQualita updatedDizQualita = dizQualitaRepository.findById(dizQualita.getId()).get();
        // Disconnect from session so that the updates on updatedDizQualita are not directly saved in db
        em.detach(updatedDizQualita);
        updatedDizQualita
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizQualitaMockMvc.perform(put("/api/diz-qualitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizQualita)))
            .andExpect(status().isOk());

        // Validate the DizQualita in the database
        List<DizQualita> dizQualitaList = dizQualitaRepository.findAll();
        assertThat(dizQualitaList).hasSize(databaseSizeBeforeUpdate);
        DizQualita testDizQualita = dizQualitaList.get(dizQualitaList.size() - 1);
        assertThat(testDizQualita.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizQualita.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizQualita.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizQualita.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizQualita.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizQualita.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizQualita.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizQualita() throws Exception {
        int databaseSizeBeforeUpdate = dizQualitaRepository.findAll().size();

        // Create the DizQualita

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizQualitaMockMvc.perform(put("/api/diz-qualitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizQualita)))
            .andExpect(status().isBadRequest());

        // Validate the DizQualita in the database
        List<DizQualita> dizQualitaList = dizQualitaRepository.findAll();
        assertThat(dizQualitaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizQualita() throws Exception {
        // Initialize the database
        dizQualitaService.save(dizQualita);

        int databaseSizeBeforeDelete = dizQualitaRepository.findAll().size();

        // Delete the dizQualita
        restDizQualitaMockMvc.perform(delete("/api/diz-qualitas/{id}", dizQualita.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizQualita> dizQualitaList = dizQualitaRepository.findAll();
        assertThat(dizQualitaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizQualita.class);
        DizQualita dizQualita1 = new DizQualita();
        dizQualita1.setId(1L);
        DizQualita dizQualita2 = new DizQualita();
        dizQualita2.setId(dizQualita1.getId());
        assertThat(dizQualita1).isEqualTo(dizQualita2);
        dizQualita2.setId(2L);
        assertThat(dizQualita1).isNotEqualTo(dizQualita2);
        dizQualita1.setId(null);
        assertThat(dizQualita1).isNotEqualTo(dizQualita2);
    }
}
