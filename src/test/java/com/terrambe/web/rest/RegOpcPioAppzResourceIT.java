package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcPioAppz;
import com.terrambe.domain.RegOpcPio;
import com.terrambe.repository.RegOpcPioAppzRepository;
import com.terrambe.service.RegOpcPioAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcPioAppzCriteria;
import com.terrambe.service.RegOpcPioAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcPioAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcPioAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcPioAppzRepository regOpcPioAppzRepository;

    @Autowired
    private RegOpcPioAppzService regOpcPioAppzService;

    @Autowired
    private RegOpcPioAppzQueryService regOpcPioAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcPioAppzMockMvc;

    private RegOpcPioAppz regOpcPioAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcPioAppzResource regOpcPioAppzResource = new RegOpcPioAppzResource(regOpcPioAppzService, regOpcPioAppzQueryService);
        this.restRegOpcPioAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcPioAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPioAppz createEntity(EntityManager em) {
        RegOpcPioAppz regOpcPioAppz = new RegOpcPioAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcPioAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPioAppz createUpdatedEntity(EntityManager em) {
        RegOpcPioAppz regOpcPioAppz = new RegOpcPioAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcPioAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcPioAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcPioAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcPioAppzRepository.findAll().size();

        // Create the RegOpcPioAppz
        restRegOpcPioAppzMockMvc.perform(post("/api/reg-opc-pio-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPioAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcPioAppz in the database
        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcPioAppz testRegOpcPioAppz = regOpcPioAppzList.get(regOpcPioAppzList.size() - 1);
        assertThat(testRegOpcPioAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcPioAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcPioAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcPioAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcPioAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcPioAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcPioAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcPioAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcPioAppzRepository.findAll().size();

        // Create the RegOpcPioAppz with an existing ID
        regOpcPioAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcPioAppzMockMvc.perform(post("/api/reg-opc-pio-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPioAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPioAppz in the database
        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcPioAppzRepository.findAll().size();
        // set the field null
        regOpcPioAppz.setIdAzienda(null);

        // Create the RegOpcPioAppz, which fails.

        restRegOpcPioAppzMockMvc.perform(post("/api/reg-opc-pio-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPioAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzs() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPioAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcPioAppz() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get the regOpcPioAppz
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs/{id}", regOpcPioAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcPioAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda is not null
        defaultRegOpcPioAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcPioAppzList where idAzienda is null
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcPioAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPioAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcPioAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd is not null
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcPioAppzList where idUnitaProd is null
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPioAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcPioAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento is not null
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcPioAppzList where idAppezzamento is null
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcPioAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcPioAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali is not null
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcPioAppzList where dataInizVali is null
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPioAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcPioAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali is not null
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcPioAppzList where dataFineVali is null
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPioAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcPioAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator is not null
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcPioAppzList where userIdCreator is null
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPioAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcPioAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod is not null
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcPioAppzList where userIdLastMod is null
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);

        // Get all the regOpcPioAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPioAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPioAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPioAppzsByAppzToOpcPioIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);
        RegOpcPio appzToOpcPio = RegOpcPioResourceIT.createEntity(em);
        em.persist(appzToOpcPio);
        em.flush();
        regOpcPioAppz.setAppzToOpcPio(appzToOpcPio);
        regOpcPioAppzRepository.saveAndFlush(regOpcPioAppz);
        Long appzToOpcPioId = appzToOpcPio.getId();

        // Get all the regOpcPioAppzList where appzToOpcPio equals to appzToOpcPioId
        defaultRegOpcPioAppzShouldBeFound("appzToOpcPioId.equals=" + appzToOpcPioId);

        // Get all the regOpcPioAppzList where appzToOpcPio equals to appzToOpcPioId + 1
        defaultRegOpcPioAppzShouldNotBeFound("appzToOpcPioId.equals=" + (appzToOpcPioId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcPioAppzShouldBeFound(String filter) throws Exception {
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPioAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcPioAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcPioAppz() throws Exception {
        // Get the regOpcPioAppz
        restRegOpcPioAppzMockMvc.perform(get("/api/reg-opc-pio-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcPioAppz() throws Exception {
        // Initialize the database
        regOpcPioAppzService.save(regOpcPioAppz);

        int databaseSizeBeforeUpdate = regOpcPioAppzRepository.findAll().size();

        // Update the regOpcPioAppz
        RegOpcPioAppz updatedRegOpcPioAppz = regOpcPioAppzRepository.findById(regOpcPioAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcPioAppz are not directly saved in db
        em.detach(updatedRegOpcPioAppz);
        updatedRegOpcPioAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcPioAppzMockMvc.perform(put("/api/reg-opc-pio-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcPioAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcPioAppz in the database
        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcPioAppz testRegOpcPioAppz = regOpcPioAppzList.get(regOpcPioAppzList.size() - 1);
        assertThat(testRegOpcPioAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcPioAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcPioAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcPioAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcPioAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcPioAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcPioAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcPioAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcPioAppzRepository.findAll().size();

        // Create the RegOpcPioAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcPioAppzMockMvc.perform(put("/api/reg-opc-pio-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPioAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPioAppz in the database
        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcPioAppz() throws Exception {
        // Initialize the database
        regOpcPioAppzService.save(regOpcPioAppz);

        int databaseSizeBeforeDelete = regOpcPioAppzRepository.findAll().size();

        // Delete the regOpcPioAppz
        restRegOpcPioAppzMockMvc.perform(delete("/api/reg-opc-pio-appzs/{id}", regOpcPioAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcPioAppz> regOpcPioAppzList = regOpcPioAppzRepository.findAll();
        assertThat(regOpcPioAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcPioAppz.class);
        RegOpcPioAppz regOpcPioAppz1 = new RegOpcPioAppz();
        regOpcPioAppz1.setId(1L);
        RegOpcPioAppz regOpcPioAppz2 = new RegOpcPioAppz();
        regOpcPioAppz2.setId(regOpcPioAppz1.getId());
        assertThat(regOpcPioAppz1).isEqualTo(regOpcPioAppz2);
        regOpcPioAppz2.setId(2L);
        assertThat(regOpcPioAppz1).isNotEqualTo(regOpcPioAppz2);
        regOpcPioAppz1.setId(null);
        assertThat(regOpcPioAppz1).isNotEqualTo(regOpcPioAppz2);
    }
}
