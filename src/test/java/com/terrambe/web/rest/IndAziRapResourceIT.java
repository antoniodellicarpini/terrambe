package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndAziRap;
import com.terrambe.domain.AziRappresentante;
import com.terrambe.repository.IndAziRapRepository;
import com.terrambe.service.IndAziRapService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndAziRapCriteria;
import com.terrambe.service.IndAziRapQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndAziRapResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndAziRapResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndAziRapRepository indAziRapRepository;

    @Autowired
    private IndAziRapService indAziRapService;

    @Autowired
    private IndAziRapQueryService indAziRapQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndAziRapMockMvc;

    private IndAziRap indAziRap;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndAziRapResource indAziRapResource = new IndAziRapResource(indAziRapService, indAziRapQueryService);
        this.restIndAziRapMockMvc = MockMvcBuilders.standaloneSetup(indAziRapResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziRap createEntity(EntityManager em) {
        IndAziRap indAziRap = new IndAziRap()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indAziRap;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziRap createUpdatedEntity(EntityManager em) {
        IndAziRap indAziRap = new IndAziRap()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indAziRap;
    }

    @BeforeEach
    public void initTest() {
        indAziRap = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndAziRap() throws Exception {
        int databaseSizeBeforeCreate = indAziRapRepository.findAll().size();

        // Create the IndAziRap
        restIndAziRapMockMvc.perform(post("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRap)))
            .andExpect(status().isCreated());

        // Validate the IndAziRap in the database
        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeCreate + 1);
        IndAziRap testIndAziRap = indAziRapList.get(indAziRapList.size() - 1);
        assertThat(testIndAziRap.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndAziRap.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndAziRap.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndAziRap.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndAziRap.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndAziRap.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndAziRap.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndAziRap.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndAziRap.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndAziRap.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndAziRap.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndAziRap.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndAziRap.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndAziRap.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndAziRapWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indAziRapRepository.findAll().size();

        // Create the IndAziRap with an existing ID
        indAziRap.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndAziRapMockMvc.perform(post("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRap)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziRap in the database
        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziRapRepository.findAll().size();
        // set the field null
        indAziRap.setIndirizzo(null);

        // Create the IndAziRap, which fails.

        restIndAziRapMockMvc.perform(post("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRap)))
            .andExpect(status().isBadRequest());

        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziRapRepository.findAll().size();
        // set the field null
        indAziRap.setNazione(null);

        // Create the IndAziRap, which fails.

        restIndAziRapMockMvc.perform(post("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRap)))
            .andExpect(status().isBadRequest());

        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndAziRaps() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndAziRap() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get the indAziRap
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps/{id}", indAziRap.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indAziRap.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndAziRapShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indAziRapList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziRapShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndAziRapShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indAziRapList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziRapShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where indirizzo is not null
        defaultIndAziRapShouldBeFound("indirizzo.specified=true");

        // Get all the indAziRapList where indirizzo is null
        defaultIndAziRapShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where cap equals to DEFAULT_CAP
        defaultIndAziRapShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indAziRapList where cap equals to UPDATED_CAP
        defaultIndAziRapShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndAziRapShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indAziRapList where cap equals to UPDATED_CAP
        defaultIndAziRapShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where cap is not null
        defaultIndAziRapShouldBeFound("cap.specified=true");

        // Get all the indAziRapList where cap is null
        defaultIndAziRapShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where nazione equals to DEFAULT_NAZIONE
        defaultIndAziRapShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indAziRapList where nazione equals to UPDATED_NAZIONE
        defaultIndAziRapShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndAziRapShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indAziRapList where nazione equals to UPDATED_NAZIONE
        defaultIndAziRapShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where nazione is not null
        defaultIndAziRapShouldBeFound("nazione.specified=true");

        // Get all the indAziRapList where nazione is null
        defaultIndAziRapShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where regione equals to DEFAULT_REGIONE
        defaultIndAziRapShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indAziRapList where regione equals to UPDATED_REGIONE
        defaultIndAziRapShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndAziRapShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indAziRapList where regione equals to UPDATED_REGIONE
        defaultIndAziRapShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where regione is not null
        defaultIndAziRapShouldBeFound("regione.specified=true");

        // Get all the indAziRapList where regione is null
        defaultIndAziRapShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where provincia equals to DEFAULT_PROVINCIA
        defaultIndAziRapShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indAziRapList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziRapShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndAziRapShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indAziRapList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziRapShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where provincia is not null
        defaultIndAziRapShouldBeFound("provincia.specified=true");

        // Get all the indAziRapList where provincia is null
        defaultIndAziRapShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where comune equals to DEFAULT_COMUNE
        defaultIndAziRapShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indAziRapList where comune equals to UPDATED_COMUNE
        defaultIndAziRapShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndAziRapShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indAziRapList where comune equals to UPDATED_COMUNE
        defaultIndAziRapShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where comune is not null
        defaultIndAziRapShouldBeFound("comune.specified=true");

        // Get all the indAziRapList where comune is null
        defaultIndAziRapShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndAziRapShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indAziRapList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziRapShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndAziRapShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indAziRapList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziRapShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where numeroCiv is not null
        defaultIndAziRapShouldBeFound("numeroCiv.specified=true");

        // Get all the indAziRapList where numeroCiv is null
        defaultIndAziRapShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude equals to DEFAULT_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indAziRapList where latitude equals to UPDATED_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indAziRapList where latitude equals to UPDATED_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude is not null
        defaultIndAziRapShouldBeFound("latitude.specified=true");

        // Get all the indAziRapList where latitude is null
        defaultIndAziRapShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziRapList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziRapList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude is less than DEFAULT_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indAziRapList where latitude is less than UPDATED_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where latitude is greater than DEFAULT_LATITUDE
        defaultIndAziRapShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indAziRapList where latitude is greater than SMALLER_LATITUDE
        defaultIndAziRapShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude equals to DEFAULT_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indAziRapList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude is not null
        defaultIndAziRapShouldBeFound("longitude.specified=true");

        // Get all the indAziRapList where longitude is null
        defaultIndAziRapShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude is less than DEFAULT_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapList where longitude is less than UPDATED_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndAziRapShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapList where longitude is greater than SMALLER_LONGITUDE
        defaultIndAziRapShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali is not null
        defaultIndAziRapShouldBeFound("dataInizVali.specified=true");

        // Get all the indAziRapList where dataInizVali is null
        defaultIndAziRapShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndAziRapShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali is not null
        defaultIndAziRapShouldBeFound("dataFineVali.specified=true");

        // Get all the indAziRapList where dataFineVali is null
        defaultIndAziRapShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndAziRapShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndAziRapShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator is not null
        defaultIndAziRapShouldBeFound("userIdCreator.specified=true");

        // Get all the indAziRapList where userIdCreator is null
        defaultIndAziRapShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndAziRapShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndAziRapShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod is not null
        defaultIndAziRapShouldBeFound("userIdLastMod.specified=true");

        // Get all the indAziRapList where userIdLastMod is null
        defaultIndAziRapShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);

        // Get all the indAziRapList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndAziRapShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndAziRapsByIndToAziRapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapRepository.saveAndFlush(indAziRap);
        AziRappresentante indToAziRap = AziRappresentanteResourceIT.createEntity(em);
        em.persist(indToAziRap);
        em.flush();
        indAziRap.setIndToAziRap(indToAziRap);
        indAziRapRepository.saveAndFlush(indAziRap);
        Long indToAziRapId = indToAziRap.getId();

        // Get all the indAziRapList where indToAziRap equals to indToAziRapId
        defaultIndAziRapShouldBeFound("indToAziRapId.equals=" + indToAziRapId);

        // Get all the indAziRapList where indToAziRap equals to indToAziRapId + 1
        defaultIndAziRapShouldNotBeFound("indToAziRapId.equals=" + (indToAziRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndAziRapShouldBeFound(String filter) throws Exception {
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziRap.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndAziRapShouldNotBeFound(String filter) throws Exception {
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndAziRap() throws Exception {
        // Get the indAziRap
        restIndAziRapMockMvc.perform(get("/api/ind-azi-raps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndAziRap() throws Exception {
        // Initialize the database
        indAziRapService.save(indAziRap);

        int databaseSizeBeforeUpdate = indAziRapRepository.findAll().size();

        // Update the indAziRap
        IndAziRap updatedIndAziRap = indAziRapRepository.findById(indAziRap.getId()).get();
        // Disconnect from session so that the updates on updatedIndAziRap are not directly saved in db
        em.detach(updatedIndAziRap);
        updatedIndAziRap
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndAziRapMockMvc.perform(put("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndAziRap)))
            .andExpect(status().isOk());

        // Validate the IndAziRap in the database
        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeUpdate);
        IndAziRap testIndAziRap = indAziRapList.get(indAziRapList.size() - 1);
        assertThat(testIndAziRap.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndAziRap.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndAziRap.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndAziRap.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndAziRap.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndAziRap.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndAziRap.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndAziRap.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndAziRap.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndAziRap.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndAziRap.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndAziRap.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndAziRap.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndAziRap.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndAziRap() throws Exception {
        int databaseSizeBeforeUpdate = indAziRapRepository.findAll().size();

        // Create the IndAziRap

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndAziRapMockMvc.perform(put("/api/ind-azi-raps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRap)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziRap in the database
        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndAziRap() throws Exception {
        // Initialize the database
        indAziRapService.save(indAziRap);

        int databaseSizeBeforeDelete = indAziRapRepository.findAll().size();

        // Delete the indAziRap
        restIndAziRapMockMvc.perform(delete("/api/ind-azi-raps/{id}", indAziRap.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndAziRap> indAziRapList = indAziRapRepository.findAll();
        assertThat(indAziRapList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndAziRap.class);
        IndAziRap indAziRap1 = new IndAziRap();
        indAziRap1.setId(1L);
        IndAziRap indAziRap2 = new IndAziRap();
        indAziRap2.setId(indAziRap1.getId());
        assertThat(indAziRap1).isEqualTo(indAziRap2);
        indAziRap2.setId(2L);
        assertThat(indAziRap1).isNotEqualTo(indAziRap2);
        indAziRap1.setId(null);
        assertThat(indAziRap1).isNotEqualTo(indAziRap2);
    }
}
