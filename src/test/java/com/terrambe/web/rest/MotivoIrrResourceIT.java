package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.MotivoIrr;
import com.terrambe.domain.RegOpcIrr;
import com.terrambe.repository.MotivoIrrRepository;
import com.terrambe.service.MotivoIrrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.MotivoIrrCriteria;
import com.terrambe.service.MotivoIrrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MotivoIrrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class MotivoIrrResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private MotivoIrrRepository motivoIrrRepository;

    @Autowired
    private MotivoIrrService motivoIrrService;

    @Autowired
    private MotivoIrrQueryService motivoIrrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMotivoIrrMockMvc;

    private MotivoIrr motivoIrr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotivoIrrResource motivoIrrResource = new MotivoIrrResource(motivoIrrService, motivoIrrQueryService);
        this.restMotivoIrrMockMvc = MockMvcBuilders.standaloneSetup(motivoIrrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoIrr createEntity(EntityManager em) {
        MotivoIrr motivoIrr = new MotivoIrr()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return motivoIrr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoIrr createUpdatedEntity(EntityManager em) {
        MotivoIrr motivoIrr = new MotivoIrr()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return motivoIrr;
    }

    @BeforeEach
    public void initTest() {
        motivoIrr = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotivoIrr() throws Exception {
        int databaseSizeBeforeCreate = motivoIrrRepository.findAll().size();

        // Create the MotivoIrr
        restMotivoIrrMockMvc.perform(post("/api/motivo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoIrr)))
            .andExpect(status().isCreated());

        // Validate the MotivoIrr in the database
        List<MotivoIrr> motivoIrrList = motivoIrrRepository.findAll();
        assertThat(motivoIrrList).hasSize(databaseSizeBeforeCreate + 1);
        MotivoIrr testMotivoIrr = motivoIrrList.get(motivoIrrList.size() - 1);
        assertThat(testMotivoIrr.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testMotivoIrr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testMotivoIrr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testMotivoIrr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testMotivoIrr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createMotivoIrrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motivoIrrRepository.findAll().size();

        // Create the MotivoIrr with an existing ID
        motivoIrr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotivoIrrMockMvc.perform(post("/api/motivo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoIrr)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoIrr in the database
        List<MotivoIrr> motivoIrrList = motivoIrrRepository.findAll();
        assertThat(motivoIrrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMotivoIrrs() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getMotivoIrr() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get the motivoIrr
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs/{id}", motivoIrr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motivoIrr.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultMotivoIrrShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the motivoIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoIrrShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultMotivoIrrShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the motivoIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoIrrShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where descrizione is not null
        defaultMotivoIrrShouldBeFound("descrizione.specified=true");

        // Get all the motivoIrrList where descrizione is null
        defaultMotivoIrrShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali is not null
        defaultMotivoIrrShouldBeFound("dataInizVali.specified=true");

        // Get all the motivoIrrList where dataInizVali is null
        defaultMotivoIrrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultMotivoIrrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoIrrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultMotivoIrrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali is not null
        defaultMotivoIrrShouldBeFound("dataFineVali.specified=true");

        // Get all the motivoIrrList where dataFineVali is null
        defaultMotivoIrrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultMotivoIrrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoIrrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultMotivoIrrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator is not null
        defaultMotivoIrrShouldBeFound("userIdCreator.specified=true");

        // Get all the motivoIrrList where userIdCreator is null
        defaultMotivoIrrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultMotivoIrrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoIrrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultMotivoIrrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod is not null
        defaultMotivoIrrShouldBeFound("userIdLastMod.specified=true");

        // Get all the motivoIrrList where userIdLastMod is null
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoIrrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);

        // Get all the motivoIrrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoIrrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoIrrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultMotivoIrrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllMotivoIrrsByMotivoToOpcIrrIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoIrrRepository.saveAndFlush(motivoIrr);
        RegOpcIrr motivoToOpcIrr = RegOpcIrrResourceIT.createEntity(em);
        em.persist(motivoToOpcIrr);
        em.flush();
        motivoIrr.addMotivoToOpcIrr(motivoToOpcIrr);
        motivoIrrRepository.saveAndFlush(motivoIrr);
        Long motivoToOpcIrrId = motivoToOpcIrr.getId();

        // Get all the motivoIrrList where motivoToOpcIrr equals to motivoToOpcIrrId
        defaultMotivoIrrShouldBeFound("motivoToOpcIrrId.equals=" + motivoToOpcIrrId);

        // Get all the motivoIrrList where motivoToOpcIrr equals to motivoToOpcIrrId + 1
        defaultMotivoIrrShouldNotBeFound("motivoToOpcIrrId.equals=" + (motivoToOpcIrrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMotivoIrrShouldBeFound(String filter) throws Exception {
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMotivoIrrShouldNotBeFound(String filter) throws Exception {
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMotivoIrr() throws Exception {
        // Get the motivoIrr
        restMotivoIrrMockMvc.perform(get("/api/motivo-irrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotivoIrr() throws Exception {
        // Initialize the database
        motivoIrrService.save(motivoIrr);

        int databaseSizeBeforeUpdate = motivoIrrRepository.findAll().size();

        // Update the motivoIrr
        MotivoIrr updatedMotivoIrr = motivoIrrRepository.findById(motivoIrr.getId()).get();
        // Disconnect from session so that the updates on updatedMotivoIrr are not directly saved in db
        em.detach(updatedMotivoIrr);
        updatedMotivoIrr
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restMotivoIrrMockMvc.perform(put("/api/motivo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotivoIrr)))
            .andExpect(status().isOk());

        // Validate the MotivoIrr in the database
        List<MotivoIrr> motivoIrrList = motivoIrrRepository.findAll();
        assertThat(motivoIrrList).hasSize(databaseSizeBeforeUpdate);
        MotivoIrr testMotivoIrr = motivoIrrList.get(motivoIrrList.size() - 1);
        assertThat(testMotivoIrr.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testMotivoIrr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testMotivoIrr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testMotivoIrr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testMotivoIrr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingMotivoIrr() throws Exception {
        int databaseSizeBeforeUpdate = motivoIrrRepository.findAll().size();

        // Create the MotivoIrr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMotivoIrrMockMvc.perform(put("/api/motivo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoIrr)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoIrr in the database
        List<MotivoIrr> motivoIrrList = motivoIrrRepository.findAll();
        assertThat(motivoIrrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMotivoIrr() throws Exception {
        // Initialize the database
        motivoIrrService.save(motivoIrr);

        int databaseSizeBeforeDelete = motivoIrrRepository.findAll().size();

        // Delete the motivoIrr
        restMotivoIrrMockMvc.perform(delete("/api/motivo-irrs/{id}", motivoIrr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MotivoIrr> motivoIrrList = motivoIrrRepository.findAll();
        assertThat(motivoIrrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotivoIrr.class);
        MotivoIrr motivoIrr1 = new MotivoIrr();
        motivoIrr1.setId(1L);
        MotivoIrr motivoIrr2 = new MotivoIrr();
        motivoIrr2.setId(motivoIrr1.getId());
        assertThat(motivoIrr1).isEqualTo(motivoIrr2);
        motivoIrr2.setId(2L);
        assertThat(motivoIrr1).isNotEqualTo(motivoIrr2);
        motivoIrr1.setId(null);
        assertThat(motivoIrr1).isNotEqualTo(motivoIrr2);
    }
}
