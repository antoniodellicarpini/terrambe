package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipologiaPermessoOperatore;
import com.terrambe.domain.PermessiOperatore;
import com.terrambe.repository.TipologiaPermessoOperatoreRepository;
import com.terrambe.service.TipologiaPermessoOperatoreService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipologiaPermessoOperatoreCriteria;
import com.terrambe.service.TipologiaPermessoOperatoreQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipologiaPermessoOperatoreResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipologiaPermessoOperatoreResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipologiaPermessoOperatoreRepository tipologiaPermessoOperatoreRepository;

    @Autowired
    private TipologiaPermessoOperatoreService tipologiaPermessoOperatoreService;

    @Autowired
    private TipologiaPermessoOperatoreQueryService tipologiaPermessoOperatoreQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipologiaPermessoOperatoreMockMvc;

    private TipologiaPermessoOperatore tipologiaPermessoOperatore;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipologiaPermessoOperatoreResource tipologiaPermessoOperatoreResource = new TipologiaPermessoOperatoreResource(tipologiaPermessoOperatoreService, tipologiaPermessoOperatoreQueryService);
        this.restTipologiaPermessoOperatoreMockMvc = MockMvcBuilders.standaloneSetup(tipologiaPermessoOperatoreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaPermessoOperatore createEntity(EntityManager em) {
        TipologiaPermessoOperatore tipologiaPermessoOperatore = new TipologiaPermessoOperatore()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipologiaPermessoOperatore;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipologiaPermessoOperatore createUpdatedEntity(EntityManager em) {
        TipologiaPermessoOperatore tipologiaPermessoOperatore = new TipologiaPermessoOperatore()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipologiaPermessoOperatore;
    }

    @BeforeEach
    public void initTest() {
        tipologiaPermessoOperatore = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipologiaPermessoOperatore() throws Exception {
        int databaseSizeBeforeCreate = tipologiaPermessoOperatoreRepository.findAll().size();

        // Create the TipologiaPermessoOperatore
        restTipologiaPermessoOperatoreMockMvc.perform(post("/api/tipologia-permesso-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaPermessoOperatore)))
            .andExpect(status().isCreated());

        // Validate the TipologiaPermessoOperatore in the database
        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeCreate + 1);
        TipologiaPermessoOperatore testTipologiaPermessoOperatore = tipologiaPermessoOperatoreList.get(tipologiaPermessoOperatoreList.size() - 1);
        assertThat(testTipologiaPermessoOperatore.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipologiaPermessoOperatore.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipologiaPermessoOperatore.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipologiaPermessoOperatore.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipologiaPermessoOperatore.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipologiaPermessoOperatore.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipologiaPermessoOperatoreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipologiaPermessoOperatoreRepository.findAll().size();

        // Create the TipologiaPermessoOperatore with an existing ID
        tipologiaPermessoOperatore.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipologiaPermessoOperatoreMockMvc.perform(post("/api/tipologia-permesso-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaPermessoOperatore)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaPermessoOperatore in the database
        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipologiaPermessoOperatoreRepository.findAll().size();
        // set the field null
        tipologiaPermessoOperatore.setDescrizione(null);

        // Create the TipologiaPermessoOperatore, which fails.

        restTipologiaPermessoOperatoreMockMvc.perform(post("/api/tipologia-permesso-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaPermessoOperatore)))
            .andExpect(status().isBadRequest());

        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatores() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaPermessoOperatore.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipologiaPermessoOperatore() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get the tipologiaPermessoOperatore
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores/{id}", tipologiaPermessoOperatore.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipologiaPermessoOperatore.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipologiaPermessoOperatoreShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipologiaPermessoOperatoreList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologiaPermessoOperatoreShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipologiaPermessoOperatoreShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipologiaPermessoOperatoreList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipologiaPermessoOperatoreShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where descrizione is not null
        defaultTipologiaPermessoOperatoreShouldBeFound("descrizione.specified=true");

        // Get all the tipologiaPermessoOperatoreList where descrizione is null
        defaultTipologiaPermessoOperatoreShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is not null
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.specified=true");

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is null
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is not null
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.specified=true");

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is null
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipologiaPermessoOperatoreList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipologiaPermessoOperatoreShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is not null
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.specified=true");

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is null
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipologiaPermessoOperatoreList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is not null
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is null
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipologiaPermessoOperatoreList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipologiaPermessoOperatoreShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipologiaPermessoOperatoresByTipPermToPermOperIsEqualToSomething() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);
        PermessiOperatore tipPermToPermOper = PermessiOperatoreResourceIT.createEntity(em);
        em.persist(tipPermToPermOper);
        em.flush();
        tipologiaPermessoOperatore.addTipPermToPermOper(tipPermToPermOper);
        tipologiaPermessoOperatoreRepository.saveAndFlush(tipologiaPermessoOperatore);
        Long tipPermToPermOperId = tipPermToPermOper.getId();

        // Get all the tipologiaPermessoOperatoreList where tipPermToPermOper equals to tipPermToPermOperId
        defaultTipologiaPermessoOperatoreShouldBeFound("tipPermToPermOperId.equals=" + tipPermToPermOperId);

        // Get all the tipologiaPermessoOperatoreList where tipPermToPermOper equals to tipPermToPermOperId + 1
        defaultTipologiaPermessoOperatoreShouldNotBeFound("tipPermToPermOperId.equals=" + (tipPermToPermOperId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipologiaPermessoOperatoreShouldBeFound(String filter) throws Exception {
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipologiaPermessoOperatore.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipologiaPermessoOperatoreShouldNotBeFound(String filter) throws Exception {
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipologiaPermessoOperatore() throws Exception {
        // Get the tipologiaPermessoOperatore
        restTipologiaPermessoOperatoreMockMvc.perform(get("/api/tipologia-permesso-operatores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipologiaPermessoOperatore() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreService.save(tipologiaPermessoOperatore);

        int databaseSizeBeforeUpdate = tipologiaPermessoOperatoreRepository.findAll().size();

        // Update the tipologiaPermessoOperatore
        TipologiaPermessoOperatore updatedTipologiaPermessoOperatore = tipologiaPermessoOperatoreRepository.findById(tipologiaPermessoOperatore.getId()).get();
        // Disconnect from session so that the updates on updatedTipologiaPermessoOperatore are not directly saved in db
        em.detach(updatedTipologiaPermessoOperatore);
        updatedTipologiaPermessoOperatore
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipologiaPermessoOperatoreMockMvc.perform(put("/api/tipologia-permesso-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipologiaPermessoOperatore)))
            .andExpect(status().isOk());

        // Validate the TipologiaPermessoOperatore in the database
        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeUpdate);
        TipologiaPermessoOperatore testTipologiaPermessoOperatore = tipologiaPermessoOperatoreList.get(tipologiaPermessoOperatoreList.size() - 1);
        assertThat(testTipologiaPermessoOperatore.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipologiaPermessoOperatore.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipologiaPermessoOperatore.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipologiaPermessoOperatore.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipologiaPermessoOperatore.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipologiaPermessoOperatore.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipologiaPermessoOperatore() throws Exception {
        int databaseSizeBeforeUpdate = tipologiaPermessoOperatoreRepository.findAll().size();

        // Create the TipologiaPermessoOperatore

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipologiaPermessoOperatoreMockMvc.perform(put("/api/tipologia-permesso-operatores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipologiaPermessoOperatore)))
            .andExpect(status().isBadRequest());

        // Validate the TipologiaPermessoOperatore in the database
        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipologiaPermessoOperatore() throws Exception {
        // Initialize the database
        tipologiaPermessoOperatoreService.save(tipologiaPermessoOperatore);

        int databaseSizeBeforeDelete = tipologiaPermessoOperatoreRepository.findAll().size();

        // Delete the tipologiaPermessoOperatore
        restTipologiaPermessoOperatoreMockMvc.perform(delete("/api/tipologia-permesso-operatores/{id}", tipologiaPermessoOperatore.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipologiaPermessoOperatore> tipologiaPermessoOperatoreList = tipologiaPermessoOperatoreRepository.findAll();
        assertThat(tipologiaPermessoOperatoreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipologiaPermessoOperatore.class);
        TipologiaPermessoOperatore tipologiaPermessoOperatore1 = new TipologiaPermessoOperatore();
        tipologiaPermessoOperatore1.setId(1L);
        TipologiaPermessoOperatore tipologiaPermessoOperatore2 = new TipologiaPermessoOperatore();
        tipologiaPermessoOperatore2.setId(tipologiaPermessoOperatore1.getId());
        assertThat(tipologiaPermessoOperatore1).isEqualTo(tipologiaPermessoOperatore2);
        tipologiaPermessoOperatore2.setId(2L);
        assertThat(tipologiaPermessoOperatore1).isNotEqualTo(tipologiaPermessoOperatore2);
        tipologiaPermessoOperatore1.setId(null);
        assertThat(tipologiaPermessoOperatore1).isNotEqualTo(tipologiaPermessoOperatore2);
    }
}
