package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.domain.FerAnagrafica;
import com.terrambe.domain.Fornitori;
import com.terrambe.domain.TipoRegMag;
import com.terrambe.repository.RegCaricoFertilizzanteRepository;
import com.terrambe.service.RegCaricoFertilizzanteService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegCaricoFertilizzanteCriteria;
import com.terrambe.service.RegCaricoFertilizzanteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegCaricoFertilizzanteResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegCaricoFertilizzanteResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Double DEFAULT_QUANTITA = 1D;
    private static final Double UPDATED_QUANTITA = 2D;
    private static final Double SMALLER_QUANTITA = 1D - 1D;

    private static final Float DEFAULT_PREZZO_UNITARIO = 1F;
    private static final Float UPDATED_PREZZO_UNITARIO = 2F;
    private static final Float SMALLER_PREZZO_UNITARIO = 1F - 1F;

    private static final LocalDate DEFAULT_DATA_CARICO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_CARICO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_CARICO = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_DDT = "AAAAAAAAAA";
    private static final String UPDATED_DDT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_SCADENZA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_SCADENZA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_SCADENZA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegCaricoFertilizzanteRepository regCaricoFertilizzanteRepository;

    @Autowired
    private RegCaricoFertilizzanteService regCaricoFertilizzanteService;

    @Autowired
    private RegCaricoFertilizzanteQueryService regCaricoFertilizzanteQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegCaricoFertilizzanteMockMvc;

    private RegCaricoFertilizzante regCaricoFertilizzante;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegCaricoFertilizzanteResource regCaricoFertilizzanteResource = new RegCaricoFertilizzanteResource(regCaricoFertilizzanteService, regCaricoFertilizzanteQueryService);
        this.restRegCaricoFertilizzanteMockMvc = MockMvcBuilders.standaloneSetup(regCaricoFertilizzanteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoFertilizzante createEntity(EntityManager em) {
        RegCaricoFertilizzante regCaricoFertilizzante = new RegCaricoFertilizzante()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .quantita(DEFAULT_QUANTITA)
            .prezzoUnitario(DEFAULT_PREZZO_UNITARIO)
            .dataCarico(DEFAULT_DATA_CARICO)
            .ddt(DEFAULT_DDT)
            .dataScadenza(DEFAULT_DATA_SCADENZA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regCaricoFertilizzante;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegCaricoFertilizzante createUpdatedEntity(EntityManager em) {
        RegCaricoFertilizzante regCaricoFertilizzante = new RegCaricoFertilizzante()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regCaricoFertilizzante;
    }

    @BeforeEach
    public void initTest() {
        regCaricoFertilizzante = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegCaricoFertilizzante() throws Exception {
        int databaseSizeBeforeCreate = regCaricoFertilizzanteRepository.findAll().size();

        // Create the RegCaricoFertilizzante
        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isCreated());

        // Validate the RegCaricoFertilizzante in the database
        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeCreate + 1);
        RegCaricoFertilizzante testRegCaricoFertilizzante = regCaricoFertilizzanteList.get(regCaricoFertilizzanteList.size() - 1);
        assertThat(testRegCaricoFertilizzante.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegCaricoFertilizzante.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegCaricoFertilizzante.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testRegCaricoFertilizzante.getPrezzoUnitario()).isEqualTo(DEFAULT_PREZZO_UNITARIO);
        assertThat(testRegCaricoFertilizzante.getDataCarico()).isEqualTo(DEFAULT_DATA_CARICO);
        assertThat(testRegCaricoFertilizzante.getDdt()).isEqualTo(DEFAULT_DDT);
        assertThat(testRegCaricoFertilizzante.getDataScadenza()).isEqualTo(DEFAULT_DATA_SCADENZA);
        assertThat(testRegCaricoFertilizzante.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegCaricoFertilizzante.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegCaricoFertilizzante.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegCaricoFertilizzante.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegCaricoFertilizzante.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegCaricoFertilizzanteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regCaricoFertilizzanteRepository.findAll().size();

        // Create the RegCaricoFertilizzante with an existing ID
        regCaricoFertilizzante.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoFertilizzante in the database
        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setIdAzienda(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQuantitaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setQuantita(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrezzoUnitarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setPrezzoUnitario(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataCaricoIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setDataCarico(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDdtIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setDdt(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataScadenzaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regCaricoFertilizzanteRepository.findAll().size();
        // set the field null
        regCaricoFertilizzante.setDataScadenza(null);

        // Create the RegCaricoFertilizzante, which fails.

        restRegCaricoFertilizzanteMockMvc.perform(post("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantes() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoFertilizzante.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT.toString())))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegCaricoFertilizzante() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get the regCaricoFertilizzante
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes/{id}", regCaricoFertilizzante.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regCaricoFertilizzante.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA.doubleValue()))
            .andExpect(jsonPath("$.prezzoUnitario").value(DEFAULT_PREZZO_UNITARIO.doubleValue()))
            .andExpect(jsonPath("$.dataCarico").value(DEFAULT_DATA_CARICO.toString()))
            .andExpect(jsonPath("$.ddt").value(DEFAULT_DDT.toString()))
            .andExpect(jsonPath("$.dataScadenza").value(DEFAULT_DATA_SCADENZA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda is not null
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.specified=true");

        // Get all the regCaricoFertilizzanteList where idAzienda is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regCaricoFertilizzanteList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegCaricoFertilizzanteShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is not null
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.specified=true");

        // Get all the regCaricoFertilizzanteList where idUnitaProd is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regCaricoFertilizzanteList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegCaricoFertilizzanteShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita equals to DEFAULT_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.equals=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.equals=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita in DEFAULT_QUANTITA or UPDATED_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.in=" + DEFAULT_QUANTITA + "," + UPDATED_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita equals to UPDATED_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.in=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita is not null
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.specified=true");

        // Get all the regCaricoFertilizzanteList where quantita is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita is greater than or equal to DEFAULT_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.greaterThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita is greater than or equal to UPDATED_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.greaterThanOrEqual=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita is less than or equal to DEFAULT_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.lessThanOrEqual=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita is less than or equal to SMALLER_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.lessThanOrEqual=" + SMALLER_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita is less than DEFAULT_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.lessThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita is less than UPDATED_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.lessThan=" + UPDATED_QUANTITA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByQuantitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where quantita is greater than DEFAULT_QUANTITA
        defaultRegCaricoFertilizzanteShouldNotBeFound("quantita.greaterThan=" + DEFAULT_QUANTITA);

        // Get all the regCaricoFertilizzanteList where quantita is greater than SMALLER_QUANTITA
        defaultRegCaricoFertilizzanteShouldBeFound("quantita.greaterThan=" + SMALLER_QUANTITA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario equals to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.equals=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.equals=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario in DEFAULT_PREZZO_UNITARIO or UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.in=" + DEFAULT_PREZZO_UNITARIO + "," + UPDATED_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario equals to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.in=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is not null
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.specified=true");

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is greater than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.greaterThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is greater than or equal to UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.greaterThanOrEqual=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is less than or equal to DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.lessThanOrEqual=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is less than or equal to SMALLER_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.lessThanOrEqual=" + SMALLER_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is less than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.lessThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is less than UPDATED_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.lessThan=" + UPDATED_PREZZO_UNITARIO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByPrezzoUnitarioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is greater than DEFAULT_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldNotBeFound("prezzoUnitario.greaterThan=" + DEFAULT_PREZZO_UNITARIO);

        // Get all the regCaricoFertilizzanteList where prezzoUnitario is greater than SMALLER_PREZZO_UNITARIO
        defaultRegCaricoFertilizzanteShouldBeFound("prezzoUnitario.greaterThan=" + SMALLER_PREZZO_UNITARIO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico equals to DEFAULT_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.equals=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.equals=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico in DEFAULT_DATA_CARICO or UPDATED_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.in=" + DEFAULT_DATA_CARICO + "," + UPDATED_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico equals to UPDATED_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.in=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico is not null
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.specified=true");

        // Get all the regCaricoFertilizzanteList where dataCarico is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico is greater than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.greaterThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico is greater than or equal to UPDATED_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.greaterThanOrEqual=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico is less than or equal to DEFAULT_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.lessThanOrEqual=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico is less than or equal to SMALLER_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.lessThanOrEqual=" + SMALLER_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico is less than DEFAULT_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.lessThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico is less than UPDATED_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.lessThan=" + UPDATED_DATA_CARICO);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataCaricoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataCarico is greater than DEFAULT_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataCarico.greaterThan=" + DEFAULT_DATA_CARICO);

        // Get all the regCaricoFertilizzanteList where dataCarico is greater than SMALLER_DATA_CARICO
        defaultRegCaricoFertilizzanteShouldBeFound("dataCarico.greaterThan=" + SMALLER_DATA_CARICO);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDdtIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where ddt equals to DEFAULT_DDT
        defaultRegCaricoFertilizzanteShouldBeFound("ddt.equals=" + DEFAULT_DDT);

        // Get all the regCaricoFertilizzanteList where ddt equals to UPDATED_DDT
        defaultRegCaricoFertilizzanteShouldNotBeFound("ddt.equals=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDdtIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where ddt in DEFAULT_DDT or UPDATED_DDT
        defaultRegCaricoFertilizzanteShouldBeFound("ddt.in=" + DEFAULT_DDT + "," + UPDATED_DDT);

        // Get all the regCaricoFertilizzanteList where ddt equals to UPDATED_DDT
        defaultRegCaricoFertilizzanteShouldNotBeFound("ddt.in=" + UPDATED_DDT);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDdtIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where ddt is not null
        defaultRegCaricoFertilizzanteShouldBeFound("ddt.specified=true");

        // Get all the regCaricoFertilizzanteList where ddt is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("ddt.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza equals to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.equals=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.equals=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza in DEFAULT_DATA_SCADENZA or UPDATED_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.in=" + DEFAULT_DATA_SCADENZA + "," + UPDATED_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.in=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza is not null
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.specified=true");

        // Get all the regCaricoFertilizzanteList where dataScadenza is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza is greater than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.greaterThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza is greater than or equal to UPDATED_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.greaterThanOrEqual=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza is less than or equal to DEFAULT_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.lessThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza is less than or equal to SMALLER_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.lessThanOrEqual=" + SMALLER_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza is less than DEFAULT_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.lessThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza is less than UPDATED_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.lessThan=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataScadenzaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataScadenza is greater than DEFAULT_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataScadenza.greaterThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the regCaricoFertilizzanteList where dataScadenza is greater than SMALLER_DATA_SCADENZA
        defaultRegCaricoFertilizzanteShouldBeFound("dataScadenza.greaterThan=" + SMALLER_DATA_SCADENZA);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali is not null
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.specified=true");

        // Get all the regCaricoFertilizzanteList where dataInizVali is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regCaricoFertilizzanteList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali is not null
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.specified=true");

        // Get all the regCaricoFertilizzanteList where dataFineVali is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regCaricoFertilizzanteList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegCaricoFertilizzanteShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator is not null
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.specified=true");

        // Get all the regCaricoFertilizzanteList where userIdCreator is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regCaricoFertilizzanteList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegCaricoFertilizzanteShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is not null
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.specified=true");

        // Get all the regCaricoFertilizzanteList where userIdLastMod is null
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regCaricoFertilizzanteList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegCaricoFertilizzanteShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByMagUbicazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        MagUbicazione magUbicazione = MagUbicazioneResourceIT.createEntity(em);
        em.persist(magUbicazione);
        em.flush();
        regCaricoFertilizzante.setMagUbicazione(magUbicazione);
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        Long magUbicazioneId = magUbicazione.getId();

        // Get all the regCaricoFertilizzanteList where magUbicazione equals to magUbicazioneId
        defaultRegCaricoFertilizzanteShouldBeFound("magUbicazioneId.equals=" + magUbicazioneId);

        // Get all the regCaricoFertilizzanteList where magUbicazione equals to magUbicazioneId + 1
        defaultRegCaricoFertilizzanteShouldNotBeFound("magUbicazioneId.equals=" + (magUbicazioneId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByFeranagraficaIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        FerAnagrafica feranagrafica = FerAnagraficaResourceIT.createEntity(em);
        em.persist(feranagrafica);
        em.flush();
        regCaricoFertilizzante.setFeranagrafica(feranagrafica);
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        Long feranagraficaId = feranagrafica.getId();

        // Get all the regCaricoFertilizzanteList where feranagrafica equals to feranagraficaId
        defaultRegCaricoFertilizzanteShouldBeFound("feranagraficaId.equals=" + feranagraficaId);

        // Get all the regCaricoFertilizzanteList where feranagrafica equals to feranagraficaId + 1
        defaultRegCaricoFertilizzanteShouldNotBeFound("feranagraficaId.equals=" + (feranagraficaId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByCaricoFertToFornitoriIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        Fornitori caricoFertToFornitori = FornitoriResourceIT.createEntity(em);
        em.persist(caricoFertToFornitori);
        em.flush();
        regCaricoFertilizzante.setCaricoFertToFornitori(caricoFertToFornitori);
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        Long caricoFertToFornitoriId = caricoFertToFornitori.getId();

        // Get all the regCaricoFertilizzanteList where caricoFertToFornitori equals to caricoFertToFornitoriId
        defaultRegCaricoFertilizzanteShouldBeFound("caricoFertToFornitoriId.equals=" + caricoFertToFornitoriId);

        // Get all the regCaricoFertilizzanteList where caricoFertToFornitori equals to caricoFertToFornitoriId + 1
        defaultRegCaricoFertilizzanteShouldNotBeFound("caricoFertToFornitoriId.equals=" + (caricoFertToFornitoriId + 1));
    }


    @Test
    @Transactional
    public void getAllRegCaricoFertilizzantesByRegFertToTipoRegIsEqualToSomething() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        TipoRegMag regFertToTipoReg = TipoRegMagResourceIT.createEntity(em);
        em.persist(regFertToTipoReg);
        em.flush();
        regCaricoFertilizzante.setRegFertToTipoReg(regFertToTipoReg);
        regCaricoFertilizzanteRepository.saveAndFlush(regCaricoFertilizzante);
        Long regFertToTipoRegId = regFertToTipoReg.getId();

        // Get all the regCaricoFertilizzanteList where regFertToTipoReg equals to regFertToTipoRegId
        defaultRegCaricoFertilizzanteShouldBeFound("regFertToTipoRegId.equals=" + regFertToTipoRegId);

        // Get all the regCaricoFertilizzanteList where regFertToTipoReg equals to regFertToTipoRegId + 1
        defaultRegCaricoFertilizzanteShouldNotBeFound("regFertToTipoRegId.equals=" + (regFertToTipoRegId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegCaricoFertilizzanteShouldBeFound(String filter) throws Exception {
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regCaricoFertilizzante.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA.doubleValue())))
            .andExpect(jsonPath("$.[*].prezzoUnitario").value(hasItem(DEFAULT_PREZZO_UNITARIO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataCarico").value(hasItem(DEFAULT_DATA_CARICO.toString())))
            .andExpect(jsonPath("$.[*].ddt").value(hasItem(DEFAULT_DDT)))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegCaricoFertilizzanteShouldNotBeFound(String filter) throws Exception {
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegCaricoFertilizzante() throws Exception {
        // Get the regCaricoFertilizzante
        restRegCaricoFertilizzanteMockMvc.perform(get("/api/reg-carico-fertilizzantes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegCaricoFertilizzante() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteService.save(regCaricoFertilizzante);

        int databaseSizeBeforeUpdate = regCaricoFertilizzanteRepository.findAll().size();

        // Update the regCaricoFertilizzante
        RegCaricoFertilizzante updatedRegCaricoFertilizzante = regCaricoFertilizzanteRepository.findById(regCaricoFertilizzante.getId()).get();
        // Disconnect from session so that the updates on updatedRegCaricoFertilizzante are not directly saved in db
        em.detach(updatedRegCaricoFertilizzante);
        updatedRegCaricoFertilizzante
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .quantita(UPDATED_QUANTITA)
            .prezzoUnitario(UPDATED_PREZZO_UNITARIO)
            .dataCarico(UPDATED_DATA_CARICO)
            .ddt(UPDATED_DDT)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegCaricoFertilizzanteMockMvc.perform(put("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegCaricoFertilizzante)))
            .andExpect(status().isOk());

        // Validate the RegCaricoFertilizzante in the database
        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeUpdate);
        RegCaricoFertilizzante testRegCaricoFertilizzante = regCaricoFertilizzanteList.get(regCaricoFertilizzanteList.size() - 1);
        assertThat(testRegCaricoFertilizzante.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegCaricoFertilizzante.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegCaricoFertilizzante.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testRegCaricoFertilizzante.getPrezzoUnitario()).isEqualTo(UPDATED_PREZZO_UNITARIO);
        assertThat(testRegCaricoFertilizzante.getDataCarico()).isEqualTo(UPDATED_DATA_CARICO);
        assertThat(testRegCaricoFertilizzante.getDdt()).isEqualTo(UPDATED_DDT);
        assertThat(testRegCaricoFertilizzante.getDataScadenza()).isEqualTo(UPDATED_DATA_SCADENZA);
        assertThat(testRegCaricoFertilizzante.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegCaricoFertilizzante.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegCaricoFertilizzante.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegCaricoFertilizzante.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegCaricoFertilizzante.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegCaricoFertilizzante() throws Exception {
        int databaseSizeBeforeUpdate = regCaricoFertilizzanteRepository.findAll().size();

        // Create the RegCaricoFertilizzante

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegCaricoFertilizzanteMockMvc.perform(put("/api/reg-carico-fertilizzantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regCaricoFertilizzante)))
            .andExpect(status().isBadRequest());

        // Validate the RegCaricoFertilizzante in the database
        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegCaricoFertilizzante() throws Exception {
        // Initialize the database
        regCaricoFertilizzanteService.save(regCaricoFertilizzante);

        int databaseSizeBeforeDelete = regCaricoFertilizzanteRepository.findAll().size();

        // Delete the regCaricoFertilizzante
        restRegCaricoFertilizzanteMockMvc.perform(delete("/api/reg-carico-fertilizzantes/{id}", regCaricoFertilizzante.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegCaricoFertilizzante> regCaricoFertilizzanteList = regCaricoFertilizzanteRepository.findAll();
        assertThat(regCaricoFertilizzanteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegCaricoFertilizzante.class);
        RegCaricoFertilizzante regCaricoFertilizzante1 = new RegCaricoFertilizzante();
        regCaricoFertilizzante1.setId(1L);
        RegCaricoFertilizzante regCaricoFertilizzante2 = new RegCaricoFertilizzante();
        regCaricoFertilizzante2.setId(regCaricoFertilizzante1.getId());
        assertThat(regCaricoFertilizzante1).isEqualTo(regCaricoFertilizzante2);
        regCaricoFertilizzante2.setId(2L);
        assertThat(regCaricoFertilizzante1).isNotEqualTo(regCaricoFertilizzante2);
        regCaricoFertilizzante1.setId(null);
        assertThat(regCaricoFertilizzante1).isNotEqualTo(regCaricoFertilizzante2);
    }
}
