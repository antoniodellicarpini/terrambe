package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.FertTipo;
import com.terrambe.domain.FerAnagrafica;
import com.terrambe.repository.FertTipoRepository;
import com.terrambe.service.FertTipoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FertTipoCriteria;
import com.terrambe.service.FertTipoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FertTipoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FertTipoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    @Autowired
    private FertTipoRepository fertTipoRepository;

    @Autowired
    private FertTipoService fertTipoService;

    @Autowired
    private FertTipoQueryService fertTipoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFertTipoMockMvc;

    private FertTipo fertTipo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FertTipoResource fertTipoResource = new FertTipoResource(fertTipoService, fertTipoQueryService);
        this.restFertTipoMockMvc = MockMvcBuilders.standaloneSetup(fertTipoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FertTipo createEntity(EntityManager em) {
        FertTipo fertTipo = new FertTipo()
            .descrizione(DEFAULT_DESCRIZIONE);
        return fertTipo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FertTipo createUpdatedEntity(EntityManager em) {
        FertTipo fertTipo = new FertTipo()
            .descrizione(UPDATED_DESCRIZIONE);
        return fertTipo;
    }

    @BeforeEach
    public void initTest() {
        fertTipo = createEntity(em);
    }

    @Test
    @Transactional
    public void createFertTipo() throws Exception {
        int databaseSizeBeforeCreate = fertTipoRepository.findAll().size();

        // Create the FertTipo
        restFertTipoMockMvc.perform(post("/api/fert-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fertTipo)))
            .andExpect(status().isCreated());

        // Validate the FertTipo in the database
        List<FertTipo> fertTipoList = fertTipoRepository.findAll();
        assertThat(fertTipoList).hasSize(databaseSizeBeforeCreate + 1);
        FertTipo testFertTipo = fertTipoList.get(fertTipoList.size() - 1);
        assertThat(testFertTipo.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void createFertTipoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fertTipoRepository.findAll().size();

        // Create the FertTipo with an existing ID
        fertTipo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFertTipoMockMvc.perform(post("/api/fert-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fertTipo)))
            .andExpect(status().isBadRequest());

        // Validate the FertTipo in the database
        List<FertTipo> fertTipoList = fertTipoRepository.findAll();
        assertThat(fertTipoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFertTipos() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);

        // Get all the fertTipoList
        restFertTipoMockMvc.perform(get("/api/fert-tipos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fertTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())));
    }
    
    @Test
    @Transactional
    public void getFertTipo() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);

        // Get the fertTipo
        restFertTipoMockMvc.perform(get("/api/fert-tipos/{id}", fertTipo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fertTipo.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()));
    }

    @Test
    @Transactional
    public void getAllFertTiposByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);

        // Get all the fertTipoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultFertTipoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the fertTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFertTipoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFertTiposByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);

        // Get all the fertTipoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultFertTipoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the fertTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFertTipoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFertTiposByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);

        // Get all the fertTipoList where descrizione is not null
        defaultFertTipoShouldBeFound("descrizione.specified=true");

        // Get all the fertTipoList where descrizione is null
        defaultFertTipoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFertTiposByFerAnagraficaIsEqualToSomething() throws Exception {
        // Initialize the database
        fertTipoRepository.saveAndFlush(fertTipo);
        FerAnagrafica ferAnagrafica = FerAnagraficaResourceIT.createEntity(em);
        em.persist(ferAnagrafica);
        em.flush();
        fertTipo.addFerAnagrafica(ferAnagrafica);
        fertTipoRepository.saveAndFlush(fertTipo);
        Long ferAnagraficaId = ferAnagrafica.getId();

        // Get all the fertTipoList where ferAnagrafica equals to ferAnagraficaId
        defaultFertTipoShouldBeFound("ferAnagraficaId.equals=" + ferAnagraficaId);

        // Get all the fertTipoList where ferAnagrafica equals to ferAnagraficaId + 1
        defaultFertTipoShouldNotBeFound("ferAnagraficaId.equals=" + (ferAnagraficaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFertTipoShouldBeFound(String filter) throws Exception {
        restFertTipoMockMvc.perform(get("/api/fert-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fertTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)));

        // Check, that the count call also returns 1
        restFertTipoMockMvc.perform(get("/api/fert-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFertTipoShouldNotBeFound(String filter) throws Exception {
        restFertTipoMockMvc.perform(get("/api/fert-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFertTipoMockMvc.perform(get("/api/fert-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFertTipo() throws Exception {
        // Get the fertTipo
        restFertTipoMockMvc.perform(get("/api/fert-tipos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFertTipo() throws Exception {
        // Initialize the database
        fertTipoService.save(fertTipo);

        int databaseSizeBeforeUpdate = fertTipoRepository.findAll().size();

        // Update the fertTipo
        FertTipo updatedFertTipo = fertTipoRepository.findById(fertTipo.getId()).get();
        // Disconnect from session so that the updates on updatedFertTipo are not directly saved in db
        em.detach(updatedFertTipo);
        updatedFertTipo
            .descrizione(UPDATED_DESCRIZIONE);

        restFertTipoMockMvc.perform(put("/api/fert-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFertTipo)))
            .andExpect(status().isOk());

        // Validate the FertTipo in the database
        List<FertTipo> fertTipoList = fertTipoRepository.findAll();
        assertThat(fertTipoList).hasSize(databaseSizeBeforeUpdate);
        FertTipo testFertTipo = fertTipoList.get(fertTipoList.size() - 1);
        assertThat(testFertTipo.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void updateNonExistingFertTipo() throws Exception {
        int databaseSizeBeforeUpdate = fertTipoRepository.findAll().size();

        // Create the FertTipo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFertTipoMockMvc.perform(put("/api/fert-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fertTipo)))
            .andExpect(status().isBadRequest());

        // Validate the FertTipo in the database
        List<FertTipo> fertTipoList = fertTipoRepository.findAll();
        assertThat(fertTipoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFertTipo() throws Exception {
        // Initialize the database
        fertTipoService.save(fertTipo);

        int databaseSizeBeforeDelete = fertTipoRepository.findAll().size();

        // Delete the fertTipo
        restFertTipoMockMvc.perform(delete("/api/fert-tipos/{id}", fertTipo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FertTipo> fertTipoList = fertTipoRepository.findAll();
        assertThat(fertTipoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FertTipo.class);
        FertTipo fertTipo1 = new FertTipo();
        fertTipo1.setId(1L);
        FertTipo fertTipo2 = new FertTipo();
        fertTipo2.setId(fertTipo1.getId());
        assertThat(fertTipo1).isEqualTo(fertTipo2);
        fertTipo2.setId(2L);
        assertThat(fertTipo1).isNotEqualTo(fertTipo2);
        fertTipo1.setId(null);
        assertThat(fertTipo1).isNotEqualTo(fertTipo2);
    }
}
