package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizDestinazioni;
import com.terrambe.repository.DizDestinazioniRepository;
import com.terrambe.service.DizDestinazioniService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizDestinazioniCriteria;
import com.terrambe.service.DizDestinazioniQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizDestinazioniResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizDestinazioniResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizDestinazioniRepository dizDestinazioniRepository;

    @Autowired
    private DizDestinazioniService dizDestinazioniService;

    @Autowired
    private DizDestinazioniQueryService dizDestinazioniQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizDestinazioniMockMvc;

    private DizDestinazioni dizDestinazioni;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizDestinazioniResource dizDestinazioniResource = new DizDestinazioniResource(dizDestinazioniService, dizDestinazioniQueryService);
        this.restDizDestinazioniMockMvc = MockMvcBuilders.standaloneSetup(dizDestinazioniResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizDestinazioni createEntity(EntityManager em) {
        DizDestinazioni dizDestinazioni = new DizDestinazioni()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizDestinazioni;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizDestinazioni createUpdatedEntity(EntityManager em) {
        DizDestinazioni dizDestinazioni = new DizDestinazioni()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizDestinazioni;
    }

    @BeforeEach
    public void initTest() {
        dizDestinazioni = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizDestinazioni() throws Exception {
        int databaseSizeBeforeCreate = dizDestinazioniRepository.findAll().size();

        // Create the DizDestinazioni
        restDizDestinazioniMockMvc.perform(post("/api/diz-destinazionis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizDestinazioni)))
            .andExpect(status().isCreated());

        // Validate the DizDestinazioni in the database
        List<DizDestinazioni> dizDestinazioniList = dizDestinazioniRepository.findAll();
        assertThat(dizDestinazioniList).hasSize(databaseSizeBeforeCreate + 1);
        DizDestinazioni testDizDestinazioni = dizDestinazioniList.get(dizDestinazioniList.size() - 1);
        assertThat(testDizDestinazioni.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizDestinazioni.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizDestinazioni.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizDestinazioni.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizDestinazioni.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizDestinazioni.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizDestinazioni.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizDestinazioniWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizDestinazioniRepository.findAll().size();

        // Create the DizDestinazioni with an existing ID
        dizDestinazioni.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizDestinazioniMockMvc.perform(post("/api/diz-destinazionis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizDestinazioni)))
            .andExpect(status().isBadRequest());

        // Validate the DizDestinazioni in the database
        List<DizDestinazioni> dizDestinazioniList = dizDestinazioniRepository.findAll();
        assertThat(dizDestinazioniList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizDestinazionis() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizDestinazioni.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizDestinazioni() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get the dizDestinazioni
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis/{id}", dizDestinazioni.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizDestinazioni.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where codice equals to DEFAULT_CODICE
        defaultDizDestinazioniShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizDestinazioniList where codice equals to UPDATED_CODICE
        defaultDizDestinazioniShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizDestinazioniShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizDestinazioniList where codice equals to UPDATED_CODICE
        defaultDizDestinazioniShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where codice is not null
        defaultDizDestinazioniShouldBeFound("codice.specified=true");

        // Get all the dizDestinazioniList where codice is null
        defaultDizDestinazioniShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizDestinazioniShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizDestinazioniList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizDestinazioniShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizDestinazioniShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizDestinazioniList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizDestinazioniShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where descrizione is not null
        defaultDizDestinazioniShouldBeFound("descrizione.specified=true");

        // Get all the dizDestinazioniList where descrizione is null
        defaultDizDestinazioniShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali is not null
        defaultDizDestinazioniShouldBeFound("dataInizVali.specified=true");

        // Get all the dizDestinazioniList where dataInizVali is null
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizDestinazioniShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizDestinazioniList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizDestinazioniShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali is not null
        defaultDizDestinazioniShouldBeFound("dataFineVali.specified=true");

        // Get all the dizDestinazioniList where dataFineVali is null
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizDestinazioniShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizDestinazioniList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizDestinazioniShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator is not null
        defaultDizDestinazioniShouldBeFound("userIdCreator.specified=true");

        // Get all the dizDestinazioniList where userIdCreator is null
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizDestinazioniShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizDestinazioniList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizDestinazioniShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod is not null
        defaultDizDestinazioniShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizDestinazioniList where userIdLastMod is null
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizDestinazionisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizDestinazioniRepository.saveAndFlush(dizDestinazioni);

        // Get all the dizDestinazioniList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizDestinazioniList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizDestinazioniShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizDestinazioniShouldBeFound(String filter) throws Exception {
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizDestinazioni.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizDestinazioniShouldNotBeFound(String filter) throws Exception {
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizDestinazioni() throws Exception {
        // Get the dizDestinazioni
        restDizDestinazioniMockMvc.perform(get("/api/diz-destinazionis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizDestinazioni() throws Exception {
        // Initialize the database
        dizDestinazioniService.save(dizDestinazioni);

        int databaseSizeBeforeUpdate = dizDestinazioniRepository.findAll().size();

        // Update the dizDestinazioni
        DizDestinazioni updatedDizDestinazioni = dizDestinazioniRepository.findById(dizDestinazioni.getId()).get();
        // Disconnect from session so that the updates on updatedDizDestinazioni are not directly saved in db
        em.detach(updatedDizDestinazioni);
        updatedDizDestinazioni
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizDestinazioniMockMvc.perform(put("/api/diz-destinazionis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizDestinazioni)))
            .andExpect(status().isOk());

        // Validate the DizDestinazioni in the database
        List<DizDestinazioni> dizDestinazioniList = dizDestinazioniRepository.findAll();
        assertThat(dizDestinazioniList).hasSize(databaseSizeBeforeUpdate);
        DizDestinazioni testDizDestinazioni = dizDestinazioniList.get(dizDestinazioniList.size() - 1);
        assertThat(testDizDestinazioni.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizDestinazioni.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizDestinazioni.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizDestinazioni.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizDestinazioni.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizDestinazioni.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizDestinazioni.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizDestinazioni() throws Exception {
        int databaseSizeBeforeUpdate = dizDestinazioniRepository.findAll().size();

        // Create the DizDestinazioni

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizDestinazioniMockMvc.perform(put("/api/diz-destinazionis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizDestinazioni)))
            .andExpect(status().isBadRequest());

        // Validate the DizDestinazioni in the database
        List<DizDestinazioni> dizDestinazioniList = dizDestinazioniRepository.findAll();
        assertThat(dizDestinazioniList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizDestinazioni() throws Exception {
        // Initialize the database
        dizDestinazioniService.save(dizDestinazioni);

        int databaseSizeBeforeDelete = dizDestinazioniRepository.findAll().size();

        // Delete the dizDestinazioni
        restDizDestinazioniMockMvc.perform(delete("/api/diz-destinazionis/{id}", dizDestinazioni.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizDestinazioni> dizDestinazioniList = dizDestinazioniRepository.findAll();
        assertThat(dizDestinazioniList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizDestinazioni.class);
        DizDestinazioni dizDestinazioni1 = new DizDestinazioni();
        dizDestinazioni1.setId(1L);
        DizDestinazioni dizDestinazioni2 = new DizDestinazioni();
        dizDestinazioni2.setId(dizDestinazioni1.getId());
        assertThat(dizDestinazioni1).isEqualTo(dizDestinazioni2);
        dizDestinazioni2.setId(2L);
        assertThat(dizDestinazioni1).isNotEqualTo(dizDestinazioni2);
        dizDestinazioni1.setId(null);
        assertThat(dizDestinazioni1).isNotEqualTo(dizDestinazioni2);
    }
}
