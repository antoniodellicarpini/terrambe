package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.repository.ColturaAppezzamentoRepository;
import com.terrambe.service.ColturaAppezzamentoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ColturaAppezzamentoCriteria;
import com.terrambe.service.ColturaAppezzamentoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ColturaAppezzamentoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ColturaAppezzamentoResourceIT {

    private static final Boolean DEFAULT_PRIMARIA = false;
    private static final Boolean UPDATED_PRIMARIA = true;

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZIO_COLTURA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO_COLTURA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO_COLTURA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_COLTURA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_COLTURA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_COLTURA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FIORITURA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FIORITURA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FIORITURA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_FIORITURA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_FIORITURA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_FIORITURA = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_RACCOLTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_RACCOLTA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_RACCOLTA = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_NUMERO_PIANTE = 1;
    private static final Integer UPDATED_NUMERO_PIANTE = 2;
    private static final Integer SMALLER_NUMERO_PIANTE = 1 - 1;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private ColturaAppezzamentoRepository colturaAppezzamentoRepository;

    @Autowired
    private ColturaAppezzamentoService colturaAppezzamentoService;

    @Autowired
    private ColturaAppezzamentoQueryService colturaAppezzamentoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restColturaAppezzamentoMockMvc;

    private ColturaAppezzamento colturaAppezzamento;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ColturaAppezzamentoResource colturaAppezzamentoResource = new ColturaAppezzamentoResource(colturaAppezzamentoService, colturaAppezzamentoQueryService);
        this.restColturaAppezzamentoMockMvc = MockMvcBuilders.standaloneSetup(colturaAppezzamentoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ColturaAppezzamento createEntity(EntityManager em) {
        ColturaAppezzamento colturaAppezzamento = new ColturaAppezzamento()
            .primaria(DEFAULT_PRIMARIA)
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizioColtura(DEFAULT_DATA_INIZIO_COLTURA)
            .dataFineColtura(DEFAULT_DATA_FINE_COLTURA)
            .dataFioritura(DEFAULT_DATA_FIORITURA)
            .dataFineFioritura(DEFAULT_DATA_FINE_FIORITURA)
            .dataRaccolta(DEFAULT_DATA_RACCOLTA)
            .numeroPiante(DEFAULT_NUMERO_PIANTE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return colturaAppezzamento;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ColturaAppezzamento createUpdatedEntity(EntityManager em) {
        ColturaAppezzamento colturaAppezzamento = new ColturaAppezzamento()
            .primaria(UPDATED_PRIMARIA)
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizioColtura(UPDATED_DATA_INIZIO_COLTURA)
            .dataFineColtura(UPDATED_DATA_FINE_COLTURA)
            .dataFioritura(UPDATED_DATA_FIORITURA)
            .dataFineFioritura(UPDATED_DATA_FINE_FIORITURA)
            .dataRaccolta(UPDATED_DATA_RACCOLTA)
            .numeroPiante(UPDATED_NUMERO_PIANTE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return colturaAppezzamento;
    }

    @BeforeEach
    public void initTest() {
        colturaAppezzamento = createEntity(em);
    }

    @Test
    @Transactional
    public void createColturaAppezzamento() throws Exception {
        int databaseSizeBeforeCreate = colturaAppezzamentoRepository.findAll().size();

        // Create the ColturaAppezzamento
        restColturaAppezzamentoMockMvc.perform(post("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isCreated());

        // Validate the ColturaAppezzamento in the database
        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeCreate + 1);
        ColturaAppezzamento testColturaAppezzamento = colturaAppezzamentoList.get(colturaAppezzamentoList.size() - 1);
        assertThat(testColturaAppezzamento.isPrimaria()).isEqualTo(DEFAULT_PRIMARIA);
        assertThat(testColturaAppezzamento.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testColturaAppezzamento.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testColturaAppezzamento.getDataInizioColtura()).isEqualTo(DEFAULT_DATA_INIZIO_COLTURA);
        assertThat(testColturaAppezzamento.getDataFineColtura()).isEqualTo(DEFAULT_DATA_FINE_COLTURA);
        assertThat(testColturaAppezzamento.getDataFioritura()).isEqualTo(DEFAULT_DATA_FIORITURA);
        assertThat(testColturaAppezzamento.getDataFineFioritura()).isEqualTo(DEFAULT_DATA_FINE_FIORITURA);
        assertThat(testColturaAppezzamento.getDataRaccolta()).isEqualTo(DEFAULT_DATA_RACCOLTA);
        assertThat(testColturaAppezzamento.getNumeroPiante()).isEqualTo(DEFAULT_NUMERO_PIANTE);
        assertThat(testColturaAppezzamento.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testColturaAppezzamento.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testColturaAppezzamento.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testColturaAppezzamento.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testColturaAppezzamento.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createColturaAppezzamentoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = colturaAppezzamentoRepository.findAll().size();

        // Create the ColturaAppezzamento with an existing ID
        colturaAppezzamento.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restColturaAppezzamentoMockMvc.perform(post("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isBadRequest());

        // Validate the ColturaAppezzamento in the database
        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = colturaAppezzamentoRepository.findAll().size();
        // set the field null
        colturaAppezzamento.setIdAzienda(null);

        // Create the ColturaAppezzamento, which fails.

        restColturaAppezzamentoMockMvc.perform(post("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isBadRequest());

        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataInizioColturaIsRequired() throws Exception {
        int databaseSizeBeforeTest = colturaAppezzamentoRepository.findAll().size();
        // set the field null
        colturaAppezzamento.setDataInizioColtura(null);

        // Create the ColturaAppezzamento, which fails.

        restColturaAppezzamentoMockMvc.perform(post("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isBadRequest());

        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataFineColturaIsRequired() throws Exception {
        int databaseSizeBeforeTest = colturaAppezzamentoRepository.findAll().size();
        // set the field null
        colturaAppezzamento.setDataFineColtura(null);

        // Create the ColturaAppezzamento, which fails.

        restColturaAppezzamentoMockMvc.perform(post("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isBadRequest());

        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentos() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(colturaAppezzamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].primaria").value(hasItem(DEFAULT_PRIMARIA.booleanValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizioColtura").value(hasItem(DEFAULT_DATA_INIZIO_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].dataFineColtura").value(hasItem(DEFAULT_DATA_FINE_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].dataFioritura").value(hasItem(DEFAULT_DATA_FIORITURA.toString())))
            .andExpect(jsonPath("$.[*].dataFineFioritura").value(hasItem(DEFAULT_DATA_FINE_FIORITURA.toString())))
            .andExpect(jsonPath("$.[*].dataRaccolta").value(hasItem(DEFAULT_DATA_RACCOLTA.toString())))
            .andExpect(jsonPath("$.[*].numeroPiante").value(hasItem(DEFAULT_NUMERO_PIANTE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getColturaAppezzamento() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get the colturaAppezzamento
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos/{id}", colturaAppezzamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(colturaAppezzamento.getId().intValue()))
            .andExpect(jsonPath("$.primaria").value(DEFAULT_PRIMARIA.booleanValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizioColtura").value(DEFAULT_DATA_INIZIO_COLTURA.toString()))
            .andExpect(jsonPath("$.dataFineColtura").value(DEFAULT_DATA_FINE_COLTURA.toString()))
            .andExpect(jsonPath("$.dataFioritura").value(DEFAULT_DATA_FIORITURA.toString()))
            .andExpect(jsonPath("$.dataFineFioritura").value(DEFAULT_DATA_FINE_FIORITURA.toString()))
            .andExpect(jsonPath("$.dataRaccolta").value(DEFAULT_DATA_RACCOLTA.toString()))
            .andExpect(jsonPath("$.numeroPiante").value(DEFAULT_NUMERO_PIANTE))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByPrimariaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where primaria equals to DEFAULT_PRIMARIA
        defaultColturaAppezzamentoShouldBeFound("primaria.equals=" + DEFAULT_PRIMARIA);

        // Get all the colturaAppezzamentoList where primaria equals to UPDATED_PRIMARIA
        defaultColturaAppezzamentoShouldNotBeFound("primaria.equals=" + UPDATED_PRIMARIA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByPrimariaIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where primaria in DEFAULT_PRIMARIA or UPDATED_PRIMARIA
        defaultColturaAppezzamentoShouldBeFound("primaria.in=" + DEFAULT_PRIMARIA + "," + UPDATED_PRIMARIA);

        // Get all the colturaAppezzamentoList where primaria equals to UPDATED_PRIMARIA
        defaultColturaAppezzamentoShouldNotBeFound("primaria.in=" + UPDATED_PRIMARIA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByPrimariaIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where primaria is not null
        defaultColturaAppezzamentoShouldBeFound("primaria.specified=true");

        // Get all the colturaAppezzamentoList where primaria is null
        defaultColturaAppezzamentoShouldNotBeFound("primaria.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda is not null
        defaultColturaAppezzamentoShouldBeFound("idAzienda.specified=true");

        // Get all the colturaAppezzamentoList where idAzienda is null
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultColturaAppezzamentoShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the colturaAppezzamentoList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultColturaAppezzamentoShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd is not null
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.specified=true");

        // Get all the colturaAppezzamentoList where idUnitaProd is null
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the colturaAppezzamentoList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultColturaAppezzamentoShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura equals to DEFAULT_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.equals=" + DEFAULT_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura equals to UPDATED_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.equals=" + UPDATED_DATA_INIZIO_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura in DEFAULT_DATA_INIZIO_COLTURA or UPDATED_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.in=" + DEFAULT_DATA_INIZIO_COLTURA + "," + UPDATED_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura equals to UPDATED_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.in=" + UPDATED_DATA_INIZIO_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura is not null
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.specified=true");

        // Get all the colturaAppezzamentoList where dataInizioColtura is null
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura is greater than or equal to DEFAULT_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura is greater than or equal to UPDATED_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.greaterThanOrEqual=" + UPDATED_DATA_INIZIO_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura is less than or equal to DEFAULT_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.lessThanOrEqual=" + DEFAULT_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura is less than or equal to SMALLER_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.lessThanOrEqual=" + SMALLER_DATA_INIZIO_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura is less than DEFAULT_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.lessThan=" + DEFAULT_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura is less than UPDATED_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.lessThan=" + UPDATED_DATA_INIZIO_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizioColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizioColtura is greater than DEFAULT_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataInizioColtura.greaterThan=" + DEFAULT_DATA_INIZIO_COLTURA);

        // Get all the colturaAppezzamentoList where dataInizioColtura is greater than SMALLER_DATA_INIZIO_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataInizioColtura.greaterThan=" + SMALLER_DATA_INIZIO_COLTURA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura equals to DEFAULT_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.equals=" + DEFAULT_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura equals to UPDATED_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.equals=" + UPDATED_DATA_FINE_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura in DEFAULT_DATA_FINE_COLTURA or UPDATED_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.in=" + DEFAULT_DATA_FINE_COLTURA + "," + UPDATED_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura equals to UPDATED_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.in=" + UPDATED_DATA_FINE_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura is not null
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.specified=true");

        // Get all the colturaAppezzamentoList where dataFineColtura is null
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura is greater than or equal to DEFAULT_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.greaterThanOrEqual=" + DEFAULT_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura is greater than or equal to UPDATED_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.greaterThanOrEqual=" + UPDATED_DATA_FINE_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura is less than or equal to DEFAULT_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.lessThanOrEqual=" + DEFAULT_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura is less than or equal to SMALLER_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.lessThanOrEqual=" + SMALLER_DATA_FINE_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura is less than DEFAULT_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.lessThan=" + DEFAULT_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura is less than UPDATED_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.lessThan=" + UPDATED_DATA_FINE_COLTURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineColtura is greater than DEFAULT_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineColtura.greaterThan=" + DEFAULT_DATA_FINE_COLTURA);

        // Get all the colturaAppezzamentoList where dataFineColtura is greater than SMALLER_DATA_FINE_COLTURA
        defaultColturaAppezzamentoShouldBeFound("dataFineColtura.greaterThan=" + SMALLER_DATA_FINE_COLTURA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura equals to DEFAULT_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.equals=" + DEFAULT_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura equals to UPDATED_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.equals=" + UPDATED_DATA_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura in DEFAULT_DATA_FIORITURA or UPDATED_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.in=" + DEFAULT_DATA_FIORITURA + "," + UPDATED_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura equals to UPDATED_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.in=" + UPDATED_DATA_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura is not null
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.specified=true");

        // Get all the colturaAppezzamentoList where dataFioritura is null
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura is greater than or equal to DEFAULT_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.greaterThanOrEqual=" + DEFAULT_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura is greater than or equal to UPDATED_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.greaterThanOrEqual=" + UPDATED_DATA_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura is less than or equal to DEFAULT_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.lessThanOrEqual=" + DEFAULT_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura is less than or equal to SMALLER_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.lessThanOrEqual=" + SMALLER_DATA_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura is less than DEFAULT_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.lessThan=" + DEFAULT_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura is less than UPDATED_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.lessThan=" + UPDATED_DATA_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFiorituraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFioritura is greater than DEFAULT_DATA_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFioritura.greaterThan=" + DEFAULT_DATA_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFioritura is greater than SMALLER_DATA_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFioritura.greaterThan=" + SMALLER_DATA_FIORITURA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura equals to DEFAULT_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.equals=" + DEFAULT_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura equals to UPDATED_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.equals=" + UPDATED_DATA_FINE_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura in DEFAULT_DATA_FINE_FIORITURA or UPDATED_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.in=" + DEFAULT_DATA_FINE_FIORITURA + "," + UPDATED_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura equals to UPDATED_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.in=" + UPDATED_DATA_FINE_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura is not null
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.specified=true");

        // Get all the colturaAppezzamentoList where dataFineFioritura is null
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura is greater than or equal to DEFAULT_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.greaterThanOrEqual=" + DEFAULT_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura is greater than or equal to UPDATED_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.greaterThanOrEqual=" + UPDATED_DATA_FINE_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura is less than or equal to DEFAULT_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.lessThanOrEqual=" + DEFAULT_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura is less than or equal to SMALLER_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.lessThanOrEqual=" + SMALLER_DATA_FINE_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura is less than DEFAULT_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.lessThan=" + DEFAULT_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura is less than UPDATED_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.lessThan=" + UPDATED_DATA_FINE_FIORITURA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineFiorituraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineFioritura is greater than DEFAULT_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldNotBeFound("dataFineFioritura.greaterThan=" + DEFAULT_DATA_FINE_FIORITURA);

        // Get all the colturaAppezzamentoList where dataFineFioritura is greater than SMALLER_DATA_FINE_FIORITURA
        defaultColturaAppezzamentoShouldBeFound("dataFineFioritura.greaterThan=" + SMALLER_DATA_FINE_FIORITURA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta equals to DEFAULT_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.equals=" + DEFAULT_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta equals to UPDATED_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.equals=" + UPDATED_DATA_RACCOLTA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta in DEFAULT_DATA_RACCOLTA or UPDATED_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.in=" + DEFAULT_DATA_RACCOLTA + "," + UPDATED_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta equals to UPDATED_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.in=" + UPDATED_DATA_RACCOLTA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta is not null
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.specified=true");

        // Get all the colturaAppezzamentoList where dataRaccolta is null
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta is greater than or equal to DEFAULT_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.greaterThanOrEqual=" + DEFAULT_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta is greater than or equal to UPDATED_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.greaterThanOrEqual=" + UPDATED_DATA_RACCOLTA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta is less than or equal to DEFAULT_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.lessThanOrEqual=" + DEFAULT_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta is less than or equal to SMALLER_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.lessThanOrEqual=" + SMALLER_DATA_RACCOLTA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta is less than DEFAULT_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.lessThan=" + DEFAULT_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta is less than UPDATED_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.lessThan=" + UPDATED_DATA_RACCOLTA);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataRaccoltaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataRaccolta is greater than DEFAULT_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldNotBeFound("dataRaccolta.greaterThan=" + DEFAULT_DATA_RACCOLTA);

        // Get all the colturaAppezzamentoList where dataRaccolta is greater than SMALLER_DATA_RACCOLTA
        defaultColturaAppezzamentoShouldBeFound("dataRaccolta.greaterThan=" + SMALLER_DATA_RACCOLTA);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante equals to DEFAULT_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.equals=" + DEFAULT_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante equals to UPDATED_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.equals=" + UPDATED_NUMERO_PIANTE);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante in DEFAULT_NUMERO_PIANTE or UPDATED_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.in=" + DEFAULT_NUMERO_PIANTE + "," + UPDATED_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante equals to UPDATED_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.in=" + UPDATED_NUMERO_PIANTE);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante is not null
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.specified=true");

        // Get all the colturaAppezzamentoList where numeroPiante is null
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante is greater than or equal to DEFAULT_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.greaterThanOrEqual=" + DEFAULT_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante is greater than or equal to UPDATED_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.greaterThanOrEqual=" + UPDATED_NUMERO_PIANTE);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante is less than or equal to DEFAULT_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.lessThanOrEqual=" + DEFAULT_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante is less than or equal to SMALLER_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.lessThanOrEqual=" + SMALLER_NUMERO_PIANTE);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante is less than DEFAULT_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.lessThan=" + DEFAULT_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante is less than UPDATED_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.lessThan=" + UPDATED_NUMERO_PIANTE);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByNumeroPianteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where numeroPiante is greater than DEFAULT_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldNotBeFound("numeroPiante.greaterThan=" + DEFAULT_NUMERO_PIANTE);

        // Get all the colturaAppezzamentoList where numeroPiante is greater than SMALLER_NUMERO_PIANTE
        defaultColturaAppezzamentoShouldBeFound("numeroPiante.greaterThan=" + SMALLER_NUMERO_PIANTE);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali is not null
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.specified=true");

        // Get all the colturaAppezzamentoList where dataInizVali is null
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the colturaAppezzamentoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultColturaAppezzamentoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali is not null
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.specified=true");

        // Get all the colturaAppezzamentoList where dataFineVali is null
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the colturaAppezzamentoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultColturaAppezzamentoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator is not null
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.specified=true");

        // Get all the colturaAppezzamentoList where userIdCreator is null
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the colturaAppezzamentoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultColturaAppezzamentoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod is not null
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.specified=true");

        // Get all the colturaAppezzamentoList where userIdLastMod is null
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllColturaAppezzamentosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);

        // Get all the colturaAppezzamentoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the colturaAppezzamentoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultColturaAppezzamentoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByColtToAppeIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);
        Appezzamenti coltToAppe = AppezzamentiResourceIT.createEntity(em);
        em.persist(coltToAppe);
        em.flush();
        colturaAppezzamento.setColtToAppe(coltToAppe);
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);
        Long coltToAppeId = coltToAppe.getId();

        // Get all the colturaAppezzamentoList where coltToAppe equals to coltToAppeId
        defaultColturaAppezzamentoShouldBeFound("coltToAppeId.equals=" + coltToAppeId);

        // Get all the colturaAppezzamentoList where coltToAppe equals to coltToAppeId + 1
        defaultColturaAppezzamentoShouldNotBeFound("coltToAppeId.equals=" + (coltToAppeId + 1));
    }


    @Test
    @Transactional
    public void getAllColturaAppezzamentosByColtToAgeaIsEqualToSomething() throws Exception {
        // Initialize the database
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);
        Agea20152020Matrice coltToAgea = Agea20152020MatriceResourceIT.createEntity(em);
        em.persist(coltToAgea);
        em.flush();
        colturaAppezzamento.setColtToAgea(coltToAgea);
        colturaAppezzamentoRepository.saveAndFlush(colturaAppezzamento);
        Long coltToAgeaId = coltToAgea.getId();

        // Get all the colturaAppezzamentoList where coltToAgea equals to coltToAgeaId
        defaultColturaAppezzamentoShouldBeFound("coltToAgeaId.equals=" + coltToAgeaId);

        // Get all the colturaAppezzamentoList where coltToAgea equals to coltToAgeaId + 1
        defaultColturaAppezzamentoShouldNotBeFound("coltToAgeaId.equals=" + (coltToAgeaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultColturaAppezzamentoShouldBeFound(String filter) throws Exception {
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(colturaAppezzamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].primaria").value(hasItem(DEFAULT_PRIMARIA.booleanValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizioColtura").value(hasItem(DEFAULT_DATA_INIZIO_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].dataFineColtura").value(hasItem(DEFAULT_DATA_FINE_COLTURA.toString())))
            .andExpect(jsonPath("$.[*].dataFioritura").value(hasItem(DEFAULT_DATA_FIORITURA.toString())))
            .andExpect(jsonPath("$.[*].dataFineFioritura").value(hasItem(DEFAULT_DATA_FINE_FIORITURA.toString())))
            .andExpect(jsonPath("$.[*].dataRaccolta").value(hasItem(DEFAULT_DATA_RACCOLTA.toString())))
            .andExpect(jsonPath("$.[*].numeroPiante").value(hasItem(DEFAULT_NUMERO_PIANTE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultColturaAppezzamentoShouldNotBeFound(String filter) throws Exception {
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingColturaAppezzamento() throws Exception {
        // Get the colturaAppezzamento
        restColturaAppezzamentoMockMvc.perform(get("/api/coltura-appezzamentos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateColturaAppezzamento() throws Exception {
        // Initialize the database
        colturaAppezzamentoService.save(colturaAppezzamento);

        int databaseSizeBeforeUpdate = colturaAppezzamentoRepository.findAll().size();

        // Update the colturaAppezzamento
        ColturaAppezzamento updatedColturaAppezzamento = colturaAppezzamentoRepository.findById(colturaAppezzamento.getId()).get();
        // Disconnect from session so that the updates on updatedColturaAppezzamento are not directly saved in db
        em.detach(updatedColturaAppezzamento);
        updatedColturaAppezzamento
            .primaria(UPDATED_PRIMARIA)
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizioColtura(UPDATED_DATA_INIZIO_COLTURA)
            .dataFineColtura(UPDATED_DATA_FINE_COLTURA)
            .dataFioritura(UPDATED_DATA_FIORITURA)
            .dataFineFioritura(UPDATED_DATA_FINE_FIORITURA)
            .dataRaccolta(UPDATED_DATA_RACCOLTA)
            .numeroPiante(UPDATED_NUMERO_PIANTE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restColturaAppezzamentoMockMvc.perform(put("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedColturaAppezzamento)))
            .andExpect(status().isOk());

        // Validate the ColturaAppezzamento in the database
        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeUpdate);
        ColturaAppezzamento testColturaAppezzamento = colturaAppezzamentoList.get(colturaAppezzamentoList.size() - 1);
        assertThat(testColturaAppezzamento.isPrimaria()).isEqualTo(UPDATED_PRIMARIA);
        assertThat(testColturaAppezzamento.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testColturaAppezzamento.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testColturaAppezzamento.getDataInizioColtura()).isEqualTo(UPDATED_DATA_INIZIO_COLTURA);
        assertThat(testColturaAppezzamento.getDataFineColtura()).isEqualTo(UPDATED_DATA_FINE_COLTURA);
        assertThat(testColturaAppezzamento.getDataFioritura()).isEqualTo(UPDATED_DATA_FIORITURA);
        assertThat(testColturaAppezzamento.getDataFineFioritura()).isEqualTo(UPDATED_DATA_FINE_FIORITURA);
        assertThat(testColturaAppezzamento.getDataRaccolta()).isEqualTo(UPDATED_DATA_RACCOLTA);
        assertThat(testColturaAppezzamento.getNumeroPiante()).isEqualTo(UPDATED_NUMERO_PIANTE);
        assertThat(testColturaAppezzamento.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testColturaAppezzamento.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testColturaAppezzamento.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testColturaAppezzamento.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testColturaAppezzamento.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingColturaAppezzamento() throws Exception {
        int databaseSizeBeforeUpdate = colturaAppezzamentoRepository.findAll().size();

        // Create the ColturaAppezzamento

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restColturaAppezzamentoMockMvc.perform(put("/api/coltura-appezzamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colturaAppezzamento)))
            .andExpect(status().isBadRequest());

        // Validate the ColturaAppezzamento in the database
        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteColturaAppezzamento() throws Exception {
        // Initialize the database
        colturaAppezzamentoService.save(colturaAppezzamento);

        int databaseSizeBeforeDelete = colturaAppezzamentoRepository.findAll().size();

        // Delete the colturaAppezzamento
        restColturaAppezzamentoMockMvc.perform(delete("/api/coltura-appezzamentos/{id}", colturaAppezzamento.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ColturaAppezzamento> colturaAppezzamentoList = colturaAppezzamentoRepository.findAll();
        assertThat(colturaAppezzamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ColturaAppezzamento.class);
        ColturaAppezzamento colturaAppezzamento1 = new ColturaAppezzamento();
        colturaAppezzamento1.setId(1L);
        ColturaAppezzamento colturaAppezzamento2 = new ColturaAppezzamento();
        colturaAppezzamento2.setId(colturaAppezzamento1.getId());
        assertThat(colturaAppezzamento1).isEqualTo(colturaAppezzamento2);
        colturaAppezzamento2.setId(2L);
        assertThat(colturaAppezzamento1).isNotEqualTo(colturaAppezzamento2);
        colturaAppezzamento1.setId(null);
        assertThat(colturaAppezzamento1).isNotEqualTo(colturaAppezzamento2);
    }
}
