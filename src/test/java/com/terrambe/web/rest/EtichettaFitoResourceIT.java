package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.domain.ProbabileFitoTossicita;
import com.terrambe.domain.NoteAggiuntiveEtichetta;
import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.domain.EtichettaPittogrammi;
import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.domain.TitolareRegistrazione;
import com.terrambe.domain.Formulazione;
import com.terrambe.repository.EtichettaFitoRepository;
import com.terrambe.service.EtichettaFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.EtichettaFitoCriteria;
import com.terrambe.service.EtichettaFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtichettaFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class EtichettaFitoResourceIT {

    private static final String DEFAULT_NOME_COMMERCIALE = "AAAAAAAAAA";
    private static final String UPDATED_NOME_COMMERCIALE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_REG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_REG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_REG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_REGISTRAZIONE_MINISTERIALE = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRAZIONE_MINISTERIALE = "BBBBBBBBBB";

    private static final String DEFAULT_SIGLA = "AAAAAAAAAA";
    private static final String UPDATED_SIGLA = "BBBBBBBBBB";

    private static final String DEFAULT_BIO = "AAAAAAAAAA";
    private static final String UPDATED_BIO = "BBBBBBBBBB";

    private static final String DEFAULT_PPO = "AAAAAAAAAA";
    private static final String UPDATED_PPO = "BBBBBBBBBB";

    private static final String DEFAULT_REVOCATO = "AAAAAAAAAA";
    private static final String UPDATED_REVOCATO = "BBBBBBBBBB";

    private static final String DEFAULT_REVOCA_AUTORIZZAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_REVOCA_AUTORIZZAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_SCADENZA_COMMERCIO = "AAAAAAAAAA";
    private static final String UPDATED_SCADENZA_COMMERCIO = "BBBBBBBBBB";

    private static final String DEFAULT_SCADENZA_UTILIZZO = "AAAAAAAAAA";
    private static final String UPDATED_SCADENZA_UTILIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_AVV_CLP = "AAAAAAAAAA";
    private static final String UPDATED_AVV_CLP = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private EtichettaFitoRepository etichettaFitoRepository;

    @Autowired
    private EtichettaFitoService etichettaFitoService;

    @Autowired
    private EtichettaFitoQueryService etichettaFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEtichettaFitoMockMvc;

    private EtichettaFito etichettaFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EtichettaFitoResource etichettaFitoResource = new EtichettaFitoResource(etichettaFitoService, etichettaFitoQueryService);
        this.restEtichettaFitoMockMvc = MockMvcBuilders.standaloneSetup(etichettaFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaFito createEntity(EntityManager em) {
        EtichettaFito etichettaFito = new EtichettaFito()
            .nomeCommerciale(DEFAULT_NOME_COMMERCIALE)
            .dataReg(DEFAULT_DATA_REG)
            .registrazioneMinisteriale(DEFAULT_REGISTRAZIONE_MINISTERIALE)
            .sigla(DEFAULT_SIGLA)
            .bio(DEFAULT_BIO)
            .ppo(DEFAULT_PPO)
            .revocato(DEFAULT_REVOCATO)
            .revocaAutorizzazione(DEFAULT_REVOCA_AUTORIZZAZIONE)
            .scadenzaCommercio(DEFAULT_SCADENZA_COMMERCIO)
            .scadenzaUtilizzo(DEFAULT_SCADENZA_UTILIZZO)
            .avvClp(DEFAULT_AVV_CLP)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return etichettaFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaFito createUpdatedEntity(EntityManager em) {
        EtichettaFito etichettaFito = new EtichettaFito()
            .nomeCommerciale(UPDATED_NOME_COMMERCIALE)
            .dataReg(UPDATED_DATA_REG)
            .registrazioneMinisteriale(UPDATED_REGISTRAZIONE_MINISTERIALE)
            .sigla(UPDATED_SIGLA)
            .bio(UPDATED_BIO)
            .ppo(UPDATED_PPO)
            .revocato(UPDATED_REVOCATO)
            .revocaAutorizzazione(UPDATED_REVOCA_AUTORIZZAZIONE)
            .scadenzaCommercio(UPDATED_SCADENZA_COMMERCIO)
            .scadenzaUtilizzo(UPDATED_SCADENZA_UTILIZZO)
            .avvClp(UPDATED_AVV_CLP)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return etichettaFito;
    }

    @BeforeEach
    public void initTest() {
        etichettaFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtichettaFito() throws Exception {
        int databaseSizeBeforeCreate = etichettaFitoRepository.findAll().size();

        // Create the EtichettaFito
        restEtichettaFitoMockMvc.perform(post("/api/etichetta-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFito)))
            .andExpect(status().isCreated());

        // Validate the EtichettaFito in the database
        List<EtichettaFito> etichettaFitoList = etichettaFitoRepository.findAll();
        assertThat(etichettaFitoList).hasSize(databaseSizeBeforeCreate + 1);
        EtichettaFito testEtichettaFito = etichettaFitoList.get(etichettaFitoList.size() - 1);
        assertThat(testEtichettaFito.getNomeCommerciale()).isEqualTo(DEFAULT_NOME_COMMERCIALE);
        assertThat(testEtichettaFito.getDataReg()).isEqualTo(DEFAULT_DATA_REG);
        assertThat(testEtichettaFito.getRegistrazioneMinisteriale()).isEqualTo(DEFAULT_REGISTRAZIONE_MINISTERIALE);
        assertThat(testEtichettaFito.getSigla()).isEqualTo(DEFAULT_SIGLA);
        assertThat(testEtichettaFito.getBio()).isEqualTo(DEFAULT_BIO);
        assertThat(testEtichettaFito.getPpo()).isEqualTo(DEFAULT_PPO);
        assertThat(testEtichettaFito.getRevocato()).isEqualTo(DEFAULT_REVOCATO);
        assertThat(testEtichettaFito.getRevocaAutorizzazione()).isEqualTo(DEFAULT_REVOCA_AUTORIZZAZIONE);
        assertThat(testEtichettaFito.getScadenzaCommercio()).isEqualTo(DEFAULT_SCADENZA_COMMERCIO);
        assertThat(testEtichettaFito.getScadenzaUtilizzo()).isEqualTo(DEFAULT_SCADENZA_UTILIZZO);
        assertThat(testEtichettaFito.getAvvClp()).isEqualTo(DEFAULT_AVV_CLP);
        assertThat(testEtichettaFito.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testEtichettaFito.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testEtichettaFito.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testEtichettaFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testEtichettaFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testEtichettaFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testEtichettaFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testEtichettaFito.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createEtichettaFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etichettaFitoRepository.findAll().size();

        // Create the EtichettaFito with an existing ID
        etichettaFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtichettaFitoMockMvc.perform(post("/api/etichetta-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFito)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaFito in the database
        List<EtichettaFito> etichettaFitoList = etichettaFitoRepository.findAll();
        assertThat(etichettaFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitos() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeCommerciale").value(hasItem(DEFAULT_NOME_COMMERCIALE.toString())))
            .andExpect(jsonPath("$.[*].dataReg").value(hasItem(DEFAULT_DATA_REG.toString())))
            .andExpect(jsonPath("$.[*].registrazioneMinisteriale").value(hasItem(DEFAULT_REGISTRAZIONE_MINISTERIALE.toString())))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA.toString())))
            .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO.toString())))
            .andExpect(jsonPath("$.[*].ppo").value(hasItem(DEFAULT_PPO.toString())))
            .andExpect(jsonPath("$.[*].revocato").value(hasItem(DEFAULT_REVOCATO.toString())))
            .andExpect(jsonPath("$.[*].revocaAutorizzazione").value(hasItem(DEFAULT_REVOCA_AUTORIZZAZIONE.toString())))
            .andExpect(jsonPath("$.[*].scadenzaCommercio").value(hasItem(DEFAULT_SCADENZA_COMMERCIO.toString())))
            .andExpect(jsonPath("$.[*].scadenzaUtilizzo").value(hasItem(DEFAULT_SCADENZA_UTILIZZO.toString())))
            .andExpect(jsonPath("$.[*].avvClp").value(hasItem(DEFAULT_AVV_CLP.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtichettaFito() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get the etichettaFito
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos/{id}", etichettaFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etichettaFito.getId().intValue()))
            .andExpect(jsonPath("$.nomeCommerciale").value(DEFAULT_NOME_COMMERCIALE.toString()))
            .andExpect(jsonPath("$.dataReg").value(DEFAULT_DATA_REG.toString()))
            .andExpect(jsonPath("$.registrazioneMinisteriale").value(DEFAULT_REGISTRAZIONE_MINISTERIALE.toString()))
            .andExpect(jsonPath("$.sigla").value(DEFAULT_SIGLA.toString()))
            .andExpect(jsonPath("$.bio").value(DEFAULT_BIO.toString()))
            .andExpect(jsonPath("$.ppo").value(DEFAULT_PPO.toString()))
            .andExpect(jsonPath("$.revocato").value(DEFAULT_REVOCATO.toString()))
            .andExpect(jsonPath("$.revocaAutorizzazione").value(DEFAULT_REVOCA_AUTORIZZAZIONE.toString()))
            .andExpect(jsonPath("$.scadenzaCommercio").value(DEFAULT_SCADENZA_COMMERCIO.toString()))
            .andExpect(jsonPath("$.scadenzaUtilizzo").value(DEFAULT_SCADENZA_UTILIZZO.toString()))
            .andExpect(jsonPath("$.avvClp").value(DEFAULT_AVV_CLP.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByNomeCommercialeIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where nomeCommerciale equals to DEFAULT_NOME_COMMERCIALE
        defaultEtichettaFitoShouldBeFound("nomeCommerciale.equals=" + DEFAULT_NOME_COMMERCIALE);

        // Get all the etichettaFitoList where nomeCommerciale equals to UPDATED_NOME_COMMERCIALE
        defaultEtichettaFitoShouldNotBeFound("nomeCommerciale.equals=" + UPDATED_NOME_COMMERCIALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByNomeCommercialeIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where nomeCommerciale in DEFAULT_NOME_COMMERCIALE or UPDATED_NOME_COMMERCIALE
        defaultEtichettaFitoShouldBeFound("nomeCommerciale.in=" + DEFAULT_NOME_COMMERCIALE + "," + UPDATED_NOME_COMMERCIALE);

        // Get all the etichettaFitoList where nomeCommerciale equals to UPDATED_NOME_COMMERCIALE
        defaultEtichettaFitoShouldNotBeFound("nomeCommerciale.in=" + UPDATED_NOME_COMMERCIALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByNomeCommercialeIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where nomeCommerciale is not null
        defaultEtichettaFitoShouldBeFound("nomeCommerciale.specified=true");

        // Get all the etichettaFitoList where nomeCommerciale is null
        defaultEtichettaFitoShouldNotBeFound("nomeCommerciale.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg equals to DEFAULT_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.equals=" + DEFAULT_DATA_REG);

        // Get all the etichettaFitoList where dataReg equals to UPDATED_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.equals=" + UPDATED_DATA_REG);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg in DEFAULT_DATA_REG or UPDATED_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.in=" + DEFAULT_DATA_REG + "," + UPDATED_DATA_REG);

        // Get all the etichettaFitoList where dataReg equals to UPDATED_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.in=" + UPDATED_DATA_REG);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg is not null
        defaultEtichettaFitoShouldBeFound("dataReg.specified=true");

        // Get all the etichettaFitoList where dataReg is null
        defaultEtichettaFitoShouldNotBeFound("dataReg.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg is greater than or equal to DEFAULT_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.greaterThanOrEqual=" + DEFAULT_DATA_REG);

        // Get all the etichettaFitoList where dataReg is greater than or equal to UPDATED_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.greaterThanOrEqual=" + UPDATED_DATA_REG);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg is less than or equal to DEFAULT_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.lessThanOrEqual=" + DEFAULT_DATA_REG);

        // Get all the etichettaFitoList where dataReg is less than or equal to SMALLER_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.lessThanOrEqual=" + SMALLER_DATA_REG);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg is less than DEFAULT_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.lessThan=" + DEFAULT_DATA_REG);

        // Get all the etichettaFitoList where dataReg is less than UPDATED_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.lessThan=" + UPDATED_DATA_REG);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataRegIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataReg is greater than DEFAULT_DATA_REG
        defaultEtichettaFitoShouldNotBeFound("dataReg.greaterThan=" + DEFAULT_DATA_REG);

        // Get all the etichettaFitoList where dataReg is greater than SMALLER_DATA_REG
        defaultEtichettaFitoShouldBeFound("dataReg.greaterThan=" + SMALLER_DATA_REG);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByRegistrazioneMinisterialeIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where registrazioneMinisteriale equals to DEFAULT_REGISTRAZIONE_MINISTERIALE
        defaultEtichettaFitoShouldBeFound("registrazioneMinisteriale.equals=" + DEFAULT_REGISTRAZIONE_MINISTERIALE);

        // Get all the etichettaFitoList where registrazioneMinisteriale equals to UPDATED_REGISTRAZIONE_MINISTERIALE
        defaultEtichettaFitoShouldNotBeFound("registrazioneMinisteriale.equals=" + UPDATED_REGISTRAZIONE_MINISTERIALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRegistrazioneMinisterialeIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where registrazioneMinisteriale in DEFAULT_REGISTRAZIONE_MINISTERIALE or UPDATED_REGISTRAZIONE_MINISTERIALE
        defaultEtichettaFitoShouldBeFound("registrazioneMinisteriale.in=" + DEFAULT_REGISTRAZIONE_MINISTERIALE + "," + UPDATED_REGISTRAZIONE_MINISTERIALE);

        // Get all the etichettaFitoList where registrazioneMinisteriale equals to UPDATED_REGISTRAZIONE_MINISTERIALE
        defaultEtichettaFitoShouldNotBeFound("registrazioneMinisteriale.in=" + UPDATED_REGISTRAZIONE_MINISTERIALE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRegistrazioneMinisterialeIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where registrazioneMinisteriale is not null
        defaultEtichettaFitoShouldBeFound("registrazioneMinisteriale.specified=true");

        // Get all the etichettaFitoList where registrazioneMinisteriale is null
        defaultEtichettaFitoShouldNotBeFound("registrazioneMinisteriale.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosBySiglaIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where sigla equals to DEFAULT_SIGLA
        defaultEtichettaFitoShouldBeFound("sigla.equals=" + DEFAULT_SIGLA);

        // Get all the etichettaFitoList where sigla equals to UPDATED_SIGLA
        defaultEtichettaFitoShouldNotBeFound("sigla.equals=" + UPDATED_SIGLA);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosBySiglaIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where sigla in DEFAULT_SIGLA or UPDATED_SIGLA
        defaultEtichettaFitoShouldBeFound("sigla.in=" + DEFAULT_SIGLA + "," + UPDATED_SIGLA);

        // Get all the etichettaFitoList where sigla equals to UPDATED_SIGLA
        defaultEtichettaFitoShouldNotBeFound("sigla.in=" + UPDATED_SIGLA);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosBySiglaIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where sigla is not null
        defaultEtichettaFitoShouldBeFound("sigla.specified=true");

        // Get all the etichettaFitoList where sigla is null
        defaultEtichettaFitoShouldNotBeFound("sigla.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByBioIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where bio equals to DEFAULT_BIO
        defaultEtichettaFitoShouldBeFound("bio.equals=" + DEFAULT_BIO);

        // Get all the etichettaFitoList where bio equals to UPDATED_BIO
        defaultEtichettaFitoShouldNotBeFound("bio.equals=" + UPDATED_BIO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByBioIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where bio in DEFAULT_BIO or UPDATED_BIO
        defaultEtichettaFitoShouldBeFound("bio.in=" + DEFAULT_BIO + "," + UPDATED_BIO);

        // Get all the etichettaFitoList where bio equals to UPDATED_BIO
        defaultEtichettaFitoShouldNotBeFound("bio.in=" + UPDATED_BIO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByBioIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where bio is not null
        defaultEtichettaFitoShouldBeFound("bio.specified=true");

        // Get all the etichettaFitoList where bio is null
        defaultEtichettaFitoShouldNotBeFound("bio.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByPpoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ppo equals to DEFAULT_PPO
        defaultEtichettaFitoShouldBeFound("ppo.equals=" + DEFAULT_PPO);

        // Get all the etichettaFitoList where ppo equals to UPDATED_PPO
        defaultEtichettaFitoShouldNotBeFound("ppo.equals=" + UPDATED_PPO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByPpoIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ppo in DEFAULT_PPO or UPDATED_PPO
        defaultEtichettaFitoShouldBeFound("ppo.in=" + DEFAULT_PPO + "," + UPDATED_PPO);

        // Get all the etichettaFitoList where ppo equals to UPDATED_PPO
        defaultEtichettaFitoShouldNotBeFound("ppo.in=" + UPDATED_PPO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByPpoIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ppo is not null
        defaultEtichettaFitoShouldBeFound("ppo.specified=true");

        // Get all the etichettaFitoList where ppo is null
        defaultEtichettaFitoShouldNotBeFound("ppo.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocatoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocato equals to DEFAULT_REVOCATO
        defaultEtichettaFitoShouldBeFound("revocato.equals=" + DEFAULT_REVOCATO);

        // Get all the etichettaFitoList where revocato equals to UPDATED_REVOCATO
        defaultEtichettaFitoShouldNotBeFound("revocato.equals=" + UPDATED_REVOCATO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocatoIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocato in DEFAULT_REVOCATO or UPDATED_REVOCATO
        defaultEtichettaFitoShouldBeFound("revocato.in=" + DEFAULT_REVOCATO + "," + UPDATED_REVOCATO);

        // Get all the etichettaFitoList where revocato equals to UPDATED_REVOCATO
        defaultEtichettaFitoShouldNotBeFound("revocato.in=" + UPDATED_REVOCATO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocato is not null
        defaultEtichettaFitoShouldBeFound("revocato.specified=true");

        // Get all the etichettaFitoList where revocato is null
        defaultEtichettaFitoShouldNotBeFound("revocato.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocaAutorizzazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocaAutorizzazione equals to DEFAULT_REVOCA_AUTORIZZAZIONE
        defaultEtichettaFitoShouldBeFound("revocaAutorizzazione.equals=" + DEFAULT_REVOCA_AUTORIZZAZIONE);

        // Get all the etichettaFitoList where revocaAutorizzazione equals to UPDATED_REVOCA_AUTORIZZAZIONE
        defaultEtichettaFitoShouldNotBeFound("revocaAutorizzazione.equals=" + UPDATED_REVOCA_AUTORIZZAZIONE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocaAutorizzazioneIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocaAutorizzazione in DEFAULT_REVOCA_AUTORIZZAZIONE or UPDATED_REVOCA_AUTORIZZAZIONE
        defaultEtichettaFitoShouldBeFound("revocaAutorizzazione.in=" + DEFAULT_REVOCA_AUTORIZZAZIONE + "," + UPDATED_REVOCA_AUTORIZZAZIONE);

        // Get all the etichettaFitoList where revocaAutorizzazione equals to UPDATED_REVOCA_AUTORIZZAZIONE
        defaultEtichettaFitoShouldNotBeFound("revocaAutorizzazione.in=" + UPDATED_REVOCA_AUTORIZZAZIONE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByRevocaAutorizzazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where revocaAutorizzazione is not null
        defaultEtichettaFitoShouldBeFound("revocaAutorizzazione.specified=true");

        // Get all the etichettaFitoList where revocaAutorizzazione is null
        defaultEtichettaFitoShouldNotBeFound("revocaAutorizzazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaCommercioIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaCommercio equals to DEFAULT_SCADENZA_COMMERCIO
        defaultEtichettaFitoShouldBeFound("scadenzaCommercio.equals=" + DEFAULT_SCADENZA_COMMERCIO);

        // Get all the etichettaFitoList where scadenzaCommercio equals to UPDATED_SCADENZA_COMMERCIO
        defaultEtichettaFitoShouldNotBeFound("scadenzaCommercio.equals=" + UPDATED_SCADENZA_COMMERCIO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaCommercioIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaCommercio in DEFAULT_SCADENZA_COMMERCIO or UPDATED_SCADENZA_COMMERCIO
        defaultEtichettaFitoShouldBeFound("scadenzaCommercio.in=" + DEFAULT_SCADENZA_COMMERCIO + "," + UPDATED_SCADENZA_COMMERCIO);

        // Get all the etichettaFitoList where scadenzaCommercio equals to UPDATED_SCADENZA_COMMERCIO
        defaultEtichettaFitoShouldNotBeFound("scadenzaCommercio.in=" + UPDATED_SCADENZA_COMMERCIO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaCommercioIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaCommercio is not null
        defaultEtichettaFitoShouldBeFound("scadenzaCommercio.specified=true");

        // Get all the etichettaFitoList where scadenzaCommercio is null
        defaultEtichettaFitoShouldNotBeFound("scadenzaCommercio.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaUtilizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaUtilizzo equals to DEFAULT_SCADENZA_UTILIZZO
        defaultEtichettaFitoShouldBeFound("scadenzaUtilizzo.equals=" + DEFAULT_SCADENZA_UTILIZZO);

        // Get all the etichettaFitoList where scadenzaUtilizzo equals to UPDATED_SCADENZA_UTILIZZO
        defaultEtichettaFitoShouldNotBeFound("scadenzaUtilizzo.equals=" + UPDATED_SCADENZA_UTILIZZO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaUtilizzoIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaUtilizzo in DEFAULT_SCADENZA_UTILIZZO or UPDATED_SCADENZA_UTILIZZO
        defaultEtichettaFitoShouldBeFound("scadenzaUtilizzo.in=" + DEFAULT_SCADENZA_UTILIZZO + "," + UPDATED_SCADENZA_UTILIZZO);

        // Get all the etichettaFitoList where scadenzaUtilizzo equals to UPDATED_SCADENZA_UTILIZZO
        defaultEtichettaFitoShouldNotBeFound("scadenzaUtilizzo.in=" + UPDATED_SCADENZA_UTILIZZO);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByScadenzaUtilizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where scadenzaUtilizzo is not null
        defaultEtichettaFitoShouldBeFound("scadenzaUtilizzo.specified=true");

        // Get all the etichettaFitoList where scadenzaUtilizzo is null
        defaultEtichettaFitoShouldNotBeFound("scadenzaUtilizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByAvvClpIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where avvClp equals to DEFAULT_AVV_CLP
        defaultEtichettaFitoShouldBeFound("avvClp.equals=" + DEFAULT_AVV_CLP);

        // Get all the etichettaFitoList where avvClp equals to UPDATED_AVV_CLP
        defaultEtichettaFitoShouldNotBeFound("avvClp.equals=" + UPDATED_AVV_CLP);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByAvvClpIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where avvClp in DEFAULT_AVV_CLP or UPDATED_AVV_CLP
        defaultEtichettaFitoShouldBeFound("avvClp.in=" + DEFAULT_AVV_CLP + "," + UPDATED_AVV_CLP);

        // Get all the etichettaFitoList where avvClp equals to UPDATED_AVV_CLP
        defaultEtichettaFitoShouldNotBeFound("avvClp.in=" + UPDATED_AVV_CLP);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByAvvClpIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where avvClp is not null
        defaultEtichettaFitoShouldBeFound("avvClp.specified=true");

        // Get all the etichettaFitoList where avvClp is null
        defaultEtichettaFitoShouldNotBeFound("avvClp.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultEtichettaFitoShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the etichettaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaFitoShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultEtichettaFitoShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the etichettaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaFitoShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where tipoImport is not null
        defaultEtichettaFitoShouldBeFound("tipoImport.specified=true");

        // Get all the etichettaFitoList where tipoImport is null
        defaultEtichettaFitoShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where operatore equals to DEFAULT_OPERATORE
        defaultEtichettaFitoShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the etichettaFitoList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaFitoShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultEtichettaFitoShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the etichettaFitoList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaFitoShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where operatore is not null
        defaultEtichettaFitoShouldBeFound("operatore.specified=true");

        // Get all the etichettaFitoList where operatore is null
        defaultEtichettaFitoShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ts equals to DEFAULT_TS
        defaultEtichettaFitoShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the etichettaFitoList where ts equals to UPDATED_TS
        defaultEtichettaFitoShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ts in DEFAULT_TS or UPDATED_TS
        defaultEtichettaFitoShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the etichettaFitoList where ts equals to UPDATED_TS
        defaultEtichettaFitoShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where ts is not null
        defaultEtichettaFitoShouldBeFound("ts.specified=true");

        // Get all the etichettaFitoList where ts is null
        defaultEtichettaFitoShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali is not null
        defaultEtichettaFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the etichettaFitoList where dataInizVali is null
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultEtichettaFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali is not null
        defaultEtichettaFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the etichettaFitoList where dataFineVali is null
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultEtichettaFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultEtichettaFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator is not null
        defaultEtichettaFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the etichettaFitoList where userIdCreator is null
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultEtichettaFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultEtichettaFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod is not null
        defaultEtichettaFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the etichettaFitoList where userIdLastMod is null
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);

        // Get all the etichettaFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultEtichettaFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByIncompatibilitaetichettaIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        IncompatibilitaSA incompatibilitaetichetta = IncompatibilitaSAResourceIT.createEntity(em);
        em.persist(incompatibilitaetichetta);
        em.flush();
        etichettaFito.addIncompatibilitaetichetta(incompatibilitaetichetta);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long incompatibilitaetichettaId = incompatibilitaetichetta.getId();

        // Get all the etichettaFitoList where incompatibilitaetichetta equals to incompatibilitaetichettaId
        defaultEtichettaFitoShouldBeFound("incompatibilitaetichettaId.equals=" + incompatibilitaetichettaId);

        // Get all the etichettaFitoList where incompatibilitaetichetta equals to incompatibilitaetichettaId + 1
        defaultEtichettaFitoShouldNotBeFound("incompatibilitaetichettaId.equals=" + (incompatibilitaetichettaId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByProbfitotossicitaIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        ProbabileFitoTossicita probfitotossicita = ProbabileFitoTossicitaResourceIT.createEntity(em);
        em.persist(probfitotossicita);
        em.flush();
        etichettaFito.addProbfitotossicita(probfitotossicita);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long probfitotossicitaId = probfitotossicita.getId();

        // Get all the etichettaFitoList where probfitotossicita equals to probfitotossicitaId
        defaultEtichettaFitoShouldBeFound("probfitotossicitaId.equals=" + probfitotossicitaId);

        // Get all the etichettaFitoList where probfitotossicita equals to probfitotossicitaId + 1
        defaultEtichettaFitoShouldNotBeFound("probfitotossicitaId.equals=" + (probfitotossicitaId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByNoteagguntiveetiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        NoteAggiuntiveEtichetta noteagguntiveeti = NoteAggiuntiveEtichettaResourceIT.createEntity(em);
        em.persist(noteagguntiveeti);
        em.flush();
        etichettaFito.addNoteagguntiveeti(noteagguntiveeti);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long noteagguntiveetiId = noteagguntiveeti.getId();

        // Get all the etichettaFitoList where noteagguntiveeti equals to noteagguntiveetiId
        defaultEtichettaFitoShouldBeFound("noteagguntiveetiId.equals=" + noteagguntiveetiId);

        // Get all the etichettaFitoList where noteagguntiveeti equals to noteagguntiveetiId + 1
        defaultEtichettaFitoShouldNotBeFound("noteagguntiveetiId.equals=" + (noteagguntiveetiId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByEtisostanzeattiveIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        EtichettaSostanzeAttive etisostanzeattive = EtichettaSostanzeAttiveResourceIT.createEntity(em);
        em.persist(etisostanzeattive);
        em.flush();
        etichettaFito.addEtisostanzeattive(etisostanzeattive);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long etisostanzeattiveId = etisostanzeattive.getId();

        // Get all the etichettaFitoList where etisostanzeattive equals to etisostanzeattiveId
        defaultEtichettaFitoShouldBeFound("etisostanzeattiveId.equals=" + etisostanzeattiveId);

        // Get all the etichettaFitoList where etisostanzeattive equals to etisostanzeattiveId + 1
        defaultEtichettaFitoShouldNotBeFound("etisostanzeattiveId.equals=" + (etisostanzeattiveId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByEtichettafrasiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        EtichettaFrasi etichettafrasi = EtichettaFrasiResourceIT.createEntity(em);
        em.persist(etichettafrasi);
        em.flush();
        etichettaFito.addEtichettafrasi(etichettafrasi);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long etichettafrasiId = etichettafrasi.getId();

        // Get all the etichettaFitoList where etichettafrasi equals to etichettafrasiId
        defaultEtichettaFitoShouldBeFound("etichettafrasiId.equals=" + etichettafrasiId);

        // Get all the etichettaFitoList where etichettafrasi equals to etichettafrasiId + 1
        defaultEtichettaFitoShouldNotBeFound("etichettafrasiId.equals=" + (etichettafrasiId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByEtichettapittogrammiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        EtichettaPittogrammi etichettapittogrammi = EtichettaPittogrammiResourceIT.createEntity(em);
        em.persist(etichettapittogrammi);
        em.flush();
        etichettaFito.addEtichettapittogrammi(etichettapittogrammi);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long etichettapittogrammiId = etichettapittogrammi.getId();

        // Get all the etichettaFitoList where etichettapittogrammi equals to etichettapittogrammiId
        defaultEtichettaFitoShouldBeFound("etichettapittogrammiId.equals=" + etichettapittogrammiId);

        // Get all the etichettaFitoList where etichettapittogrammi equals to etichettapittogrammiId + 1
        defaultEtichettaFitoShouldNotBeFound("etichettapittogrammiId.equals=" + (etichettapittogrammiId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByEtichettatipofitofarmacoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        EtiTipologiaFito etichettatipofitofarmaco = EtiTipologiaFitoResourceIT.createEntity(em);
        em.persist(etichettatipofitofarmaco);
        em.flush();
        etichettaFito.addEtichettatipofitofarmaco(etichettatipofitofarmaco);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long etichettatipofitofarmacoId = etichettatipofitofarmaco.getId();

        // Get all the etichettaFitoList where etichettatipofitofarmaco equals to etichettatipofitofarmacoId
        defaultEtichettaFitoShouldBeFound("etichettatipofitofarmacoId.equals=" + etichettatipofitofarmacoId);

        // Get all the etichettaFitoList where etichettatipofitofarmaco equals to etichettatipofitofarmacoId + 1
        defaultEtichettaFitoShouldNotBeFound("etichettatipofitofarmacoId.equals=" + (etichettatipofitofarmacoId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByEtiFitoToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        TabellaRaccordo etiFitoToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(etiFitoToRaccordo);
        em.flush();
        etichettaFito.addEtiFitoToRaccordo(etiFitoToRaccordo);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long etiFitoToRaccordoId = etiFitoToRaccordo.getId();

        // Get all the etichettaFitoList where etiFitoToRaccordo equals to etiFitoToRaccordoId
        defaultEtichettaFitoShouldBeFound("etiFitoToRaccordoId.equals=" + etiFitoToRaccordoId);

        // Get all the etichettaFitoList where etiFitoToRaccordo equals to etiFitoToRaccordoId + 1
        defaultEtichettaFitoShouldNotBeFound("etiFitoToRaccordoId.equals=" + (etiFitoToRaccordoId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByTitolareRegistrazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        TitolareRegistrazione titolareRegistrazione = TitolareRegistrazioneResourceIT.createEntity(em);
        em.persist(titolareRegistrazione);
        em.flush();
        etichettaFito.setTitolareRegistrazione(titolareRegistrazione);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long titolareRegistrazioneId = titolareRegistrazione.getId();

        // Get all the etichettaFitoList where titolareRegistrazione equals to titolareRegistrazioneId
        defaultEtichettaFitoShouldBeFound("titolareRegistrazioneId.equals=" + titolareRegistrazioneId);

        // Get all the etichettaFitoList where titolareRegistrazione equals to titolareRegistrazioneId + 1
        defaultEtichettaFitoShouldNotBeFound("titolareRegistrazioneId.equals=" + (titolareRegistrazioneId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFitosByFormulazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Formulazione formulazione = FormulazioneResourceIT.createEntity(em);
        em.persist(formulazione);
        em.flush();
        etichettaFito.setFormulazione(formulazione);
        etichettaFitoRepository.saveAndFlush(etichettaFito);
        Long formulazioneId = formulazione.getId();

        // Get all the etichettaFitoList where formulazione equals to formulazioneId
        defaultEtichettaFitoShouldBeFound("formulazioneId.equals=" + formulazioneId);

        // Get all the etichettaFitoList where formulazione equals to formulazioneId + 1
        defaultEtichettaFitoShouldNotBeFound("formulazioneId.equals=" + (formulazioneId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtichettaFitoShouldBeFound(String filter) throws Exception {
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeCommerciale").value(hasItem(DEFAULT_NOME_COMMERCIALE)))
            .andExpect(jsonPath("$.[*].dataReg").value(hasItem(DEFAULT_DATA_REG.toString())))
            .andExpect(jsonPath("$.[*].registrazioneMinisteriale").value(hasItem(DEFAULT_REGISTRAZIONE_MINISTERIALE)))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA)))
            .andExpect(jsonPath("$.[*].bio").value(hasItem(DEFAULT_BIO)))
            .andExpect(jsonPath("$.[*].ppo").value(hasItem(DEFAULT_PPO)))
            .andExpect(jsonPath("$.[*].revocato").value(hasItem(DEFAULT_REVOCATO)))
            .andExpect(jsonPath("$.[*].revocaAutorizzazione").value(hasItem(DEFAULT_REVOCA_AUTORIZZAZIONE)))
            .andExpect(jsonPath("$.[*].scadenzaCommercio").value(hasItem(DEFAULT_SCADENZA_COMMERCIO)))
            .andExpect(jsonPath("$.[*].scadenzaUtilizzo").value(hasItem(DEFAULT_SCADENZA_UTILIZZO)))
            .andExpect(jsonPath("$.[*].avvClp").value(hasItem(DEFAULT_AVV_CLP)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtichettaFitoShouldNotBeFound(String filter) throws Exception {
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtichettaFito() throws Exception {
        // Get the etichettaFito
        restEtichettaFitoMockMvc.perform(get("/api/etichetta-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtichettaFito() throws Exception {
        // Initialize the database
        etichettaFitoService.save(etichettaFito);

        int databaseSizeBeforeUpdate = etichettaFitoRepository.findAll().size();

        // Update the etichettaFito
        EtichettaFito updatedEtichettaFito = etichettaFitoRepository.findById(etichettaFito.getId()).get();
        // Disconnect from session so that the updates on updatedEtichettaFito are not directly saved in db
        em.detach(updatedEtichettaFito);
        updatedEtichettaFito
            .nomeCommerciale(UPDATED_NOME_COMMERCIALE)
            .dataReg(UPDATED_DATA_REG)
            .registrazioneMinisteriale(UPDATED_REGISTRAZIONE_MINISTERIALE)
            .sigla(UPDATED_SIGLA)
            .bio(UPDATED_BIO)
            .ppo(UPDATED_PPO)
            .revocato(UPDATED_REVOCATO)
            .revocaAutorizzazione(UPDATED_REVOCA_AUTORIZZAZIONE)
            .scadenzaCommercio(UPDATED_SCADENZA_COMMERCIO)
            .scadenzaUtilizzo(UPDATED_SCADENZA_UTILIZZO)
            .avvClp(UPDATED_AVV_CLP)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restEtichettaFitoMockMvc.perform(put("/api/etichetta-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtichettaFito)))
            .andExpect(status().isOk());

        // Validate the EtichettaFito in the database
        List<EtichettaFito> etichettaFitoList = etichettaFitoRepository.findAll();
        assertThat(etichettaFitoList).hasSize(databaseSizeBeforeUpdate);
        EtichettaFito testEtichettaFito = etichettaFitoList.get(etichettaFitoList.size() - 1);
        assertThat(testEtichettaFito.getNomeCommerciale()).isEqualTo(UPDATED_NOME_COMMERCIALE);
        assertThat(testEtichettaFito.getDataReg()).isEqualTo(UPDATED_DATA_REG);
        assertThat(testEtichettaFito.getRegistrazioneMinisteriale()).isEqualTo(UPDATED_REGISTRAZIONE_MINISTERIALE);
        assertThat(testEtichettaFito.getSigla()).isEqualTo(UPDATED_SIGLA);
        assertThat(testEtichettaFito.getBio()).isEqualTo(UPDATED_BIO);
        assertThat(testEtichettaFito.getPpo()).isEqualTo(UPDATED_PPO);
        assertThat(testEtichettaFito.getRevocato()).isEqualTo(UPDATED_REVOCATO);
        assertThat(testEtichettaFito.getRevocaAutorizzazione()).isEqualTo(UPDATED_REVOCA_AUTORIZZAZIONE);
        assertThat(testEtichettaFito.getScadenzaCommercio()).isEqualTo(UPDATED_SCADENZA_COMMERCIO);
        assertThat(testEtichettaFito.getScadenzaUtilizzo()).isEqualTo(UPDATED_SCADENZA_UTILIZZO);
        assertThat(testEtichettaFito.getAvvClp()).isEqualTo(UPDATED_AVV_CLP);
        assertThat(testEtichettaFito.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testEtichettaFito.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testEtichettaFito.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testEtichettaFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testEtichettaFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testEtichettaFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testEtichettaFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testEtichettaFito.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtichettaFito() throws Exception {
        int databaseSizeBeforeUpdate = etichettaFitoRepository.findAll().size();

        // Create the EtichettaFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtichettaFitoMockMvc.perform(put("/api/etichetta-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFito)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaFito in the database
        List<EtichettaFito> etichettaFitoList = etichettaFitoRepository.findAll();
        assertThat(etichettaFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtichettaFito() throws Exception {
        // Initialize the database
        etichettaFitoService.save(etichettaFito);

        int databaseSizeBeforeDelete = etichettaFitoRepository.findAll().size();

        // Delete the etichettaFito
        restEtichettaFitoMockMvc.perform(delete("/api/etichetta-fitos/{id}", etichettaFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtichettaFito> etichettaFitoList = etichettaFitoRepository.findAll();
        assertThat(etichettaFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtichettaFito.class);
        EtichettaFito etichettaFito1 = new EtichettaFito();
        etichettaFito1.setId(1L);
        EtichettaFito etichettaFito2 = new EtichettaFito();
        etichettaFito2.setId(etichettaFito1.getId());
        assertThat(etichettaFito1).isEqualTo(etichettaFito2);
        etichettaFito2.setId(2L);
        assertThat(etichettaFito1).isNotEqualTo(etichettaFito2);
        etichettaFito1.setId(null);
        assertThat(etichettaFito1).isNotEqualTo(etichettaFito2);
    }
}
