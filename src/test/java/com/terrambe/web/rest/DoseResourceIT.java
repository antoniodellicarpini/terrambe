package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Dose;
import com.terrambe.domain.Dosaggio;
import com.terrambe.domain.UnitaMisura;
import com.terrambe.repository.DoseRepository;
import com.terrambe.service.DoseService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DoseCriteria;
import com.terrambe.service.DoseQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DoseResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DoseResourceIT {

    private static final Double DEFAULT_DOSE_MIN = 1D;
    private static final Double UPDATED_DOSE_MIN = 2D;
    private static final Double SMALLER_DOSE_MIN = 1D - 1D;

    private static final Double DEFAULT_DOSE_MAX = 1D;
    private static final Double UPDATED_DOSE_MAX = 2D;
    private static final Double SMALLER_DOSE_MAX = 1D - 1D;

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DoseRepository doseRepository;

    @Autowired
    private DoseService doseService;

    @Autowired
    private DoseQueryService doseQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDoseMockMvc;

    private Dose dose;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DoseResource doseResource = new DoseResource(doseService, doseQueryService);
        this.restDoseMockMvc = MockMvcBuilders.standaloneSetup(doseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dose createEntity(EntityManager em) {
        Dose dose = new Dose()
            .doseMin(DEFAULT_DOSE_MIN)
            .doseMax(DEFAULT_DOSE_MAX)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dose;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dose createUpdatedEntity(EntityManager em) {
        Dose dose = new Dose()
            .doseMin(UPDATED_DOSE_MIN)
            .doseMax(UPDATED_DOSE_MAX)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dose;
    }

    @BeforeEach
    public void initTest() {
        dose = createEntity(em);
    }

    @Test
    @Transactional
    public void createDose() throws Exception {
        int databaseSizeBeforeCreate = doseRepository.findAll().size();

        // Create the Dose
        restDoseMockMvc.perform(post("/api/doses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dose)))
            .andExpect(status().isCreated());

        // Validate the Dose in the database
        List<Dose> doseList = doseRepository.findAll();
        assertThat(doseList).hasSize(databaseSizeBeforeCreate + 1);
        Dose testDose = doseList.get(doseList.size() - 1);
        assertThat(testDose.getDoseMin()).isEqualTo(DEFAULT_DOSE_MIN);
        assertThat(testDose.getDoseMax()).isEqualTo(DEFAULT_DOSE_MAX);
        assertThat(testDose.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testDose.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testDose.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testDose.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDose.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDose.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDose.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDose.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDoseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doseRepository.findAll().size();

        // Create the Dose with an existing ID
        dose.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoseMockMvc.perform(post("/api/doses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dose)))
            .andExpect(status().isBadRequest());

        // Validate the Dose in the database
        List<Dose> doseList = doseRepository.findAll();
        assertThat(doseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDoses() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList
        restDoseMockMvc.perform(get("/api/doses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dose.getId().intValue())))
            .andExpect(jsonPath("$.[*].doseMin").value(hasItem(DEFAULT_DOSE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].doseMax").value(hasItem(DEFAULT_DOSE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDose() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get the dose
        restDoseMockMvc.perform(get("/api/doses/{id}", dose.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dose.getId().intValue()))
            .andExpect(jsonPath("$.doseMin").value(DEFAULT_DOSE_MIN.doubleValue()))
            .andExpect(jsonPath("$.doseMax").value(DEFAULT_DOSE_MAX.doubleValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin equals to DEFAULT_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.equals=" + DEFAULT_DOSE_MIN);

        // Get all the doseList where doseMin equals to UPDATED_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.equals=" + UPDATED_DOSE_MIN);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin in DEFAULT_DOSE_MIN or UPDATED_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.in=" + DEFAULT_DOSE_MIN + "," + UPDATED_DOSE_MIN);

        // Get all the doseList where doseMin equals to UPDATED_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.in=" + UPDATED_DOSE_MIN);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin is not null
        defaultDoseShouldBeFound("doseMin.specified=true");

        // Get all the doseList where doseMin is null
        defaultDoseShouldNotBeFound("doseMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin is greater than or equal to DEFAULT_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.greaterThanOrEqual=" + DEFAULT_DOSE_MIN);

        // Get all the doseList where doseMin is greater than or equal to UPDATED_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.greaterThanOrEqual=" + UPDATED_DOSE_MIN);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin is less than or equal to DEFAULT_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.lessThanOrEqual=" + DEFAULT_DOSE_MIN);

        // Get all the doseList where doseMin is less than or equal to SMALLER_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.lessThanOrEqual=" + SMALLER_DOSE_MIN);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin is less than DEFAULT_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.lessThan=" + DEFAULT_DOSE_MIN);

        // Get all the doseList where doseMin is less than UPDATED_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.lessThan=" + UPDATED_DOSE_MIN);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMin is greater than DEFAULT_DOSE_MIN
        defaultDoseShouldNotBeFound("doseMin.greaterThan=" + DEFAULT_DOSE_MIN);

        // Get all the doseList where doseMin is greater than SMALLER_DOSE_MIN
        defaultDoseShouldBeFound("doseMin.greaterThan=" + SMALLER_DOSE_MIN);
    }


    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax equals to DEFAULT_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.equals=" + DEFAULT_DOSE_MAX);

        // Get all the doseList where doseMax equals to UPDATED_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.equals=" + UPDATED_DOSE_MAX);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax in DEFAULT_DOSE_MAX or UPDATED_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.in=" + DEFAULT_DOSE_MAX + "," + UPDATED_DOSE_MAX);

        // Get all the doseList where doseMax equals to UPDATED_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.in=" + UPDATED_DOSE_MAX);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax is not null
        defaultDoseShouldBeFound("doseMax.specified=true");

        // Get all the doseList where doseMax is null
        defaultDoseShouldNotBeFound("doseMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax is greater than or equal to DEFAULT_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.greaterThanOrEqual=" + DEFAULT_DOSE_MAX);

        // Get all the doseList where doseMax is greater than or equal to UPDATED_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.greaterThanOrEqual=" + UPDATED_DOSE_MAX);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax is less than or equal to DEFAULT_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.lessThanOrEqual=" + DEFAULT_DOSE_MAX);

        // Get all the doseList where doseMax is less than or equal to SMALLER_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.lessThanOrEqual=" + SMALLER_DOSE_MAX);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax is less than DEFAULT_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.lessThan=" + DEFAULT_DOSE_MAX);

        // Get all the doseList where doseMax is less than UPDATED_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.lessThan=" + UPDATED_DOSE_MAX);
    }

    @Test
    @Transactional
    public void getAllDosesByDoseMaxIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where doseMax is greater than DEFAULT_DOSE_MAX
        defaultDoseShouldNotBeFound("doseMax.greaterThan=" + DEFAULT_DOSE_MAX);

        // Get all the doseList where doseMax is greater than SMALLER_DOSE_MAX
        defaultDoseShouldBeFound("doseMax.greaterThan=" + SMALLER_DOSE_MAX);
    }


    @Test
    @Transactional
    public void getAllDosesByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultDoseShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the doseList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultDoseShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllDosesByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultDoseShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the doseList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultDoseShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllDosesByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where tipoImport is not null
        defaultDoseShouldBeFound("tipoImport.specified=true");

        // Get all the doseList where tipoImport is null
        defaultDoseShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where operatore equals to DEFAULT_OPERATORE
        defaultDoseShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the doseList where operatore equals to UPDATED_OPERATORE
        defaultDoseShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllDosesByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultDoseShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the doseList where operatore equals to UPDATED_OPERATORE
        defaultDoseShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllDosesByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where operatore is not null
        defaultDoseShouldBeFound("operatore.specified=true");

        // Get all the doseList where operatore is null
        defaultDoseShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where ts equals to DEFAULT_TS
        defaultDoseShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the doseList where ts equals to UPDATED_TS
        defaultDoseShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllDosesByTsIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where ts in DEFAULT_TS or UPDATED_TS
        defaultDoseShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the doseList where ts equals to UPDATED_TS
        defaultDoseShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllDosesByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where ts is not null
        defaultDoseShouldBeFound("ts.specified=true");

        // Get all the doseList where ts is null
        defaultDoseShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali is not null
        defaultDoseShouldBeFound("dataInizVali.specified=true");

        // Get all the doseList where dataInizVali is null
        defaultDoseShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDoseShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the doseList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDoseShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali is not null
        defaultDoseShouldBeFound("dataFineVali.specified=true");

        // Get all the doseList where dataFineVali is null
        defaultDoseShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDoseShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the doseList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDoseShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator is not null
        defaultDoseShouldBeFound("userIdCreator.specified=true");

        // Get all the doseList where userIdCreator is null
        defaultDoseShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDoseShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the doseList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDoseShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod is not null
        defaultDoseShouldBeFound("userIdLastMod.specified=true");

        // Get all the doseList where userIdLastMod is null
        defaultDoseShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);

        // Get all the doseList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDoseShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the doseList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDoseShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllDosesByDoseToDosagIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);
        Dosaggio doseToDosag = DosaggioResourceIT.createEntity(em);
        em.persist(doseToDosag);
        em.flush();
        dose.setDoseToDosag(doseToDosag);
        doseRepository.saveAndFlush(dose);
        Long doseToDosagId = doseToDosag.getId();

        // Get all the doseList where doseToDosag equals to doseToDosagId
        defaultDoseShouldBeFound("doseToDosagId.equals=" + doseToDosagId);

        // Get all the doseList where doseToDosag equals to doseToDosagId + 1
        defaultDoseShouldNotBeFound("doseToDosagId.equals=" + (doseToDosagId + 1));
    }


    @Test
    @Transactional
    public void getAllDosesByDoseToUniMisuIsEqualToSomething() throws Exception {
        // Initialize the database
        doseRepository.saveAndFlush(dose);
        UnitaMisura doseToUniMisu = UnitaMisuraResourceIT.createEntity(em);
        em.persist(doseToUniMisu);
        em.flush();
        dose.setDoseToUniMisu(doseToUniMisu);
        doseRepository.saveAndFlush(dose);
        Long doseToUniMisuId = doseToUniMisu.getId();

        // Get all the doseList where doseToUniMisu equals to doseToUniMisuId
        defaultDoseShouldBeFound("doseToUniMisuId.equals=" + doseToUniMisuId);

        // Get all the doseList where doseToUniMisu equals to doseToUniMisuId + 1
        defaultDoseShouldNotBeFound("doseToUniMisuId.equals=" + (doseToUniMisuId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDoseShouldBeFound(String filter) throws Exception {
        restDoseMockMvc.perform(get("/api/doses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dose.getId().intValue())))
            .andExpect(jsonPath("$.[*].doseMin").value(hasItem(DEFAULT_DOSE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].doseMax").value(hasItem(DEFAULT_DOSE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDoseMockMvc.perform(get("/api/doses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDoseShouldNotBeFound(String filter) throws Exception {
        restDoseMockMvc.perform(get("/api/doses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDoseMockMvc.perform(get("/api/doses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDose() throws Exception {
        // Get the dose
        restDoseMockMvc.perform(get("/api/doses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDose() throws Exception {
        // Initialize the database
        doseService.save(dose);

        int databaseSizeBeforeUpdate = doseRepository.findAll().size();

        // Update the dose
        Dose updatedDose = doseRepository.findById(dose.getId()).get();
        // Disconnect from session so that the updates on updatedDose are not directly saved in db
        em.detach(updatedDose);
        updatedDose
            .doseMin(UPDATED_DOSE_MIN)
            .doseMax(UPDATED_DOSE_MAX)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDoseMockMvc.perform(put("/api/doses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDose)))
            .andExpect(status().isOk());

        // Validate the Dose in the database
        List<Dose> doseList = doseRepository.findAll();
        assertThat(doseList).hasSize(databaseSizeBeforeUpdate);
        Dose testDose = doseList.get(doseList.size() - 1);
        assertThat(testDose.getDoseMin()).isEqualTo(UPDATED_DOSE_MIN);
        assertThat(testDose.getDoseMax()).isEqualTo(UPDATED_DOSE_MAX);
        assertThat(testDose.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testDose.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testDose.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testDose.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDose.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDose.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDose.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDose.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDose() throws Exception {
        int databaseSizeBeforeUpdate = doseRepository.findAll().size();

        // Create the Dose

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDoseMockMvc.perform(put("/api/doses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dose)))
            .andExpect(status().isBadRequest());

        // Validate the Dose in the database
        List<Dose> doseList = doseRepository.findAll();
        assertThat(doseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDose() throws Exception {
        // Initialize the database
        doseService.save(dose);

        int databaseSizeBeforeDelete = doseRepository.findAll().size();

        // Delete the dose
        restDoseMockMvc.perform(delete("/api/doses/{id}", dose.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Dose> doseList = doseRepository.findAll();
        assertThat(doseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dose.class);
        Dose dose1 = new Dose();
        dose1.setId(1L);
        Dose dose2 = new Dose();
        dose2.setId(dose1.getId());
        assertThat(dose1).isEqualTo(dose2);
        dose2.setId(2L);
        assertThat(dose1).isNotEqualTo(dose2);
        dose1.setId(null);
        assertThat(dose1).isNotEqualTo(dose2);
    }
}
