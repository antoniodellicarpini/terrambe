package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.VincoliTerritoriali;
import com.terrambe.repository.VincoliTerritorialiRepository;
import com.terrambe.service.VincoliTerritorialiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.VincoliTerritorialiCriteria;
import com.terrambe.service.VincoliTerritorialiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VincoliTerritorialiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class VincoliTerritorialiResourceIT {

    private static final Boolean DEFAULT_FLAG_ATTIVO = false;
    private static final Boolean UPDATED_FLAG_ATTIVO = true;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private VincoliTerritorialiRepository vincoliTerritorialiRepository;

    @Autowired
    private VincoliTerritorialiService vincoliTerritorialiService;

    @Autowired
    private VincoliTerritorialiQueryService vincoliTerritorialiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restVincoliTerritorialiMockMvc;

    private VincoliTerritoriali vincoliTerritoriali;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VincoliTerritorialiResource vincoliTerritorialiResource = new VincoliTerritorialiResource(vincoliTerritorialiService, vincoliTerritorialiQueryService);
        this.restVincoliTerritorialiMockMvc = MockMvcBuilders.standaloneSetup(vincoliTerritorialiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VincoliTerritoriali createEntity(EntityManager em) {
        VincoliTerritoriali vincoliTerritoriali = new VincoliTerritoriali()
            .flagAttivo(DEFAULT_FLAG_ATTIVO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return vincoliTerritoriali;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VincoliTerritoriali createUpdatedEntity(EntityManager em) {
        VincoliTerritoriali vincoliTerritoriali = new VincoliTerritoriali()
            .flagAttivo(UPDATED_FLAG_ATTIVO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return vincoliTerritoriali;
    }

    @BeforeEach
    public void initTest() {
        vincoliTerritoriali = createEntity(em);
    }

    @Test
    @Transactional
    public void createVincoliTerritoriali() throws Exception {
        int databaseSizeBeforeCreate = vincoliTerritorialiRepository.findAll().size();

        // Create the VincoliTerritoriali
        restVincoliTerritorialiMockMvc.perform(post("/api/vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vincoliTerritoriali)))
            .andExpect(status().isCreated());

        // Validate the VincoliTerritoriali in the database
        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeCreate + 1);
        VincoliTerritoriali testVincoliTerritoriali = vincoliTerritorialiList.get(vincoliTerritorialiList.size() - 1);
        assertThat(testVincoliTerritoriali.isFlagAttivo()).isEqualTo(DEFAULT_FLAG_ATTIVO);
        assertThat(testVincoliTerritoriali.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testVincoliTerritoriali.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testVincoliTerritoriali.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testVincoliTerritoriali.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testVincoliTerritoriali.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createVincoliTerritorialiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vincoliTerritorialiRepository.findAll().size();

        // Create the VincoliTerritoriali with an existing ID
        vincoliTerritoriali.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVincoliTerritorialiMockMvc.perform(post("/api/vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        // Validate the VincoliTerritoriali in the database
        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFlagAttivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = vincoliTerritorialiRepository.findAll().size();
        // set the field null
        vincoliTerritoriali.setFlagAttivo(null);

        // Create the VincoliTerritoriali, which fails.

        restVincoliTerritorialiMockMvc.perform(post("/api/vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialis() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vincoliTerritoriali.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagAttivo").value(hasItem(DEFAULT_FLAG_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getVincoliTerritoriali() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get the vincoliTerritoriali
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis/{id}", vincoliTerritoriali.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vincoliTerritoriali.getId().intValue()))
            .andExpect(jsonPath("$.flagAttivo").value(DEFAULT_FLAG_ATTIVO.booleanValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByFlagAttivoIsEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where flagAttivo equals to DEFAULT_FLAG_ATTIVO
        defaultVincoliTerritorialiShouldBeFound("flagAttivo.equals=" + DEFAULT_FLAG_ATTIVO);

        // Get all the vincoliTerritorialiList where flagAttivo equals to UPDATED_FLAG_ATTIVO
        defaultVincoliTerritorialiShouldNotBeFound("flagAttivo.equals=" + UPDATED_FLAG_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByFlagAttivoIsInShouldWork() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where flagAttivo in DEFAULT_FLAG_ATTIVO or UPDATED_FLAG_ATTIVO
        defaultVincoliTerritorialiShouldBeFound("flagAttivo.in=" + DEFAULT_FLAG_ATTIVO + "," + UPDATED_FLAG_ATTIVO);

        // Get all the vincoliTerritorialiList where flagAttivo equals to UPDATED_FLAG_ATTIVO
        defaultVincoliTerritorialiShouldNotBeFound("flagAttivo.in=" + UPDATED_FLAG_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByFlagAttivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where flagAttivo is not null
        defaultVincoliTerritorialiShouldBeFound("flagAttivo.specified=true");

        // Get all the vincoliTerritorialiList where flagAttivo is null
        defaultVincoliTerritorialiShouldNotBeFound("flagAttivo.specified=false");
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali is not null
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.specified=true");

        // Get all the vincoliTerritorialiList where dataInizVali is null
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the vincoliTerritorialiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultVincoliTerritorialiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali is not null
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.specified=true");

        // Get all the vincoliTerritorialiList where dataFineVali is null
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the vincoliTerritorialiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultVincoliTerritorialiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator is not null
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.specified=true");

        // Get all the vincoliTerritorialiList where userIdCreator is null
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the vincoliTerritorialiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultVincoliTerritorialiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod is not null
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.specified=true");

        // Get all the vincoliTerritorialiList where userIdLastMod is null
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllVincoliTerritorialisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vincoliTerritorialiRepository.saveAndFlush(vincoliTerritoriali);

        // Get all the vincoliTerritorialiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the vincoliTerritorialiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultVincoliTerritorialiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultVincoliTerritorialiShouldBeFound(String filter) throws Exception {
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vincoliTerritoriali.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagAttivo").value(hasItem(DEFAULT_FLAG_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultVincoliTerritorialiShouldNotBeFound(String filter) throws Exception {
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingVincoliTerritoriali() throws Exception {
        // Get the vincoliTerritoriali
        restVincoliTerritorialiMockMvc.perform(get("/api/vincoli-territorialis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVincoliTerritoriali() throws Exception {
        // Initialize the database
        vincoliTerritorialiService.save(vincoliTerritoriali);

        int databaseSizeBeforeUpdate = vincoliTerritorialiRepository.findAll().size();

        // Update the vincoliTerritoriali
        VincoliTerritoriali updatedVincoliTerritoriali = vincoliTerritorialiRepository.findById(vincoliTerritoriali.getId()).get();
        // Disconnect from session so that the updates on updatedVincoliTerritoriali are not directly saved in db
        em.detach(updatedVincoliTerritoriali);
        updatedVincoliTerritoriali
            .flagAttivo(UPDATED_FLAG_ATTIVO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restVincoliTerritorialiMockMvc.perform(put("/api/vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVincoliTerritoriali)))
            .andExpect(status().isOk());

        // Validate the VincoliTerritoriali in the database
        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeUpdate);
        VincoliTerritoriali testVincoliTerritoriali = vincoliTerritorialiList.get(vincoliTerritorialiList.size() - 1);
        assertThat(testVincoliTerritoriali.isFlagAttivo()).isEqualTo(UPDATED_FLAG_ATTIVO);
        assertThat(testVincoliTerritoriali.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testVincoliTerritoriali.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testVincoliTerritoriali.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testVincoliTerritoriali.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testVincoliTerritoriali.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingVincoliTerritoriali() throws Exception {
        int databaseSizeBeforeUpdate = vincoliTerritorialiRepository.findAll().size();

        // Create the VincoliTerritoriali

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVincoliTerritorialiMockMvc.perform(put("/api/vincoli-territorialis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vincoliTerritoriali)))
            .andExpect(status().isBadRequest());

        // Validate the VincoliTerritoriali in the database
        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVincoliTerritoriali() throws Exception {
        // Initialize the database
        vincoliTerritorialiService.save(vincoliTerritoriali);

        int databaseSizeBeforeDelete = vincoliTerritorialiRepository.findAll().size();

        // Delete the vincoliTerritoriali
        restVincoliTerritorialiMockMvc.perform(delete("/api/vincoli-territorialis/{id}", vincoliTerritoriali.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VincoliTerritoriali> vincoliTerritorialiList = vincoliTerritorialiRepository.findAll();
        assertThat(vincoliTerritorialiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VincoliTerritoriali.class);
        VincoliTerritoriali vincoliTerritoriali1 = new VincoliTerritoriali();
        vincoliTerritoriali1.setId(1L);
        VincoliTerritoriali vincoliTerritoriali2 = new VincoliTerritoriali();
        vincoliTerritoriali2.setId(vincoliTerritoriali1.getId());
        assertThat(vincoliTerritoriali1).isEqualTo(vincoliTerritoriali2);
        vincoliTerritoriali2.setId(2L);
        assertThat(vincoliTerritoriali1).isNotEqualTo(vincoliTerritoriali2);
        vincoliTerritoriali1.setId(null);
        assertThat(vincoliTerritoriali1).isNotEqualTo(vincoliTerritoriali2);
    }
}
