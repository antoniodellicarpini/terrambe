package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.domain.IndUniAna;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.AtrAnagrafica;
import com.terrambe.repository.UniAnagraficaRepository;
import com.terrambe.service.UniAnagraficaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.UniAnagraficaCriteria;
import com.terrambe.service.UniAnagraficaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UniAnagraficaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class UniAnagraficaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private UniAnagraficaRepository uniAnagraficaRepository;

    @Autowired
    private UniAnagraficaService uniAnagraficaService;

    @Autowired
    private UniAnagraficaQueryService uniAnagraficaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUniAnagraficaMockMvc;

    private UniAnagrafica uniAnagrafica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UniAnagraficaResource uniAnagraficaResource = new UniAnagraficaResource(uniAnagraficaService, uniAnagraficaQueryService);
        this.restUniAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(uniAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UniAnagrafica createEntity(EntityManager em) {
        UniAnagrafica uniAnagrafica = new UniAnagrafica()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return uniAnagrafica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UniAnagrafica createUpdatedEntity(EntityManager em) {
        UniAnagrafica uniAnagrafica = new UniAnagrafica()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return uniAnagrafica;
    }

    @BeforeEach
    public void initTest() {
        uniAnagrafica = createEntity(em);
    }

    @Test
    @Transactional
    public void createUniAnagrafica() throws Exception {
        int databaseSizeBeforeCreate = uniAnagraficaRepository.findAll().size();

        // Create the UniAnagrafica
        restUniAnagraficaMockMvc.perform(post("/api/uni-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uniAnagrafica)))
            .andExpect(status().isCreated());

        // Validate the UniAnagrafica in the database
        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeCreate + 1);
        UniAnagrafica testUniAnagrafica = uniAnagraficaList.get(uniAnagraficaList.size() - 1);
        assertThat(testUniAnagrafica.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testUniAnagrafica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testUniAnagrafica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testUniAnagrafica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testUniAnagrafica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testUniAnagrafica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createUniAnagraficaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uniAnagraficaRepository.findAll().size();

        // Create the UniAnagrafica with an existing ID
        uniAnagrafica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUniAnagraficaMockMvc.perform(post("/api/uni-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uniAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the UniAnagrafica in the database
        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = uniAnagraficaRepository.findAll().size();
        // set the field null
        uniAnagrafica.setDescrizione(null);

        // Create the UniAnagrafica, which fails.

        restUniAnagraficaMockMvc.perform(post("/api/uni-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uniAnagrafica)))
            .andExpect(status().isBadRequest());

        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficas() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uniAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getUniAnagrafica() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get the uniAnagrafica
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas/{id}", uniAnagrafica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(uniAnagrafica.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultUniAnagraficaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the uniAnagraficaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultUniAnagraficaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultUniAnagraficaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the uniAnagraficaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultUniAnagraficaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where descrizione is not null
        defaultUniAnagraficaShouldBeFound("descrizione.specified=true");

        // Get all the uniAnagraficaList where descrizione is null
        defaultUniAnagraficaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali is not null
        defaultUniAnagraficaShouldBeFound("dataInizVali.specified=true");

        // Get all the uniAnagraficaList where dataInizVali is null
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultUniAnagraficaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the uniAnagraficaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultUniAnagraficaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali is not null
        defaultUniAnagraficaShouldBeFound("dataFineVali.specified=true");

        // Get all the uniAnagraficaList where dataFineVali is null
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultUniAnagraficaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the uniAnagraficaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultUniAnagraficaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator is not null
        defaultUniAnagraficaShouldBeFound("userIdCreator.specified=true");

        // Get all the uniAnagraficaList where userIdCreator is null
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultUniAnagraficaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the uniAnagraficaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultUniAnagraficaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod is not null
        defaultUniAnagraficaShouldBeFound("userIdLastMod.specified=true");

        // Get all the uniAnagraficaList where userIdLastMod is null
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUniAnagraficasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);

        // Get all the uniAnagraficaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the uniAnagraficaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultUniAnagraficaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByUniAnaToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        IndUniAna uniAnaToInd = IndUniAnaResourceIT.createEntity(em);
        em.persist(uniAnaToInd);
        em.flush();
        uniAnagrafica.addUniAnaToInd(uniAnaToInd);
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        Long uniAnaToIndId = uniAnaToInd.getId();

        // Get all the uniAnagraficaList where uniAnaToInd equals to uniAnaToIndId
        defaultUniAnagraficaShouldBeFound("uniAnaToIndId.equals=" + uniAnaToIndId);

        // Get all the uniAnagraficaList where uniAnaToInd equals to uniAnaToIndId + 1
        defaultUniAnagraficaShouldNotBeFound("uniAnaToIndId.equals=" + (uniAnaToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByUnianaazianaIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        AziAnagrafica unianaaziana = AziAnagraficaResourceIT.createEntity(em);
        em.persist(unianaaziana);
        em.flush();
        uniAnagrafica.setUnianaaziana(unianaaziana);
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        Long unianaazianaId = unianaaziana.getId();

        // Get all the uniAnagraficaList where unianaaziana equals to unianaazianaId
        defaultUniAnagraficaShouldBeFound("unianaazianaId.equals=" + unianaazianaId);

        // Get all the uniAnagraficaList where unianaaziana equals to unianaazianaId + 1
        defaultUniAnagraficaShouldNotBeFound("unianaazianaId.equals=" + (unianaazianaId + 1));
    }


    @Test
    @Transactional
    public void getAllUniAnagraficasByUniAnaToAtrAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        AtrAnagrafica uniAnaToAtrAna = AtrAnagraficaResourceIT.createEntity(em);
        em.persist(uniAnaToAtrAna);
        em.flush();
        uniAnagrafica.addUniAnaToAtrAna(uniAnaToAtrAna);
        uniAnagraficaRepository.saveAndFlush(uniAnagrafica);
        Long uniAnaToAtrAnaId = uniAnaToAtrAna.getId();

        // Get all the uniAnagraficaList where uniAnaToAtrAna equals to uniAnaToAtrAnaId
        defaultUniAnagraficaShouldBeFound("uniAnaToAtrAnaId.equals=" + uniAnaToAtrAnaId);

        // Get all the uniAnagraficaList where uniAnaToAtrAna equals to uniAnaToAtrAnaId + 1
        defaultUniAnagraficaShouldNotBeFound("uniAnaToAtrAnaId.equals=" + (uniAnaToAtrAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUniAnagraficaShouldBeFound(String filter) throws Exception {
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uniAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUniAnagraficaShouldNotBeFound(String filter) throws Exception {
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUniAnagrafica() throws Exception {
        // Get the uniAnagrafica
        restUniAnagraficaMockMvc.perform(get("/api/uni-anagraficas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUniAnagrafica() throws Exception {
        // Initialize the database
        uniAnagraficaService.save(uniAnagrafica);

        int databaseSizeBeforeUpdate = uniAnagraficaRepository.findAll().size();

        // Update the uniAnagrafica
        UniAnagrafica updatedUniAnagrafica = uniAnagraficaRepository.findById(uniAnagrafica.getId()).get();
        // Disconnect from session so that the updates on updatedUniAnagrafica are not directly saved in db
        em.detach(updatedUniAnagrafica);
        updatedUniAnagrafica
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restUniAnagraficaMockMvc.perform(put("/api/uni-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUniAnagrafica)))
            .andExpect(status().isOk());

        // Validate the UniAnagrafica in the database
        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeUpdate);
        UniAnagrafica testUniAnagrafica = uniAnagraficaList.get(uniAnagraficaList.size() - 1);
        assertThat(testUniAnagrafica.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testUniAnagrafica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testUniAnagrafica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testUniAnagrafica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testUniAnagrafica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testUniAnagrafica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingUniAnagrafica() throws Exception {
        int databaseSizeBeforeUpdate = uniAnagraficaRepository.findAll().size();

        // Create the UniAnagrafica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUniAnagraficaMockMvc.perform(put("/api/uni-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uniAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the UniAnagrafica in the database
        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUniAnagrafica() throws Exception {
        // Initialize the database
        uniAnagraficaService.save(uniAnagrafica);

        int databaseSizeBeforeDelete = uniAnagraficaRepository.findAll().size();

        // Delete the uniAnagrafica
        restUniAnagraficaMockMvc.perform(delete("/api/uni-anagraficas/{id}", uniAnagrafica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UniAnagrafica> uniAnagraficaList = uniAnagraficaRepository.findAll();
        assertThat(uniAnagraficaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UniAnagrafica.class);
        UniAnagrafica uniAnagrafica1 = new UniAnagrafica();
        uniAnagrafica1.setId(1L);
        UniAnagrafica uniAnagrafica2 = new UniAnagrafica();
        uniAnagrafica2.setId(uniAnagrafica1.getId());
        assertThat(uniAnagrafica1).isEqualTo(uniAnagrafica2);
        uniAnagrafica2.setId(2L);
        assertThat(uniAnagrafica1).isNotEqualTo(uniAnagrafica2);
        uniAnagrafica1.setId(null);
        assertThat(uniAnagrafica1).isNotEqualTo(uniAnagrafica2);
    }
}
