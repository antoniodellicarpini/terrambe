package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.domain.IndNascRapCli;
import com.terrambe.domain.Cliente;
import com.terrambe.domain.IndAziRapCli;
import com.terrambe.domain.RecAziRapCli;
import com.terrambe.repository.AziRappresentanteCliRepository;
import com.terrambe.service.AziRappresentanteCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziRappresentanteCliCriteria;
import com.terrambe.service.AziRappresentanteCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziRappresentanteCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziRappresentanteCliResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME = "BBBBBBBBBB";

    private static final String DEFAULT_SESSO = "AAAAAAAAAA";
    private static final String UPDATED_SESSO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_NASCITA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_NASCITA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_NASCITA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CODICE_FISCALE = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODICE_FISCALE = "BBBBBBBBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziRappresentanteCliRepository aziRappresentanteCliRepository;

    @Autowired
    private AziRappresentanteCliService aziRappresentanteCliService;

    @Autowired
    private AziRappresentanteCliQueryService aziRappresentanteCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziRappresentanteCliMockMvc;

    private AziRappresentanteCli aziRappresentanteCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziRappresentanteCliResource aziRappresentanteCliResource = new AziRappresentanteCliResource(aziRappresentanteCliService, aziRappresentanteCliQueryService);
        this.restAziRappresentanteCliMockMvc = MockMvcBuilders.standaloneSetup(aziRappresentanteCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziRappresentanteCli createEntity(EntityManager em) {
        AziRappresentanteCli aziRappresentanteCli = new AziRappresentanteCli()
            .nome(DEFAULT_NOME)
            .cognome(DEFAULT_COGNOME)
            .sesso(DEFAULT_SESSO)
            .dataNascita(DEFAULT_DATA_NASCITA)
            .codiceFiscale(DEFAULT_CODICE_FISCALE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziRappresentanteCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziRappresentanteCli createUpdatedEntity(EntityManager em) {
        AziRappresentanteCli aziRappresentanteCli = new AziRappresentanteCli()
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziRappresentanteCli;
    }

    @BeforeEach
    public void initTest() {
        aziRappresentanteCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziRappresentanteCli() throws Exception {
        int databaseSizeBeforeCreate = aziRappresentanteCliRepository.findAll().size();

        // Create the AziRappresentanteCli
        restAziRappresentanteCliMockMvc.perform(post("/api/azi-rappresentante-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentanteCli)))
            .andExpect(status().isCreated());

        // Validate the AziRappresentanteCli in the database
        List<AziRappresentanteCli> aziRappresentanteCliList = aziRappresentanteCliRepository.findAll();
        assertThat(aziRappresentanteCliList).hasSize(databaseSizeBeforeCreate + 1);
        AziRappresentanteCli testAziRappresentanteCli = aziRappresentanteCliList.get(aziRappresentanteCliList.size() - 1);
        assertThat(testAziRappresentanteCli.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testAziRappresentanteCli.getCognome()).isEqualTo(DEFAULT_COGNOME);
        assertThat(testAziRappresentanteCli.getSesso()).isEqualTo(DEFAULT_SESSO);
        assertThat(testAziRappresentanteCli.getDataNascita()).isEqualTo(DEFAULT_DATA_NASCITA);
        assertThat(testAziRappresentanteCli.getCodiceFiscale()).isEqualTo(DEFAULT_CODICE_FISCALE);
        assertThat(testAziRappresentanteCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziRappresentanteCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziRappresentanteCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziRappresentanteCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziRappresentanteCli.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziRappresentanteCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziRappresentanteCliRepository.findAll().size();

        // Create the AziRappresentanteCli with an existing ID
        aziRappresentanteCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziRappresentanteCliMockMvc.perform(post("/api/azi-rappresentante-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentanteCli)))
            .andExpect(status().isBadRequest());

        // Validate the AziRappresentanteCli in the database
        List<AziRappresentanteCli> aziRappresentanteCliList = aziRappresentanteCliRepository.findAll();
        assertThat(aziRappresentanteCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClis() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziRappresentanteCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME.toString())))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO.toString())))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziRappresentanteCli() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get the aziRappresentanteCli
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis/{id}", aziRappresentanteCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziRappresentanteCli.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME.toString()))
            .andExpect(jsonPath("$.sesso").value(DEFAULT_SESSO.toString()))
            .andExpect(jsonPath("$.dataNascita").value(DEFAULT_DATA_NASCITA.toString()))
            .andExpect(jsonPath("$.codiceFiscale").value(DEFAULT_CODICE_FISCALE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where nome equals to DEFAULT_NOME
        defaultAziRappresentanteCliShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the aziRappresentanteCliList where nome equals to UPDATED_NOME
        defaultAziRappresentanteCliShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultAziRappresentanteCliShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the aziRappresentanteCliList where nome equals to UPDATED_NOME
        defaultAziRappresentanteCliShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where nome is not null
        defaultAziRappresentanteCliShouldBeFound("nome.specified=true");

        // Get all the aziRappresentanteCliList where nome is null
        defaultAziRappresentanteCliShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCognomeIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where cognome equals to DEFAULT_COGNOME
        defaultAziRappresentanteCliShouldBeFound("cognome.equals=" + DEFAULT_COGNOME);

        // Get all the aziRappresentanteCliList where cognome equals to UPDATED_COGNOME
        defaultAziRappresentanteCliShouldNotBeFound("cognome.equals=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCognomeIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where cognome in DEFAULT_COGNOME or UPDATED_COGNOME
        defaultAziRappresentanteCliShouldBeFound("cognome.in=" + DEFAULT_COGNOME + "," + UPDATED_COGNOME);

        // Get all the aziRappresentanteCliList where cognome equals to UPDATED_COGNOME
        defaultAziRappresentanteCliShouldNotBeFound("cognome.in=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCognomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where cognome is not null
        defaultAziRappresentanteCliShouldBeFound("cognome.specified=true");

        // Get all the aziRappresentanteCliList where cognome is null
        defaultAziRappresentanteCliShouldNotBeFound("cognome.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisBySessoIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where sesso equals to DEFAULT_SESSO
        defaultAziRappresentanteCliShouldBeFound("sesso.equals=" + DEFAULT_SESSO);

        // Get all the aziRappresentanteCliList where sesso equals to UPDATED_SESSO
        defaultAziRappresentanteCliShouldNotBeFound("sesso.equals=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisBySessoIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where sesso in DEFAULT_SESSO or UPDATED_SESSO
        defaultAziRappresentanteCliShouldBeFound("sesso.in=" + DEFAULT_SESSO + "," + UPDATED_SESSO);

        // Get all the aziRappresentanteCliList where sesso equals to UPDATED_SESSO
        defaultAziRappresentanteCliShouldNotBeFound("sesso.in=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisBySessoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where sesso is not null
        defaultAziRappresentanteCliShouldBeFound("sesso.specified=true");

        // Get all the aziRappresentanteCliList where sesso is null
        defaultAziRappresentanteCliShouldNotBeFound("sesso.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita equals to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.equals=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.equals=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita in DEFAULT_DATA_NASCITA or UPDATED_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.in=" + DEFAULT_DATA_NASCITA + "," + UPDATED_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.in=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita is not null
        defaultAziRappresentanteCliShouldBeFound("dataNascita.specified=true");

        // Get all the aziRappresentanteCliList where dataNascita is null
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita is greater than or equal to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.greaterThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita is greater than or equal to UPDATED_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.greaterThanOrEqual=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita is less than or equal to DEFAULT_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.lessThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita is less than or equal to SMALLER_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.lessThanOrEqual=" + SMALLER_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita is less than DEFAULT_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.lessThan=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita is less than UPDATED_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.lessThan=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataNascitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataNascita is greater than DEFAULT_DATA_NASCITA
        defaultAziRappresentanteCliShouldNotBeFound("dataNascita.greaterThan=" + DEFAULT_DATA_NASCITA);

        // Get all the aziRappresentanteCliList where dataNascita is greater than SMALLER_DATA_NASCITA
        defaultAziRappresentanteCliShouldBeFound("dataNascita.greaterThan=" + SMALLER_DATA_NASCITA);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCodiceFiscaleIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where codiceFiscale equals to DEFAULT_CODICE_FISCALE
        defaultAziRappresentanteCliShouldBeFound("codiceFiscale.equals=" + DEFAULT_CODICE_FISCALE);

        // Get all the aziRappresentanteCliList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultAziRappresentanteCliShouldNotBeFound("codiceFiscale.equals=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCodiceFiscaleIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where codiceFiscale in DEFAULT_CODICE_FISCALE or UPDATED_CODICE_FISCALE
        defaultAziRappresentanteCliShouldBeFound("codiceFiscale.in=" + DEFAULT_CODICE_FISCALE + "," + UPDATED_CODICE_FISCALE);

        // Get all the aziRappresentanteCliList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultAziRappresentanteCliShouldNotBeFound("codiceFiscale.in=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCodiceFiscaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where codiceFiscale is not null
        defaultAziRappresentanteCliShouldBeFound("codiceFiscale.specified=true");

        // Get all the aziRappresentanteCliList where codiceFiscale is null
        defaultAziRappresentanteCliShouldNotBeFound("codiceFiscale.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali is not null
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.specified=true");

        // Get all the aziRappresentanteCliList where dataInizVali is null
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziRappresentanteCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziRappresentanteCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali is not null
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.specified=true");

        // Get all the aziRappresentanteCliList where dataFineVali is null
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziRappresentanteCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziRappresentanteCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator is not null
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.specified=true");

        // Get all the aziRappresentanteCliList where userIdCreator is null
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziRappresentanteCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziRappresentanteCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod is not null
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziRappresentanteCliList where userIdLastMod is null
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziRappresentanteClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);

        // Get all the aziRappresentanteCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziRappresentanteCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziRappresentanteCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCliRapToIndNascIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        IndNascRapCli cliRapToIndNasc = IndNascRapCliResourceIT.createEntity(em);
        em.persist(cliRapToIndNasc);
        em.flush();
        aziRappresentanteCli.setCliRapToIndNasc(cliRapToIndNasc);
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        Long cliRapToIndNascId = cliRapToIndNasc.getId();

        // Get all the aziRappresentanteCliList where cliRapToIndNasc equals to cliRapToIndNascId
        defaultAziRappresentanteCliShouldBeFound("cliRapToIndNascId.equals=" + cliRapToIndNascId);

        // Get all the aziRappresentanteCliList where cliRapToIndNasc equals to cliRapToIndNascId + 1
        defaultAziRappresentanteCliShouldNotBeFound("cliRapToIndNascId.equals=" + (cliRapToIndNascId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCliRapToCliAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        Cliente cliRapToCliAna = ClienteResourceIT.createEntity(em);
        em.persist(cliRapToCliAna);
        em.flush();
        aziRappresentanteCli.addCliRapToCliAna(cliRapToCliAna);
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        Long cliRapToCliAnaId = cliRapToCliAna.getId();

        // Get all the aziRappresentanteCliList where cliRapToCliAna equals to cliRapToCliAnaId
        defaultAziRappresentanteCliShouldBeFound("cliRapToCliAnaId.equals=" + cliRapToCliAnaId);

        // Get all the aziRappresentanteCliList where cliRapToCliAna equals to cliRapToCliAnaId + 1
        defaultAziRappresentanteCliShouldNotBeFound("cliRapToCliAnaId.equals=" + (cliRapToCliAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCliRapToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        IndAziRapCli cliRapToInd = IndAziRapCliResourceIT.createEntity(em);
        em.persist(cliRapToInd);
        em.flush();
        aziRappresentanteCli.addCliRapToInd(cliRapToInd);
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        Long cliRapToIndId = cliRapToInd.getId();

        // Get all the aziRappresentanteCliList where cliRapToInd equals to cliRapToIndId
        defaultAziRappresentanteCliShouldBeFound("cliRapToIndId.equals=" + cliRapToIndId);

        // Get all the aziRappresentanteCliList where cliRapToInd equals to cliRapToIndId + 1
        defaultAziRappresentanteCliShouldNotBeFound("cliRapToIndId.equals=" + (cliRapToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllAziRappresentanteClisByCliRapToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        RecAziRapCli cliRapToRec = RecAziRapCliResourceIT.createEntity(em);
        em.persist(cliRapToRec);
        em.flush();
        aziRappresentanteCli.addCliRapToRec(cliRapToRec);
        aziRappresentanteCliRepository.saveAndFlush(aziRappresentanteCli);
        Long cliRapToRecId = cliRapToRec.getId();

        // Get all the aziRappresentanteCliList where cliRapToRec equals to cliRapToRecId
        defaultAziRappresentanteCliShouldBeFound("cliRapToRecId.equals=" + cliRapToRecId);

        // Get all the aziRappresentanteCliList where cliRapToRec equals to cliRapToRecId + 1
        defaultAziRappresentanteCliShouldNotBeFound("cliRapToRecId.equals=" + (cliRapToRecId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziRappresentanteCliShouldBeFound(String filter) throws Exception {
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziRappresentanteCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME)))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO)))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziRappresentanteCliShouldNotBeFound(String filter) throws Exception {
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziRappresentanteCli() throws Exception {
        // Get the aziRappresentanteCli
        restAziRappresentanteCliMockMvc.perform(get("/api/azi-rappresentante-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziRappresentanteCli() throws Exception {
        // Initialize the database
        aziRappresentanteCliService.save(aziRappresentanteCli);

        int databaseSizeBeforeUpdate = aziRappresentanteCliRepository.findAll().size();

        // Update the aziRappresentanteCli
        AziRappresentanteCli updatedAziRappresentanteCli = aziRappresentanteCliRepository.findById(aziRappresentanteCli.getId()).get();
        // Disconnect from session so that the updates on updatedAziRappresentanteCli are not directly saved in db
        em.detach(updatedAziRappresentanteCli);
        updatedAziRappresentanteCli
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziRappresentanteCliMockMvc.perform(put("/api/azi-rappresentante-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziRappresentanteCli)))
            .andExpect(status().isOk());

        // Validate the AziRappresentanteCli in the database
        List<AziRappresentanteCli> aziRappresentanteCliList = aziRappresentanteCliRepository.findAll();
        assertThat(aziRappresentanteCliList).hasSize(databaseSizeBeforeUpdate);
        AziRappresentanteCli testAziRappresentanteCli = aziRappresentanteCliList.get(aziRappresentanteCliList.size() - 1);
        assertThat(testAziRappresentanteCli.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testAziRappresentanteCli.getCognome()).isEqualTo(UPDATED_COGNOME);
        assertThat(testAziRappresentanteCli.getSesso()).isEqualTo(UPDATED_SESSO);
        assertThat(testAziRappresentanteCli.getDataNascita()).isEqualTo(UPDATED_DATA_NASCITA);
        assertThat(testAziRappresentanteCli.getCodiceFiscale()).isEqualTo(UPDATED_CODICE_FISCALE);
        assertThat(testAziRappresentanteCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziRappresentanteCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziRappresentanteCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziRappresentanteCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziRappresentanteCli.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziRappresentanteCli() throws Exception {
        int databaseSizeBeforeUpdate = aziRappresentanteCliRepository.findAll().size();

        // Create the AziRappresentanteCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziRappresentanteCliMockMvc.perform(put("/api/azi-rappresentante-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziRappresentanteCli)))
            .andExpect(status().isBadRequest());

        // Validate the AziRappresentanteCli in the database
        List<AziRappresentanteCli> aziRappresentanteCliList = aziRappresentanteCliRepository.findAll();
        assertThat(aziRappresentanteCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziRappresentanteCli() throws Exception {
        // Initialize the database
        aziRappresentanteCliService.save(aziRappresentanteCli);

        int databaseSizeBeforeDelete = aziRappresentanteCliRepository.findAll().size();

        // Delete the aziRappresentanteCli
        restAziRappresentanteCliMockMvc.perform(delete("/api/azi-rappresentante-clis/{id}", aziRappresentanteCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziRappresentanteCli> aziRappresentanteCliList = aziRappresentanteCliRepository.findAll();
        assertThat(aziRappresentanteCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziRappresentanteCli.class);
        AziRappresentanteCli aziRappresentanteCli1 = new AziRappresentanteCli();
        aziRappresentanteCli1.setId(1L);
        AziRappresentanteCli aziRappresentanteCli2 = new AziRappresentanteCli();
        aziRappresentanteCli2.setId(aziRappresentanteCli1.getId());
        assertThat(aziRappresentanteCli1).isEqualTo(aziRappresentanteCli2);
        aziRappresentanteCli2.setId(2L);
        assertThat(aziRappresentanteCli1).isNotEqualTo(aziRappresentanteCli2);
        aziRappresentanteCli1.setId(null);
        assertThat(aziRappresentanteCli1).isNotEqualTo(aziRappresentanteCli2);
    }
}
