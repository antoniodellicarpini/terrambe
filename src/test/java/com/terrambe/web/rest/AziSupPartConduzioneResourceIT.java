package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziSupPartConduzione;
import com.terrambe.repository.AziSupPartConduzioneRepository;
import com.terrambe.service.AziSupPartConduzioneService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziSupPartConduzioneCriteria;
import com.terrambe.service.AziSupPartConduzioneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziSupPartConduzioneResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziSupPartConduzioneResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziSupPartConduzioneRepository aziSupPartConduzioneRepository;

    @Autowired
    private AziSupPartConduzioneService aziSupPartConduzioneService;

    @Autowired
    private AziSupPartConduzioneQueryService aziSupPartConduzioneQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziSupPartConduzioneMockMvc;

    private AziSupPartConduzione aziSupPartConduzione;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziSupPartConduzioneResource aziSupPartConduzioneResource = new AziSupPartConduzioneResource(aziSupPartConduzioneService, aziSupPartConduzioneQueryService);
        this.restAziSupPartConduzioneMockMvc = MockMvcBuilders.standaloneSetup(aziSupPartConduzioneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSupPartConduzione createEntity(EntityManager em) {
        AziSupPartConduzione aziSupPartConduzione = new AziSupPartConduzione()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizio(DEFAULT_DATA_INIZIO)
            .dataFine(DEFAULT_DATA_FINE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziSupPartConduzione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSupPartConduzione createUpdatedEntity(EntityManager em) {
        AziSupPartConduzione aziSupPartConduzione = new AziSupPartConduzione()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziSupPartConduzione;
    }

    @BeforeEach
    public void initTest() {
        aziSupPartConduzione = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziSupPartConduzione() throws Exception {
        int databaseSizeBeforeCreate = aziSupPartConduzioneRepository.findAll().size();

        // Create the AziSupPartConduzione
        restAziSupPartConduzioneMockMvc.perform(post("/api/azi-sup-part-conduziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartConduzione)))
            .andExpect(status().isCreated());

        // Validate the AziSupPartConduzione in the database
        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeCreate + 1);
        AziSupPartConduzione testAziSupPartConduzione = aziSupPartConduzioneList.get(aziSupPartConduzioneList.size() - 1);
        assertThat(testAziSupPartConduzione.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testAziSupPartConduzione.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
        assertThat(testAziSupPartConduzione.getDataFine()).isEqualTo(DEFAULT_DATA_FINE);
        assertThat(testAziSupPartConduzione.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziSupPartConduzione.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziSupPartConduzione.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziSupPartConduzione.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziSupPartConduzione.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziSupPartConduzioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziSupPartConduzioneRepository.findAll().size();

        // Create the AziSupPartConduzione with an existing ID
        aziSupPartConduzione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziSupPartConduzioneMockMvc.perform(post("/api/azi-sup-part-conduziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartConduzione)))
            .andExpect(status().isBadRequest());

        // Validate the AziSupPartConduzione in the database
        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSupPartConduzioneRepository.findAll().size();
        // set the field null
        aziSupPartConduzione.setDescrizione(null);

        // Create the AziSupPartConduzione, which fails.

        restAziSupPartConduzioneMockMvc.perform(post("/api/azi-sup-part-conduziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartConduzione)))
            .andExpect(status().isBadRequest());

        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduziones() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSupPartConduzione.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziSupPartConduzione() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get the aziSupPartConduzione
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones/{id}", aziSupPartConduzione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziSupPartConduzione.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultAziSupPartConduzioneShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the aziSupPartConduzioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAziSupPartConduzioneShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultAziSupPartConduzioneShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the aziSupPartConduzioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultAziSupPartConduzioneShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where descrizione is not null
        defaultAziSupPartConduzioneShouldBeFound("descrizione.specified=true");

        // Get all the aziSupPartConduzioneList where descrizione is null
        defaultAziSupPartConduzioneShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio equals to DEFAULT_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.equals=" + DEFAULT_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.equals=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio in DEFAULT_DATA_INIZIO or UPDATED_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.in=" + DEFAULT_DATA_INIZIO + "," + UPDATED_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.in=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio is not null
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.specified=true");

        // Get all the aziSupPartConduzioneList where dataInizio is null
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio is greater than or equal to DEFAULT_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio is greater than or equal to UPDATED_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.greaterThanOrEqual=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio is less than or equal to DEFAULT_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.lessThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio is less than or equal to SMALLER_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.lessThanOrEqual=" + SMALLER_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio is less than DEFAULT_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.lessThan=" + DEFAULT_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio is less than UPDATED_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.lessThan=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizio is greater than DEFAULT_DATA_INIZIO
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizio.greaterThan=" + DEFAULT_DATA_INIZIO);

        // Get all the aziSupPartConduzioneList where dataInizio is greater than SMALLER_DATA_INIZIO
        defaultAziSupPartConduzioneShouldBeFound("dataInizio.greaterThan=" + SMALLER_DATA_INIZIO);
    }


    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine equals to DEFAULT_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.equals=" + DEFAULT_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine equals to UPDATED_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.equals=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine in DEFAULT_DATA_FINE or UPDATED_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.in=" + DEFAULT_DATA_FINE + "," + UPDATED_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine equals to UPDATED_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.in=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine is not null
        defaultAziSupPartConduzioneShouldBeFound("dataFine.specified=true");

        // Get all the aziSupPartConduzioneList where dataFine is null
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine is greater than or equal to DEFAULT_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.greaterThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine is greater than or equal to UPDATED_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.greaterThanOrEqual=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine is less than or equal to DEFAULT_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.lessThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine is less than or equal to SMALLER_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.lessThanOrEqual=" + SMALLER_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine is less than DEFAULT_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.lessThan=" + DEFAULT_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine is less than UPDATED_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.lessThan=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFine is greater than DEFAULT_DATA_FINE
        defaultAziSupPartConduzioneShouldNotBeFound("dataFine.greaterThan=" + DEFAULT_DATA_FINE);

        // Get all the aziSupPartConduzioneList where dataFine is greater than SMALLER_DATA_FINE
        defaultAziSupPartConduzioneShouldBeFound("dataFine.greaterThan=" + SMALLER_DATA_FINE);
    }


    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali is not null
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.specified=true");

        // Get all the aziSupPartConduzioneList where dataInizVali is null
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartConduzioneList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali is not null
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.specified=true");

        // Get all the aziSupPartConduzioneList where dataFineVali is null
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartConduzioneList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziSupPartConduzioneShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator is not null
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.specified=true");

        // Get all the aziSupPartConduzioneList where userIdCreator is null
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartConduzioneList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziSupPartConduzioneShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod is not null
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziSupPartConduzioneList where userIdLastMod is null
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartConduzionesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartConduzioneRepository.saveAndFlush(aziSupPartConduzione);

        // Get all the aziSupPartConduzioneList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartConduzioneList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziSupPartConduzioneShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziSupPartConduzioneShouldBeFound(String filter) throws Exception {
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSupPartConduzione.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziSupPartConduzioneShouldNotBeFound(String filter) throws Exception {
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziSupPartConduzione() throws Exception {
        // Get the aziSupPartConduzione
        restAziSupPartConduzioneMockMvc.perform(get("/api/azi-sup-part-conduziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziSupPartConduzione() throws Exception {
        // Initialize the database
        aziSupPartConduzioneService.save(aziSupPartConduzione);

        int databaseSizeBeforeUpdate = aziSupPartConduzioneRepository.findAll().size();

        // Update the aziSupPartConduzione
        AziSupPartConduzione updatedAziSupPartConduzione = aziSupPartConduzioneRepository.findById(aziSupPartConduzione.getId()).get();
        // Disconnect from session so that the updates on updatedAziSupPartConduzione are not directly saved in db
        em.detach(updatedAziSupPartConduzione);
        updatedAziSupPartConduzione
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziSupPartConduzioneMockMvc.perform(put("/api/azi-sup-part-conduziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziSupPartConduzione)))
            .andExpect(status().isOk());

        // Validate the AziSupPartConduzione in the database
        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeUpdate);
        AziSupPartConduzione testAziSupPartConduzione = aziSupPartConduzioneList.get(aziSupPartConduzioneList.size() - 1);
        assertThat(testAziSupPartConduzione.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testAziSupPartConduzione.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
        assertThat(testAziSupPartConduzione.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
        assertThat(testAziSupPartConduzione.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziSupPartConduzione.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziSupPartConduzione.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziSupPartConduzione.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziSupPartConduzione.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziSupPartConduzione() throws Exception {
        int databaseSizeBeforeUpdate = aziSupPartConduzioneRepository.findAll().size();

        // Create the AziSupPartConduzione

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziSupPartConduzioneMockMvc.perform(put("/api/azi-sup-part-conduziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartConduzione)))
            .andExpect(status().isBadRequest());

        // Validate the AziSupPartConduzione in the database
        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziSupPartConduzione() throws Exception {
        // Initialize the database
        aziSupPartConduzioneService.save(aziSupPartConduzione);

        int databaseSizeBeforeDelete = aziSupPartConduzioneRepository.findAll().size();

        // Delete the aziSupPartConduzione
        restAziSupPartConduzioneMockMvc.perform(delete("/api/azi-sup-part-conduziones/{id}", aziSupPartConduzione.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziSupPartConduzione> aziSupPartConduzioneList = aziSupPartConduzioneRepository.findAll();
        assertThat(aziSupPartConduzioneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziSupPartConduzione.class);
        AziSupPartConduzione aziSupPartConduzione1 = new AziSupPartConduzione();
        aziSupPartConduzione1.setId(1L);
        AziSupPartConduzione aziSupPartConduzione2 = new AziSupPartConduzione();
        aziSupPartConduzione2.setId(aziSupPartConduzione1.getId());
        assertThat(aziSupPartConduzione1).isEqualTo(aziSupPartConduzione2);
        aziSupPartConduzione2.setId(2L);
        assertThat(aziSupPartConduzione1).isNotEqualTo(aziSupPartConduzione2);
        aziSupPartConduzione1.setId(null);
        assertThat(aziSupPartConduzione1).isNotEqualTo(aziSupPartConduzione2);
    }
}
