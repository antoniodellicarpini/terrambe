package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.repository.AziSupPartDettaglioRepository;
import com.terrambe.service.AziSupPartDettaglioService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziSupPartDettaglioCriteria;
import com.terrambe.service.AziSupPartDettaglioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziSupPartDettaglioResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziSupPartDettaglioResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_TARA = 1F;
    private static final Float UPDATED_TARA = 2F;
    private static final Float SMALLER_TARA = 1F - 1F;

    private static final String DEFAULT_UUID_ESRI = "AAAAAAAAAA";
    private static final String UPDATED_UUID_ESRI = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziSupPartDettaglioRepository aziSupPartDettaglioRepository;

    @Autowired
    private AziSupPartDettaglioService aziSupPartDettaglioService;

    @Autowired
    private AziSupPartDettaglioQueryService aziSupPartDettaglioQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziSupPartDettaglioMockMvc;

    private AziSupPartDettaglio aziSupPartDettaglio;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziSupPartDettaglioResource aziSupPartDettaglioResource = new AziSupPartDettaglioResource(aziSupPartDettaglioService, aziSupPartDettaglioQueryService);
        this.restAziSupPartDettaglioMockMvc = MockMvcBuilders.standaloneSetup(aziSupPartDettaglioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSupPartDettaglio createEntity(EntityManager em) {
        AziSupPartDettaglio aziSupPartDettaglio = new AziSupPartDettaglio()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .tara(DEFAULT_TARA)
            .uuidEsri(DEFAULT_UUID_ESRI)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziSupPartDettaglio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziSupPartDettaglio createUpdatedEntity(EntityManager em) {
        AziSupPartDettaglio aziSupPartDettaglio = new AziSupPartDettaglio()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .tara(UPDATED_TARA)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziSupPartDettaglio;
    }

    @BeforeEach
    public void initTest() {
        aziSupPartDettaglio = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziSupPartDettaglio() throws Exception {
        int databaseSizeBeforeCreate = aziSupPartDettaglioRepository.findAll().size();

        // Create the AziSupPartDettaglio
        restAziSupPartDettaglioMockMvc.perform(post("/api/azi-sup-part-dettaglios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartDettaglio)))
            .andExpect(status().isCreated());

        // Validate the AziSupPartDettaglio in the database
        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeCreate + 1);
        AziSupPartDettaglio testAziSupPartDettaglio = aziSupPartDettaglioList.get(aziSupPartDettaglioList.size() - 1);
        assertThat(testAziSupPartDettaglio.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testAziSupPartDettaglio.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testAziSupPartDettaglio.getTara()).isEqualTo(DEFAULT_TARA);
        assertThat(testAziSupPartDettaglio.getUuidEsri()).isEqualTo(DEFAULT_UUID_ESRI);
        assertThat(testAziSupPartDettaglio.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziSupPartDettaglio.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziSupPartDettaglio.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziSupPartDettaglio.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziSupPartDettaglio.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziSupPartDettaglioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziSupPartDettaglioRepository.findAll().size();

        // Create the AziSupPartDettaglio with an existing ID
        aziSupPartDettaglio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziSupPartDettaglioMockMvc.perform(post("/api/azi-sup-part-dettaglios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartDettaglio)))
            .andExpect(status().isBadRequest());

        // Validate the AziSupPartDettaglio in the database
        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziSupPartDettaglioRepository.findAll().size();
        // set the field null
        aziSupPartDettaglio.setIdAzienda(null);

        // Create the AziSupPartDettaglio, which fails.

        restAziSupPartDettaglioMockMvc.perform(post("/api/azi-sup-part-dettaglios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartDettaglio)))
            .andExpect(status().isBadRequest());

        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettaglios() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSupPartDettaglio.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].tara").value(hasItem(DEFAULT_TARA.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziSupPartDettaglio() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get the aziSupPartDettaglio
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios/{id}", aziSupPartDettaglio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziSupPartDettaglio.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.tara").value(DEFAULT_TARA.doubleValue()))
            .andExpect(jsonPath("$.uuidEsri").value(DEFAULT_UUID_ESRI.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda is not null
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.specified=true");

        // Get all the aziSupPartDettaglioList where idAzienda is null
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultAziSupPartDettaglioShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the aziSupPartDettaglioList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultAziSupPartDettaglioShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd is not null
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.specified=true");

        // Get all the aziSupPartDettaglioList where idUnitaProd is null
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the aziSupPartDettaglioList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultAziSupPartDettaglioShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara equals to DEFAULT_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.equals=" + DEFAULT_TARA);

        // Get all the aziSupPartDettaglioList where tara equals to UPDATED_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.equals=" + UPDATED_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara in DEFAULT_TARA or UPDATED_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.in=" + DEFAULT_TARA + "," + UPDATED_TARA);

        // Get all the aziSupPartDettaglioList where tara equals to UPDATED_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.in=" + UPDATED_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara is not null
        defaultAziSupPartDettaglioShouldBeFound("tara.specified=true");

        // Get all the aziSupPartDettaglioList where tara is null
        defaultAziSupPartDettaglioShouldNotBeFound("tara.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara is greater than or equal to DEFAULT_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.greaterThanOrEqual=" + DEFAULT_TARA);

        // Get all the aziSupPartDettaglioList where tara is greater than or equal to UPDATED_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.greaterThanOrEqual=" + UPDATED_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara is less than or equal to DEFAULT_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.lessThanOrEqual=" + DEFAULT_TARA);

        // Get all the aziSupPartDettaglioList where tara is less than or equal to SMALLER_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.lessThanOrEqual=" + SMALLER_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara is less than DEFAULT_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.lessThan=" + DEFAULT_TARA);

        // Get all the aziSupPartDettaglioList where tara is less than UPDATED_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.lessThan=" + UPDATED_TARA);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByTaraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where tara is greater than DEFAULT_TARA
        defaultAziSupPartDettaglioShouldNotBeFound("tara.greaterThan=" + DEFAULT_TARA);

        // Get all the aziSupPartDettaglioList where tara is greater than SMALLER_TARA
        defaultAziSupPartDettaglioShouldBeFound("tara.greaterThan=" + SMALLER_TARA);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUuidEsriIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where uuidEsri equals to DEFAULT_UUID_ESRI
        defaultAziSupPartDettaglioShouldBeFound("uuidEsri.equals=" + DEFAULT_UUID_ESRI);

        // Get all the aziSupPartDettaglioList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAziSupPartDettaglioShouldNotBeFound("uuidEsri.equals=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUuidEsriIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where uuidEsri in DEFAULT_UUID_ESRI or UPDATED_UUID_ESRI
        defaultAziSupPartDettaglioShouldBeFound("uuidEsri.in=" + DEFAULT_UUID_ESRI + "," + UPDATED_UUID_ESRI);

        // Get all the aziSupPartDettaglioList where uuidEsri equals to UPDATED_UUID_ESRI
        defaultAziSupPartDettaglioShouldNotBeFound("uuidEsri.in=" + UPDATED_UUID_ESRI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUuidEsriIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where uuidEsri is not null
        defaultAziSupPartDettaglioShouldBeFound("uuidEsri.specified=true");

        // Get all the aziSupPartDettaglioList where uuidEsri is null
        defaultAziSupPartDettaglioShouldNotBeFound("uuidEsri.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali is not null
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.specified=true");

        // Get all the aziSupPartDettaglioList where dataInizVali is null
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziSupPartDettaglioList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali is not null
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.specified=true");

        // Get all the aziSupPartDettaglioList where dataFineVali is null
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziSupPartDettaglioList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziSupPartDettaglioShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator is not null
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.specified=true");

        // Get all the aziSupPartDettaglioList where userIdCreator is null
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziSupPartDettaglioList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziSupPartDettaglioShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod is not null
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziSupPartDettaglioList where userIdLastMod is null
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);

        // Get all the aziSupPartDettaglioList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziSupPartDettaglioList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziSupPartDettaglioShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDettaglioToPartIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);
        AziSuperficieParticella dettaglioToPart = AziSuperficieParticellaResourceIT.createEntity(em);
        em.persist(dettaglioToPart);
        em.flush();
        aziSupPartDettaglio.setDettaglioToPart(dettaglioToPart);
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);
        Long dettaglioToPartId = dettaglioToPart.getId();

        // Get all the aziSupPartDettaglioList where dettaglioToPart equals to dettaglioToPartId
        defaultAziSupPartDettaglioShouldBeFound("dettaglioToPartId.equals=" + dettaglioToPartId);

        // Get all the aziSupPartDettaglioList where dettaglioToPart equals to dettaglioToPartId + 1
        defaultAziSupPartDettaglioShouldNotBeFound("dettaglioToPartId.equals=" + (dettaglioToPartId + 1));
    }


    @Test
    @Transactional
    public void getAllAziSupPartDettagliosByDettToAgeaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);
        Agea20152020Matrice dettToAgea = Agea20152020MatriceResourceIT.createEntity(em);
        em.persist(dettToAgea);
        em.flush();
        aziSupPartDettaglio.setDettToAgea(dettToAgea);
        aziSupPartDettaglioRepository.saveAndFlush(aziSupPartDettaglio);
        Long dettToAgeaId = dettToAgea.getId();

        // Get all the aziSupPartDettaglioList where dettToAgea equals to dettToAgeaId
        defaultAziSupPartDettaglioShouldBeFound("dettToAgeaId.equals=" + dettToAgeaId);

        // Get all the aziSupPartDettaglioList where dettToAgea equals to dettToAgeaId + 1
        defaultAziSupPartDettaglioShouldNotBeFound("dettToAgeaId.equals=" + (dettToAgeaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziSupPartDettaglioShouldBeFound(String filter) throws Exception {
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziSupPartDettaglio.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].tara").value(hasItem(DEFAULT_TARA.doubleValue())))
            .andExpect(jsonPath("$.[*].uuidEsri").value(hasItem(DEFAULT_UUID_ESRI)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziSupPartDettaglioShouldNotBeFound(String filter) throws Exception {
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziSupPartDettaglio() throws Exception {
        // Get the aziSupPartDettaglio
        restAziSupPartDettaglioMockMvc.perform(get("/api/azi-sup-part-dettaglios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziSupPartDettaglio() throws Exception {
        // Initialize the database
        aziSupPartDettaglioService.save(aziSupPartDettaglio);

        int databaseSizeBeforeUpdate = aziSupPartDettaglioRepository.findAll().size();

        // Update the aziSupPartDettaglio
        AziSupPartDettaglio updatedAziSupPartDettaglio = aziSupPartDettaglioRepository.findById(aziSupPartDettaglio.getId()).get();
        // Disconnect from session so that the updates on updatedAziSupPartDettaglio are not directly saved in db
        em.detach(updatedAziSupPartDettaglio);
        updatedAziSupPartDettaglio
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .tara(UPDATED_TARA)
            .uuidEsri(UPDATED_UUID_ESRI)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziSupPartDettaglioMockMvc.perform(put("/api/azi-sup-part-dettaglios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziSupPartDettaglio)))
            .andExpect(status().isOk());

        // Validate the AziSupPartDettaglio in the database
        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeUpdate);
        AziSupPartDettaglio testAziSupPartDettaglio = aziSupPartDettaglioList.get(aziSupPartDettaglioList.size() - 1);
        assertThat(testAziSupPartDettaglio.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testAziSupPartDettaglio.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testAziSupPartDettaglio.getTara()).isEqualTo(UPDATED_TARA);
        assertThat(testAziSupPartDettaglio.getUuidEsri()).isEqualTo(UPDATED_UUID_ESRI);
        assertThat(testAziSupPartDettaglio.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziSupPartDettaglio.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziSupPartDettaglio.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziSupPartDettaglio.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziSupPartDettaglio.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziSupPartDettaglio() throws Exception {
        int databaseSizeBeforeUpdate = aziSupPartDettaglioRepository.findAll().size();

        // Create the AziSupPartDettaglio

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziSupPartDettaglioMockMvc.perform(put("/api/azi-sup-part-dettaglios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziSupPartDettaglio)))
            .andExpect(status().isBadRequest());

        // Validate the AziSupPartDettaglio in the database
        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziSupPartDettaglio() throws Exception {
        // Initialize the database
        aziSupPartDettaglioService.save(aziSupPartDettaglio);

        int databaseSizeBeforeDelete = aziSupPartDettaglioRepository.findAll().size();

        // Delete the aziSupPartDettaglio
        restAziSupPartDettaglioMockMvc.perform(delete("/api/azi-sup-part-dettaglios/{id}", aziSupPartDettaglio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziSupPartDettaglio> aziSupPartDettaglioList = aziSupPartDettaglioRepository.findAll();
        assertThat(aziSupPartDettaglioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziSupPartDettaglio.class);
        AziSupPartDettaglio aziSupPartDettaglio1 = new AziSupPartDettaglio();
        aziSupPartDettaglio1.setId(1L);
        AziSupPartDettaglio aziSupPartDettaglio2 = new AziSupPartDettaglio();
        aziSupPartDettaglio2.setId(aziSupPartDettaglio1.getId());
        assertThat(aziSupPartDettaglio1).isEqualTo(aziSupPartDettaglio2);
        aziSupPartDettaglio2.setId(2L);
        assertThat(aziSupPartDettaglio1).isNotEqualTo(aziSupPartDettaglio2);
        aziSupPartDettaglio1.setId(null);
        assertThat(aziSupPartDettaglio1).isNotEqualTo(aziSupPartDettaglio2);
    }
}
