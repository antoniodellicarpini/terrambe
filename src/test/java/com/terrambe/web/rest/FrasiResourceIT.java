package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Frasi;
import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.repository.FrasiRepository;
import com.terrambe.service.FrasiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FrasiCriteria;
import com.terrambe.service.FrasiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FrasiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FrasiResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FrasiRepository frasiRepository;

    @Autowired
    private FrasiService frasiService;

    @Autowired
    private FrasiQueryService frasiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFrasiMockMvc;

    private Frasi frasi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FrasiResource frasiResource = new FrasiResource(frasiService, frasiQueryService);
        this.restFrasiMockMvc = MockMvcBuilders.standaloneSetup(frasiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Frasi createEntity(EntityManager em) {
        Frasi frasi = new Frasi()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return frasi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Frasi createUpdatedEntity(EntityManager em) {
        Frasi frasi = new Frasi()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return frasi;
    }

    @BeforeEach
    public void initTest() {
        frasi = createEntity(em);
    }

    @Test
    @Transactional
    public void createFrasi() throws Exception {
        int databaseSizeBeforeCreate = frasiRepository.findAll().size();

        // Create the Frasi
        restFrasiMockMvc.perform(post("/api/frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(frasi)))
            .andExpect(status().isCreated());

        // Validate the Frasi in the database
        List<Frasi> frasiList = frasiRepository.findAll();
        assertThat(frasiList).hasSize(databaseSizeBeforeCreate + 1);
        Frasi testFrasi = frasiList.get(frasiList.size() - 1);
        assertThat(testFrasi.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testFrasi.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testFrasi.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testFrasi.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testFrasi.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testFrasi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFrasi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFrasi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFrasi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFrasi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFrasiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = frasiRepository.findAll().size();

        // Create the Frasi with an existing ID
        frasi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFrasiMockMvc.perform(post("/api/frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(frasi)))
            .andExpect(status().isBadRequest());

        // Validate the Frasi in the database
        List<Frasi> frasiList = frasiRepository.findAll();
        assertThat(frasiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFrasis() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList
        restFrasiMockMvc.perform(get("/api/frasis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(frasi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getFrasi() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get the frasi
        restFrasiMockMvc.perform(get("/api/frasis/{id}", frasi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(frasi.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFrasisByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where codice equals to DEFAULT_CODICE
        defaultFrasiShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the frasiList where codice equals to UPDATED_CODICE
        defaultFrasiShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllFrasisByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultFrasiShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the frasiList where codice equals to UPDATED_CODICE
        defaultFrasiShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllFrasisByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where codice is not null
        defaultFrasiShouldBeFound("codice.specified=true");

        // Get all the frasiList where codice is null
        defaultFrasiShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultFrasiShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the frasiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFrasiShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFrasisByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultFrasiShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the frasiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFrasiShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFrasisByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where descrizione is not null
        defaultFrasiShouldBeFound("descrizione.specified=true");

        // Get all the frasiList where descrizione is null
        defaultFrasiShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultFrasiShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the frasiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFrasiShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFrasisByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultFrasiShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the frasiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFrasiShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFrasisByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where tipoImport is not null
        defaultFrasiShouldBeFound("tipoImport.specified=true");

        // Get all the frasiList where tipoImport is null
        defaultFrasiShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where operatore equals to DEFAULT_OPERATORE
        defaultFrasiShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the frasiList where operatore equals to UPDATED_OPERATORE
        defaultFrasiShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFrasisByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultFrasiShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the frasiList where operatore equals to UPDATED_OPERATORE
        defaultFrasiShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFrasisByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where operatore is not null
        defaultFrasiShouldBeFound("operatore.specified=true");

        // Get all the frasiList where operatore is null
        defaultFrasiShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where ts equals to DEFAULT_TS
        defaultFrasiShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the frasiList where ts equals to UPDATED_TS
        defaultFrasiShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFrasisByTsIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where ts in DEFAULT_TS or UPDATED_TS
        defaultFrasiShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the frasiList where ts equals to UPDATED_TS
        defaultFrasiShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFrasisByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where ts is not null
        defaultFrasiShouldBeFound("ts.specified=true");

        // Get all the frasiList where ts is null
        defaultFrasiShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali is not null
        defaultFrasiShouldBeFound("dataInizVali.specified=true");

        // Get all the frasiList where dataInizVali is null
        defaultFrasiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFrasiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the frasiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFrasiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali is not null
        defaultFrasiShouldBeFound("dataFineVali.specified=true");

        // Get all the frasiList where dataFineVali is null
        defaultFrasiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFrasisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFrasiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the frasiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFrasiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator is not null
        defaultFrasiShouldBeFound("userIdCreator.specified=true");

        // Get all the frasiList where userIdCreator is null
        defaultFrasiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFrasiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the frasiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFrasiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod is not null
        defaultFrasiShouldBeFound("userIdLastMod.specified=true");

        // Get all the frasiList where userIdLastMod is null
        defaultFrasiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFrasisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);

        // Get all the frasiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFrasiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the frasiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFrasiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFrasisByFrasietichettaIsEqualToSomething() throws Exception {
        // Initialize the database
        frasiRepository.saveAndFlush(frasi);
        EtichettaFrasi frasietichetta = EtichettaFrasiResourceIT.createEntity(em);
        em.persist(frasietichetta);
        em.flush();
        frasi.addFrasietichetta(frasietichetta);
        frasiRepository.saveAndFlush(frasi);
        Long frasietichettaId = frasietichetta.getId();

        // Get all the frasiList where frasietichetta equals to frasietichettaId
        defaultFrasiShouldBeFound("frasietichettaId.equals=" + frasietichettaId);

        // Get all the frasiList where frasietichetta equals to frasietichettaId + 1
        defaultFrasiShouldNotBeFound("frasietichettaId.equals=" + (frasietichettaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFrasiShouldBeFound(String filter) throws Exception {
        restFrasiMockMvc.perform(get("/api/frasis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(frasi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFrasiMockMvc.perform(get("/api/frasis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFrasiShouldNotBeFound(String filter) throws Exception {
        restFrasiMockMvc.perform(get("/api/frasis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFrasiMockMvc.perform(get("/api/frasis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFrasi() throws Exception {
        // Get the frasi
        restFrasiMockMvc.perform(get("/api/frasis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFrasi() throws Exception {
        // Initialize the database
        frasiService.save(frasi);

        int databaseSizeBeforeUpdate = frasiRepository.findAll().size();

        // Update the frasi
        Frasi updatedFrasi = frasiRepository.findById(frasi.getId()).get();
        // Disconnect from session so that the updates on updatedFrasi are not directly saved in db
        em.detach(updatedFrasi);
        updatedFrasi
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFrasiMockMvc.perform(put("/api/frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFrasi)))
            .andExpect(status().isOk());

        // Validate the Frasi in the database
        List<Frasi> frasiList = frasiRepository.findAll();
        assertThat(frasiList).hasSize(databaseSizeBeforeUpdate);
        Frasi testFrasi = frasiList.get(frasiList.size() - 1);
        assertThat(testFrasi.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testFrasi.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testFrasi.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testFrasi.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testFrasi.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testFrasi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFrasi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFrasi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFrasi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFrasi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFrasi() throws Exception {
        int databaseSizeBeforeUpdate = frasiRepository.findAll().size();

        // Create the Frasi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFrasiMockMvc.perform(put("/api/frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(frasi)))
            .andExpect(status().isBadRequest());

        // Validate the Frasi in the database
        List<Frasi> frasiList = frasiRepository.findAll();
        assertThat(frasiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFrasi() throws Exception {
        // Initialize the database
        frasiService.save(frasi);

        int databaseSizeBeforeDelete = frasiRepository.findAll().size();

        // Delete the frasi
        restFrasiMockMvc.perform(delete("/api/frasis/{id}", frasi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Frasi> frasiList = frasiRepository.findAll();
        assertThat(frasiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Frasi.class);
        Frasi frasi1 = new Frasi();
        frasi1.setId(1L);
        Frasi frasi2 = new Frasi();
        frasi2.setId(frasi1.getId());
        assertThat(frasi1).isEqualTo(frasi2);
        frasi2.setId(2L);
        assertThat(frasi1).isNotEqualTo(frasi2);
        frasi1.setId(null);
        assertThat(frasi1).isNotEqualTo(frasi2);
    }
}
