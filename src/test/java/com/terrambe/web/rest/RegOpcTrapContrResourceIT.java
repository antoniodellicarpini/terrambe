package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.domain.RegOpcTrap;
import com.terrambe.domain.TrapContrTipo;
import com.terrambe.repository.RegOpcTrapContrRepository;
import com.terrambe.service.RegOpcTrapContrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcTrapContrCriteria;
import com.terrambe.service.RegOpcTrapContrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcTrapContrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcTrapContrResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_CONTROLLO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_CONTROLLO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_CONTROLLO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_SOSTI_FEROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_SOSTI_FEROM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_SOSTI_FEROM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_PARAMETRI = "AAAAAAAAAA";
    private static final String UPDATED_PARAMETRI = "BBBBBBBBBB";

    @Autowired
    private RegOpcTrapContrRepository regOpcTrapContrRepository;

    @Autowired
    private RegOpcTrapContrService regOpcTrapContrService;

    @Autowired
    private RegOpcTrapContrQueryService regOpcTrapContrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcTrapContrMockMvc;

    private RegOpcTrapContr regOpcTrapContr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcTrapContrResource regOpcTrapContrResource = new RegOpcTrapContrResource(regOpcTrapContrService, regOpcTrapContrQueryService);
        this.restRegOpcTrapContrMockMvc = MockMvcBuilders.standaloneSetup(regOpcTrapContrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrapContr createEntity(EntityManager em) {
        RegOpcTrapContr regOpcTrapContr = new RegOpcTrapContr()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataControllo(DEFAULT_DATA_CONTROLLO)
            .dataSostiFerom(DEFAULT_DATA_SOSTI_FEROM)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE)
            .parametri(DEFAULT_PARAMETRI);
        return regOpcTrapContr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcTrapContr createUpdatedEntity(EntityManager em) {
        RegOpcTrapContr regOpcTrapContr = new RegOpcTrapContr()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataControllo(UPDATED_DATA_CONTROLLO)
            .dataSostiFerom(UPDATED_DATA_SOSTI_FEROM)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE)
            .parametri(UPDATED_PARAMETRI);
        return regOpcTrapContr;
    }

    @BeforeEach
    public void initTest() {
        regOpcTrapContr = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcTrapContr() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapContrRepository.findAll().size();

        // Create the RegOpcTrapContr
        restRegOpcTrapContrMockMvc.perform(post("/api/reg-opc-trap-contrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapContr)))
            .andExpect(status().isCreated());

        // Validate the RegOpcTrapContr in the database
        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcTrapContr testRegOpcTrapContr = regOpcTrapContrList.get(regOpcTrapContrList.size() - 1);
        assertThat(testRegOpcTrapContr.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcTrapContr.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcTrapContr.getDataControllo()).isEqualTo(DEFAULT_DATA_CONTROLLO);
        assertThat(testRegOpcTrapContr.getDataSostiFerom()).isEqualTo(DEFAULT_DATA_SOSTI_FEROM);
        assertThat(testRegOpcTrapContr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcTrapContr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcTrapContr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcTrapContr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcTrapContr.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testRegOpcTrapContr.getParametri()).isEqualTo(DEFAULT_PARAMETRI);
    }

    @Test
    @Transactional
    public void createRegOpcTrapContrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcTrapContrRepository.findAll().size();

        // Create the RegOpcTrapContr with an existing ID
        regOpcTrapContr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcTrapContrMockMvc.perform(post("/api/reg-opc-trap-contrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapContr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrapContr in the database
        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcTrapContrRepository.findAll().size();
        // set the field null
        regOpcTrapContr.setIdAzienda(null);

        // Create the RegOpcTrapContr, which fails.

        restRegOpcTrapContrMockMvc.perform(post("/api/reg-opc-trap-contrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapContr)))
            .andExpect(status().isBadRequest());

        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrs() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrapContr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataControllo").value(hasItem(DEFAULT_DATA_CONTROLLO.toString())))
            .andExpect(jsonPath("$.[*].dataSostiFerom").value(hasItem(DEFAULT_DATA_SOSTI_FEROM.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].parametri").value(hasItem(DEFAULT_PARAMETRI.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcTrapContr() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get the regOpcTrapContr
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs/{id}", regOpcTrapContr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcTrapContr.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataControllo").value(DEFAULT_DATA_CONTROLLO.toString()))
            .andExpect(jsonPath("$.dataSostiFerom").value(DEFAULT_DATA_SOSTI_FEROM.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.parametri").value(DEFAULT_PARAMETRI.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda is not null
        defaultRegOpcTrapContrShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcTrapContrList where idAzienda is null
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcTrapContrShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcTrapContrList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcTrapContrShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd is not null
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcTrapContrList where idUnitaProd is null
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcTrapContrList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcTrapContrShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo equals to DEFAULT_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.equals=" + DEFAULT_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo equals to UPDATED_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.equals=" + UPDATED_DATA_CONTROLLO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo in DEFAULT_DATA_CONTROLLO or UPDATED_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.in=" + DEFAULT_DATA_CONTROLLO + "," + UPDATED_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo equals to UPDATED_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.in=" + UPDATED_DATA_CONTROLLO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo is not null
        defaultRegOpcTrapContrShouldBeFound("dataControllo.specified=true");

        // Get all the regOpcTrapContrList where dataControllo is null
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo is greater than or equal to DEFAULT_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.greaterThanOrEqual=" + DEFAULT_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo is greater than or equal to UPDATED_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.greaterThanOrEqual=" + UPDATED_DATA_CONTROLLO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo is less than or equal to DEFAULT_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.lessThanOrEqual=" + DEFAULT_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo is less than or equal to SMALLER_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.lessThanOrEqual=" + SMALLER_DATA_CONTROLLO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo is less than DEFAULT_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.lessThan=" + DEFAULT_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo is less than UPDATED_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.lessThan=" + UPDATED_DATA_CONTROLLO);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataControlloIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataControllo is greater than DEFAULT_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldNotBeFound("dataControllo.greaterThan=" + DEFAULT_DATA_CONTROLLO);

        // Get all the regOpcTrapContrList where dataControllo is greater than SMALLER_DATA_CONTROLLO
        defaultRegOpcTrapContrShouldBeFound("dataControllo.greaterThan=" + SMALLER_DATA_CONTROLLO);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom equals to DEFAULT_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.equals=" + DEFAULT_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom equals to UPDATED_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.equals=" + UPDATED_DATA_SOSTI_FEROM);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom in DEFAULT_DATA_SOSTI_FEROM or UPDATED_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.in=" + DEFAULT_DATA_SOSTI_FEROM + "," + UPDATED_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom equals to UPDATED_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.in=" + UPDATED_DATA_SOSTI_FEROM);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom is not null
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.specified=true");

        // Get all the regOpcTrapContrList where dataSostiFerom is null
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom is greater than or equal to DEFAULT_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.greaterThanOrEqual=" + DEFAULT_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom is greater than or equal to UPDATED_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.greaterThanOrEqual=" + UPDATED_DATA_SOSTI_FEROM);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom is less than or equal to DEFAULT_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.lessThanOrEqual=" + DEFAULT_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom is less than or equal to SMALLER_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.lessThanOrEqual=" + SMALLER_DATA_SOSTI_FEROM);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom is less than DEFAULT_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.lessThan=" + DEFAULT_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom is less than UPDATED_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.lessThan=" + UPDATED_DATA_SOSTI_FEROM);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataSostiFeromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataSostiFerom is greater than DEFAULT_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldNotBeFound("dataSostiFerom.greaterThan=" + DEFAULT_DATA_SOSTI_FEROM);

        // Get all the regOpcTrapContrList where dataSostiFerom is greater than SMALLER_DATA_SOSTI_FEROM
        defaultRegOpcTrapContrShouldBeFound("dataSostiFerom.greaterThan=" + SMALLER_DATA_SOSTI_FEROM);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali is not null
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcTrapContrList where dataInizVali is null
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcTrapContrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcTrapContrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali is not null
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcTrapContrList where dataFineVali is null
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcTrapContrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcTrapContrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator is not null
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcTrapContrList where userIdCreator is null
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcTrapContrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcTrapContrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod is not null
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcTrapContrList where userIdLastMod is null
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);

        // Get all the regOpcTrapContrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcTrapContrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcTrapContrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByTrapContrToOpcTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);
        RegOpcTrap trapContrToOpcTrap = RegOpcTrapResourceIT.createEntity(em);
        em.persist(trapContrToOpcTrap);
        em.flush();
        regOpcTrapContr.setTrapContrToOpcTrap(trapContrToOpcTrap);
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);
        Long trapContrToOpcTrapId = trapContrToOpcTrap.getId();

        // Get all the regOpcTrapContrList where trapContrToOpcTrap equals to trapContrToOpcTrapId
        defaultRegOpcTrapContrShouldBeFound("trapContrToOpcTrapId.equals=" + trapContrToOpcTrapId);

        // Get all the regOpcTrapContrList where trapContrToOpcTrap equals to trapContrToOpcTrapId + 1
        defaultRegOpcTrapContrShouldNotBeFound("trapContrToOpcTrapId.equals=" + (trapContrToOpcTrapId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcTrapContrsByTrapContrTipoToTrapContrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);
        TrapContrTipo trapContrTipoToTrapContr = TrapContrTipoResourceIT.createEntity(em);
        em.persist(trapContrTipoToTrapContr);
        em.flush();
        regOpcTrapContr.setTrapContrTipoToTrapContr(trapContrTipoToTrapContr);
        regOpcTrapContrRepository.saveAndFlush(regOpcTrapContr);
        Long trapContrTipoToTrapContrId = trapContrTipoToTrapContr.getId();

        // Get all the regOpcTrapContrList where trapContrTipoToTrapContr equals to trapContrTipoToTrapContrId
        defaultRegOpcTrapContrShouldBeFound("trapContrTipoToTrapContrId.equals=" + trapContrTipoToTrapContrId);

        // Get all the regOpcTrapContrList where trapContrTipoToTrapContr equals to trapContrTipoToTrapContrId + 1
        defaultRegOpcTrapContrShouldNotBeFound("trapContrTipoToTrapContrId.equals=" + (trapContrTipoToTrapContrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcTrapContrShouldBeFound(String filter) throws Exception {
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcTrapContr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataControllo").value(hasItem(DEFAULT_DATA_CONTROLLO.toString())))
            .andExpect(jsonPath("$.[*].dataSostiFerom").value(hasItem(DEFAULT_DATA_SOSTI_FEROM.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].parametri").value(hasItem(DEFAULT_PARAMETRI.toString())));

        // Check, that the count call also returns 1
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcTrapContrShouldNotBeFound(String filter) throws Exception {
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcTrapContr() throws Exception {
        // Get the regOpcTrapContr
        restRegOpcTrapContrMockMvc.perform(get("/api/reg-opc-trap-contrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcTrapContr() throws Exception {
        // Initialize the database
        regOpcTrapContrService.save(regOpcTrapContr);

        int databaseSizeBeforeUpdate = regOpcTrapContrRepository.findAll().size();

        // Update the regOpcTrapContr
        RegOpcTrapContr updatedRegOpcTrapContr = regOpcTrapContrRepository.findById(regOpcTrapContr.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcTrapContr are not directly saved in db
        em.detach(updatedRegOpcTrapContr);
        updatedRegOpcTrapContr
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataControllo(UPDATED_DATA_CONTROLLO)
            .dataSostiFerom(UPDATED_DATA_SOSTI_FEROM)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE)
            .parametri(UPDATED_PARAMETRI);

        restRegOpcTrapContrMockMvc.perform(put("/api/reg-opc-trap-contrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcTrapContr)))
            .andExpect(status().isOk());

        // Validate the RegOpcTrapContr in the database
        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeUpdate);
        RegOpcTrapContr testRegOpcTrapContr = regOpcTrapContrList.get(regOpcTrapContrList.size() - 1);
        assertThat(testRegOpcTrapContr.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcTrapContr.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcTrapContr.getDataControllo()).isEqualTo(UPDATED_DATA_CONTROLLO);
        assertThat(testRegOpcTrapContr.getDataSostiFerom()).isEqualTo(UPDATED_DATA_SOSTI_FEROM);
        assertThat(testRegOpcTrapContr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcTrapContr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcTrapContr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcTrapContr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcTrapContr.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testRegOpcTrapContr.getParametri()).isEqualTo(UPDATED_PARAMETRI);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcTrapContr() throws Exception {
        int databaseSizeBeforeUpdate = regOpcTrapContrRepository.findAll().size();

        // Create the RegOpcTrapContr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcTrapContrMockMvc.perform(put("/api/reg-opc-trap-contrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcTrapContr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcTrapContr in the database
        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcTrapContr() throws Exception {
        // Initialize the database
        regOpcTrapContrService.save(regOpcTrapContr);

        int databaseSizeBeforeDelete = regOpcTrapContrRepository.findAll().size();

        // Delete the regOpcTrapContr
        restRegOpcTrapContrMockMvc.perform(delete("/api/reg-opc-trap-contrs/{id}", regOpcTrapContr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcTrapContr> regOpcTrapContrList = regOpcTrapContrRepository.findAll();
        assertThat(regOpcTrapContrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcTrapContr.class);
        RegOpcTrapContr regOpcTrapContr1 = new RegOpcTrapContr();
        regOpcTrapContr1.setId(1L);
        RegOpcTrapContr regOpcTrapContr2 = new RegOpcTrapContr();
        regOpcTrapContr2.setId(regOpcTrapContr1.getId());
        assertThat(regOpcTrapContr1).isEqualTo(regOpcTrapContr2);
        regOpcTrapContr2.setId(2L);
        assertThat(regOpcTrapContr1).isNotEqualTo(regOpcTrapContr2);
        regOpcTrapContr1.setId(null);
        assertThat(regOpcTrapContr1).isNotEqualTo(regOpcTrapContr2);
    }
}
