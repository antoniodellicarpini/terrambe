package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Avversita;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.AvversitaRepository;
import com.terrambe.service.AvversitaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AvversitaCriteria;
import com.terrambe.service.AvversitaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AvversitaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AvversitaResourceIT {

    private static final String DEFAULT_COD_AVVERSITA = "AAAAAAAAAA";
    private static final String UPDATED_COD_AVVERSITA = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_SCI = "AAAAAAAAAA";
    private static final String UPDATED_NOME_SCI = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AvversitaRepository avversitaRepository;

    @Autowired
    private AvversitaService avversitaService;

    @Autowired
    private AvversitaQueryService avversitaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAvversitaMockMvc;

    private Avversita avversita;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AvversitaResource avversitaResource = new AvversitaResource(avversitaService, avversitaQueryService);
        this.restAvversitaMockMvc = MockMvcBuilders.standaloneSetup(avversitaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Avversita createEntity(EntityManager em) {
        Avversita avversita = new Avversita()
            .codAvversita(DEFAULT_COD_AVVERSITA)
            .nomeSci(DEFAULT_NOME_SCI)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return avversita;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Avversita createUpdatedEntity(EntityManager em) {
        Avversita avversita = new Avversita()
            .codAvversita(UPDATED_COD_AVVERSITA)
            .nomeSci(UPDATED_NOME_SCI)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return avversita;
    }

    @BeforeEach
    public void initTest() {
        avversita = createEntity(em);
    }

    @Test
    @Transactional
    public void createAvversita() throws Exception {
        int databaseSizeBeforeCreate = avversitaRepository.findAll().size();

        // Create the Avversita
        restAvversitaMockMvc.perform(post("/api/avversitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(avversita)))
            .andExpect(status().isCreated());

        // Validate the Avversita in the database
        List<Avversita> avversitaList = avversitaRepository.findAll();
        assertThat(avversitaList).hasSize(databaseSizeBeforeCreate + 1);
        Avversita testAvversita = avversitaList.get(avversitaList.size() - 1);
        assertThat(testAvversita.getCodAvversita()).isEqualTo(DEFAULT_COD_AVVERSITA);
        assertThat(testAvversita.getNomeSci()).isEqualTo(DEFAULT_NOME_SCI);
        assertThat(testAvversita.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testAvversita.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testAvversita.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testAvversita.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAvversita.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAvversita.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAvversita.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAvversita.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAvversitaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = avversitaRepository.findAll().size();

        // Create the Avversita with an existing ID
        avversita.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAvversitaMockMvc.perform(post("/api/avversitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(avversita)))
            .andExpect(status().isBadRequest());

        // Validate the Avversita in the database
        List<Avversita> avversitaList = avversitaRepository.findAll();
        assertThat(avversitaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAvversitas() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList
        restAvversitaMockMvc.perform(get("/api/avversitas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(avversita.getId().intValue())))
            .andExpect(jsonPath("$.[*].codAvversita").value(hasItem(DEFAULT_COD_AVVERSITA.toString())))
            .andExpect(jsonPath("$.[*].nomeSci").value(hasItem(DEFAULT_NOME_SCI.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAvversita() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get the avversita
        restAvversitaMockMvc.perform(get("/api/avversitas/{id}", avversita.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(avversita.getId().intValue()))
            .andExpect(jsonPath("$.codAvversita").value(DEFAULT_COD_AVVERSITA.toString()))
            .andExpect(jsonPath("$.nomeSci").value(DEFAULT_NOME_SCI.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAvversitasByCodAvversitaIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where codAvversita equals to DEFAULT_COD_AVVERSITA
        defaultAvversitaShouldBeFound("codAvversita.equals=" + DEFAULT_COD_AVVERSITA);

        // Get all the avversitaList where codAvversita equals to UPDATED_COD_AVVERSITA
        defaultAvversitaShouldNotBeFound("codAvversita.equals=" + UPDATED_COD_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllAvversitasByCodAvversitaIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where codAvversita in DEFAULT_COD_AVVERSITA or UPDATED_COD_AVVERSITA
        defaultAvversitaShouldBeFound("codAvversita.in=" + DEFAULT_COD_AVVERSITA + "," + UPDATED_COD_AVVERSITA);

        // Get all the avversitaList where codAvversita equals to UPDATED_COD_AVVERSITA
        defaultAvversitaShouldNotBeFound("codAvversita.in=" + UPDATED_COD_AVVERSITA);
    }

    @Test
    @Transactional
    public void getAllAvversitasByCodAvversitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where codAvversita is not null
        defaultAvversitaShouldBeFound("codAvversita.specified=true");

        // Get all the avversitaList where codAvversita is null
        defaultAvversitaShouldNotBeFound("codAvversita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByNomeSciIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where nomeSci equals to DEFAULT_NOME_SCI
        defaultAvversitaShouldBeFound("nomeSci.equals=" + DEFAULT_NOME_SCI);

        // Get all the avversitaList where nomeSci equals to UPDATED_NOME_SCI
        defaultAvversitaShouldNotBeFound("nomeSci.equals=" + UPDATED_NOME_SCI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByNomeSciIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where nomeSci in DEFAULT_NOME_SCI or UPDATED_NOME_SCI
        defaultAvversitaShouldBeFound("nomeSci.in=" + DEFAULT_NOME_SCI + "," + UPDATED_NOME_SCI);

        // Get all the avversitaList where nomeSci equals to UPDATED_NOME_SCI
        defaultAvversitaShouldNotBeFound("nomeSci.in=" + UPDATED_NOME_SCI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByNomeSciIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where nomeSci is not null
        defaultAvversitaShouldBeFound("nomeSci.specified=true");

        // Get all the avversitaList where nomeSci is null
        defaultAvversitaShouldNotBeFound("nomeSci.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultAvversitaShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the avversitaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultAvversitaShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllAvversitasByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultAvversitaShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the avversitaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultAvversitaShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllAvversitasByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where tipoImport is not null
        defaultAvversitaShouldBeFound("tipoImport.specified=true");

        // Get all the avversitaList where tipoImport is null
        defaultAvversitaShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where operatore equals to DEFAULT_OPERATORE
        defaultAvversitaShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the avversitaList where operatore equals to UPDATED_OPERATORE
        defaultAvversitaShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllAvversitasByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultAvversitaShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the avversitaList where operatore equals to UPDATED_OPERATORE
        defaultAvversitaShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllAvversitasByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where operatore is not null
        defaultAvversitaShouldBeFound("operatore.specified=true");

        // Get all the avversitaList where operatore is null
        defaultAvversitaShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where ts equals to DEFAULT_TS
        defaultAvversitaShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the avversitaList where ts equals to UPDATED_TS
        defaultAvversitaShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllAvversitasByTsIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where ts in DEFAULT_TS or UPDATED_TS
        defaultAvversitaShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the avversitaList where ts equals to UPDATED_TS
        defaultAvversitaShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllAvversitasByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where ts is not null
        defaultAvversitaShouldBeFound("ts.specified=true");

        // Get all the avversitaList where ts is null
        defaultAvversitaShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali is not null
        defaultAvversitaShouldBeFound("dataInizVali.specified=true");

        // Get all the avversitaList where dataInizVali is null
        defaultAvversitaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAvversitaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the avversitaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAvversitaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali is not null
        defaultAvversitaShouldBeFound("dataFineVali.specified=true");

        // Get all the avversitaList where dataFineVali is null
        defaultAvversitaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAvversitasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAvversitaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the avversitaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAvversitaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator is not null
        defaultAvversitaShouldBeFound("userIdCreator.specified=true");

        // Get all the avversitaList where userIdCreator is null
        defaultAvversitaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAvversitaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the avversitaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAvversitaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod is not null
        defaultAvversitaShouldBeFound("userIdLastMod.specified=true");

        // Get all the avversitaList where userIdLastMod is null
        defaultAvversitaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAvversitasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);

        // Get all the avversitaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAvversitaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the avversitaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAvversitaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAvversitasByAvversToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        avversitaRepository.saveAndFlush(avversita);
        TabellaRaccordo avversToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(avversToRaccordo);
        em.flush();
        avversita.addAvversToRaccordo(avversToRaccordo);
        avversitaRepository.saveAndFlush(avversita);
        Long avversToRaccordoId = avversToRaccordo.getId();

        // Get all the avversitaList where avversToRaccordo equals to avversToRaccordoId
        defaultAvversitaShouldBeFound("avversToRaccordoId.equals=" + avversToRaccordoId);

        // Get all the avversitaList where avversToRaccordo equals to avversToRaccordoId + 1
        defaultAvversitaShouldNotBeFound("avversToRaccordoId.equals=" + (avversToRaccordoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAvversitaShouldBeFound(String filter) throws Exception {
        restAvversitaMockMvc.perform(get("/api/avversitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(avversita.getId().intValue())))
            .andExpect(jsonPath("$.[*].codAvversita").value(hasItem(DEFAULT_COD_AVVERSITA)))
            .andExpect(jsonPath("$.[*].nomeSci").value(hasItem(DEFAULT_NOME_SCI)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAvversitaMockMvc.perform(get("/api/avversitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAvversitaShouldNotBeFound(String filter) throws Exception {
        restAvversitaMockMvc.perform(get("/api/avversitas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAvversitaMockMvc.perform(get("/api/avversitas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAvversita() throws Exception {
        // Get the avversita
        restAvversitaMockMvc.perform(get("/api/avversitas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAvversita() throws Exception {
        // Initialize the database
        avversitaService.save(avversita);

        int databaseSizeBeforeUpdate = avversitaRepository.findAll().size();

        // Update the avversita
        Avversita updatedAvversita = avversitaRepository.findById(avversita.getId()).get();
        // Disconnect from session so that the updates on updatedAvversita are not directly saved in db
        em.detach(updatedAvversita);
        updatedAvversita
            .codAvversita(UPDATED_COD_AVVERSITA)
            .nomeSci(UPDATED_NOME_SCI)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAvversitaMockMvc.perform(put("/api/avversitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAvversita)))
            .andExpect(status().isOk());

        // Validate the Avversita in the database
        List<Avversita> avversitaList = avversitaRepository.findAll();
        assertThat(avversitaList).hasSize(databaseSizeBeforeUpdate);
        Avversita testAvversita = avversitaList.get(avversitaList.size() - 1);
        assertThat(testAvversita.getCodAvversita()).isEqualTo(UPDATED_COD_AVVERSITA);
        assertThat(testAvversita.getNomeSci()).isEqualTo(UPDATED_NOME_SCI);
        assertThat(testAvversita.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testAvversita.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testAvversita.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testAvversita.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAvversita.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAvversita.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAvversita.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAvversita.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAvversita() throws Exception {
        int databaseSizeBeforeUpdate = avversitaRepository.findAll().size();

        // Create the Avversita

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAvversitaMockMvc.perform(put("/api/avversitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(avversita)))
            .andExpect(status().isBadRequest());

        // Validate the Avversita in the database
        List<Avversita> avversitaList = avversitaRepository.findAll();
        assertThat(avversitaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAvversita() throws Exception {
        // Initialize the database
        avversitaService.save(avversita);

        int databaseSizeBeforeDelete = avversitaRepository.findAll().size();

        // Delete the avversita
        restAvversitaMockMvc.perform(delete("/api/avversitas/{id}", avversita.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Avversita> avversitaList = avversitaRepository.findAll();
        assertThat(avversitaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Avversita.class);
        Avversita avversita1 = new Avversita();
        avversita1.setId(1L);
        Avversita avversita2 = new Avversita();
        avversita2.setId(avversita1.getId());
        assertThat(avversita1).isEqualTo(avversita2);
        avversita2.setId(2L);
        assertThat(avversita1).isNotEqualTo(avversita2);
        avversita1.setId(null);
        assertThat(avversita1).isNotEqualTo(avversita2);
    }
}
