package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Formulazione;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.FormulazioneRepository;
import com.terrambe.service.FormulazioneService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FormulazioneCriteria;
import com.terrambe.service.FormulazioneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FormulazioneResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FormulazioneResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_SIGLA_FORMULAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_SIGLA_FORMULAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FormulazioneRepository formulazioneRepository;

    @Autowired
    private FormulazioneService formulazioneService;

    @Autowired
    private FormulazioneQueryService formulazioneQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFormulazioneMockMvc;

    private Formulazione formulazione;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FormulazioneResource formulazioneResource = new FormulazioneResource(formulazioneService, formulazioneQueryService);
        this.restFormulazioneMockMvc = MockMvcBuilders.standaloneSetup(formulazioneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Formulazione createEntity(EntityManager em) {
        Formulazione formulazione = new Formulazione()
            .descrizione(DEFAULT_DESCRIZIONE)
            .siglaFormulazione(DEFAULT_SIGLA_FORMULAZIONE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return formulazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Formulazione createUpdatedEntity(EntityManager em) {
        Formulazione formulazione = new Formulazione()
            .descrizione(UPDATED_DESCRIZIONE)
            .siglaFormulazione(UPDATED_SIGLA_FORMULAZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return formulazione;
    }

    @BeforeEach
    public void initTest() {
        formulazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createFormulazione() throws Exception {
        int databaseSizeBeforeCreate = formulazioneRepository.findAll().size();

        // Create the Formulazione
        restFormulazioneMockMvc.perform(post("/api/formulaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulazione)))
            .andExpect(status().isCreated());

        // Validate the Formulazione in the database
        List<Formulazione> formulazioneList = formulazioneRepository.findAll();
        assertThat(formulazioneList).hasSize(databaseSizeBeforeCreate + 1);
        Formulazione testFormulazione = formulazioneList.get(formulazioneList.size() - 1);
        assertThat(testFormulazione.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testFormulazione.getSiglaFormulazione()).isEqualTo(DEFAULT_SIGLA_FORMULAZIONE);
        assertThat(testFormulazione.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testFormulazione.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testFormulazione.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testFormulazione.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFormulazione.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFormulazione.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFormulazione.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFormulazione.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFormulazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = formulazioneRepository.findAll().size();

        // Create the Formulazione with an existing ID
        formulazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFormulazioneMockMvc.perform(post("/api/formulaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulazione)))
            .andExpect(status().isBadRequest());

        // Validate the Formulazione in the database
        List<Formulazione> formulazioneList = formulazioneRepository.findAll();
        assertThat(formulazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFormulaziones() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList
        restFormulazioneMockMvc.perform(get("/api/formulaziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(formulazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].siglaFormulazione").value(hasItem(DEFAULT_SIGLA_FORMULAZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getFormulazione() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get the formulazione
        restFormulazioneMockMvc.perform(get("/api/formulaziones/{id}", formulazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(formulazione.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.siglaFormulazione").value(DEFAULT_SIGLA_FORMULAZIONE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultFormulazioneShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the formulazioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFormulazioneShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultFormulazioneShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the formulazioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultFormulazioneShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where descrizione is not null
        defaultFormulazioneShouldBeFound("descrizione.specified=true");

        // Get all the formulazioneList where descrizione is null
        defaultFormulazioneShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesBySiglaFormulazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where siglaFormulazione equals to DEFAULT_SIGLA_FORMULAZIONE
        defaultFormulazioneShouldBeFound("siglaFormulazione.equals=" + DEFAULT_SIGLA_FORMULAZIONE);

        // Get all the formulazioneList where siglaFormulazione equals to UPDATED_SIGLA_FORMULAZIONE
        defaultFormulazioneShouldNotBeFound("siglaFormulazione.equals=" + UPDATED_SIGLA_FORMULAZIONE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesBySiglaFormulazioneIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where siglaFormulazione in DEFAULT_SIGLA_FORMULAZIONE or UPDATED_SIGLA_FORMULAZIONE
        defaultFormulazioneShouldBeFound("siglaFormulazione.in=" + DEFAULT_SIGLA_FORMULAZIONE + "," + UPDATED_SIGLA_FORMULAZIONE);

        // Get all the formulazioneList where siglaFormulazione equals to UPDATED_SIGLA_FORMULAZIONE
        defaultFormulazioneShouldNotBeFound("siglaFormulazione.in=" + UPDATED_SIGLA_FORMULAZIONE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesBySiglaFormulazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where siglaFormulazione is not null
        defaultFormulazioneShouldBeFound("siglaFormulazione.specified=true");

        // Get all the formulazioneList where siglaFormulazione is null
        defaultFormulazioneShouldNotBeFound("siglaFormulazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultFormulazioneShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the formulazioneList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFormulazioneShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultFormulazioneShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the formulazioneList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFormulazioneShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where tipoImport is not null
        defaultFormulazioneShouldBeFound("tipoImport.specified=true");

        // Get all the formulazioneList where tipoImport is null
        defaultFormulazioneShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where operatore equals to DEFAULT_OPERATORE
        defaultFormulazioneShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the formulazioneList where operatore equals to UPDATED_OPERATORE
        defaultFormulazioneShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultFormulazioneShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the formulazioneList where operatore equals to UPDATED_OPERATORE
        defaultFormulazioneShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where operatore is not null
        defaultFormulazioneShouldBeFound("operatore.specified=true");

        // Get all the formulazioneList where operatore is null
        defaultFormulazioneShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where ts equals to DEFAULT_TS
        defaultFormulazioneShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the formulazioneList where ts equals to UPDATED_TS
        defaultFormulazioneShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTsIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where ts in DEFAULT_TS or UPDATED_TS
        defaultFormulazioneShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the formulazioneList where ts equals to UPDATED_TS
        defaultFormulazioneShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where ts is not null
        defaultFormulazioneShouldBeFound("ts.specified=true");

        // Get all the formulazioneList where ts is null
        defaultFormulazioneShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali is not null
        defaultFormulazioneShouldBeFound("dataInizVali.specified=true");

        // Get all the formulazioneList where dataInizVali is null
        defaultFormulazioneShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFormulazioneShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the formulazioneList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFormulazioneShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali is not null
        defaultFormulazioneShouldBeFound("dataFineVali.specified=true");

        // Get all the formulazioneList where dataFineVali is null
        defaultFormulazioneShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFormulazioneShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the formulazioneList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFormulazioneShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator is not null
        defaultFormulazioneShouldBeFound("userIdCreator.specified=true");

        // Get all the formulazioneList where userIdCreator is null
        defaultFormulazioneShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFormulazioneShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the formulazioneList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFormulazioneShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod is not null
        defaultFormulazioneShouldBeFound("userIdLastMod.specified=true");

        // Get all the formulazioneList where userIdLastMod is null
        defaultFormulazioneShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFormulazionesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);

        // Get all the formulazioneList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFormulazioneShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the formulazioneList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFormulazioneShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFormulazionesByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        formulazioneRepository.saveAndFlush(formulazione);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        formulazione.addEtichettaFito(etichettaFito);
        formulazioneRepository.saveAndFlush(formulazione);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the formulazioneList where etichettaFito equals to etichettaFitoId
        defaultFormulazioneShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the formulazioneList where etichettaFito equals to etichettaFitoId + 1
        defaultFormulazioneShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFormulazioneShouldBeFound(String filter) throws Exception {
        restFormulazioneMockMvc.perform(get("/api/formulaziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(formulazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].siglaFormulazione").value(hasItem(DEFAULT_SIGLA_FORMULAZIONE)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFormulazioneMockMvc.perform(get("/api/formulaziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFormulazioneShouldNotBeFound(String filter) throws Exception {
        restFormulazioneMockMvc.perform(get("/api/formulaziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFormulazioneMockMvc.perform(get("/api/formulaziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFormulazione() throws Exception {
        // Get the formulazione
        restFormulazioneMockMvc.perform(get("/api/formulaziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFormulazione() throws Exception {
        // Initialize the database
        formulazioneService.save(formulazione);

        int databaseSizeBeforeUpdate = formulazioneRepository.findAll().size();

        // Update the formulazione
        Formulazione updatedFormulazione = formulazioneRepository.findById(formulazione.getId()).get();
        // Disconnect from session so that the updates on updatedFormulazione are not directly saved in db
        em.detach(updatedFormulazione);
        updatedFormulazione
            .descrizione(UPDATED_DESCRIZIONE)
            .siglaFormulazione(UPDATED_SIGLA_FORMULAZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFormulazioneMockMvc.perform(put("/api/formulaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFormulazione)))
            .andExpect(status().isOk());

        // Validate the Formulazione in the database
        List<Formulazione> formulazioneList = formulazioneRepository.findAll();
        assertThat(formulazioneList).hasSize(databaseSizeBeforeUpdate);
        Formulazione testFormulazione = formulazioneList.get(formulazioneList.size() - 1);
        assertThat(testFormulazione.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testFormulazione.getSiglaFormulazione()).isEqualTo(UPDATED_SIGLA_FORMULAZIONE);
        assertThat(testFormulazione.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testFormulazione.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testFormulazione.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testFormulazione.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFormulazione.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFormulazione.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFormulazione.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFormulazione.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFormulazione() throws Exception {
        int databaseSizeBeforeUpdate = formulazioneRepository.findAll().size();

        // Create the Formulazione

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFormulazioneMockMvc.perform(put("/api/formulaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulazione)))
            .andExpect(status().isBadRequest());

        // Validate the Formulazione in the database
        List<Formulazione> formulazioneList = formulazioneRepository.findAll();
        assertThat(formulazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFormulazione() throws Exception {
        // Initialize the database
        formulazioneService.save(formulazione);

        int databaseSizeBeforeDelete = formulazioneRepository.findAll().size();

        // Delete the formulazione
        restFormulazioneMockMvc.perform(delete("/api/formulaziones/{id}", formulazione.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Formulazione> formulazioneList = formulazioneRepository.findAll();
        assertThat(formulazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Formulazione.class);
        Formulazione formulazione1 = new Formulazione();
        formulazione1.setId(1L);
        Formulazione formulazione2 = new Formulazione();
        formulazione2.setId(formulazione1.getId());
        assertThat(formulazione1).isEqualTo(formulazione2);
        formulazione2.setId(2L);
        assertThat(formulazione1).isNotEqualTo(formulazione2);
        formulazione1.setId(null);
        assertThat(formulazione1).isNotEqualTo(formulazione2);
    }
}
