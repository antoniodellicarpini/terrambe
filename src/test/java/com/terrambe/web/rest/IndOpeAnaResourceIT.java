package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndOpeAna;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.IndOpeAnaRepository;
import com.terrambe.service.IndOpeAnaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndOpeAnaCriteria;
import com.terrambe.service.IndOpeAnaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndOpeAnaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndOpeAnaResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndOpeAnaRepository indOpeAnaRepository;

    @Autowired
    private IndOpeAnaService indOpeAnaService;

    @Autowired
    private IndOpeAnaQueryService indOpeAnaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndOpeAnaMockMvc;

    private IndOpeAna indOpeAna;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndOpeAnaResource indOpeAnaResource = new IndOpeAnaResource(indOpeAnaService, indOpeAnaQueryService);
        this.restIndOpeAnaMockMvc = MockMvcBuilders.standaloneSetup(indOpeAnaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndOpeAna createEntity(EntityManager em) {
        IndOpeAna indOpeAna = new IndOpeAna()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indOpeAna;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndOpeAna createUpdatedEntity(EntityManager em) {
        IndOpeAna indOpeAna = new IndOpeAna()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indOpeAna;
    }

    @BeforeEach
    public void initTest() {
        indOpeAna = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndOpeAna() throws Exception {
        int databaseSizeBeforeCreate = indOpeAnaRepository.findAll().size();

        // Create the IndOpeAna
        restIndOpeAnaMockMvc.perform(post("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indOpeAna)))
            .andExpect(status().isCreated());

        // Validate the IndOpeAna in the database
        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeCreate + 1);
        IndOpeAna testIndOpeAna = indOpeAnaList.get(indOpeAnaList.size() - 1);
        assertThat(testIndOpeAna.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndOpeAna.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndOpeAna.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndOpeAna.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndOpeAna.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndOpeAna.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndOpeAna.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndOpeAna.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndOpeAna.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndOpeAna.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndOpeAna.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndOpeAna.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndOpeAna.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndOpeAna.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndOpeAnaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indOpeAnaRepository.findAll().size();

        // Create the IndOpeAna with an existing ID
        indOpeAna.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndOpeAnaMockMvc.perform(post("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndOpeAna in the database
        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indOpeAnaRepository.findAll().size();
        // set the field null
        indOpeAna.setIndirizzo(null);

        // Create the IndOpeAna, which fails.

        restIndOpeAnaMockMvc.perform(post("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indOpeAna)))
            .andExpect(status().isBadRequest());

        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indOpeAnaRepository.findAll().size();
        // set the field null
        indOpeAna.setNazione(null);

        // Create the IndOpeAna, which fails.

        restIndOpeAnaMockMvc.perform(post("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indOpeAna)))
            .andExpect(status().isBadRequest());

        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnas() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndOpeAna() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get the indOpeAna
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas/{id}", indOpeAna.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indOpeAna.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndOpeAnaShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indOpeAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndOpeAnaShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndOpeAnaShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indOpeAnaList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndOpeAnaShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where indirizzo is not null
        defaultIndOpeAnaShouldBeFound("indirizzo.specified=true");

        // Get all the indOpeAnaList where indirizzo is null
        defaultIndOpeAnaShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where cap equals to DEFAULT_CAP
        defaultIndOpeAnaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indOpeAnaList where cap equals to UPDATED_CAP
        defaultIndOpeAnaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndOpeAnaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indOpeAnaList where cap equals to UPDATED_CAP
        defaultIndOpeAnaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where cap is not null
        defaultIndOpeAnaShouldBeFound("cap.specified=true");

        // Get all the indOpeAnaList where cap is null
        defaultIndOpeAnaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where nazione equals to DEFAULT_NAZIONE
        defaultIndOpeAnaShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indOpeAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndOpeAnaShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndOpeAnaShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indOpeAnaList where nazione equals to UPDATED_NAZIONE
        defaultIndOpeAnaShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where nazione is not null
        defaultIndOpeAnaShouldBeFound("nazione.specified=true");

        // Get all the indOpeAnaList where nazione is null
        defaultIndOpeAnaShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where regione equals to DEFAULT_REGIONE
        defaultIndOpeAnaShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indOpeAnaList where regione equals to UPDATED_REGIONE
        defaultIndOpeAnaShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndOpeAnaShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indOpeAnaList where regione equals to UPDATED_REGIONE
        defaultIndOpeAnaShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where regione is not null
        defaultIndOpeAnaShouldBeFound("regione.specified=true");

        // Get all the indOpeAnaList where regione is null
        defaultIndOpeAnaShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where provincia equals to DEFAULT_PROVINCIA
        defaultIndOpeAnaShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indOpeAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndOpeAnaShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndOpeAnaShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indOpeAnaList where provincia equals to UPDATED_PROVINCIA
        defaultIndOpeAnaShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where provincia is not null
        defaultIndOpeAnaShouldBeFound("provincia.specified=true");

        // Get all the indOpeAnaList where provincia is null
        defaultIndOpeAnaShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where comune equals to DEFAULT_COMUNE
        defaultIndOpeAnaShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indOpeAnaList where comune equals to UPDATED_COMUNE
        defaultIndOpeAnaShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndOpeAnaShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indOpeAnaList where comune equals to UPDATED_COMUNE
        defaultIndOpeAnaShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where comune is not null
        defaultIndOpeAnaShouldBeFound("comune.specified=true");

        // Get all the indOpeAnaList where comune is null
        defaultIndOpeAnaShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndOpeAnaShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indOpeAnaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndOpeAnaShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndOpeAnaShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indOpeAnaList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndOpeAnaShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where numeroCiv is not null
        defaultIndOpeAnaShouldBeFound("numeroCiv.specified=true");

        // Get all the indOpeAnaList where numeroCiv is null
        defaultIndOpeAnaShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude equals to DEFAULT_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indOpeAnaList where latitude equals to UPDATED_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indOpeAnaList where latitude equals to UPDATED_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude is not null
        defaultIndOpeAnaShouldBeFound("latitude.specified=true");

        // Get all the indOpeAnaList where latitude is null
        defaultIndOpeAnaShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indOpeAnaList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indOpeAnaList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude is less than DEFAULT_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indOpeAnaList where latitude is less than UPDATED_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where latitude is greater than DEFAULT_LATITUDE
        defaultIndOpeAnaShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indOpeAnaList where latitude is greater than SMALLER_LATITUDE
        defaultIndOpeAnaShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude equals to DEFAULT_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indOpeAnaList where longitude equals to UPDATED_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indOpeAnaList where longitude equals to UPDATED_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude is not null
        defaultIndOpeAnaShouldBeFound("longitude.specified=true");

        // Get all the indOpeAnaList where longitude is null
        defaultIndOpeAnaShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indOpeAnaList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indOpeAnaList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude is less than DEFAULT_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indOpeAnaList where longitude is less than UPDATED_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndOpeAnaShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indOpeAnaList where longitude is greater than SMALLER_LONGITUDE
        defaultIndOpeAnaShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali is not null
        defaultIndOpeAnaShouldBeFound("dataInizVali.specified=true");

        // Get all the indOpeAnaList where dataInizVali is null
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndOpeAnaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indOpeAnaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndOpeAnaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali is not null
        defaultIndOpeAnaShouldBeFound("dataFineVali.specified=true");

        // Get all the indOpeAnaList where dataFineVali is null
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndOpeAnaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indOpeAnaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndOpeAnaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator is not null
        defaultIndOpeAnaShouldBeFound("userIdCreator.specified=true");

        // Get all the indOpeAnaList where userIdCreator is null
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndOpeAnaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indOpeAnaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndOpeAnaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod is not null
        defaultIndOpeAnaShouldBeFound("userIdLastMod.specified=true");

        // Get all the indOpeAnaList where userIdLastMod is null
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndOpeAnasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);

        // Get all the indOpeAnaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indOpeAnaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndOpeAnaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndOpeAnasByIndToOpeAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        indOpeAnaRepository.saveAndFlush(indOpeAna);
        OpeAnagrafica indToOpeAna = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(indToOpeAna);
        em.flush();
        indOpeAna.setIndToOpeAna(indToOpeAna);
        indOpeAnaRepository.saveAndFlush(indOpeAna);
        Long indToOpeAnaId = indToOpeAna.getId();

        // Get all the indOpeAnaList where indToOpeAna equals to indToOpeAnaId
        defaultIndOpeAnaShouldBeFound("indToOpeAnaId.equals=" + indToOpeAnaId);

        // Get all the indOpeAnaList where indToOpeAna equals to indToOpeAnaId + 1
        defaultIndOpeAnaShouldNotBeFound("indToOpeAnaId.equals=" + (indToOpeAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndOpeAnaShouldBeFound(String filter) throws Exception {
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndOpeAnaShouldNotBeFound(String filter) throws Exception {
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndOpeAna() throws Exception {
        // Get the indOpeAna
        restIndOpeAnaMockMvc.perform(get("/api/ind-ope-anas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndOpeAna() throws Exception {
        // Initialize the database
        indOpeAnaService.save(indOpeAna);

        int databaseSizeBeforeUpdate = indOpeAnaRepository.findAll().size();

        // Update the indOpeAna
        IndOpeAna updatedIndOpeAna = indOpeAnaRepository.findById(indOpeAna.getId()).get();
        // Disconnect from session so that the updates on updatedIndOpeAna are not directly saved in db
        em.detach(updatedIndOpeAna);
        updatedIndOpeAna
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndOpeAnaMockMvc.perform(put("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndOpeAna)))
            .andExpect(status().isOk());

        // Validate the IndOpeAna in the database
        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeUpdate);
        IndOpeAna testIndOpeAna = indOpeAnaList.get(indOpeAnaList.size() - 1);
        assertThat(testIndOpeAna.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndOpeAna.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndOpeAna.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndOpeAna.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndOpeAna.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndOpeAna.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndOpeAna.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndOpeAna.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndOpeAna.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndOpeAna.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndOpeAna.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndOpeAna.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndOpeAna.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndOpeAna.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndOpeAna() throws Exception {
        int databaseSizeBeforeUpdate = indOpeAnaRepository.findAll().size();

        // Create the IndOpeAna

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndOpeAnaMockMvc.perform(put("/api/ind-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the IndOpeAna in the database
        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndOpeAna() throws Exception {
        // Initialize the database
        indOpeAnaService.save(indOpeAna);

        int databaseSizeBeforeDelete = indOpeAnaRepository.findAll().size();

        // Delete the indOpeAna
        restIndOpeAnaMockMvc.perform(delete("/api/ind-ope-anas/{id}", indOpeAna.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndOpeAna> indOpeAnaList = indOpeAnaRepository.findAll();
        assertThat(indOpeAnaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndOpeAna.class);
        IndOpeAna indOpeAna1 = new IndOpeAna();
        indOpeAna1.setId(1L);
        IndOpeAna indOpeAna2 = new IndOpeAna();
        indOpeAna2.setId(indOpeAna1.getId());
        assertThat(indOpeAna1).isEqualTo(indOpeAna2);
        indOpeAna2.setId(2L);
        assertThat(indOpeAna1).isNotEqualTo(indOpeAna2);
        indOpeAna1.setId(null);
        assertThat(indOpeAna1).isNotEqualTo(indOpeAna2);
    }
}
