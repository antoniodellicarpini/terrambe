package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.domain.Sito;
import com.terrambe.domain.ColtureBDF;
import com.terrambe.domain.Dosaggio;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.FaseFenologica;
import com.terrambe.domain.TipoDistConc;
import com.terrambe.domain.Avversita;
import com.terrambe.repository.TabellaRaccordoRepository;
import com.terrambe.service.TabellaRaccordoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TabellaRaccordoCriteria;
import com.terrambe.service.TabellaRaccordoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TabellaRaccordoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TabellaRaccordoResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TabellaRaccordoRepository tabellaRaccordoRepository;

    @Autowired
    private TabellaRaccordoService tabellaRaccordoService;

    @Autowired
    private TabellaRaccordoQueryService tabellaRaccordoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTabellaRaccordoMockMvc;

    private TabellaRaccordo tabellaRaccordo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TabellaRaccordoResource tabellaRaccordoResource = new TabellaRaccordoResource(tabellaRaccordoService, tabellaRaccordoQueryService);
        this.restTabellaRaccordoMockMvc = MockMvcBuilders.standaloneSetup(tabellaRaccordoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TabellaRaccordo createEntity(EntityManager em) {
        TabellaRaccordo tabellaRaccordo = new TabellaRaccordo()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tabellaRaccordo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TabellaRaccordo createUpdatedEntity(EntityManager em) {
        TabellaRaccordo tabellaRaccordo = new TabellaRaccordo()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tabellaRaccordo;
    }

    @BeforeEach
    public void initTest() {
        tabellaRaccordo = createEntity(em);
    }

    @Test
    @Transactional
    public void createTabellaRaccordo() throws Exception {
        int databaseSizeBeforeCreate = tabellaRaccordoRepository.findAll().size();

        // Create the TabellaRaccordo
        restTabellaRaccordoMockMvc.perform(post("/api/tabella-raccordos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tabellaRaccordo)))
            .andExpect(status().isCreated());

        // Validate the TabellaRaccordo in the database
        List<TabellaRaccordo> tabellaRaccordoList = tabellaRaccordoRepository.findAll();
        assertThat(tabellaRaccordoList).hasSize(databaseSizeBeforeCreate + 1);
        TabellaRaccordo testTabellaRaccordo = tabellaRaccordoList.get(tabellaRaccordoList.size() - 1);
        assertThat(testTabellaRaccordo.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testTabellaRaccordo.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testTabellaRaccordo.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testTabellaRaccordo.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTabellaRaccordo.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTabellaRaccordo.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTabellaRaccordo.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTabellaRaccordo.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTabellaRaccordoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tabellaRaccordoRepository.findAll().size();

        // Create the TabellaRaccordo with an existing ID
        tabellaRaccordo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTabellaRaccordoMockMvc.perform(post("/api/tabella-raccordos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tabellaRaccordo)))
            .andExpect(status().isBadRequest());

        // Validate the TabellaRaccordo in the database
        List<TabellaRaccordo> tabellaRaccordoList = tabellaRaccordoRepository.findAll();
        assertThat(tabellaRaccordoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordos() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tabellaRaccordo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTabellaRaccordo() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get the tabellaRaccordo
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos/{id}", tabellaRaccordo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tabellaRaccordo.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultTabellaRaccordoShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the tabellaRaccordoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTabellaRaccordoShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultTabellaRaccordoShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the tabellaRaccordoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTabellaRaccordoShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where tipoImport is not null
        defaultTabellaRaccordoShouldBeFound("tipoImport.specified=true");

        // Get all the tabellaRaccordoList where tipoImport is null
        defaultTabellaRaccordoShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where operatore equals to DEFAULT_OPERATORE
        defaultTabellaRaccordoShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the tabellaRaccordoList where operatore equals to UPDATED_OPERATORE
        defaultTabellaRaccordoShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultTabellaRaccordoShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the tabellaRaccordoList where operatore equals to UPDATED_OPERATORE
        defaultTabellaRaccordoShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where operatore is not null
        defaultTabellaRaccordoShouldBeFound("operatore.specified=true");

        // Get all the tabellaRaccordoList where operatore is null
        defaultTabellaRaccordoShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where ts equals to DEFAULT_TS
        defaultTabellaRaccordoShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the tabellaRaccordoList where ts equals to UPDATED_TS
        defaultTabellaRaccordoShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where ts in DEFAULT_TS or UPDATED_TS
        defaultTabellaRaccordoShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the tabellaRaccordoList where ts equals to UPDATED_TS
        defaultTabellaRaccordoShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where ts is not null
        defaultTabellaRaccordoShouldBeFound("ts.specified=true");

        // Get all the tabellaRaccordoList where ts is null
        defaultTabellaRaccordoShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali is not null
        defaultTabellaRaccordoShouldBeFound("dataInizVali.specified=true");

        // Get all the tabellaRaccordoList where dataInizVali is null
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tabellaRaccordoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTabellaRaccordoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali is not null
        defaultTabellaRaccordoShouldBeFound("dataFineVali.specified=true");

        // Get all the tabellaRaccordoList where dataFineVali is null
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTabellaRaccordoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tabellaRaccordoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTabellaRaccordoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator is not null
        defaultTabellaRaccordoShouldBeFound("userIdCreator.specified=true");

        // Get all the tabellaRaccordoList where userIdCreator is null
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTabellaRaccordoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tabellaRaccordoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTabellaRaccordoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod is not null
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.specified=true");

        // Get all the tabellaRaccordoList where userIdLastMod is null
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTabellaRaccordosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);

        // Get all the tabellaRaccordoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tabellaRaccordoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTabellaRaccordoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToSitoIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Sito raccordoToSito = SitoResourceIT.createEntity(em);
        em.persist(raccordoToSito);
        em.flush();
        tabellaRaccordo.setRaccordoToSito(raccordoToSito);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToSitoId = raccordoToSito.getId();

        // Get all the tabellaRaccordoList where raccordoToSito equals to raccordoToSitoId
        defaultTabellaRaccordoShouldBeFound("raccordoToSitoId.equals=" + raccordoToSitoId);

        // Get all the tabellaRaccordoList where raccordoToSito equals to raccordoToSitoId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToSitoId.equals=" + (raccordoToSitoId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToColtBdfIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        ColtureBDF raccordoToColtBdf = ColtureBDFResourceIT.createEntity(em);
        em.persist(raccordoToColtBdf);
        em.flush();
        tabellaRaccordo.setRaccordoToColtBdf(raccordoToColtBdf);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToColtBdfId = raccordoToColtBdf.getId();

        // Get all the tabellaRaccordoList where raccordoToColtBdf equals to raccordoToColtBdfId
        defaultTabellaRaccordoShouldBeFound("raccordoToColtBdfId.equals=" + raccordoToColtBdfId);

        // Get all the tabellaRaccordoList where raccordoToColtBdf equals to raccordoToColtBdfId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToColtBdfId.equals=" + (raccordoToColtBdfId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToDosagIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Dosaggio raccordoToDosag = DosaggioResourceIT.createEntity(em);
        em.persist(raccordoToDosag);
        em.flush();
        tabellaRaccordo.setRaccordoToDosag(raccordoToDosag);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToDosagId = raccordoToDosag.getId();

        // Get all the tabellaRaccordoList where raccordoToDosag equals to raccordoToDosagId
        defaultTabellaRaccordoShouldBeFound("raccordoToDosagId.equals=" + raccordoToDosagId);

        // Get all the tabellaRaccordoList where raccordoToDosag equals to raccordoToDosagId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToDosagId.equals=" + (raccordoToDosagId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToEtiFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        EtichettaFito raccordoToEtiFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(raccordoToEtiFito);
        em.flush();
        tabellaRaccordo.setRaccordoToEtiFito(raccordoToEtiFito);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToEtiFitoId = raccordoToEtiFito.getId();

        // Get all the tabellaRaccordoList where raccordoToEtiFito equals to raccordoToEtiFitoId
        defaultTabellaRaccordoShouldBeFound("raccordoToEtiFitoId.equals=" + raccordoToEtiFitoId);

        // Get all the tabellaRaccordoList where raccordoToEtiFito equals to raccordoToEtiFitoId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToEtiFitoId.equals=" + (raccordoToEtiFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToFaseFenoIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        FaseFenologica raccordoToFaseFeno = FaseFenologicaResourceIT.createEntity(em);
        em.persist(raccordoToFaseFeno);
        em.flush();
        tabellaRaccordo.setRaccordoToFaseFeno(raccordoToFaseFeno);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToFaseFenoId = raccordoToFaseFeno.getId();

        // Get all the tabellaRaccordoList where raccordoToFaseFeno equals to raccordoToFaseFenoId
        defaultTabellaRaccordoShouldBeFound("raccordoToFaseFenoId.equals=" + raccordoToFaseFenoId);

        // Get all the tabellaRaccordoList where raccordoToFaseFeno equals to raccordoToFaseFenoId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToFaseFenoId.equals=" + (raccordoToFaseFenoId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToTipoDistrIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        TipoDistConc raccordoToTipoDistr = TipoDistConcResourceIT.createEntity(em);
        em.persist(raccordoToTipoDistr);
        em.flush();
        tabellaRaccordo.setRaccordoToTipoDistr(raccordoToTipoDistr);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToTipoDistrId = raccordoToTipoDistr.getId();

        // Get all the tabellaRaccordoList where raccordoToTipoDistr equals to raccordoToTipoDistrId
        defaultTabellaRaccordoShouldBeFound("raccordoToTipoDistrId.equals=" + raccordoToTipoDistrId);

        // Get all the tabellaRaccordoList where raccordoToTipoDistr equals to raccordoToTipoDistrId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToTipoDistrId.equals=" + (raccordoToTipoDistrId + 1));
    }


    @Test
    @Transactional
    public void getAllTabellaRaccordosByRaccordoToAvversIsEqualToSomething() throws Exception {
        // Initialize the database
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Avversita raccordoToAvvers = AvversitaResourceIT.createEntity(em);
        em.persist(raccordoToAvvers);
        em.flush();
        tabellaRaccordo.setRaccordoToAvvers(raccordoToAvvers);
        tabellaRaccordoRepository.saveAndFlush(tabellaRaccordo);
        Long raccordoToAvversId = raccordoToAvvers.getId();

        // Get all the tabellaRaccordoList where raccordoToAvvers equals to raccordoToAvversId
        defaultTabellaRaccordoShouldBeFound("raccordoToAvversId.equals=" + raccordoToAvversId);

        // Get all the tabellaRaccordoList where raccordoToAvvers equals to raccordoToAvversId + 1
        defaultTabellaRaccordoShouldNotBeFound("raccordoToAvversId.equals=" + (raccordoToAvversId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTabellaRaccordoShouldBeFound(String filter) throws Exception {
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tabellaRaccordo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTabellaRaccordoShouldNotBeFound(String filter) throws Exception {
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTabellaRaccordo() throws Exception {
        // Get the tabellaRaccordo
        restTabellaRaccordoMockMvc.perform(get("/api/tabella-raccordos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTabellaRaccordo() throws Exception {
        // Initialize the database
        tabellaRaccordoService.save(tabellaRaccordo);

        int databaseSizeBeforeUpdate = tabellaRaccordoRepository.findAll().size();

        // Update the tabellaRaccordo
        TabellaRaccordo updatedTabellaRaccordo = tabellaRaccordoRepository.findById(tabellaRaccordo.getId()).get();
        // Disconnect from session so that the updates on updatedTabellaRaccordo are not directly saved in db
        em.detach(updatedTabellaRaccordo);
        updatedTabellaRaccordo
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTabellaRaccordoMockMvc.perform(put("/api/tabella-raccordos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTabellaRaccordo)))
            .andExpect(status().isOk());

        // Validate the TabellaRaccordo in the database
        List<TabellaRaccordo> tabellaRaccordoList = tabellaRaccordoRepository.findAll();
        assertThat(tabellaRaccordoList).hasSize(databaseSizeBeforeUpdate);
        TabellaRaccordo testTabellaRaccordo = tabellaRaccordoList.get(tabellaRaccordoList.size() - 1);
        assertThat(testTabellaRaccordo.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testTabellaRaccordo.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testTabellaRaccordo.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testTabellaRaccordo.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTabellaRaccordo.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTabellaRaccordo.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTabellaRaccordo.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTabellaRaccordo.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTabellaRaccordo() throws Exception {
        int databaseSizeBeforeUpdate = tabellaRaccordoRepository.findAll().size();

        // Create the TabellaRaccordo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTabellaRaccordoMockMvc.perform(put("/api/tabella-raccordos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tabellaRaccordo)))
            .andExpect(status().isBadRequest());

        // Validate the TabellaRaccordo in the database
        List<TabellaRaccordo> tabellaRaccordoList = tabellaRaccordoRepository.findAll();
        assertThat(tabellaRaccordoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTabellaRaccordo() throws Exception {
        // Initialize the database
        tabellaRaccordoService.save(tabellaRaccordo);

        int databaseSizeBeforeDelete = tabellaRaccordoRepository.findAll().size();

        // Delete the tabellaRaccordo
        restTabellaRaccordoMockMvc.perform(delete("/api/tabella-raccordos/{id}", tabellaRaccordo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TabellaRaccordo> tabellaRaccordoList = tabellaRaccordoRepository.findAll();
        assertThat(tabellaRaccordoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TabellaRaccordo.class);
        TabellaRaccordo tabellaRaccordo1 = new TabellaRaccordo();
        tabellaRaccordo1.setId(1L);
        TabellaRaccordo tabellaRaccordo2 = new TabellaRaccordo();
        tabellaRaccordo2.setId(tabellaRaccordo1.getId());
        assertThat(tabellaRaccordo1).isEqualTo(tabellaRaccordo2);
        tabellaRaccordo2.setId(2L);
        assertThat(tabellaRaccordo1).isNotEqualTo(tabellaRaccordo2);
        tabellaRaccordo1.setId(null);
        assertThat(tabellaRaccordo1).isNotEqualTo(tabellaRaccordo2);
    }
}
