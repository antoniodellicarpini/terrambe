package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Dosaggio;
import com.terrambe.domain.Dose;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.DosaggioRepository;
import com.terrambe.service.DosaggioService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DosaggioCriteria;
import com.terrambe.service.DosaggioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DosaggioResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DosaggioResourceIT {

    private static final String DEFAULT_CARENZA_C = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_C = "BBBBBBBBBB";

    private static final String DEFAULT_CARENZA_P = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_P = "BBBBBBBBBB";

    private static final String DEFAULT_CARENZA_D = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_D = "BBBBBBBBBB";

    private static final String DEFAULT_CARENZA_O = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_O = "BBBBBBBBBB";

    private static final String DEFAULT_CARENZA_T = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_T = "BBBBBBBBBB";

    private static final String DEFAULT_CARENZA_ND = "AAAAAAAAAA";
    private static final String UPDATED_CARENZA_ND = "BBBBBBBBBB";

    private static final String DEFAULT_ACQUA_HA_MIN = "AAAAAAAAAA";
    private static final String UPDATED_ACQUA_HA_MIN = "BBBBBBBBBB";

    private static final String DEFAULT_ACQUA_HA_MAX = "AAAAAAAAAA";
    private static final String UPDATED_ACQUA_HA_MAX = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_MAX_INT = "AAAAAAAAAA";
    private static final String UPDATED_NUM_MAX_INT = "BBBBBBBBBB";

    private static final String DEFAULT_RIF_MAX_TRATT = "AAAAAAAAAA";
    private static final String UPDATED_RIF_MAX_TRATT = "BBBBBBBBBB";

    private static final String DEFAULT_INTERV_TRATT = "AAAAAAAAAA";
    private static final String UPDATED_INTERV_TRATT = "BBBBBBBBBB";

    private static final String DEFAULT_INTERV_TRATT_MAX = "AAAAAAAAAA";
    private static final String UPDATED_INTERV_TRATT_MAX = "BBBBBBBBBB";

    private static final String DEFAULT_SCADENZA_DOSI = "AAAAAAAAAA";
    private static final String UPDATED_SCADENZA_DOSI = "BBBBBBBBBB";

    private static final String DEFAULT_EPOCA_INTERVENTO = "AAAAAAAAAA";
    private static final String UPDATED_EPOCA_INTERVENTO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DosaggioRepository dosaggioRepository;

    @Autowired
    private DosaggioService dosaggioService;

    @Autowired
    private DosaggioQueryService dosaggioQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDosaggioMockMvc;

    private Dosaggio dosaggio;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DosaggioResource dosaggioResource = new DosaggioResource(dosaggioService, dosaggioQueryService);
        this.restDosaggioMockMvc = MockMvcBuilders.standaloneSetup(dosaggioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dosaggio createEntity(EntityManager em) {
        Dosaggio dosaggio = new Dosaggio()
            .carenzaC(DEFAULT_CARENZA_C)
            .carenzaP(DEFAULT_CARENZA_P)
            .carenzaD(DEFAULT_CARENZA_D)
            .carenzaO(DEFAULT_CARENZA_O)
            .carenzaT(DEFAULT_CARENZA_T)
            .carenzaNd(DEFAULT_CARENZA_ND)
            .acquaHaMin(DEFAULT_ACQUA_HA_MIN)
            .acquaHaMax(DEFAULT_ACQUA_HA_MAX)
            .numMaxInt(DEFAULT_NUM_MAX_INT)
            .rifMaxTratt(DEFAULT_RIF_MAX_TRATT)
            .intervTratt(DEFAULT_INTERV_TRATT)
            .intervTrattMax(DEFAULT_INTERV_TRATT_MAX)
            .scadenzaDosi(DEFAULT_SCADENZA_DOSI)
            .epocaIntervento(DEFAULT_EPOCA_INTERVENTO)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dosaggio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dosaggio createUpdatedEntity(EntityManager em) {
        Dosaggio dosaggio = new Dosaggio()
            .carenzaC(UPDATED_CARENZA_C)
            .carenzaP(UPDATED_CARENZA_P)
            .carenzaD(UPDATED_CARENZA_D)
            .carenzaO(UPDATED_CARENZA_O)
            .carenzaT(UPDATED_CARENZA_T)
            .carenzaNd(UPDATED_CARENZA_ND)
            .acquaHaMin(UPDATED_ACQUA_HA_MIN)
            .acquaHaMax(UPDATED_ACQUA_HA_MAX)
            .numMaxInt(UPDATED_NUM_MAX_INT)
            .rifMaxTratt(UPDATED_RIF_MAX_TRATT)
            .intervTratt(UPDATED_INTERV_TRATT)
            .intervTrattMax(UPDATED_INTERV_TRATT_MAX)
            .scadenzaDosi(UPDATED_SCADENZA_DOSI)
            .epocaIntervento(UPDATED_EPOCA_INTERVENTO)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dosaggio;
    }

    @BeforeEach
    public void initTest() {
        dosaggio = createEntity(em);
    }

    @Test
    @Transactional
    public void createDosaggio() throws Exception {
        int databaseSizeBeforeCreate = dosaggioRepository.findAll().size();

        // Create the Dosaggio
        restDosaggioMockMvc.perform(post("/api/dosaggios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggio)))
            .andExpect(status().isCreated());

        // Validate the Dosaggio in the database
        List<Dosaggio> dosaggioList = dosaggioRepository.findAll();
        assertThat(dosaggioList).hasSize(databaseSizeBeforeCreate + 1);
        Dosaggio testDosaggio = dosaggioList.get(dosaggioList.size() - 1);
        assertThat(testDosaggio.getCarenzaC()).isEqualTo(DEFAULT_CARENZA_C);
        assertThat(testDosaggio.getCarenzaP()).isEqualTo(DEFAULT_CARENZA_P);
        assertThat(testDosaggio.getCarenzaD()).isEqualTo(DEFAULT_CARENZA_D);
        assertThat(testDosaggio.getCarenzaO()).isEqualTo(DEFAULT_CARENZA_O);
        assertThat(testDosaggio.getCarenzaT()).isEqualTo(DEFAULT_CARENZA_T);
        assertThat(testDosaggio.getCarenzaNd()).isEqualTo(DEFAULT_CARENZA_ND);
        assertThat(testDosaggio.getAcquaHaMin()).isEqualTo(DEFAULT_ACQUA_HA_MIN);
        assertThat(testDosaggio.getAcquaHaMax()).isEqualTo(DEFAULT_ACQUA_HA_MAX);
        assertThat(testDosaggio.getNumMaxInt()).isEqualTo(DEFAULT_NUM_MAX_INT);
        assertThat(testDosaggio.getRifMaxTratt()).isEqualTo(DEFAULT_RIF_MAX_TRATT);
        assertThat(testDosaggio.getIntervTratt()).isEqualTo(DEFAULT_INTERV_TRATT);
        assertThat(testDosaggio.getIntervTrattMax()).isEqualTo(DEFAULT_INTERV_TRATT_MAX);
        assertThat(testDosaggio.getScadenzaDosi()).isEqualTo(DEFAULT_SCADENZA_DOSI);
        assertThat(testDosaggio.getEpocaIntervento()).isEqualTo(DEFAULT_EPOCA_INTERVENTO);
        assertThat(testDosaggio.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testDosaggio.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testDosaggio.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testDosaggio.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDosaggio.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDosaggio.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDosaggio.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDosaggio.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDosaggioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dosaggioRepository.findAll().size();

        // Create the Dosaggio with an existing ID
        dosaggio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDosaggioMockMvc.perform(post("/api/dosaggios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggio)))
            .andExpect(status().isBadRequest());

        // Validate the Dosaggio in the database
        List<Dosaggio> dosaggioList = dosaggioRepository.findAll();
        assertThat(dosaggioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDosaggios() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList
        restDosaggioMockMvc.perform(get("/api/dosaggios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosaggio.getId().intValue())))
            .andExpect(jsonPath("$.[*].carenzaC").value(hasItem(DEFAULT_CARENZA_C.toString())))
            .andExpect(jsonPath("$.[*].carenzaP").value(hasItem(DEFAULT_CARENZA_P.toString())))
            .andExpect(jsonPath("$.[*].carenzaD").value(hasItem(DEFAULT_CARENZA_D.toString())))
            .andExpect(jsonPath("$.[*].carenzaO").value(hasItem(DEFAULT_CARENZA_O.toString())))
            .andExpect(jsonPath("$.[*].carenzaT").value(hasItem(DEFAULT_CARENZA_T.toString())))
            .andExpect(jsonPath("$.[*].carenzaNd").value(hasItem(DEFAULT_CARENZA_ND.toString())))
            .andExpect(jsonPath("$.[*].acquaHaMin").value(hasItem(DEFAULT_ACQUA_HA_MIN.toString())))
            .andExpect(jsonPath("$.[*].acquaHaMax").value(hasItem(DEFAULT_ACQUA_HA_MAX.toString())))
            .andExpect(jsonPath("$.[*].numMaxInt").value(hasItem(DEFAULT_NUM_MAX_INT.toString())))
            .andExpect(jsonPath("$.[*].rifMaxTratt").value(hasItem(DEFAULT_RIF_MAX_TRATT.toString())))
            .andExpect(jsonPath("$.[*].intervTratt").value(hasItem(DEFAULT_INTERV_TRATT.toString())))
            .andExpect(jsonPath("$.[*].intervTrattMax").value(hasItem(DEFAULT_INTERV_TRATT_MAX.toString())))
            .andExpect(jsonPath("$.[*].scadenzaDosi").value(hasItem(DEFAULT_SCADENZA_DOSI.toString())))
            .andExpect(jsonPath("$.[*].epocaIntervento").value(hasItem(DEFAULT_EPOCA_INTERVENTO.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDosaggio() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get the dosaggio
        restDosaggioMockMvc.perform(get("/api/dosaggios/{id}", dosaggio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dosaggio.getId().intValue()))
            .andExpect(jsonPath("$.carenzaC").value(DEFAULT_CARENZA_C.toString()))
            .andExpect(jsonPath("$.carenzaP").value(DEFAULT_CARENZA_P.toString()))
            .andExpect(jsonPath("$.carenzaD").value(DEFAULT_CARENZA_D.toString()))
            .andExpect(jsonPath("$.carenzaO").value(DEFAULT_CARENZA_O.toString()))
            .andExpect(jsonPath("$.carenzaT").value(DEFAULT_CARENZA_T.toString()))
            .andExpect(jsonPath("$.carenzaNd").value(DEFAULT_CARENZA_ND.toString()))
            .andExpect(jsonPath("$.acquaHaMin").value(DEFAULT_ACQUA_HA_MIN.toString()))
            .andExpect(jsonPath("$.acquaHaMax").value(DEFAULT_ACQUA_HA_MAX.toString()))
            .andExpect(jsonPath("$.numMaxInt").value(DEFAULT_NUM_MAX_INT.toString()))
            .andExpect(jsonPath("$.rifMaxTratt").value(DEFAULT_RIF_MAX_TRATT.toString()))
            .andExpect(jsonPath("$.intervTratt").value(DEFAULT_INTERV_TRATT.toString()))
            .andExpect(jsonPath("$.intervTrattMax").value(DEFAULT_INTERV_TRATT_MAX.toString()))
            .andExpect(jsonPath("$.scadenzaDosi").value(DEFAULT_SCADENZA_DOSI.toString()))
            .andExpect(jsonPath("$.epocaIntervento").value(DEFAULT_EPOCA_INTERVENTO.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaCIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaC equals to DEFAULT_CARENZA_C
        defaultDosaggioShouldBeFound("carenzaC.equals=" + DEFAULT_CARENZA_C);

        // Get all the dosaggioList where carenzaC equals to UPDATED_CARENZA_C
        defaultDosaggioShouldNotBeFound("carenzaC.equals=" + UPDATED_CARENZA_C);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaCIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaC in DEFAULT_CARENZA_C or UPDATED_CARENZA_C
        defaultDosaggioShouldBeFound("carenzaC.in=" + DEFAULT_CARENZA_C + "," + UPDATED_CARENZA_C);

        // Get all the dosaggioList where carenzaC equals to UPDATED_CARENZA_C
        defaultDosaggioShouldNotBeFound("carenzaC.in=" + UPDATED_CARENZA_C);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaCIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaC is not null
        defaultDosaggioShouldBeFound("carenzaC.specified=true");

        // Get all the dosaggioList where carenzaC is null
        defaultDosaggioShouldNotBeFound("carenzaC.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaPIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaP equals to DEFAULT_CARENZA_P
        defaultDosaggioShouldBeFound("carenzaP.equals=" + DEFAULT_CARENZA_P);

        // Get all the dosaggioList where carenzaP equals to UPDATED_CARENZA_P
        defaultDosaggioShouldNotBeFound("carenzaP.equals=" + UPDATED_CARENZA_P);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaPIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaP in DEFAULT_CARENZA_P or UPDATED_CARENZA_P
        defaultDosaggioShouldBeFound("carenzaP.in=" + DEFAULT_CARENZA_P + "," + UPDATED_CARENZA_P);

        // Get all the dosaggioList where carenzaP equals to UPDATED_CARENZA_P
        defaultDosaggioShouldNotBeFound("carenzaP.in=" + UPDATED_CARENZA_P);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaPIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaP is not null
        defaultDosaggioShouldBeFound("carenzaP.specified=true");

        // Get all the dosaggioList where carenzaP is null
        defaultDosaggioShouldNotBeFound("carenzaP.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaDIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaD equals to DEFAULT_CARENZA_D
        defaultDosaggioShouldBeFound("carenzaD.equals=" + DEFAULT_CARENZA_D);

        // Get all the dosaggioList where carenzaD equals to UPDATED_CARENZA_D
        defaultDosaggioShouldNotBeFound("carenzaD.equals=" + UPDATED_CARENZA_D);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaDIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaD in DEFAULT_CARENZA_D or UPDATED_CARENZA_D
        defaultDosaggioShouldBeFound("carenzaD.in=" + DEFAULT_CARENZA_D + "," + UPDATED_CARENZA_D);

        // Get all the dosaggioList where carenzaD equals to UPDATED_CARENZA_D
        defaultDosaggioShouldNotBeFound("carenzaD.in=" + UPDATED_CARENZA_D);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaDIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaD is not null
        defaultDosaggioShouldBeFound("carenzaD.specified=true");

        // Get all the dosaggioList where carenzaD is null
        defaultDosaggioShouldNotBeFound("carenzaD.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaOIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaO equals to DEFAULT_CARENZA_O
        defaultDosaggioShouldBeFound("carenzaO.equals=" + DEFAULT_CARENZA_O);

        // Get all the dosaggioList where carenzaO equals to UPDATED_CARENZA_O
        defaultDosaggioShouldNotBeFound("carenzaO.equals=" + UPDATED_CARENZA_O);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaOIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaO in DEFAULT_CARENZA_O or UPDATED_CARENZA_O
        defaultDosaggioShouldBeFound("carenzaO.in=" + DEFAULT_CARENZA_O + "," + UPDATED_CARENZA_O);

        // Get all the dosaggioList where carenzaO equals to UPDATED_CARENZA_O
        defaultDosaggioShouldNotBeFound("carenzaO.in=" + UPDATED_CARENZA_O);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaOIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaO is not null
        defaultDosaggioShouldBeFound("carenzaO.specified=true");

        // Get all the dosaggioList where carenzaO is null
        defaultDosaggioShouldNotBeFound("carenzaO.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaTIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaT equals to DEFAULT_CARENZA_T
        defaultDosaggioShouldBeFound("carenzaT.equals=" + DEFAULT_CARENZA_T);

        // Get all the dosaggioList where carenzaT equals to UPDATED_CARENZA_T
        defaultDosaggioShouldNotBeFound("carenzaT.equals=" + UPDATED_CARENZA_T);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaTIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaT in DEFAULT_CARENZA_T or UPDATED_CARENZA_T
        defaultDosaggioShouldBeFound("carenzaT.in=" + DEFAULT_CARENZA_T + "," + UPDATED_CARENZA_T);

        // Get all the dosaggioList where carenzaT equals to UPDATED_CARENZA_T
        defaultDosaggioShouldNotBeFound("carenzaT.in=" + UPDATED_CARENZA_T);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaTIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaT is not null
        defaultDosaggioShouldBeFound("carenzaT.specified=true");

        // Get all the dosaggioList where carenzaT is null
        defaultDosaggioShouldNotBeFound("carenzaT.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaNdIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaNd equals to DEFAULT_CARENZA_ND
        defaultDosaggioShouldBeFound("carenzaNd.equals=" + DEFAULT_CARENZA_ND);

        // Get all the dosaggioList where carenzaNd equals to UPDATED_CARENZA_ND
        defaultDosaggioShouldNotBeFound("carenzaNd.equals=" + UPDATED_CARENZA_ND);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaNdIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaNd in DEFAULT_CARENZA_ND or UPDATED_CARENZA_ND
        defaultDosaggioShouldBeFound("carenzaNd.in=" + DEFAULT_CARENZA_ND + "," + UPDATED_CARENZA_ND);

        // Get all the dosaggioList where carenzaNd equals to UPDATED_CARENZA_ND
        defaultDosaggioShouldNotBeFound("carenzaNd.in=" + UPDATED_CARENZA_ND);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByCarenzaNdIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where carenzaNd is not null
        defaultDosaggioShouldBeFound("carenzaNd.specified=true");

        // Get all the dosaggioList where carenzaNd is null
        defaultDosaggioShouldNotBeFound("carenzaNd.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMinIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMin equals to DEFAULT_ACQUA_HA_MIN
        defaultDosaggioShouldBeFound("acquaHaMin.equals=" + DEFAULT_ACQUA_HA_MIN);

        // Get all the dosaggioList where acquaHaMin equals to UPDATED_ACQUA_HA_MIN
        defaultDosaggioShouldNotBeFound("acquaHaMin.equals=" + UPDATED_ACQUA_HA_MIN);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMinIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMin in DEFAULT_ACQUA_HA_MIN or UPDATED_ACQUA_HA_MIN
        defaultDosaggioShouldBeFound("acquaHaMin.in=" + DEFAULT_ACQUA_HA_MIN + "," + UPDATED_ACQUA_HA_MIN);

        // Get all the dosaggioList where acquaHaMin equals to UPDATED_ACQUA_HA_MIN
        defaultDosaggioShouldNotBeFound("acquaHaMin.in=" + UPDATED_ACQUA_HA_MIN);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMin is not null
        defaultDosaggioShouldBeFound("acquaHaMin.specified=true");

        // Get all the dosaggioList where acquaHaMin is null
        defaultDosaggioShouldNotBeFound("acquaHaMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMax equals to DEFAULT_ACQUA_HA_MAX
        defaultDosaggioShouldBeFound("acquaHaMax.equals=" + DEFAULT_ACQUA_HA_MAX);

        // Get all the dosaggioList where acquaHaMax equals to UPDATED_ACQUA_HA_MAX
        defaultDosaggioShouldNotBeFound("acquaHaMax.equals=" + UPDATED_ACQUA_HA_MAX);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMaxIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMax in DEFAULT_ACQUA_HA_MAX or UPDATED_ACQUA_HA_MAX
        defaultDosaggioShouldBeFound("acquaHaMax.in=" + DEFAULT_ACQUA_HA_MAX + "," + UPDATED_ACQUA_HA_MAX);

        // Get all the dosaggioList where acquaHaMax equals to UPDATED_ACQUA_HA_MAX
        defaultDosaggioShouldNotBeFound("acquaHaMax.in=" + UPDATED_ACQUA_HA_MAX);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByAcquaHaMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where acquaHaMax is not null
        defaultDosaggioShouldBeFound("acquaHaMax.specified=true");

        // Get all the dosaggioList where acquaHaMax is null
        defaultDosaggioShouldNotBeFound("acquaHaMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByNumMaxIntIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where numMaxInt equals to DEFAULT_NUM_MAX_INT
        defaultDosaggioShouldBeFound("numMaxInt.equals=" + DEFAULT_NUM_MAX_INT);

        // Get all the dosaggioList where numMaxInt equals to UPDATED_NUM_MAX_INT
        defaultDosaggioShouldNotBeFound("numMaxInt.equals=" + UPDATED_NUM_MAX_INT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByNumMaxIntIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where numMaxInt in DEFAULT_NUM_MAX_INT or UPDATED_NUM_MAX_INT
        defaultDosaggioShouldBeFound("numMaxInt.in=" + DEFAULT_NUM_MAX_INT + "," + UPDATED_NUM_MAX_INT);

        // Get all the dosaggioList where numMaxInt equals to UPDATED_NUM_MAX_INT
        defaultDosaggioShouldNotBeFound("numMaxInt.in=" + UPDATED_NUM_MAX_INT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByNumMaxIntIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where numMaxInt is not null
        defaultDosaggioShouldBeFound("numMaxInt.specified=true");

        // Get all the dosaggioList where numMaxInt is null
        defaultDosaggioShouldNotBeFound("numMaxInt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByRifMaxTrattIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where rifMaxTratt equals to DEFAULT_RIF_MAX_TRATT
        defaultDosaggioShouldBeFound("rifMaxTratt.equals=" + DEFAULT_RIF_MAX_TRATT);

        // Get all the dosaggioList where rifMaxTratt equals to UPDATED_RIF_MAX_TRATT
        defaultDosaggioShouldNotBeFound("rifMaxTratt.equals=" + UPDATED_RIF_MAX_TRATT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByRifMaxTrattIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where rifMaxTratt in DEFAULT_RIF_MAX_TRATT or UPDATED_RIF_MAX_TRATT
        defaultDosaggioShouldBeFound("rifMaxTratt.in=" + DEFAULT_RIF_MAX_TRATT + "," + UPDATED_RIF_MAX_TRATT);

        // Get all the dosaggioList where rifMaxTratt equals to UPDATED_RIF_MAX_TRATT
        defaultDosaggioShouldNotBeFound("rifMaxTratt.in=" + UPDATED_RIF_MAX_TRATT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByRifMaxTrattIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where rifMaxTratt is not null
        defaultDosaggioShouldBeFound("rifMaxTratt.specified=true");

        // Get all the dosaggioList where rifMaxTratt is null
        defaultDosaggioShouldNotBeFound("rifMaxTratt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTratt equals to DEFAULT_INTERV_TRATT
        defaultDosaggioShouldBeFound("intervTratt.equals=" + DEFAULT_INTERV_TRATT);

        // Get all the dosaggioList where intervTratt equals to UPDATED_INTERV_TRATT
        defaultDosaggioShouldNotBeFound("intervTratt.equals=" + UPDATED_INTERV_TRATT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTratt in DEFAULT_INTERV_TRATT or UPDATED_INTERV_TRATT
        defaultDosaggioShouldBeFound("intervTratt.in=" + DEFAULT_INTERV_TRATT + "," + UPDATED_INTERV_TRATT);

        // Get all the dosaggioList where intervTratt equals to UPDATED_INTERV_TRATT
        defaultDosaggioShouldNotBeFound("intervTratt.in=" + UPDATED_INTERV_TRATT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTratt is not null
        defaultDosaggioShouldBeFound("intervTratt.specified=true");

        // Get all the dosaggioList where intervTratt is null
        defaultDosaggioShouldNotBeFound("intervTratt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTrattMax equals to DEFAULT_INTERV_TRATT_MAX
        defaultDosaggioShouldBeFound("intervTrattMax.equals=" + DEFAULT_INTERV_TRATT_MAX);

        // Get all the dosaggioList where intervTrattMax equals to UPDATED_INTERV_TRATT_MAX
        defaultDosaggioShouldNotBeFound("intervTrattMax.equals=" + UPDATED_INTERV_TRATT_MAX);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattMaxIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTrattMax in DEFAULT_INTERV_TRATT_MAX or UPDATED_INTERV_TRATT_MAX
        defaultDosaggioShouldBeFound("intervTrattMax.in=" + DEFAULT_INTERV_TRATT_MAX + "," + UPDATED_INTERV_TRATT_MAX);

        // Get all the dosaggioList where intervTrattMax equals to UPDATED_INTERV_TRATT_MAX
        defaultDosaggioShouldNotBeFound("intervTrattMax.in=" + UPDATED_INTERV_TRATT_MAX);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByIntervTrattMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where intervTrattMax is not null
        defaultDosaggioShouldBeFound("intervTrattMax.specified=true");

        // Get all the dosaggioList where intervTrattMax is null
        defaultDosaggioShouldNotBeFound("intervTrattMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByScadenzaDosiIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where scadenzaDosi equals to DEFAULT_SCADENZA_DOSI
        defaultDosaggioShouldBeFound("scadenzaDosi.equals=" + DEFAULT_SCADENZA_DOSI);

        // Get all the dosaggioList where scadenzaDosi equals to UPDATED_SCADENZA_DOSI
        defaultDosaggioShouldNotBeFound("scadenzaDosi.equals=" + UPDATED_SCADENZA_DOSI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByScadenzaDosiIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where scadenzaDosi in DEFAULT_SCADENZA_DOSI or UPDATED_SCADENZA_DOSI
        defaultDosaggioShouldBeFound("scadenzaDosi.in=" + DEFAULT_SCADENZA_DOSI + "," + UPDATED_SCADENZA_DOSI);

        // Get all the dosaggioList where scadenzaDosi equals to UPDATED_SCADENZA_DOSI
        defaultDosaggioShouldNotBeFound("scadenzaDosi.in=" + UPDATED_SCADENZA_DOSI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByScadenzaDosiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where scadenzaDosi is not null
        defaultDosaggioShouldBeFound("scadenzaDosi.specified=true");

        // Get all the dosaggioList where scadenzaDosi is null
        defaultDosaggioShouldNotBeFound("scadenzaDosi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultDosaggioShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the dosaggioList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultDosaggioShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultDosaggioShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the dosaggioList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultDosaggioShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where tipoImport is not null
        defaultDosaggioShouldBeFound("tipoImport.specified=true");

        // Get all the dosaggioList where tipoImport is null
        defaultDosaggioShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where operatore equals to DEFAULT_OPERATORE
        defaultDosaggioShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the dosaggioList where operatore equals to UPDATED_OPERATORE
        defaultDosaggioShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultDosaggioShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the dosaggioList where operatore equals to UPDATED_OPERATORE
        defaultDosaggioShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where operatore is not null
        defaultDosaggioShouldBeFound("operatore.specified=true");

        // Get all the dosaggioList where operatore is null
        defaultDosaggioShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where ts equals to DEFAULT_TS
        defaultDosaggioShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the dosaggioList where ts equals to UPDATED_TS
        defaultDosaggioShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where ts in DEFAULT_TS or UPDATED_TS
        defaultDosaggioShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the dosaggioList where ts equals to UPDATED_TS
        defaultDosaggioShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where ts is not null
        defaultDosaggioShouldBeFound("ts.specified=true");

        // Get all the dosaggioList where ts is null
        defaultDosaggioShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali is not null
        defaultDosaggioShouldBeFound("dataInizVali.specified=true");

        // Get all the dosaggioList where dataInizVali is null
        defaultDosaggioShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDosaggioShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dosaggioList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDosaggioShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali is not null
        defaultDosaggioShouldBeFound("dataFineVali.specified=true");

        // Get all the dosaggioList where dataFineVali is null
        defaultDosaggioShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDosaggioShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dosaggioList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDosaggioShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator is not null
        defaultDosaggioShouldBeFound("userIdCreator.specified=true");

        // Get all the dosaggioList where userIdCreator is null
        defaultDosaggioShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDosaggioShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dosaggioList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDosaggioShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod is not null
        defaultDosaggioShouldBeFound("userIdLastMod.specified=true");

        // Get all the dosaggioList where userIdLastMod is null
        defaultDosaggioShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDosaggiosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);

        // Get all the dosaggioList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDosaggioShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dosaggioList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDosaggioShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllDosaggiosByDosagToDoseIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);
        Dose dosagToDose = DoseResourceIT.createEntity(em);
        em.persist(dosagToDose);
        em.flush();
        dosaggio.addDosagToDose(dosagToDose);
        dosaggioRepository.saveAndFlush(dosaggio);
        Long dosagToDoseId = dosagToDose.getId();

        // Get all the dosaggioList where dosagToDose equals to dosagToDoseId
        defaultDosaggioShouldBeFound("dosagToDoseId.equals=" + dosagToDoseId);

        // Get all the dosaggioList where dosagToDose equals to dosagToDoseId + 1
        defaultDosaggioShouldNotBeFound("dosagToDoseId.equals=" + (dosagToDoseId + 1));
    }


    @Test
    @Transactional
    public void getAllDosaggiosByDosagToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        dosaggioRepository.saveAndFlush(dosaggio);
        TabellaRaccordo dosagToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(dosagToRaccordo);
        em.flush();
        dosaggio.addDosagToRaccordo(dosagToRaccordo);
        dosaggioRepository.saveAndFlush(dosaggio);
        Long dosagToRaccordoId = dosagToRaccordo.getId();

        // Get all the dosaggioList where dosagToRaccordo equals to dosagToRaccordoId
        defaultDosaggioShouldBeFound("dosagToRaccordoId.equals=" + dosagToRaccordoId);

        // Get all the dosaggioList where dosagToRaccordo equals to dosagToRaccordoId + 1
        defaultDosaggioShouldNotBeFound("dosagToRaccordoId.equals=" + (dosagToRaccordoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDosaggioShouldBeFound(String filter) throws Exception {
        restDosaggioMockMvc.perform(get("/api/dosaggios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosaggio.getId().intValue())))
            .andExpect(jsonPath("$.[*].carenzaC").value(hasItem(DEFAULT_CARENZA_C)))
            .andExpect(jsonPath("$.[*].carenzaP").value(hasItem(DEFAULT_CARENZA_P)))
            .andExpect(jsonPath("$.[*].carenzaD").value(hasItem(DEFAULT_CARENZA_D)))
            .andExpect(jsonPath("$.[*].carenzaO").value(hasItem(DEFAULT_CARENZA_O)))
            .andExpect(jsonPath("$.[*].carenzaT").value(hasItem(DEFAULT_CARENZA_T)))
            .andExpect(jsonPath("$.[*].carenzaNd").value(hasItem(DEFAULT_CARENZA_ND)))
            .andExpect(jsonPath("$.[*].acquaHaMin").value(hasItem(DEFAULT_ACQUA_HA_MIN)))
            .andExpect(jsonPath("$.[*].acquaHaMax").value(hasItem(DEFAULT_ACQUA_HA_MAX)))
            .andExpect(jsonPath("$.[*].numMaxInt").value(hasItem(DEFAULT_NUM_MAX_INT)))
            .andExpect(jsonPath("$.[*].rifMaxTratt").value(hasItem(DEFAULT_RIF_MAX_TRATT)))
            .andExpect(jsonPath("$.[*].intervTratt").value(hasItem(DEFAULT_INTERV_TRATT)))
            .andExpect(jsonPath("$.[*].intervTrattMax").value(hasItem(DEFAULT_INTERV_TRATT_MAX)))
            .andExpect(jsonPath("$.[*].scadenzaDosi").value(hasItem(DEFAULT_SCADENZA_DOSI)))
            .andExpect(jsonPath("$.[*].epocaIntervento").value(hasItem(DEFAULT_EPOCA_INTERVENTO.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDosaggioMockMvc.perform(get("/api/dosaggios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDosaggioShouldNotBeFound(String filter) throws Exception {
        restDosaggioMockMvc.perform(get("/api/dosaggios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDosaggioMockMvc.perform(get("/api/dosaggios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDosaggio() throws Exception {
        // Get the dosaggio
        restDosaggioMockMvc.perform(get("/api/dosaggios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDosaggio() throws Exception {
        // Initialize the database
        dosaggioService.save(dosaggio);

        int databaseSizeBeforeUpdate = dosaggioRepository.findAll().size();

        // Update the dosaggio
        Dosaggio updatedDosaggio = dosaggioRepository.findById(dosaggio.getId()).get();
        // Disconnect from session so that the updates on updatedDosaggio are not directly saved in db
        em.detach(updatedDosaggio);
        updatedDosaggio
            .carenzaC(UPDATED_CARENZA_C)
            .carenzaP(UPDATED_CARENZA_P)
            .carenzaD(UPDATED_CARENZA_D)
            .carenzaO(UPDATED_CARENZA_O)
            .carenzaT(UPDATED_CARENZA_T)
            .carenzaNd(UPDATED_CARENZA_ND)
            .acquaHaMin(UPDATED_ACQUA_HA_MIN)
            .acquaHaMax(UPDATED_ACQUA_HA_MAX)
            .numMaxInt(UPDATED_NUM_MAX_INT)
            .rifMaxTratt(UPDATED_RIF_MAX_TRATT)
            .intervTratt(UPDATED_INTERV_TRATT)
            .intervTrattMax(UPDATED_INTERV_TRATT_MAX)
            .scadenzaDosi(UPDATED_SCADENZA_DOSI)
            .epocaIntervento(UPDATED_EPOCA_INTERVENTO)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDosaggioMockMvc.perform(put("/api/dosaggios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDosaggio)))
            .andExpect(status().isOk());

        // Validate the Dosaggio in the database
        List<Dosaggio> dosaggioList = dosaggioRepository.findAll();
        assertThat(dosaggioList).hasSize(databaseSizeBeforeUpdate);
        Dosaggio testDosaggio = dosaggioList.get(dosaggioList.size() - 1);
        assertThat(testDosaggio.getCarenzaC()).isEqualTo(UPDATED_CARENZA_C);
        assertThat(testDosaggio.getCarenzaP()).isEqualTo(UPDATED_CARENZA_P);
        assertThat(testDosaggio.getCarenzaD()).isEqualTo(UPDATED_CARENZA_D);
        assertThat(testDosaggio.getCarenzaO()).isEqualTo(UPDATED_CARENZA_O);
        assertThat(testDosaggio.getCarenzaT()).isEqualTo(UPDATED_CARENZA_T);
        assertThat(testDosaggio.getCarenzaNd()).isEqualTo(UPDATED_CARENZA_ND);
        assertThat(testDosaggio.getAcquaHaMin()).isEqualTo(UPDATED_ACQUA_HA_MIN);
        assertThat(testDosaggio.getAcquaHaMax()).isEqualTo(UPDATED_ACQUA_HA_MAX);
        assertThat(testDosaggio.getNumMaxInt()).isEqualTo(UPDATED_NUM_MAX_INT);
        assertThat(testDosaggio.getRifMaxTratt()).isEqualTo(UPDATED_RIF_MAX_TRATT);
        assertThat(testDosaggio.getIntervTratt()).isEqualTo(UPDATED_INTERV_TRATT);
        assertThat(testDosaggio.getIntervTrattMax()).isEqualTo(UPDATED_INTERV_TRATT_MAX);
        assertThat(testDosaggio.getScadenzaDosi()).isEqualTo(UPDATED_SCADENZA_DOSI);
        assertThat(testDosaggio.getEpocaIntervento()).isEqualTo(UPDATED_EPOCA_INTERVENTO);
        assertThat(testDosaggio.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testDosaggio.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testDosaggio.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testDosaggio.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDosaggio.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDosaggio.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDosaggio.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDosaggio.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDosaggio() throws Exception {
        int databaseSizeBeforeUpdate = dosaggioRepository.findAll().size();

        // Create the Dosaggio

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDosaggioMockMvc.perform(put("/api/dosaggios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosaggio)))
            .andExpect(status().isBadRequest());

        // Validate the Dosaggio in the database
        List<Dosaggio> dosaggioList = dosaggioRepository.findAll();
        assertThat(dosaggioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDosaggio() throws Exception {
        // Initialize the database
        dosaggioService.save(dosaggio);

        int databaseSizeBeforeDelete = dosaggioRepository.findAll().size();

        // Delete the dosaggio
        restDosaggioMockMvc.perform(delete("/api/dosaggios/{id}", dosaggio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Dosaggio> dosaggioList = dosaggioRepository.findAll();
        assertThat(dosaggioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dosaggio.class);
        Dosaggio dosaggio1 = new Dosaggio();
        dosaggio1.setId(1L);
        Dosaggio dosaggio2 = new Dosaggio();
        dosaggio2.setId(dosaggio1.getId());
        assertThat(dosaggio1).isEqualTo(dosaggio2);
        dosaggio2.setId(2L);
        assertThat(dosaggio1).isNotEqualTo(dosaggio2);
        dosaggio1.setId(null);
        assertThat(dosaggio1).isNotEqualTo(dosaggio2);
    }
}
