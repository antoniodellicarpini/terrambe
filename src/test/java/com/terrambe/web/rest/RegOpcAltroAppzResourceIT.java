package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcAltroAppz;
import com.terrambe.domain.RegOpcAltro;
import com.terrambe.repository.RegOpcAltroAppzRepository;
import com.terrambe.service.RegOpcAltroAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcAltroAppzCriteria;
import com.terrambe.service.RegOpcAltroAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcAltroAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcAltroAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcAltroAppzRepository regOpcAltroAppzRepository;

    @Autowired
    private RegOpcAltroAppzService regOpcAltroAppzService;

    @Autowired
    private RegOpcAltroAppzQueryService regOpcAltroAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcAltroAppzMockMvc;

    private RegOpcAltroAppz regOpcAltroAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcAltroAppzResource regOpcAltroAppzResource = new RegOpcAltroAppzResource(regOpcAltroAppzService, regOpcAltroAppzQueryService);
        this.restRegOpcAltroAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcAltroAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcAltroAppz createEntity(EntityManager em) {
        RegOpcAltroAppz regOpcAltroAppz = new RegOpcAltroAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcAltroAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcAltroAppz createUpdatedEntity(EntityManager em) {
        RegOpcAltroAppz regOpcAltroAppz = new RegOpcAltroAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcAltroAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcAltroAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcAltroAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcAltroAppzRepository.findAll().size();

        // Create the RegOpcAltroAppz
        restRegOpcAltroAppzMockMvc.perform(post("/api/reg-opc-altro-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltroAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcAltroAppz in the database
        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcAltroAppz testRegOpcAltroAppz = regOpcAltroAppzList.get(regOpcAltroAppzList.size() - 1);
        assertThat(testRegOpcAltroAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcAltroAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcAltroAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcAltroAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcAltroAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcAltroAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcAltroAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcAltroAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcAltroAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcAltroAppzRepository.findAll().size();

        // Create the RegOpcAltroAppz with an existing ID
        regOpcAltroAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcAltroAppzMockMvc.perform(post("/api/reg-opc-altro-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltroAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcAltroAppz in the database
        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcAltroAppzRepository.findAll().size();
        // set the field null
        regOpcAltroAppz.setIdAzienda(null);

        // Create the RegOpcAltroAppz, which fails.

        restRegOpcAltroAppzMockMvc.perform(post("/api/reg-opc-altro-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltroAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzs() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcAltroAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcAltroAppz() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get the regOpcAltroAppz
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs/{id}", regOpcAltroAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcAltroAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda is not null
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcAltroAppzList where idAzienda is null
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcAltroAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcAltroAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcAltroAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd is not null
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcAltroAppzList where idUnitaProd is null
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcAltroAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcAltroAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata is not null
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcAltroAppzList where percentLavorata is null
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcAltroAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcAltroAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento is not null
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcAltroAppzList where idAppezzamento is null
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcAltroAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcAltroAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali is not null
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcAltroAppzList where dataInizVali is null
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcAltroAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali is not null
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcAltroAppzList where dataFineVali is null
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcAltroAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcAltroAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator is not null
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcAltroAppzList where userIdCreator is null
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcAltroAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcAltroAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod is not null
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcAltroAppzList where userIdLastMod is null
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);

        // Get all the regOpcAltroAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcAltroAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcAltroAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcAltroAppzsByAppzToOpcAltroIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);
        RegOpcAltro appzToOpcAltro = RegOpcAltroResourceIT.createEntity(em);
        em.persist(appzToOpcAltro);
        em.flush();
        regOpcAltroAppz.setAppzToOpcAltro(appzToOpcAltro);
        regOpcAltroAppzRepository.saveAndFlush(regOpcAltroAppz);
        Long appzToOpcAltroId = appzToOpcAltro.getId();

        // Get all the regOpcAltroAppzList where appzToOpcAltro equals to appzToOpcAltroId
        defaultRegOpcAltroAppzShouldBeFound("appzToOpcAltroId.equals=" + appzToOpcAltroId);

        // Get all the regOpcAltroAppzList where appzToOpcAltro equals to appzToOpcAltroId + 1
        defaultRegOpcAltroAppzShouldNotBeFound("appzToOpcAltroId.equals=" + (appzToOpcAltroId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcAltroAppzShouldBeFound(String filter) throws Exception {
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcAltroAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcAltroAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcAltroAppz() throws Exception {
        // Get the regOpcAltroAppz
        restRegOpcAltroAppzMockMvc.perform(get("/api/reg-opc-altro-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcAltroAppz() throws Exception {
        // Initialize the database
        regOpcAltroAppzService.save(regOpcAltroAppz);

        int databaseSizeBeforeUpdate = regOpcAltroAppzRepository.findAll().size();

        // Update the regOpcAltroAppz
        RegOpcAltroAppz updatedRegOpcAltroAppz = regOpcAltroAppzRepository.findById(regOpcAltroAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcAltroAppz are not directly saved in db
        em.detach(updatedRegOpcAltroAppz);
        updatedRegOpcAltroAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcAltroAppzMockMvc.perform(put("/api/reg-opc-altro-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcAltroAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcAltroAppz in the database
        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcAltroAppz testRegOpcAltroAppz = regOpcAltroAppzList.get(regOpcAltroAppzList.size() - 1);
        assertThat(testRegOpcAltroAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcAltroAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcAltroAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcAltroAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcAltroAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcAltroAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcAltroAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcAltroAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcAltroAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcAltroAppzRepository.findAll().size();

        // Create the RegOpcAltroAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcAltroAppzMockMvc.perform(put("/api/reg-opc-altro-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcAltroAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcAltroAppz in the database
        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcAltroAppz() throws Exception {
        // Initialize the database
        regOpcAltroAppzService.save(regOpcAltroAppz);

        int databaseSizeBeforeDelete = regOpcAltroAppzRepository.findAll().size();

        // Delete the regOpcAltroAppz
        restRegOpcAltroAppzMockMvc.perform(delete("/api/reg-opc-altro-appzs/{id}", regOpcAltroAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcAltroAppz> regOpcAltroAppzList = regOpcAltroAppzRepository.findAll();
        assertThat(regOpcAltroAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcAltroAppz.class);
        RegOpcAltroAppz regOpcAltroAppz1 = new RegOpcAltroAppz();
        regOpcAltroAppz1.setId(1L);
        RegOpcAltroAppz regOpcAltroAppz2 = new RegOpcAltroAppz();
        regOpcAltroAppz2.setId(regOpcAltroAppz1.getId());
        assertThat(regOpcAltroAppz1).isEqualTo(regOpcAltroAppz2);
        regOpcAltroAppz2.setId(2L);
        assertThat(regOpcAltroAppz1).isNotEqualTo(regOpcAltroAppz2);
        regOpcAltroAppz1.setId(null);
        assertThat(regOpcAltroAppz1).isNotEqualTo(regOpcAltroAppz2);
    }
}
