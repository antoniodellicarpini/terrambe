package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcSeminaAppz;
import com.terrambe.domain.RegOpcSemina;
import com.terrambe.repository.RegOpcSeminaAppzRepository;
import com.terrambe.service.RegOpcSeminaAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcSeminaAppzCriteria;
import com.terrambe.service.RegOpcSeminaAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcSeminaAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcSeminaAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_LAVORATA = 1F;
    private static final Float UPDATED_PERCENT_LAVORATA = 2F;
    private static final Float SMALLER_PERCENT_LAVORATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcSeminaAppzRepository regOpcSeminaAppzRepository;

    @Autowired
    private RegOpcSeminaAppzService regOpcSeminaAppzService;

    @Autowired
    private RegOpcSeminaAppzQueryService regOpcSeminaAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcSeminaAppzMockMvc;

    private RegOpcSeminaAppz regOpcSeminaAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcSeminaAppzResource regOpcSeminaAppzResource = new RegOpcSeminaAppzResource(regOpcSeminaAppzService, regOpcSeminaAppzQueryService);
        this.restRegOpcSeminaAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcSeminaAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcSeminaAppz createEntity(EntityManager em) {
        RegOpcSeminaAppz regOpcSeminaAppz = new RegOpcSeminaAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentLavorata(DEFAULT_PERCENT_LAVORATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcSeminaAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcSeminaAppz createUpdatedEntity(EntityManager em) {
        RegOpcSeminaAppz regOpcSeminaAppz = new RegOpcSeminaAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcSeminaAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcSeminaAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcSeminaAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcSeminaAppzRepository.findAll().size();

        // Create the RegOpcSeminaAppz
        restRegOpcSeminaAppzMockMvc.perform(post("/api/reg-opc-semina-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSeminaAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcSeminaAppz in the database
        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcSeminaAppz testRegOpcSeminaAppz = regOpcSeminaAppzList.get(regOpcSeminaAppzList.size() - 1);
        assertThat(testRegOpcSeminaAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcSeminaAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcSeminaAppz.getPercentLavorata()).isEqualTo(DEFAULT_PERCENT_LAVORATA);
        assertThat(testRegOpcSeminaAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcSeminaAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcSeminaAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcSeminaAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcSeminaAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcSeminaAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcSeminaAppzRepository.findAll().size();

        // Create the RegOpcSeminaAppz with an existing ID
        regOpcSeminaAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcSeminaAppzMockMvc.perform(post("/api/reg-opc-semina-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSeminaAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcSeminaAppz in the database
        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcSeminaAppzRepository.findAll().size();
        // set the field null
        regOpcSeminaAppz.setIdAzienda(null);

        // Create the RegOpcSeminaAppz, which fails.

        restRegOpcSeminaAppzMockMvc.perform(post("/api/reg-opc-semina-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSeminaAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzs() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcSeminaAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcSeminaAppz() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get the regOpcSeminaAppz
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs/{id}", regOpcSeminaAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcSeminaAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentLavorata").value(DEFAULT_PERCENT_LAVORATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda is not null
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcSeminaAppzList where idAzienda is null
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcSeminaAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd is not null
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcSeminaAppzList where idUnitaProd is null
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcSeminaAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata equals to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.equals=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.equals=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata in DEFAULT_PERCENT_LAVORATA or UPDATED_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.in=" + DEFAULT_PERCENT_LAVORATA + "," + UPDATED_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata equals to UPDATED_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.in=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata is not null
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.specified=true");

        // Get all the regOpcSeminaAppzList where percentLavorata is null
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata is greater than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.greaterThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata is greater than or equal to UPDATED_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.greaterThanOrEqual=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata is less than or equal to DEFAULT_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.lessThanOrEqual=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata is less than or equal to SMALLER_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.lessThanOrEqual=" + SMALLER_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata is less than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.lessThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata is less than UPDATED_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.lessThan=" + UPDATED_PERCENT_LAVORATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByPercentLavorataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where percentLavorata is greater than DEFAULT_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldNotBeFound("percentLavorata.greaterThan=" + DEFAULT_PERCENT_LAVORATA);

        // Get all the regOpcSeminaAppzList where percentLavorata is greater than SMALLER_PERCENT_LAVORATA
        defaultRegOpcSeminaAppzShouldBeFound("percentLavorata.greaterThan=" + SMALLER_PERCENT_LAVORATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento is not null
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcSeminaAppzList where idAppezzamento is null
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcSeminaAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcSeminaAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali is not null
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcSeminaAppzList where dataInizVali is null
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali is not null
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcSeminaAppzList where dataFineVali is null
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcSeminaAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator is not null
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcSeminaAppzList where userIdCreator is null
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcSeminaAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod is not null
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcSeminaAppzList where userIdLastMod is null
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);

        // Get all the regOpcSeminaAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcSeminaAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminaAppzsByAppzToOpcSeminaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);
        RegOpcSemina appzToOpcSemina = RegOpcSeminaResourceIT.createEntity(em);
        em.persist(appzToOpcSemina);
        em.flush();
        regOpcSeminaAppz.setAppzToOpcSemina(appzToOpcSemina);
        regOpcSeminaAppzRepository.saveAndFlush(regOpcSeminaAppz);
        Long appzToOpcSeminaId = appzToOpcSemina.getId();

        // Get all the regOpcSeminaAppzList where appzToOpcSemina equals to appzToOpcSeminaId
        defaultRegOpcSeminaAppzShouldBeFound("appzToOpcSeminaId.equals=" + appzToOpcSeminaId);

        // Get all the regOpcSeminaAppzList where appzToOpcSemina equals to appzToOpcSeminaId + 1
        defaultRegOpcSeminaAppzShouldNotBeFound("appzToOpcSeminaId.equals=" + (appzToOpcSeminaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcSeminaAppzShouldBeFound(String filter) throws Exception {
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcSeminaAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentLavorata").value(hasItem(DEFAULT_PERCENT_LAVORATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcSeminaAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcSeminaAppz() throws Exception {
        // Get the regOpcSeminaAppz
        restRegOpcSeminaAppzMockMvc.perform(get("/api/reg-opc-semina-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcSeminaAppz() throws Exception {
        // Initialize the database
        regOpcSeminaAppzService.save(regOpcSeminaAppz);

        int databaseSizeBeforeUpdate = regOpcSeminaAppzRepository.findAll().size();

        // Update the regOpcSeminaAppz
        RegOpcSeminaAppz updatedRegOpcSeminaAppz = regOpcSeminaAppzRepository.findById(regOpcSeminaAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcSeminaAppz are not directly saved in db
        em.detach(updatedRegOpcSeminaAppz);
        updatedRegOpcSeminaAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentLavorata(UPDATED_PERCENT_LAVORATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcSeminaAppzMockMvc.perform(put("/api/reg-opc-semina-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcSeminaAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcSeminaAppz in the database
        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcSeminaAppz testRegOpcSeminaAppz = regOpcSeminaAppzList.get(regOpcSeminaAppzList.size() - 1);
        assertThat(testRegOpcSeminaAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcSeminaAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcSeminaAppz.getPercentLavorata()).isEqualTo(UPDATED_PERCENT_LAVORATA);
        assertThat(testRegOpcSeminaAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcSeminaAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcSeminaAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcSeminaAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcSeminaAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcSeminaAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcSeminaAppzRepository.findAll().size();

        // Create the RegOpcSeminaAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcSeminaAppzMockMvc.perform(put("/api/reg-opc-semina-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSeminaAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcSeminaAppz in the database
        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcSeminaAppz() throws Exception {
        // Initialize the database
        regOpcSeminaAppzService.save(regOpcSeminaAppz);

        int databaseSizeBeforeDelete = regOpcSeminaAppzRepository.findAll().size();

        // Delete the regOpcSeminaAppz
        restRegOpcSeminaAppzMockMvc.perform(delete("/api/reg-opc-semina-appzs/{id}", regOpcSeminaAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcSeminaAppz> regOpcSeminaAppzList = regOpcSeminaAppzRepository.findAll();
        assertThat(regOpcSeminaAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcSeminaAppz.class);
        RegOpcSeminaAppz regOpcSeminaAppz1 = new RegOpcSeminaAppz();
        regOpcSeminaAppz1.setId(1L);
        RegOpcSeminaAppz regOpcSeminaAppz2 = new RegOpcSeminaAppz();
        regOpcSeminaAppz2.setId(regOpcSeminaAppz1.getId());
        assertThat(regOpcSeminaAppz1).isEqualTo(regOpcSeminaAppz2);
        regOpcSeminaAppz2.setId(2L);
        assertThat(regOpcSeminaAppz1).isNotEqualTo(regOpcSeminaAppz2);
        regOpcSeminaAppz1.setId(null);
        assertThat(regOpcSeminaAppz1).isNotEqualTo(regOpcSeminaAppz2);
    }
}
