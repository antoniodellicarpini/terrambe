package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndAziRapCli;
import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.repository.IndAziRapCliRepository;
import com.terrambe.service.IndAziRapCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndAziRapCliCriteria;
import com.terrambe.service.IndAziRapCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndAziRapCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndAziRapCliResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndAziRapCliRepository indAziRapCliRepository;

    @Autowired
    private IndAziRapCliService indAziRapCliService;

    @Autowired
    private IndAziRapCliQueryService indAziRapCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndAziRapCliMockMvc;

    private IndAziRapCli indAziRapCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndAziRapCliResource indAziRapCliResource = new IndAziRapCliResource(indAziRapCliService, indAziRapCliQueryService);
        this.restIndAziRapCliMockMvc = MockMvcBuilders.standaloneSetup(indAziRapCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziRapCli createEntity(EntityManager em) {
        IndAziRapCli indAziRapCli = new IndAziRapCli()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indAziRapCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziRapCli createUpdatedEntity(EntityManager em) {
        IndAziRapCli indAziRapCli = new IndAziRapCli()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indAziRapCli;
    }

    @BeforeEach
    public void initTest() {
        indAziRapCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndAziRapCli() throws Exception {
        int databaseSizeBeforeCreate = indAziRapCliRepository.findAll().size();

        // Create the IndAziRapCli
        restIndAziRapCliMockMvc.perform(post("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRapCli)))
            .andExpect(status().isCreated());

        // Validate the IndAziRapCli in the database
        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeCreate + 1);
        IndAziRapCli testIndAziRapCli = indAziRapCliList.get(indAziRapCliList.size() - 1);
        assertThat(testIndAziRapCli.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndAziRapCli.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndAziRapCli.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndAziRapCli.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndAziRapCli.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndAziRapCli.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndAziRapCli.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndAziRapCli.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndAziRapCli.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndAziRapCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndAziRapCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndAziRapCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndAziRapCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndAziRapCli.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndAziRapCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indAziRapCliRepository.findAll().size();

        // Create the IndAziRapCli with an existing ID
        indAziRapCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndAziRapCliMockMvc.perform(post("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziRapCli in the database
        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziRapCliRepository.findAll().size();
        // set the field null
        indAziRapCli.setIndirizzo(null);

        // Create the IndAziRapCli, which fails.

        restIndAziRapCliMockMvc.perform(post("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRapCli)))
            .andExpect(status().isBadRequest());

        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziRapCliRepository.findAll().size();
        // set the field null
        indAziRapCli.setNazione(null);

        // Create the IndAziRapCli, which fails.

        restIndAziRapCliMockMvc.perform(post("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRapCli)))
            .andExpect(status().isBadRequest());

        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClis() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndAziRapCli() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get the indAziRapCli
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis/{id}", indAziRapCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indAziRapCli.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndAziRapCliShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indAziRapCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziRapCliShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndAziRapCliShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indAziRapCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziRapCliShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where indirizzo is not null
        defaultIndAziRapCliShouldBeFound("indirizzo.specified=true");

        // Get all the indAziRapCliList where indirizzo is null
        defaultIndAziRapCliShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where cap equals to DEFAULT_CAP
        defaultIndAziRapCliShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indAziRapCliList where cap equals to UPDATED_CAP
        defaultIndAziRapCliShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndAziRapCliShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indAziRapCliList where cap equals to UPDATED_CAP
        defaultIndAziRapCliShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where cap is not null
        defaultIndAziRapCliShouldBeFound("cap.specified=true");

        // Get all the indAziRapCliList where cap is null
        defaultIndAziRapCliShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where nazione equals to DEFAULT_NAZIONE
        defaultIndAziRapCliShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indAziRapCliList where nazione equals to UPDATED_NAZIONE
        defaultIndAziRapCliShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndAziRapCliShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indAziRapCliList where nazione equals to UPDATED_NAZIONE
        defaultIndAziRapCliShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where nazione is not null
        defaultIndAziRapCliShouldBeFound("nazione.specified=true");

        // Get all the indAziRapCliList where nazione is null
        defaultIndAziRapCliShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where regione equals to DEFAULT_REGIONE
        defaultIndAziRapCliShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indAziRapCliList where regione equals to UPDATED_REGIONE
        defaultIndAziRapCliShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndAziRapCliShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indAziRapCliList where regione equals to UPDATED_REGIONE
        defaultIndAziRapCliShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where regione is not null
        defaultIndAziRapCliShouldBeFound("regione.specified=true");

        // Get all the indAziRapCliList where regione is null
        defaultIndAziRapCliShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where provincia equals to DEFAULT_PROVINCIA
        defaultIndAziRapCliShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indAziRapCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziRapCliShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndAziRapCliShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indAziRapCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziRapCliShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where provincia is not null
        defaultIndAziRapCliShouldBeFound("provincia.specified=true");

        // Get all the indAziRapCliList where provincia is null
        defaultIndAziRapCliShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where comune equals to DEFAULT_COMUNE
        defaultIndAziRapCliShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indAziRapCliList where comune equals to UPDATED_COMUNE
        defaultIndAziRapCliShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndAziRapCliShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indAziRapCliList where comune equals to UPDATED_COMUNE
        defaultIndAziRapCliShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where comune is not null
        defaultIndAziRapCliShouldBeFound("comune.specified=true");

        // Get all the indAziRapCliList where comune is null
        defaultIndAziRapCliShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndAziRapCliShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indAziRapCliList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziRapCliShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndAziRapCliShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indAziRapCliList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziRapCliShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where numeroCiv is not null
        defaultIndAziRapCliShouldBeFound("numeroCiv.specified=true");

        // Get all the indAziRapCliList where numeroCiv is null
        defaultIndAziRapCliShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude equals to DEFAULT_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indAziRapCliList where latitude equals to UPDATED_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indAziRapCliList where latitude equals to UPDATED_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude is not null
        defaultIndAziRapCliShouldBeFound("latitude.specified=true");

        // Get all the indAziRapCliList where latitude is null
        defaultIndAziRapCliShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziRapCliList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziRapCliList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude is less than DEFAULT_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indAziRapCliList where latitude is less than UPDATED_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where latitude is greater than DEFAULT_LATITUDE
        defaultIndAziRapCliShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indAziRapCliList where latitude is greater than SMALLER_LATITUDE
        defaultIndAziRapCliShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude equals to DEFAULT_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapCliList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indAziRapCliList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude is not null
        defaultIndAziRapCliShouldBeFound("longitude.specified=true");

        // Get all the indAziRapCliList where longitude is null
        defaultIndAziRapCliShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapCliList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapCliList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude is less than DEFAULT_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapCliList where longitude is less than UPDATED_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndAziRapCliShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziRapCliList where longitude is greater than SMALLER_LONGITUDE
        defaultIndAziRapCliShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali is not null
        defaultIndAziRapCliShouldBeFound("dataInizVali.specified=true");

        // Get all the indAziRapCliList where dataInizVali is null
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndAziRapCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziRapCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndAziRapCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali is not null
        defaultIndAziRapCliShouldBeFound("dataFineVali.specified=true");

        // Get all the indAziRapCliList where dataFineVali is null
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndAziRapCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziRapCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndAziRapCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator is not null
        defaultIndAziRapCliShouldBeFound("userIdCreator.specified=true");

        // Get all the indAziRapCliList where userIdCreator is null
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndAziRapCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziRapCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndAziRapCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod is not null
        defaultIndAziRapCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the indAziRapCliList where userIdLastMod is null
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziRapClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);

        // Get all the indAziRapCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziRapCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndAziRapCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndAziRapClisByIndToCliRapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziRapCliRepository.saveAndFlush(indAziRapCli);
        AziRappresentanteCli indToCliRap = AziRappresentanteCliResourceIT.createEntity(em);
        em.persist(indToCliRap);
        em.flush();
        indAziRapCli.setIndToCliRap(indToCliRap);
        indAziRapCliRepository.saveAndFlush(indAziRapCli);
        Long indToCliRapId = indToCliRap.getId();

        // Get all the indAziRapCliList where indToCliRap equals to indToCliRapId
        defaultIndAziRapCliShouldBeFound("indToCliRapId.equals=" + indToCliRapId);

        // Get all the indAziRapCliList where indToCliRap equals to indToCliRapId + 1
        defaultIndAziRapCliShouldNotBeFound("indToCliRapId.equals=" + (indToCliRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndAziRapCliShouldBeFound(String filter) throws Exception {
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndAziRapCliShouldNotBeFound(String filter) throws Exception {
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndAziRapCli() throws Exception {
        // Get the indAziRapCli
        restIndAziRapCliMockMvc.perform(get("/api/ind-azi-rap-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndAziRapCli() throws Exception {
        // Initialize the database
        indAziRapCliService.save(indAziRapCli);

        int databaseSizeBeforeUpdate = indAziRapCliRepository.findAll().size();

        // Update the indAziRapCli
        IndAziRapCli updatedIndAziRapCli = indAziRapCliRepository.findById(indAziRapCli.getId()).get();
        // Disconnect from session so that the updates on updatedIndAziRapCli are not directly saved in db
        em.detach(updatedIndAziRapCli);
        updatedIndAziRapCli
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndAziRapCliMockMvc.perform(put("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndAziRapCli)))
            .andExpect(status().isOk());

        // Validate the IndAziRapCli in the database
        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeUpdate);
        IndAziRapCli testIndAziRapCli = indAziRapCliList.get(indAziRapCliList.size() - 1);
        assertThat(testIndAziRapCli.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndAziRapCli.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndAziRapCli.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndAziRapCli.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndAziRapCli.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndAziRapCli.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndAziRapCli.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndAziRapCli.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndAziRapCli.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndAziRapCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndAziRapCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndAziRapCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndAziRapCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndAziRapCli.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndAziRapCli() throws Exception {
        int databaseSizeBeforeUpdate = indAziRapCliRepository.findAll().size();

        // Create the IndAziRapCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndAziRapCliMockMvc.perform(put("/api/ind-azi-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziRapCli in the database
        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndAziRapCli() throws Exception {
        // Initialize the database
        indAziRapCliService.save(indAziRapCli);

        int databaseSizeBeforeDelete = indAziRapCliRepository.findAll().size();

        // Delete the indAziRapCli
        restIndAziRapCliMockMvc.perform(delete("/api/ind-azi-rap-clis/{id}", indAziRapCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndAziRapCli> indAziRapCliList = indAziRapCliRepository.findAll();
        assertThat(indAziRapCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndAziRapCli.class);
        IndAziRapCli indAziRapCli1 = new IndAziRapCli();
        indAziRapCli1.setId(1L);
        IndAziRapCli indAziRapCli2 = new IndAziRapCli();
        indAziRapCli2.setId(indAziRapCli1.getId());
        assertThat(indAziRapCli1).isEqualTo(indAziRapCli2);
        indAziRapCli2.setId(2L);
        assertThat(indAziRapCli1).isNotEqualTo(indAziRapCli2);
        indAziRapCli1.setId(null);
        assertThat(indAziRapCli1).isNotEqualTo(indAziRapCli2);
    }
}
