package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Brogliaccio;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.BrogliaccioRepository;
import com.terrambe.service.BrogliaccioService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.BrogliaccioCriteria;
import com.terrambe.service.BrogliaccioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BrogliaccioResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class BrogliaccioResourceIT {

    private static final String DEFAULT_NOME_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_NOME_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_COD_NAZ = "AAAAAAAAAA";
    private static final String UPDATED_COD_NAZ = "BBBBBBBBBB";

    private static final String DEFAULT_SEZ = "AAAAAAAAAA";
    private static final String UPDATED_SEZ = "BBBBBBBBBB";

    private static final String DEFAULT_FOGLIO = "AAAAAAAAAA";
    private static final String UPDATED_FOGLIO = "BBBBBBBBBB";

    private static final String DEFAULT_PART = "AAAAAAAAAA";
    private static final String UPDATED_PART = "BBBBBBBBBB";

    private static final String DEFAULT_SUB = "AAAAAAAAAA";
    private static final String UPDATED_SUB = "BBBBBBBBBB";

    private static final String DEFAULT_SUP_CAT = "AAAAAAAAAA";
    private static final String UPDATED_SUP_CAT = "BBBBBBBBBB";

    private static final String DEFAULT_SUP_GRA = "AAAAAAAAAA";
    private static final String UPDATED_SUP_GRA = "BBBBBBBBBB";

    private static final String DEFAULT_CONDUZ = "AAAAAAAAAA";
    private static final String UPDATED_CONDUZ = "BBBBBBBBBB";

    private static final String DEFAULT_C_P = "AAAAAAAAAA";
    private static final String UPDATED_C_P = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SUP_UTIL = "AAAAAAAAAA";
    private static final String UPDATED_SUP_UTIL = "BBBBBBBBBB";

    private static final String DEFAULT_SUP_ELEG = "AAAAAAAAAA";
    private static final String UPDATED_SUP_ELEG = "BBBBBBBBBB";

    private static final String DEFAULT_MACROUSO = "AAAAAAAAAA";
    private static final String UPDATED_MACROUSO = "BBBBBBBBBB";

    private static final String DEFAULT_OCCUPAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_OCCUPAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_DESTINAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESTINAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_USO = "AAAAAAAAAA";
    private static final String UPDATED_USO = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITA = "AAAAAAAAAA";
    private static final String UPDATED_QUALITA = "BBBBBBBBBB";

    private static final String DEFAULT_VARIETA = "AAAAAAAAAA";
    private static final String UPDATED_VARIETA = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATA_IMPORTAZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_IMPORTAZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_DATA_IMPORTAZIONE = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private BrogliaccioRepository brogliaccioRepository;

    @Autowired
    private BrogliaccioService brogliaccioService;

    @Autowired
    private BrogliaccioQueryService brogliaccioQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBrogliaccioMockMvc;

    private Brogliaccio brogliaccio;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BrogliaccioResource brogliaccioResource = new BrogliaccioResource(brogliaccioService, brogliaccioQueryService);
        this.restBrogliaccioMockMvc = MockMvcBuilders.standaloneSetup(brogliaccioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Brogliaccio createEntity(EntityManager em) {
        Brogliaccio brogliaccio = new Brogliaccio()
            .nomeComune(DEFAULT_NOME_COMUNE)
            .codNaz(DEFAULT_COD_NAZ)
            .sez(DEFAULT_SEZ)
            .foglio(DEFAULT_FOGLIO)
            .part(DEFAULT_PART)
            .sub(DEFAULT_SUB)
            .supCat(DEFAULT_SUP_CAT)
            .supGra(DEFAULT_SUP_GRA)
            .conduz(DEFAULT_CONDUZ)
            .cP(DEFAULT_C_P)
            .dataInizio(DEFAULT_DATA_INIZIO)
            .dataFine(DEFAULT_DATA_FINE)
            .supUtil(DEFAULT_SUP_UTIL)
            .supEleg(DEFAULT_SUP_ELEG)
            .macrouso(DEFAULT_MACROUSO)
            .occupazione(DEFAULT_OCCUPAZIONE)
            .destinazione(DEFAULT_DESTINAZIONE)
            .uso(DEFAULT_USO)
            .qualita(DEFAULT_QUALITA)
            .varieta(DEFAULT_VARIETA)
            .dataImportazione(DEFAULT_DATA_IMPORTAZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return brogliaccio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Brogliaccio createUpdatedEntity(EntityManager em) {
        Brogliaccio brogliaccio = new Brogliaccio()
            .nomeComune(UPDATED_NOME_COMUNE)
            .codNaz(UPDATED_COD_NAZ)
            .sez(UPDATED_SEZ)
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .supCat(UPDATED_SUP_CAT)
            .supGra(UPDATED_SUP_GRA)
            .conduz(UPDATED_CONDUZ)
            .cP(UPDATED_C_P)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .supUtil(UPDATED_SUP_UTIL)
            .supEleg(UPDATED_SUP_ELEG)
            .macrouso(UPDATED_MACROUSO)
            .occupazione(UPDATED_OCCUPAZIONE)
            .destinazione(UPDATED_DESTINAZIONE)
            .uso(UPDATED_USO)
            .qualita(UPDATED_QUALITA)
            .varieta(UPDATED_VARIETA)
            .dataImportazione(UPDATED_DATA_IMPORTAZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return brogliaccio;
    }

    @BeforeEach
    public void initTest() {
        brogliaccio = createEntity(em);
    }

    @Test
    @Transactional
    public void createBrogliaccio() throws Exception {
        int databaseSizeBeforeCreate = brogliaccioRepository.findAll().size();

        // Create the Brogliaccio
        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isCreated());

        // Validate the Brogliaccio in the database
        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeCreate + 1);
        Brogliaccio testBrogliaccio = brogliaccioList.get(brogliaccioList.size() - 1);
        assertThat(testBrogliaccio.getNomeComune()).isEqualTo(DEFAULT_NOME_COMUNE);
        assertThat(testBrogliaccio.getCodNaz()).isEqualTo(DEFAULT_COD_NAZ);
        assertThat(testBrogliaccio.getSez()).isEqualTo(DEFAULT_SEZ);
        assertThat(testBrogliaccio.getFoglio()).isEqualTo(DEFAULT_FOGLIO);
        assertThat(testBrogliaccio.getPart()).isEqualTo(DEFAULT_PART);
        assertThat(testBrogliaccio.getSub()).isEqualTo(DEFAULT_SUB);
        assertThat(testBrogliaccio.getSupCat()).isEqualTo(DEFAULT_SUP_CAT);
        assertThat(testBrogliaccio.getSupGra()).isEqualTo(DEFAULT_SUP_GRA);
        assertThat(testBrogliaccio.getConduz()).isEqualTo(DEFAULT_CONDUZ);
        assertThat(testBrogliaccio.getcP()).isEqualTo(DEFAULT_C_P);
        assertThat(testBrogliaccio.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
        assertThat(testBrogliaccio.getDataFine()).isEqualTo(DEFAULT_DATA_FINE);
        assertThat(testBrogliaccio.getSupUtil()).isEqualTo(DEFAULT_SUP_UTIL);
        assertThat(testBrogliaccio.getSupEleg()).isEqualTo(DEFAULT_SUP_ELEG);
        assertThat(testBrogliaccio.getMacrouso()).isEqualTo(DEFAULT_MACROUSO);
        assertThat(testBrogliaccio.getOccupazione()).isEqualTo(DEFAULT_OCCUPAZIONE);
        assertThat(testBrogliaccio.getDestinazione()).isEqualTo(DEFAULT_DESTINAZIONE);
        assertThat(testBrogliaccio.getUso()).isEqualTo(DEFAULT_USO);
        assertThat(testBrogliaccio.getQualita()).isEqualTo(DEFAULT_QUALITA);
        assertThat(testBrogliaccio.getVarieta()).isEqualTo(DEFAULT_VARIETA);
        assertThat(testBrogliaccio.getDataImportazione()).isEqualTo(DEFAULT_DATA_IMPORTAZIONE);
        assertThat(testBrogliaccio.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testBrogliaccio.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testBrogliaccio.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testBrogliaccio.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testBrogliaccio.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createBrogliaccioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = brogliaccioRepository.findAll().size();

        // Create the Brogliaccio with an existing ID
        brogliaccio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        // Validate the Brogliaccio in the database
        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomeComuneIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setNomeComune(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodNazIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setCodNaz(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSezIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setSez(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFoglioIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setFoglio(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPartIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setPart(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupCatIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setSupCat(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupGraIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setSupGra(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConduzIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setConduz(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkcPIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setcP(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataInizioIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setDataInizio(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataFineIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setDataFine(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupUtilIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setSupUtil(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupElegIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setSupEleg(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMacrousoIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setMacrouso(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOccupazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setOccupazione(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDestinazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setDestinazione(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsoIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setUso(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQualitaIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setQualita(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVarietaIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setVarieta(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataImportazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = brogliaccioRepository.findAll().size();
        // set the field null
        brogliaccio.setDataImportazione(null);

        // Create the Brogliaccio, which fails.

        restBrogliaccioMockMvc.perform(post("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBrogliaccios() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(brogliaccio.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeComune").value(hasItem(DEFAULT_NOME_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].codNaz").value(hasItem(DEFAULT_COD_NAZ.toString())))
            .andExpect(jsonPath("$.[*].sez").value(hasItem(DEFAULT_SEZ.toString())))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO.toString())))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART.toString())))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB.toString())))
            .andExpect(jsonPath("$.[*].supCat").value(hasItem(DEFAULT_SUP_CAT.toString())))
            .andExpect(jsonPath("$.[*].supGra").value(hasItem(DEFAULT_SUP_GRA.toString())))
            .andExpect(jsonPath("$.[*].conduz").value(hasItem(DEFAULT_CONDUZ.toString())))
            .andExpect(jsonPath("$.[*].cP").value(hasItem(DEFAULT_C_P.toString())))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].supUtil").value(hasItem(DEFAULT_SUP_UTIL.toString())))
            .andExpect(jsonPath("$.[*].supEleg").value(hasItem(DEFAULT_SUP_ELEG.toString())))
            .andExpect(jsonPath("$.[*].macrouso").value(hasItem(DEFAULT_MACROUSO.toString())))
            .andExpect(jsonPath("$.[*].occupazione").value(hasItem(DEFAULT_OCCUPAZIONE.toString())))
            .andExpect(jsonPath("$.[*].destinazione").value(hasItem(DEFAULT_DESTINAZIONE.toString())))
            .andExpect(jsonPath("$.[*].uso").value(hasItem(DEFAULT_USO.toString())))
            .andExpect(jsonPath("$.[*].qualita").value(hasItem(DEFAULT_QUALITA.toString())))
            .andExpect(jsonPath("$.[*].varieta").value(hasItem(DEFAULT_VARIETA.toString())))
            .andExpect(jsonPath("$.[*].dataImportazione").value(hasItem(DEFAULT_DATA_IMPORTAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getBrogliaccio() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get the brogliaccio
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios/{id}", brogliaccio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(brogliaccio.getId().intValue()))
            .andExpect(jsonPath("$.nomeComune").value(DEFAULT_NOME_COMUNE.toString()))
            .andExpect(jsonPath("$.codNaz").value(DEFAULT_COD_NAZ.toString()))
            .andExpect(jsonPath("$.sez").value(DEFAULT_SEZ.toString()))
            .andExpect(jsonPath("$.foglio").value(DEFAULT_FOGLIO.toString()))
            .andExpect(jsonPath("$.part").value(DEFAULT_PART.toString()))
            .andExpect(jsonPath("$.sub").value(DEFAULT_SUB.toString()))
            .andExpect(jsonPath("$.supCat").value(DEFAULT_SUP_CAT.toString()))
            .andExpect(jsonPath("$.supGra").value(DEFAULT_SUP_GRA.toString()))
            .andExpect(jsonPath("$.conduz").value(DEFAULT_CONDUZ.toString()))
            .andExpect(jsonPath("$.cP").value(DEFAULT_C_P.toString()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()))
            .andExpect(jsonPath("$.supUtil").value(DEFAULT_SUP_UTIL.toString()))
            .andExpect(jsonPath("$.supEleg").value(DEFAULT_SUP_ELEG.toString()))
            .andExpect(jsonPath("$.macrouso").value(DEFAULT_MACROUSO.toString()))
            .andExpect(jsonPath("$.occupazione").value(DEFAULT_OCCUPAZIONE.toString()))
            .andExpect(jsonPath("$.destinazione").value(DEFAULT_DESTINAZIONE.toString()))
            .andExpect(jsonPath("$.uso").value(DEFAULT_USO.toString()))
            .andExpect(jsonPath("$.qualita").value(DEFAULT_QUALITA.toString()))
            .andExpect(jsonPath("$.varieta").value(DEFAULT_VARIETA.toString()))
            .andExpect(jsonPath("$.dataImportazione").value(DEFAULT_DATA_IMPORTAZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByNomeComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where nomeComune equals to DEFAULT_NOME_COMUNE
        defaultBrogliaccioShouldBeFound("nomeComune.equals=" + DEFAULT_NOME_COMUNE);

        // Get all the brogliaccioList where nomeComune equals to UPDATED_NOME_COMUNE
        defaultBrogliaccioShouldNotBeFound("nomeComune.equals=" + UPDATED_NOME_COMUNE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByNomeComuneIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where nomeComune in DEFAULT_NOME_COMUNE or UPDATED_NOME_COMUNE
        defaultBrogliaccioShouldBeFound("nomeComune.in=" + DEFAULT_NOME_COMUNE + "," + UPDATED_NOME_COMUNE);

        // Get all the brogliaccioList where nomeComune equals to UPDATED_NOME_COMUNE
        defaultBrogliaccioShouldNotBeFound("nomeComune.in=" + UPDATED_NOME_COMUNE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByNomeComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where nomeComune is not null
        defaultBrogliaccioShouldBeFound("nomeComune.specified=true");

        // Get all the brogliaccioList where nomeComune is null
        defaultBrogliaccioShouldNotBeFound("nomeComune.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByCodNazIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where codNaz equals to DEFAULT_COD_NAZ
        defaultBrogliaccioShouldBeFound("codNaz.equals=" + DEFAULT_COD_NAZ);

        // Get all the brogliaccioList where codNaz equals to UPDATED_COD_NAZ
        defaultBrogliaccioShouldNotBeFound("codNaz.equals=" + UPDATED_COD_NAZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByCodNazIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where codNaz in DEFAULT_COD_NAZ or UPDATED_COD_NAZ
        defaultBrogliaccioShouldBeFound("codNaz.in=" + DEFAULT_COD_NAZ + "," + UPDATED_COD_NAZ);

        // Get all the brogliaccioList where codNaz equals to UPDATED_COD_NAZ
        defaultBrogliaccioShouldNotBeFound("codNaz.in=" + UPDATED_COD_NAZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByCodNazIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where codNaz is not null
        defaultBrogliaccioShouldBeFound("codNaz.specified=true");

        // Get all the brogliaccioList where codNaz is null
        defaultBrogliaccioShouldNotBeFound("codNaz.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySezIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sez equals to DEFAULT_SEZ
        defaultBrogliaccioShouldBeFound("sez.equals=" + DEFAULT_SEZ);

        // Get all the brogliaccioList where sez equals to UPDATED_SEZ
        defaultBrogliaccioShouldNotBeFound("sez.equals=" + UPDATED_SEZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySezIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sez in DEFAULT_SEZ or UPDATED_SEZ
        defaultBrogliaccioShouldBeFound("sez.in=" + DEFAULT_SEZ + "," + UPDATED_SEZ);

        // Get all the brogliaccioList where sez equals to UPDATED_SEZ
        defaultBrogliaccioShouldNotBeFound("sez.in=" + UPDATED_SEZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySezIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sez is not null
        defaultBrogliaccioShouldBeFound("sez.specified=true");

        // Get all the brogliaccioList where sez is null
        defaultBrogliaccioShouldNotBeFound("sez.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByFoglioIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where foglio equals to DEFAULT_FOGLIO
        defaultBrogliaccioShouldBeFound("foglio.equals=" + DEFAULT_FOGLIO);

        // Get all the brogliaccioList where foglio equals to UPDATED_FOGLIO
        defaultBrogliaccioShouldNotBeFound("foglio.equals=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByFoglioIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where foglio in DEFAULT_FOGLIO or UPDATED_FOGLIO
        defaultBrogliaccioShouldBeFound("foglio.in=" + DEFAULT_FOGLIO + "," + UPDATED_FOGLIO);

        // Get all the brogliaccioList where foglio equals to UPDATED_FOGLIO
        defaultBrogliaccioShouldNotBeFound("foglio.in=" + UPDATED_FOGLIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByFoglioIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where foglio is not null
        defaultBrogliaccioShouldBeFound("foglio.specified=true");

        // Get all the brogliaccioList where foglio is null
        defaultBrogliaccioShouldNotBeFound("foglio.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByPartIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where part equals to DEFAULT_PART
        defaultBrogliaccioShouldBeFound("part.equals=" + DEFAULT_PART);

        // Get all the brogliaccioList where part equals to UPDATED_PART
        defaultBrogliaccioShouldNotBeFound("part.equals=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByPartIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where part in DEFAULT_PART or UPDATED_PART
        defaultBrogliaccioShouldBeFound("part.in=" + DEFAULT_PART + "," + UPDATED_PART);

        // Get all the brogliaccioList where part equals to UPDATED_PART
        defaultBrogliaccioShouldNotBeFound("part.in=" + UPDATED_PART);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByPartIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where part is not null
        defaultBrogliaccioShouldBeFound("part.specified=true");

        // Get all the brogliaccioList where part is null
        defaultBrogliaccioShouldNotBeFound("part.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySubIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sub equals to DEFAULT_SUB
        defaultBrogliaccioShouldBeFound("sub.equals=" + DEFAULT_SUB);

        // Get all the brogliaccioList where sub equals to UPDATED_SUB
        defaultBrogliaccioShouldNotBeFound("sub.equals=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySubIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sub in DEFAULT_SUB or UPDATED_SUB
        defaultBrogliaccioShouldBeFound("sub.in=" + DEFAULT_SUB + "," + UPDATED_SUB);

        // Get all the brogliaccioList where sub equals to UPDATED_SUB
        defaultBrogliaccioShouldNotBeFound("sub.in=" + UPDATED_SUB);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySubIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where sub is not null
        defaultBrogliaccioShouldBeFound("sub.specified=true");

        // Get all the brogliaccioList where sub is null
        defaultBrogliaccioShouldNotBeFound("sub.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupCatIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supCat equals to DEFAULT_SUP_CAT
        defaultBrogliaccioShouldBeFound("supCat.equals=" + DEFAULT_SUP_CAT);

        // Get all the brogliaccioList where supCat equals to UPDATED_SUP_CAT
        defaultBrogliaccioShouldNotBeFound("supCat.equals=" + UPDATED_SUP_CAT);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupCatIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supCat in DEFAULT_SUP_CAT or UPDATED_SUP_CAT
        defaultBrogliaccioShouldBeFound("supCat.in=" + DEFAULT_SUP_CAT + "," + UPDATED_SUP_CAT);

        // Get all the brogliaccioList where supCat equals to UPDATED_SUP_CAT
        defaultBrogliaccioShouldNotBeFound("supCat.in=" + UPDATED_SUP_CAT);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupCatIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supCat is not null
        defaultBrogliaccioShouldBeFound("supCat.specified=true");

        // Get all the brogliaccioList where supCat is null
        defaultBrogliaccioShouldNotBeFound("supCat.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupGraIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supGra equals to DEFAULT_SUP_GRA
        defaultBrogliaccioShouldBeFound("supGra.equals=" + DEFAULT_SUP_GRA);

        // Get all the brogliaccioList where supGra equals to UPDATED_SUP_GRA
        defaultBrogliaccioShouldNotBeFound("supGra.equals=" + UPDATED_SUP_GRA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupGraIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supGra in DEFAULT_SUP_GRA or UPDATED_SUP_GRA
        defaultBrogliaccioShouldBeFound("supGra.in=" + DEFAULT_SUP_GRA + "," + UPDATED_SUP_GRA);

        // Get all the brogliaccioList where supGra equals to UPDATED_SUP_GRA
        defaultBrogliaccioShouldNotBeFound("supGra.in=" + UPDATED_SUP_GRA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupGraIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supGra is not null
        defaultBrogliaccioShouldBeFound("supGra.specified=true");

        // Get all the brogliaccioList where supGra is null
        defaultBrogliaccioShouldNotBeFound("supGra.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByConduzIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where conduz equals to DEFAULT_CONDUZ
        defaultBrogliaccioShouldBeFound("conduz.equals=" + DEFAULT_CONDUZ);

        // Get all the brogliaccioList where conduz equals to UPDATED_CONDUZ
        defaultBrogliaccioShouldNotBeFound("conduz.equals=" + UPDATED_CONDUZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByConduzIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where conduz in DEFAULT_CONDUZ or UPDATED_CONDUZ
        defaultBrogliaccioShouldBeFound("conduz.in=" + DEFAULT_CONDUZ + "," + UPDATED_CONDUZ);

        // Get all the brogliaccioList where conduz equals to UPDATED_CONDUZ
        defaultBrogliaccioShouldNotBeFound("conduz.in=" + UPDATED_CONDUZ);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByConduzIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where conduz is not null
        defaultBrogliaccioShouldBeFound("conduz.specified=true");

        // Get all the brogliaccioList where conduz is null
        defaultBrogliaccioShouldNotBeFound("conduz.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBycPIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where cP equals to DEFAULT_C_P
        defaultBrogliaccioShouldBeFound("cP.equals=" + DEFAULT_C_P);

        // Get all the brogliaccioList where cP equals to UPDATED_C_P
        defaultBrogliaccioShouldNotBeFound("cP.equals=" + UPDATED_C_P);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBycPIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where cP in DEFAULT_C_P or UPDATED_C_P
        defaultBrogliaccioShouldBeFound("cP.in=" + DEFAULT_C_P + "," + UPDATED_C_P);

        // Get all the brogliaccioList where cP equals to UPDATED_C_P
        defaultBrogliaccioShouldNotBeFound("cP.in=" + UPDATED_C_P);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBycPIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where cP is not null
        defaultBrogliaccioShouldBeFound("cP.specified=true");

        // Get all the brogliaccioList where cP is null
        defaultBrogliaccioShouldNotBeFound("cP.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio equals to DEFAULT_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.equals=" + DEFAULT_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.equals=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio in DEFAULT_DATA_INIZIO or UPDATED_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.in=" + DEFAULT_DATA_INIZIO + "," + UPDATED_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.in=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio is not null
        defaultBrogliaccioShouldBeFound("dataInizio.specified=true");

        // Get all the brogliaccioList where dataInizio is null
        defaultBrogliaccioShouldNotBeFound("dataInizio.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio is greater than or equal to DEFAULT_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio is greater than or equal to UPDATED_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.greaterThanOrEqual=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio is less than or equal to DEFAULT_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.lessThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio is less than or equal to SMALLER_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.lessThanOrEqual=" + SMALLER_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio is less than DEFAULT_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.lessThan=" + DEFAULT_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio is less than UPDATED_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.lessThan=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizio is greater than DEFAULT_DATA_INIZIO
        defaultBrogliaccioShouldNotBeFound("dataInizio.greaterThan=" + DEFAULT_DATA_INIZIO);

        // Get all the brogliaccioList where dataInizio is greater than SMALLER_DATA_INIZIO
        defaultBrogliaccioShouldBeFound("dataInizio.greaterThan=" + SMALLER_DATA_INIZIO);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine equals to DEFAULT_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.equals=" + DEFAULT_DATA_FINE);

        // Get all the brogliaccioList where dataFine equals to UPDATED_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.equals=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine in DEFAULT_DATA_FINE or UPDATED_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.in=" + DEFAULT_DATA_FINE + "," + UPDATED_DATA_FINE);

        // Get all the brogliaccioList where dataFine equals to UPDATED_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.in=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine is not null
        defaultBrogliaccioShouldBeFound("dataFine.specified=true");

        // Get all the brogliaccioList where dataFine is null
        defaultBrogliaccioShouldNotBeFound("dataFine.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine is greater than or equal to DEFAULT_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.greaterThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the brogliaccioList where dataFine is greater than or equal to UPDATED_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.greaterThanOrEqual=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine is less than or equal to DEFAULT_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.lessThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the brogliaccioList where dataFine is less than or equal to SMALLER_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.lessThanOrEqual=" + SMALLER_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine is less than DEFAULT_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.lessThan=" + DEFAULT_DATA_FINE);

        // Get all the brogliaccioList where dataFine is less than UPDATED_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.lessThan=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFine is greater than DEFAULT_DATA_FINE
        defaultBrogliaccioShouldNotBeFound("dataFine.greaterThan=" + DEFAULT_DATA_FINE);

        // Get all the brogliaccioList where dataFine is greater than SMALLER_DATA_FINE
        defaultBrogliaccioShouldBeFound("dataFine.greaterThan=" + SMALLER_DATA_FINE);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosBySupUtilIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supUtil equals to DEFAULT_SUP_UTIL
        defaultBrogliaccioShouldBeFound("supUtil.equals=" + DEFAULT_SUP_UTIL);

        // Get all the brogliaccioList where supUtil equals to UPDATED_SUP_UTIL
        defaultBrogliaccioShouldNotBeFound("supUtil.equals=" + UPDATED_SUP_UTIL);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupUtilIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supUtil in DEFAULT_SUP_UTIL or UPDATED_SUP_UTIL
        defaultBrogliaccioShouldBeFound("supUtil.in=" + DEFAULT_SUP_UTIL + "," + UPDATED_SUP_UTIL);

        // Get all the brogliaccioList where supUtil equals to UPDATED_SUP_UTIL
        defaultBrogliaccioShouldNotBeFound("supUtil.in=" + UPDATED_SUP_UTIL);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupUtilIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supUtil is not null
        defaultBrogliaccioShouldBeFound("supUtil.specified=true");

        // Get all the brogliaccioList where supUtil is null
        defaultBrogliaccioShouldNotBeFound("supUtil.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupElegIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supEleg equals to DEFAULT_SUP_ELEG
        defaultBrogliaccioShouldBeFound("supEleg.equals=" + DEFAULT_SUP_ELEG);

        // Get all the brogliaccioList where supEleg equals to UPDATED_SUP_ELEG
        defaultBrogliaccioShouldNotBeFound("supEleg.equals=" + UPDATED_SUP_ELEG);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupElegIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supEleg in DEFAULT_SUP_ELEG or UPDATED_SUP_ELEG
        defaultBrogliaccioShouldBeFound("supEleg.in=" + DEFAULT_SUP_ELEG + "," + UPDATED_SUP_ELEG);

        // Get all the brogliaccioList where supEleg equals to UPDATED_SUP_ELEG
        defaultBrogliaccioShouldNotBeFound("supEleg.in=" + UPDATED_SUP_ELEG);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosBySupElegIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where supEleg is not null
        defaultBrogliaccioShouldBeFound("supEleg.specified=true");

        // Get all the brogliaccioList where supEleg is null
        defaultBrogliaccioShouldNotBeFound("supEleg.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByMacrousoIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where macrouso equals to DEFAULT_MACROUSO
        defaultBrogliaccioShouldBeFound("macrouso.equals=" + DEFAULT_MACROUSO);

        // Get all the brogliaccioList where macrouso equals to UPDATED_MACROUSO
        defaultBrogliaccioShouldNotBeFound("macrouso.equals=" + UPDATED_MACROUSO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByMacrousoIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where macrouso in DEFAULT_MACROUSO or UPDATED_MACROUSO
        defaultBrogliaccioShouldBeFound("macrouso.in=" + DEFAULT_MACROUSO + "," + UPDATED_MACROUSO);

        // Get all the brogliaccioList where macrouso equals to UPDATED_MACROUSO
        defaultBrogliaccioShouldNotBeFound("macrouso.in=" + UPDATED_MACROUSO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByMacrousoIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where macrouso is not null
        defaultBrogliaccioShouldBeFound("macrouso.specified=true");

        // Get all the brogliaccioList where macrouso is null
        defaultBrogliaccioShouldNotBeFound("macrouso.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByOccupazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where occupazione equals to DEFAULT_OCCUPAZIONE
        defaultBrogliaccioShouldBeFound("occupazione.equals=" + DEFAULT_OCCUPAZIONE);

        // Get all the brogliaccioList where occupazione equals to UPDATED_OCCUPAZIONE
        defaultBrogliaccioShouldNotBeFound("occupazione.equals=" + UPDATED_OCCUPAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByOccupazioneIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where occupazione in DEFAULT_OCCUPAZIONE or UPDATED_OCCUPAZIONE
        defaultBrogliaccioShouldBeFound("occupazione.in=" + DEFAULT_OCCUPAZIONE + "," + UPDATED_OCCUPAZIONE);

        // Get all the brogliaccioList where occupazione equals to UPDATED_OCCUPAZIONE
        defaultBrogliaccioShouldNotBeFound("occupazione.in=" + UPDATED_OCCUPAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByOccupazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where occupazione is not null
        defaultBrogliaccioShouldBeFound("occupazione.specified=true");

        // Get all the brogliaccioList where occupazione is null
        defaultBrogliaccioShouldNotBeFound("occupazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDestinazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where destinazione equals to DEFAULT_DESTINAZIONE
        defaultBrogliaccioShouldBeFound("destinazione.equals=" + DEFAULT_DESTINAZIONE);

        // Get all the brogliaccioList where destinazione equals to UPDATED_DESTINAZIONE
        defaultBrogliaccioShouldNotBeFound("destinazione.equals=" + UPDATED_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDestinazioneIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where destinazione in DEFAULT_DESTINAZIONE or UPDATED_DESTINAZIONE
        defaultBrogliaccioShouldBeFound("destinazione.in=" + DEFAULT_DESTINAZIONE + "," + UPDATED_DESTINAZIONE);

        // Get all the brogliaccioList where destinazione equals to UPDATED_DESTINAZIONE
        defaultBrogliaccioShouldNotBeFound("destinazione.in=" + UPDATED_DESTINAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDestinazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where destinazione is not null
        defaultBrogliaccioShouldBeFound("destinazione.specified=true");

        // Get all the brogliaccioList where destinazione is null
        defaultBrogliaccioShouldNotBeFound("destinazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUsoIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where uso equals to DEFAULT_USO
        defaultBrogliaccioShouldBeFound("uso.equals=" + DEFAULT_USO);

        // Get all the brogliaccioList where uso equals to UPDATED_USO
        defaultBrogliaccioShouldNotBeFound("uso.equals=" + UPDATED_USO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUsoIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where uso in DEFAULT_USO or UPDATED_USO
        defaultBrogliaccioShouldBeFound("uso.in=" + DEFAULT_USO + "," + UPDATED_USO);

        // Get all the brogliaccioList where uso equals to UPDATED_USO
        defaultBrogliaccioShouldNotBeFound("uso.in=" + UPDATED_USO);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUsoIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where uso is not null
        defaultBrogliaccioShouldBeFound("uso.specified=true");

        // Get all the brogliaccioList where uso is null
        defaultBrogliaccioShouldNotBeFound("uso.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByQualitaIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where qualita equals to DEFAULT_QUALITA
        defaultBrogliaccioShouldBeFound("qualita.equals=" + DEFAULT_QUALITA);

        // Get all the brogliaccioList where qualita equals to UPDATED_QUALITA
        defaultBrogliaccioShouldNotBeFound("qualita.equals=" + UPDATED_QUALITA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByQualitaIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where qualita in DEFAULT_QUALITA or UPDATED_QUALITA
        defaultBrogliaccioShouldBeFound("qualita.in=" + DEFAULT_QUALITA + "," + UPDATED_QUALITA);

        // Get all the brogliaccioList where qualita equals to UPDATED_QUALITA
        defaultBrogliaccioShouldNotBeFound("qualita.in=" + UPDATED_QUALITA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByQualitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where qualita is not null
        defaultBrogliaccioShouldBeFound("qualita.specified=true");

        // Get all the brogliaccioList where qualita is null
        defaultBrogliaccioShouldNotBeFound("qualita.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByVarietaIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where varieta equals to DEFAULT_VARIETA
        defaultBrogliaccioShouldBeFound("varieta.equals=" + DEFAULT_VARIETA);

        // Get all the brogliaccioList where varieta equals to UPDATED_VARIETA
        defaultBrogliaccioShouldNotBeFound("varieta.equals=" + UPDATED_VARIETA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByVarietaIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where varieta in DEFAULT_VARIETA or UPDATED_VARIETA
        defaultBrogliaccioShouldBeFound("varieta.in=" + DEFAULT_VARIETA + "," + UPDATED_VARIETA);

        // Get all the brogliaccioList where varieta equals to UPDATED_VARIETA
        defaultBrogliaccioShouldNotBeFound("varieta.in=" + UPDATED_VARIETA);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByVarietaIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where varieta is not null
        defaultBrogliaccioShouldBeFound("varieta.specified=true");

        // Get all the brogliaccioList where varieta is null
        defaultBrogliaccioShouldNotBeFound("varieta.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataImportazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataImportazione equals to DEFAULT_DATA_IMPORTAZIONE
        defaultBrogliaccioShouldBeFound("dataImportazione.equals=" + DEFAULT_DATA_IMPORTAZIONE);

        // Get all the brogliaccioList where dataImportazione equals to UPDATED_DATA_IMPORTAZIONE
        defaultBrogliaccioShouldNotBeFound("dataImportazione.equals=" + UPDATED_DATA_IMPORTAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataImportazioneIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataImportazione in DEFAULT_DATA_IMPORTAZIONE or UPDATED_DATA_IMPORTAZIONE
        defaultBrogliaccioShouldBeFound("dataImportazione.in=" + DEFAULT_DATA_IMPORTAZIONE + "," + UPDATED_DATA_IMPORTAZIONE);

        // Get all the brogliaccioList where dataImportazione equals to UPDATED_DATA_IMPORTAZIONE
        defaultBrogliaccioShouldNotBeFound("dataImportazione.in=" + UPDATED_DATA_IMPORTAZIONE);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataImportazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataImportazione is not null
        defaultBrogliaccioShouldBeFound("dataImportazione.specified=true");

        // Get all the brogliaccioList where dataImportazione is null
        defaultBrogliaccioShouldNotBeFound("dataImportazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali is not null
        defaultBrogliaccioShouldBeFound("dataInizVali.specified=true");

        // Get all the brogliaccioList where dataInizVali is null
        defaultBrogliaccioShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultBrogliaccioShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the brogliaccioList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultBrogliaccioShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali is not null
        defaultBrogliaccioShouldBeFound("dataFineVali.specified=true");

        // Get all the brogliaccioList where dataFineVali is null
        defaultBrogliaccioShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultBrogliaccioShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the brogliaccioList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultBrogliaccioShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator is not null
        defaultBrogliaccioShouldBeFound("userIdCreator.specified=true");

        // Get all the brogliaccioList where userIdCreator is null
        defaultBrogliaccioShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultBrogliaccioShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the brogliaccioList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultBrogliaccioShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod is not null
        defaultBrogliaccioShouldBeFound("userIdLastMod.specified=true");

        // Get all the brogliaccioList where userIdLastMod is null
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBrogliacciosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);

        // Get all the brogliaccioList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultBrogliaccioShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the brogliaccioList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultBrogliaccioShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllBrogliacciosByBroglToAziAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        brogliaccioRepository.saveAndFlush(brogliaccio);
        AziAnagrafica broglToAziAna = AziAnagraficaResourceIT.createEntity(em);
        em.persist(broglToAziAna);
        em.flush();
        brogliaccio.setBroglToAziAna(broglToAziAna);
        brogliaccioRepository.saveAndFlush(brogliaccio);
        Long broglToAziAnaId = broglToAziAna.getId();

        // Get all the brogliaccioList where broglToAziAna equals to broglToAziAnaId
        defaultBrogliaccioShouldBeFound("broglToAziAnaId.equals=" + broglToAziAnaId);

        // Get all the brogliaccioList where broglToAziAna equals to broglToAziAnaId + 1
        defaultBrogliaccioShouldNotBeFound("broglToAziAnaId.equals=" + (broglToAziAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBrogliaccioShouldBeFound(String filter) throws Exception {
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(brogliaccio.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeComune").value(hasItem(DEFAULT_NOME_COMUNE)))
            .andExpect(jsonPath("$.[*].codNaz").value(hasItem(DEFAULT_COD_NAZ)))
            .andExpect(jsonPath("$.[*].sez").value(hasItem(DEFAULT_SEZ)))
            .andExpect(jsonPath("$.[*].foglio").value(hasItem(DEFAULT_FOGLIO)))
            .andExpect(jsonPath("$.[*].part").value(hasItem(DEFAULT_PART)))
            .andExpect(jsonPath("$.[*].sub").value(hasItem(DEFAULT_SUB)))
            .andExpect(jsonPath("$.[*].supCat").value(hasItem(DEFAULT_SUP_CAT)))
            .andExpect(jsonPath("$.[*].supGra").value(hasItem(DEFAULT_SUP_GRA)))
            .andExpect(jsonPath("$.[*].conduz").value(hasItem(DEFAULT_CONDUZ)))
            .andExpect(jsonPath("$.[*].cP").value(hasItem(DEFAULT_C_P)))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].supUtil").value(hasItem(DEFAULT_SUP_UTIL)))
            .andExpect(jsonPath("$.[*].supEleg").value(hasItem(DEFAULT_SUP_ELEG)))
            .andExpect(jsonPath("$.[*].macrouso").value(hasItem(DEFAULT_MACROUSO)))
            .andExpect(jsonPath("$.[*].occupazione").value(hasItem(DEFAULT_OCCUPAZIONE)))
            .andExpect(jsonPath("$.[*].destinazione").value(hasItem(DEFAULT_DESTINAZIONE)))
            .andExpect(jsonPath("$.[*].uso").value(hasItem(DEFAULT_USO)))
            .andExpect(jsonPath("$.[*].qualita").value(hasItem(DEFAULT_QUALITA)))
            .andExpect(jsonPath("$.[*].varieta").value(hasItem(DEFAULT_VARIETA)))
            .andExpect(jsonPath("$.[*].dataImportazione").value(hasItem(DEFAULT_DATA_IMPORTAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBrogliaccioShouldNotBeFound(String filter) throws Exception {
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBrogliaccio() throws Exception {
        // Get the brogliaccio
        restBrogliaccioMockMvc.perform(get("/api/brogliaccios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBrogliaccio() throws Exception {
        // Initialize the database
        brogliaccioService.save(brogliaccio);

        int databaseSizeBeforeUpdate = brogliaccioRepository.findAll().size();

        // Update the brogliaccio
        Brogliaccio updatedBrogliaccio = brogliaccioRepository.findById(brogliaccio.getId()).get();
        // Disconnect from session so that the updates on updatedBrogliaccio are not directly saved in db
        em.detach(updatedBrogliaccio);
        updatedBrogliaccio
            .nomeComune(UPDATED_NOME_COMUNE)
            .codNaz(UPDATED_COD_NAZ)
            .sez(UPDATED_SEZ)
            .foglio(UPDATED_FOGLIO)
            .part(UPDATED_PART)
            .sub(UPDATED_SUB)
            .supCat(UPDATED_SUP_CAT)
            .supGra(UPDATED_SUP_GRA)
            .conduz(UPDATED_CONDUZ)
            .cP(UPDATED_C_P)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .supUtil(UPDATED_SUP_UTIL)
            .supEleg(UPDATED_SUP_ELEG)
            .macrouso(UPDATED_MACROUSO)
            .occupazione(UPDATED_OCCUPAZIONE)
            .destinazione(UPDATED_DESTINAZIONE)
            .uso(UPDATED_USO)
            .qualita(UPDATED_QUALITA)
            .varieta(UPDATED_VARIETA)
            .dataImportazione(UPDATED_DATA_IMPORTAZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restBrogliaccioMockMvc.perform(put("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBrogliaccio)))
            .andExpect(status().isOk());

        // Validate the Brogliaccio in the database
        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeUpdate);
        Brogliaccio testBrogliaccio = brogliaccioList.get(brogliaccioList.size() - 1);
        assertThat(testBrogliaccio.getNomeComune()).isEqualTo(UPDATED_NOME_COMUNE);
        assertThat(testBrogliaccio.getCodNaz()).isEqualTo(UPDATED_COD_NAZ);
        assertThat(testBrogliaccio.getSez()).isEqualTo(UPDATED_SEZ);
        assertThat(testBrogliaccio.getFoglio()).isEqualTo(UPDATED_FOGLIO);
        assertThat(testBrogliaccio.getPart()).isEqualTo(UPDATED_PART);
        assertThat(testBrogliaccio.getSub()).isEqualTo(UPDATED_SUB);
        assertThat(testBrogliaccio.getSupCat()).isEqualTo(UPDATED_SUP_CAT);
        assertThat(testBrogliaccio.getSupGra()).isEqualTo(UPDATED_SUP_GRA);
        assertThat(testBrogliaccio.getConduz()).isEqualTo(UPDATED_CONDUZ);
        assertThat(testBrogliaccio.getcP()).isEqualTo(UPDATED_C_P);
        assertThat(testBrogliaccio.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
        assertThat(testBrogliaccio.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
        assertThat(testBrogliaccio.getSupUtil()).isEqualTo(UPDATED_SUP_UTIL);
        assertThat(testBrogliaccio.getSupEleg()).isEqualTo(UPDATED_SUP_ELEG);
        assertThat(testBrogliaccio.getMacrouso()).isEqualTo(UPDATED_MACROUSO);
        assertThat(testBrogliaccio.getOccupazione()).isEqualTo(UPDATED_OCCUPAZIONE);
        assertThat(testBrogliaccio.getDestinazione()).isEqualTo(UPDATED_DESTINAZIONE);
        assertThat(testBrogliaccio.getUso()).isEqualTo(UPDATED_USO);
        assertThat(testBrogliaccio.getQualita()).isEqualTo(UPDATED_QUALITA);
        assertThat(testBrogliaccio.getVarieta()).isEqualTo(UPDATED_VARIETA);
        assertThat(testBrogliaccio.getDataImportazione()).isEqualTo(UPDATED_DATA_IMPORTAZIONE);
        assertThat(testBrogliaccio.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testBrogliaccio.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testBrogliaccio.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testBrogliaccio.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testBrogliaccio.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingBrogliaccio() throws Exception {
        int databaseSizeBeforeUpdate = brogliaccioRepository.findAll().size();

        // Create the Brogliaccio

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBrogliaccioMockMvc.perform(put("/api/brogliaccios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brogliaccio)))
            .andExpect(status().isBadRequest());

        // Validate the Brogliaccio in the database
        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBrogliaccio() throws Exception {
        // Initialize the database
        brogliaccioService.save(brogliaccio);

        int databaseSizeBeforeDelete = brogliaccioRepository.findAll().size();

        // Delete the brogliaccio
        restBrogliaccioMockMvc.perform(delete("/api/brogliaccios/{id}", brogliaccio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Brogliaccio> brogliaccioList = brogliaccioRepository.findAll();
        assertThat(brogliaccioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Brogliaccio.class);
        Brogliaccio brogliaccio1 = new Brogliaccio();
        brogliaccio1.setId(1L);
        Brogliaccio brogliaccio2 = new Brogliaccio();
        brogliaccio2.setId(brogliaccio1.getId());
        assertThat(brogliaccio1).isEqualTo(brogliaccio2);
        brogliaccio2.setId(2L);
        assertThat(brogliaccio1).isNotEqualTo(brogliaccio2);
        brogliaccio1.setId(null);
        assertThat(brogliaccio1).isNotEqualTo(brogliaccio2);
    }
}
