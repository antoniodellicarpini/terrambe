package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndAziendaCli;
import com.terrambe.domain.Cliente;
import com.terrambe.repository.IndAziendaCliRepository;
import com.terrambe.service.IndAziendaCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndAziendaCliCriteria;
import com.terrambe.service.IndAziendaCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndAziendaCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndAziendaCliResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_CIV = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_CIV = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;
    private static final Double SMALLER_LATITUDE = 1D - 1D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;
    private static final Double SMALLER_LONGITUDE = 1D - 1D;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IndAziendaCliRepository indAziendaCliRepository;

    @Autowired
    private IndAziendaCliService indAziendaCliService;

    @Autowired
    private IndAziendaCliQueryService indAziendaCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndAziendaCliMockMvc;

    private IndAziendaCli indAziendaCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndAziendaCliResource indAziendaCliResource = new IndAziendaCliResource(indAziendaCliService, indAziendaCliQueryService);
        this.restIndAziendaCliMockMvc = MockMvcBuilders.standaloneSetup(indAziendaCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziendaCli createEntity(EntityManager em) {
        IndAziendaCli indAziendaCli = new IndAziendaCli()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .numeroCiv(DEFAULT_NUMERO_CIV)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return indAziendaCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndAziendaCli createUpdatedEntity(EntityManager em) {
        IndAziendaCli indAziendaCli = new IndAziendaCli()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return indAziendaCli;
    }

    @BeforeEach
    public void initTest() {
        indAziendaCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndAziendaCli() throws Exception {
        int databaseSizeBeforeCreate = indAziendaCliRepository.findAll().size();

        // Create the IndAziendaCli
        restIndAziendaCliMockMvc.perform(post("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziendaCli)))
            .andExpect(status().isCreated());

        // Validate the IndAziendaCli in the database
        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeCreate + 1);
        IndAziendaCli testIndAziendaCli = indAziendaCliList.get(indAziendaCliList.size() - 1);
        assertThat(testIndAziendaCli.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndAziendaCli.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndAziendaCli.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndAziendaCli.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndAziendaCli.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndAziendaCli.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndAziendaCli.getNumeroCiv()).isEqualTo(DEFAULT_NUMERO_CIV);
        assertThat(testIndAziendaCli.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testIndAziendaCli.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testIndAziendaCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndAziendaCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndAziendaCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndAziendaCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIndAziendaCli.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIndAziendaCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indAziendaCliRepository.findAll().size();

        // Create the IndAziendaCli with an existing ID
        indAziendaCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndAziendaCliMockMvc.perform(post("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziendaCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziendaCli in the database
        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziendaCliRepository.findAll().size();
        // set the field null
        indAziendaCli.setIndirizzo(null);

        // Create the IndAziendaCli, which fails.

        restIndAziendaCliMockMvc.perform(post("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziendaCli)))
            .andExpect(status().isBadRequest());

        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indAziendaCliRepository.findAll().size();
        // set the field null
        indAziendaCli.setNazione(null);

        // Create the IndAziendaCli, which fails.

        restIndAziendaCliMockMvc.perform(post("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziendaCli)))
            .andExpect(status().isBadRequest());

        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClis() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziendaCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIndAziendaCli() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get the indAziendaCli
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis/{id}", indAziendaCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indAziendaCli.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.numeroCiv").value(DEFAULT_NUMERO_CIV.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndAziendaCliShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indAziendaCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziendaCliShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndAziendaCliShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indAziendaCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndAziendaCliShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where indirizzo is not null
        defaultIndAziendaCliShouldBeFound("indirizzo.specified=true");

        // Get all the indAziendaCliList where indirizzo is null
        defaultIndAziendaCliShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where cap equals to DEFAULT_CAP
        defaultIndAziendaCliShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indAziendaCliList where cap equals to UPDATED_CAP
        defaultIndAziendaCliShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndAziendaCliShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indAziendaCliList where cap equals to UPDATED_CAP
        defaultIndAziendaCliShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where cap is not null
        defaultIndAziendaCliShouldBeFound("cap.specified=true");

        // Get all the indAziendaCliList where cap is null
        defaultIndAziendaCliShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where nazione equals to DEFAULT_NAZIONE
        defaultIndAziendaCliShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indAziendaCliList where nazione equals to UPDATED_NAZIONE
        defaultIndAziendaCliShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndAziendaCliShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indAziendaCliList where nazione equals to UPDATED_NAZIONE
        defaultIndAziendaCliShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where nazione is not null
        defaultIndAziendaCliShouldBeFound("nazione.specified=true");

        // Get all the indAziendaCliList where nazione is null
        defaultIndAziendaCliShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where regione equals to DEFAULT_REGIONE
        defaultIndAziendaCliShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indAziendaCliList where regione equals to UPDATED_REGIONE
        defaultIndAziendaCliShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndAziendaCliShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indAziendaCliList where regione equals to UPDATED_REGIONE
        defaultIndAziendaCliShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where regione is not null
        defaultIndAziendaCliShouldBeFound("regione.specified=true");

        // Get all the indAziendaCliList where regione is null
        defaultIndAziendaCliShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where provincia equals to DEFAULT_PROVINCIA
        defaultIndAziendaCliShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indAziendaCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziendaCliShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndAziendaCliShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indAziendaCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndAziendaCliShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where provincia is not null
        defaultIndAziendaCliShouldBeFound("provincia.specified=true");

        // Get all the indAziendaCliList where provincia is null
        defaultIndAziendaCliShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where comune equals to DEFAULT_COMUNE
        defaultIndAziendaCliShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indAziendaCliList where comune equals to UPDATED_COMUNE
        defaultIndAziendaCliShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndAziendaCliShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indAziendaCliList where comune equals to UPDATED_COMUNE
        defaultIndAziendaCliShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where comune is not null
        defaultIndAziendaCliShouldBeFound("comune.specified=true");

        // Get all the indAziendaCliList where comune is null
        defaultIndAziendaCliShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNumeroCivIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where numeroCiv equals to DEFAULT_NUMERO_CIV
        defaultIndAziendaCliShouldBeFound("numeroCiv.equals=" + DEFAULT_NUMERO_CIV);

        // Get all the indAziendaCliList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziendaCliShouldNotBeFound("numeroCiv.equals=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNumeroCivIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where numeroCiv in DEFAULT_NUMERO_CIV or UPDATED_NUMERO_CIV
        defaultIndAziendaCliShouldBeFound("numeroCiv.in=" + DEFAULT_NUMERO_CIV + "," + UPDATED_NUMERO_CIV);

        // Get all the indAziendaCliList where numeroCiv equals to UPDATED_NUMERO_CIV
        defaultIndAziendaCliShouldNotBeFound("numeroCiv.in=" + UPDATED_NUMERO_CIV);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByNumeroCivIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where numeroCiv is not null
        defaultIndAziendaCliShouldBeFound("numeroCiv.specified=true");

        // Get all the indAziendaCliList where numeroCiv is null
        defaultIndAziendaCliShouldNotBeFound("numeroCiv.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude equals to DEFAULT_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the indAziendaCliList where latitude equals to UPDATED_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the indAziendaCliList where latitude equals to UPDATED_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude is not null
        defaultIndAziendaCliShouldBeFound("latitude.specified=true");

        // Get all the indAziendaCliList where latitude is null
        defaultIndAziendaCliShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziendaCliList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the indAziendaCliList where latitude is less than or equal to SMALLER_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude is less than DEFAULT_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the indAziendaCliList where latitude is less than UPDATED_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where latitude is greater than DEFAULT_LATITUDE
        defaultIndAziendaCliShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the indAziendaCliList where latitude is greater than SMALLER_LATITUDE
        defaultIndAziendaCliShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude equals to DEFAULT_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaCliList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the indAziendaCliList where longitude equals to UPDATED_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude is not null
        defaultIndAziendaCliShouldBeFound("longitude.specified=true");

        // Get all the indAziendaCliList where longitude is null
        defaultIndAziendaCliShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaCliList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaCliList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude is less than DEFAULT_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaCliList where longitude is less than UPDATED_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where longitude is greater than DEFAULT_LONGITUDE
        defaultIndAziendaCliShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the indAziendaCliList where longitude is greater than SMALLER_LONGITUDE
        defaultIndAziendaCliShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali is not null
        defaultIndAziendaCliShouldBeFound("dataInizVali.specified=true");

        // Get all the indAziendaCliList where dataInizVali is null
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndAziendaCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indAziendaCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndAziendaCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali is not null
        defaultIndAziendaCliShouldBeFound("dataFineVali.specified=true");

        // Get all the indAziendaCliList where dataFineVali is null
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndAziendaCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indAziendaCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndAziendaCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator is not null
        defaultIndAziendaCliShouldBeFound("userIdCreator.specified=true");

        // Get all the indAziendaCliList where userIdCreator is null
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndAziendaCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indAziendaCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndAziendaCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod is not null
        defaultIndAziendaCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the indAziendaCliList where userIdLastMod is null
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndAziendaClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);

        // Get all the indAziendaCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indAziendaCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndAziendaCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndAziendaClisByIndToCliIsEqualToSomething() throws Exception {
        // Initialize the database
        indAziendaCliRepository.saveAndFlush(indAziendaCli);
        Cliente indToCli = ClienteResourceIT.createEntity(em);
        em.persist(indToCli);
        em.flush();
        indAziendaCli.setIndToCli(indToCli);
        indAziendaCliRepository.saveAndFlush(indAziendaCli);
        Long indToCliId = indToCli.getId();

        // Get all the indAziendaCliList where indToCli equals to indToCliId
        defaultIndAziendaCliShouldBeFound("indToCliId.equals=" + indToCliId);

        // Get all the indAziendaCliList where indToCli equals to indToCliId + 1
        defaultIndAziendaCliShouldNotBeFound("indToCliId.equals=" + (indToCliId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndAziendaCliShouldBeFound(String filter) throws Exception {
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indAziendaCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].numeroCiv").value(hasItem(DEFAULT_NUMERO_CIV)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndAziendaCliShouldNotBeFound(String filter) throws Exception {
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndAziendaCli() throws Exception {
        // Get the indAziendaCli
        restIndAziendaCliMockMvc.perform(get("/api/ind-azienda-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndAziendaCli() throws Exception {
        // Initialize the database
        indAziendaCliService.save(indAziendaCli);

        int databaseSizeBeforeUpdate = indAziendaCliRepository.findAll().size();

        // Update the indAziendaCli
        IndAziendaCli updatedIndAziendaCli = indAziendaCliRepository.findById(indAziendaCli.getId()).get();
        // Disconnect from session so that the updates on updatedIndAziendaCli are not directly saved in db
        em.detach(updatedIndAziendaCli);
        updatedIndAziendaCli
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .numeroCiv(UPDATED_NUMERO_CIV)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIndAziendaCliMockMvc.perform(put("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndAziendaCli)))
            .andExpect(status().isOk());

        // Validate the IndAziendaCli in the database
        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeUpdate);
        IndAziendaCli testIndAziendaCli = indAziendaCliList.get(indAziendaCliList.size() - 1);
        assertThat(testIndAziendaCli.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndAziendaCli.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndAziendaCli.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndAziendaCli.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndAziendaCli.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndAziendaCli.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndAziendaCli.getNumeroCiv()).isEqualTo(UPDATED_NUMERO_CIV);
        assertThat(testIndAziendaCli.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testIndAziendaCli.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testIndAziendaCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndAziendaCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndAziendaCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndAziendaCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIndAziendaCli.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIndAziendaCli() throws Exception {
        int databaseSizeBeforeUpdate = indAziendaCliRepository.findAll().size();

        // Create the IndAziendaCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndAziendaCliMockMvc.perform(put("/api/ind-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indAziendaCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndAziendaCli in the database
        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndAziendaCli() throws Exception {
        // Initialize the database
        indAziendaCliService.save(indAziendaCli);

        int databaseSizeBeforeDelete = indAziendaCliRepository.findAll().size();

        // Delete the indAziendaCli
        restIndAziendaCliMockMvc.perform(delete("/api/ind-azienda-clis/{id}", indAziendaCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndAziendaCli> indAziendaCliList = indAziendaCliRepository.findAll();
        assertThat(indAziendaCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndAziendaCli.class);
        IndAziendaCli indAziendaCli1 = new IndAziendaCli();
        indAziendaCli1.setId(1L);
        IndAziendaCli indAziendaCli2 = new IndAziendaCli();
        indAziendaCli2.setId(indAziendaCli1.getId());
        assertThat(indAziendaCli1).isEqualTo(indAziendaCli2);
        indAziendaCli2.setId(2L);
        assertThat(indAziendaCli1).isNotEqualTo(indAziendaCli2);
        indAziendaCli1.setId(null);
        assertThat(indAziendaCli1).isNotEqualTo(indAziendaCli2);
    }
}
