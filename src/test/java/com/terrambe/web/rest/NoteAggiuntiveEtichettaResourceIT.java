package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.NoteAggiuntiveEtichetta;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.NoteAggiuntiveEtichettaRepository;
import com.terrambe.service.NoteAggiuntiveEtichettaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.NoteAggiuntiveEtichettaCriteria;
import com.terrambe.service.NoteAggiuntiveEtichettaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NoteAggiuntiveEtichettaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class NoteAggiuntiveEtichettaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private NoteAggiuntiveEtichettaRepository noteAggiuntiveEtichettaRepository;

    @Autowired
    private NoteAggiuntiveEtichettaService noteAggiuntiveEtichettaService;

    @Autowired
    private NoteAggiuntiveEtichettaQueryService noteAggiuntiveEtichettaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNoteAggiuntiveEtichettaMockMvc;

    private NoteAggiuntiveEtichetta noteAggiuntiveEtichetta;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NoteAggiuntiveEtichettaResource noteAggiuntiveEtichettaResource = new NoteAggiuntiveEtichettaResource(noteAggiuntiveEtichettaService, noteAggiuntiveEtichettaQueryService);
        this.restNoteAggiuntiveEtichettaMockMvc = MockMvcBuilders.standaloneSetup(noteAggiuntiveEtichettaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NoteAggiuntiveEtichetta createEntity(EntityManager em) {
        NoteAggiuntiveEtichetta noteAggiuntiveEtichetta = new NoteAggiuntiveEtichetta()
            .descrizione(DEFAULT_DESCRIZIONE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return noteAggiuntiveEtichetta;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NoteAggiuntiveEtichetta createUpdatedEntity(EntityManager em) {
        NoteAggiuntiveEtichetta noteAggiuntiveEtichetta = new NoteAggiuntiveEtichetta()
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return noteAggiuntiveEtichetta;
    }

    @BeforeEach
    public void initTest() {
        noteAggiuntiveEtichetta = createEntity(em);
    }

    @Test
    @Transactional
    public void createNoteAggiuntiveEtichetta() throws Exception {
        int databaseSizeBeforeCreate = noteAggiuntiveEtichettaRepository.findAll().size();

        // Create the NoteAggiuntiveEtichetta
        restNoteAggiuntiveEtichettaMockMvc.perform(post("/api/note-aggiuntive-etichettas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noteAggiuntiveEtichetta)))
            .andExpect(status().isCreated());

        // Validate the NoteAggiuntiveEtichetta in the database
        List<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettaList = noteAggiuntiveEtichettaRepository.findAll();
        assertThat(noteAggiuntiveEtichettaList).hasSize(databaseSizeBeforeCreate + 1);
        NoteAggiuntiveEtichetta testNoteAggiuntiveEtichetta = noteAggiuntiveEtichettaList.get(noteAggiuntiveEtichettaList.size() - 1);
        assertThat(testNoteAggiuntiveEtichetta.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testNoteAggiuntiveEtichetta.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testNoteAggiuntiveEtichetta.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testNoteAggiuntiveEtichetta.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testNoteAggiuntiveEtichetta.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testNoteAggiuntiveEtichetta.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testNoteAggiuntiveEtichetta.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testNoteAggiuntiveEtichetta.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testNoteAggiuntiveEtichetta.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createNoteAggiuntiveEtichettaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = noteAggiuntiveEtichettaRepository.findAll().size();

        // Create the NoteAggiuntiveEtichetta with an existing ID
        noteAggiuntiveEtichetta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNoteAggiuntiveEtichettaMockMvc.perform(post("/api/note-aggiuntive-etichettas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noteAggiuntiveEtichetta)))
            .andExpect(status().isBadRequest());

        // Validate the NoteAggiuntiveEtichetta in the database
        List<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettaList = noteAggiuntiveEtichettaRepository.findAll();
        assertThat(noteAggiuntiveEtichettaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettas() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(noteAggiuntiveEtichetta.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getNoteAggiuntiveEtichetta() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get the noteAggiuntiveEtichetta
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas/{id}", noteAggiuntiveEtichetta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(noteAggiuntiveEtichetta.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultNoteAggiuntiveEtichettaShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the noteAggiuntiveEtichettaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultNoteAggiuntiveEtichettaShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the noteAggiuntiveEtichettaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where tipoImport is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("tipoImport.specified=true");

        // Get all the noteAggiuntiveEtichettaList where tipoImport is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where operatore equals to DEFAULT_OPERATORE
        defaultNoteAggiuntiveEtichettaShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the noteAggiuntiveEtichettaList where operatore equals to UPDATED_OPERATORE
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultNoteAggiuntiveEtichettaShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the noteAggiuntiveEtichettaList where operatore equals to UPDATED_OPERATORE
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where operatore is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("operatore.specified=true");

        // Get all the noteAggiuntiveEtichettaList where operatore is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where ts equals to DEFAULT_TS
        defaultNoteAggiuntiveEtichettaShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the noteAggiuntiveEtichettaList where ts equals to UPDATED_TS
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTsIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where ts in DEFAULT_TS or UPDATED_TS
        defaultNoteAggiuntiveEtichettaShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the noteAggiuntiveEtichettaList where ts equals to UPDATED_TS
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where ts is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("ts.specified=true");

        // Get all the noteAggiuntiveEtichettaList where ts is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.specified=true");

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.specified=true");

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the noteAggiuntiveEtichettaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultNoteAggiuntiveEtichettaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.specified=true");

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the noteAggiuntiveEtichettaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is not null
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.specified=true");

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is null
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the noteAggiuntiveEtichettaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultNoteAggiuntiveEtichettaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllNoteAggiuntiveEtichettasByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        noteAggiuntiveEtichetta.setEtichettaFito(etichettaFito);
        noteAggiuntiveEtichettaRepository.saveAndFlush(noteAggiuntiveEtichetta);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the noteAggiuntiveEtichettaList where etichettaFito equals to etichettaFitoId
        defaultNoteAggiuntiveEtichettaShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the noteAggiuntiveEtichettaList where etichettaFito equals to etichettaFitoId + 1
        defaultNoteAggiuntiveEtichettaShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNoteAggiuntiveEtichettaShouldBeFound(String filter) throws Exception {
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(noteAggiuntiveEtichetta.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNoteAggiuntiveEtichettaShouldNotBeFound(String filter) throws Exception {
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNoteAggiuntiveEtichetta() throws Exception {
        // Get the noteAggiuntiveEtichetta
        restNoteAggiuntiveEtichettaMockMvc.perform(get("/api/note-aggiuntive-etichettas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNoteAggiuntiveEtichetta() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaService.save(noteAggiuntiveEtichetta);

        int databaseSizeBeforeUpdate = noteAggiuntiveEtichettaRepository.findAll().size();

        // Update the noteAggiuntiveEtichetta
        NoteAggiuntiveEtichetta updatedNoteAggiuntiveEtichetta = noteAggiuntiveEtichettaRepository.findById(noteAggiuntiveEtichetta.getId()).get();
        // Disconnect from session so that the updates on updatedNoteAggiuntiveEtichetta are not directly saved in db
        em.detach(updatedNoteAggiuntiveEtichetta);
        updatedNoteAggiuntiveEtichetta
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restNoteAggiuntiveEtichettaMockMvc.perform(put("/api/note-aggiuntive-etichettas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNoteAggiuntiveEtichetta)))
            .andExpect(status().isOk());

        // Validate the NoteAggiuntiveEtichetta in the database
        List<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettaList = noteAggiuntiveEtichettaRepository.findAll();
        assertThat(noteAggiuntiveEtichettaList).hasSize(databaseSizeBeforeUpdate);
        NoteAggiuntiveEtichetta testNoteAggiuntiveEtichetta = noteAggiuntiveEtichettaList.get(noteAggiuntiveEtichettaList.size() - 1);
        assertThat(testNoteAggiuntiveEtichetta.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testNoteAggiuntiveEtichetta.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testNoteAggiuntiveEtichetta.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testNoteAggiuntiveEtichetta.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testNoteAggiuntiveEtichetta.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testNoteAggiuntiveEtichetta.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testNoteAggiuntiveEtichetta.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testNoteAggiuntiveEtichetta.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testNoteAggiuntiveEtichetta.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingNoteAggiuntiveEtichetta() throws Exception {
        int databaseSizeBeforeUpdate = noteAggiuntiveEtichettaRepository.findAll().size();

        // Create the NoteAggiuntiveEtichetta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNoteAggiuntiveEtichettaMockMvc.perform(put("/api/note-aggiuntive-etichettas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noteAggiuntiveEtichetta)))
            .andExpect(status().isBadRequest());

        // Validate the NoteAggiuntiveEtichetta in the database
        List<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettaList = noteAggiuntiveEtichettaRepository.findAll();
        assertThat(noteAggiuntiveEtichettaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNoteAggiuntiveEtichetta() throws Exception {
        // Initialize the database
        noteAggiuntiveEtichettaService.save(noteAggiuntiveEtichetta);

        int databaseSizeBeforeDelete = noteAggiuntiveEtichettaRepository.findAll().size();

        // Delete the noteAggiuntiveEtichetta
        restNoteAggiuntiveEtichettaMockMvc.perform(delete("/api/note-aggiuntive-etichettas/{id}", noteAggiuntiveEtichetta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettaList = noteAggiuntiveEtichettaRepository.findAll();
        assertThat(noteAggiuntiveEtichettaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NoteAggiuntiveEtichetta.class);
        NoteAggiuntiveEtichetta noteAggiuntiveEtichetta1 = new NoteAggiuntiveEtichetta();
        noteAggiuntiveEtichetta1.setId(1L);
        NoteAggiuntiveEtichetta noteAggiuntiveEtichetta2 = new NoteAggiuntiveEtichetta();
        noteAggiuntiveEtichetta2.setId(noteAggiuntiveEtichetta1.getId());
        assertThat(noteAggiuntiveEtichetta1).isEqualTo(noteAggiuntiveEtichetta2);
        noteAggiuntiveEtichetta2.setId(2L);
        assertThat(noteAggiuntiveEtichetta1).isNotEqualTo(noteAggiuntiveEtichetta2);
        noteAggiuntiveEtichetta1.setId(null);
        assertThat(noteAggiuntiveEtichetta1).isNotEqualTo(noteAggiuntiveEtichetta2);
    }
}
