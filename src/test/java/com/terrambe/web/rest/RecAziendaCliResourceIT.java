package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecAziendaCli;
import com.terrambe.domain.Cliente;
import com.terrambe.repository.RecAziendaCliRepository;
import com.terrambe.service.RecAziendaCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecAziendaCliCriteria;
import com.terrambe.service.RecAziendaCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecAziendaCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecAziendaCliResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecAziendaCliRepository recAziendaCliRepository;

    @Autowired
    private RecAziendaCliService recAziendaCliService;

    @Autowired
    private RecAziendaCliQueryService recAziendaCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecAziendaCliMockMvc;

    private RecAziendaCli recAziendaCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecAziendaCliResource recAziendaCliResource = new RecAziendaCliResource(recAziendaCliService, recAziendaCliQueryService);
        this.restRecAziendaCliMockMvc = MockMvcBuilders.standaloneSetup(recAziendaCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziendaCli createEntity(EntityManager em) {
        RecAziendaCli recAziendaCli = new RecAziendaCli()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recAziendaCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAziendaCli createUpdatedEntity(EntityManager em) {
        RecAziendaCli recAziendaCli = new RecAziendaCli()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recAziendaCli;
    }

    @BeforeEach
    public void initTest() {
        recAziendaCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecAziendaCli() throws Exception {
        int databaseSizeBeforeCreate = recAziendaCliRepository.findAll().size();

        // Create the RecAziendaCli
        restRecAziendaCliMockMvc.perform(post("/api/rec-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziendaCli)))
            .andExpect(status().isCreated());

        // Validate the RecAziendaCli in the database
        List<RecAziendaCli> recAziendaCliList = recAziendaCliRepository.findAll();
        assertThat(recAziendaCliList).hasSize(databaseSizeBeforeCreate + 1);
        RecAziendaCli testRecAziendaCli = recAziendaCliList.get(recAziendaCliList.size() - 1);
        assertThat(testRecAziendaCli.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecAziendaCli.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecAziendaCli.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecAziendaCli.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecAziendaCli.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecAziendaCli.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecAziendaCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecAziendaCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecAziendaCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecAziendaCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecAziendaCli.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecAziendaCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recAziendaCliRepository.findAll().size();

        // Create the RecAziendaCli with an existing ID
        recAziendaCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecAziendaCliMockMvc.perform(post("/api/rec-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziendaCli)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziendaCli in the database
        List<RecAziendaCli> recAziendaCliList = recAziendaCliRepository.findAll();
        assertThat(recAziendaCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecAziendaClis() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziendaCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecAziendaCli() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get the recAziendaCli
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis/{id}", recAziendaCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recAziendaCli.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where email equals to DEFAULT_EMAIL
        defaultRecAziendaCliShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recAziendaCliList where email equals to UPDATED_EMAIL
        defaultRecAziendaCliShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecAziendaCliShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recAziendaCliList where email equals to UPDATED_EMAIL
        defaultRecAziendaCliShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where email is not null
        defaultRecAziendaCliShouldBeFound("email.specified=true");

        // Get all the recAziendaCliList where email is null
        defaultRecAziendaCliShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where pec equals to DEFAULT_PEC
        defaultRecAziendaCliShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recAziendaCliList where pec equals to UPDATED_PEC
        defaultRecAziendaCliShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecAziendaCliShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recAziendaCliList where pec equals to UPDATED_PEC
        defaultRecAziendaCliShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where pec is not null
        defaultRecAziendaCliShouldBeFound("pec.specified=true");

        // Get all the recAziendaCliList where pec is null
        defaultRecAziendaCliShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where telefono equals to DEFAULT_TELEFONO
        defaultRecAziendaCliShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recAziendaCliList where telefono equals to UPDATED_TELEFONO
        defaultRecAziendaCliShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecAziendaCliShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recAziendaCliList where telefono equals to UPDATED_TELEFONO
        defaultRecAziendaCliShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where telefono is not null
        defaultRecAziendaCliShouldBeFound("telefono.specified=true");

        // Get all the recAziendaCliList where telefono is null
        defaultRecAziendaCliShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where fax equals to DEFAULT_FAX
        defaultRecAziendaCliShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recAziendaCliList where fax equals to UPDATED_FAX
        defaultRecAziendaCliShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecAziendaCliShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recAziendaCliList where fax equals to UPDATED_FAX
        defaultRecAziendaCliShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where fax is not null
        defaultRecAziendaCliShouldBeFound("fax.specified=true");

        // Get all the recAziendaCliList where fax is null
        defaultRecAziendaCliShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell equals to DEFAULT_CELL
        defaultRecAziendaCliShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recAziendaCliList where cell equals to UPDATED_CELL
        defaultRecAziendaCliShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecAziendaCliShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recAziendaCliList where cell equals to UPDATED_CELL
        defaultRecAziendaCliShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell is not null
        defaultRecAziendaCliShouldBeFound("cell.specified=true");

        // Get all the recAziendaCliList where cell is null
        defaultRecAziendaCliShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell2 equals to DEFAULT_CELL_2
        defaultRecAziendaCliShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recAziendaCliList where cell2 equals to UPDATED_CELL_2
        defaultRecAziendaCliShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecAziendaCliShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recAziendaCliList where cell2 equals to UPDATED_CELL_2
        defaultRecAziendaCliShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where cell2 is not null
        defaultRecAziendaCliShouldBeFound("cell2.specified=true");

        // Get all the recAziendaCliList where cell2 is null
        defaultRecAziendaCliShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali is not null
        defaultRecAziendaCliShouldBeFound("dataInizVali.specified=true");

        // Get all the recAziendaCliList where dataInizVali is null
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecAziendaCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali is not null
        defaultRecAziendaCliShouldBeFound("dataFineVali.specified=true");

        // Get all the recAziendaCliList where dataFineVali is null
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecAziendaCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecAziendaCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator is not null
        defaultRecAziendaCliShouldBeFound("userIdCreator.specified=true");

        // Get all the recAziendaCliList where userIdCreator is null
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecAziendaCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecAziendaCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod is not null
        defaultRecAziendaCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the recAziendaCliList where userIdLastMod is null
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendaClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);

        // Get all the recAziendaCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecAziendaCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecAziendaClisByRecToCliIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaCliRepository.saveAndFlush(recAziendaCli);
        Cliente recToCli = ClienteResourceIT.createEntity(em);
        em.persist(recToCli);
        em.flush();
        recAziendaCli.setRecToCli(recToCli);
        recAziendaCliRepository.saveAndFlush(recAziendaCli);
        Long recToCliId = recToCli.getId();

        // Get all the recAziendaCliList where recToCli equals to recToCliId
        defaultRecAziendaCliShouldBeFound("recToCliId.equals=" + recToCliId);

        // Get all the recAziendaCliList where recToCli equals to recToCliId + 1
        defaultRecAziendaCliShouldNotBeFound("recToCliId.equals=" + (recToCliId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecAziendaCliShouldBeFound(String filter) throws Exception {
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAziendaCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecAziendaCliShouldNotBeFound(String filter) throws Exception {
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecAziendaCli() throws Exception {
        // Get the recAziendaCli
        restRecAziendaCliMockMvc.perform(get("/api/rec-azienda-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecAziendaCli() throws Exception {
        // Initialize the database
        recAziendaCliService.save(recAziendaCli);

        int databaseSizeBeforeUpdate = recAziendaCliRepository.findAll().size();

        // Update the recAziendaCli
        RecAziendaCli updatedRecAziendaCli = recAziendaCliRepository.findById(recAziendaCli.getId()).get();
        // Disconnect from session so that the updates on updatedRecAziendaCli are not directly saved in db
        em.detach(updatedRecAziendaCli);
        updatedRecAziendaCli
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecAziendaCliMockMvc.perform(put("/api/rec-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecAziendaCli)))
            .andExpect(status().isOk());

        // Validate the RecAziendaCli in the database
        List<RecAziendaCli> recAziendaCliList = recAziendaCliRepository.findAll();
        assertThat(recAziendaCliList).hasSize(databaseSizeBeforeUpdate);
        RecAziendaCli testRecAziendaCli = recAziendaCliList.get(recAziendaCliList.size() - 1);
        assertThat(testRecAziendaCli.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecAziendaCli.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecAziendaCli.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecAziendaCli.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecAziendaCli.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecAziendaCli.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecAziendaCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecAziendaCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecAziendaCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecAziendaCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecAziendaCli.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecAziendaCli() throws Exception {
        int databaseSizeBeforeUpdate = recAziendaCliRepository.findAll().size();

        // Create the RecAziendaCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecAziendaCliMockMvc.perform(put("/api/rec-azienda-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAziendaCli)))
            .andExpect(status().isBadRequest());

        // Validate the RecAziendaCli in the database
        List<RecAziendaCli> recAziendaCliList = recAziendaCliRepository.findAll();
        assertThat(recAziendaCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecAziendaCli() throws Exception {
        // Initialize the database
        recAziendaCliService.save(recAziendaCli);

        int databaseSizeBeforeDelete = recAziendaCliRepository.findAll().size();

        // Delete the recAziendaCli
        restRecAziendaCliMockMvc.perform(delete("/api/rec-azienda-clis/{id}", recAziendaCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecAziendaCli> recAziendaCliList = recAziendaCliRepository.findAll();
        assertThat(recAziendaCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecAziendaCli.class);
        RecAziendaCli recAziendaCli1 = new RecAziendaCli();
        recAziendaCli1.setId(1L);
        RecAziendaCli recAziendaCli2 = new RecAziendaCli();
        recAziendaCli2.setId(recAziendaCli1.getId());
        assertThat(recAziendaCli1).isEqualTo(recAziendaCli2);
        recAziendaCli2.setId(2L);
        assertThat(recAziendaCli1).isNotEqualTo(recAziendaCli2);
        recAziendaCli1.setId(null);
        assertThat(recAziendaCli1).isNotEqualTo(recAziendaCli2);
    }
}
