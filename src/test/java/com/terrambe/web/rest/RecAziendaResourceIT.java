package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecAzienda;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.RecAziendaRepository;
import com.terrambe.service.RecAziendaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecAziendaCriteria;
import com.terrambe.service.RecAziendaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecAziendaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecAziendaResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecAziendaRepository recAziendaRepository;

    @Autowired
    private RecAziendaService recAziendaService;

    @Autowired
    private RecAziendaQueryService recAziendaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecAziendaMockMvc;

    private RecAzienda recAzienda;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecAziendaResource recAziendaResource = new RecAziendaResource(recAziendaService, recAziendaQueryService);
        this.restRecAziendaMockMvc = MockMvcBuilders.standaloneSetup(recAziendaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAzienda createEntity(EntityManager em) {
        RecAzienda recAzienda = new RecAzienda()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recAzienda;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecAzienda createUpdatedEntity(EntityManager em) {
        RecAzienda recAzienda = new RecAzienda()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recAzienda;
    }

    @BeforeEach
    public void initTest() {
        recAzienda = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecAzienda() throws Exception {
        int databaseSizeBeforeCreate = recAziendaRepository.findAll().size();

        // Create the RecAzienda
        restRecAziendaMockMvc.perform(post("/api/rec-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAzienda)))
            .andExpect(status().isCreated());

        // Validate the RecAzienda in the database
        List<RecAzienda> recAziendaList = recAziendaRepository.findAll();
        assertThat(recAziendaList).hasSize(databaseSizeBeforeCreate + 1);
        RecAzienda testRecAzienda = recAziendaList.get(recAziendaList.size() - 1);
        assertThat(testRecAzienda.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecAzienda.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecAzienda.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecAzienda.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecAzienda.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecAzienda.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecAzienda.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecAzienda.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecAzienda.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecAzienda.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecAzienda.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecAziendaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recAziendaRepository.findAll().size();

        // Create the RecAzienda with an existing ID
        recAzienda.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecAziendaMockMvc.perform(post("/api/rec-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAzienda)))
            .andExpect(status().isBadRequest());

        // Validate the RecAzienda in the database
        List<RecAzienda> recAziendaList = recAziendaRepository.findAll();
        assertThat(recAziendaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecAziendas() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAzienda.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecAzienda() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get the recAzienda
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas/{id}", recAzienda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recAzienda.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecAziendasByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where email equals to DEFAULT_EMAIL
        defaultRecAziendaShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recAziendaList where email equals to UPDATED_EMAIL
        defaultRecAziendaShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecAziendaShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recAziendaList where email equals to UPDATED_EMAIL
        defaultRecAziendaShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where email is not null
        defaultRecAziendaShouldBeFound("email.specified=true");

        // Get all the recAziendaList where email is null
        defaultRecAziendaShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where pec equals to DEFAULT_PEC
        defaultRecAziendaShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recAziendaList where pec equals to UPDATED_PEC
        defaultRecAziendaShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecAziendaShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recAziendaList where pec equals to UPDATED_PEC
        defaultRecAziendaShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where pec is not null
        defaultRecAziendaShouldBeFound("pec.specified=true");

        // Get all the recAziendaList where pec is null
        defaultRecAziendaShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where telefono equals to DEFAULT_TELEFONO
        defaultRecAziendaShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recAziendaList where telefono equals to UPDATED_TELEFONO
        defaultRecAziendaShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecAziendaShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recAziendaList where telefono equals to UPDATED_TELEFONO
        defaultRecAziendaShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where telefono is not null
        defaultRecAziendaShouldBeFound("telefono.specified=true");

        // Get all the recAziendaList where telefono is null
        defaultRecAziendaShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where fax equals to DEFAULT_FAX
        defaultRecAziendaShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recAziendaList where fax equals to UPDATED_FAX
        defaultRecAziendaShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecAziendaShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recAziendaList where fax equals to UPDATED_FAX
        defaultRecAziendaShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where fax is not null
        defaultRecAziendaShouldBeFound("fax.specified=true");

        // Get all the recAziendaList where fax is null
        defaultRecAziendaShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell equals to DEFAULT_CELL
        defaultRecAziendaShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recAziendaList where cell equals to UPDATED_CELL
        defaultRecAziendaShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecAziendaShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recAziendaList where cell equals to UPDATED_CELL
        defaultRecAziendaShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell is not null
        defaultRecAziendaShouldBeFound("cell.specified=true");

        // Get all the recAziendaList where cell is null
        defaultRecAziendaShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell2 equals to DEFAULT_CELL_2
        defaultRecAziendaShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recAziendaList where cell2 equals to UPDATED_CELL_2
        defaultRecAziendaShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecAziendaShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recAziendaList where cell2 equals to UPDATED_CELL_2
        defaultRecAziendaShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where cell2 is not null
        defaultRecAziendaShouldBeFound("cell2.specified=true");

        // Get all the recAziendaList where cell2 is null
        defaultRecAziendaShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali is not null
        defaultRecAziendaShouldBeFound("dataInizVali.specified=true");

        // Get all the recAziendaList where dataInizVali is null
        defaultRecAziendaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecAziendaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recAziendaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecAziendaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali is not null
        defaultRecAziendaShouldBeFound("dataFineVali.specified=true");

        // Get all the recAziendaList where dataFineVali is null
        defaultRecAziendaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecAziendaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recAziendaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecAziendaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator is not null
        defaultRecAziendaShouldBeFound("userIdCreator.specified=true");

        // Get all the recAziendaList where userIdCreator is null
        defaultRecAziendaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecAziendaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recAziendaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecAziendaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod is not null
        defaultRecAziendaShouldBeFound("userIdLastMod.specified=true");

        // Get all the recAziendaList where userIdLastMod is null
        defaultRecAziendaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecAziendasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);

        // Get all the recAziendaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecAziendaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recAziendaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecAziendaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecAziendasByRecToAziIsEqualToSomething() throws Exception {
        // Initialize the database
        recAziendaRepository.saveAndFlush(recAzienda);
        AziAnagrafica recToAzi = AziAnagraficaResourceIT.createEntity(em);
        em.persist(recToAzi);
        em.flush();
        recAzienda.setRecToAzi(recToAzi);
        recAziendaRepository.saveAndFlush(recAzienda);
        Long recToAziId = recToAzi.getId();

        // Get all the recAziendaList where recToAzi equals to recToAziId
        defaultRecAziendaShouldBeFound("recToAziId.equals=" + recToAziId);

        // Get all the recAziendaList where recToAzi equals to recToAziId + 1
        defaultRecAziendaShouldNotBeFound("recToAziId.equals=" + (recToAziId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecAziendaShouldBeFound(String filter) throws Exception {
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recAzienda.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecAziendaShouldNotBeFound(String filter) throws Exception {
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecAzienda() throws Exception {
        // Get the recAzienda
        restRecAziendaMockMvc.perform(get("/api/rec-aziendas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecAzienda() throws Exception {
        // Initialize the database
        recAziendaService.save(recAzienda);

        int databaseSizeBeforeUpdate = recAziendaRepository.findAll().size();

        // Update the recAzienda
        RecAzienda updatedRecAzienda = recAziendaRepository.findById(recAzienda.getId()).get();
        // Disconnect from session so that the updates on updatedRecAzienda are not directly saved in db
        em.detach(updatedRecAzienda);
        updatedRecAzienda
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecAziendaMockMvc.perform(put("/api/rec-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecAzienda)))
            .andExpect(status().isOk());

        // Validate the RecAzienda in the database
        List<RecAzienda> recAziendaList = recAziendaRepository.findAll();
        assertThat(recAziendaList).hasSize(databaseSizeBeforeUpdate);
        RecAzienda testRecAzienda = recAziendaList.get(recAziendaList.size() - 1);
        assertThat(testRecAzienda.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecAzienda.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecAzienda.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecAzienda.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecAzienda.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecAzienda.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecAzienda.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecAzienda.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecAzienda.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecAzienda.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecAzienda.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecAzienda() throws Exception {
        int databaseSizeBeforeUpdate = recAziendaRepository.findAll().size();

        // Create the RecAzienda

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecAziendaMockMvc.perform(put("/api/rec-aziendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recAzienda)))
            .andExpect(status().isBadRequest());

        // Validate the RecAzienda in the database
        List<RecAzienda> recAziendaList = recAziendaRepository.findAll();
        assertThat(recAziendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecAzienda() throws Exception {
        // Initialize the database
        recAziendaService.save(recAzienda);

        int databaseSizeBeforeDelete = recAziendaRepository.findAll().size();

        // Delete the recAzienda
        restRecAziendaMockMvc.perform(delete("/api/rec-aziendas/{id}", recAzienda.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecAzienda> recAziendaList = recAziendaRepository.findAll();
        assertThat(recAziendaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecAzienda.class);
        RecAzienda recAzienda1 = new RecAzienda();
        recAzienda1.setId(1L);
        RecAzienda recAzienda2 = new RecAzienda();
        recAzienda2.setId(recAzienda1.getId());
        assertThat(recAzienda1).isEqualTo(recAzienda2);
        recAzienda2.setId(2L);
        assertThat(recAzienda1).isNotEqualTo(recAzienda2);
        recAzienda1.setId(null);
        assertThat(recAzienda1).isNotEqualTo(recAzienda2);
    }
}
