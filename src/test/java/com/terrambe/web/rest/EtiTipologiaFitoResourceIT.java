package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.TipologiaFito;
import com.terrambe.repository.EtiTipologiaFitoRepository;
import com.terrambe.service.EtiTipologiaFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.EtiTipologiaFitoCriteria;
import com.terrambe.service.EtiTipologiaFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtiTipologiaFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class EtiTipologiaFitoResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private EtiTipologiaFitoRepository etiTipologiaFitoRepository;

    @Autowired
    private EtiTipologiaFitoService etiTipologiaFitoService;

    @Autowired
    private EtiTipologiaFitoQueryService etiTipologiaFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEtiTipologiaFitoMockMvc;

    private EtiTipologiaFito etiTipologiaFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EtiTipologiaFitoResource etiTipologiaFitoResource = new EtiTipologiaFitoResource(etiTipologiaFitoService, etiTipologiaFitoQueryService);
        this.restEtiTipologiaFitoMockMvc = MockMvcBuilders.standaloneSetup(etiTipologiaFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtiTipologiaFito createEntity(EntityManager em) {
        EtiTipologiaFito etiTipologiaFito = new EtiTipologiaFito()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return etiTipologiaFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtiTipologiaFito createUpdatedEntity(EntityManager em) {
        EtiTipologiaFito etiTipologiaFito = new EtiTipologiaFito()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return etiTipologiaFito;
    }

    @BeforeEach
    public void initTest() {
        etiTipologiaFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtiTipologiaFito() throws Exception {
        int databaseSizeBeforeCreate = etiTipologiaFitoRepository.findAll().size();

        // Create the EtiTipologiaFito
        restEtiTipologiaFitoMockMvc.perform(post("/api/eti-tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etiTipologiaFito)))
            .andExpect(status().isCreated());

        // Validate the EtiTipologiaFito in the database
        List<EtiTipologiaFito> etiTipologiaFitoList = etiTipologiaFitoRepository.findAll();
        assertThat(etiTipologiaFitoList).hasSize(databaseSizeBeforeCreate + 1);
        EtiTipologiaFito testEtiTipologiaFito = etiTipologiaFitoList.get(etiTipologiaFitoList.size() - 1);
        assertThat(testEtiTipologiaFito.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testEtiTipologiaFito.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testEtiTipologiaFito.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testEtiTipologiaFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testEtiTipologiaFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testEtiTipologiaFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testEtiTipologiaFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testEtiTipologiaFito.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createEtiTipologiaFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etiTipologiaFitoRepository.findAll().size();

        // Create the EtiTipologiaFito with an existing ID
        etiTipologiaFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtiTipologiaFitoMockMvc.perform(post("/api/eti-tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etiTipologiaFito)))
            .andExpect(status().isBadRequest());

        // Validate the EtiTipologiaFito in the database
        List<EtiTipologiaFito> etiTipologiaFitoList = etiTipologiaFitoRepository.findAll();
        assertThat(etiTipologiaFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitos() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etiTipologiaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtiTipologiaFito() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get the etiTipologiaFito
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos/{id}", etiTipologiaFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etiTipologiaFito.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultEtiTipologiaFitoShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the etiTipologiaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtiTipologiaFitoShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultEtiTipologiaFitoShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the etiTipologiaFitoList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtiTipologiaFitoShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where tipoImport is not null
        defaultEtiTipologiaFitoShouldBeFound("tipoImport.specified=true");

        // Get all the etiTipologiaFitoList where tipoImport is null
        defaultEtiTipologiaFitoShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where operatore equals to DEFAULT_OPERATORE
        defaultEtiTipologiaFitoShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the etiTipologiaFitoList where operatore equals to UPDATED_OPERATORE
        defaultEtiTipologiaFitoShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultEtiTipologiaFitoShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the etiTipologiaFitoList where operatore equals to UPDATED_OPERATORE
        defaultEtiTipologiaFitoShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where operatore is not null
        defaultEtiTipologiaFitoShouldBeFound("operatore.specified=true");

        // Get all the etiTipologiaFitoList where operatore is null
        defaultEtiTipologiaFitoShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where ts equals to DEFAULT_TS
        defaultEtiTipologiaFitoShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the etiTipologiaFitoList where ts equals to UPDATED_TS
        defaultEtiTipologiaFitoShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTsIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where ts in DEFAULT_TS or UPDATED_TS
        defaultEtiTipologiaFitoShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the etiTipologiaFitoList where ts equals to UPDATED_TS
        defaultEtiTipologiaFitoShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where ts is not null
        defaultEtiTipologiaFitoShouldBeFound("ts.specified=true");

        // Get all the etiTipologiaFitoList where ts is null
        defaultEtiTipologiaFitoShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali is not null
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the etiTipologiaFitoList where dataInizVali is null
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etiTipologiaFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali is not null
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the etiTipologiaFitoList where dataFineVali is null
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etiTipologiaFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultEtiTipologiaFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator is not null
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the etiTipologiaFitoList where userIdCreator is null
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etiTipologiaFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultEtiTipologiaFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod is not null
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the etiTipologiaFitoList where userIdLastMod is null
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);

        // Get all the etiTipologiaFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etiTipologiaFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultEtiTipologiaFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        etiTipologiaFito.setEtichettaFito(etichettaFito);
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the etiTipologiaFitoList where etichettaFito equals to etichettaFitoId
        defaultEtiTipologiaFitoShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the etiTipologiaFitoList where etichettaFito equals to etichettaFitoId + 1
        defaultEtiTipologiaFitoShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllEtiTipologiaFitosByTipologiaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);
        TipologiaFito tipologiaFito = TipologiaFitoResourceIT.createEntity(em);
        em.persist(tipologiaFito);
        em.flush();
        etiTipologiaFito.setTipologiaFito(tipologiaFito);
        etiTipologiaFitoRepository.saveAndFlush(etiTipologiaFito);
        Long tipologiaFitoId = tipologiaFito.getId();

        // Get all the etiTipologiaFitoList where tipologiaFito equals to tipologiaFitoId
        defaultEtiTipologiaFitoShouldBeFound("tipologiaFitoId.equals=" + tipologiaFitoId);

        // Get all the etiTipologiaFitoList where tipologiaFito equals to tipologiaFitoId + 1
        defaultEtiTipologiaFitoShouldNotBeFound("tipologiaFitoId.equals=" + (tipologiaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtiTipologiaFitoShouldBeFound(String filter) throws Exception {
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etiTipologiaFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtiTipologiaFitoShouldNotBeFound(String filter) throws Exception {
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtiTipologiaFito() throws Exception {
        // Get the etiTipologiaFito
        restEtiTipologiaFitoMockMvc.perform(get("/api/eti-tipologia-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtiTipologiaFito() throws Exception {
        // Initialize the database
        etiTipologiaFitoService.save(etiTipologiaFito);

        int databaseSizeBeforeUpdate = etiTipologiaFitoRepository.findAll().size();

        // Update the etiTipologiaFito
        EtiTipologiaFito updatedEtiTipologiaFito = etiTipologiaFitoRepository.findById(etiTipologiaFito.getId()).get();
        // Disconnect from session so that the updates on updatedEtiTipologiaFito are not directly saved in db
        em.detach(updatedEtiTipologiaFito);
        updatedEtiTipologiaFito
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restEtiTipologiaFitoMockMvc.perform(put("/api/eti-tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtiTipologiaFito)))
            .andExpect(status().isOk());

        // Validate the EtiTipologiaFito in the database
        List<EtiTipologiaFito> etiTipologiaFitoList = etiTipologiaFitoRepository.findAll();
        assertThat(etiTipologiaFitoList).hasSize(databaseSizeBeforeUpdate);
        EtiTipologiaFito testEtiTipologiaFito = etiTipologiaFitoList.get(etiTipologiaFitoList.size() - 1);
        assertThat(testEtiTipologiaFito.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testEtiTipologiaFito.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testEtiTipologiaFito.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testEtiTipologiaFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testEtiTipologiaFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testEtiTipologiaFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testEtiTipologiaFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testEtiTipologiaFito.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtiTipologiaFito() throws Exception {
        int databaseSizeBeforeUpdate = etiTipologiaFitoRepository.findAll().size();

        // Create the EtiTipologiaFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtiTipologiaFitoMockMvc.perform(put("/api/eti-tipologia-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etiTipologiaFito)))
            .andExpect(status().isBadRequest());

        // Validate the EtiTipologiaFito in the database
        List<EtiTipologiaFito> etiTipologiaFitoList = etiTipologiaFitoRepository.findAll();
        assertThat(etiTipologiaFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtiTipologiaFito() throws Exception {
        // Initialize the database
        etiTipologiaFitoService.save(etiTipologiaFito);

        int databaseSizeBeforeDelete = etiTipologiaFitoRepository.findAll().size();

        // Delete the etiTipologiaFito
        restEtiTipologiaFitoMockMvc.perform(delete("/api/eti-tipologia-fitos/{id}", etiTipologiaFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtiTipologiaFito> etiTipologiaFitoList = etiTipologiaFitoRepository.findAll();
        assertThat(etiTipologiaFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtiTipologiaFito.class);
        EtiTipologiaFito etiTipologiaFito1 = new EtiTipologiaFito();
        etiTipologiaFito1.setId(1L);
        EtiTipologiaFito etiTipologiaFito2 = new EtiTipologiaFito();
        etiTipologiaFito2.setId(etiTipologiaFito1.getId());
        assertThat(etiTipologiaFito1).isEqualTo(etiTipologiaFito2);
        etiTipologiaFito2.setId(2L);
        assertThat(etiTipologiaFito1).isNotEqualTo(etiTipologiaFito2);
        etiTipologiaFito1.setId(null);
        assertThat(etiTipologiaFito1).isNotEqualTo(etiTipologiaFito2);
    }
}
