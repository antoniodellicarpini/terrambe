package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoTrappola;
import com.terrambe.domain.Trappole;
import com.terrambe.repository.TipoTrappolaRepository;
import com.terrambe.service.TipoTrappolaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoTrappolaCriteria;
import com.terrambe.service.TipoTrappolaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoTrappolaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoTrappolaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipoTrappolaRepository tipoTrappolaRepository;

    @Autowired
    private TipoTrappolaService tipoTrappolaService;

    @Autowired
    private TipoTrappolaQueryService tipoTrappolaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoTrappolaMockMvc;

    private TipoTrappola tipoTrappola;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoTrappolaResource tipoTrappolaResource = new TipoTrappolaResource(tipoTrappolaService, tipoTrappolaQueryService);
        this.restTipoTrappolaMockMvc = MockMvcBuilders.standaloneSetup(tipoTrappolaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoTrappola createEntity(EntityManager em) {
        TipoTrappola tipoTrappola = new TipoTrappola()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipoTrappola;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoTrappola createUpdatedEntity(EntityManager em) {
        TipoTrappola tipoTrappola = new TipoTrappola()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipoTrappola;
    }

    @BeforeEach
    public void initTest() {
        tipoTrappola = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoTrappola() throws Exception {
        int databaseSizeBeforeCreate = tipoTrappolaRepository.findAll().size();

        // Create the TipoTrappola
        restTipoTrappolaMockMvc.perform(post("/api/tipo-trappolas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoTrappola)))
            .andExpect(status().isCreated());

        // Validate the TipoTrappola in the database
        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeCreate + 1);
        TipoTrappola testTipoTrappola = tipoTrappolaList.get(tipoTrappolaList.size() - 1);
        assertThat(testTipoTrappola.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoTrappola.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoTrappola.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoTrappola.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoTrappola.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipoTrappola.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipoTrappolaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoTrappolaRepository.findAll().size();

        // Create the TipoTrappola with an existing ID
        tipoTrappola.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoTrappolaMockMvc.perform(post("/api/tipo-trappolas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoTrappola)))
            .andExpect(status().isBadRequest());

        // Validate the TipoTrappola in the database
        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoTrappolaRepository.findAll().size();
        // set the field null
        tipoTrappola.setDescrizione(null);

        // Create the TipoTrappola, which fails.

        restTipoTrappolaMockMvc.perform(post("/api/tipo-trappolas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoTrappola)))
            .andExpect(status().isBadRequest());

        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolas() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoTrappola.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipoTrappola() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get the tipoTrappola
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas/{id}", tipoTrappola.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoTrappola.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoTrappolaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoTrappolaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoTrappolaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoTrappolaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoTrappolaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoTrappolaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where descrizione is not null
        defaultTipoTrappolaShouldBeFound("descrizione.specified=true");

        // Get all the tipoTrappolaList where descrizione is null
        defaultTipoTrappolaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali is not null
        defaultTipoTrappolaShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoTrappolaList where dataInizVali is null
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoTrappolaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoTrappolaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoTrappolaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali is not null
        defaultTipoTrappolaShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoTrappolaList where dataFineVali is null
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoTrappolaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoTrappolaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoTrappolaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator is not null
        defaultTipoTrappolaShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoTrappolaList where userIdCreator is null
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoTrappolaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoTrappolaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoTrappolaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod is not null
        defaultTipoTrappolaShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoTrappolaList where userIdLastMod is null
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoTrappolasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);

        // Get all the tipoTrappolaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoTrappolaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoTrappolaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoTrappolasByTipoTrapToTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);
        Trappole tipoTrapToTrap = TrappoleResourceIT.createEntity(em);
        em.persist(tipoTrapToTrap);
        em.flush();
        tipoTrappola.addTipoTrapToTrap(tipoTrapToTrap);
        tipoTrappolaRepository.saveAndFlush(tipoTrappola);
        Long tipoTrapToTrapId = tipoTrapToTrap.getId();

        // Get all the tipoTrappolaList where tipoTrapToTrap equals to tipoTrapToTrapId
        defaultTipoTrappolaShouldBeFound("tipoTrapToTrapId.equals=" + tipoTrapToTrapId);

        // Get all the tipoTrappolaList where tipoTrapToTrap equals to tipoTrapToTrapId + 1
        defaultTipoTrappolaShouldNotBeFound("tipoTrapToTrapId.equals=" + (tipoTrapToTrapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoTrappolaShouldBeFound(String filter) throws Exception {
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoTrappola.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoTrappolaShouldNotBeFound(String filter) throws Exception {
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoTrappola() throws Exception {
        // Get the tipoTrappola
        restTipoTrappolaMockMvc.perform(get("/api/tipo-trappolas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoTrappola() throws Exception {
        // Initialize the database
        tipoTrappolaService.save(tipoTrappola);

        int databaseSizeBeforeUpdate = tipoTrappolaRepository.findAll().size();

        // Update the tipoTrappola
        TipoTrappola updatedTipoTrappola = tipoTrappolaRepository.findById(tipoTrappola.getId()).get();
        // Disconnect from session so that the updates on updatedTipoTrappola are not directly saved in db
        em.detach(updatedTipoTrappola);
        updatedTipoTrappola
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipoTrappolaMockMvc.perform(put("/api/tipo-trappolas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoTrappola)))
            .andExpect(status().isOk());

        // Validate the TipoTrappola in the database
        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeUpdate);
        TipoTrappola testTipoTrappola = tipoTrappolaList.get(tipoTrappolaList.size() - 1);
        assertThat(testTipoTrappola.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoTrappola.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoTrappola.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoTrappola.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoTrappola.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipoTrappola.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoTrappola() throws Exception {
        int databaseSizeBeforeUpdate = tipoTrappolaRepository.findAll().size();

        // Create the TipoTrappola

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoTrappolaMockMvc.perform(put("/api/tipo-trappolas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoTrappola)))
            .andExpect(status().isBadRequest());

        // Validate the TipoTrappola in the database
        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoTrappola() throws Exception {
        // Initialize the database
        tipoTrappolaService.save(tipoTrappola);

        int databaseSizeBeforeDelete = tipoTrappolaRepository.findAll().size();

        // Delete the tipoTrappola
        restTipoTrappolaMockMvc.perform(delete("/api/tipo-trappolas/{id}", tipoTrappola.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoTrappola> tipoTrappolaList = tipoTrappolaRepository.findAll();
        assertThat(tipoTrappolaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoTrappola.class);
        TipoTrappola tipoTrappola1 = new TipoTrappola();
        tipoTrappola1.setId(1L);
        TipoTrappola tipoTrappola2 = new TipoTrappola();
        tipoTrappola2.setId(tipoTrappola1.getId());
        assertThat(tipoTrappola1).isEqualTo(tipoTrappola2);
        tipoTrappola2.setId(2L);
        assertThat(tipoTrappola1).isNotEqualTo(tipoTrappola2);
        tipoTrappola1.setId(null);
        assertThat(tipoTrappola1).isNotEqualTo(tipoTrappola2);
    }
}
