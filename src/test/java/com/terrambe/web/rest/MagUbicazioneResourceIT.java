package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.domain.IndMagUbi;
import com.terrambe.domain.RecMagUbi;
import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.repository.MagUbicazioneRepository;
import com.terrambe.service.MagUbicazioneService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.MagUbicazioneCriteria;
import com.terrambe.service.MagUbicazioneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MagUbicazioneResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class MagUbicazioneResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZIO_MAG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO_MAG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO_MAG = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_MAG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_MAG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_MAG = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private MagUbicazioneRepository magUbicazioneRepository;

    @Autowired
    private MagUbicazioneService magUbicazioneService;

    @Autowired
    private MagUbicazioneQueryService magUbicazioneQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMagUbicazioneMockMvc;

    private MagUbicazione magUbicazione;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MagUbicazioneResource magUbicazioneResource = new MagUbicazioneResource(magUbicazioneService, magUbicazioneQueryService);
        this.restMagUbicazioneMockMvc = MockMvcBuilders.standaloneSetup(magUbicazioneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MagUbicazione createEntity(EntityManager em) {
        MagUbicazione magUbicazione = new MagUbicazione()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizioMag(DEFAULT_DATA_INIZIO_MAG)
            .dataFineMag(DEFAULT_DATA_FINE_MAG)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return magUbicazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MagUbicazione createUpdatedEntity(EntityManager em) {
        MagUbicazione magUbicazione = new MagUbicazione()
            .idAzienda(UPDATED_ID_AZIENDA)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizioMag(UPDATED_DATA_INIZIO_MAG)
            .dataFineMag(UPDATED_DATA_FINE_MAG)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return magUbicazione;
    }

    @BeforeEach
    public void initTest() {
        magUbicazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createMagUbicazione() throws Exception {
        int databaseSizeBeforeCreate = magUbicazioneRepository.findAll().size();

        // Create the MagUbicazione
        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isCreated());

        // Validate the MagUbicazione in the database
        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeCreate + 1);
        MagUbicazione testMagUbicazione = magUbicazioneList.get(magUbicazioneList.size() - 1);
        assertThat(testMagUbicazione.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testMagUbicazione.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testMagUbicazione.getDataInizioMag()).isEqualTo(DEFAULT_DATA_INIZIO_MAG);
        assertThat(testMagUbicazione.getDataFineMag()).isEqualTo(DEFAULT_DATA_FINE_MAG);
        assertThat(testMagUbicazione.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testMagUbicazione.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testMagUbicazione.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createMagUbicazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = magUbicazioneRepository.findAll().size();

        // Create the MagUbicazione with an existing ID
        magUbicazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        // Validate the MagUbicazione in the database
        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = magUbicazioneRepository.findAll().size();
        // set the field null
        magUbicazione.setIdAzienda(null);

        // Create the MagUbicazione, which fails.

        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = magUbicazioneRepository.findAll().size();
        // set the field null
        magUbicazione.setDescrizione(null);

        // Create the MagUbicazione, which fails.

        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataInizioMagIsRequired() throws Exception {
        int databaseSizeBeforeTest = magUbicazioneRepository.findAll().size();
        // set the field null
        magUbicazione.setDataInizioMag(null);

        // Create the MagUbicazione, which fails.

        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataFineMagIsRequired() throws Exception {
        int databaseSizeBeforeTest = magUbicazioneRepository.findAll().size();
        // set the field null
        magUbicazione.setDataFineMag(null);

        // Create the MagUbicazione, which fails.

        restMagUbicazioneMockMvc.perform(post("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMagUbicaziones() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(magUbicazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizioMag").value(hasItem(DEFAULT_DATA_INIZIO_MAG.toString())))
            .andExpect(jsonPath("$.[*].dataFineMag").value(hasItem(DEFAULT_DATA_FINE_MAG.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getMagUbicazione() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get the magUbicazione
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones/{id}", magUbicazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(magUbicazione.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizioMag").value(DEFAULT_DATA_INIZIO_MAG.toString()))
            .andExpect(jsonPath("$.dataFineMag").value(DEFAULT_DATA_FINE_MAG.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda is not null
        defaultMagUbicazioneShouldBeFound("idAzienda.specified=true");

        // Get all the magUbicazioneList where idAzienda is null
        defaultMagUbicazioneShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultMagUbicazioneShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the magUbicazioneList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultMagUbicazioneShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultMagUbicazioneShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the magUbicazioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMagUbicazioneShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultMagUbicazioneShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the magUbicazioneList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMagUbicazioneShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where descrizione is not null
        defaultMagUbicazioneShouldBeFound("descrizione.specified=true");

        // Get all the magUbicazioneList where descrizione is null
        defaultMagUbicazioneShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag equals to DEFAULT_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.equals=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag equals to UPDATED_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.equals=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag in DEFAULT_DATA_INIZIO_MAG or UPDATED_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.in=" + DEFAULT_DATA_INIZIO_MAG + "," + UPDATED_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag equals to UPDATED_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.in=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag is not null
        defaultMagUbicazioneShouldBeFound("dataInizioMag.specified=true");

        // Get all the magUbicazioneList where dataInizioMag is null
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag is greater than or equal to DEFAULT_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag is greater than or equal to UPDATED_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.greaterThanOrEqual=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag is less than or equal to DEFAULT_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.lessThanOrEqual=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag is less than or equal to SMALLER_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.lessThanOrEqual=" + SMALLER_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsLessThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag is less than DEFAULT_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.lessThan=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag is less than UPDATED_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.lessThan=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataInizioMagIsGreaterThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataInizioMag is greater than DEFAULT_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldNotBeFound("dataInizioMag.greaterThan=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the magUbicazioneList where dataInizioMag is greater than SMALLER_DATA_INIZIO_MAG
        defaultMagUbicazioneShouldBeFound("dataInizioMag.greaterThan=" + SMALLER_DATA_INIZIO_MAG);
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag equals to DEFAULT_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.equals=" + DEFAULT_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag equals to UPDATED_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.equals=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag in DEFAULT_DATA_FINE_MAG or UPDATED_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.in=" + DEFAULT_DATA_FINE_MAG + "," + UPDATED_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag equals to UPDATED_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.in=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag is not null
        defaultMagUbicazioneShouldBeFound("dataFineMag.specified=true");

        // Get all the magUbicazioneList where dataFineMag is null
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag is greater than or equal to DEFAULT_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.greaterThanOrEqual=" + DEFAULT_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag is greater than or equal to UPDATED_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.greaterThanOrEqual=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag is less than or equal to DEFAULT_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.lessThanOrEqual=" + DEFAULT_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag is less than or equal to SMALLER_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.lessThanOrEqual=" + SMALLER_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsLessThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag is less than DEFAULT_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.lessThan=" + DEFAULT_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag is less than UPDATED_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.lessThan=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByDataFineMagIsGreaterThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where dataFineMag is greater than DEFAULT_DATA_FINE_MAG
        defaultMagUbicazioneShouldNotBeFound("dataFineMag.greaterThan=" + DEFAULT_DATA_FINE_MAG);

        // Get all the magUbicazioneList where dataFineMag is greater than SMALLER_DATA_FINE_MAG
        defaultMagUbicazioneShouldBeFound("dataFineMag.greaterThan=" + SMALLER_DATA_FINE_MAG);
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator is not null
        defaultMagUbicazioneShouldBeFound("userIdCreator.specified=true");

        // Get all the magUbicazioneList where userIdCreator is null
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultMagUbicazioneShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the magUbicazioneList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultMagUbicazioneShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod is not null
        defaultMagUbicazioneShouldBeFound("userIdLastMod.specified=true");

        // Get all the magUbicazioneList where userIdLastMod is null
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMagUbicazionesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);

        // Get all the magUbicazioneList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the magUbicazioneList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultMagUbicazioneShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByMagUbiToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        IndMagUbi magUbiToInd = IndMagUbiResourceIT.createEntity(em);
        em.persist(magUbiToInd);
        em.flush();
        magUbicazione.addMagUbiToInd(magUbiToInd);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long magUbiToIndId = magUbiToInd.getId();

        // Get all the magUbicazioneList where magUbiToInd equals to magUbiToIndId
        defaultMagUbicazioneShouldBeFound("magUbiToIndId.equals=" + magUbiToIndId);

        // Get all the magUbicazioneList where magUbiToInd equals to magUbiToIndId + 1
        defaultMagUbicazioneShouldNotBeFound("magUbiToIndId.equals=" + (magUbiToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByMagUbiToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        RecMagUbi magUbiToRec = RecMagUbiResourceIT.createEntity(em);
        em.persist(magUbiToRec);
        em.flush();
        magUbicazione.addMagUbiToRec(magUbiToRec);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long magUbiToRecId = magUbiToRec.getId();

        // Get all the magUbicazioneList where magUbiToRec equals to magUbiToRecId
        defaultMagUbicazioneShouldBeFound("magUbiToRecId.equals=" + magUbiToRecId);

        // Get all the magUbicazioneList where magUbiToRec equals to magUbiToRecId + 1
        defaultMagUbicazioneShouldNotBeFound("magUbiToRecId.equals=" + (magUbiToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByRegcaricotrappoleIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        RegCaricoTrappole regcaricotrappole = RegCaricoTrappoleResourceIT.createEntity(em);
        em.persist(regcaricotrappole);
        em.flush();
        magUbicazione.addRegcaricotrappole(regcaricotrappole);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long regcaricotrappoleId = regcaricotrappole.getId();

        // Get all the magUbicazioneList where regcaricotrappole equals to regcaricotrappoleId
        defaultMagUbicazioneShouldBeFound("regcaricotrappoleId.equals=" + regcaricotrappoleId);

        // Get all the magUbicazioneList where regcaricotrappole equals to regcaricotrappoleId + 1
        defaultMagUbicazioneShouldNotBeFound("regcaricotrappoleId.equals=" + (regcaricotrappoleId + 1));
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByRegcaricofitofarmacoIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        RegCaricoFitoFarmaco regcaricofitofarmaco = RegCaricoFitoFarmacoResourceIT.createEntity(em);
        em.persist(regcaricofitofarmaco);
        em.flush();
        magUbicazione.addRegcaricofitofarmaco(regcaricofitofarmaco);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long regcaricofitofarmacoId = regcaricofitofarmaco.getId();

        // Get all the magUbicazioneList where regcaricofitofarmaco equals to regcaricofitofarmacoId
        defaultMagUbicazioneShouldBeFound("regcaricofitofarmacoId.equals=" + regcaricofitofarmacoId);

        // Get all the magUbicazioneList where regcaricofitofarmaco equals to regcaricofitofarmacoId + 1
        defaultMagUbicazioneShouldNotBeFound("regcaricofitofarmacoId.equals=" + (regcaricofitofarmacoId + 1));
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByRegcaricofertilizzanteIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        RegCaricoFertilizzante regcaricofertilizzante = RegCaricoFertilizzanteResourceIT.createEntity(em);
        em.persist(regcaricofertilizzante);
        em.flush();
        magUbicazione.addRegcaricofertilizzante(regcaricofertilizzante);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long regcaricofertilizzanteId = regcaricofertilizzante.getId();

        // Get all the magUbicazioneList where regcaricofertilizzante equals to regcaricofertilizzanteId
        defaultMagUbicazioneShouldBeFound("regcaricofertilizzanteId.equals=" + regcaricofertilizzanteId);

        // Get all the magUbicazioneList where regcaricofertilizzante equals to regcaricofertilizzanteId + 1
        defaultMagUbicazioneShouldNotBeFound("regcaricofertilizzanteId.equals=" + (regcaricofertilizzanteId + 1));
    }


    @Test
    @Transactional
    public void getAllMagUbicazionesByUnianagraficaIsEqualToSomething() throws Exception {
        // Initialize the database
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        UniAnagrafica unianagrafica = UniAnagraficaResourceIT.createEntity(em);
        em.persist(unianagrafica);
        em.flush();
        magUbicazione.setUnianagrafica(unianagrafica);
        magUbicazioneRepository.saveAndFlush(magUbicazione);
        Long unianagraficaId = unianagrafica.getId();

        // Get all the magUbicazioneList where unianagrafica equals to unianagraficaId
        defaultMagUbicazioneShouldBeFound("unianagraficaId.equals=" + unianagraficaId);

        // Get all the magUbicazioneList where unianagrafica equals to unianagraficaId + 1
        defaultMagUbicazioneShouldNotBeFound("unianagraficaId.equals=" + (unianagraficaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMagUbicazioneShouldBeFound(String filter) throws Exception {
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(magUbicazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizioMag").value(hasItem(DEFAULT_DATA_INIZIO_MAG.toString())))
            .andExpect(jsonPath("$.[*].dataFineMag").value(hasItem(DEFAULT_DATA_FINE_MAG.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMagUbicazioneShouldNotBeFound(String filter) throws Exception {
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMagUbicazione() throws Exception {
        // Get the magUbicazione
        restMagUbicazioneMockMvc.perform(get("/api/mag-ubicaziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMagUbicazione() throws Exception {
        // Initialize the database
        magUbicazioneService.save(magUbicazione);

        int databaseSizeBeforeUpdate = magUbicazioneRepository.findAll().size();

        // Update the magUbicazione
        MagUbicazione updatedMagUbicazione = magUbicazioneRepository.findById(magUbicazione.getId()).get();
        // Disconnect from session so that the updates on updatedMagUbicazione are not directly saved in db
        em.detach(updatedMagUbicazione);
        updatedMagUbicazione
            .idAzienda(UPDATED_ID_AZIENDA)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizioMag(UPDATED_DATA_INIZIO_MAG)
            .dataFineMag(UPDATED_DATA_FINE_MAG)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restMagUbicazioneMockMvc.perform(put("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMagUbicazione)))
            .andExpect(status().isOk());

        // Validate the MagUbicazione in the database
        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeUpdate);
        MagUbicazione testMagUbicazione = magUbicazioneList.get(magUbicazioneList.size() - 1);
        assertThat(testMagUbicazione.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testMagUbicazione.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testMagUbicazione.getDataInizioMag()).isEqualTo(UPDATED_DATA_INIZIO_MAG);
        assertThat(testMagUbicazione.getDataFineMag()).isEqualTo(UPDATED_DATA_FINE_MAG);
        assertThat(testMagUbicazione.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testMagUbicazione.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testMagUbicazione.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingMagUbicazione() throws Exception {
        int databaseSizeBeforeUpdate = magUbicazioneRepository.findAll().size();

        // Create the MagUbicazione

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMagUbicazioneMockMvc.perform(put("/api/mag-ubicaziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(magUbicazione)))
            .andExpect(status().isBadRequest());

        // Validate the MagUbicazione in the database
        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMagUbicazione() throws Exception {
        // Initialize the database
        magUbicazioneService.save(magUbicazione);

        int databaseSizeBeforeDelete = magUbicazioneRepository.findAll().size();

        // Delete the magUbicazione
        restMagUbicazioneMockMvc.perform(delete("/api/mag-ubicaziones/{id}", magUbicazione.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MagUbicazione> magUbicazioneList = magUbicazioneRepository.findAll();
        assertThat(magUbicazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MagUbicazione.class);
        MagUbicazione magUbicazione1 = new MagUbicazione();
        magUbicazione1.setId(1L);
        MagUbicazione magUbicazione2 = new MagUbicazione();
        magUbicazione2.setId(magUbicazione1.getId());
        assertThat(magUbicazione1).isEqualTo(magUbicazione2);
        magUbicazione2.setId(2L);
        assertThat(magUbicazione1).isNotEqualTo(magUbicazione2);
        magUbicazione1.setId(null);
        assertThat(magUbicazione1).isNotEqualTo(magUbicazione2);
    }
}
