package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoIrr;
import com.terrambe.domain.RegOpcIrr;
import com.terrambe.repository.TipoIrrRepository;
import com.terrambe.service.TipoIrrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoIrrCriteria;
import com.terrambe.service.TipoIrrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoIrrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoIrrResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoIrrRepository tipoIrrRepository;

    @Autowired
    private TipoIrrService tipoIrrService;

    @Autowired
    private TipoIrrQueryService tipoIrrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoIrrMockMvc;

    private TipoIrr tipoIrr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoIrrResource tipoIrrResource = new TipoIrrResource(tipoIrrService, tipoIrrQueryService);
        this.restTipoIrrMockMvc = MockMvcBuilders.standaloneSetup(tipoIrrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoIrr createEntity(EntityManager em) {
        TipoIrr tipoIrr = new TipoIrr()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoIrr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoIrr createUpdatedEntity(EntityManager em) {
        TipoIrr tipoIrr = new TipoIrr()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoIrr;
    }

    @BeforeEach
    public void initTest() {
        tipoIrr = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoIrr() throws Exception {
        int databaseSizeBeforeCreate = tipoIrrRepository.findAll().size();

        // Create the TipoIrr
        restTipoIrrMockMvc.perform(post("/api/tipo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoIrr)))
            .andExpect(status().isCreated());

        // Validate the TipoIrr in the database
        List<TipoIrr> tipoIrrList = tipoIrrRepository.findAll();
        assertThat(tipoIrrList).hasSize(databaseSizeBeforeCreate + 1);
        TipoIrr testTipoIrr = tipoIrrList.get(tipoIrrList.size() - 1);
        assertThat(testTipoIrr.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoIrr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoIrr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoIrr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoIrr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoIrrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoIrrRepository.findAll().size();

        // Create the TipoIrr with an existing ID
        tipoIrr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoIrrMockMvc.perform(post("/api/tipo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoIrr)))
            .andExpect(status().isBadRequest());

        // Validate the TipoIrr in the database
        List<TipoIrr> tipoIrrList = tipoIrrRepository.findAll();
        assertThat(tipoIrrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoIrrs() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoIrr() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get the tipoIrr
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs/{id}", tipoIrr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoIrr.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoIrrShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoIrrShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoIrrShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoIrrList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoIrrShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where descrizione is not null
        defaultTipoIrrShouldBeFound("descrizione.specified=true");

        // Get all the tipoIrrList where descrizione is null
        defaultTipoIrrShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali is not null
        defaultTipoIrrShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoIrrList where dataInizVali is null
        defaultTipoIrrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoIrrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoIrrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoIrrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali is not null
        defaultTipoIrrShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoIrrList where dataFineVali is null
        defaultTipoIrrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoIrrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoIrrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoIrrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator is not null
        defaultTipoIrrShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoIrrList where userIdCreator is null
        defaultTipoIrrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoIrrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoIrrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoIrrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod is not null
        defaultTipoIrrShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoIrrList where userIdLastMod is null
        defaultTipoIrrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoIrrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);

        // Get all the tipoIrrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoIrrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoIrrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoIrrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoIrrsByTipoIrrToOpcIrrIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoIrrRepository.saveAndFlush(tipoIrr);
        RegOpcIrr tipoIrrToOpcIrr = RegOpcIrrResourceIT.createEntity(em);
        em.persist(tipoIrrToOpcIrr);
        em.flush();
        tipoIrr.addTipoIrrToOpcIrr(tipoIrrToOpcIrr);
        tipoIrrRepository.saveAndFlush(tipoIrr);
        Long tipoIrrToOpcIrrId = tipoIrrToOpcIrr.getId();

        // Get all the tipoIrrList where tipoIrrToOpcIrr equals to tipoIrrToOpcIrrId
        defaultTipoIrrShouldBeFound("tipoIrrToOpcIrrId.equals=" + tipoIrrToOpcIrrId);

        // Get all the tipoIrrList where tipoIrrToOpcIrr equals to tipoIrrToOpcIrrId + 1
        defaultTipoIrrShouldNotBeFound("tipoIrrToOpcIrrId.equals=" + (tipoIrrToOpcIrrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoIrrShouldBeFound(String filter) throws Exception {
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoIrrShouldNotBeFound(String filter) throws Exception {
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoIrr() throws Exception {
        // Get the tipoIrr
        restTipoIrrMockMvc.perform(get("/api/tipo-irrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoIrr() throws Exception {
        // Initialize the database
        tipoIrrService.save(tipoIrr);

        int databaseSizeBeforeUpdate = tipoIrrRepository.findAll().size();

        // Update the tipoIrr
        TipoIrr updatedTipoIrr = tipoIrrRepository.findById(tipoIrr.getId()).get();
        // Disconnect from session so that the updates on updatedTipoIrr are not directly saved in db
        em.detach(updatedTipoIrr);
        updatedTipoIrr
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoIrrMockMvc.perform(put("/api/tipo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoIrr)))
            .andExpect(status().isOk());

        // Validate the TipoIrr in the database
        List<TipoIrr> tipoIrrList = tipoIrrRepository.findAll();
        assertThat(tipoIrrList).hasSize(databaseSizeBeforeUpdate);
        TipoIrr testTipoIrr = tipoIrrList.get(tipoIrrList.size() - 1);
        assertThat(testTipoIrr.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoIrr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoIrr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoIrr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoIrr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoIrr() throws Exception {
        int databaseSizeBeforeUpdate = tipoIrrRepository.findAll().size();

        // Create the TipoIrr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoIrrMockMvc.perform(put("/api/tipo-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoIrr)))
            .andExpect(status().isBadRequest());

        // Validate the TipoIrr in the database
        List<TipoIrr> tipoIrrList = tipoIrrRepository.findAll();
        assertThat(tipoIrrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoIrr() throws Exception {
        // Initialize the database
        tipoIrrService.save(tipoIrr);

        int databaseSizeBeforeDelete = tipoIrrRepository.findAll().size();

        // Delete the tipoIrr
        restTipoIrrMockMvc.perform(delete("/api/tipo-irrs/{id}", tipoIrr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoIrr> tipoIrrList = tipoIrrRepository.findAll();
        assertThat(tipoIrrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoIrr.class);
        TipoIrr tipoIrr1 = new TipoIrr();
        tipoIrr1.setId(1L);
        TipoIrr tipoIrr2 = new TipoIrr();
        tipoIrr2.setId(tipoIrr1.getId());
        assertThat(tipoIrr1).isEqualTo(tipoIrr2);
        tipoIrr2.setId(2L);
        assertThat(tipoIrr1).isNotEqualTo(tipoIrr2);
        tipoIrr1.setId(null);
        assertThat(tipoIrr1).isNotEqualTo(tipoIrr2);
    }
}
