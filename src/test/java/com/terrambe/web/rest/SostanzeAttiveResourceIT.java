package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.SostanzeAttive;
import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.repository.SostanzeAttiveRepository;
import com.terrambe.service.SostanzeAttiveService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.SostanzeAttiveCriteria;
import com.terrambe.service.SostanzeAttiveQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SostanzeAttiveResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class SostanzeAttiveResourceIT {

    private static final String DEFAULT_ID_SOSTANZA = "AAAAAAAAAA";
    private static final String UPDATED_ID_SOSTANZA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private SostanzeAttiveRepository sostanzeAttiveRepository;

    @Autowired
    private SostanzeAttiveService sostanzeAttiveService;

    @Autowired
    private SostanzeAttiveQueryService sostanzeAttiveQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSostanzeAttiveMockMvc;

    private SostanzeAttive sostanzeAttive;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SostanzeAttiveResource sostanzeAttiveResource = new SostanzeAttiveResource(sostanzeAttiveService, sostanzeAttiveQueryService);
        this.restSostanzeAttiveMockMvc = MockMvcBuilders.standaloneSetup(sostanzeAttiveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SostanzeAttive createEntity(EntityManager em) {
        SostanzeAttive sostanzeAttive = new SostanzeAttive()
            .idSostanza(DEFAULT_ID_SOSTANZA)
            .descrizione(DEFAULT_DESCRIZIONE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return sostanzeAttive;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SostanzeAttive createUpdatedEntity(EntityManager em) {
        SostanzeAttive sostanzeAttive = new SostanzeAttive()
            .idSostanza(UPDATED_ID_SOSTANZA)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return sostanzeAttive;
    }

    @BeforeEach
    public void initTest() {
        sostanzeAttive = createEntity(em);
    }

    @Test
    @Transactional
    public void createSostanzeAttive() throws Exception {
        int databaseSizeBeforeCreate = sostanzeAttiveRepository.findAll().size();

        // Create the SostanzeAttive
        restSostanzeAttiveMockMvc.perform(post("/api/sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sostanzeAttive)))
            .andExpect(status().isCreated());

        // Validate the SostanzeAttive in the database
        List<SostanzeAttive> sostanzeAttiveList = sostanzeAttiveRepository.findAll();
        assertThat(sostanzeAttiveList).hasSize(databaseSizeBeforeCreate + 1);
        SostanzeAttive testSostanzeAttive = sostanzeAttiveList.get(sostanzeAttiveList.size() - 1);
        assertThat(testSostanzeAttive.getIdSostanza()).isEqualTo(DEFAULT_ID_SOSTANZA);
        assertThat(testSostanzeAttive.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testSostanzeAttive.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testSostanzeAttive.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testSostanzeAttive.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testSostanzeAttive.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testSostanzeAttive.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testSostanzeAttive.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testSostanzeAttive.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testSostanzeAttive.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createSostanzeAttiveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sostanzeAttiveRepository.findAll().size();

        // Create the SostanzeAttive with an existing ID
        sostanzeAttive.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSostanzeAttiveMockMvc.perform(post("/api/sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sostanzeAttive)))
            .andExpect(status().isBadRequest());

        // Validate the SostanzeAttive in the database
        List<SostanzeAttive> sostanzeAttiveList = sostanzeAttiveRepository.findAll();
        assertThat(sostanzeAttiveList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSostanzeAttives() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sostanzeAttive.getId().intValue())))
            .andExpect(jsonPath("$.[*].idSostanza").value(hasItem(DEFAULT_ID_SOSTANZA.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getSostanzeAttive() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get the sostanzeAttive
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives/{id}", sostanzeAttive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sostanzeAttive.getId().intValue()))
            .andExpect(jsonPath("$.idSostanza").value(DEFAULT_ID_SOSTANZA.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByIdSostanzaIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where idSostanza equals to DEFAULT_ID_SOSTANZA
        defaultSostanzeAttiveShouldBeFound("idSostanza.equals=" + DEFAULT_ID_SOSTANZA);

        // Get all the sostanzeAttiveList where idSostanza equals to UPDATED_ID_SOSTANZA
        defaultSostanzeAttiveShouldNotBeFound("idSostanza.equals=" + UPDATED_ID_SOSTANZA);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByIdSostanzaIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where idSostanza in DEFAULT_ID_SOSTANZA or UPDATED_ID_SOSTANZA
        defaultSostanzeAttiveShouldBeFound("idSostanza.in=" + DEFAULT_ID_SOSTANZA + "," + UPDATED_ID_SOSTANZA);

        // Get all the sostanzeAttiveList where idSostanza equals to UPDATED_ID_SOSTANZA
        defaultSostanzeAttiveShouldNotBeFound("idSostanza.in=" + UPDATED_ID_SOSTANZA);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByIdSostanzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where idSostanza is not null
        defaultSostanzeAttiveShouldBeFound("idSostanza.specified=true");

        // Get all the sostanzeAttiveList where idSostanza is null
        defaultSostanzeAttiveShouldNotBeFound("idSostanza.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultSostanzeAttiveShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the sostanzeAttiveList where descrizione equals to UPDATED_DESCRIZIONE
        defaultSostanzeAttiveShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultSostanzeAttiveShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the sostanzeAttiveList where descrizione equals to UPDATED_DESCRIZIONE
        defaultSostanzeAttiveShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where descrizione is not null
        defaultSostanzeAttiveShouldBeFound("descrizione.specified=true");

        // Get all the sostanzeAttiveList where descrizione is null
        defaultSostanzeAttiveShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultSostanzeAttiveShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the sostanzeAttiveList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultSostanzeAttiveShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultSostanzeAttiveShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the sostanzeAttiveList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultSostanzeAttiveShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where tipoImport is not null
        defaultSostanzeAttiveShouldBeFound("tipoImport.specified=true");

        // Get all the sostanzeAttiveList where tipoImport is null
        defaultSostanzeAttiveShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where operatore equals to DEFAULT_OPERATORE
        defaultSostanzeAttiveShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the sostanzeAttiveList where operatore equals to UPDATED_OPERATORE
        defaultSostanzeAttiveShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultSostanzeAttiveShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the sostanzeAttiveList where operatore equals to UPDATED_OPERATORE
        defaultSostanzeAttiveShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where operatore is not null
        defaultSostanzeAttiveShouldBeFound("operatore.specified=true");

        // Get all the sostanzeAttiveList where operatore is null
        defaultSostanzeAttiveShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where ts equals to DEFAULT_TS
        defaultSostanzeAttiveShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the sostanzeAttiveList where ts equals to UPDATED_TS
        defaultSostanzeAttiveShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTsIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where ts in DEFAULT_TS or UPDATED_TS
        defaultSostanzeAttiveShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the sostanzeAttiveList where ts equals to UPDATED_TS
        defaultSostanzeAttiveShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where ts is not null
        defaultSostanzeAttiveShouldBeFound("ts.specified=true");

        // Get all the sostanzeAttiveList where ts is null
        defaultSostanzeAttiveShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali is not null
        defaultSostanzeAttiveShouldBeFound("dataInizVali.specified=true");

        // Get all the sostanzeAttiveList where dataInizVali is null
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the sostanzeAttiveList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultSostanzeAttiveShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali is not null
        defaultSostanzeAttiveShouldBeFound("dataFineVali.specified=true");

        // Get all the sostanzeAttiveList where dataFineVali is null
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultSostanzeAttiveShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the sostanzeAttiveList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultSostanzeAttiveShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator is not null
        defaultSostanzeAttiveShouldBeFound("userIdCreator.specified=true");

        // Get all the sostanzeAttiveList where userIdCreator is null
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultSostanzeAttiveShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the sostanzeAttiveList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultSostanzeAttiveShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod is not null
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.specified=true");

        // Get all the sostanzeAttiveList where userIdLastMod is null
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllSostanzeAttivesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);

        // Get all the sostanzeAttiveList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the sostanzeAttiveList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultSostanzeAttiveShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllSostanzeAttivesBySostAttToIncompIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);
        IncompatibilitaSA sostAttToIncomp = IncompatibilitaSAResourceIT.createEntity(em);
        em.persist(sostAttToIncomp);
        em.flush();
        sostanzeAttive.addSostAttToIncomp(sostAttToIncomp);
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);
        Long sostAttToIncompId = sostAttToIncomp.getId();

        // Get all the sostanzeAttiveList where sostAttToIncomp equals to sostAttToIncompId
        defaultSostanzeAttiveShouldBeFound("sostAttToIncompId.equals=" + sostAttToIncompId);

        // Get all the sostanzeAttiveList where sostAttToIncomp equals to sostAttToIncompId + 1
        defaultSostanzeAttiveShouldNotBeFound("sostAttToIncompId.equals=" + (sostAttToIncompId + 1));
    }


    @Test
    @Transactional
    public void getAllSostanzeAttivesBySostAttToEtiSostAttIsEqualToSomething() throws Exception {
        // Initialize the database
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);
        EtichettaSostanzeAttive sostAttToEtiSostAtt = EtichettaSostanzeAttiveResourceIT.createEntity(em);
        em.persist(sostAttToEtiSostAtt);
        em.flush();
        sostanzeAttive.addSostAttToEtiSostAtt(sostAttToEtiSostAtt);
        sostanzeAttiveRepository.saveAndFlush(sostanzeAttive);
        Long sostAttToEtiSostAttId = sostAttToEtiSostAtt.getId();

        // Get all the sostanzeAttiveList where sostAttToEtiSostAtt equals to sostAttToEtiSostAttId
        defaultSostanzeAttiveShouldBeFound("sostAttToEtiSostAttId.equals=" + sostAttToEtiSostAttId);

        // Get all the sostanzeAttiveList where sostAttToEtiSostAtt equals to sostAttToEtiSostAttId + 1
        defaultSostanzeAttiveShouldNotBeFound("sostAttToEtiSostAttId.equals=" + (sostAttToEtiSostAttId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSostanzeAttiveShouldBeFound(String filter) throws Exception {
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sostanzeAttive.getId().intValue())))
            .andExpect(jsonPath("$.[*].idSostanza").value(hasItem(DEFAULT_ID_SOSTANZA)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSostanzeAttiveShouldNotBeFound(String filter) throws Exception {
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSostanzeAttive() throws Exception {
        // Get the sostanzeAttive
        restSostanzeAttiveMockMvc.perform(get("/api/sostanze-attives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSostanzeAttive() throws Exception {
        // Initialize the database
        sostanzeAttiveService.save(sostanzeAttive);

        int databaseSizeBeforeUpdate = sostanzeAttiveRepository.findAll().size();

        // Update the sostanzeAttive
        SostanzeAttive updatedSostanzeAttive = sostanzeAttiveRepository.findById(sostanzeAttive.getId()).get();
        // Disconnect from session so that the updates on updatedSostanzeAttive are not directly saved in db
        em.detach(updatedSostanzeAttive);
        updatedSostanzeAttive
            .idSostanza(UPDATED_ID_SOSTANZA)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restSostanzeAttiveMockMvc.perform(put("/api/sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSostanzeAttive)))
            .andExpect(status().isOk());

        // Validate the SostanzeAttive in the database
        List<SostanzeAttive> sostanzeAttiveList = sostanzeAttiveRepository.findAll();
        assertThat(sostanzeAttiveList).hasSize(databaseSizeBeforeUpdate);
        SostanzeAttive testSostanzeAttive = sostanzeAttiveList.get(sostanzeAttiveList.size() - 1);
        assertThat(testSostanzeAttive.getIdSostanza()).isEqualTo(UPDATED_ID_SOSTANZA);
        assertThat(testSostanzeAttive.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testSostanzeAttive.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testSostanzeAttive.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testSostanzeAttive.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testSostanzeAttive.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testSostanzeAttive.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testSostanzeAttive.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testSostanzeAttive.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testSostanzeAttive.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingSostanzeAttive() throws Exception {
        int databaseSizeBeforeUpdate = sostanzeAttiveRepository.findAll().size();

        // Create the SostanzeAttive

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSostanzeAttiveMockMvc.perform(put("/api/sostanze-attives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sostanzeAttive)))
            .andExpect(status().isBadRequest());

        // Validate the SostanzeAttive in the database
        List<SostanzeAttive> sostanzeAttiveList = sostanzeAttiveRepository.findAll();
        assertThat(sostanzeAttiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSostanzeAttive() throws Exception {
        // Initialize the database
        sostanzeAttiveService.save(sostanzeAttive);

        int databaseSizeBeforeDelete = sostanzeAttiveRepository.findAll().size();

        // Delete the sostanzeAttive
        restSostanzeAttiveMockMvc.perform(delete("/api/sostanze-attives/{id}", sostanzeAttive.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SostanzeAttive> sostanzeAttiveList = sostanzeAttiveRepository.findAll();
        assertThat(sostanzeAttiveList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SostanzeAttive.class);
        SostanzeAttive sostanzeAttive1 = new SostanzeAttive();
        sostanzeAttive1.setId(1L);
        SostanzeAttive sostanzeAttive2 = new SostanzeAttive();
        sostanzeAttive2.setId(sostanzeAttive1.getId());
        assertThat(sostanzeAttive1).isEqualTo(sostanzeAttive2);
        sostanzeAttive2.setId(2L);
        assertThat(sostanzeAttive1).isNotEqualTo(sostanzeAttive2);
        sostanzeAttive1.setId(null);
        assertThat(sostanzeAttive1).isNotEqualTo(sostanzeAttive2);
    }
}
