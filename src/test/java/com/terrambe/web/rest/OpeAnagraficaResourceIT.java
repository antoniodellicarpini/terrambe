package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.domain.IndNascOpeAna;
import com.terrambe.domain.IndOpeAna;
import com.terrambe.domain.RecOpeAna;
import com.terrambe.domain.PermessiOperatore;
import com.terrambe.domain.OpePatentino;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.OpeQualifica;
import com.terrambe.domain.Fornitori;
import com.terrambe.domain.TipoContratto;
import com.terrambe.repository.OpeAnagraficaRepository;
import com.terrambe.service.OpeAnagraficaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.OpeAnagraficaCriteria;
import com.terrambe.service.OpeAnagraficaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OpeAnagraficaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class OpeAnagraficaResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Boolean DEFAULT_ATTIVO = false;
    private static final Boolean UPDATED_ATTIVO = true;

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME = "BBBBBBBBBB";

    private static final String DEFAULT_SESSO = "AAAAAAAAAA";
    private static final String UPDATED_SESSO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_NASCITA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_NASCITA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_NASCITA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CODICE_FISCALE = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODICE_FISCALE = "BBBBBBBBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private OpeAnagraficaRepository opeAnagraficaRepository;

    @Mock
    private OpeAnagraficaRepository opeAnagraficaRepositoryMock;

    @Mock
    private OpeAnagraficaService opeAnagraficaServiceMock;

    @Autowired
    private OpeAnagraficaService opeAnagraficaService;

    @Autowired
    private OpeAnagraficaQueryService opeAnagraficaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOpeAnagraficaMockMvc;

    private OpeAnagrafica opeAnagrafica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OpeAnagraficaResource opeAnagraficaResource = new OpeAnagraficaResource(opeAnagraficaService, opeAnagraficaQueryService);
        this.restOpeAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(opeAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpeAnagrafica createEntity(EntityManager em) {
        OpeAnagrafica opeAnagrafica = new OpeAnagrafica()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .attivo(DEFAULT_ATTIVO)
            .nome(DEFAULT_NOME)
            .cognome(DEFAULT_COGNOME)
            .sesso(DEFAULT_SESSO)
            .dataNascita(DEFAULT_DATA_NASCITA)
            .codiceFiscale(DEFAULT_CODICE_FISCALE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return opeAnagrafica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpeAnagrafica createUpdatedEntity(EntityManager em) {
        OpeAnagrafica opeAnagrafica = new OpeAnagrafica()
            .idAzienda(UPDATED_ID_AZIENDA)
            .attivo(UPDATED_ATTIVO)
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return opeAnagrafica;
    }

    @BeforeEach
    public void initTest() {
        opeAnagrafica = createEntity(em);
    }

    @Test
    @Transactional
    public void createOpeAnagrafica() throws Exception {
        int databaseSizeBeforeCreate = opeAnagraficaRepository.findAll().size();

        // Create the OpeAnagrafica
        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isCreated());

        // Validate the OpeAnagrafica in the database
        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeCreate + 1);
        OpeAnagrafica testOpeAnagrafica = opeAnagraficaList.get(opeAnagraficaList.size() - 1);
        assertThat(testOpeAnagrafica.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testOpeAnagrafica.isAttivo()).isEqualTo(DEFAULT_ATTIVO);
        assertThat(testOpeAnagrafica.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testOpeAnagrafica.getCognome()).isEqualTo(DEFAULT_COGNOME);
        assertThat(testOpeAnagrafica.getSesso()).isEqualTo(DEFAULT_SESSO);
        assertThat(testOpeAnagrafica.getDataNascita()).isEqualTo(DEFAULT_DATA_NASCITA);
        assertThat(testOpeAnagrafica.getCodiceFiscale()).isEqualTo(DEFAULT_CODICE_FISCALE);
        assertThat(testOpeAnagrafica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testOpeAnagrafica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testOpeAnagrafica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testOpeAnagrafica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testOpeAnagrafica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createOpeAnagraficaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = opeAnagraficaRepository.findAll().size();

        // Create the OpeAnagrafica with an existing ID
        opeAnagrafica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the OpeAnagrafica in the database
        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeAnagraficaRepository.findAll().size();
        // set the field null
        opeAnagrafica.setIdAzienda(null);

        // Create the OpeAnagrafica, which fails.

        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeAnagraficaRepository.findAll().size();
        // set the field null
        opeAnagrafica.setNome(null);

        // Create the OpeAnagrafica, which fails.

        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCognomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeAnagraficaRepository.findAll().size();
        // set the field null
        opeAnagrafica.setCognome(null);

        // Create the OpeAnagrafica, which fails.

        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataNascitaIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeAnagraficaRepository.findAll().size();
        // set the field null
        opeAnagrafica.setDataNascita(null);

        // Create the OpeAnagrafica, which fails.

        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodiceFiscaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeAnagraficaRepository.findAll().size();
        // set the field null
        opeAnagrafica.setCodiceFiscale(null);

        // Create the OpeAnagrafica, which fails.

        restOpeAnagraficaMockMvc.perform(post("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficas() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opeAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME.toString())))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO.toString())))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllOpeAnagraficasWithEagerRelationshipsIsEnabled() throws Exception {
        OpeAnagraficaResource opeAnagraficaResource = new OpeAnagraficaResource(opeAnagraficaServiceMock, opeAnagraficaQueryService);
        when(opeAnagraficaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restOpeAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(opeAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas?eagerload=true"))
        .andExpect(status().isOk());

        verify(opeAnagraficaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllOpeAnagraficasWithEagerRelationshipsIsNotEnabled() throws Exception {
        OpeAnagraficaResource opeAnagraficaResource = new OpeAnagraficaResource(opeAnagraficaServiceMock, opeAnagraficaQueryService);
            when(opeAnagraficaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restOpeAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(opeAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas?eagerload=true"))
        .andExpect(status().isOk());

            verify(opeAnagraficaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getOpeAnagrafica() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get the opeAnagrafica
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas/{id}", opeAnagrafica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(opeAnagrafica.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.attivo").value(DEFAULT_ATTIVO.booleanValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME.toString()))
            .andExpect(jsonPath("$.sesso").value(DEFAULT_SESSO.toString()))
            .andExpect(jsonPath("$.dataNascita").value(DEFAULT_DATA_NASCITA.toString()))
            .andExpect(jsonPath("$.codiceFiscale").value(DEFAULT_CODICE_FISCALE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda is not null
        defaultOpeAnagraficaShouldBeFound("idAzienda.specified=true");

        // Get all the opeAnagraficaList where idAzienda is null
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultOpeAnagraficaShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the opeAnagraficaList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultOpeAnagraficaShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByAttivoIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where attivo equals to DEFAULT_ATTIVO
        defaultOpeAnagraficaShouldBeFound("attivo.equals=" + DEFAULT_ATTIVO);

        // Get all the opeAnagraficaList where attivo equals to UPDATED_ATTIVO
        defaultOpeAnagraficaShouldNotBeFound("attivo.equals=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByAttivoIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where attivo in DEFAULT_ATTIVO or UPDATED_ATTIVO
        defaultOpeAnagraficaShouldBeFound("attivo.in=" + DEFAULT_ATTIVO + "," + UPDATED_ATTIVO);

        // Get all the opeAnagraficaList where attivo equals to UPDATED_ATTIVO
        defaultOpeAnagraficaShouldNotBeFound("attivo.in=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByAttivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where attivo is not null
        defaultOpeAnagraficaShouldBeFound("attivo.specified=true");

        // Get all the opeAnagraficaList where attivo is null
        defaultOpeAnagraficaShouldNotBeFound("attivo.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where nome equals to DEFAULT_NOME
        defaultOpeAnagraficaShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the opeAnagraficaList where nome equals to UPDATED_NOME
        defaultOpeAnagraficaShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultOpeAnagraficaShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the opeAnagraficaList where nome equals to UPDATED_NOME
        defaultOpeAnagraficaShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where nome is not null
        defaultOpeAnagraficaShouldBeFound("nome.specified=true");

        // Get all the opeAnagraficaList where nome is null
        defaultOpeAnagraficaShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByCognomeIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where cognome equals to DEFAULT_COGNOME
        defaultOpeAnagraficaShouldBeFound("cognome.equals=" + DEFAULT_COGNOME);

        // Get all the opeAnagraficaList where cognome equals to UPDATED_COGNOME
        defaultOpeAnagraficaShouldNotBeFound("cognome.equals=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByCognomeIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where cognome in DEFAULT_COGNOME or UPDATED_COGNOME
        defaultOpeAnagraficaShouldBeFound("cognome.in=" + DEFAULT_COGNOME + "," + UPDATED_COGNOME);

        // Get all the opeAnagraficaList where cognome equals to UPDATED_COGNOME
        defaultOpeAnagraficaShouldNotBeFound("cognome.in=" + UPDATED_COGNOME);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByCognomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where cognome is not null
        defaultOpeAnagraficaShouldBeFound("cognome.specified=true");

        // Get all the opeAnagraficaList where cognome is null
        defaultOpeAnagraficaShouldNotBeFound("cognome.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasBySessoIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where sesso equals to DEFAULT_SESSO
        defaultOpeAnagraficaShouldBeFound("sesso.equals=" + DEFAULT_SESSO);

        // Get all the opeAnagraficaList where sesso equals to UPDATED_SESSO
        defaultOpeAnagraficaShouldNotBeFound("sesso.equals=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasBySessoIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where sesso in DEFAULT_SESSO or UPDATED_SESSO
        defaultOpeAnagraficaShouldBeFound("sesso.in=" + DEFAULT_SESSO + "," + UPDATED_SESSO);

        // Get all the opeAnagraficaList where sesso equals to UPDATED_SESSO
        defaultOpeAnagraficaShouldNotBeFound("sesso.in=" + UPDATED_SESSO);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasBySessoIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where sesso is not null
        defaultOpeAnagraficaShouldBeFound("sesso.specified=true");

        // Get all the opeAnagraficaList where sesso is null
        defaultOpeAnagraficaShouldNotBeFound("sesso.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita equals to DEFAULT_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.equals=" + DEFAULT_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.equals=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita in DEFAULT_DATA_NASCITA or UPDATED_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.in=" + DEFAULT_DATA_NASCITA + "," + UPDATED_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita equals to UPDATED_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.in=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita is not null
        defaultOpeAnagraficaShouldBeFound("dataNascita.specified=true");

        // Get all the opeAnagraficaList where dataNascita is null
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita is greater than or equal to DEFAULT_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.greaterThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita is greater than or equal to UPDATED_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.greaterThanOrEqual=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita is less than or equal to DEFAULT_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.lessThanOrEqual=" + DEFAULT_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita is less than or equal to SMALLER_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.lessThanOrEqual=" + SMALLER_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita is less than DEFAULT_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.lessThan=" + DEFAULT_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita is less than UPDATED_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.lessThan=" + UPDATED_DATA_NASCITA);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataNascitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataNascita is greater than DEFAULT_DATA_NASCITA
        defaultOpeAnagraficaShouldNotBeFound("dataNascita.greaterThan=" + DEFAULT_DATA_NASCITA);

        // Get all the opeAnagraficaList where dataNascita is greater than SMALLER_DATA_NASCITA
        defaultOpeAnagraficaShouldBeFound("dataNascita.greaterThan=" + SMALLER_DATA_NASCITA);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByCodiceFiscaleIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where codiceFiscale equals to DEFAULT_CODICE_FISCALE
        defaultOpeAnagraficaShouldBeFound("codiceFiscale.equals=" + DEFAULT_CODICE_FISCALE);

        // Get all the opeAnagraficaList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultOpeAnagraficaShouldNotBeFound("codiceFiscale.equals=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByCodiceFiscaleIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where codiceFiscale in DEFAULT_CODICE_FISCALE or UPDATED_CODICE_FISCALE
        defaultOpeAnagraficaShouldBeFound("codiceFiscale.in=" + DEFAULT_CODICE_FISCALE + "," + UPDATED_CODICE_FISCALE);

        // Get all the opeAnagraficaList where codiceFiscale equals to UPDATED_CODICE_FISCALE
        defaultOpeAnagraficaShouldNotBeFound("codiceFiscale.in=" + UPDATED_CODICE_FISCALE);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByCodiceFiscaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where codiceFiscale is not null
        defaultOpeAnagraficaShouldBeFound("codiceFiscale.specified=true");

        // Get all the opeAnagraficaList where codiceFiscale is null
        defaultOpeAnagraficaShouldNotBeFound("codiceFiscale.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali is not null
        defaultOpeAnagraficaShouldBeFound("dataInizVali.specified=true");

        // Get all the opeAnagraficaList where dataInizVali is null
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeAnagraficaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultOpeAnagraficaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali is not null
        defaultOpeAnagraficaShouldBeFound("dataFineVali.specified=true");

        // Get all the opeAnagraficaList where dataFineVali is null
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultOpeAnagraficaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeAnagraficaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultOpeAnagraficaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator is not null
        defaultOpeAnagraficaShouldBeFound("userIdCreator.specified=true");

        // Get all the opeAnagraficaList where userIdCreator is null
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultOpeAnagraficaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeAnagraficaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultOpeAnagraficaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod is not null
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.specified=true");

        // Get all the opeAnagraficaList where userIdLastMod is null
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeAnagraficasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);

        // Get all the opeAnagraficaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeAnagraficaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultOpeAnagraficaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnaToIndNascIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        IndNascOpeAna opeAnaToIndNasc = IndNascOpeAnaResourceIT.createEntity(em);
        em.persist(opeAnaToIndNasc);
        em.flush();
        opeAnagrafica.setOpeAnaToIndNasc(opeAnaToIndNasc);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnaToIndNascId = opeAnaToIndNasc.getId();

        // Get all the opeAnagraficaList where opeAnaToIndNasc equals to opeAnaToIndNascId
        defaultOpeAnagraficaShouldBeFound("opeAnaToIndNascId.equals=" + opeAnaToIndNascId);

        // Get all the opeAnagraficaList where opeAnaToIndNasc equals to opeAnaToIndNascId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnaToIndNascId.equals=" + (opeAnaToIndNascId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnaToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        IndOpeAna opeAnaToInd = IndOpeAnaResourceIT.createEntity(em);
        em.persist(opeAnaToInd);
        em.flush();
        opeAnagrafica.addOpeAnaToInd(opeAnaToInd);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnaToIndId = opeAnaToInd.getId();

        // Get all the opeAnagraficaList where opeAnaToInd equals to opeAnaToIndId
        defaultOpeAnagraficaShouldBeFound("opeAnaToIndId.equals=" + opeAnaToIndId);

        // Get all the opeAnagraficaList where opeAnaToInd equals to opeAnaToIndId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnaToIndId.equals=" + (opeAnaToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnaToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        RecOpeAna opeAnaToRec = RecOpeAnaResourceIT.createEntity(em);
        em.persist(opeAnaToRec);
        em.flush();
        opeAnagrafica.addOpeAnaToRec(opeAnaToRec);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnaToRecId = opeAnaToRec.getId();

        // Get all the opeAnagraficaList where opeAnaToRec equals to opeAnaToRecId
        defaultOpeAnagraficaShouldBeFound("opeAnaToRecId.equals=" + opeAnaToRecId);

        // Get all the opeAnagraficaList where opeAnaToRec equals to opeAnaToRecId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnaToRecId.equals=" + (opeAnaToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByPermessioperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        PermessiOperatore permessioperatore = PermessiOperatoreResourceIT.createEntity(em);
        em.persist(permessioperatore);
        em.flush();
        opeAnagrafica.addPermessioperatore(permessioperatore);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long permessioperatoreId = permessioperatore.getId();

        // Get all the opeAnagraficaList where permessioperatore equals to permessioperatoreId
        defaultOpeAnagraficaShouldBeFound("permessioperatoreId.equals=" + permessioperatoreId);

        // Get all the opeAnagraficaList where permessioperatore equals to permessioperatoreId + 1
        defaultOpeAnagraficaShouldNotBeFound("permessioperatoreId.equals=" + (permessioperatoreId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnagToPateIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        OpePatentino opeAnagToPate = OpePatentinoResourceIT.createEntity(em);
        em.persist(opeAnagToPate);
        em.flush();
        opeAnagrafica.addOpeAnagToPate(opeAnagToPate);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnagToPateId = opeAnagToPate.getId();

        // Get all the opeAnagraficaList where opeAnagToPate equals to opeAnagToPateId
        defaultOpeAnagraficaShouldBeFound("opeAnagToPateId.equals=" + opeAnagToPateId);

        // Get all the opeAnagraficaList where opeAnagToPate equals to opeAnagToPateId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnagToPateId.equals=" + (opeAnagToPateId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOperatoriaziIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        AziAnagrafica operatoriazi = AziAnagraficaResourceIT.createEntity(em);
        em.persist(operatoriazi);
        em.flush();
        opeAnagrafica.setOperatoriazi(operatoriazi);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long operatoriaziId = operatoriazi.getId();

        // Get all the opeAnagraficaList where operatoriazi equals to operatoriaziId
        defaultOpeAnagraficaShouldBeFound("operatoriaziId.equals=" + operatoriaziId);

        // Get all the opeAnagraficaList where operatoriazi equals to operatoriaziId + 1
        defaultOpeAnagraficaShouldNotBeFound("operatoriaziId.equals=" + (operatoriaziId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnagToOpeQualIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        OpeQualifica opeAnagToOpeQual = OpeQualificaResourceIT.createEntity(em);
        em.persist(opeAnagToOpeQual);
        em.flush();
        opeAnagrafica.setOpeAnagToOpeQual(opeAnagToOpeQual);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnagToOpeQualId = opeAnagToOpeQual.getId();

        // Get all the opeAnagraficaList where opeAnagToOpeQual equals to opeAnagToOpeQualId
        defaultOpeAnagraficaShouldBeFound("opeAnagToOpeQualId.equals=" + opeAnagToOpeQualId);

        // Get all the opeAnagraficaList where opeAnagToOpeQual equals to opeAnagToOpeQualId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnagToOpeQualId.equals=" + (opeAnagToOpeQualId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeAnaToFornIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Fornitori opeAnaToForn = FornitoriResourceIT.createEntity(em);
        em.persist(opeAnaToForn);
        em.flush();
        opeAnagrafica.addOpeAnaToForn(opeAnaToForn);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeAnaToFornId = opeAnaToForn.getId();

        // Get all the opeAnagraficaList where opeAnaToForn equals to opeAnaToFornId
        defaultOpeAnagraficaShouldBeFound("opeAnaToFornId.equals=" + opeAnaToFornId);

        // Get all the opeAnagraficaList where opeAnaToForn equals to opeAnaToFornId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeAnaToFornId.equals=" + (opeAnaToFornId + 1));
    }


    @Test
    @Transactional
    public void getAllOpeAnagraficasByOpeToTipoContratIsEqualToSomething() throws Exception {
        // Initialize the database
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        TipoContratto opeToTipoContrat = TipoContrattoResourceIT.createEntity(em);
        em.persist(opeToTipoContrat);
        em.flush();
        opeAnagrafica.setOpeToTipoContrat(opeToTipoContrat);
        opeAnagraficaRepository.saveAndFlush(opeAnagrafica);
        Long opeToTipoContratId = opeToTipoContrat.getId();

        // Get all the opeAnagraficaList where opeToTipoContrat equals to opeToTipoContratId
        defaultOpeAnagraficaShouldBeFound("opeToTipoContratId.equals=" + opeToTipoContratId);

        // Get all the opeAnagraficaList where opeToTipoContrat equals to opeToTipoContratId + 1
        defaultOpeAnagraficaShouldNotBeFound("opeToTipoContratId.equals=" + (opeToTipoContratId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOpeAnagraficaShouldBeFound(String filter) throws Exception {
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opeAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME)))
            .andExpect(jsonPath("$.[*].sesso").value(hasItem(DEFAULT_SESSO)))
            .andExpect(jsonPath("$.[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOpeAnagraficaShouldNotBeFound(String filter) throws Exception {
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOpeAnagrafica() throws Exception {
        // Get the opeAnagrafica
        restOpeAnagraficaMockMvc.perform(get("/api/ope-anagraficas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOpeAnagrafica() throws Exception {
        // Initialize the database
        opeAnagraficaService.save(opeAnagrafica);

        int databaseSizeBeforeUpdate = opeAnagraficaRepository.findAll().size();

        // Update the opeAnagrafica
        OpeAnagrafica updatedOpeAnagrafica = opeAnagraficaRepository.findById(opeAnagrafica.getId()).get();
        // Disconnect from session so that the updates on updatedOpeAnagrafica are not directly saved in db
        em.detach(updatedOpeAnagrafica);
        updatedOpeAnagrafica
            .idAzienda(UPDATED_ID_AZIENDA)
            .attivo(UPDATED_ATTIVO)
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .sesso(UPDATED_SESSO)
            .dataNascita(UPDATED_DATA_NASCITA)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restOpeAnagraficaMockMvc.perform(put("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOpeAnagrafica)))
            .andExpect(status().isOk());

        // Validate the OpeAnagrafica in the database
        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeUpdate);
        OpeAnagrafica testOpeAnagrafica = opeAnagraficaList.get(opeAnagraficaList.size() - 1);
        assertThat(testOpeAnagrafica.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testOpeAnagrafica.isAttivo()).isEqualTo(UPDATED_ATTIVO);
        assertThat(testOpeAnagrafica.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testOpeAnagrafica.getCognome()).isEqualTo(UPDATED_COGNOME);
        assertThat(testOpeAnagrafica.getSesso()).isEqualTo(UPDATED_SESSO);
        assertThat(testOpeAnagrafica.getDataNascita()).isEqualTo(UPDATED_DATA_NASCITA);
        assertThat(testOpeAnagrafica.getCodiceFiscale()).isEqualTo(UPDATED_CODICE_FISCALE);
        assertThat(testOpeAnagrafica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testOpeAnagrafica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testOpeAnagrafica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testOpeAnagrafica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testOpeAnagrafica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingOpeAnagrafica() throws Exception {
        int databaseSizeBeforeUpdate = opeAnagraficaRepository.findAll().size();

        // Create the OpeAnagrafica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOpeAnagraficaMockMvc.perform(put("/api/ope-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the OpeAnagrafica in the database
        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOpeAnagrafica() throws Exception {
        // Initialize the database
        opeAnagraficaService.save(opeAnagrafica);

        int databaseSizeBeforeDelete = opeAnagraficaRepository.findAll().size();

        // Delete the opeAnagrafica
        restOpeAnagraficaMockMvc.perform(delete("/api/ope-anagraficas/{id}", opeAnagrafica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OpeAnagrafica> opeAnagraficaList = opeAnagraficaRepository.findAll();
        assertThat(opeAnagraficaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpeAnagrafica.class);
        OpeAnagrafica opeAnagrafica1 = new OpeAnagrafica();
        opeAnagrafica1.setId(1L);
        OpeAnagrafica opeAnagrafica2 = new OpeAnagrafica();
        opeAnagrafica2.setId(opeAnagrafica1.getId());
        assertThat(opeAnagrafica1).isEqualTo(opeAnagrafica2);
        opeAnagrafica2.setId(2L);
        assertThat(opeAnagrafica1).isNotEqualTo(opeAnagrafica2);
        opeAnagrafica1.setId(null);
        assertThat(opeAnagrafica1).isNotEqualTo(opeAnagrafica2);
    }
}
