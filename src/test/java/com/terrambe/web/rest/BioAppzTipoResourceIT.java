package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.BioAppzTipo;
import com.terrambe.domain.Appezzamenti;
import com.terrambe.repository.BioAppzTipoRepository;
import com.terrambe.service.BioAppzTipoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.BioAppzTipoCriteria;
import com.terrambe.service.BioAppzTipoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BioAppzTipoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class BioAppzTipoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private BioAppzTipoRepository bioAppzTipoRepository;

    @Autowired
    private BioAppzTipoService bioAppzTipoService;

    @Autowired
    private BioAppzTipoQueryService bioAppzTipoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBioAppzTipoMockMvc;

    private BioAppzTipo bioAppzTipo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BioAppzTipoResource bioAppzTipoResource = new BioAppzTipoResource(bioAppzTipoService, bioAppzTipoQueryService);
        this.restBioAppzTipoMockMvc = MockMvcBuilders.standaloneSetup(bioAppzTipoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BioAppzTipo createEntity(EntityManager em) {
        BioAppzTipo bioAppzTipo = new BioAppzTipo()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return bioAppzTipo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BioAppzTipo createUpdatedEntity(EntityManager em) {
        BioAppzTipo bioAppzTipo = new BioAppzTipo()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return bioAppzTipo;
    }

    @BeforeEach
    public void initTest() {
        bioAppzTipo = createEntity(em);
    }

    @Test
    @Transactional
    public void createBioAppzTipo() throws Exception {
        int databaseSizeBeforeCreate = bioAppzTipoRepository.findAll().size();

        // Create the BioAppzTipo
        restBioAppzTipoMockMvc.perform(post("/api/bio-appz-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bioAppzTipo)))
            .andExpect(status().isCreated());

        // Validate the BioAppzTipo in the database
        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeCreate + 1);
        BioAppzTipo testBioAppzTipo = bioAppzTipoList.get(bioAppzTipoList.size() - 1);
        assertThat(testBioAppzTipo.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testBioAppzTipo.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testBioAppzTipo.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testBioAppzTipo.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testBioAppzTipo.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testBioAppzTipo.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createBioAppzTipoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bioAppzTipoRepository.findAll().size();

        // Create the BioAppzTipo with an existing ID
        bioAppzTipo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBioAppzTipoMockMvc.perform(post("/api/bio-appz-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bioAppzTipo)))
            .andExpect(status().isBadRequest());

        // Validate the BioAppzTipo in the database
        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = bioAppzTipoRepository.findAll().size();
        // set the field null
        bioAppzTipo.setDescrizione(null);

        // Create the BioAppzTipo, which fails.

        restBioAppzTipoMockMvc.perform(post("/api/bio-appz-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bioAppzTipo)))
            .andExpect(status().isBadRequest());

        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBioAppzTipos() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bioAppzTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getBioAppzTipo() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get the bioAppzTipo
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos/{id}", bioAppzTipo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bioAppzTipo.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultBioAppzTipoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the bioAppzTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultBioAppzTipoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultBioAppzTipoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the bioAppzTipoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultBioAppzTipoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where descrizione is not null
        defaultBioAppzTipoShouldBeFound("descrizione.specified=true");

        // Get all the bioAppzTipoList where descrizione is null
        defaultBioAppzTipoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali is not null
        defaultBioAppzTipoShouldBeFound("dataInizVali.specified=true");

        // Get all the bioAppzTipoList where dataInizVali is null
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultBioAppzTipoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the bioAppzTipoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultBioAppzTipoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali is not null
        defaultBioAppzTipoShouldBeFound("dataFineVali.specified=true");

        // Get all the bioAppzTipoList where dataFineVali is null
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultBioAppzTipoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the bioAppzTipoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultBioAppzTipoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator is not null
        defaultBioAppzTipoShouldBeFound("userIdCreator.specified=true");

        // Get all the bioAppzTipoList where userIdCreator is null
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultBioAppzTipoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the bioAppzTipoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultBioAppzTipoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod is not null
        defaultBioAppzTipoShouldBeFound("userIdLastMod.specified=true");

        // Get all the bioAppzTipoList where userIdLastMod is null
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllBioAppzTiposByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);

        // Get all the bioAppzTipoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the bioAppzTipoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultBioAppzTipoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllBioAppzTiposByBioAppzTipoToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);
        Appezzamenti bioAppzTipoToAppz = AppezzamentiResourceIT.createEntity(em);
        em.persist(bioAppzTipoToAppz);
        em.flush();
        bioAppzTipo.addBioAppzTipoToAppz(bioAppzTipoToAppz);
        bioAppzTipoRepository.saveAndFlush(bioAppzTipo);
        Long bioAppzTipoToAppzId = bioAppzTipoToAppz.getId();

        // Get all the bioAppzTipoList where bioAppzTipoToAppz equals to bioAppzTipoToAppzId
        defaultBioAppzTipoShouldBeFound("bioAppzTipoToAppzId.equals=" + bioAppzTipoToAppzId);

        // Get all the bioAppzTipoList where bioAppzTipoToAppz equals to bioAppzTipoToAppzId + 1
        defaultBioAppzTipoShouldNotBeFound("bioAppzTipoToAppzId.equals=" + (bioAppzTipoToAppzId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBioAppzTipoShouldBeFound(String filter) throws Exception {
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bioAppzTipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBioAppzTipoShouldNotBeFound(String filter) throws Exception {
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBioAppzTipo() throws Exception {
        // Get the bioAppzTipo
        restBioAppzTipoMockMvc.perform(get("/api/bio-appz-tipos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBioAppzTipo() throws Exception {
        // Initialize the database
        bioAppzTipoService.save(bioAppzTipo);

        int databaseSizeBeforeUpdate = bioAppzTipoRepository.findAll().size();

        // Update the bioAppzTipo
        BioAppzTipo updatedBioAppzTipo = bioAppzTipoRepository.findById(bioAppzTipo.getId()).get();
        // Disconnect from session so that the updates on updatedBioAppzTipo are not directly saved in db
        em.detach(updatedBioAppzTipo);
        updatedBioAppzTipo
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restBioAppzTipoMockMvc.perform(put("/api/bio-appz-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBioAppzTipo)))
            .andExpect(status().isOk());

        // Validate the BioAppzTipo in the database
        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeUpdate);
        BioAppzTipo testBioAppzTipo = bioAppzTipoList.get(bioAppzTipoList.size() - 1);
        assertThat(testBioAppzTipo.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testBioAppzTipo.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testBioAppzTipo.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testBioAppzTipo.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testBioAppzTipo.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testBioAppzTipo.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingBioAppzTipo() throws Exception {
        int databaseSizeBeforeUpdate = bioAppzTipoRepository.findAll().size();

        // Create the BioAppzTipo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBioAppzTipoMockMvc.perform(put("/api/bio-appz-tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bioAppzTipo)))
            .andExpect(status().isBadRequest());

        // Validate the BioAppzTipo in the database
        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBioAppzTipo() throws Exception {
        // Initialize the database
        bioAppzTipoService.save(bioAppzTipo);

        int databaseSizeBeforeDelete = bioAppzTipoRepository.findAll().size();

        // Delete the bioAppzTipo
        restBioAppzTipoMockMvc.perform(delete("/api/bio-appz-tipos/{id}", bioAppzTipo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BioAppzTipo> bioAppzTipoList = bioAppzTipoRepository.findAll();
        assertThat(bioAppzTipoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BioAppzTipo.class);
        BioAppzTipo bioAppzTipo1 = new BioAppzTipo();
        bioAppzTipo1.setId(1L);
        BioAppzTipo bioAppzTipo2 = new BioAppzTipo();
        bioAppzTipo2.setId(bioAppzTipo1.getId());
        assertThat(bioAppzTipo1).isEqualTo(bioAppzTipo2);
        bioAppzTipo2.setId(2L);
        assertThat(bioAppzTipo1).isNotEqualTo(bioAppzTipo2);
        bioAppzTipo1.setId(null);
        assertThat(bioAppzTipo1).isNotEqualTo(bioAppzTipo2);
    }
}
