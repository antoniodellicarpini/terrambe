package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Comuni;
import com.terrambe.repository.ComuniRepository;
import com.terrambe.service.ComuniService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.ComuniCriteria;
import com.terrambe.service.ComuniQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ComuniResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class ComuniResourceIT {

    private static final String DEFAULT_CODI_PROV = "AAAAAAAAAA";
    private static final String UPDATED_CODI_PROV = "BBBBBBBBBB";

    private static final String DEFAULT_CODI_COMU = "AAAAAAAAAA";
    private static final String UPDATED_CODI_COMU = "BBBBBBBBBB";

    private static final String DEFAULT_DESC_COMU = "AAAAAAAAAA";
    private static final String UPDATED_DESC_COMU = "BBBBBBBBBB";

    private static final String DEFAULT_DESC_PROV = "AAAAAAAAAA";
    private static final String UPDATED_DESC_PROV = "BBBBBBBBBB";

    private static final String DEFAULT_CODI_SIGL_PROV = "AAAAAAAAAA";
    private static final String UPDATED_CODI_SIGL_PROV = "BBBBBBBBBB";

    private static final String DEFAULT_CODI_FISC_LUNA = "AAAAAAAAAA";
    private static final String UPDATED_CODI_FISC_LUNA = "BBBBBBBBBB";

    private static final String DEFAULT_DESC_REGI = "AAAAAAAAAA";
    private static final String UPDATED_DESC_REGI = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_BELFIORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_BELFIORE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private ComuniRepository comuniRepository;

    @Autowired
    private ComuniService comuniService;

    @Autowired
    private ComuniQueryService comuniQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restComuniMockMvc;

    private Comuni comuni;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ComuniResource comuniResource = new ComuniResource(comuniService, comuniQueryService);
        this.restComuniMockMvc = MockMvcBuilders.standaloneSetup(comuniResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comuni createEntity(EntityManager em) {
        Comuni comuni = new Comuni()
            .codiProv(DEFAULT_CODI_PROV)
            .codiComu(DEFAULT_CODI_COMU)
            .descComu(DEFAULT_DESC_COMU)
            .descProv(DEFAULT_DESC_PROV)
            .codiSiglProv(DEFAULT_CODI_SIGL_PROV)
            .codiFiscLuna(DEFAULT_CODI_FISC_LUNA)
            .descRegi(DEFAULT_DESC_REGI)
            .codiceBelfiore(DEFAULT_CODICE_BELFIORE)
            .dataInizio(DEFAULT_DATA_INIZIO)
            .dataFine(DEFAULT_DATA_FINE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return comuni;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comuni createUpdatedEntity(EntityManager em) {
        Comuni comuni = new Comuni()
            .codiProv(UPDATED_CODI_PROV)
            .codiComu(UPDATED_CODI_COMU)
            .descComu(UPDATED_DESC_COMU)
            .descProv(UPDATED_DESC_PROV)
            .codiSiglProv(UPDATED_CODI_SIGL_PROV)
            .codiFiscLuna(UPDATED_CODI_FISC_LUNA)
            .descRegi(UPDATED_DESC_REGI)
            .codiceBelfiore(UPDATED_CODICE_BELFIORE)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return comuni;
    }

    @BeforeEach
    public void initTest() {
        comuni = createEntity(em);
    }

    @Test
    @Transactional
    public void createComuni() throws Exception {
        int databaseSizeBeforeCreate = comuniRepository.findAll().size();

        // Create the Comuni
        restComuniMockMvc.perform(post("/api/comunis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comuni)))
            .andExpect(status().isCreated());

        // Validate the Comuni in the database
        List<Comuni> comuniList = comuniRepository.findAll();
        assertThat(comuniList).hasSize(databaseSizeBeforeCreate + 1);
        Comuni testComuni = comuniList.get(comuniList.size() - 1);
        assertThat(testComuni.getCodiProv()).isEqualTo(DEFAULT_CODI_PROV);
        assertThat(testComuni.getCodiComu()).isEqualTo(DEFAULT_CODI_COMU);
        assertThat(testComuni.getDescComu()).isEqualTo(DEFAULT_DESC_COMU);
        assertThat(testComuni.getDescProv()).isEqualTo(DEFAULT_DESC_PROV);
        assertThat(testComuni.getCodiSiglProv()).isEqualTo(DEFAULT_CODI_SIGL_PROV);
        assertThat(testComuni.getCodiFiscLuna()).isEqualTo(DEFAULT_CODI_FISC_LUNA);
        assertThat(testComuni.getDescRegi()).isEqualTo(DEFAULT_DESC_REGI);
        assertThat(testComuni.getCodiceBelfiore()).isEqualTo(DEFAULT_CODICE_BELFIORE);
        assertThat(testComuni.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
        assertThat(testComuni.getDataFine()).isEqualTo(DEFAULT_DATA_FINE);
        assertThat(testComuni.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testComuni.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testComuni.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testComuni.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testComuni.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createComuniWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = comuniRepository.findAll().size();

        // Create the Comuni with an existing ID
        comuni.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComuniMockMvc.perform(post("/api/comunis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comuni)))
            .andExpect(status().isBadRequest());

        // Validate the Comuni in the database
        List<Comuni> comuniList = comuniRepository.findAll();
        assertThat(comuniList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllComunis() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList
        restComuniMockMvc.perform(get("/api/comunis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comuni.getId().intValue())))
            .andExpect(jsonPath("$.[*].codiProv").value(hasItem(DEFAULT_CODI_PROV.toString())))
            .andExpect(jsonPath("$.[*].codiComu").value(hasItem(DEFAULT_CODI_COMU.toString())))
            .andExpect(jsonPath("$.[*].descComu").value(hasItem(DEFAULT_DESC_COMU.toString())))
            .andExpect(jsonPath("$.[*].descProv").value(hasItem(DEFAULT_DESC_PROV.toString())))
            .andExpect(jsonPath("$.[*].codiSiglProv").value(hasItem(DEFAULT_CODI_SIGL_PROV.toString())))
            .andExpect(jsonPath("$.[*].codiFiscLuna").value(hasItem(DEFAULT_CODI_FISC_LUNA.toString())))
            .andExpect(jsonPath("$.[*].descRegi").value(hasItem(DEFAULT_DESC_REGI.toString())))
            .andExpect(jsonPath("$.[*].codiceBelfiore").value(hasItem(DEFAULT_CODICE_BELFIORE.toString())))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getComuni() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get the comuni
        restComuniMockMvc.perform(get("/api/comunis/{id}", comuni.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(comuni.getId().intValue()))
            .andExpect(jsonPath("$.codiProv").value(DEFAULT_CODI_PROV.toString()))
            .andExpect(jsonPath("$.codiComu").value(DEFAULT_CODI_COMU.toString()))
            .andExpect(jsonPath("$.descComu").value(DEFAULT_DESC_COMU.toString()))
            .andExpect(jsonPath("$.descProv").value(DEFAULT_DESC_PROV.toString()))
            .andExpect(jsonPath("$.codiSiglProv").value(DEFAULT_CODI_SIGL_PROV.toString()))
            .andExpect(jsonPath("$.codiFiscLuna").value(DEFAULT_CODI_FISC_LUNA.toString()))
            .andExpect(jsonPath("$.descRegi").value(DEFAULT_DESC_REGI.toString()))
            .andExpect(jsonPath("$.codiceBelfiore").value(DEFAULT_CODICE_BELFIORE.toString()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllComunisByCodiProvIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiProv equals to DEFAULT_CODI_PROV
        defaultComuniShouldBeFound("codiProv.equals=" + DEFAULT_CODI_PROV);

        // Get all the comuniList where codiProv equals to UPDATED_CODI_PROV
        defaultComuniShouldNotBeFound("codiProv.equals=" + UPDATED_CODI_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiProvIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiProv in DEFAULT_CODI_PROV or UPDATED_CODI_PROV
        defaultComuniShouldBeFound("codiProv.in=" + DEFAULT_CODI_PROV + "," + UPDATED_CODI_PROV);

        // Get all the comuniList where codiProv equals to UPDATED_CODI_PROV
        defaultComuniShouldNotBeFound("codiProv.in=" + UPDATED_CODI_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiProvIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiProv is not null
        defaultComuniShouldBeFound("codiProv.specified=true");

        // Get all the comuniList where codiProv is null
        defaultComuniShouldNotBeFound("codiProv.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByCodiComuIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiComu equals to DEFAULT_CODI_COMU
        defaultComuniShouldBeFound("codiComu.equals=" + DEFAULT_CODI_COMU);

        // Get all the comuniList where codiComu equals to UPDATED_CODI_COMU
        defaultComuniShouldNotBeFound("codiComu.equals=" + UPDATED_CODI_COMU);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiComuIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiComu in DEFAULT_CODI_COMU or UPDATED_CODI_COMU
        defaultComuniShouldBeFound("codiComu.in=" + DEFAULT_CODI_COMU + "," + UPDATED_CODI_COMU);

        // Get all the comuniList where codiComu equals to UPDATED_CODI_COMU
        defaultComuniShouldNotBeFound("codiComu.in=" + UPDATED_CODI_COMU);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiComuIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiComu is not null
        defaultComuniShouldBeFound("codiComu.specified=true");

        // Get all the comuniList where codiComu is null
        defaultComuniShouldNotBeFound("codiComu.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDescComuIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descComu equals to DEFAULT_DESC_COMU
        defaultComuniShouldBeFound("descComu.equals=" + DEFAULT_DESC_COMU);

        // Get all the comuniList where descComu equals to UPDATED_DESC_COMU
        defaultComuniShouldNotBeFound("descComu.equals=" + UPDATED_DESC_COMU);
    }

    @Test
    @Transactional
    public void getAllComunisByDescComuIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descComu in DEFAULT_DESC_COMU or UPDATED_DESC_COMU
        defaultComuniShouldBeFound("descComu.in=" + DEFAULT_DESC_COMU + "," + UPDATED_DESC_COMU);

        // Get all the comuniList where descComu equals to UPDATED_DESC_COMU
        defaultComuniShouldNotBeFound("descComu.in=" + UPDATED_DESC_COMU);
    }

    @Test
    @Transactional
    public void getAllComunisByDescComuIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descComu is not null
        defaultComuniShouldBeFound("descComu.specified=true");

        // Get all the comuniList where descComu is null
        defaultComuniShouldNotBeFound("descComu.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDescProvIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descProv equals to DEFAULT_DESC_PROV
        defaultComuniShouldBeFound("descProv.equals=" + DEFAULT_DESC_PROV);

        // Get all the comuniList where descProv equals to UPDATED_DESC_PROV
        defaultComuniShouldNotBeFound("descProv.equals=" + UPDATED_DESC_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByDescProvIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descProv in DEFAULT_DESC_PROV or UPDATED_DESC_PROV
        defaultComuniShouldBeFound("descProv.in=" + DEFAULT_DESC_PROV + "," + UPDATED_DESC_PROV);

        // Get all the comuniList where descProv equals to UPDATED_DESC_PROV
        defaultComuniShouldNotBeFound("descProv.in=" + UPDATED_DESC_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByDescProvIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descProv is not null
        defaultComuniShouldBeFound("descProv.specified=true");

        // Get all the comuniList where descProv is null
        defaultComuniShouldNotBeFound("descProv.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByCodiSiglProvIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiSiglProv equals to DEFAULT_CODI_SIGL_PROV
        defaultComuniShouldBeFound("codiSiglProv.equals=" + DEFAULT_CODI_SIGL_PROV);

        // Get all the comuniList where codiSiglProv equals to UPDATED_CODI_SIGL_PROV
        defaultComuniShouldNotBeFound("codiSiglProv.equals=" + UPDATED_CODI_SIGL_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiSiglProvIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiSiglProv in DEFAULT_CODI_SIGL_PROV or UPDATED_CODI_SIGL_PROV
        defaultComuniShouldBeFound("codiSiglProv.in=" + DEFAULT_CODI_SIGL_PROV + "," + UPDATED_CODI_SIGL_PROV);

        // Get all the comuniList where codiSiglProv equals to UPDATED_CODI_SIGL_PROV
        defaultComuniShouldNotBeFound("codiSiglProv.in=" + UPDATED_CODI_SIGL_PROV);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiSiglProvIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiSiglProv is not null
        defaultComuniShouldBeFound("codiSiglProv.specified=true");

        // Get all the comuniList where codiSiglProv is null
        defaultComuniShouldNotBeFound("codiSiglProv.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByCodiFiscLunaIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiFiscLuna equals to DEFAULT_CODI_FISC_LUNA
        defaultComuniShouldBeFound("codiFiscLuna.equals=" + DEFAULT_CODI_FISC_LUNA);

        // Get all the comuniList where codiFiscLuna equals to UPDATED_CODI_FISC_LUNA
        defaultComuniShouldNotBeFound("codiFiscLuna.equals=" + UPDATED_CODI_FISC_LUNA);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiFiscLunaIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiFiscLuna in DEFAULT_CODI_FISC_LUNA or UPDATED_CODI_FISC_LUNA
        defaultComuniShouldBeFound("codiFiscLuna.in=" + DEFAULT_CODI_FISC_LUNA + "," + UPDATED_CODI_FISC_LUNA);

        // Get all the comuniList where codiFiscLuna equals to UPDATED_CODI_FISC_LUNA
        defaultComuniShouldNotBeFound("codiFiscLuna.in=" + UPDATED_CODI_FISC_LUNA);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiFiscLunaIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiFiscLuna is not null
        defaultComuniShouldBeFound("codiFiscLuna.specified=true");

        // Get all the comuniList where codiFiscLuna is null
        defaultComuniShouldNotBeFound("codiFiscLuna.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDescRegiIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descRegi equals to DEFAULT_DESC_REGI
        defaultComuniShouldBeFound("descRegi.equals=" + DEFAULT_DESC_REGI);

        // Get all the comuniList where descRegi equals to UPDATED_DESC_REGI
        defaultComuniShouldNotBeFound("descRegi.equals=" + UPDATED_DESC_REGI);
    }

    @Test
    @Transactional
    public void getAllComunisByDescRegiIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descRegi in DEFAULT_DESC_REGI or UPDATED_DESC_REGI
        defaultComuniShouldBeFound("descRegi.in=" + DEFAULT_DESC_REGI + "," + UPDATED_DESC_REGI);

        // Get all the comuniList where descRegi equals to UPDATED_DESC_REGI
        defaultComuniShouldNotBeFound("descRegi.in=" + UPDATED_DESC_REGI);
    }

    @Test
    @Transactional
    public void getAllComunisByDescRegiIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where descRegi is not null
        defaultComuniShouldBeFound("descRegi.specified=true");

        // Get all the comuniList where descRegi is null
        defaultComuniShouldNotBeFound("descRegi.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByCodiceBelfioreIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiceBelfiore equals to DEFAULT_CODICE_BELFIORE
        defaultComuniShouldBeFound("codiceBelfiore.equals=" + DEFAULT_CODICE_BELFIORE);

        // Get all the comuniList where codiceBelfiore equals to UPDATED_CODICE_BELFIORE
        defaultComuniShouldNotBeFound("codiceBelfiore.equals=" + UPDATED_CODICE_BELFIORE);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiceBelfioreIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiceBelfiore in DEFAULT_CODICE_BELFIORE or UPDATED_CODICE_BELFIORE
        defaultComuniShouldBeFound("codiceBelfiore.in=" + DEFAULT_CODICE_BELFIORE + "," + UPDATED_CODICE_BELFIORE);

        // Get all the comuniList where codiceBelfiore equals to UPDATED_CODICE_BELFIORE
        defaultComuniShouldNotBeFound("codiceBelfiore.in=" + UPDATED_CODICE_BELFIORE);
    }

    @Test
    @Transactional
    public void getAllComunisByCodiceBelfioreIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where codiceBelfiore is not null
        defaultComuniShouldBeFound("codiceBelfiore.specified=true");

        // Get all the comuniList where codiceBelfiore is null
        defaultComuniShouldNotBeFound("codiceBelfiore.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio equals to DEFAULT_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.equals=" + DEFAULT_DATA_INIZIO);

        // Get all the comuniList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.equals=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio in DEFAULT_DATA_INIZIO or UPDATED_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.in=" + DEFAULT_DATA_INIZIO + "," + UPDATED_DATA_INIZIO);

        // Get all the comuniList where dataInizio equals to UPDATED_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.in=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio is not null
        defaultComuniShouldBeFound("dataInizio.specified=true");

        // Get all the comuniList where dataInizio is null
        defaultComuniShouldNotBeFound("dataInizio.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio is greater than or equal to DEFAULT_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the comuniList where dataInizio is greater than or equal to UPDATED_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.greaterThanOrEqual=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio is less than or equal to DEFAULT_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.lessThanOrEqual=" + DEFAULT_DATA_INIZIO);

        // Get all the comuniList where dataInizio is less than or equal to SMALLER_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.lessThanOrEqual=" + SMALLER_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio is less than DEFAULT_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.lessThan=" + DEFAULT_DATA_INIZIO);

        // Get all the comuniList where dataInizio is less than UPDATED_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.lessThan=" + UPDATED_DATA_INIZIO);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizio is greater than DEFAULT_DATA_INIZIO
        defaultComuniShouldNotBeFound("dataInizio.greaterThan=" + DEFAULT_DATA_INIZIO);

        // Get all the comuniList where dataInizio is greater than SMALLER_DATA_INIZIO
        defaultComuniShouldBeFound("dataInizio.greaterThan=" + SMALLER_DATA_INIZIO);
    }


    @Test
    @Transactional
    public void getAllComunisByDataFineIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine equals to DEFAULT_DATA_FINE
        defaultComuniShouldBeFound("dataFine.equals=" + DEFAULT_DATA_FINE);

        // Get all the comuniList where dataFine equals to UPDATED_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.equals=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine in DEFAULT_DATA_FINE or UPDATED_DATA_FINE
        defaultComuniShouldBeFound("dataFine.in=" + DEFAULT_DATA_FINE + "," + UPDATED_DATA_FINE);

        // Get all the comuniList where dataFine equals to UPDATED_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.in=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine is not null
        defaultComuniShouldBeFound("dataFine.specified=true");

        // Get all the comuniList where dataFine is null
        defaultComuniShouldNotBeFound("dataFine.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine is greater than or equal to DEFAULT_DATA_FINE
        defaultComuniShouldBeFound("dataFine.greaterThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the comuniList where dataFine is greater than or equal to UPDATED_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.greaterThanOrEqual=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine is less than or equal to DEFAULT_DATA_FINE
        defaultComuniShouldBeFound("dataFine.lessThanOrEqual=" + DEFAULT_DATA_FINE);

        // Get all the comuniList where dataFine is less than or equal to SMALLER_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.lessThanOrEqual=" + SMALLER_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine is less than DEFAULT_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.lessThan=" + DEFAULT_DATA_FINE);

        // Get all the comuniList where dataFine is less than UPDATED_DATA_FINE
        defaultComuniShouldBeFound("dataFine.lessThan=" + UPDATED_DATA_FINE);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFine is greater than DEFAULT_DATA_FINE
        defaultComuniShouldNotBeFound("dataFine.greaterThan=" + DEFAULT_DATA_FINE);

        // Get all the comuniList where dataFine is greater than SMALLER_DATA_FINE
        defaultComuniShouldBeFound("dataFine.greaterThan=" + SMALLER_DATA_FINE);
    }


    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali is not null
        defaultComuniShouldBeFound("dataInizVali.specified=true");

        // Get all the comuniList where dataInizVali is null
        defaultComuniShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultComuniShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the comuniList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultComuniShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali is not null
        defaultComuniShouldBeFound("dataFineVali.specified=true");

        // Get all the comuniList where dataFineVali is null
        defaultComuniShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllComunisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultComuniShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the comuniList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultComuniShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator is not null
        defaultComuniShouldBeFound("userIdCreator.specified=true");

        // Get all the comuniList where userIdCreator is null
        defaultComuniShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultComuniShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the comuniList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultComuniShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod is not null
        defaultComuniShouldBeFound("userIdLastMod.specified=true");

        // Get all the comuniList where userIdLastMod is null
        defaultComuniShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllComunisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        comuniRepository.saveAndFlush(comuni);

        // Get all the comuniList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultComuniShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the comuniList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultComuniShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultComuniShouldBeFound(String filter) throws Exception {
        restComuniMockMvc.perform(get("/api/comunis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comuni.getId().intValue())))
            .andExpect(jsonPath("$.[*].codiProv").value(hasItem(DEFAULT_CODI_PROV)))
            .andExpect(jsonPath("$.[*].codiComu").value(hasItem(DEFAULT_CODI_COMU)))
            .andExpect(jsonPath("$.[*].descComu").value(hasItem(DEFAULT_DESC_COMU)))
            .andExpect(jsonPath("$.[*].descProv").value(hasItem(DEFAULT_DESC_PROV)))
            .andExpect(jsonPath("$.[*].codiSiglProv").value(hasItem(DEFAULT_CODI_SIGL_PROV)))
            .andExpect(jsonPath("$.[*].codiFiscLuna").value(hasItem(DEFAULT_CODI_FISC_LUNA)))
            .andExpect(jsonPath("$.[*].descRegi").value(hasItem(DEFAULT_DESC_REGI)))
            .andExpect(jsonPath("$.[*].codiceBelfiore").value(hasItem(DEFAULT_CODICE_BELFIORE)))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restComuniMockMvc.perform(get("/api/comunis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultComuniShouldNotBeFound(String filter) throws Exception {
        restComuniMockMvc.perform(get("/api/comunis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restComuniMockMvc.perform(get("/api/comunis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingComuni() throws Exception {
        // Get the comuni
        restComuniMockMvc.perform(get("/api/comunis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComuni() throws Exception {
        // Initialize the database
        comuniService.save(comuni);

        int databaseSizeBeforeUpdate = comuniRepository.findAll().size();

        // Update the comuni
        Comuni updatedComuni = comuniRepository.findById(comuni.getId()).get();
        // Disconnect from session so that the updates on updatedComuni are not directly saved in db
        em.detach(updatedComuni);
        updatedComuni
            .codiProv(UPDATED_CODI_PROV)
            .codiComu(UPDATED_CODI_COMU)
            .descComu(UPDATED_DESC_COMU)
            .descProv(UPDATED_DESC_PROV)
            .codiSiglProv(UPDATED_CODI_SIGL_PROV)
            .codiFiscLuna(UPDATED_CODI_FISC_LUNA)
            .descRegi(UPDATED_DESC_REGI)
            .codiceBelfiore(UPDATED_CODICE_BELFIORE)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restComuniMockMvc.perform(put("/api/comunis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedComuni)))
            .andExpect(status().isOk());

        // Validate the Comuni in the database
        List<Comuni> comuniList = comuniRepository.findAll();
        assertThat(comuniList).hasSize(databaseSizeBeforeUpdate);
        Comuni testComuni = comuniList.get(comuniList.size() - 1);
        assertThat(testComuni.getCodiProv()).isEqualTo(UPDATED_CODI_PROV);
        assertThat(testComuni.getCodiComu()).isEqualTo(UPDATED_CODI_COMU);
        assertThat(testComuni.getDescComu()).isEqualTo(UPDATED_DESC_COMU);
        assertThat(testComuni.getDescProv()).isEqualTo(UPDATED_DESC_PROV);
        assertThat(testComuni.getCodiSiglProv()).isEqualTo(UPDATED_CODI_SIGL_PROV);
        assertThat(testComuni.getCodiFiscLuna()).isEqualTo(UPDATED_CODI_FISC_LUNA);
        assertThat(testComuni.getDescRegi()).isEqualTo(UPDATED_DESC_REGI);
        assertThat(testComuni.getCodiceBelfiore()).isEqualTo(UPDATED_CODICE_BELFIORE);
        assertThat(testComuni.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
        assertThat(testComuni.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
        assertThat(testComuni.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testComuni.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testComuni.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testComuni.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testComuni.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingComuni() throws Exception {
        int databaseSizeBeforeUpdate = comuniRepository.findAll().size();

        // Create the Comuni

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restComuniMockMvc.perform(put("/api/comunis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comuni)))
            .andExpect(status().isBadRequest());

        // Validate the Comuni in the database
        List<Comuni> comuniList = comuniRepository.findAll();
        assertThat(comuniList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteComuni() throws Exception {
        // Initialize the database
        comuniService.save(comuni);

        int databaseSizeBeforeDelete = comuniRepository.findAll().size();

        // Delete the comuni
        restComuniMockMvc.perform(delete("/api/comunis/{id}", comuni.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Comuni> comuniList = comuniRepository.findAll();
        assertThat(comuniList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Comuni.class);
        Comuni comuni1 = new Comuni();
        comuni1.setId(1L);
        Comuni comuni2 = new Comuni();
        comuni2.setId(comuni1.getId());
        assertThat(comuni1).isEqualTo(comuni2);
        comuni2.setId(2L);
        assertThat(comuni1).isNotEqualTo(comuni2);
        comuni1.setId(null);
        assertThat(comuni1).isNotEqualTo(comuni2);
    }
}
