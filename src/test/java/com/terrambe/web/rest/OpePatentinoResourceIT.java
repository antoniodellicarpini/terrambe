package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.OpePatentino;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.repository.OpePatentinoRepository;
import com.terrambe.service.OpePatentinoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.OpePatentinoCriteria;
import com.terrambe.service.OpePatentinoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OpePatentinoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class OpePatentinoResourceIT {

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_RILASCIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_RILASCIO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_RILASCIO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_SCADENZA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_SCADENZA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_SCADENZA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_ENTE_RILASCIATO = "AAAAAAAAAA";
    private static final String UPDATED_ENTE_RILASCIATO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private OpePatentinoRepository opePatentinoRepository;

    @Autowired
    private OpePatentinoService opePatentinoService;

    @Autowired
    private OpePatentinoQueryService opePatentinoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOpePatentinoMockMvc;

    private OpePatentino opePatentino;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OpePatentinoResource opePatentinoResource = new OpePatentinoResource(opePatentinoService, opePatentinoQueryService);
        this.restOpePatentinoMockMvc = MockMvcBuilders.standaloneSetup(opePatentinoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpePatentino createEntity(EntityManager em) {
        OpePatentino opePatentino = new OpePatentino()
            .numero(DEFAULT_NUMERO)
            .dataRilascio(DEFAULT_DATA_RILASCIO)
            .dataScadenza(DEFAULT_DATA_SCADENZA)
            .enteRilasciato(DEFAULT_ENTE_RILASCIATO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return opePatentino;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpePatentino createUpdatedEntity(EntityManager em) {
        OpePatentino opePatentino = new OpePatentino()
            .numero(UPDATED_NUMERO)
            .dataRilascio(UPDATED_DATA_RILASCIO)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .enteRilasciato(UPDATED_ENTE_RILASCIATO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return opePatentino;
    }

    @BeforeEach
    public void initTest() {
        opePatentino = createEntity(em);
    }

    @Test
    @Transactional
    public void createOpePatentino() throws Exception {
        int databaseSizeBeforeCreate = opePatentinoRepository.findAll().size();

        // Create the OpePatentino
        restOpePatentinoMockMvc.perform(post("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isCreated());

        // Validate the OpePatentino in the database
        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeCreate + 1);
        OpePatentino testOpePatentino = opePatentinoList.get(opePatentinoList.size() - 1);
        assertThat(testOpePatentino.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testOpePatentino.getDataRilascio()).isEqualTo(DEFAULT_DATA_RILASCIO);
        assertThat(testOpePatentino.getDataScadenza()).isEqualTo(DEFAULT_DATA_SCADENZA);
        assertThat(testOpePatentino.getEnteRilasciato()).isEqualTo(DEFAULT_ENTE_RILASCIATO);
        assertThat(testOpePatentino.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testOpePatentino.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testOpePatentino.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testOpePatentino.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testOpePatentino.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createOpePatentinoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = opePatentinoRepository.findAll().size();

        // Create the OpePatentino with an existing ID
        opePatentino.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOpePatentinoMockMvc.perform(post("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isBadRequest());

        // Validate the OpePatentino in the database
        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumeroIsRequired() throws Exception {
        int databaseSizeBeforeTest = opePatentinoRepository.findAll().size();
        // set the field null
        opePatentino.setNumero(null);

        // Create the OpePatentino, which fails.

        restOpePatentinoMockMvc.perform(post("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isBadRequest());

        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataRilascioIsRequired() throws Exception {
        int databaseSizeBeforeTest = opePatentinoRepository.findAll().size();
        // set the field null
        opePatentino.setDataRilascio(null);

        // Create the OpePatentino, which fails.

        restOpePatentinoMockMvc.perform(post("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isBadRequest());

        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataScadenzaIsRequired() throws Exception {
        int databaseSizeBeforeTest = opePatentinoRepository.findAll().size();
        // set the field null
        opePatentino.setDataScadenza(null);

        // Create the OpePatentino, which fails.

        restOpePatentinoMockMvc.perform(post("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isBadRequest());

        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOpePatentinos() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opePatentino.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO.toString())))
            .andExpect(jsonPath("$.[*].dataRilascio").value(hasItem(DEFAULT_DATA_RILASCIO.toString())))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].enteRilasciato").value(hasItem(DEFAULT_ENTE_RILASCIATO.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getOpePatentino() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get the opePatentino
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos/{id}", opePatentino.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(opePatentino.getId().intValue()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO.toString()))
            .andExpect(jsonPath("$.dataRilascio").value(DEFAULT_DATA_RILASCIO.toString()))
            .andExpect(jsonPath("$.dataScadenza").value(DEFAULT_DATA_SCADENZA.toString()))
            .andExpect(jsonPath("$.enteRilasciato").value(DEFAULT_ENTE_RILASCIATO.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByNumeroIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where numero equals to DEFAULT_NUMERO
        defaultOpePatentinoShouldBeFound("numero.equals=" + DEFAULT_NUMERO);

        // Get all the opePatentinoList where numero equals to UPDATED_NUMERO
        defaultOpePatentinoShouldNotBeFound("numero.equals=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByNumeroIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where numero in DEFAULT_NUMERO or UPDATED_NUMERO
        defaultOpePatentinoShouldBeFound("numero.in=" + DEFAULT_NUMERO + "," + UPDATED_NUMERO);

        // Get all the opePatentinoList where numero equals to UPDATED_NUMERO
        defaultOpePatentinoShouldNotBeFound("numero.in=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByNumeroIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where numero is not null
        defaultOpePatentinoShouldBeFound("numero.specified=true");

        // Get all the opePatentinoList where numero is null
        defaultOpePatentinoShouldNotBeFound("numero.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio equals to DEFAULT_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.equals=" + DEFAULT_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio equals to UPDATED_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.equals=" + UPDATED_DATA_RILASCIO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio in DEFAULT_DATA_RILASCIO or UPDATED_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.in=" + DEFAULT_DATA_RILASCIO + "," + UPDATED_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio equals to UPDATED_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.in=" + UPDATED_DATA_RILASCIO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio is not null
        defaultOpePatentinoShouldBeFound("dataRilascio.specified=true");

        // Get all the opePatentinoList where dataRilascio is null
        defaultOpePatentinoShouldNotBeFound("dataRilascio.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio is greater than or equal to DEFAULT_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.greaterThanOrEqual=" + DEFAULT_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio is greater than or equal to UPDATED_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.greaterThanOrEqual=" + UPDATED_DATA_RILASCIO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio is less than or equal to DEFAULT_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.lessThanOrEqual=" + DEFAULT_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio is less than or equal to SMALLER_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.lessThanOrEqual=" + SMALLER_DATA_RILASCIO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio is less than DEFAULT_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.lessThan=" + DEFAULT_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio is less than UPDATED_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.lessThan=" + UPDATED_DATA_RILASCIO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataRilascioIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataRilascio is greater than DEFAULT_DATA_RILASCIO
        defaultOpePatentinoShouldNotBeFound("dataRilascio.greaterThan=" + DEFAULT_DATA_RILASCIO);

        // Get all the opePatentinoList where dataRilascio is greater than SMALLER_DATA_RILASCIO
        defaultOpePatentinoShouldBeFound("dataRilascio.greaterThan=" + SMALLER_DATA_RILASCIO);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza equals to DEFAULT_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.equals=" + DEFAULT_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.equals=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza in DEFAULT_DATA_SCADENZA or UPDATED_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.in=" + DEFAULT_DATA_SCADENZA + "," + UPDATED_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza equals to UPDATED_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.in=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza is not null
        defaultOpePatentinoShouldBeFound("dataScadenza.specified=true");

        // Get all the opePatentinoList where dataScadenza is null
        defaultOpePatentinoShouldNotBeFound("dataScadenza.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza is greater than or equal to DEFAULT_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.greaterThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza is greater than or equal to UPDATED_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.greaterThanOrEqual=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza is less than or equal to DEFAULT_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.lessThanOrEqual=" + DEFAULT_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza is less than or equal to SMALLER_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.lessThanOrEqual=" + SMALLER_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza is less than DEFAULT_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.lessThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza is less than UPDATED_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.lessThan=" + UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataScadenzaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataScadenza is greater than DEFAULT_DATA_SCADENZA
        defaultOpePatentinoShouldNotBeFound("dataScadenza.greaterThan=" + DEFAULT_DATA_SCADENZA);

        // Get all the opePatentinoList where dataScadenza is greater than SMALLER_DATA_SCADENZA
        defaultOpePatentinoShouldBeFound("dataScadenza.greaterThan=" + SMALLER_DATA_SCADENZA);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByEnteRilasciatoIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where enteRilasciato equals to DEFAULT_ENTE_RILASCIATO
        defaultOpePatentinoShouldBeFound("enteRilasciato.equals=" + DEFAULT_ENTE_RILASCIATO);

        // Get all the opePatentinoList where enteRilasciato equals to UPDATED_ENTE_RILASCIATO
        defaultOpePatentinoShouldNotBeFound("enteRilasciato.equals=" + UPDATED_ENTE_RILASCIATO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByEnteRilasciatoIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where enteRilasciato in DEFAULT_ENTE_RILASCIATO or UPDATED_ENTE_RILASCIATO
        defaultOpePatentinoShouldBeFound("enteRilasciato.in=" + DEFAULT_ENTE_RILASCIATO + "," + UPDATED_ENTE_RILASCIATO);

        // Get all the opePatentinoList where enteRilasciato equals to UPDATED_ENTE_RILASCIATO
        defaultOpePatentinoShouldNotBeFound("enteRilasciato.in=" + UPDATED_ENTE_RILASCIATO);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByEnteRilasciatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where enteRilasciato is not null
        defaultOpePatentinoShouldBeFound("enteRilasciato.specified=true");

        // Get all the opePatentinoList where enteRilasciato is null
        defaultOpePatentinoShouldNotBeFound("enteRilasciato.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali is not null
        defaultOpePatentinoShouldBeFound("dataInizVali.specified=true");

        // Get all the opePatentinoList where dataInizVali is null
        defaultOpePatentinoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultOpePatentinoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opePatentinoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultOpePatentinoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali is not null
        defaultOpePatentinoShouldBeFound("dataFineVali.specified=true");

        // Get all the opePatentinoList where dataFineVali is null
        defaultOpePatentinoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultOpePatentinoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opePatentinoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultOpePatentinoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator is not null
        defaultOpePatentinoShouldBeFound("userIdCreator.specified=true");

        // Get all the opePatentinoList where userIdCreator is null
        defaultOpePatentinoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultOpePatentinoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opePatentinoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultOpePatentinoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod is not null
        defaultOpePatentinoShouldBeFound("userIdLastMod.specified=true");

        // Get all the opePatentinoList where userIdLastMod is null
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpePatentinosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);

        // Get all the opePatentinoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultOpePatentinoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opePatentinoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultOpePatentinoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByPateToOpeAnagIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);
        OpeAnagrafica pateToOpeAnag = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(pateToOpeAnag);
        em.flush();
        opePatentino.setPateToOpeAnag(pateToOpeAnag);
        opePatentinoRepository.saveAndFlush(opePatentino);
        Long pateToOpeAnagId = pateToOpeAnag.getId();

        // Get all the opePatentinoList where pateToOpeAnag equals to pateToOpeAnagId
        defaultOpePatentinoShouldBeFound("pateToOpeAnagId.equals=" + pateToOpeAnagId);

        // Get all the opePatentinoList where pateToOpeAnag equals to pateToOpeAnagId + 1
        defaultOpePatentinoShouldNotBeFound("pateToOpeAnagId.equals=" + (pateToOpeAnagId + 1));
    }


    @Test
    @Transactional
    public void getAllOpePatentinosByOpepatentinotipologiaIsEqualToSomething() throws Exception {
        // Initialize the database
        opePatentinoRepository.saveAndFlush(opePatentino);
        OpePatentinoTipologia opepatentinotipologia = OpePatentinoTipologiaResourceIT.createEntity(em);
        em.persist(opepatentinotipologia);
        em.flush();
        opePatentino.setOpepatentinotipologia(opepatentinotipologia);
        opePatentinoRepository.saveAndFlush(opePatentino);
        Long opepatentinotipologiaId = opepatentinotipologia.getId();

        // Get all the opePatentinoList where opepatentinotipologia equals to opepatentinotipologiaId
        defaultOpePatentinoShouldBeFound("opepatentinotipologiaId.equals=" + opepatentinotipologiaId);

        // Get all the opePatentinoList where opepatentinotipologia equals to opepatentinotipologiaId + 1
        defaultOpePatentinoShouldNotBeFound("opepatentinotipologiaId.equals=" + (opepatentinotipologiaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOpePatentinoShouldBeFound(String filter) throws Exception {
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opePatentino.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].dataRilascio").value(hasItem(DEFAULT_DATA_RILASCIO.toString())))
            .andExpect(jsonPath("$.[*].dataScadenza").value(hasItem(DEFAULT_DATA_SCADENZA.toString())))
            .andExpect(jsonPath("$.[*].enteRilasciato").value(hasItem(DEFAULT_ENTE_RILASCIATO)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOpePatentinoShouldNotBeFound(String filter) throws Exception {
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOpePatentino() throws Exception {
        // Get the opePatentino
        restOpePatentinoMockMvc.perform(get("/api/ope-patentinos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOpePatentino() throws Exception {
        // Initialize the database
        opePatentinoService.save(opePatentino);

        int databaseSizeBeforeUpdate = opePatentinoRepository.findAll().size();

        // Update the opePatentino
        OpePatentino updatedOpePatentino = opePatentinoRepository.findById(opePatentino.getId()).get();
        // Disconnect from session so that the updates on updatedOpePatentino are not directly saved in db
        em.detach(updatedOpePatentino);
        updatedOpePatentino
            .numero(UPDATED_NUMERO)
            .dataRilascio(UPDATED_DATA_RILASCIO)
            .dataScadenza(UPDATED_DATA_SCADENZA)
            .enteRilasciato(UPDATED_ENTE_RILASCIATO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restOpePatentinoMockMvc.perform(put("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOpePatentino)))
            .andExpect(status().isOk());

        // Validate the OpePatentino in the database
        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeUpdate);
        OpePatentino testOpePatentino = opePatentinoList.get(opePatentinoList.size() - 1);
        assertThat(testOpePatentino.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testOpePatentino.getDataRilascio()).isEqualTo(UPDATED_DATA_RILASCIO);
        assertThat(testOpePatentino.getDataScadenza()).isEqualTo(UPDATED_DATA_SCADENZA);
        assertThat(testOpePatentino.getEnteRilasciato()).isEqualTo(UPDATED_ENTE_RILASCIATO);
        assertThat(testOpePatentino.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testOpePatentino.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testOpePatentino.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testOpePatentino.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testOpePatentino.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingOpePatentino() throws Exception {
        int databaseSizeBeforeUpdate = opePatentinoRepository.findAll().size();

        // Create the OpePatentino

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOpePatentinoMockMvc.perform(put("/api/ope-patentinos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opePatentino)))
            .andExpect(status().isBadRequest());

        // Validate the OpePatentino in the database
        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOpePatentino() throws Exception {
        // Initialize the database
        opePatentinoService.save(opePatentino);

        int databaseSizeBeforeDelete = opePatentinoRepository.findAll().size();

        // Delete the opePatentino
        restOpePatentinoMockMvc.perform(delete("/api/ope-patentinos/{id}", opePatentino.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OpePatentino> opePatentinoList = opePatentinoRepository.findAll();
        assertThat(opePatentinoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpePatentino.class);
        OpePatentino opePatentino1 = new OpePatentino();
        opePatentino1.setId(1L);
        OpePatentino opePatentino2 = new OpePatentino();
        opePatentino2.setId(opePatentino1.getId());
        assertThat(opePatentino1).isEqualTo(opePatentino2);
        opePatentino2.setId(2L);
        assertThat(opePatentino1).isNotEqualTo(opePatentino2);
        opePatentino1.setId(null);
        assertThat(opePatentino1).isNotEqualTo(opePatentino2);
    }
}
