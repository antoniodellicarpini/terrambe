package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndNascForn;
import com.terrambe.repository.IndNascFornRepository;
import com.terrambe.service.IndNascFornService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndNascFornCriteria;
import com.terrambe.service.IndNascFornQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndNascFornResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndNascFornResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private IndNascFornRepository indNascFornRepository;

    @Autowired
    private IndNascFornService indNascFornService;

    @Autowired
    private IndNascFornQueryService indNascFornQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndNascFornMockMvc;

    private IndNascForn indNascForn;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndNascFornResource indNascFornResource = new IndNascFornResource(indNascFornService, indNascFornQueryService);
        this.restIndNascFornMockMvc = MockMvcBuilders.standaloneSetup(indNascFornResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascForn createEntity(EntityManager em) {
        IndNascForn indNascForn = new IndNascForn()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return indNascForn;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascForn createUpdatedEntity(EntityManager em) {
        IndNascForn indNascForn = new IndNascForn()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return indNascForn;
    }

    @BeforeEach
    public void initTest() {
        indNascForn = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndNascForn() throws Exception {
        int databaseSizeBeforeCreate = indNascFornRepository.findAll().size();

        // Create the IndNascForn
        restIndNascFornMockMvc.perform(post("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascForn)))
            .andExpect(status().isCreated());

        // Validate the IndNascForn in the database
        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeCreate + 1);
        IndNascForn testIndNascForn = indNascFornList.get(indNascFornList.size() - 1);
        assertThat(testIndNascForn.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndNascForn.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndNascForn.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndNascForn.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndNascForn.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndNascForn.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndNascForn.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndNascForn.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndNascForn.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndNascForn.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createIndNascFornWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indNascFornRepository.findAll().size();

        // Create the IndNascForn with an existing ID
        indNascForn.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndNascFornMockMvc.perform(post("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascForn)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascForn in the database
        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascFornRepository.findAll().size();
        // set the field null
        indNascForn.setIndirizzo(null);

        // Create the IndNascForn, which fails.

        restIndNascFornMockMvc.perform(post("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascForn)))
            .andExpect(status().isBadRequest());

        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascFornRepository.findAll().size();
        // set the field null
        indNascForn.setNazione(null);

        // Create the IndNascForn, which fails.

        restIndNascFornMockMvc.perform(post("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascForn)))
            .andExpect(status().isBadRequest());

        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndNascForns() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getIndNascForn() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get the indNascForn
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns/{id}", indNascForn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indNascForn.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndNascFornShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indNascFornList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascFornShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndNascFornShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indNascFornList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascFornShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where indirizzo is not null
        defaultIndNascFornShouldBeFound("indirizzo.specified=true");

        // Get all the indNascFornList where indirizzo is null
        defaultIndNascFornShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where cap equals to DEFAULT_CAP
        defaultIndNascFornShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indNascFornList where cap equals to UPDATED_CAP
        defaultIndNascFornShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndNascFornShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indNascFornList where cap equals to UPDATED_CAP
        defaultIndNascFornShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where cap is not null
        defaultIndNascFornShouldBeFound("cap.specified=true");

        // Get all the indNascFornList where cap is null
        defaultIndNascFornShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where nazione equals to DEFAULT_NAZIONE
        defaultIndNascFornShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indNascFornList where nazione equals to UPDATED_NAZIONE
        defaultIndNascFornShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndNascFornShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indNascFornList where nazione equals to UPDATED_NAZIONE
        defaultIndNascFornShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where nazione is not null
        defaultIndNascFornShouldBeFound("nazione.specified=true");

        // Get all the indNascFornList where nazione is null
        defaultIndNascFornShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where regione equals to DEFAULT_REGIONE
        defaultIndNascFornShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indNascFornList where regione equals to UPDATED_REGIONE
        defaultIndNascFornShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndNascFornShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indNascFornList where regione equals to UPDATED_REGIONE
        defaultIndNascFornShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where regione is not null
        defaultIndNascFornShouldBeFound("regione.specified=true");

        // Get all the indNascFornList where regione is null
        defaultIndNascFornShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where provincia equals to DEFAULT_PROVINCIA
        defaultIndNascFornShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indNascFornList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascFornShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndNascFornShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indNascFornList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascFornShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where provincia is not null
        defaultIndNascFornShouldBeFound("provincia.specified=true");

        // Get all the indNascFornList where provincia is null
        defaultIndNascFornShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where comune equals to DEFAULT_COMUNE
        defaultIndNascFornShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indNascFornList where comune equals to UPDATED_COMUNE
        defaultIndNascFornShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndNascFornShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indNascFornList where comune equals to UPDATED_COMUNE
        defaultIndNascFornShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where comune is not null
        defaultIndNascFornShouldBeFound("comune.specified=true");

        // Get all the indNascFornList where comune is null
        defaultIndNascFornShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali is not null
        defaultIndNascFornShouldBeFound("dataInizVali.specified=true");

        // Get all the indNascFornList where dataInizVali is null
        defaultIndNascFornShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndNascFornShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascFornList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndNascFornShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali is not null
        defaultIndNascFornShouldBeFound("dataFineVali.specified=true");

        // Get all the indNascFornList where dataFineVali is null
        defaultIndNascFornShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndNascFornShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascFornList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndNascFornShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator is not null
        defaultIndNascFornShouldBeFound("userIdCreator.specified=true");

        // Get all the indNascFornList where userIdCreator is null
        defaultIndNascFornShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndNascFornShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascFornList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndNascFornShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod is not null
        defaultIndNascFornShouldBeFound("userIdLastMod.specified=true");

        // Get all the indNascFornList where userIdLastMod is null
        defaultIndNascFornShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascFornsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascFornRepository.saveAndFlush(indNascForn);

        // Get all the indNascFornList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascFornShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascFornList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndNascFornShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndNascFornShouldBeFound(String filter) throws Exception {
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascForn.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndNascFornShouldNotBeFound(String filter) throws Exception {
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndNascForn() throws Exception {
        // Get the indNascForn
        restIndNascFornMockMvc.perform(get("/api/ind-nasc-forns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndNascForn() throws Exception {
        // Initialize the database
        indNascFornService.save(indNascForn);

        int databaseSizeBeforeUpdate = indNascFornRepository.findAll().size();

        // Update the indNascForn
        IndNascForn updatedIndNascForn = indNascFornRepository.findById(indNascForn.getId()).get();
        // Disconnect from session so that the updates on updatedIndNascForn are not directly saved in db
        em.detach(updatedIndNascForn);
        updatedIndNascForn
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restIndNascFornMockMvc.perform(put("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndNascForn)))
            .andExpect(status().isOk());

        // Validate the IndNascForn in the database
        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeUpdate);
        IndNascForn testIndNascForn = indNascFornList.get(indNascFornList.size() - 1);
        assertThat(testIndNascForn.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndNascForn.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndNascForn.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndNascForn.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndNascForn.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndNascForn.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndNascForn.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndNascForn.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndNascForn.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndNascForn.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingIndNascForn() throws Exception {
        int databaseSizeBeforeUpdate = indNascFornRepository.findAll().size();

        // Create the IndNascForn

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndNascFornMockMvc.perform(put("/api/ind-nasc-forns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascForn)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascForn in the database
        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndNascForn() throws Exception {
        // Initialize the database
        indNascFornService.save(indNascForn);

        int databaseSizeBeforeDelete = indNascFornRepository.findAll().size();

        // Delete the indNascForn
        restIndNascFornMockMvc.perform(delete("/api/ind-nasc-forns/{id}", indNascForn.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndNascForn> indNascFornList = indNascFornRepository.findAll();
        assertThat(indNascFornList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndNascForn.class);
        IndNascForn indNascForn1 = new IndNascForn();
        indNascForn1.setId(1L);
        IndNascForn indNascForn2 = new IndNascForn();
        indNascForn2.setId(indNascForn1.getId());
        assertThat(indNascForn1).isEqualTo(indNascForn2);
        indNascForn2.setId(2L);
        assertThat(indNascForn1).isNotEqualTo(indNascForn2);
        indNascForn1.setId(null);
        assertThat(indNascForn1).isNotEqualTo(indNascForn2);
    }
}
