package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoRegMag;
import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.repository.TipoRegMagRepository;
import com.terrambe.service.TipoRegMagService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoRegMagCriteria;
import com.terrambe.service.TipoRegMagQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoRegMagResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoRegMagResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final Integer DEFAULT_MOLTIPLICATORE = 1;
    private static final Integer UPDATED_MOLTIPLICATORE = 2;
    private static final Integer SMALLER_MOLTIPLICATORE = 1 - 1;

    private static final LocalDate DEFAULT_DATA_INIZIO_MAG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO_MAG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO_MAG = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_MAG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_MAG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_MAG = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TipoRegMagRepository tipoRegMagRepository;

    @Autowired
    private TipoRegMagService tipoRegMagService;

    @Autowired
    private TipoRegMagQueryService tipoRegMagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoRegMagMockMvc;

    private TipoRegMag tipoRegMag;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoRegMagResource tipoRegMagResource = new TipoRegMagResource(tipoRegMagService, tipoRegMagQueryService);
        this.restTipoRegMagMockMvc = MockMvcBuilders.standaloneSetup(tipoRegMagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoRegMag createEntity(EntityManager em) {
        TipoRegMag tipoRegMag = new TipoRegMag()
            .descrizione(DEFAULT_DESCRIZIONE)
            .moltiplicatore(DEFAULT_MOLTIPLICATORE)
            .dataInizioMag(DEFAULT_DATA_INIZIO_MAG)
            .dataFineMag(DEFAULT_DATA_FINE_MAG)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return tipoRegMag;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoRegMag createUpdatedEntity(EntityManager em) {
        TipoRegMag tipoRegMag = new TipoRegMag()
            .descrizione(UPDATED_DESCRIZIONE)
            .moltiplicatore(UPDATED_MOLTIPLICATORE)
            .dataInizioMag(UPDATED_DATA_INIZIO_MAG)
            .dataFineMag(UPDATED_DATA_FINE_MAG)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return tipoRegMag;
    }

    @BeforeEach
    public void initTest() {
        tipoRegMag = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoRegMag() throws Exception {
        int databaseSizeBeforeCreate = tipoRegMagRepository.findAll().size();

        // Create the TipoRegMag
        restTipoRegMagMockMvc.perform(post("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isCreated());

        // Validate the TipoRegMag in the database
        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeCreate + 1);
        TipoRegMag testTipoRegMag = tipoRegMagList.get(tipoRegMagList.size() - 1);
        assertThat(testTipoRegMag.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoRegMag.getMoltiplicatore()).isEqualTo(DEFAULT_MOLTIPLICATORE);
        assertThat(testTipoRegMag.getDataInizioMag()).isEqualTo(DEFAULT_DATA_INIZIO_MAG);
        assertThat(testTipoRegMag.getDataFineMag()).isEqualTo(DEFAULT_DATA_FINE_MAG);
        assertThat(testTipoRegMag.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoRegMag.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTipoRegMag.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTipoRegMagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoRegMagRepository.findAll().size();

        // Create the TipoRegMag with an existing ID
        tipoRegMag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoRegMagMockMvc.perform(post("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isBadRequest());

        // Validate the TipoRegMag in the database
        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoRegMagRepository.findAll().size();
        // set the field null
        tipoRegMag.setDescrizione(null);

        // Create the TipoRegMag, which fails.

        restTipoRegMagMockMvc.perform(post("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isBadRequest());

        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataInizioMagIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoRegMagRepository.findAll().size();
        // set the field null
        tipoRegMag.setDataInizioMag(null);

        // Create the TipoRegMag, which fails.

        restTipoRegMagMockMvc.perform(post("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isBadRequest());

        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataFineMagIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoRegMagRepository.findAll().size();
        // set the field null
        tipoRegMag.setDataFineMag(null);

        // Create the TipoRegMag, which fails.

        restTipoRegMagMockMvc.perform(post("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isBadRequest());

        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoRegMags() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoRegMag.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].moltiplicatore").value(hasItem(DEFAULT_MOLTIPLICATORE)))
            .andExpect(jsonPath("$.[*].dataInizioMag").value(hasItem(DEFAULT_DATA_INIZIO_MAG.toString())))
            .andExpect(jsonPath("$.[*].dataFineMag").value(hasItem(DEFAULT_DATA_FINE_MAG.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTipoRegMag() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get the tipoRegMag
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags/{id}", tipoRegMag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoRegMag.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.moltiplicatore").value(DEFAULT_MOLTIPLICATORE))
            .andExpect(jsonPath("$.dataInizioMag").value(DEFAULT_DATA_INIZIO_MAG.toString()))
            .andExpect(jsonPath("$.dataFineMag").value(DEFAULT_DATA_FINE_MAG.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoRegMagShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoRegMagList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoRegMagShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoRegMagShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoRegMagList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoRegMagShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where descrizione is not null
        defaultTipoRegMagShouldBeFound("descrizione.specified=true");

        // Get all the tipoRegMagList where descrizione is null
        defaultTipoRegMagShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore equals to DEFAULT_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.equals=" + DEFAULT_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore equals to UPDATED_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.equals=" + UPDATED_MOLTIPLICATORE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore in DEFAULT_MOLTIPLICATORE or UPDATED_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.in=" + DEFAULT_MOLTIPLICATORE + "," + UPDATED_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore equals to UPDATED_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.in=" + UPDATED_MOLTIPLICATORE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore is not null
        defaultTipoRegMagShouldBeFound("moltiplicatore.specified=true");

        // Get all the tipoRegMagList where moltiplicatore is null
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore is greater than or equal to DEFAULT_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.greaterThanOrEqual=" + DEFAULT_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore is greater than or equal to UPDATED_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.greaterThanOrEqual=" + UPDATED_MOLTIPLICATORE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore is less than or equal to DEFAULT_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.lessThanOrEqual=" + DEFAULT_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore is less than or equal to SMALLER_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.lessThanOrEqual=" + SMALLER_MOLTIPLICATORE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore is less than DEFAULT_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.lessThan=" + DEFAULT_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore is less than UPDATED_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.lessThan=" + UPDATED_MOLTIPLICATORE);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByMoltiplicatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where moltiplicatore is greater than DEFAULT_MOLTIPLICATORE
        defaultTipoRegMagShouldNotBeFound("moltiplicatore.greaterThan=" + DEFAULT_MOLTIPLICATORE);

        // Get all the tipoRegMagList where moltiplicatore is greater than SMALLER_MOLTIPLICATORE
        defaultTipoRegMagShouldBeFound("moltiplicatore.greaterThan=" + SMALLER_MOLTIPLICATORE);
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag equals to DEFAULT_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.equals=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag equals to UPDATED_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.equals=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag in DEFAULT_DATA_INIZIO_MAG or UPDATED_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.in=" + DEFAULT_DATA_INIZIO_MAG + "," + UPDATED_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag equals to UPDATED_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.in=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag is not null
        defaultTipoRegMagShouldBeFound("dataInizioMag.specified=true");

        // Get all the tipoRegMagList where dataInizioMag is null
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag is greater than or equal to DEFAULT_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag is greater than or equal to UPDATED_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.greaterThanOrEqual=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag is less than or equal to DEFAULT_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.lessThanOrEqual=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag is less than or equal to SMALLER_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.lessThanOrEqual=" + SMALLER_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag is less than DEFAULT_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.lessThan=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag is less than UPDATED_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.lessThan=" + UPDATED_DATA_INIZIO_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataInizioMagIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataInizioMag is greater than DEFAULT_DATA_INIZIO_MAG
        defaultTipoRegMagShouldNotBeFound("dataInizioMag.greaterThan=" + DEFAULT_DATA_INIZIO_MAG);

        // Get all the tipoRegMagList where dataInizioMag is greater than SMALLER_DATA_INIZIO_MAG
        defaultTipoRegMagShouldBeFound("dataInizioMag.greaterThan=" + SMALLER_DATA_INIZIO_MAG);
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag equals to DEFAULT_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.equals=" + DEFAULT_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag equals to UPDATED_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.equals=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag in DEFAULT_DATA_FINE_MAG or UPDATED_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.in=" + DEFAULT_DATA_FINE_MAG + "," + UPDATED_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag equals to UPDATED_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.in=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag is not null
        defaultTipoRegMagShouldBeFound("dataFineMag.specified=true");

        // Get all the tipoRegMagList where dataFineMag is null
        defaultTipoRegMagShouldNotBeFound("dataFineMag.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag is greater than or equal to DEFAULT_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.greaterThanOrEqual=" + DEFAULT_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag is greater than or equal to UPDATED_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.greaterThanOrEqual=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag is less than or equal to DEFAULT_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.lessThanOrEqual=" + DEFAULT_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag is less than or equal to SMALLER_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.lessThanOrEqual=" + SMALLER_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag is less than DEFAULT_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.lessThan=" + DEFAULT_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag is less than UPDATED_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.lessThan=" + UPDATED_DATA_FINE_MAG);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByDataFineMagIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where dataFineMag is greater than DEFAULT_DATA_FINE_MAG
        defaultTipoRegMagShouldNotBeFound("dataFineMag.greaterThan=" + DEFAULT_DATA_FINE_MAG);

        // Get all the tipoRegMagList where dataFineMag is greater than SMALLER_DATA_FINE_MAG
        defaultTipoRegMagShouldBeFound("dataFineMag.greaterThan=" + SMALLER_DATA_FINE_MAG);
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator is not null
        defaultTipoRegMagShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoRegMagList where userIdCreator is null
        defaultTipoRegMagShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoRegMagShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoRegMagList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoRegMagShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod is not null
        defaultTipoRegMagShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoRegMagList where userIdLastMod is null
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoRegMagsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);

        // Get all the tipoRegMagList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoRegMagShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoRegMagList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoRegMagShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByTipoRegToRegFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        RegCaricoFitoFarmaco tipoRegToRegFito = RegCaricoFitoFarmacoResourceIT.createEntity(em);
        em.persist(tipoRegToRegFito);
        em.flush();
        tipoRegMag.addTipoRegToRegFito(tipoRegToRegFito);
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        Long tipoRegToRegFitoId = tipoRegToRegFito.getId();

        // Get all the tipoRegMagList where tipoRegToRegFito equals to tipoRegToRegFitoId
        defaultTipoRegMagShouldBeFound("tipoRegToRegFitoId.equals=" + tipoRegToRegFitoId);

        // Get all the tipoRegMagList where tipoRegToRegFito equals to tipoRegToRegFitoId + 1
        defaultTipoRegMagShouldNotBeFound("tipoRegToRegFitoId.equals=" + (tipoRegToRegFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByTipoRegToRegFertIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        RegCaricoFertilizzante tipoRegToRegFert = RegCaricoFertilizzanteResourceIT.createEntity(em);
        em.persist(tipoRegToRegFert);
        em.flush();
        tipoRegMag.addTipoRegToRegFert(tipoRegToRegFert);
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        Long tipoRegToRegFertId = tipoRegToRegFert.getId();

        // Get all the tipoRegMagList where tipoRegToRegFert equals to tipoRegToRegFertId
        defaultTipoRegMagShouldBeFound("tipoRegToRegFertId.equals=" + tipoRegToRegFertId);

        // Get all the tipoRegMagList where tipoRegToRegFert equals to tipoRegToRegFertId + 1
        defaultTipoRegMagShouldNotBeFound("tipoRegToRegFertId.equals=" + (tipoRegToRegFertId + 1));
    }


    @Test
    @Transactional
    public void getAllTipoRegMagsByTipoRegToRegTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        RegCaricoTrappole tipoRegToRegTrap = RegCaricoTrappoleResourceIT.createEntity(em);
        em.persist(tipoRegToRegTrap);
        em.flush();
        tipoRegMag.addTipoRegToRegTrap(tipoRegToRegTrap);
        tipoRegMagRepository.saveAndFlush(tipoRegMag);
        Long tipoRegToRegTrapId = tipoRegToRegTrap.getId();

        // Get all the tipoRegMagList where tipoRegToRegTrap equals to tipoRegToRegTrapId
        defaultTipoRegMagShouldBeFound("tipoRegToRegTrapId.equals=" + tipoRegToRegTrapId);

        // Get all the tipoRegMagList where tipoRegToRegTrap equals to tipoRegToRegTrapId + 1
        defaultTipoRegMagShouldNotBeFound("tipoRegToRegTrapId.equals=" + (tipoRegToRegTrapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoRegMagShouldBeFound(String filter) throws Exception {
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoRegMag.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].moltiplicatore").value(hasItem(DEFAULT_MOLTIPLICATORE)))
            .andExpect(jsonPath("$.[*].dataInizioMag").value(hasItem(DEFAULT_DATA_INIZIO_MAG.toString())))
            .andExpect(jsonPath("$.[*].dataFineMag").value(hasItem(DEFAULT_DATA_FINE_MAG.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoRegMagShouldNotBeFound(String filter) throws Exception {
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoRegMag() throws Exception {
        // Get the tipoRegMag
        restTipoRegMagMockMvc.perform(get("/api/tipo-reg-mags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoRegMag() throws Exception {
        // Initialize the database
        tipoRegMagService.save(tipoRegMag);

        int databaseSizeBeforeUpdate = tipoRegMagRepository.findAll().size();

        // Update the tipoRegMag
        TipoRegMag updatedTipoRegMag = tipoRegMagRepository.findById(tipoRegMag.getId()).get();
        // Disconnect from session so that the updates on updatedTipoRegMag are not directly saved in db
        em.detach(updatedTipoRegMag);
        updatedTipoRegMag
            .descrizione(UPDATED_DESCRIZIONE)
            .moltiplicatore(UPDATED_MOLTIPLICATORE)
            .dataInizioMag(UPDATED_DATA_INIZIO_MAG)
            .dataFineMag(UPDATED_DATA_FINE_MAG)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTipoRegMagMockMvc.perform(put("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoRegMag)))
            .andExpect(status().isOk());

        // Validate the TipoRegMag in the database
        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeUpdate);
        TipoRegMag testTipoRegMag = tipoRegMagList.get(tipoRegMagList.size() - 1);
        assertThat(testTipoRegMag.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoRegMag.getMoltiplicatore()).isEqualTo(UPDATED_MOLTIPLICATORE);
        assertThat(testTipoRegMag.getDataInizioMag()).isEqualTo(UPDATED_DATA_INIZIO_MAG);
        assertThat(testTipoRegMag.getDataFineMag()).isEqualTo(UPDATED_DATA_FINE_MAG);
        assertThat(testTipoRegMag.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoRegMag.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTipoRegMag.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoRegMag() throws Exception {
        int databaseSizeBeforeUpdate = tipoRegMagRepository.findAll().size();

        // Create the TipoRegMag

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoRegMagMockMvc.perform(put("/api/tipo-reg-mags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoRegMag)))
            .andExpect(status().isBadRequest());

        // Validate the TipoRegMag in the database
        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoRegMag() throws Exception {
        // Initialize the database
        tipoRegMagService.save(tipoRegMag);

        int databaseSizeBeforeDelete = tipoRegMagRepository.findAll().size();

        // Delete the tipoRegMag
        restTipoRegMagMockMvc.perform(delete("/api/tipo-reg-mags/{id}", tipoRegMag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoRegMag> tipoRegMagList = tipoRegMagRepository.findAll();
        assertThat(tipoRegMagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoRegMag.class);
        TipoRegMag tipoRegMag1 = new TipoRegMag();
        tipoRegMag1.setId(1L);
        TipoRegMag tipoRegMag2 = new TipoRegMag();
        tipoRegMag2.setId(tipoRegMag1.getId());
        assertThat(tipoRegMag1).isEqualTo(tipoRegMag2);
        tipoRegMag2.setId(2L);
        assertThat(tipoRegMag1).isNotEqualTo(tipoRegMag2);
        tipoRegMag1.setId(null);
        assertThat(tipoRegMag1).isNotEqualTo(tipoRegMag2);
    }
}
