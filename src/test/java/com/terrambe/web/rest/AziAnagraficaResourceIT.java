package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.Cliente;
import com.terrambe.domain.IndAzienda;
import com.terrambe.domain.RecAzienda;
import com.terrambe.domain.Brogliaccio;
import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.domain.AziRappresentante;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.domain.UserTerram;
import com.terrambe.repository.AziAnagraficaRepository;
import com.terrambe.service.AziAnagraficaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AziAnagraficaCriteria;
import com.terrambe.service.AziAnagraficaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziAnagraficaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AziAnagraficaResourceIT {

    private static final String DEFAULT_CUAA = "AAAAAAAAAAA";
    private static final String UPDATED_CUAA = "BBBBBBBBBBB";

    private static final String DEFAULT_PARTITAIVA = "AAAAAAAAAAA";
    private static final String UPDATED_PARTITAIVA = "BBBBBBBBBBB";

    private static final String DEFAULT_CODICEFISCALE = "AAAAAAAAAAAAAAAA";
    private static final String UPDATED_CODICEFISCALE = "BBBBBBBBBBBBBBBB";

    private static final String DEFAULT_CODICE_ATECO = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_ATECO = "BBBBBBBBBB";

    private static final String DEFAULT_RAGIONE_SOCIALE = "AAAAAAAAAA";
    private static final String UPDATED_RAGIONE_SOCIALE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_ATTIVO = false;
    private static final Boolean UPDATED_ATTIVO = true;

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AziAnagraficaRepository aziAnagraficaRepository;

    @Autowired
    private AziAnagraficaService aziAnagraficaService;

    @Autowired
    private AziAnagraficaQueryService aziAnagraficaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAziAnagraficaMockMvc;

    private AziAnagrafica aziAnagrafica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AziAnagraficaResource aziAnagraficaResource = new AziAnagraficaResource(aziAnagraficaService, aziAnagraficaQueryService);
        this.restAziAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(aziAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziAnagrafica createEntity(EntityManager em) {
        AziAnagrafica aziAnagrafica = new AziAnagrafica()
            .cuaa(DEFAULT_CUAA)
            .partitaiva(DEFAULT_PARTITAIVA)
            .codicefiscale(DEFAULT_CODICEFISCALE)
            .codiceAteco(DEFAULT_CODICE_ATECO)
            .ragioneSociale(DEFAULT_RAGIONE_SOCIALE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .attivo(DEFAULT_ATTIVO)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return aziAnagrafica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AziAnagrafica createUpdatedEntity(EntityManager em) {
        AziAnagrafica aziAnagrafica = new AziAnagrafica()
            .cuaa(UPDATED_CUAA)
            .partitaiva(UPDATED_PARTITAIVA)
            .codicefiscale(UPDATED_CODICEFISCALE)
            .codiceAteco(UPDATED_CODICE_ATECO)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .attivo(UPDATED_ATTIVO)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return aziAnagrafica;
    }

    @BeforeEach
    public void initTest() {
        aziAnagrafica = createEntity(em);
    }

    @Test
    @Transactional
    public void createAziAnagrafica() throws Exception {
        int databaseSizeBeforeCreate = aziAnagraficaRepository.findAll().size();

        // Create the AziAnagrafica
        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isCreated());

        // Validate the AziAnagrafica in the database
        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeCreate + 1);
        AziAnagrafica testAziAnagrafica = aziAnagraficaList.get(aziAnagraficaList.size() - 1);
        assertThat(testAziAnagrafica.getCuaa()).isEqualTo(DEFAULT_CUAA);
        assertThat(testAziAnagrafica.getPartitaiva()).isEqualTo(DEFAULT_PARTITAIVA);
        assertThat(testAziAnagrafica.getCodicefiscale()).isEqualTo(DEFAULT_CODICEFISCALE);
        assertThat(testAziAnagrafica.getCodiceAteco()).isEqualTo(DEFAULT_CODICE_ATECO);
        assertThat(testAziAnagrafica.getRagioneSociale()).isEqualTo(DEFAULT_RAGIONE_SOCIALE);
        assertThat(testAziAnagrafica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAziAnagrafica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAziAnagrafica.isAttivo()).isEqualTo(DEFAULT_ATTIVO);
        assertThat(testAziAnagrafica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAziAnagrafica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAziAnagrafica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAziAnagraficaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziAnagraficaRepository.findAll().size();

        // Create the AziAnagrafica with an existing ID
        aziAnagrafica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the AziAnagrafica in the database
        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCuaaIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziAnagraficaRepository.findAll().size();
        // set the field null
        aziAnagrafica.setCuaa(null);

        // Create the AziAnagrafica, which fails.

        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPartitaivaIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziAnagraficaRepository.findAll().size();
        // set the field null
        aziAnagrafica.setPartitaiva(null);

        // Create the AziAnagrafica, which fails.

        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodicefiscaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziAnagraficaRepository.findAll().size();
        // set the field null
        aziAnagrafica.setCodicefiscale(null);

        // Create the AziAnagrafica, which fails.

        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodiceAtecoIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziAnagraficaRepository.findAll().size();
        // set the field null
        aziAnagrafica.setCodiceAteco(null);

        // Create the AziAnagrafica, which fails.

        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRagioneSocialeIsRequired() throws Exception {
        int databaseSizeBeforeTest = aziAnagraficaRepository.findAll().size();
        // set the field null
        aziAnagrafica.setRagioneSociale(null);

        // Create the AziAnagrafica, which fails.

        restAziAnagraficaMockMvc.perform(post("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficas() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].cuaa").value(hasItem(DEFAULT_CUAA.toString())))
            .andExpect(jsonPath("$.[*].partitaiva").value(hasItem(DEFAULT_PARTITAIVA.toString())))
            .andExpect(jsonPath("$.[*].codicefiscale").value(hasItem(DEFAULT_CODICEFISCALE.toString())))
            .andExpect(jsonPath("$.[*].codiceAteco").value(hasItem(DEFAULT_CODICE_ATECO.toString())))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAziAnagrafica() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get the aziAnagrafica
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas/{id}", aziAnagrafica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aziAnagrafica.getId().intValue()))
            .andExpect(jsonPath("$.cuaa").value(DEFAULT_CUAA.toString()))
            .andExpect(jsonPath("$.partitaiva").value(DEFAULT_PARTITAIVA.toString()))
            .andExpect(jsonPath("$.codicefiscale").value(DEFAULT_CODICEFISCALE.toString()))
            .andExpect(jsonPath("$.codiceAteco").value(DEFAULT_CODICE_ATECO.toString()))
            .andExpect(jsonPath("$.ragioneSociale").value(DEFAULT_RAGIONE_SOCIALE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.attivo").value(DEFAULT_ATTIVO.booleanValue()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCuaaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where cuaa equals to DEFAULT_CUAA
        defaultAziAnagraficaShouldBeFound("cuaa.equals=" + DEFAULT_CUAA);

        // Get all the aziAnagraficaList where cuaa equals to UPDATED_CUAA
        defaultAziAnagraficaShouldNotBeFound("cuaa.equals=" + UPDATED_CUAA);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCuaaIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where cuaa in DEFAULT_CUAA or UPDATED_CUAA
        defaultAziAnagraficaShouldBeFound("cuaa.in=" + DEFAULT_CUAA + "," + UPDATED_CUAA);

        // Get all the aziAnagraficaList where cuaa equals to UPDATED_CUAA
        defaultAziAnagraficaShouldNotBeFound("cuaa.in=" + UPDATED_CUAA);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCuaaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where cuaa is not null
        defaultAziAnagraficaShouldBeFound("cuaa.specified=true");

        // Get all the aziAnagraficaList where cuaa is null
        defaultAziAnagraficaShouldNotBeFound("cuaa.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByPartitaivaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where partitaiva equals to DEFAULT_PARTITAIVA
        defaultAziAnagraficaShouldBeFound("partitaiva.equals=" + DEFAULT_PARTITAIVA);

        // Get all the aziAnagraficaList where partitaiva equals to UPDATED_PARTITAIVA
        defaultAziAnagraficaShouldNotBeFound("partitaiva.equals=" + UPDATED_PARTITAIVA);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByPartitaivaIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where partitaiva in DEFAULT_PARTITAIVA or UPDATED_PARTITAIVA
        defaultAziAnagraficaShouldBeFound("partitaiva.in=" + DEFAULT_PARTITAIVA + "," + UPDATED_PARTITAIVA);

        // Get all the aziAnagraficaList where partitaiva equals to UPDATED_PARTITAIVA
        defaultAziAnagraficaShouldNotBeFound("partitaiva.in=" + UPDATED_PARTITAIVA);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByPartitaivaIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where partitaiva is not null
        defaultAziAnagraficaShouldBeFound("partitaiva.specified=true");

        // Get all the aziAnagraficaList where partitaiva is null
        defaultAziAnagraficaShouldNotBeFound("partitaiva.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodicefiscaleIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codicefiscale equals to DEFAULT_CODICEFISCALE
        defaultAziAnagraficaShouldBeFound("codicefiscale.equals=" + DEFAULT_CODICEFISCALE);

        // Get all the aziAnagraficaList where codicefiscale equals to UPDATED_CODICEFISCALE
        defaultAziAnagraficaShouldNotBeFound("codicefiscale.equals=" + UPDATED_CODICEFISCALE);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodicefiscaleIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codicefiscale in DEFAULT_CODICEFISCALE or UPDATED_CODICEFISCALE
        defaultAziAnagraficaShouldBeFound("codicefiscale.in=" + DEFAULT_CODICEFISCALE + "," + UPDATED_CODICEFISCALE);

        // Get all the aziAnagraficaList where codicefiscale equals to UPDATED_CODICEFISCALE
        defaultAziAnagraficaShouldNotBeFound("codicefiscale.in=" + UPDATED_CODICEFISCALE);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodicefiscaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codicefiscale is not null
        defaultAziAnagraficaShouldBeFound("codicefiscale.specified=true");

        // Get all the aziAnagraficaList where codicefiscale is null
        defaultAziAnagraficaShouldNotBeFound("codicefiscale.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodiceAtecoIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codiceAteco equals to DEFAULT_CODICE_ATECO
        defaultAziAnagraficaShouldBeFound("codiceAteco.equals=" + DEFAULT_CODICE_ATECO);

        // Get all the aziAnagraficaList where codiceAteco equals to UPDATED_CODICE_ATECO
        defaultAziAnagraficaShouldNotBeFound("codiceAteco.equals=" + UPDATED_CODICE_ATECO);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodiceAtecoIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codiceAteco in DEFAULT_CODICE_ATECO or UPDATED_CODICE_ATECO
        defaultAziAnagraficaShouldBeFound("codiceAteco.in=" + DEFAULT_CODICE_ATECO + "," + UPDATED_CODICE_ATECO);

        // Get all the aziAnagraficaList where codiceAteco equals to UPDATED_CODICE_ATECO
        defaultAziAnagraficaShouldNotBeFound("codiceAteco.in=" + UPDATED_CODICE_ATECO);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByCodiceAtecoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where codiceAteco is not null
        defaultAziAnagraficaShouldBeFound("codiceAteco.specified=true");

        // Get all the aziAnagraficaList where codiceAteco is null
        defaultAziAnagraficaShouldNotBeFound("codiceAteco.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByRagioneSocialeIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where ragioneSociale equals to DEFAULT_RAGIONE_SOCIALE
        defaultAziAnagraficaShouldBeFound("ragioneSociale.equals=" + DEFAULT_RAGIONE_SOCIALE);

        // Get all the aziAnagraficaList where ragioneSociale equals to UPDATED_RAGIONE_SOCIALE
        defaultAziAnagraficaShouldNotBeFound("ragioneSociale.equals=" + UPDATED_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByRagioneSocialeIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where ragioneSociale in DEFAULT_RAGIONE_SOCIALE or UPDATED_RAGIONE_SOCIALE
        defaultAziAnagraficaShouldBeFound("ragioneSociale.in=" + DEFAULT_RAGIONE_SOCIALE + "," + UPDATED_RAGIONE_SOCIALE);

        // Get all the aziAnagraficaList where ragioneSociale equals to UPDATED_RAGIONE_SOCIALE
        defaultAziAnagraficaShouldNotBeFound("ragioneSociale.in=" + UPDATED_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByRagioneSocialeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where ragioneSociale is not null
        defaultAziAnagraficaShouldBeFound("ragioneSociale.specified=true");

        // Get all the aziAnagraficaList where ragioneSociale is null
        defaultAziAnagraficaShouldNotBeFound("ragioneSociale.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali is not null
        defaultAziAnagraficaShouldBeFound("dataInizVali.specified=true");

        // Get all the aziAnagraficaList where dataInizVali is null
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAziAnagraficaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the aziAnagraficaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAziAnagraficaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali is not null
        defaultAziAnagraficaShouldBeFound("dataFineVali.specified=true");

        // Get all the aziAnagraficaList where dataFineVali is null
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAziAnagraficaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the aziAnagraficaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAziAnagraficaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAttivoIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where attivo equals to DEFAULT_ATTIVO
        defaultAziAnagraficaShouldBeFound("attivo.equals=" + DEFAULT_ATTIVO);

        // Get all the aziAnagraficaList where attivo equals to UPDATED_ATTIVO
        defaultAziAnagraficaShouldNotBeFound("attivo.equals=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByAttivoIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where attivo in DEFAULT_ATTIVO or UPDATED_ATTIVO
        defaultAziAnagraficaShouldBeFound("attivo.in=" + DEFAULT_ATTIVO + "," + UPDATED_ATTIVO);

        // Get all the aziAnagraficaList where attivo equals to UPDATED_ATTIVO
        defaultAziAnagraficaShouldNotBeFound("attivo.in=" + UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByAttivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where attivo is not null
        defaultAziAnagraficaShouldBeFound("attivo.specified=true");

        // Get all the aziAnagraficaList where attivo is null
        defaultAziAnagraficaShouldNotBeFound("attivo.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator is not null
        defaultAziAnagraficaShouldBeFound("userIdCreator.specified=true");

        // Get all the aziAnagraficaList where userIdCreator is null
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAziAnagraficaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the aziAnagraficaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAziAnagraficaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod is not null
        defaultAziAnagraficaShouldBeFound("userIdLastMod.specified=true");

        // Get all the aziAnagraficaList where userIdLastMod is null
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAziAnagraficasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);

        // Get all the aziAnagraficaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the aziAnagraficaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAziAnagraficaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToCliIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Cliente aziAnaToCli = ClienteResourceIT.createEntity(em);
        em.persist(aziAnaToCli);
        em.flush();
        aziAnagrafica.addAziAnaToCli(aziAnaToCli);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToCliId = aziAnaToCli.getId();

        // Get all the aziAnagraficaList where aziAnaToCli equals to aziAnaToCliId
        defaultAziAnagraficaShouldBeFound("aziAnaToCliId.equals=" + aziAnaToCliId);

        // Get all the aziAnagraficaList where aziAnaToCli equals to aziAnaToCliId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToCliId.equals=" + (aziAnaToCliId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziToIndIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        IndAzienda aziToInd = IndAziendaResourceIT.createEntity(em);
        em.persist(aziToInd);
        em.flush();
        aziAnagrafica.addAziToInd(aziToInd);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziToIndId = aziToInd.getId();

        // Get all the aziAnagraficaList where aziToInd equals to aziToIndId
        defaultAziAnagraficaShouldBeFound("aziToIndId.equals=" + aziToIndId);

        // Get all the aziAnagraficaList where aziToInd equals to aziToIndId + 1
        defaultAziAnagraficaShouldNotBeFound("aziToIndId.equals=" + (aziToIndId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziToRecIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        RecAzienda aziToRec = RecAziendaResourceIT.createEntity(em);
        em.persist(aziToRec);
        em.flush();
        aziAnagrafica.addAziToRec(aziToRec);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziToRecId = aziToRec.getId();

        // Get all the aziAnagraficaList where aziToRec equals to aziToRecId
        defaultAziAnagraficaShouldBeFound("aziToRecId.equals=" + aziToRecId);

        // Get all the aziAnagraficaList where aziToRec equals to aziToRecId + 1
        defaultAziAnagraficaShouldNotBeFound("aziToRecId.equals=" + (aziToRecId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToBroglIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Brogliaccio aziAnaToBrogl = BrogliaccioResourceIT.createEntity(em);
        em.persist(aziAnaToBrogl);
        em.flush();
        aziAnagrafica.addAziAnaToBrogl(aziAnaToBrogl);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToBroglId = aziAnaToBrogl.getId();

        // Get all the aziAnagraficaList where aziAnaToBrogl equals to aziAnaToBroglId
        defaultAziAnagraficaShouldBeFound("aziAnaToBroglId.equals=" + aziAnaToBroglId);

        // Get all the aziAnagraficaList where aziAnaToBrogl equals to aziAnaToBroglId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToBroglId.equals=" + (aziAnaToBroglId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByNaturaGiuridicaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        AziFormaGiuridica naturaGiuridica = AziFormaGiuridicaResourceIT.createEntity(em);
        em.persist(naturaGiuridica);
        em.flush();
        aziAnagrafica.setNaturaGiuridica(naturaGiuridica);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long naturaGiuridicaId = naturaGiuridica.getId();

        // Get all the aziAnagraficaList where naturaGiuridica equals to naturaGiuridicaId
        defaultAziAnagraficaShouldBeFound("naturaGiuridicaId.equals=" + naturaGiuridicaId);

        // Get all the aziAnagraficaList where naturaGiuridica equals to naturaGiuridicaId + 1
        defaultAziAnagraficaShouldNotBeFound("naturaGiuridicaId.equals=" + (naturaGiuridicaId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToAziRapIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        AziRappresentante aziAnaToAziRap = AziRappresentanteResourceIT.createEntity(em);
        em.persist(aziAnaToAziRap);
        em.flush();
        aziAnagrafica.setAziAnaToAziRap(aziAnaToAziRap);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToAziRapId = aziAnaToAziRap.getId();

        // Get all the aziAnagraficaList where aziAnaToAziRap equals to aziAnaToAziRapId
        defaultAziAnagraficaShouldBeFound("aziAnaToAziRapId.equals=" + aziAnaToAziRapId);

        // Get all the aziAnagraficaList where aziAnaToAziRap equals to aziAnaToAziRapId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToAziRapId.equals=" + (aziAnaToAziRapId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToUniAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        UniAnagrafica aziAnaToUniAna = UniAnagraficaResourceIT.createEntity(em);
        em.persist(aziAnaToUniAna);
        em.flush();
        aziAnagrafica.addAziAnaToUniAna(aziAnaToUniAna);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToUniAnaId = aziAnaToUniAna.getId();

        // Get all the aziAnagraficaList where aziAnaToUniAna equals to aziAnaToUniAnaId
        defaultAziAnagraficaShouldBeFound("aziAnaToUniAnaId.equals=" + aziAnaToUniAnaId);

        // Get all the aziAnagraficaList where aziAnaToUniAna equals to aziAnaToUniAnaId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToUniAnaId.equals=" + (aziAnaToUniAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToOpeAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        OpeAnagrafica aziAnaToOpeAna = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(aziAnaToOpeAna);
        em.flush();
        aziAnagrafica.addAziAnaToOpeAna(aziAnaToOpeAna);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToOpeAnaId = aziAnaToOpeAna.getId();

        // Get all the aziAnagraficaList where aziAnaToOpeAna equals to aziAnaToOpeAnaId
        defaultAziAnagraficaShouldBeFound("aziAnaToOpeAnaId.equals=" + aziAnaToOpeAnaId);

        // Get all the aziAnagraficaList where aziAnaToOpeAna equals to aziAnaToOpeAnaId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToOpeAnaId.equals=" + (aziAnaToOpeAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllAziAnagraficasByAziAnaToUsrTerIsEqualToSomething() throws Exception {
        // Initialize the database
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        UserTerram aziAnaToUsrTer = UserTerramResourceIT.createEntity(em);
        em.persist(aziAnaToUsrTer);
        em.flush();
        aziAnagrafica.addAziAnaToUsrTer(aziAnaToUsrTer);
        aziAnagraficaRepository.saveAndFlush(aziAnagrafica);
        Long aziAnaToUsrTerId = aziAnaToUsrTer.getId();

        // Get all the aziAnagraficaList where aziAnaToUsrTer equals to aziAnaToUsrTerId
        defaultAziAnagraficaShouldBeFound("aziAnaToUsrTerId.equals=" + aziAnaToUsrTerId);

        // Get all the aziAnagraficaList where aziAnaToUsrTer equals to aziAnaToUsrTerId + 1
        defaultAziAnagraficaShouldNotBeFound("aziAnaToUsrTerId.equals=" + (aziAnaToUsrTerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAziAnagraficaShouldBeFound(String filter) throws Exception {
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aziAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].cuaa").value(hasItem(DEFAULT_CUAA)))
            .andExpect(jsonPath("$.[*].partitaiva").value(hasItem(DEFAULT_PARTITAIVA)))
            .andExpect(jsonPath("$.[*].codicefiscale").value(hasItem(DEFAULT_CODICEFISCALE)))
            .andExpect(jsonPath("$.[*].codiceAteco").value(hasItem(DEFAULT_CODICE_ATECO)))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAziAnagraficaShouldNotBeFound(String filter) throws Exception {
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAziAnagrafica() throws Exception {
        // Get the aziAnagrafica
        restAziAnagraficaMockMvc.perform(get("/api/azi-anagraficas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAziAnagrafica() throws Exception {
        // Initialize the database
        aziAnagraficaService.save(aziAnagrafica);

        int databaseSizeBeforeUpdate = aziAnagraficaRepository.findAll().size();

        // Update the aziAnagrafica
        AziAnagrafica updatedAziAnagrafica = aziAnagraficaRepository.findById(aziAnagrafica.getId()).get();
        // Disconnect from session so that the updates on updatedAziAnagrafica are not directly saved in db
        em.detach(updatedAziAnagrafica);
        updatedAziAnagrafica
            .cuaa(UPDATED_CUAA)
            .partitaiva(UPDATED_PARTITAIVA)
            .codicefiscale(UPDATED_CODICEFISCALE)
            .codiceAteco(UPDATED_CODICE_ATECO)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .attivo(UPDATED_ATTIVO)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAziAnagraficaMockMvc.perform(put("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAziAnagrafica)))
            .andExpect(status().isOk());

        // Validate the AziAnagrafica in the database
        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeUpdate);
        AziAnagrafica testAziAnagrafica = aziAnagraficaList.get(aziAnagraficaList.size() - 1);
        assertThat(testAziAnagrafica.getCuaa()).isEqualTo(UPDATED_CUAA);
        assertThat(testAziAnagrafica.getPartitaiva()).isEqualTo(UPDATED_PARTITAIVA);
        assertThat(testAziAnagrafica.getCodicefiscale()).isEqualTo(UPDATED_CODICEFISCALE);
        assertThat(testAziAnagrafica.getCodiceAteco()).isEqualTo(UPDATED_CODICE_ATECO);
        assertThat(testAziAnagrafica.getRagioneSociale()).isEqualTo(UPDATED_RAGIONE_SOCIALE);
        assertThat(testAziAnagrafica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAziAnagrafica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAziAnagrafica.isAttivo()).isEqualTo(UPDATED_ATTIVO);
        assertThat(testAziAnagrafica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAziAnagrafica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAziAnagrafica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAziAnagrafica() throws Exception {
        int databaseSizeBeforeUpdate = aziAnagraficaRepository.findAll().size();

        // Create the AziAnagrafica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziAnagraficaMockMvc.perform(put("/api/azi-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aziAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the AziAnagrafica in the database
        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAziAnagrafica() throws Exception {
        // Initialize the database
        aziAnagraficaService.save(aziAnagrafica);

        int databaseSizeBeforeDelete = aziAnagraficaRepository.findAll().size();

        // Delete the aziAnagrafica
        restAziAnagraficaMockMvc.perform(delete("/api/azi-anagraficas/{id}", aziAnagrafica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AziAnagrafica> aziAnagraficaList = aziAnagraficaRepository.findAll();
        assertThat(aziAnagraficaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AziAnagrafica.class);
        AziAnagrafica aziAnagrafica1 = new AziAnagrafica();
        aziAnagrafica1.setId(1L);
        AziAnagrafica aziAnagrafica2 = new AziAnagrafica();
        aziAnagrafica2.setId(aziAnagrafica1.getId());
        assertThat(aziAnagrafica1).isEqualTo(aziAnagrafica2);
        aziAnagrafica2.setId(2L);
        assertThat(aziAnagrafica1).isNotEqualTo(aziAnagrafica2);
        aziAnagrafica1.setId(null);
        assertThat(aziAnagrafica1).isNotEqualTo(aziAnagrafica2);
    }
}
