package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.UnitaMisura;
import com.terrambe.domain.Dose;
import com.terrambe.repository.UnitaMisuraRepository;
import com.terrambe.service.UnitaMisuraService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.UnitaMisuraCriteria;
import com.terrambe.service.UnitaMisuraQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UnitaMisuraResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class UnitaMisuraResourceIT {

    private static final String DEFAULT_COD_UM_DOSE = "AAAAAAAAAA";
    private static final String UPDATED_COD_UM_DOSE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private UnitaMisuraRepository unitaMisuraRepository;

    @Autowired
    private UnitaMisuraService unitaMisuraService;

    @Autowired
    private UnitaMisuraQueryService unitaMisuraQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUnitaMisuraMockMvc;

    private UnitaMisura unitaMisura;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UnitaMisuraResource unitaMisuraResource = new UnitaMisuraResource(unitaMisuraService, unitaMisuraQueryService);
        this.restUnitaMisuraMockMvc = MockMvcBuilders.standaloneSetup(unitaMisuraResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnitaMisura createEntity(EntityManager em) {
        UnitaMisura unitaMisura = new UnitaMisura()
            .codUmDose(DEFAULT_COD_UM_DOSE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return unitaMisura;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnitaMisura createUpdatedEntity(EntityManager em) {
        UnitaMisura unitaMisura = new UnitaMisura()
            .codUmDose(UPDATED_COD_UM_DOSE)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return unitaMisura;
    }

    @BeforeEach
    public void initTest() {
        unitaMisura = createEntity(em);
    }

    @Test
    @Transactional
    public void createUnitaMisura() throws Exception {
        int databaseSizeBeforeCreate = unitaMisuraRepository.findAll().size();

        // Create the UnitaMisura
        restUnitaMisuraMockMvc.perform(post("/api/unita-misuras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitaMisura)))
            .andExpect(status().isCreated());

        // Validate the UnitaMisura in the database
        List<UnitaMisura> unitaMisuraList = unitaMisuraRepository.findAll();
        assertThat(unitaMisuraList).hasSize(databaseSizeBeforeCreate + 1);
        UnitaMisura testUnitaMisura = unitaMisuraList.get(unitaMisuraList.size() - 1);
        assertThat(testUnitaMisura.getCodUmDose()).isEqualTo(DEFAULT_COD_UM_DOSE);
        assertThat(testUnitaMisura.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testUnitaMisura.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testUnitaMisura.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testUnitaMisura.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testUnitaMisura.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testUnitaMisura.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testUnitaMisura.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testUnitaMisura.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testUnitaMisura.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createUnitaMisuraWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = unitaMisuraRepository.findAll().size();

        // Create the UnitaMisura with an existing ID
        unitaMisura.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnitaMisuraMockMvc.perform(post("/api/unita-misuras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitaMisura)))
            .andExpect(status().isBadRequest());

        // Validate the UnitaMisura in the database
        List<UnitaMisura> unitaMisuraList = unitaMisuraRepository.findAll();
        assertThat(unitaMisuraList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUnitaMisuras() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unitaMisura.getId().intValue())))
            .andExpect(jsonPath("$.[*].codUmDose").value(hasItem(DEFAULT_COD_UM_DOSE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getUnitaMisura() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get the unitaMisura
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras/{id}", unitaMisura.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(unitaMisura.getId().intValue()))
            .andExpect(jsonPath("$.codUmDose").value(DEFAULT_COD_UM_DOSE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByCodUmDoseIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where codUmDose equals to DEFAULT_COD_UM_DOSE
        defaultUnitaMisuraShouldBeFound("codUmDose.equals=" + DEFAULT_COD_UM_DOSE);

        // Get all the unitaMisuraList where codUmDose equals to UPDATED_COD_UM_DOSE
        defaultUnitaMisuraShouldNotBeFound("codUmDose.equals=" + UPDATED_COD_UM_DOSE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByCodUmDoseIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where codUmDose in DEFAULT_COD_UM_DOSE or UPDATED_COD_UM_DOSE
        defaultUnitaMisuraShouldBeFound("codUmDose.in=" + DEFAULT_COD_UM_DOSE + "," + UPDATED_COD_UM_DOSE);

        // Get all the unitaMisuraList where codUmDose equals to UPDATED_COD_UM_DOSE
        defaultUnitaMisuraShouldNotBeFound("codUmDose.in=" + UPDATED_COD_UM_DOSE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByCodUmDoseIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where codUmDose is not null
        defaultUnitaMisuraShouldBeFound("codUmDose.specified=true");

        // Get all the unitaMisuraList where codUmDose is null
        defaultUnitaMisuraShouldNotBeFound("codUmDose.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultUnitaMisuraShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the unitaMisuraList where descrizione equals to UPDATED_DESCRIZIONE
        defaultUnitaMisuraShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultUnitaMisuraShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the unitaMisuraList where descrizione equals to UPDATED_DESCRIZIONE
        defaultUnitaMisuraShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where descrizione is not null
        defaultUnitaMisuraShouldBeFound("descrizione.specified=true");

        // Get all the unitaMisuraList where descrizione is null
        defaultUnitaMisuraShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultUnitaMisuraShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the unitaMisuraList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultUnitaMisuraShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultUnitaMisuraShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the unitaMisuraList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultUnitaMisuraShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where tipoImport is not null
        defaultUnitaMisuraShouldBeFound("tipoImport.specified=true");

        // Get all the unitaMisuraList where tipoImport is null
        defaultUnitaMisuraShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where operatore equals to DEFAULT_OPERATORE
        defaultUnitaMisuraShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the unitaMisuraList where operatore equals to UPDATED_OPERATORE
        defaultUnitaMisuraShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultUnitaMisuraShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the unitaMisuraList where operatore equals to UPDATED_OPERATORE
        defaultUnitaMisuraShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where operatore is not null
        defaultUnitaMisuraShouldBeFound("operatore.specified=true");

        // Get all the unitaMisuraList where operatore is null
        defaultUnitaMisuraShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where ts equals to DEFAULT_TS
        defaultUnitaMisuraShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the unitaMisuraList where ts equals to UPDATED_TS
        defaultUnitaMisuraShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTsIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where ts in DEFAULT_TS or UPDATED_TS
        defaultUnitaMisuraShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the unitaMisuraList where ts equals to UPDATED_TS
        defaultUnitaMisuraShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where ts is not null
        defaultUnitaMisuraShouldBeFound("ts.specified=true");

        // Get all the unitaMisuraList where ts is null
        defaultUnitaMisuraShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali is not null
        defaultUnitaMisuraShouldBeFound("dataInizVali.specified=true");

        // Get all the unitaMisuraList where dataInizVali is null
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultUnitaMisuraShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the unitaMisuraList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultUnitaMisuraShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali is not null
        defaultUnitaMisuraShouldBeFound("dataFineVali.specified=true");

        // Get all the unitaMisuraList where dataFineVali is null
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultUnitaMisuraShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the unitaMisuraList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultUnitaMisuraShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator is not null
        defaultUnitaMisuraShouldBeFound("userIdCreator.specified=true");

        // Get all the unitaMisuraList where userIdCreator is null
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultUnitaMisuraShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the unitaMisuraList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultUnitaMisuraShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod is not null
        defaultUnitaMisuraShouldBeFound("userIdLastMod.specified=true");

        // Get all the unitaMisuraList where userIdLastMod is null
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllUnitaMisurasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);

        // Get all the unitaMisuraList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the unitaMisuraList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultUnitaMisuraShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllUnitaMisurasByUniMisuToDoseIsEqualToSomething() throws Exception {
        // Initialize the database
        unitaMisuraRepository.saveAndFlush(unitaMisura);
        Dose uniMisuToDose = DoseResourceIT.createEntity(em);
        em.persist(uniMisuToDose);
        em.flush();
        unitaMisura.addUniMisuToDose(uniMisuToDose);
        unitaMisuraRepository.saveAndFlush(unitaMisura);
        Long uniMisuToDoseId = uniMisuToDose.getId();

        // Get all the unitaMisuraList where uniMisuToDose equals to uniMisuToDoseId
        defaultUnitaMisuraShouldBeFound("uniMisuToDoseId.equals=" + uniMisuToDoseId);

        // Get all the unitaMisuraList where uniMisuToDose equals to uniMisuToDoseId + 1
        defaultUnitaMisuraShouldNotBeFound("uniMisuToDoseId.equals=" + (uniMisuToDoseId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUnitaMisuraShouldBeFound(String filter) throws Exception {
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unitaMisura.getId().intValue())))
            .andExpect(jsonPath("$.[*].codUmDose").value(hasItem(DEFAULT_COD_UM_DOSE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUnitaMisuraShouldNotBeFound(String filter) throws Exception {
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUnitaMisura() throws Exception {
        // Get the unitaMisura
        restUnitaMisuraMockMvc.perform(get("/api/unita-misuras/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnitaMisura() throws Exception {
        // Initialize the database
        unitaMisuraService.save(unitaMisura);

        int databaseSizeBeforeUpdate = unitaMisuraRepository.findAll().size();

        // Update the unitaMisura
        UnitaMisura updatedUnitaMisura = unitaMisuraRepository.findById(unitaMisura.getId()).get();
        // Disconnect from session so that the updates on updatedUnitaMisura are not directly saved in db
        em.detach(updatedUnitaMisura);
        updatedUnitaMisura
            .codUmDose(UPDATED_COD_UM_DOSE)
            .descrizione(UPDATED_DESCRIZIONE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restUnitaMisuraMockMvc.perform(put("/api/unita-misuras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUnitaMisura)))
            .andExpect(status().isOk());

        // Validate the UnitaMisura in the database
        List<UnitaMisura> unitaMisuraList = unitaMisuraRepository.findAll();
        assertThat(unitaMisuraList).hasSize(databaseSizeBeforeUpdate);
        UnitaMisura testUnitaMisura = unitaMisuraList.get(unitaMisuraList.size() - 1);
        assertThat(testUnitaMisura.getCodUmDose()).isEqualTo(UPDATED_COD_UM_DOSE);
        assertThat(testUnitaMisura.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testUnitaMisura.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testUnitaMisura.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testUnitaMisura.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testUnitaMisura.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testUnitaMisura.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testUnitaMisura.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testUnitaMisura.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testUnitaMisura.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingUnitaMisura() throws Exception {
        int databaseSizeBeforeUpdate = unitaMisuraRepository.findAll().size();

        // Create the UnitaMisura

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnitaMisuraMockMvc.perform(put("/api/unita-misuras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitaMisura)))
            .andExpect(status().isBadRequest());

        // Validate the UnitaMisura in the database
        List<UnitaMisura> unitaMisuraList = unitaMisuraRepository.findAll();
        assertThat(unitaMisuraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUnitaMisura() throws Exception {
        // Initialize the database
        unitaMisuraService.save(unitaMisura);

        int databaseSizeBeforeDelete = unitaMisuraRepository.findAll().size();

        // Delete the unitaMisura
        restUnitaMisuraMockMvc.perform(delete("/api/unita-misuras/{id}", unitaMisura.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UnitaMisura> unitaMisuraList = unitaMisuraRepository.findAll();
        assertThat(unitaMisuraList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UnitaMisura.class);
        UnitaMisura unitaMisura1 = new UnitaMisura();
        unitaMisura1.setId(1L);
        UnitaMisura unitaMisura2 = new UnitaMisura();
        unitaMisura2.setId(unitaMisura1.getId());
        assertThat(unitaMisura1).isEqualTo(unitaMisura2);
        unitaMisura2.setId(2L);
        assertThat(unitaMisura1).isNotEqualTo(unitaMisura2);
        unitaMisura1.setId(null);
        assertThat(unitaMisura1).isNotEqualTo(unitaMisura2);
    }
}
