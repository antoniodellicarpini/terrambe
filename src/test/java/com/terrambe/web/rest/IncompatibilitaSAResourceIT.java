package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.SostanzeAttive;
import com.terrambe.repository.IncompatibilitaSARepository;
import com.terrambe.service.IncompatibilitaSAService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IncompatibilitaSACriteria;
import com.terrambe.service.IncompatibilitaSAQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IncompatibilitaSAResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IncompatibilitaSAResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private IncompatibilitaSARepository incompatibilitaSARepository;

    @Autowired
    private IncompatibilitaSAService incompatibilitaSAService;

    @Autowired
    private IncompatibilitaSAQueryService incompatibilitaSAQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIncompatibilitaSAMockMvc;

    private IncompatibilitaSA incompatibilitaSA;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IncompatibilitaSAResource incompatibilitaSAResource = new IncompatibilitaSAResource(incompatibilitaSAService, incompatibilitaSAQueryService);
        this.restIncompatibilitaSAMockMvc = MockMvcBuilders.standaloneSetup(incompatibilitaSAResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IncompatibilitaSA createEntity(EntityManager em) {
        IncompatibilitaSA incompatibilitaSA = new IncompatibilitaSA()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return incompatibilitaSA;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IncompatibilitaSA createUpdatedEntity(EntityManager em) {
        IncompatibilitaSA incompatibilitaSA = new IncompatibilitaSA()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return incompatibilitaSA;
    }

    @BeforeEach
    public void initTest() {
        incompatibilitaSA = createEntity(em);
    }

    @Test
    @Transactional
    public void createIncompatibilitaSA() throws Exception {
        int databaseSizeBeforeCreate = incompatibilitaSARepository.findAll().size();

        // Create the IncompatibilitaSA
        restIncompatibilitaSAMockMvc.perform(post("/api/incompatibilita-sas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(incompatibilitaSA)))
            .andExpect(status().isCreated());

        // Validate the IncompatibilitaSA in the database
        List<IncompatibilitaSA> incompatibilitaSAList = incompatibilitaSARepository.findAll();
        assertThat(incompatibilitaSAList).hasSize(databaseSizeBeforeCreate + 1);
        IncompatibilitaSA testIncompatibilitaSA = incompatibilitaSAList.get(incompatibilitaSAList.size() - 1);
        assertThat(testIncompatibilitaSA.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testIncompatibilitaSA.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testIncompatibilitaSA.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testIncompatibilitaSA.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIncompatibilitaSA.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIncompatibilitaSA.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIncompatibilitaSA.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testIncompatibilitaSA.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createIncompatibilitaSAWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = incompatibilitaSARepository.findAll().size();

        // Create the IncompatibilitaSA with an existing ID
        incompatibilitaSA.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIncompatibilitaSAMockMvc.perform(post("/api/incompatibilita-sas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(incompatibilitaSA)))
            .andExpect(status().isBadRequest());

        // Validate the IncompatibilitaSA in the database
        List<IncompatibilitaSA> incompatibilitaSAList = incompatibilitaSARepository.findAll();
        assertThat(incompatibilitaSAList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSAS() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(incompatibilitaSA.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getIncompatibilitaSA() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get the incompatibilitaSA
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas/{id}", incompatibilitaSA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(incompatibilitaSA.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultIncompatibilitaSAShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the incompatibilitaSAList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultIncompatibilitaSAShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultIncompatibilitaSAShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the incompatibilitaSAList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultIncompatibilitaSAShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where tipoImport is not null
        defaultIncompatibilitaSAShouldBeFound("tipoImport.specified=true");

        // Get all the incompatibilitaSAList where tipoImport is null
        defaultIncompatibilitaSAShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where operatore equals to DEFAULT_OPERATORE
        defaultIncompatibilitaSAShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the incompatibilitaSAList where operatore equals to UPDATED_OPERATORE
        defaultIncompatibilitaSAShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultIncompatibilitaSAShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the incompatibilitaSAList where operatore equals to UPDATED_OPERATORE
        defaultIncompatibilitaSAShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where operatore is not null
        defaultIncompatibilitaSAShouldBeFound("operatore.specified=true");

        // Get all the incompatibilitaSAList where operatore is null
        defaultIncompatibilitaSAShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where ts equals to DEFAULT_TS
        defaultIncompatibilitaSAShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the incompatibilitaSAList where ts equals to UPDATED_TS
        defaultIncompatibilitaSAShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTsIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where ts in DEFAULT_TS or UPDATED_TS
        defaultIncompatibilitaSAShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the incompatibilitaSAList where ts equals to UPDATED_TS
        defaultIncompatibilitaSAShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where ts is not null
        defaultIncompatibilitaSAShouldBeFound("ts.specified=true");

        // Get all the incompatibilitaSAList where ts is null
        defaultIncompatibilitaSAShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali is not null
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.specified=true");

        // Get all the incompatibilitaSAList where dataInizVali is null
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the incompatibilitaSAList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIncompatibilitaSAShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali is not null
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.specified=true");

        // Get all the incompatibilitaSAList where dataFineVali is null
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the incompatibilitaSAList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIncompatibilitaSAShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator is not null
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.specified=true");

        // Get all the incompatibilitaSAList where userIdCreator is null
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the incompatibilitaSAList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIncompatibilitaSAShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod is not null
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.specified=true");

        // Get all the incompatibilitaSAList where userIdLastMod is null
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIncompatibilitaSASByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);

        // Get all the incompatibilitaSAList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the incompatibilitaSAList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIncompatibilitaSAShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSASByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        incompatibilitaSA.setEtichettaFito(etichettaFito);
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the incompatibilitaSAList where etichettaFito equals to etichettaFitoId
        defaultIncompatibilitaSAShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the incompatibilitaSAList where etichettaFito equals to etichettaFitoId + 1
        defaultIncompatibilitaSAShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllIncompatibilitaSASByIncompToSostAttIsEqualToSomething() throws Exception {
        // Initialize the database
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);
        SostanzeAttive incompToSostAtt = SostanzeAttiveResourceIT.createEntity(em);
        em.persist(incompToSostAtt);
        em.flush();
        incompatibilitaSA.setIncompToSostAtt(incompToSostAtt);
        incompatibilitaSARepository.saveAndFlush(incompatibilitaSA);
        Long incompToSostAttId = incompToSostAtt.getId();

        // Get all the incompatibilitaSAList where incompToSostAtt equals to incompToSostAttId
        defaultIncompatibilitaSAShouldBeFound("incompToSostAttId.equals=" + incompToSostAttId);

        // Get all the incompatibilitaSAList where incompToSostAtt equals to incompToSostAttId + 1
        defaultIncompatibilitaSAShouldNotBeFound("incompToSostAttId.equals=" + (incompToSostAttId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIncompatibilitaSAShouldBeFound(String filter) throws Exception {
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(incompatibilitaSA.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIncompatibilitaSAShouldNotBeFound(String filter) throws Exception {
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIncompatibilitaSA() throws Exception {
        // Get the incompatibilitaSA
        restIncompatibilitaSAMockMvc.perform(get("/api/incompatibilita-sas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIncompatibilitaSA() throws Exception {
        // Initialize the database
        incompatibilitaSAService.save(incompatibilitaSA);

        int databaseSizeBeforeUpdate = incompatibilitaSARepository.findAll().size();

        // Update the incompatibilitaSA
        IncompatibilitaSA updatedIncompatibilitaSA = incompatibilitaSARepository.findById(incompatibilitaSA.getId()).get();
        // Disconnect from session so that the updates on updatedIncompatibilitaSA are not directly saved in db
        em.detach(updatedIncompatibilitaSA);
        updatedIncompatibilitaSA
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restIncompatibilitaSAMockMvc.perform(put("/api/incompatibilita-sas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIncompatibilitaSA)))
            .andExpect(status().isOk());

        // Validate the IncompatibilitaSA in the database
        List<IncompatibilitaSA> incompatibilitaSAList = incompatibilitaSARepository.findAll();
        assertThat(incompatibilitaSAList).hasSize(databaseSizeBeforeUpdate);
        IncompatibilitaSA testIncompatibilitaSA = incompatibilitaSAList.get(incompatibilitaSAList.size() - 1);
        assertThat(testIncompatibilitaSA.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testIncompatibilitaSA.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testIncompatibilitaSA.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testIncompatibilitaSA.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIncompatibilitaSA.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIncompatibilitaSA.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIncompatibilitaSA.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testIncompatibilitaSA.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingIncompatibilitaSA() throws Exception {
        int databaseSizeBeforeUpdate = incompatibilitaSARepository.findAll().size();

        // Create the IncompatibilitaSA

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIncompatibilitaSAMockMvc.perform(put("/api/incompatibilita-sas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(incompatibilitaSA)))
            .andExpect(status().isBadRequest());

        // Validate the IncompatibilitaSA in the database
        List<IncompatibilitaSA> incompatibilitaSAList = incompatibilitaSARepository.findAll();
        assertThat(incompatibilitaSAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIncompatibilitaSA() throws Exception {
        // Initialize the database
        incompatibilitaSAService.save(incompatibilitaSA);

        int databaseSizeBeforeDelete = incompatibilitaSARepository.findAll().size();

        // Delete the incompatibilitaSA
        restIncompatibilitaSAMockMvc.perform(delete("/api/incompatibilita-sas/{id}", incompatibilitaSA.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IncompatibilitaSA> incompatibilitaSAList = incompatibilitaSARepository.findAll();
        assertThat(incompatibilitaSAList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IncompatibilitaSA.class);
        IncompatibilitaSA incompatibilitaSA1 = new IncompatibilitaSA();
        incompatibilitaSA1.setId(1L);
        IncompatibilitaSA incompatibilitaSA2 = new IncompatibilitaSA();
        incompatibilitaSA2.setId(incompatibilitaSA1.getId());
        assertThat(incompatibilitaSA1).isEqualTo(incompatibilitaSA2);
        incompatibilitaSA2.setId(2L);
        assertThat(incompatibilitaSA1).isNotEqualTo(incompatibilitaSA2);
        incompatibilitaSA1.setId(null);
        assertThat(incompatibilitaSA1).isNotEqualTo(incompatibilitaSA2);
    }
}
