package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcSemina;
import com.terrambe.domain.RegOpcSeminaAppz;
import com.terrambe.domain.TipoSemina;
import com.terrambe.repository.RegOpcSeminaRepository;
import com.terrambe.service.RegOpcSeminaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcSeminaCriteria;
import com.terrambe.service.RegOpcSeminaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcSeminaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcSeminaResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcSeminaRepository regOpcSeminaRepository;

    @Autowired
    private RegOpcSeminaService regOpcSeminaService;

    @Autowired
    private RegOpcSeminaQueryService regOpcSeminaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcSeminaMockMvc;

    private RegOpcSemina regOpcSemina;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcSeminaResource regOpcSeminaResource = new RegOpcSeminaResource(regOpcSeminaService, regOpcSeminaQueryService);
        this.restRegOpcSeminaMockMvc = MockMvcBuilders.standaloneSetup(regOpcSeminaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcSemina createEntity(EntityManager em) {
        RegOpcSemina regOpcSemina = new RegOpcSemina()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcSemina;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcSemina createUpdatedEntity(EntityManager em) {
        RegOpcSemina regOpcSemina = new RegOpcSemina()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcSemina;
    }

    @BeforeEach
    public void initTest() {
        regOpcSemina = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcSemina() throws Exception {
        int databaseSizeBeforeCreate = regOpcSeminaRepository.findAll().size();

        // Create the RegOpcSemina
        restRegOpcSeminaMockMvc.perform(post("/api/reg-opc-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSemina)))
            .andExpect(status().isCreated());

        // Validate the RegOpcSemina in the database
        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcSemina testRegOpcSemina = regOpcSeminaList.get(regOpcSeminaList.size() - 1);
        assertThat(testRegOpcSemina.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcSemina.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcSemina.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcSemina.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcSemina.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcSemina.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcSemina.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcSemina.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcSemina.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcSemina.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcSemina.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcSemina.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcSemina.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcSeminaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcSeminaRepository.findAll().size();

        // Create the RegOpcSemina with an existing ID
        regOpcSemina.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcSeminaMockMvc.perform(post("/api/reg-opc-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSemina)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcSemina in the database
        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcSeminaRepository.findAll().size();
        // set the field null
        regOpcSemina.setIdAzienda(null);

        // Create the RegOpcSemina, which fails.

        restRegOpcSeminaMockMvc.perform(post("/api/reg-opc-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSemina)))
            .andExpect(status().isBadRequest());

        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminas() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcSemina.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcSemina() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get the regOpcSemina
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas/{id}", regOpcSemina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcSemina.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda is not null
        defaultRegOpcSeminaShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcSeminaList where idAzienda is null
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcSeminaShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcSeminaList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcSeminaShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd is not null
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcSeminaList where idUnitaProd is null
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcSeminaShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcSeminaList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcSeminaShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc is not null
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcSeminaList where dataInizOpc is null
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcSeminaList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcSeminaShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc is not null
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcSeminaList where dataFineOpc is null
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcSeminaShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcSeminaList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcSeminaShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcSeminaShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcSeminaList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcSeminaShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcSeminaShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcSeminaList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcSeminaShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where tempoImpiegato is not null
        defaultRegOpcSeminaShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcSeminaList where tempoImpiegato is null
        defaultRegOpcSeminaShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura is not null
        defaultRegOpcSeminaShouldBeFound("idColtura.specified=true");

        // Get all the regOpcSeminaList where idColtura is null
        defaultRegOpcSeminaShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcSeminaShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcSeminaList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcSeminaShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore is not null
        defaultRegOpcSeminaShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcSeminaList where idOperatore is null
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcSeminaShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcSeminaList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcSeminaShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo is not null
        defaultRegOpcSeminaShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcSeminaList where idMezzo is null
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcSeminaShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcSeminaList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcSeminaShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali is not null
        defaultRegOpcSeminaShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcSeminaList where dataInizVali is null
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcSeminaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcSeminaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali is not null
        defaultRegOpcSeminaShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcSeminaList where dataFineVali is null
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcSeminaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcSeminaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcSeminaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator is not null
        defaultRegOpcSeminaShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcSeminaList where userIdCreator is null
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcSeminaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcSeminaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcSeminaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod is not null
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcSeminaList where userIdLastMod is null
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcSeminasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);

        // Get all the regOpcSeminaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcSeminaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcSeminaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByOpcSeminaToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);
        RegOpcSeminaAppz opcSeminaToAppz = RegOpcSeminaAppzResourceIT.createEntity(em);
        em.persist(opcSeminaToAppz);
        em.flush();
        regOpcSemina.addOpcSeminaToAppz(opcSeminaToAppz);
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);
        Long opcSeminaToAppzId = opcSeminaToAppz.getId();

        // Get all the regOpcSeminaList where opcSeminaToAppz equals to opcSeminaToAppzId
        defaultRegOpcSeminaShouldBeFound("opcSeminaToAppzId.equals=" + opcSeminaToAppzId);

        // Get all the regOpcSeminaList where opcSeminaToAppz equals to opcSeminaToAppzId + 1
        defaultRegOpcSeminaShouldNotBeFound("opcSeminaToAppzId.equals=" + (opcSeminaToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcSeminasByOpcSeminaToTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);
        TipoSemina opcSeminaToTipo = TipoSeminaResourceIT.createEntity(em);
        em.persist(opcSeminaToTipo);
        em.flush();
        regOpcSemina.setOpcSeminaToTipo(opcSeminaToTipo);
        regOpcSeminaRepository.saveAndFlush(regOpcSemina);
        Long opcSeminaToTipoId = opcSeminaToTipo.getId();

        // Get all the regOpcSeminaList where opcSeminaToTipo equals to opcSeminaToTipoId
        defaultRegOpcSeminaShouldBeFound("opcSeminaToTipoId.equals=" + opcSeminaToTipoId);

        // Get all the regOpcSeminaList where opcSeminaToTipo equals to opcSeminaToTipoId + 1
        defaultRegOpcSeminaShouldNotBeFound("opcSeminaToTipoId.equals=" + (opcSeminaToTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcSeminaShouldBeFound(String filter) throws Exception {
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcSemina.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcSeminaShouldNotBeFound(String filter) throws Exception {
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcSemina() throws Exception {
        // Get the regOpcSemina
        restRegOpcSeminaMockMvc.perform(get("/api/reg-opc-seminas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcSemina() throws Exception {
        // Initialize the database
        regOpcSeminaService.save(regOpcSemina);

        int databaseSizeBeforeUpdate = regOpcSeminaRepository.findAll().size();

        // Update the regOpcSemina
        RegOpcSemina updatedRegOpcSemina = regOpcSeminaRepository.findById(regOpcSemina.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcSemina are not directly saved in db
        em.detach(updatedRegOpcSemina);
        updatedRegOpcSemina
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcSeminaMockMvc.perform(put("/api/reg-opc-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcSemina)))
            .andExpect(status().isOk());

        // Validate the RegOpcSemina in the database
        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeUpdate);
        RegOpcSemina testRegOpcSemina = regOpcSeminaList.get(regOpcSeminaList.size() - 1);
        assertThat(testRegOpcSemina.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcSemina.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcSemina.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcSemina.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcSemina.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcSemina.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcSemina.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcSemina.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcSemina.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcSemina.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcSemina.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcSemina.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcSemina.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcSemina() throws Exception {
        int databaseSizeBeforeUpdate = regOpcSeminaRepository.findAll().size();

        // Create the RegOpcSemina

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcSeminaMockMvc.perform(put("/api/reg-opc-seminas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcSemina)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcSemina in the database
        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcSemina() throws Exception {
        // Initialize the database
        regOpcSeminaService.save(regOpcSemina);

        int databaseSizeBeforeDelete = regOpcSeminaRepository.findAll().size();

        // Delete the regOpcSemina
        restRegOpcSeminaMockMvc.perform(delete("/api/reg-opc-seminas/{id}", regOpcSemina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcSemina> regOpcSeminaList = regOpcSeminaRepository.findAll();
        assertThat(regOpcSeminaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcSemina.class);
        RegOpcSemina regOpcSemina1 = new RegOpcSemina();
        regOpcSemina1.setId(1L);
        RegOpcSemina regOpcSemina2 = new RegOpcSemina();
        regOpcSemina2.setId(regOpcSemina1.getId());
        assertThat(regOpcSemina1).isEqualTo(regOpcSemina2);
        regOpcSemina2.setId(2L);
        assertThat(regOpcSemina1).isNotEqualTo(regOpcSemina2);
        regOpcSemina1.setId(null);
        assertThat(regOpcSemina1).isNotEqualTo(regOpcSemina2);
    }
}
