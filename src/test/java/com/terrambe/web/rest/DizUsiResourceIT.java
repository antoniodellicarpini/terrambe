package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizUsi;
import com.terrambe.repository.DizUsiRepository;
import com.terrambe.service.DizUsiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizUsiCriteria;
import com.terrambe.service.DizUsiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizUsiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizUsiResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizUsiRepository dizUsiRepository;

    @Autowired
    private DizUsiService dizUsiService;

    @Autowired
    private DizUsiQueryService dizUsiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizUsiMockMvc;

    private DizUsi dizUsi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizUsiResource dizUsiResource = new DizUsiResource(dizUsiService, dizUsiQueryService);
        this.restDizUsiMockMvc = MockMvcBuilders.standaloneSetup(dizUsiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizUsi createEntity(EntityManager em) {
        DizUsi dizUsi = new DizUsi()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizUsi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizUsi createUpdatedEntity(EntityManager em) {
        DizUsi dizUsi = new DizUsi()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizUsi;
    }

    @BeforeEach
    public void initTest() {
        dizUsi = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizUsi() throws Exception {
        int databaseSizeBeforeCreate = dizUsiRepository.findAll().size();

        // Create the DizUsi
        restDizUsiMockMvc.perform(post("/api/diz-usis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizUsi)))
            .andExpect(status().isCreated());

        // Validate the DizUsi in the database
        List<DizUsi> dizUsiList = dizUsiRepository.findAll();
        assertThat(dizUsiList).hasSize(databaseSizeBeforeCreate + 1);
        DizUsi testDizUsi = dizUsiList.get(dizUsiList.size() - 1);
        assertThat(testDizUsi.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizUsi.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizUsi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizUsi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizUsi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizUsi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizUsi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizUsiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizUsiRepository.findAll().size();

        // Create the DizUsi with an existing ID
        dizUsi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizUsiMockMvc.perform(post("/api/diz-usis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizUsi)))
            .andExpect(status().isBadRequest());

        // Validate the DizUsi in the database
        List<DizUsi> dizUsiList = dizUsiRepository.findAll();
        assertThat(dizUsiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizUsis() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList
        restDizUsiMockMvc.perform(get("/api/diz-usis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizUsi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizUsi() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get the dizUsi
        restDizUsiMockMvc.perform(get("/api/diz-usis/{id}", dizUsi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizUsi.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizUsisByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where codice equals to DEFAULT_CODICE
        defaultDizUsiShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizUsiList where codice equals to UPDATED_CODICE
        defaultDizUsiShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizUsisByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizUsiShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizUsiList where codice equals to UPDATED_CODICE
        defaultDizUsiShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizUsisByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where codice is not null
        defaultDizUsiShouldBeFound("codice.specified=true");

        // Get all the dizUsiList where codice is null
        defaultDizUsiShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizUsiShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizUsiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizUsiShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizUsiShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizUsiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizUsiShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where descrizione is not null
        defaultDizUsiShouldBeFound("descrizione.specified=true");

        // Get all the dizUsiList where descrizione is null
        defaultDizUsiShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali is not null
        defaultDizUsiShouldBeFound("dataInizVali.specified=true");

        // Get all the dizUsiList where dataInizVali is null
        defaultDizUsiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizUsiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizUsiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizUsiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali is not null
        defaultDizUsiShouldBeFound("dataFineVali.specified=true");

        // Get all the dizUsiList where dataFineVali is null
        defaultDizUsiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizUsisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizUsiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizUsiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizUsiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator is not null
        defaultDizUsiShouldBeFound("userIdCreator.specified=true");

        // Get all the dizUsiList where userIdCreator is null
        defaultDizUsiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizUsiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizUsiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizUsiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod is not null
        defaultDizUsiShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizUsiList where userIdLastMod is null
        defaultDizUsiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizUsisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizUsiRepository.saveAndFlush(dizUsi);

        // Get all the dizUsiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizUsiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizUsiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizUsiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizUsiShouldBeFound(String filter) throws Exception {
        restDizUsiMockMvc.perform(get("/api/diz-usis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizUsi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizUsiMockMvc.perform(get("/api/diz-usis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizUsiShouldNotBeFound(String filter) throws Exception {
        restDizUsiMockMvc.perform(get("/api/diz-usis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizUsiMockMvc.perform(get("/api/diz-usis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizUsi() throws Exception {
        // Get the dizUsi
        restDizUsiMockMvc.perform(get("/api/diz-usis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizUsi() throws Exception {
        // Initialize the database
        dizUsiService.save(dizUsi);

        int databaseSizeBeforeUpdate = dizUsiRepository.findAll().size();

        // Update the dizUsi
        DizUsi updatedDizUsi = dizUsiRepository.findById(dizUsi.getId()).get();
        // Disconnect from session so that the updates on updatedDizUsi are not directly saved in db
        em.detach(updatedDizUsi);
        updatedDizUsi
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizUsiMockMvc.perform(put("/api/diz-usis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizUsi)))
            .andExpect(status().isOk());

        // Validate the DizUsi in the database
        List<DizUsi> dizUsiList = dizUsiRepository.findAll();
        assertThat(dizUsiList).hasSize(databaseSizeBeforeUpdate);
        DizUsi testDizUsi = dizUsiList.get(dizUsiList.size() - 1);
        assertThat(testDizUsi.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizUsi.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizUsi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizUsi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizUsi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizUsi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizUsi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizUsi() throws Exception {
        int databaseSizeBeforeUpdate = dizUsiRepository.findAll().size();

        // Create the DizUsi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizUsiMockMvc.perform(put("/api/diz-usis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizUsi)))
            .andExpect(status().isBadRequest());

        // Validate the DizUsi in the database
        List<DizUsi> dizUsiList = dizUsiRepository.findAll();
        assertThat(dizUsiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizUsi() throws Exception {
        // Initialize the database
        dizUsiService.save(dizUsi);

        int databaseSizeBeforeDelete = dizUsiRepository.findAll().size();

        // Delete the dizUsi
        restDizUsiMockMvc.perform(delete("/api/diz-usis/{id}", dizUsi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizUsi> dizUsiList = dizUsiRepository.findAll();
        assertThat(dizUsiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizUsi.class);
        DizUsi dizUsi1 = new DizUsi();
        dizUsi1.setId(1L);
        DizUsi dizUsi2 = new DizUsi();
        dizUsi2.setId(dizUsi1.getId());
        assertThat(dizUsi1).isEqualTo(dizUsi2);
        dizUsi2.setId(2L);
        assertThat(dizUsi1).isNotEqualTo(dizUsi2);
        dizUsi1.setId(null);
        assertThat(dizUsi1).isNotEqualTo(dizUsi2);
    }
}
