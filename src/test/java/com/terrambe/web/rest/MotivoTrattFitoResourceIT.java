package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.MotivoTrattFito;
import com.terrambe.domain.RegOpcFito;
import com.terrambe.repository.MotivoTrattFitoRepository;
import com.terrambe.service.MotivoTrattFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.MotivoTrattFitoCriteria;
import com.terrambe.service.MotivoTrattFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MotivoTrattFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class MotivoTrattFitoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private MotivoTrattFitoRepository motivoTrattFitoRepository;

    @Autowired
    private MotivoTrattFitoService motivoTrattFitoService;

    @Autowired
    private MotivoTrattFitoQueryService motivoTrattFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMotivoTrattFitoMockMvc;

    private MotivoTrattFito motivoTrattFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotivoTrattFitoResource motivoTrattFitoResource = new MotivoTrattFitoResource(motivoTrattFitoService, motivoTrattFitoQueryService);
        this.restMotivoTrattFitoMockMvc = MockMvcBuilders.standaloneSetup(motivoTrattFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoTrattFito createEntity(EntityManager em) {
        MotivoTrattFito motivoTrattFito = new MotivoTrattFito()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return motivoTrattFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivoTrattFito createUpdatedEntity(EntityManager em) {
        MotivoTrattFito motivoTrattFito = new MotivoTrattFito()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return motivoTrattFito;
    }

    @BeforeEach
    public void initTest() {
        motivoTrattFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotivoTrattFito() throws Exception {
        int databaseSizeBeforeCreate = motivoTrattFitoRepository.findAll().size();

        // Create the MotivoTrattFito
        restMotivoTrattFitoMockMvc.perform(post("/api/motivo-tratt-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoTrattFito)))
            .andExpect(status().isCreated());

        // Validate the MotivoTrattFito in the database
        List<MotivoTrattFito> motivoTrattFitoList = motivoTrattFitoRepository.findAll();
        assertThat(motivoTrattFitoList).hasSize(databaseSizeBeforeCreate + 1);
        MotivoTrattFito testMotivoTrattFito = motivoTrattFitoList.get(motivoTrattFitoList.size() - 1);
        assertThat(testMotivoTrattFito.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testMotivoTrattFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testMotivoTrattFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testMotivoTrattFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testMotivoTrattFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createMotivoTrattFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motivoTrattFitoRepository.findAll().size();

        // Create the MotivoTrattFito with an existing ID
        motivoTrattFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotivoTrattFitoMockMvc.perform(post("/api/motivo-tratt-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoTrattFito)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoTrattFito in the database
        List<MotivoTrattFito> motivoTrattFitoList = motivoTrattFitoRepository.findAll();
        assertThat(motivoTrattFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMotivoTrattFitos() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoTrattFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getMotivoTrattFito() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get the motivoTrattFito
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos/{id}", motivoTrattFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motivoTrattFito.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultMotivoTrattFitoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the motivoTrattFitoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoTrattFitoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultMotivoTrattFitoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the motivoTrattFitoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultMotivoTrattFitoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where descrizione is not null
        defaultMotivoTrattFitoShouldBeFound("descrizione.specified=true");

        // Get all the motivoTrattFitoList where descrizione is null
        defaultMotivoTrattFitoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali is not null
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the motivoTrattFitoList where dataInizVali is null
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the motivoTrattFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultMotivoTrattFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali is not null
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the motivoTrattFitoList where dataFineVali is null
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the motivoTrattFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultMotivoTrattFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator is not null
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the motivoTrattFitoList where userIdCreator is null
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the motivoTrattFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultMotivoTrattFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod is not null
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the motivoTrattFitoList where userIdLastMod is null
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllMotivoTrattFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);

        // Get all the motivoTrattFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the motivoTrattFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultMotivoTrattFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllMotivoTrattFitosByMotivoToTrattFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);
        RegOpcFito motivoToTrattFito = RegOpcFitoResourceIT.createEntity(em);
        em.persist(motivoToTrattFito);
        em.flush();
        motivoTrattFito.addMotivoToTrattFito(motivoToTrattFito);
        motivoTrattFitoRepository.saveAndFlush(motivoTrattFito);
        Long motivoToTrattFitoId = motivoToTrattFito.getId();

        // Get all the motivoTrattFitoList where motivoToTrattFito equals to motivoToTrattFitoId
        defaultMotivoTrattFitoShouldBeFound("motivoToTrattFitoId.equals=" + motivoToTrattFitoId);

        // Get all the motivoTrattFitoList where motivoToTrattFito equals to motivoToTrattFitoId + 1
        defaultMotivoTrattFitoShouldNotBeFound("motivoToTrattFitoId.equals=" + (motivoToTrattFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMotivoTrattFitoShouldBeFound(String filter) throws Exception {
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivoTrattFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMotivoTrattFitoShouldNotBeFound(String filter) throws Exception {
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMotivoTrattFito() throws Exception {
        // Get the motivoTrattFito
        restMotivoTrattFitoMockMvc.perform(get("/api/motivo-tratt-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotivoTrattFito() throws Exception {
        // Initialize the database
        motivoTrattFitoService.save(motivoTrattFito);

        int databaseSizeBeforeUpdate = motivoTrattFitoRepository.findAll().size();

        // Update the motivoTrattFito
        MotivoTrattFito updatedMotivoTrattFito = motivoTrattFitoRepository.findById(motivoTrattFito.getId()).get();
        // Disconnect from session so that the updates on updatedMotivoTrattFito are not directly saved in db
        em.detach(updatedMotivoTrattFito);
        updatedMotivoTrattFito
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restMotivoTrattFitoMockMvc.perform(put("/api/motivo-tratt-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotivoTrattFito)))
            .andExpect(status().isOk());

        // Validate the MotivoTrattFito in the database
        List<MotivoTrattFito> motivoTrattFitoList = motivoTrattFitoRepository.findAll();
        assertThat(motivoTrattFitoList).hasSize(databaseSizeBeforeUpdate);
        MotivoTrattFito testMotivoTrattFito = motivoTrattFitoList.get(motivoTrattFitoList.size() - 1);
        assertThat(testMotivoTrattFito.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testMotivoTrattFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testMotivoTrattFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testMotivoTrattFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testMotivoTrattFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingMotivoTrattFito() throws Exception {
        int databaseSizeBeforeUpdate = motivoTrattFitoRepository.findAll().size();

        // Create the MotivoTrattFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMotivoTrattFitoMockMvc.perform(put("/api/motivo-tratt-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivoTrattFito)))
            .andExpect(status().isBadRequest());

        // Validate the MotivoTrattFito in the database
        List<MotivoTrattFito> motivoTrattFitoList = motivoTrattFitoRepository.findAll();
        assertThat(motivoTrattFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMotivoTrattFito() throws Exception {
        // Initialize the database
        motivoTrattFitoService.save(motivoTrattFito);

        int databaseSizeBeforeDelete = motivoTrattFitoRepository.findAll().size();

        // Delete the motivoTrattFito
        restMotivoTrattFitoMockMvc.perform(delete("/api/motivo-tratt-fitos/{id}", motivoTrattFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MotivoTrattFito> motivoTrattFitoList = motivoTrattFitoRepository.findAll();
        assertThat(motivoTrattFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotivoTrattFito.class);
        MotivoTrattFito motivoTrattFito1 = new MotivoTrattFito();
        motivoTrattFito1.setId(1L);
        MotivoTrattFito motivoTrattFito2 = new MotivoTrattFito();
        motivoTrattFito2.setId(motivoTrattFito1.getId());
        assertThat(motivoTrattFito1).isEqualTo(motivoTrattFito2);
        motivoTrattFito2.setId(2L);
        assertThat(motivoTrattFito1).isNotEqualTo(motivoTrattFito2);
        motivoTrattFito1.setId(null);
        assertThat(motivoTrattFito1).isNotEqualTo(motivoTrattFito2);
    }
}
