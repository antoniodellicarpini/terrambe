package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcPrepterr;
import com.terrambe.domain.RegOpcPrepterrAppz;
import com.terrambe.domain.TipoPrepterr;
import com.terrambe.repository.RegOpcPrepterrRepository;
import com.terrambe.service.RegOpcPrepterrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcPrepterrCriteria;
import com.terrambe.service.RegOpcPrepterrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcPrepterrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcPrepterrResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_OPC = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_OPC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_OPC = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_OPC = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcPrepterrRepository regOpcPrepterrRepository;

    @Autowired
    private RegOpcPrepterrService regOpcPrepterrService;

    @Autowired
    private RegOpcPrepterrQueryService regOpcPrepterrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcPrepterrMockMvc;

    private RegOpcPrepterr regOpcPrepterr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcPrepterrResource regOpcPrepterrResource = new RegOpcPrepterrResource(regOpcPrepterrService, regOpcPrepterrQueryService);
        this.restRegOpcPrepterrMockMvc = MockMvcBuilders.standaloneSetup(regOpcPrepterrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPrepterr createEntity(EntityManager em) {
        RegOpcPrepterr regOpcPrepterr = new RegOpcPrepterr()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataInizOpc(DEFAULT_DATA_INIZ_OPC)
            .dataFineOpc(DEFAULT_DATA_FINE_OPC)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcPrepterr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcPrepterr createUpdatedEntity(EntityManager em) {
        RegOpcPrepterr regOpcPrepterr = new RegOpcPrepterr()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcPrepterr;
    }

    @BeforeEach
    public void initTest() {
        regOpcPrepterr = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcPrepterr() throws Exception {
        int databaseSizeBeforeCreate = regOpcPrepterrRepository.findAll().size();

        // Create the RegOpcPrepterr
        restRegOpcPrepterrMockMvc.perform(post("/api/reg-opc-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterr)))
            .andExpect(status().isCreated());

        // Validate the RegOpcPrepterr in the database
        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcPrepterr testRegOpcPrepterr = regOpcPrepterrList.get(regOpcPrepterrList.size() - 1);
        assertThat(testRegOpcPrepterr.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcPrepterr.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcPrepterr.getDataInizOpc()).isEqualTo(DEFAULT_DATA_INIZ_OPC);
        assertThat(testRegOpcPrepterr.getDataFineOpc()).isEqualTo(DEFAULT_DATA_FINE_OPC);
        assertThat(testRegOpcPrepterr.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcPrepterr.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcPrepterr.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcPrepterr.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcPrepterr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcPrepterr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcPrepterr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcPrepterr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcPrepterr.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcPrepterrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcPrepterrRepository.findAll().size();

        // Create the RegOpcPrepterr with an existing ID
        regOpcPrepterr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcPrepterrMockMvc.perform(post("/api/reg-opc-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPrepterr in the database
        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcPrepterrRepository.findAll().size();
        // set the field null
        regOpcPrepterr.setIdAzienda(null);

        // Create the RegOpcPrepterr, which fails.

        restRegOpcPrepterrMockMvc.perform(post("/api/reg-opc-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterr)))
            .andExpect(status().isBadRequest());

        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrs() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPrepterr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcPrepterr() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get the regOpcPrepterr
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs/{id}", regOpcPrepterr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcPrepterr.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataInizOpc").value(DEFAULT_DATA_INIZ_OPC.toString()))
            .andExpect(jsonPath("$.dataFineOpc").value(DEFAULT_DATA_FINE_OPC.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda is not null
        defaultRegOpcPrepterrShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcPrepterrList where idAzienda is null
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcPrepterrShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcPrepterrList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcPrepterrShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd is not null
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcPrepterrList where idUnitaProd is null
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcPrepterrList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcPrepterrShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc equals to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.equals=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.equals=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc in DEFAULT_DATA_INIZ_OPC or UPDATED_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.in=" + DEFAULT_DATA_INIZ_OPC + "," + UPDATED_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc equals to UPDATED_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.in=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc is not null
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.specified=true");

        // Get all the regOpcPrepterrList where dataInizOpc is null
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc is greater than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc is greater than or equal to UPDATED_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.greaterThanOrEqual=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc is less than or equal to DEFAULT_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.lessThanOrEqual=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc is less than or equal to SMALLER_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.lessThanOrEqual=" + SMALLER_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc is less than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.lessThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc is less than UPDATED_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.lessThan=" + UPDATED_DATA_INIZ_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizOpc is greater than DEFAULT_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataInizOpc.greaterThan=" + DEFAULT_DATA_INIZ_OPC);

        // Get all the regOpcPrepterrList where dataInizOpc is greater than SMALLER_DATA_INIZ_OPC
        defaultRegOpcPrepterrShouldBeFound("dataInizOpc.greaterThan=" + SMALLER_DATA_INIZ_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc equals to DEFAULT_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.equals=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.equals=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc in DEFAULT_DATA_FINE_OPC or UPDATED_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.in=" + DEFAULT_DATA_FINE_OPC + "," + UPDATED_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc equals to UPDATED_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.in=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc is not null
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.specified=true");

        // Get all the regOpcPrepterrList where dataFineOpc is null
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc is greater than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.greaterThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc is greater than or equal to UPDATED_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.greaterThanOrEqual=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc is less than or equal to DEFAULT_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.lessThanOrEqual=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc is less than or equal to SMALLER_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.lessThanOrEqual=" + SMALLER_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc is less than DEFAULT_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.lessThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc is less than UPDATED_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.lessThan=" + UPDATED_DATA_FINE_OPC);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineOpcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineOpc is greater than DEFAULT_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldNotBeFound("dataFineOpc.greaterThan=" + DEFAULT_DATA_FINE_OPC);

        // Get all the regOpcPrepterrList where dataFineOpc is greater than SMALLER_DATA_FINE_OPC
        defaultRegOpcPrepterrShouldBeFound("dataFineOpc.greaterThan=" + SMALLER_DATA_FINE_OPC);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcPrepterrShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcPrepterrList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcPrepterrShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcPrepterrShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcPrepterrList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcPrepterrShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where tempoImpiegato is not null
        defaultRegOpcPrepterrShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcPrepterrList where tempoImpiegato is null
        defaultRegOpcPrepterrShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura is not null
        defaultRegOpcPrepterrShouldBeFound("idColtura.specified=true");

        // Get all the regOpcPrepterrList where idColtura is null
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcPrepterrShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcPrepterrList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcPrepterrShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore is not null
        defaultRegOpcPrepterrShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcPrepterrList where idOperatore is null
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcPrepterrShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcPrepterrList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcPrepterrShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo is not null
        defaultRegOpcPrepterrShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcPrepterrList where idMezzo is null
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcPrepterrShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcPrepterrList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcPrepterrShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali is not null
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcPrepterrList where dataInizVali is null
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcPrepterrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcPrepterrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali is not null
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcPrepterrList where dataFineVali is null
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcPrepterrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcPrepterrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator is not null
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcPrepterrList where userIdCreator is null
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcPrepterrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcPrepterrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod is not null
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcPrepterrList where userIdLastMod is null
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);

        // Get all the regOpcPrepterrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcPrepterrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcPrepterrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByOpcPrepterrToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);
        RegOpcPrepterrAppz opcPrepterrToAppz = RegOpcPrepterrAppzResourceIT.createEntity(em);
        em.persist(opcPrepterrToAppz);
        em.flush();
        regOpcPrepterr.addOpcPrepterrToAppz(opcPrepterrToAppz);
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);
        Long opcPrepterrToAppzId = opcPrepterrToAppz.getId();

        // Get all the regOpcPrepterrList where opcPrepterrToAppz equals to opcPrepterrToAppzId
        defaultRegOpcPrepterrShouldBeFound("opcPrepterrToAppzId.equals=" + opcPrepterrToAppzId);

        // Get all the regOpcPrepterrList where opcPrepterrToAppz equals to opcPrepterrToAppzId + 1
        defaultRegOpcPrepterrShouldNotBeFound("opcPrepterrToAppzId.equals=" + (opcPrepterrToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcPrepterrsByOpcPrepterrToTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);
        TipoPrepterr opcPrepterrToTipo = TipoPrepterrResourceIT.createEntity(em);
        em.persist(opcPrepterrToTipo);
        em.flush();
        regOpcPrepterr.setOpcPrepterrToTipo(opcPrepterrToTipo);
        regOpcPrepterrRepository.saveAndFlush(regOpcPrepterr);
        Long opcPrepterrToTipoId = opcPrepterrToTipo.getId();

        // Get all the regOpcPrepterrList where opcPrepterrToTipo equals to opcPrepterrToTipoId
        defaultRegOpcPrepterrShouldBeFound("opcPrepterrToTipoId.equals=" + opcPrepterrToTipoId);

        // Get all the regOpcPrepterrList where opcPrepterrToTipo equals to opcPrepterrToTipoId + 1
        defaultRegOpcPrepterrShouldNotBeFound("opcPrepterrToTipoId.equals=" + (opcPrepterrToTipoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcPrepterrShouldBeFound(String filter) throws Exception {
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcPrepterr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataInizOpc").value(hasItem(DEFAULT_DATA_INIZ_OPC.toString())))
            .andExpect(jsonPath("$.[*].dataFineOpc").value(hasItem(DEFAULT_DATA_FINE_OPC.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcPrepterrShouldNotBeFound(String filter) throws Exception {
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcPrepterr() throws Exception {
        // Get the regOpcPrepterr
        restRegOpcPrepterrMockMvc.perform(get("/api/reg-opc-prepterrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcPrepterr() throws Exception {
        // Initialize the database
        regOpcPrepterrService.save(regOpcPrepterr);

        int databaseSizeBeforeUpdate = regOpcPrepterrRepository.findAll().size();

        // Update the regOpcPrepterr
        RegOpcPrepterr updatedRegOpcPrepterr = regOpcPrepterrRepository.findById(regOpcPrepterr.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcPrepterr are not directly saved in db
        em.detach(updatedRegOpcPrepterr);
        updatedRegOpcPrepterr
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataInizOpc(UPDATED_DATA_INIZ_OPC)
            .dataFineOpc(UPDATED_DATA_FINE_OPC)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcPrepterrMockMvc.perform(put("/api/reg-opc-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcPrepterr)))
            .andExpect(status().isOk());

        // Validate the RegOpcPrepterr in the database
        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeUpdate);
        RegOpcPrepterr testRegOpcPrepterr = regOpcPrepterrList.get(regOpcPrepterrList.size() - 1);
        assertThat(testRegOpcPrepterr.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcPrepterr.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcPrepterr.getDataInizOpc()).isEqualTo(UPDATED_DATA_INIZ_OPC);
        assertThat(testRegOpcPrepterr.getDataFineOpc()).isEqualTo(UPDATED_DATA_FINE_OPC);
        assertThat(testRegOpcPrepterr.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcPrepterr.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcPrepterr.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcPrepterr.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcPrepterr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcPrepterr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcPrepterr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcPrepterr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcPrepterr.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcPrepterr() throws Exception {
        int databaseSizeBeforeUpdate = regOpcPrepterrRepository.findAll().size();

        // Create the RegOpcPrepterr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcPrepterrMockMvc.perform(put("/api/reg-opc-prepterrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcPrepterr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcPrepterr in the database
        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcPrepterr() throws Exception {
        // Initialize the database
        regOpcPrepterrService.save(regOpcPrepterr);

        int databaseSizeBeforeDelete = regOpcPrepterrRepository.findAll().size();

        // Delete the regOpcPrepterr
        restRegOpcPrepterrMockMvc.perform(delete("/api/reg-opc-prepterrs/{id}", regOpcPrepterr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcPrepterr> regOpcPrepterrList = regOpcPrepterrRepository.findAll();
        assertThat(regOpcPrepterrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcPrepterr.class);
        RegOpcPrepterr regOpcPrepterr1 = new RegOpcPrepterr();
        regOpcPrepterr1.setId(1L);
        RegOpcPrepterr regOpcPrepterr2 = new RegOpcPrepterr();
        regOpcPrepterr2.setId(regOpcPrepterr1.getId());
        assertThat(regOpcPrepterr1).isEqualTo(regOpcPrepterr2);
        regOpcPrepterr2.setId(2L);
        assertThat(regOpcPrepterr1).isNotEqualTo(regOpcPrepterr2);
        regOpcPrepterr1.setId(null);
        assertThat(regOpcPrepterr1).isNotEqualTo(regOpcPrepterr2);
    }
}
