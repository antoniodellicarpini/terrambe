package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.Frasi;
import com.terrambe.repository.EtichettaFrasiRepository;
import com.terrambe.service.EtichettaFrasiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.EtichettaFrasiCriteria;
import com.terrambe.service.EtichettaFrasiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EtichettaFrasiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class EtichettaFrasiResourceIT {

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private EtichettaFrasiRepository etichettaFrasiRepository;

    @Autowired
    private EtichettaFrasiService etichettaFrasiService;

    @Autowired
    private EtichettaFrasiQueryService etichettaFrasiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEtichettaFrasiMockMvc;

    private EtichettaFrasi etichettaFrasi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EtichettaFrasiResource etichettaFrasiResource = new EtichettaFrasiResource(etichettaFrasiService, etichettaFrasiQueryService);
        this.restEtichettaFrasiMockMvc = MockMvcBuilders.standaloneSetup(etichettaFrasiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaFrasi createEntity(EntityManager em) {
        EtichettaFrasi etichettaFrasi = new EtichettaFrasi()
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return etichettaFrasi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtichettaFrasi createUpdatedEntity(EntityManager em) {
        EtichettaFrasi etichettaFrasi = new EtichettaFrasi()
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return etichettaFrasi;
    }

    @BeforeEach
    public void initTest() {
        etichettaFrasi = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtichettaFrasi() throws Exception {
        int databaseSizeBeforeCreate = etichettaFrasiRepository.findAll().size();

        // Create the EtichettaFrasi
        restEtichettaFrasiMockMvc.perform(post("/api/etichetta-frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFrasi)))
            .andExpect(status().isCreated());

        // Validate the EtichettaFrasi in the database
        List<EtichettaFrasi> etichettaFrasiList = etichettaFrasiRepository.findAll();
        assertThat(etichettaFrasiList).hasSize(databaseSizeBeforeCreate + 1);
        EtichettaFrasi testEtichettaFrasi = etichettaFrasiList.get(etichettaFrasiList.size() - 1);
        assertThat(testEtichettaFrasi.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testEtichettaFrasi.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testEtichettaFrasi.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testEtichettaFrasi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testEtichettaFrasi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testEtichettaFrasi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testEtichettaFrasi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testEtichettaFrasi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createEtichettaFrasiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etichettaFrasiRepository.findAll().size();

        // Create the EtichettaFrasi with an existing ID
        etichettaFrasi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtichettaFrasiMockMvc.perform(post("/api/etichetta-frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFrasi)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaFrasi in the database
        List<EtichettaFrasi> etichettaFrasiList = etichettaFrasiRepository.findAll();
        assertThat(etichettaFrasiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasis() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaFrasi.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtichettaFrasi() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get the etichettaFrasi
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis/{id}", etichettaFrasi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etichettaFrasi.getId().intValue()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultEtichettaFrasiShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the etichettaFrasiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaFrasiShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultEtichettaFrasiShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the etichettaFrasiList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultEtichettaFrasiShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where tipoImport is not null
        defaultEtichettaFrasiShouldBeFound("tipoImport.specified=true");

        // Get all the etichettaFrasiList where tipoImport is null
        defaultEtichettaFrasiShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where operatore equals to DEFAULT_OPERATORE
        defaultEtichettaFrasiShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the etichettaFrasiList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaFrasiShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultEtichettaFrasiShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the etichettaFrasiList where operatore equals to UPDATED_OPERATORE
        defaultEtichettaFrasiShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where operatore is not null
        defaultEtichettaFrasiShouldBeFound("operatore.specified=true");

        // Get all the etichettaFrasiList where operatore is null
        defaultEtichettaFrasiShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where ts equals to DEFAULT_TS
        defaultEtichettaFrasiShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the etichettaFrasiList where ts equals to UPDATED_TS
        defaultEtichettaFrasiShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTsIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where ts in DEFAULT_TS or UPDATED_TS
        defaultEtichettaFrasiShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the etichettaFrasiList where ts equals to UPDATED_TS
        defaultEtichettaFrasiShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where ts is not null
        defaultEtichettaFrasiShouldBeFound("ts.specified=true");

        // Get all the etichettaFrasiList where ts is null
        defaultEtichettaFrasiShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali is not null
        defaultEtichettaFrasiShouldBeFound("dataInizVali.specified=true");

        // Get all the etichettaFrasiList where dataInizVali is null
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the etichettaFrasiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultEtichettaFrasiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali is not null
        defaultEtichettaFrasiShouldBeFound("dataFineVali.specified=true");

        // Get all the etichettaFrasiList where dataFineVali is null
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultEtichettaFrasiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the etichettaFrasiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultEtichettaFrasiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator is not null
        defaultEtichettaFrasiShouldBeFound("userIdCreator.specified=true");

        // Get all the etichettaFrasiList where userIdCreator is null
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultEtichettaFrasiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the etichettaFrasiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultEtichettaFrasiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod is not null
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.specified=true");

        // Get all the etichettaFrasiList where userIdLastMod is null
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllEtichettaFrasisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);

        // Get all the etichettaFrasiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the etichettaFrasiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultEtichettaFrasiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasisByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        etichettaFrasi.setEtichettaFito(etichettaFito);
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the etichettaFrasiList where etichettaFito equals to etichettaFitoId
        defaultEtichettaFrasiShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the etichettaFrasiList where etichettaFito equals to etichettaFitoId + 1
        defaultEtichettaFrasiShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }


    @Test
    @Transactional
    public void getAllEtichettaFrasisByFrasiIsEqualToSomething() throws Exception {
        // Initialize the database
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);
        Frasi frasi = FrasiResourceIT.createEntity(em);
        em.persist(frasi);
        em.flush();
        etichettaFrasi.setFrasi(frasi);
        etichettaFrasiRepository.saveAndFlush(etichettaFrasi);
        Long frasiId = frasi.getId();

        // Get all the etichettaFrasiList where frasi equals to frasiId
        defaultEtichettaFrasiShouldBeFound("frasiId.equals=" + frasiId);

        // Get all the etichettaFrasiList where frasi equals to frasiId + 1
        defaultEtichettaFrasiShouldNotBeFound("frasiId.equals=" + (frasiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtichettaFrasiShouldBeFound(String filter) throws Exception {
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etichettaFrasi.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtichettaFrasiShouldNotBeFound(String filter) throws Exception {
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtichettaFrasi() throws Exception {
        // Get the etichettaFrasi
        restEtichettaFrasiMockMvc.perform(get("/api/etichetta-frasis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtichettaFrasi() throws Exception {
        // Initialize the database
        etichettaFrasiService.save(etichettaFrasi);

        int databaseSizeBeforeUpdate = etichettaFrasiRepository.findAll().size();

        // Update the etichettaFrasi
        EtichettaFrasi updatedEtichettaFrasi = etichettaFrasiRepository.findById(etichettaFrasi.getId()).get();
        // Disconnect from session so that the updates on updatedEtichettaFrasi are not directly saved in db
        em.detach(updatedEtichettaFrasi);
        updatedEtichettaFrasi
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restEtichettaFrasiMockMvc.perform(put("/api/etichetta-frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtichettaFrasi)))
            .andExpect(status().isOk());

        // Validate the EtichettaFrasi in the database
        List<EtichettaFrasi> etichettaFrasiList = etichettaFrasiRepository.findAll();
        assertThat(etichettaFrasiList).hasSize(databaseSizeBeforeUpdate);
        EtichettaFrasi testEtichettaFrasi = etichettaFrasiList.get(etichettaFrasiList.size() - 1);
        assertThat(testEtichettaFrasi.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testEtichettaFrasi.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testEtichettaFrasi.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testEtichettaFrasi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testEtichettaFrasi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testEtichettaFrasi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testEtichettaFrasi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testEtichettaFrasi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtichettaFrasi() throws Exception {
        int databaseSizeBeforeUpdate = etichettaFrasiRepository.findAll().size();

        // Create the EtichettaFrasi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtichettaFrasiMockMvc.perform(put("/api/etichetta-frasis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etichettaFrasi)))
            .andExpect(status().isBadRequest());

        // Validate the EtichettaFrasi in the database
        List<EtichettaFrasi> etichettaFrasiList = etichettaFrasiRepository.findAll();
        assertThat(etichettaFrasiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtichettaFrasi() throws Exception {
        // Initialize the database
        etichettaFrasiService.save(etichettaFrasi);

        int databaseSizeBeforeDelete = etichettaFrasiRepository.findAll().size();

        // Delete the etichettaFrasi
        restEtichettaFrasiMockMvc.perform(delete("/api/etichetta-frasis/{id}", etichettaFrasi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtichettaFrasi> etichettaFrasiList = etichettaFrasiRepository.findAll();
        assertThat(etichettaFrasiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtichettaFrasi.class);
        EtichettaFrasi etichettaFrasi1 = new EtichettaFrasi();
        etichettaFrasi1.setId(1L);
        EtichettaFrasi etichettaFrasi2 = new EtichettaFrasi();
        etichettaFrasi2.setId(etichettaFrasi1.getId());
        assertThat(etichettaFrasi1).isEqualTo(etichettaFrasi2);
        etichettaFrasi2.setId(2L);
        assertThat(etichettaFrasi1).isNotEqualTo(etichettaFrasi2);
        etichettaFrasi1.setId(null);
        assertThat(etichettaFrasi1).isNotEqualTo(etichettaFrasi2);
    }
}
