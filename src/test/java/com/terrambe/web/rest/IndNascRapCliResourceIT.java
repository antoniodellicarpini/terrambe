package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.IndNascRapCli;
import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.repository.IndNascRapCliRepository;
import com.terrambe.service.IndNascRapCliService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.IndNascRapCliCriteria;
import com.terrambe.service.IndNascRapCliQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndNascRapCliResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class IndNascRapCliResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_NAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGIONE = "AAAAAAAAAA";
    private static final String UPDATED_REGIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_COMUNE = "AAAAAAAAAA";
    private static final String UPDATED_COMUNE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private IndNascRapCliRepository indNascRapCliRepository;

    @Autowired
    private IndNascRapCliService indNascRapCliService;

    @Autowired
    private IndNascRapCliQueryService indNascRapCliQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIndNascRapCliMockMvc;

    private IndNascRapCli indNascRapCli;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IndNascRapCliResource indNascRapCliResource = new IndNascRapCliResource(indNascRapCliService, indNascRapCliQueryService);
        this.restIndNascRapCliMockMvc = MockMvcBuilders.standaloneSetup(indNascRapCliResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascRapCli createEntity(EntityManager em) {
        IndNascRapCli indNascRapCli = new IndNascRapCli()
            .indirizzo(DEFAULT_INDIRIZZO)
            .cap(DEFAULT_CAP)
            .nazione(DEFAULT_NAZIONE)
            .regione(DEFAULT_REGIONE)
            .provincia(DEFAULT_PROVINCIA)
            .comune(DEFAULT_COMUNE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return indNascRapCli;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndNascRapCli createUpdatedEntity(EntityManager em) {
        IndNascRapCli indNascRapCli = new IndNascRapCli()
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return indNascRapCli;
    }

    @BeforeEach
    public void initTest() {
        indNascRapCli = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndNascRapCli() throws Exception {
        int databaseSizeBeforeCreate = indNascRapCliRepository.findAll().size();

        // Create the IndNascRapCli
        restIndNascRapCliMockMvc.perform(post("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRapCli)))
            .andExpect(status().isCreated());

        // Validate the IndNascRapCli in the database
        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeCreate + 1);
        IndNascRapCli testIndNascRapCli = indNascRapCliList.get(indNascRapCliList.size() - 1);
        assertThat(testIndNascRapCli.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
        assertThat(testIndNascRapCli.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testIndNascRapCli.getNazione()).isEqualTo(DEFAULT_NAZIONE);
        assertThat(testIndNascRapCli.getRegione()).isEqualTo(DEFAULT_REGIONE);
        assertThat(testIndNascRapCli.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testIndNascRapCli.getComune()).isEqualTo(DEFAULT_COMUNE);
        assertThat(testIndNascRapCli.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testIndNascRapCli.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testIndNascRapCli.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testIndNascRapCli.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createIndNascRapCliWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = indNascRapCliRepository.findAll().size();

        // Create the IndNascRapCli with an existing ID
        indNascRapCli.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndNascRapCliMockMvc.perform(post("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascRapCli in the database
        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndirizzoIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascRapCliRepository.findAll().size();
        // set the field null
        indNascRapCli.setIndirizzo(null);

        // Create the IndNascRapCli, which fails.

        restIndNascRapCliMockMvc.perform(post("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRapCli)))
            .andExpect(status().isBadRequest());

        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = indNascRapCliRepository.findAll().size();
        // set the field null
        indNascRapCli.setNazione(null);

        // Create the IndNascRapCli, which fails.

        restIndNascRapCliMockMvc.perform(post("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRapCli)))
            .andExpect(status().isBadRequest());

        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClis() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE.toString())))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getIndNascRapCli() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get the indNascRapCli
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis/{id}", indNascRapCli.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(indNascRapCli.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.nazione").value(DEFAULT_NAZIONE.toString()))
            .andExpect(jsonPath("$.regione").value(DEFAULT_REGIONE.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.comune").value(DEFAULT_COMUNE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByIndirizzoIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where indirizzo equals to DEFAULT_INDIRIZZO
        defaultIndNascRapCliShouldBeFound("indirizzo.equals=" + DEFAULT_INDIRIZZO);

        // Get all the indNascRapCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascRapCliShouldNotBeFound("indirizzo.equals=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByIndirizzoIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where indirizzo in DEFAULT_INDIRIZZO or UPDATED_INDIRIZZO
        defaultIndNascRapCliShouldBeFound("indirizzo.in=" + DEFAULT_INDIRIZZO + "," + UPDATED_INDIRIZZO);

        // Get all the indNascRapCliList where indirizzo equals to UPDATED_INDIRIZZO
        defaultIndNascRapCliShouldNotBeFound("indirizzo.in=" + UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByIndirizzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where indirizzo is not null
        defaultIndNascRapCliShouldBeFound("indirizzo.specified=true");

        // Get all the indNascRapCliList where indirizzo is null
        defaultIndNascRapCliShouldNotBeFound("indirizzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where cap equals to DEFAULT_CAP
        defaultIndNascRapCliShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the indNascRapCliList where cap equals to UPDATED_CAP
        defaultIndNascRapCliShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByCapIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultIndNascRapCliShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the indNascRapCliList where cap equals to UPDATED_CAP
        defaultIndNascRapCliShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where cap is not null
        defaultIndNascRapCliShouldBeFound("cap.specified=true");

        // Get all the indNascRapCliList where cap is null
        defaultIndNascRapCliShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByNazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where nazione equals to DEFAULT_NAZIONE
        defaultIndNascRapCliShouldBeFound("nazione.equals=" + DEFAULT_NAZIONE);

        // Get all the indNascRapCliList where nazione equals to UPDATED_NAZIONE
        defaultIndNascRapCliShouldNotBeFound("nazione.equals=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByNazioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where nazione in DEFAULT_NAZIONE or UPDATED_NAZIONE
        defaultIndNascRapCliShouldBeFound("nazione.in=" + DEFAULT_NAZIONE + "," + UPDATED_NAZIONE);

        // Get all the indNascRapCliList where nazione equals to UPDATED_NAZIONE
        defaultIndNascRapCliShouldNotBeFound("nazione.in=" + UPDATED_NAZIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByNazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where nazione is not null
        defaultIndNascRapCliShouldBeFound("nazione.specified=true");

        // Get all the indNascRapCliList where nazione is null
        defaultIndNascRapCliShouldNotBeFound("nazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByRegioneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where regione equals to DEFAULT_REGIONE
        defaultIndNascRapCliShouldBeFound("regione.equals=" + DEFAULT_REGIONE);

        // Get all the indNascRapCliList where regione equals to UPDATED_REGIONE
        defaultIndNascRapCliShouldNotBeFound("regione.equals=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByRegioneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where regione in DEFAULT_REGIONE or UPDATED_REGIONE
        defaultIndNascRapCliShouldBeFound("regione.in=" + DEFAULT_REGIONE + "," + UPDATED_REGIONE);

        // Get all the indNascRapCliList where regione equals to UPDATED_REGIONE
        defaultIndNascRapCliShouldNotBeFound("regione.in=" + UPDATED_REGIONE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByRegioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where regione is not null
        defaultIndNascRapCliShouldBeFound("regione.specified=true");

        // Get all the indNascRapCliList where regione is null
        defaultIndNascRapCliShouldNotBeFound("regione.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where provincia equals to DEFAULT_PROVINCIA
        defaultIndNascRapCliShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

        // Get all the indNascRapCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascRapCliShouldNotBeFound("provincia.equals=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByProvinciaIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
        defaultIndNascRapCliShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

        // Get all the indNascRapCliList where provincia equals to UPDATED_PROVINCIA
        defaultIndNascRapCliShouldNotBeFound("provincia.in=" + UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByProvinciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where provincia is not null
        defaultIndNascRapCliShouldBeFound("provincia.specified=true");

        // Get all the indNascRapCliList where provincia is null
        defaultIndNascRapCliShouldNotBeFound("provincia.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByComuneIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where comune equals to DEFAULT_COMUNE
        defaultIndNascRapCliShouldBeFound("comune.equals=" + DEFAULT_COMUNE);

        // Get all the indNascRapCliList where comune equals to UPDATED_COMUNE
        defaultIndNascRapCliShouldNotBeFound("comune.equals=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByComuneIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where comune in DEFAULT_COMUNE or UPDATED_COMUNE
        defaultIndNascRapCliShouldBeFound("comune.in=" + DEFAULT_COMUNE + "," + UPDATED_COMUNE);

        // Get all the indNascRapCliList where comune equals to UPDATED_COMUNE
        defaultIndNascRapCliShouldNotBeFound("comune.in=" + UPDATED_COMUNE);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByComuneIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where comune is not null
        defaultIndNascRapCliShouldBeFound("comune.specified=true");

        // Get all the indNascRapCliList where comune is null
        defaultIndNascRapCliShouldNotBeFound("comune.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali is not null
        defaultIndNascRapCliShouldBeFound("dataInizVali.specified=true");

        // Get all the indNascRapCliList where dataInizVali is null
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultIndNascRapCliShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the indNascRapCliList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultIndNascRapCliShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali is not null
        defaultIndNascRapCliShouldBeFound("dataFineVali.specified=true");

        // Get all the indNascRapCliList where dataFineVali is null
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultIndNascRapCliShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the indNascRapCliList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultIndNascRapCliShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator is not null
        defaultIndNascRapCliShouldBeFound("userIdCreator.specified=true");

        // Get all the indNascRapCliList where userIdCreator is null
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultIndNascRapCliShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the indNascRapCliList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultIndNascRapCliShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod is not null
        defaultIndNascRapCliShouldBeFound("userIdLastMod.specified=true");

        // Get all the indNascRapCliList where userIdLastMod is null
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllIndNascRapClisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);

        // Get all the indNascRapCliList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the indNascRapCliList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultIndNascRapCliShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllIndNascRapClisByIndNascToCliRapIsEqualToSomething() throws Exception {
        // Initialize the database
        indNascRapCliRepository.saveAndFlush(indNascRapCli);
        AziRappresentanteCli indNascToCliRap = AziRappresentanteCliResourceIT.createEntity(em);
        em.persist(indNascToCliRap);
        em.flush();
        indNascRapCli.setIndNascToCliRap(indNascToCliRap);
        indNascToCliRap.setCliRapToIndNasc(indNascRapCli);
        indNascRapCliRepository.saveAndFlush(indNascRapCli);
        Long indNascToCliRapId = indNascToCliRap.getId();

        // Get all the indNascRapCliList where indNascToCliRap equals to indNascToCliRapId
        defaultIndNascRapCliShouldBeFound("indNascToCliRapId.equals=" + indNascToCliRapId);

        // Get all the indNascRapCliList where indNascToCliRap equals to indNascToCliRapId + 1
        defaultIndNascRapCliShouldNotBeFound("indNascToCliRapId.equals=" + (indNascToCliRapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndNascRapCliShouldBeFound(String filter) throws Exception {
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(indNascRapCli.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].nazione").value(hasItem(DEFAULT_NAZIONE)))
            .andExpect(jsonPath("$.[*].regione").value(hasItem(DEFAULT_REGIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)))
            .andExpect(jsonPath("$.[*].comune").value(hasItem(DEFAULT_COMUNE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndNascRapCliShouldNotBeFound(String filter) throws Exception {
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingIndNascRapCli() throws Exception {
        // Get the indNascRapCli
        restIndNascRapCliMockMvc.perform(get("/api/ind-nasc-rap-clis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndNascRapCli() throws Exception {
        // Initialize the database
        indNascRapCliService.save(indNascRapCli);

        int databaseSizeBeforeUpdate = indNascRapCliRepository.findAll().size();

        // Update the indNascRapCli
        IndNascRapCli updatedIndNascRapCli = indNascRapCliRepository.findById(indNascRapCli.getId()).get();
        // Disconnect from session so that the updates on updatedIndNascRapCli are not directly saved in db
        em.detach(updatedIndNascRapCli);
        updatedIndNascRapCli
            .indirizzo(UPDATED_INDIRIZZO)
            .cap(UPDATED_CAP)
            .nazione(UPDATED_NAZIONE)
            .regione(UPDATED_REGIONE)
            .provincia(UPDATED_PROVINCIA)
            .comune(UPDATED_COMUNE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restIndNascRapCliMockMvc.perform(put("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndNascRapCli)))
            .andExpect(status().isOk());

        // Validate the IndNascRapCli in the database
        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeUpdate);
        IndNascRapCli testIndNascRapCli = indNascRapCliList.get(indNascRapCliList.size() - 1);
        assertThat(testIndNascRapCli.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
        assertThat(testIndNascRapCli.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testIndNascRapCli.getNazione()).isEqualTo(UPDATED_NAZIONE);
        assertThat(testIndNascRapCli.getRegione()).isEqualTo(UPDATED_REGIONE);
        assertThat(testIndNascRapCli.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testIndNascRapCli.getComune()).isEqualTo(UPDATED_COMUNE);
        assertThat(testIndNascRapCli.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testIndNascRapCli.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testIndNascRapCli.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testIndNascRapCli.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingIndNascRapCli() throws Exception {
        int databaseSizeBeforeUpdate = indNascRapCliRepository.findAll().size();

        // Create the IndNascRapCli

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndNascRapCliMockMvc.perform(put("/api/ind-nasc-rap-clis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(indNascRapCli)))
            .andExpect(status().isBadRequest());

        // Validate the IndNascRapCli in the database
        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndNascRapCli() throws Exception {
        // Initialize the database
        indNascRapCliService.save(indNascRapCli);

        int databaseSizeBeforeDelete = indNascRapCliRepository.findAll().size();

        // Delete the indNascRapCli
        restIndNascRapCliMockMvc.perform(delete("/api/ind-nasc-rap-clis/{id}", indNascRapCli.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndNascRapCli> indNascRapCliList = indNascRapCliRepository.findAll();
        assertThat(indNascRapCliList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndNascRapCli.class);
        IndNascRapCli indNascRapCli1 = new IndNascRapCli();
        indNascRapCli1.setId(1L);
        IndNascRapCli indNascRapCli2 = new IndNascRapCli();
        indNascRapCli2.setId(indNascRapCli1.getId());
        assertThat(indNascRapCli1).isEqualTo(indNascRapCli2);
        indNascRapCli2.setId(2L);
        assertThat(indNascRapCli1).isNotEqualTo(indNascRapCli2);
        indNascRapCli1.setId(null);
        assertThat(indNascRapCli1).isNotEqualTo(indNascRapCli2);
    }
}
