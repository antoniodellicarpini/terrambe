package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Syslog;
import com.terrambe.repository.SyslogRepository;
import com.terrambe.service.SyslogService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.SyslogCriteria;
import com.terrambe.service.SyslogQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SyslogResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class SyslogResourceIT {

    private static final String DEFAULT_NOME_TABELLA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_TABELLA = "BBBBBBBBBB";

    private static final Integer DEFAULT_ID_JUSER = 1;
    private static final Integer UPDATED_ID_JUSER = 2;
    private static final Integer SMALLER_ID_JUSER = 1 - 1;

    private static final LocalDate DEFAULT_DATA_MODIFICA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_MODIFICA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_MODIFICA = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NOME_CAMPO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_CAMPO = "BBBBBBBBBB";

    private static final String DEFAULT_VECCHIO_VALORE = "AAAAAAAAAA";
    private static final String UPDATED_VECCHIO_VALORE = "BBBBBBBBBB";

    private static final String DEFAULT_NUOVO_VALORE = "AAAAAAAAAA";
    private static final String UPDATED_NUOVO_VALORE = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private SyslogRepository syslogRepository;

    @Autowired
    private SyslogService syslogService;

    @Autowired
    private SyslogQueryService syslogQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSyslogMockMvc;

    private Syslog syslog;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SyslogResource syslogResource = new SyslogResource(syslogService, syslogQueryService);
        this.restSyslogMockMvc = MockMvcBuilders.standaloneSetup(syslogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Syslog createEntity(EntityManager em) {
        Syslog syslog = new Syslog()
            .nomeTabella(DEFAULT_NOME_TABELLA)
            .idJuser(DEFAULT_ID_JUSER)
            .dataModifica(DEFAULT_DATA_MODIFICA)
            .nomeCampo(DEFAULT_NOME_CAMPO)
            .vecchioValore(DEFAULT_VECCHIO_VALORE)
            .nuovoValore(DEFAULT_NUOVO_VALORE)
            .note(DEFAULT_NOTE);
        return syslog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Syslog createUpdatedEntity(EntityManager em) {
        Syslog syslog = new Syslog()
            .nomeTabella(UPDATED_NOME_TABELLA)
            .idJuser(UPDATED_ID_JUSER)
            .dataModifica(UPDATED_DATA_MODIFICA)
            .nomeCampo(UPDATED_NOME_CAMPO)
            .vecchioValore(UPDATED_VECCHIO_VALORE)
            .nuovoValore(UPDATED_NUOVO_VALORE)
            .note(UPDATED_NOTE);
        return syslog;
    }

    @BeforeEach
    public void initTest() {
        syslog = createEntity(em);
    }

    @Test
    @Transactional
    public void createSyslog() throws Exception {
        int databaseSizeBeforeCreate = syslogRepository.findAll().size();

        // Create the Syslog
        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isCreated());

        // Validate the Syslog in the database
        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeCreate + 1);
        Syslog testSyslog = syslogList.get(syslogList.size() - 1);
        assertThat(testSyslog.getNomeTabella()).isEqualTo(DEFAULT_NOME_TABELLA);
        assertThat(testSyslog.getIdJuser()).isEqualTo(DEFAULT_ID_JUSER);
        assertThat(testSyslog.getDataModifica()).isEqualTo(DEFAULT_DATA_MODIFICA);
        assertThat(testSyslog.getNomeCampo()).isEqualTo(DEFAULT_NOME_CAMPO);
        assertThat(testSyslog.getVecchioValore()).isEqualTo(DEFAULT_VECCHIO_VALORE);
        assertThat(testSyslog.getNuovoValore()).isEqualTo(DEFAULT_NUOVO_VALORE);
        assertThat(testSyslog.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createSyslogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = syslogRepository.findAll().size();

        // Create the Syslog with an existing ID
        syslog.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        // Validate the Syslog in the database
        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomeTabellaIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setNomeTabella(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdJuserIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setIdJuser(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataModificaIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setDataModifica(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeCampoIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setNomeCampo(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVecchioValoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setVecchioValore(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNuovoValoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = syslogRepository.findAll().size();
        // set the field null
        syslog.setNuovoValore(null);

        // Create the Syslog, which fails.

        restSyslogMockMvc.perform(post("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSyslogs() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList
        restSyslogMockMvc.perform(get("/api/syslogs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(syslog.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeTabella").value(hasItem(DEFAULT_NOME_TABELLA.toString())))
            .andExpect(jsonPath("$.[*].idJuser").value(hasItem(DEFAULT_ID_JUSER)))
            .andExpect(jsonPath("$.[*].dataModifica").value(hasItem(DEFAULT_DATA_MODIFICA.toString())))
            .andExpect(jsonPath("$.[*].nomeCampo").value(hasItem(DEFAULT_NOME_CAMPO.toString())))
            .andExpect(jsonPath("$.[*].vecchioValore").value(hasItem(DEFAULT_VECCHIO_VALORE.toString())))
            .andExpect(jsonPath("$.[*].nuovoValore").value(hasItem(DEFAULT_NUOVO_VALORE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getSyslog() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get the syslog
        restSyslogMockMvc.perform(get("/api/syslogs/{id}", syslog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(syslog.getId().intValue()))
            .andExpect(jsonPath("$.nomeTabella").value(DEFAULT_NOME_TABELLA.toString()))
            .andExpect(jsonPath("$.idJuser").value(DEFAULT_ID_JUSER))
            .andExpect(jsonPath("$.dataModifica").value(DEFAULT_DATA_MODIFICA.toString()))
            .andExpect(jsonPath("$.nomeCampo").value(DEFAULT_NOME_CAMPO.toString()))
            .andExpect(jsonPath("$.vecchioValore").value(DEFAULT_VECCHIO_VALORE.toString()))
            .andExpect(jsonPath("$.nuovoValore").value(DEFAULT_NUOVO_VALORE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllSyslogsByNomeTabellaIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeTabella equals to DEFAULT_NOME_TABELLA
        defaultSyslogShouldBeFound("nomeTabella.equals=" + DEFAULT_NOME_TABELLA);

        // Get all the syslogList where nomeTabella equals to UPDATED_NOME_TABELLA
        defaultSyslogShouldNotBeFound("nomeTabella.equals=" + UPDATED_NOME_TABELLA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNomeTabellaIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeTabella in DEFAULT_NOME_TABELLA or UPDATED_NOME_TABELLA
        defaultSyslogShouldBeFound("nomeTabella.in=" + DEFAULT_NOME_TABELLA + "," + UPDATED_NOME_TABELLA);

        // Get all the syslogList where nomeTabella equals to UPDATED_NOME_TABELLA
        defaultSyslogShouldNotBeFound("nomeTabella.in=" + UPDATED_NOME_TABELLA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNomeTabellaIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeTabella is not null
        defaultSyslogShouldBeFound("nomeTabella.specified=true");

        // Get all the syslogList where nomeTabella is null
        defaultSyslogShouldNotBeFound("nomeTabella.specified=false");
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser equals to DEFAULT_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.equals=" + DEFAULT_ID_JUSER);

        // Get all the syslogList where idJuser equals to UPDATED_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.equals=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser in DEFAULT_ID_JUSER or UPDATED_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.in=" + DEFAULT_ID_JUSER + "," + UPDATED_ID_JUSER);

        // Get all the syslogList where idJuser equals to UPDATED_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.in=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser is not null
        defaultSyslogShouldBeFound("idJuser.specified=true");

        // Get all the syslogList where idJuser is null
        defaultSyslogShouldNotBeFound("idJuser.specified=false");
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser is greater than or equal to DEFAULT_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.greaterThanOrEqual=" + DEFAULT_ID_JUSER);

        // Get all the syslogList where idJuser is greater than or equal to UPDATED_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.greaterThanOrEqual=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser is less than or equal to DEFAULT_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.lessThanOrEqual=" + DEFAULT_ID_JUSER);

        // Get all the syslogList where idJuser is less than or equal to SMALLER_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.lessThanOrEqual=" + SMALLER_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsLessThanSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser is less than DEFAULT_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.lessThan=" + DEFAULT_ID_JUSER);

        // Get all the syslogList where idJuser is less than UPDATED_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.lessThan=" + UPDATED_ID_JUSER);
    }

    @Test
    @Transactional
    public void getAllSyslogsByIdJuserIsGreaterThanSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where idJuser is greater than DEFAULT_ID_JUSER
        defaultSyslogShouldNotBeFound("idJuser.greaterThan=" + DEFAULT_ID_JUSER);

        // Get all the syslogList where idJuser is greater than SMALLER_ID_JUSER
        defaultSyslogShouldBeFound("idJuser.greaterThan=" + SMALLER_ID_JUSER);
    }


    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica equals to DEFAULT_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.equals=" + DEFAULT_DATA_MODIFICA);

        // Get all the syslogList where dataModifica equals to UPDATED_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.equals=" + UPDATED_DATA_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica in DEFAULT_DATA_MODIFICA or UPDATED_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.in=" + DEFAULT_DATA_MODIFICA + "," + UPDATED_DATA_MODIFICA);

        // Get all the syslogList where dataModifica equals to UPDATED_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.in=" + UPDATED_DATA_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica is not null
        defaultSyslogShouldBeFound("dataModifica.specified=true");

        // Get all the syslogList where dataModifica is null
        defaultSyslogShouldNotBeFound("dataModifica.specified=false");
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica is greater than or equal to DEFAULT_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.greaterThanOrEqual=" + DEFAULT_DATA_MODIFICA);

        // Get all the syslogList where dataModifica is greater than or equal to UPDATED_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.greaterThanOrEqual=" + UPDATED_DATA_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica is less than or equal to DEFAULT_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.lessThanOrEqual=" + DEFAULT_DATA_MODIFICA);

        // Get all the syslogList where dataModifica is less than or equal to SMALLER_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.lessThanOrEqual=" + SMALLER_DATA_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsLessThanSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica is less than DEFAULT_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.lessThan=" + DEFAULT_DATA_MODIFICA);

        // Get all the syslogList where dataModifica is less than UPDATED_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.lessThan=" + UPDATED_DATA_MODIFICA);
    }

    @Test
    @Transactional
    public void getAllSyslogsByDataModificaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where dataModifica is greater than DEFAULT_DATA_MODIFICA
        defaultSyslogShouldNotBeFound("dataModifica.greaterThan=" + DEFAULT_DATA_MODIFICA);

        // Get all the syslogList where dataModifica is greater than SMALLER_DATA_MODIFICA
        defaultSyslogShouldBeFound("dataModifica.greaterThan=" + SMALLER_DATA_MODIFICA);
    }


    @Test
    @Transactional
    public void getAllSyslogsByNomeCampoIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeCampo equals to DEFAULT_NOME_CAMPO
        defaultSyslogShouldBeFound("nomeCampo.equals=" + DEFAULT_NOME_CAMPO);

        // Get all the syslogList where nomeCampo equals to UPDATED_NOME_CAMPO
        defaultSyslogShouldNotBeFound("nomeCampo.equals=" + UPDATED_NOME_CAMPO);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNomeCampoIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeCampo in DEFAULT_NOME_CAMPO or UPDATED_NOME_CAMPO
        defaultSyslogShouldBeFound("nomeCampo.in=" + DEFAULT_NOME_CAMPO + "," + UPDATED_NOME_CAMPO);

        // Get all the syslogList where nomeCampo equals to UPDATED_NOME_CAMPO
        defaultSyslogShouldNotBeFound("nomeCampo.in=" + UPDATED_NOME_CAMPO);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNomeCampoIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nomeCampo is not null
        defaultSyslogShouldBeFound("nomeCampo.specified=true");

        // Get all the syslogList where nomeCampo is null
        defaultSyslogShouldNotBeFound("nomeCampo.specified=false");
    }

    @Test
    @Transactional
    public void getAllSyslogsByVecchioValoreIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where vecchioValore equals to DEFAULT_VECCHIO_VALORE
        defaultSyslogShouldBeFound("vecchioValore.equals=" + DEFAULT_VECCHIO_VALORE);

        // Get all the syslogList where vecchioValore equals to UPDATED_VECCHIO_VALORE
        defaultSyslogShouldNotBeFound("vecchioValore.equals=" + UPDATED_VECCHIO_VALORE);
    }

    @Test
    @Transactional
    public void getAllSyslogsByVecchioValoreIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where vecchioValore in DEFAULT_VECCHIO_VALORE or UPDATED_VECCHIO_VALORE
        defaultSyslogShouldBeFound("vecchioValore.in=" + DEFAULT_VECCHIO_VALORE + "," + UPDATED_VECCHIO_VALORE);

        // Get all the syslogList where vecchioValore equals to UPDATED_VECCHIO_VALORE
        defaultSyslogShouldNotBeFound("vecchioValore.in=" + UPDATED_VECCHIO_VALORE);
    }

    @Test
    @Transactional
    public void getAllSyslogsByVecchioValoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where vecchioValore is not null
        defaultSyslogShouldBeFound("vecchioValore.specified=true");

        // Get all the syslogList where vecchioValore is null
        defaultSyslogShouldNotBeFound("vecchioValore.specified=false");
    }

    @Test
    @Transactional
    public void getAllSyslogsByNuovoValoreIsEqualToSomething() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nuovoValore equals to DEFAULT_NUOVO_VALORE
        defaultSyslogShouldBeFound("nuovoValore.equals=" + DEFAULT_NUOVO_VALORE);

        // Get all the syslogList where nuovoValore equals to UPDATED_NUOVO_VALORE
        defaultSyslogShouldNotBeFound("nuovoValore.equals=" + UPDATED_NUOVO_VALORE);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNuovoValoreIsInShouldWork() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nuovoValore in DEFAULT_NUOVO_VALORE or UPDATED_NUOVO_VALORE
        defaultSyslogShouldBeFound("nuovoValore.in=" + DEFAULT_NUOVO_VALORE + "," + UPDATED_NUOVO_VALORE);

        // Get all the syslogList where nuovoValore equals to UPDATED_NUOVO_VALORE
        defaultSyslogShouldNotBeFound("nuovoValore.in=" + UPDATED_NUOVO_VALORE);
    }

    @Test
    @Transactional
    public void getAllSyslogsByNuovoValoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        syslogRepository.saveAndFlush(syslog);

        // Get all the syslogList where nuovoValore is not null
        defaultSyslogShouldBeFound("nuovoValore.specified=true");

        // Get all the syslogList where nuovoValore is null
        defaultSyslogShouldNotBeFound("nuovoValore.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSyslogShouldBeFound(String filter) throws Exception {
        restSyslogMockMvc.perform(get("/api/syslogs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(syslog.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeTabella").value(hasItem(DEFAULT_NOME_TABELLA)))
            .andExpect(jsonPath("$.[*].idJuser").value(hasItem(DEFAULT_ID_JUSER)))
            .andExpect(jsonPath("$.[*].dataModifica").value(hasItem(DEFAULT_DATA_MODIFICA.toString())))
            .andExpect(jsonPath("$.[*].nomeCampo").value(hasItem(DEFAULT_NOME_CAMPO)))
            .andExpect(jsonPath("$.[*].vecchioValore").value(hasItem(DEFAULT_VECCHIO_VALORE)))
            .andExpect(jsonPath("$.[*].nuovoValore").value(hasItem(DEFAULT_NUOVO_VALORE)))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restSyslogMockMvc.perform(get("/api/syslogs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSyslogShouldNotBeFound(String filter) throws Exception {
        restSyslogMockMvc.perform(get("/api/syslogs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSyslogMockMvc.perform(get("/api/syslogs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSyslog() throws Exception {
        // Get the syslog
        restSyslogMockMvc.perform(get("/api/syslogs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSyslog() throws Exception {
        // Initialize the database
        syslogService.save(syslog);

        int databaseSizeBeforeUpdate = syslogRepository.findAll().size();

        // Update the syslog
        Syslog updatedSyslog = syslogRepository.findById(syslog.getId()).get();
        // Disconnect from session so that the updates on updatedSyslog are not directly saved in db
        em.detach(updatedSyslog);
        updatedSyslog
            .nomeTabella(UPDATED_NOME_TABELLA)
            .idJuser(UPDATED_ID_JUSER)
            .dataModifica(UPDATED_DATA_MODIFICA)
            .nomeCampo(UPDATED_NOME_CAMPO)
            .vecchioValore(UPDATED_VECCHIO_VALORE)
            .nuovoValore(UPDATED_NUOVO_VALORE)
            .note(UPDATED_NOTE);

        restSyslogMockMvc.perform(put("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSyslog)))
            .andExpect(status().isOk());

        // Validate the Syslog in the database
        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeUpdate);
        Syslog testSyslog = syslogList.get(syslogList.size() - 1);
        assertThat(testSyslog.getNomeTabella()).isEqualTo(UPDATED_NOME_TABELLA);
        assertThat(testSyslog.getIdJuser()).isEqualTo(UPDATED_ID_JUSER);
        assertThat(testSyslog.getDataModifica()).isEqualTo(UPDATED_DATA_MODIFICA);
        assertThat(testSyslog.getNomeCampo()).isEqualTo(UPDATED_NOME_CAMPO);
        assertThat(testSyslog.getVecchioValore()).isEqualTo(UPDATED_VECCHIO_VALORE);
        assertThat(testSyslog.getNuovoValore()).isEqualTo(UPDATED_NUOVO_VALORE);
        assertThat(testSyslog.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingSyslog() throws Exception {
        int databaseSizeBeforeUpdate = syslogRepository.findAll().size();

        // Create the Syslog

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSyslogMockMvc.perform(put("/api/syslogs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(syslog)))
            .andExpect(status().isBadRequest());

        // Validate the Syslog in the database
        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSyslog() throws Exception {
        // Initialize the database
        syslogService.save(syslog);

        int databaseSizeBeforeDelete = syslogRepository.findAll().size();

        // Delete the syslog
        restSyslogMockMvc.perform(delete("/api/syslogs/{id}", syslog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Syslog> syslogList = syslogRepository.findAll();
        assertThat(syslogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Syslog.class);
        Syslog syslog1 = new Syslog();
        syslog1.setId(1L);
        Syslog syslog2 = new Syslog();
        syslog2.setId(syslog1.getId());
        assertThat(syslog1).isEqualTo(syslog2);
        syslog2.setId(2L);
        assertThat(syslog1).isNotEqualTo(syslog2);
        syslog1.setId(null);
        assertThat(syslog1).isNotEqualTo(syslog2);
    }
}
