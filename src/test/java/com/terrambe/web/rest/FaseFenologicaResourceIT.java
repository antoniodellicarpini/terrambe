package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.FaseFenologica;
import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.FaseFenologicaRepository;
import com.terrambe.service.FaseFenologicaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.FaseFenologicaCriteria;
import com.terrambe.service.FaseFenologicaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FaseFenologicaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class FaseFenologicaResourceIT {

    private static final String DEFAULT_COD_STADIO_COLT = "AAAAAAAAAA";
    private static final String UPDATED_COD_STADIO_COLT = "BBBBBBBBBB";

    private static final String DEFAULT_DES_STADIO_COLT = "AAAAAAAAAA";
    private static final String UPDATED_DES_STADIO_COLT = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private FaseFenologicaRepository faseFenologicaRepository;

    @Autowired
    private FaseFenologicaService faseFenologicaService;

    @Autowired
    private FaseFenologicaQueryService faseFenologicaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFaseFenologicaMockMvc;

    private FaseFenologica faseFenologica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FaseFenologicaResource faseFenologicaResource = new FaseFenologicaResource(faseFenologicaService, faseFenologicaQueryService);
        this.restFaseFenologicaMockMvc = MockMvcBuilders.standaloneSetup(faseFenologicaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaseFenologica createEntity(EntityManager em) {
        FaseFenologica faseFenologica = new FaseFenologica()
            .codStadioColt(DEFAULT_COD_STADIO_COLT)
            .desStadioColt(DEFAULT_DES_STADIO_COLT)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return faseFenologica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaseFenologica createUpdatedEntity(EntityManager em) {
        FaseFenologica faseFenologica = new FaseFenologica()
            .codStadioColt(UPDATED_COD_STADIO_COLT)
            .desStadioColt(UPDATED_DES_STADIO_COLT)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return faseFenologica;
    }

    @BeforeEach
    public void initTest() {
        faseFenologica = createEntity(em);
    }

    @Test
    @Transactional
    public void createFaseFenologica() throws Exception {
        int databaseSizeBeforeCreate = faseFenologicaRepository.findAll().size();

        // Create the FaseFenologica
        restFaseFenologicaMockMvc.perform(post("/api/fase-fenologicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(faseFenologica)))
            .andExpect(status().isCreated());

        // Validate the FaseFenologica in the database
        List<FaseFenologica> faseFenologicaList = faseFenologicaRepository.findAll();
        assertThat(faseFenologicaList).hasSize(databaseSizeBeforeCreate + 1);
        FaseFenologica testFaseFenologica = faseFenologicaList.get(faseFenologicaList.size() - 1);
        assertThat(testFaseFenologica.getCodStadioColt()).isEqualTo(DEFAULT_COD_STADIO_COLT);
        assertThat(testFaseFenologica.getDesStadioColt()).isEqualTo(DEFAULT_DES_STADIO_COLT);
        assertThat(testFaseFenologica.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testFaseFenologica.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testFaseFenologica.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testFaseFenologica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testFaseFenologica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testFaseFenologica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testFaseFenologica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testFaseFenologica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createFaseFenologicaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = faseFenologicaRepository.findAll().size();

        // Create the FaseFenologica with an existing ID
        faseFenologica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFaseFenologicaMockMvc.perform(post("/api/fase-fenologicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(faseFenologica)))
            .andExpect(status().isBadRequest());

        // Validate the FaseFenologica in the database
        List<FaseFenologica> faseFenologicaList = faseFenologicaRepository.findAll();
        assertThat(faseFenologicaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFaseFenologicas() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(faseFenologica.getId().intValue())))
            .andExpect(jsonPath("$.[*].codStadioColt").value(hasItem(DEFAULT_COD_STADIO_COLT.toString())))
            .andExpect(jsonPath("$.[*].desStadioColt").value(hasItem(DEFAULT_DES_STADIO_COLT.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getFaseFenologica() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get the faseFenologica
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas/{id}", faseFenologica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(faseFenologica.getId().intValue()))
            .andExpect(jsonPath("$.codStadioColt").value(DEFAULT_COD_STADIO_COLT.toString()))
            .andExpect(jsonPath("$.desStadioColt").value(DEFAULT_DES_STADIO_COLT.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByCodStadioColtIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where codStadioColt equals to DEFAULT_COD_STADIO_COLT
        defaultFaseFenologicaShouldBeFound("codStadioColt.equals=" + DEFAULT_COD_STADIO_COLT);

        // Get all the faseFenologicaList where codStadioColt equals to UPDATED_COD_STADIO_COLT
        defaultFaseFenologicaShouldNotBeFound("codStadioColt.equals=" + UPDATED_COD_STADIO_COLT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByCodStadioColtIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where codStadioColt in DEFAULT_COD_STADIO_COLT or UPDATED_COD_STADIO_COLT
        defaultFaseFenologicaShouldBeFound("codStadioColt.in=" + DEFAULT_COD_STADIO_COLT + "," + UPDATED_COD_STADIO_COLT);

        // Get all the faseFenologicaList where codStadioColt equals to UPDATED_COD_STADIO_COLT
        defaultFaseFenologicaShouldNotBeFound("codStadioColt.in=" + UPDATED_COD_STADIO_COLT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByCodStadioColtIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where codStadioColt is not null
        defaultFaseFenologicaShouldBeFound("codStadioColt.specified=true");

        // Get all the faseFenologicaList where codStadioColt is null
        defaultFaseFenologicaShouldNotBeFound("codStadioColt.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDesStadioColtIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where desStadioColt equals to DEFAULT_DES_STADIO_COLT
        defaultFaseFenologicaShouldBeFound("desStadioColt.equals=" + DEFAULT_DES_STADIO_COLT);

        // Get all the faseFenologicaList where desStadioColt equals to UPDATED_DES_STADIO_COLT
        defaultFaseFenologicaShouldNotBeFound("desStadioColt.equals=" + UPDATED_DES_STADIO_COLT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDesStadioColtIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where desStadioColt in DEFAULT_DES_STADIO_COLT or UPDATED_DES_STADIO_COLT
        defaultFaseFenologicaShouldBeFound("desStadioColt.in=" + DEFAULT_DES_STADIO_COLT + "," + UPDATED_DES_STADIO_COLT);

        // Get all the faseFenologicaList where desStadioColt equals to UPDATED_DES_STADIO_COLT
        defaultFaseFenologicaShouldNotBeFound("desStadioColt.in=" + UPDATED_DES_STADIO_COLT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDesStadioColtIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where desStadioColt is not null
        defaultFaseFenologicaShouldBeFound("desStadioColt.specified=true");

        // Get all the faseFenologicaList where desStadioColt is null
        defaultFaseFenologicaShouldNotBeFound("desStadioColt.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultFaseFenologicaShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the faseFenologicaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFaseFenologicaShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultFaseFenologicaShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the faseFenologicaList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultFaseFenologicaShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where tipoImport is not null
        defaultFaseFenologicaShouldBeFound("tipoImport.specified=true");

        // Get all the faseFenologicaList where tipoImport is null
        defaultFaseFenologicaShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where operatore equals to DEFAULT_OPERATORE
        defaultFaseFenologicaShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the faseFenologicaList where operatore equals to UPDATED_OPERATORE
        defaultFaseFenologicaShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultFaseFenologicaShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the faseFenologicaList where operatore equals to UPDATED_OPERATORE
        defaultFaseFenologicaShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where operatore is not null
        defaultFaseFenologicaShouldBeFound("operatore.specified=true");

        // Get all the faseFenologicaList where operatore is null
        defaultFaseFenologicaShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where ts equals to DEFAULT_TS
        defaultFaseFenologicaShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the faseFenologicaList where ts equals to UPDATED_TS
        defaultFaseFenologicaShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTsIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where ts in DEFAULT_TS or UPDATED_TS
        defaultFaseFenologicaShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the faseFenologicaList where ts equals to UPDATED_TS
        defaultFaseFenologicaShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where ts is not null
        defaultFaseFenologicaShouldBeFound("ts.specified=true");

        // Get all the faseFenologicaList where ts is null
        defaultFaseFenologicaShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali is not null
        defaultFaseFenologicaShouldBeFound("dataInizVali.specified=true");

        // Get all the faseFenologicaList where dataInizVali is null
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultFaseFenologicaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the faseFenologicaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultFaseFenologicaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali is not null
        defaultFaseFenologicaShouldBeFound("dataFineVali.specified=true");

        // Get all the faseFenologicaList where dataFineVali is null
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultFaseFenologicaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the faseFenologicaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultFaseFenologicaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator is not null
        defaultFaseFenologicaShouldBeFound("userIdCreator.specified=true");

        // Get all the faseFenologicaList where userIdCreator is null
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultFaseFenologicaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the faseFenologicaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultFaseFenologicaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod is not null
        defaultFaseFenologicaShouldBeFound("userIdLastMod.specified=true");

        // Get all the faseFenologicaList where userIdLastMod is null
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllFaseFenologicasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);

        // Get all the faseFenologicaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the faseFenologicaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultFaseFenologicaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllFaseFenologicasByFaseFenoToRaccordoIsEqualToSomething() throws Exception {
        // Initialize the database
        faseFenologicaRepository.saveAndFlush(faseFenologica);
        TabellaRaccordo faseFenoToRaccordo = TabellaRaccordoResourceIT.createEntity(em);
        em.persist(faseFenoToRaccordo);
        em.flush();
        faseFenologica.addFaseFenoToRaccordo(faseFenoToRaccordo);
        faseFenologicaRepository.saveAndFlush(faseFenologica);
        Long faseFenoToRaccordoId = faseFenoToRaccordo.getId();

        // Get all the faseFenologicaList where faseFenoToRaccordo equals to faseFenoToRaccordoId
        defaultFaseFenologicaShouldBeFound("faseFenoToRaccordoId.equals=" + faseFenoToRaccordoId);

        // Get all the faseFenologicaList where faseFenoToRaccordo equals to faseFenoToRaccordoId + 1
        defaultFaseFenologicaShouldNotBeFound("faseFenoToRaccordoId.equals=" + (faseFenoToRaccordoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFaseFenologicaShouldBeFound(String filter) throws Exception {
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(faseFenologica.getId().intValue())))
            .andExpect(jsonPath("$.[*].codStadioColt").value(hasItem(DEFAULT_COD_STADIO_COLT)))
            .andExpect(jsonPath("$.[*].desStadioColt").value(hasItem(DEFAULT_DES_STADIO_COLT)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFaseFenologicaShouldNotBeFound(String filter) throws Exception {
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFaseFenologica() throws Exception {
        // Get the faseFenologica
        restFaseFenologicaMockMvc.perform(get("/api/fase-fenologicas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFaseFenologica() throws Exception {
        // Initialize the database
        faseFenologicaService.save(faseFenologica);

        int databaseSizeBeforeUpdate = faseFenologicaRepository.findAll().size();

        // Update the faseFenologica
        FaseFenologica updatedFaseFenologica = faseFenologicaRepository.findById(faseFenologica.getId()).get();
        // Disconnect from session so that the updates on updatedFaseFenologica are not directly saved in db
        em.detach(updatedFaseFenologica);
        updatedFaseFenologica
            .codStadioColt(UPDATED_COD_STADIO_COLT)
            .desStadioColt(UPDATED_DES_STADIO_COLT)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restFaseFenologicaMockMvc.perform(put("/api/fase-fenologicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFaseFenologica)))
            .andExpect(status().isOk());

        // Validate the FaseFenologica in the database
        List<FaseFenologica> faseFenologicaList = faseFenologicaRepository.findAll();
        assertThat(faseFenologicaList).hasSize(databaseSizeBeforeUpdate);
        FaseFenologica testFaseFenologica = faseFenologicaList.get(faseFenologicaList.size() - 1);
        assertThat(testFaseFenologica.getCodStadioColt()).isEqualTo(UPDATED_COD_STADIO_COLT);
        assertThat(testFaseFenologica.getDesStadioColt()).isEqualTo(UPDATED_DES_STADIO_COLT);
        assertThat(testFaseFenologica.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testFaseFenologica.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testFaseFenologica.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testFaseFenologica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testFaseFenologica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testFaseFenologica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testFaseFenologica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testFaseFenologica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingFaseFenologica() throws Exception {
        int databaseSizeBeforeUpdate = faseFenologicaRepository.findAll().size();

        // Create the FaseFenologica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFaseFenologicaMockMvc.perform(put("/api/fase-fenologicas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(faseFenologica)))
            .andExpect(status().isBadRequest());

        // Validate the FaseFenologica in the database
        List<FaseFenologica> faseFenologicaList = faseFenologicaRepository.findAll();
        assertThat(faseFenologicaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFaseFenologica() throws Exception {
        // Initialize the database
        faseFenologicaService.save(faseFenologica);

        int databaseSizeBeforeDelete = faseFenologicaRepository.findAll().size();

        // Delete the faseFenologica
        restFaseFenologicaMockMvc.perform(delete("/api/fase-fenologicas/{id}", faseFenologica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FaseFenologica> faseFenologicaList = faseFenologicaRepository.findAll();
        assertThat(faseFenologicaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FaseFenologica.class);
        FaseFenologica faseFenologica1 = new FaseFenologica();
        faseFenologica1.setId(1L);
        FaseFenologica faseFenologica2 = new FaseFenologica();
        faseFenologica2.setId(faseFenologica1.getId());
        assertThat(faseFenologica1).isEqualTo(faseFenologica2);
        faseFenologica2.setId(2L);
        assertThat(faseFenologica1).isNotEqualTo(faseFenologica2);
        faseFenologica1.setId(null);
        assertThat(faseFenologica1).isNotEqualTo(faseFenologica2);
    }
}
