package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TerramColtureMascherate;
import com.terrambe.domain.ColtureBDF;
import com.terrambe.repository.TerramColtureMascherateRepository;
import com.terrambe.service.TerramColtureMascherateService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TerramColtureMascherateCriteria;
import com.terrambe.service.TerramColtureMascherateQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TerramColtureMascherateResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TerramColtureMascherateResourceIT {

    private static final String DEFAULT_CODICE_COLTURE_MASCHERATA = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_COLTURE_MASCHERATA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TerramColtureMascherateRepository terramColtureMascherateRepository;

    @Autowired
    private TerramColtureMascherateService terramColtureMascherateService;

    @Autowired
    private TerramColtureMascherateQueryService terramColtureMascherateQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTerramColtureMascherateMockMvc;

    private TerramColtureMascherate terramColtureMascherate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TerramColtureMascherateResource terramColtureMascherateResource = new TerramColtureMascherateResource(terramColtureMascherateService, terramColtureMascherateQueryService);
        this.restTerramColtureMascherateMockMvc = MockMvcBuilders.standaloneSetup(terramColtureMascherateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TerramColtureMascherate createEntity(EntityManager em) {
        TerramColtureMascherate terramColtureMascherate = new TerramColtureMascherate()
            .codiceColtureMascherata(DEFAULT_CODICE_COLTURE_MASCHERATA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return terramColtureMascherate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TerramColtureMascherate createUpdatedEntity(EntityManager em) {
        TerramColtureMascherate terramColtureMascherate = new TerramColtureMascherate()
            .codiceColtureMascherata(UPDATED_CODICE_COLTURE_MASCHERATA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return terramColtureMascherate;
    }

    @BeforeEach
    public void initTest() {
        terramColtureMascherate = createEntity(em);
    }

    @Test
    @Transactional
    public void createTerramColtureMascherate() throws Exception {
        int databaseSizeBeforeCreate = terramColtureMascherateRepository.findAll().size();

        // Create the TerramColtureMascherate
        restTerramColtureMascherateMockMvc.perform(post("/api/terram-colture-mascherates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(terramColtureMascherate)))
            .andExpect(status().isCreated());

        // Validate the TerramColtureMascherate in the database
        List<TerramColtureMascherate> terramColtureMascherateList = terramColtureMascherateRepository.findAll();
        assertThat(terramColtureMascherateList).hasSize(databaseSizeBeforeCreate + 1);
        TerramColtureMascherate testTerramColtureMascherate = terramColtureMascherateList.get(terramColtureMascherateList.size() - 1);
        assertThat(testTerramColtureMascherate.getCodiceColtureMascherata()).isEqualTo(DEFAULT_CODICE_COLTURE_MASCHERATA);
        assertThat(testTerramColtureMascherate.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTerramColtureMascherate.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTerramColtureMascherate.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTerramColtureMascherate.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTerramColtureMascherate.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTerramColtureMascherateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = terramColtureMascherateRepository.findAll().size();

        // Create the TerramColtureMascherate with an existing ID
        terramColtureMascherate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTerramColtureMascherateMockMvc.perform(post("/api/terram-colture-mascherates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(terramColtureMascherate)))
            .andExpect(status().isBadRequest());

        // Validate the TerramColtureMascherate in the database
        List<TerramColtureMascherate> terramColtureMascherateList = terramColtureMascherateRepository.findAll();
        assertThat(terramColtureMascherateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTerramColtureMascherates() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(terramColtureMascherate.getId().intValue())))
            .andExpect(jsonPath("$.[*].codiceColtureMascherata").value(hasItem(DEFAULT_CODICE_COLTURE_MASCHERATA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTerramColtureMascherate() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get the terramColtureMascherate
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates/{id}", terramColtureMascherate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(terramColtureMascherate.getId().intValue()))
            .andExpect(jsonPath("$.codiceColtureMascherata").value(DEFAULT_CODICE_COLTURE_MASCHERATA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByCodiceColtureMascherataIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where codiceColtureMascherata equals to DEFAULT_CODICE_COLTURE_MASCHERATA
        defaultTerramColtureMascherateShouldBeFound("codiceColtureMascherata.equals=" + DEFAULT_CODICE_COLTURE_MASCHERATA);

        // Get all the terramColtureMascherateList where codiceColtureMascherata equals to UPDATED_CODICE_COLTURE_MASCHERATA
        defaultTerramColtureMascherateShouldNotBeFound("codiceColtureMascherata.equals=" + UPDATED_CODICE_COLTURE_MASCHERATA);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByCodiceColtureMascherataIsInShouldWork() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where codiceColtureMascherata in DEFAULT_CODICE_COLTURE_MASCHERATA or UPDATED_CODICE_COLTURE_MASCHERATA
        defaultTerramColtureMascherateShouldBeFound("codiceColtureMascherata.in=" + DEFAULT_CODICE_COLTURE_MASCHERATA + "," + UPDATED_CODICE_COLTURE_MASCHERATA);

        // Get all the terramColtureMascherateList where codiceColtureMascherata equals to UPDATED_CODICE_COLTURE_MASCHERATA
        defaultTerramColtureMascherateShouldNotBeFound("codiceColtureMascherata.in=" + UPDATED_CODICE_COLTURE_MASCHERATA);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByCodiceColtureMascherataIsNullOrNotNull() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where codiceColtureMascherata is not null
        defaultTerramColtureMascherateShouldBeFound("codiceColtureMascherata.specified=true");

        // Get all the terramColtureMascherateList where codiceColtureMascherata is null
        defaultTerramColtureMascherateShouldNotBeFound("codiceColtureMascherata.specified=false");
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali is not null
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.specified=true");

        // Get all the terramColtureMascherateList where dataInizVali is null
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the terramColtureMascherateList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTerramColtureMascherateShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali is not null
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.specified=true");

        // Get all the terramColtureMascherateList where dataFineVali is null
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the terramColtureMascherateList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTerramColtureMascherateShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator is not null
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.specified=true");

        // Get all the terramColtureMascherateList where userIdCreator is null
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the terramColtureMascherateList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTerramColtureMascherateShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod is not null
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.specified=true");

        // Get all the terramColtureMascherateList where userIdLastMod is null
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);

        // Get all the terramColtureMascherateList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the terramColtureMascherateList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTerramColtureMascherateShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTerramColtureMascheratesByColMascToColtBdfIsEqualToSomething() throws Exception {
        // Initialize the database
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);
        ColtureBDF colMascToColtBdf = ColtureBDFResourceIT.createEntity(em);
        em.persist(colMascToColtBdf);
        em.flush();
        terramColtureMascherate.setColMascToColtBdf(colMascToColtBdf);
        terramColtureMascherateRepository.saveAndFlush(terramColtureMascherate);
        Long colMascToColtBdfId = colMascToColtBdf.getId();

        // Get all the terramColtureMascherateList where colMascToColtBdf equals to colMascToColtBdfId
        defaultTerramColtureMascherateShouldBeFound("colMascToColtBdfId.equals=" + colMascToColtBdfId);

        // Get all the terramColtureMascherateList where colMascToColtBdf equals to colMascToColtBdfId + 1
        defaultTerramColtureMascherateShouldNotBeFound("colMascToColtBdfId.equals=" + (colMascToColtBdfId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTerramColtureMascherateShouldBeFound(String filter) throws Exception {
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(terramColtureMascherate.getId().intValue())))
            .andExpect(jsonPath("$.[*].codiceColtureMascherata").value(hasItem(DEFAULT_CODICE_COLTURE_MASCHERATA)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTerramColtureMascherateShouldNotBeFound(String filter) throws Exception {
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTerramColtureMascherate() throws Exception {
        // Get the terramColtureMascherate
        restTerramColtureMascherateMockMvc.perform(get("/api/terram-colture-mascherates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTerramColtureMascherate() throws Exception {
        // Initialize the database
        terramColtureMascherateService.save(terramColtureMascherate);

        int databaseSizeBeforeUpdate = terramColtureMascherateRepository.findAll().size();

        // Update the terramColtureMascherate
        TerramColtureMascherate updatedTerramColtureMascherate = terramColtureMascherateRepository.findById(terramColtureMascherate.getId()).get();
        // Disconnect from session so that the updates on updatedTerramColtureMascherate are not directly saved in db
        em.detach(updatedTerramColtureMascherate);
        updatedTerramColtureMascherate
            .codiceColtureMascherata(UPDATED_CODICE_COLTURE_MASCHERATA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTerramColtureMascherateMockMvc.perform(put("/api/terram-colture-mascherates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTerramColtureMascherate)))
            .andExpect(status().isOk());

        // Validate the TerramColtureMascherate in the database
        List<TerramColtureMascherate> terramColtureMascherateList = terramColtureMascherateRepository.findAll();
        assertThat(terramColtureMascherateList).hasSize(databaseSizeBeforeUpdate);
        TerramColtureMascherate testTerramColtureMascherate = terramColtureMascherateList.get(terramColtureMascherateList.size() - 1);
        assertThat(testTerramColtureMascherate.getCodiceColtureMascherata()).isEqualTo(UPDATED_CODICE_COLTURE_MASCHERATA);
        assertThat(testTerramColtureMascherate.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTerramColtureMascherate.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTerramColtureMascherate.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTerramColtureMascherate.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTerramColtureMascherate.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTerramColtureMascherate() throws Exception {
        int databaseSizeBeforeUpdate = terramColtureMascherateRepository.findAll().size();

        // Create the TerramColtureMascherate

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTerramColtureMascherateMockMvc.perform(put("/api/terram-colture-mascherates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(terramColtureMascherate)))
            .andExpect(status().isBadRequest());

        // Validate the TerramColtureMascherate in the database
        List<TerramColtureMascherate> terramColtureMascherateList = terramColtureMascherateRepository.findAll();
        assertThat(terramColtureMascherateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTerramColtureMascherate() throws Exception {
        // Initialize the database
        terramColtureMascherateService.save(terramColtureMascherate);

        int databaseSizeBeforeDelete = terramColtureMascherateRepository.findAll().size();

        // Delete the terramColtureMascherate
        restTerramColtureMascherateMockMvc.perform(delete("/api/terram-colture-mascherates/{id}", terramColtureMascherate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TerramColtureMascherate> terramColtureMascherateList = terramColtureMascherateRepository.findAll();
        assertThat(terramColtureMascherateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TerramColtureMascherate.class);
        TerramColtureMascherate terramColtureMascherate1 = new TerramColtureMascherate();
        terramColtureMascherate1.setId(1L);
        TerramColtureMascherate terramColtureMascherate2 = new TerramColtureMascherate();
        terramColtureMascherate2.setId(terramColtureMascherate1.getId());
        assertThat(terramColtureMascherate1).isEqualTo(terramColtureMascherate2);
        terramColtureMascherate2.setId(2L);
        assertThat(terramColtureMascherate1).isNotEqualTo(terramColtureMascherate2);
        terramColtureMascherate1.setId(null);
        assertThat(terramColtureMascherate1).isNotEqualTo(terramColtureMascherate2);
    }
}
