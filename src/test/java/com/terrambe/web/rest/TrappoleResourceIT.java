package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.Trappole;
import com.terrambe.domain.TipoTrappola;
import com.terrambe.repository.TrappoleRepository;
import com.terrambe.service.TrappoleService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TrappoleCriteria;
import com.terrambe.service.TrappoleQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrappoleResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TrappoleResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final String DEFAULT_MARCHIO = "AAAAAAAAAA";
    private static final String UPDATED_MARCHIO = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_TRAPPOLA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_TRAPPOLA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TrappoleRepository trappoleRepository;

    @Autowired
    private TrappoleService trappoleService;

    @Autowired
    private TrappoleQueryService trappoleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTrappoleMockMvc;

    private Trappole trappole;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrappoleResource trappoleResource = new TrappoleResource(trappoleService, trappoleQueryService);
        this.restTrappoleMockMvc = MockMvcBuilders.standaloneSetup(trappoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trappole createEntity(EntityManager em) {
        Trappole trappole = new Trappole()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .marchio(DEFAULT_MARCHIO)
            .nomeTrappola(DEFAULT_NOME_TRAPPOLA)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return trappole;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trappole createUpdatedEntity(EntityManager em) {
        Trappole trappole = new Trappole()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .marchio(UPDATED_MARCHIO)
            .nomeTrappola(UPDATED_NOME_TRAPPOLA)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return trappole;
    }

    @BeforeEach
    public void initTest() {
        trappole = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrappole() throws Exception {
        int databaseSizeBeforeCreate = trappoleRepository.findAll().size();

        // Create the Trappole
        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isCreated());

        // Validate the Trappole in the database
        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeCreate + 1);
        Trappole testTrappole = trappoleList.get(trappoleList.size() - 1);
        assertThat(testTrappole.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testTrappole.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testTrappole.getMarchio()).isEqualTo(DEFAULT_MARCHIO);
        assertThat(testTrappole.getNomeTrappola()).isEqualTo(DEFAULT_NOME_TRAPPOLA);
        assertThat(testTrappole.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTrappole.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTrappole.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTrappole.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTrappole.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTrappole.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTrappoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trappoleRepository.findAll().size();

        // Create the Trappole with an existing ID
        trappole.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        // Validate the Trappole in the database
        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = trappoleRepository.findAll().size();
        // set the field null
        trappole.setIdAzienda(null);

        // Create the Trappole, which fails.

        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMarchioIsRequired() throws Exception {
        int databaseSizeBeforeTest = trappoleRepository.findAll().size();
        // set the field null
        trappole.setMarchio(null);

        // Create the Trappole, which fails.

        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeTrappolaIsRequired() throws Exception {
        int databaseSizeBeforeTest = trappoleRepository.findAll().size();
        // set the field null
        trappole.setNomeTrappola(null);

        // Create the Trappole, which fails.

        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = trappoleRepository.findAll().size();
        // set the field null
        trappole.setDescrizione(null);

        // Create the Trappole, which fails.

        restTrappoleMockMvc.perform(post("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTrappoles() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList
        restTrappoleMockMvc.perform(get("/api/trappoles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trappole.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].marchio").value(hasItem(DEFAULT_MARCHIO.toString())))
            .andExpect(jsonPath("$.[*].nomeTrappola").value(hasItem(DEFAULT_NOME_TRAPPOLA.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTrappole() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get the trappole
        restTrappoleMockMvc.perform(get("/api/trappoles/{id}", trappole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trappole.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.marchio").value(DEFAULT_MARCHIO.toString()))
            .andExpect(jsonPath("$.nomeTrappola").value(DEFAULT_NOME_TRAPPOLA.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the trappoleList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the trappoleList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda is not null
        defaultTrappoleShouldBeFound("idAzienda.specified=true");

        // Get all the trappoleList where idAzienda is null
        defaultTrappoleShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the trappoleList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the trappoleList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the trappoleList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultTrappoleShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the trappoleList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultTrappoleShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd is not null
        defaultTrappoleShouldBeFound("idUnitaProd.specified=true");

        // Get all the trappoleList where idUnitaProd is null
        defaultTrappoleShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultTrappoleShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the trappoleList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultTrappoleShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllTrappolesByMarchioIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where marchio equals to DEFAULT_MARCHIO
        defaultTrappoleShouldBeFound("marchio.equals=" + DEFAULT_MARCHIO);

        // Get all the trappoleList where marchio equals to UPDATED_MARCHIO
        defaultTrappoleShouldNotBeFound("marchio.equals=" + UPDATED_MARCHIO);
    }

    @Test
    @Transactional
    public void getAllTrappolesByMarchioIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where marchio in DEFAULT_MARCHIO or UPDATED_MARCHIO
        defaultTrappoleShouldBeFound("marchio.in=" + DEFAULT_MARCHIO + "," + UPDATED_MARCHIO);

        // Get all the trappoleList where marchio equals to UPDATED_MARCHIO
        defaultTrappoleShouldNotBeFound("marchio.in=" + UPDATED_MARCHIO);
    }

    @Test
    @Transactional
    public void getAllTrappolesByMarchioIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where marchio is not null
        defaultTrappoleShouldBeFound("marchio.specified=true");

        // Get all the trappoleList where marchio is null
        defaultTrappoleShouldNotBeFound("marchio.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByNomeTrappolaIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where nomeTrappola equals to DEFAULT_NOME_TRAPPOLA
        defaultTrappoleShouldBeFound("nomeTrappola.equals=" + DEFAULT_NOME_TRAPPOLA);

        // Get all the trappoleList where nomeTrappola equals to UPDATED_NOME_TRAPPOLA
        defaultTrappoleShouldNotBeFound("nomeTrappola.equals=" + UPDATED_NOME_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByNomeTrappolaIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where nomeTrappola in DEFAULT_NOME_TRAPPOLA or UPDATED_NOME_TRAPPOLA
        defaultTrappoleShouldBeFound("nomeTrappola.in=" + DEFAULT_NOME_TRAPPOLA + "," + UPDATED_NOME_TRAPPOLA);

        // Get all the trappoleList where nomeTrappola equals to UPDATED_NOME_TRAPPOLA
        defaultTrappoleShouldNotBeFound("nomeTrappola.in=" + UPDATED_NOME_TRAPPOLA);
    }

    @Test
    @Transactional
    public void getAllTrappolesByNomeTrappolaIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where nomeTrappola is not null
        defaultTrappoleShouldBeFound("nomeTrappola.specified=true");

        // Get all the trappoleList where nomeTrappola is null
        defaultTrappoleShouldNotBeFound("nomeTrappola.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTrappoleShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the trappoleList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTrappoleShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTrappoleShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the trappoleList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTrappoleShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where descrizione is not null
        defaultTrappoleShouldBeFound("descrizione.specified=true");

        // Get all the trappoleList where descrizione is null
        defaultTrappoleShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali is not null
        defaultTrappoleShouldBeFound("dataInizVali.specified=true");

        // Get all the trappoleList where dataInizVali is null
        defaultTrappoleShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTrappoleShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the trappoleList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTrappoleShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali is not null
        defaultTrappoleShouldBeFound("dataFineVali.specified=true");

        // Get all the trappoleList where dataFineVali is null
        defaultTrappoleShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTrappolesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTrappoleShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the trappoleList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTrappoleShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator is not null
        defaultTrappoleShouldBeFound("userIdCreator.specified=true");

        // Get all the trappoleList where userIdCreator is null
        defaultTrappoleShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTrappoleShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the trappoleList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTrappoleShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod is not null
        defaultTrappoleShouldBeFound("userIdLastMod.specified=true");

        // Get all the trappoleList where userIdLastMod is null
        defaultTrappoleShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTrappolesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);

        // Get all the trappoleList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTrappoleShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the trappoleList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTrappoleShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTrappolesByTrapToTipoTrapIsEqualToSomething() throws Exception {
        // Initialize the database
        trappoleRepository.saveAndFlush(trappole);
        TipoTrappola trapToTipoTrap = TipoTrappolaResourceIT.createEntity(em);
        em.persist(trapToTipoTrap);
        em.flush();
        trappole.setTrapToTipoTrap(trapToTipoTrap);
        trappoleRepository.saveAndFlush(trappole);
        Long trapToTipoTrapId = trapToTipoTrap.getId();

        // Get all the trappoleList where trapToTipoTrap equals to trapToTipoTrapId
        defaultTrappoleShouldBeFound("trapToTipoTrapId.equals=" + trapToTipoTrapId);

        // Get all the trappoleList where trapToTipoTrap equals to trapToTipoTrapId + 1
        defaultTrappoleShouldNotBeFound("trapToTipoTrapId.equals=" + (trapToTipoTrapId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTrappoleShouldBeFound(String filter) throws Exception {
        restTrappoleMockMvc.perform(get("/api/trappoles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trappole.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].marchio").value(hasItem(DEFAULT_MARCHIO)))
            .andExpect(jsonPath("$.[*].nomeTrappola").value(hasItem(DEFAULT_NOME_TRAPPOLA)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTrappoleMockMvc.perform(get("/api/trappoles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTrappoleShouldNotBeFound(String filter) throws Exception {
        restTrappoleMockMvc.perform(get("/api/trappoles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTrappoleMockMvc.perform(get("/api/trappoles/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTrappole() throws Exception {
        // Get the trappole
        restTrappoleMockMvc.perform(get("/api/trappoles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrappole() throws Exception {
        // Initialize the database
        trappoleService.save(trappole);

        int databaseSizeBeforeUpdate = trappoleRepository.findAll().size();

        // Update the trappole
        Trappole updatedTrappole = trappoleRepository.findById(trappole.getId()).get();
        // Disconnect from session so that the updates on updatedTrappole are not directly saved in db
        em.detach(updatedTrappole);
        updatedTrappole
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .marchio(UPDATED_MARCHIO)
            .nomeTrappola(UPDATED_NOME_TRAPPOLA)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTrappoleMockMvc.perform(put("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrappole)))
            .andExpect(status().isOk());

        // Validate the Trappole in the database
        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeUpdate);
        Trappole testTrappole = trappoleList.get(trappoleList.size() - 1);
        assertThat(testTrappole.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testTrappole.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testTrappole.getMarchio()).isEqualTo(UPDATED_MARCHIO);
        assertThat(testTrappole.getNomeTrappola()).isEqualTo(UPDATED_NOME_TRAPPOLA);
        assertThat(testTrappole.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTrappole.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTrappole.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTrappole.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTrappole.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTrappole.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTrappole() throws Exception {
        int databaseSizeBeforeUpdate = trappoleRepository.findAll().size();

        // Create the Trappole

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrappoleMockMvc.perform(put("/api/trappoles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trappole)))
            .andExpect(status().isBadRequest());

        // Validate the Trappole in the database
        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrappole() throws Exception {
        // Initialize the database
        trappoleService.save(trappole);

        int databaseSizeBeforeDelete = trappoleRepository.findAll().size();

        // Delete the trappole
        restTrappoleMockMvc.perform(delete("/api/trappoles/{id}", trappole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Trappole> trappoleList = trappoleRepository.findAll();
        assertThat(trappoleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Trappole.class);
        Trappole trappole1 = new Trappole();
        trappole1.setId(1L);
        Trappole trappole2 = new Trappole();
        trappole2.setId(trappole1.getId());
        assertThat(trappole1).isEqualTo(trappole2);
        trappole2.setId(2L);
        assertThat(trappole1).isNotEqualTo(trappole2);
        trappole1.setId(null);
        assertThat(trappole1).isNotEqualTo(trappole2);
    }
}
