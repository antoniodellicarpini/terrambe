package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcIrr;
import com.terrambe.domain.RegOpcIrrAppz;
import com.terrambe.domain.TipoIrr;
import com.terrambe.domain.MotivoIrr;
import com.terrambe.repository.RegOpcIrrRepository;
import com.terrambe.service.RegOpcIrrService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcIrrCriteria;
import com.terrambe.service.RegOpcIrrQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcIrrResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcIrrResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_OPERAZIONE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_OPERAZIONE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_OPERAZIONE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEMPO_IMPIEGATO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPO_IMPIEGATO = "BBBBBBBBBB";

    private static final Float DEFAULT_VOLUME_ACQUA = 1F;
    private static final Float UPDATED_VOLUME_ACQUA = 2F;
    private static final Float SMALLER_VOLUME_ACQUA = 1F - 1F;

    private static final LocalDate DEFAULT_DATA_INIZIO_PERIOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO_PERIOD = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZIO_PERIOD = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_PERIOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_PERIOD = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_PERIOD = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_FREQUENZA_GIORNI = 1;
    private static final Integer UPDATED_FREQUENZA_GIORNI = 2;
    private static final Integer SMALLER_FREQUENZA_GIORNI = 1 - 1;

    private static final Long DEFAULT_ID_COLTURA = 1L;
    private static final Long UPDATED_ID_COLTURA = 2L;
    private static final Long SMALLER_ID_COLTURA = 1L - 1L;

    private static final Long DEFAULT_ID_OPERATORE = 1L;
    private static final Long UPDATED_ID_OPERATORE = 2L;
    private static final Long SMALLER_ID_OPERATORE = 1L - 1L;

    private static final Long DEFAULT_ID_MEZZO = 1L;
    private static final Long UPDATED_ID_MEZZO = 2L;
    private static final Long SMALLER_ID_MEZZO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RegOpcIrrRepository regOpcIrrRepository;

    @Autowired
    private RegOpcIrrService regOpcIrrService;

    @Autowired
    private RegOpcIrrQueryService regOpcIrrQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcIrrMockMvc;

    private RegOpcIrr regOpcIrr;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcIrrResource regOpcIrrResource = new RegOpcIrrResource(regOpcIrrService, regOpcIrrQueryService);
        this.restRegOpcIrrMockMvc = MockMvcBuilders.standaloneSetup(regOpcIrrResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcIrr createEntity(EntityManager em) {
        RegOpcIrr regOpcIrr = new RegOpcIrr()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .dataOperazione(DEFAULT_DATA_OPERAZIONE)
            .tempoImpiegato(DEFAULT_TEMPO_IMPIEGATO)
            .volumeAcqua(DEFAULT_VOLUME_ACQUA)
            .dataInizioPeriod(DEFAULT_DATA_INIZIO_PERIOD)
            .dataFinePeriod(DEFAULT_DATA_FINE_PERIOD)
            .frequenzaGiorni(DEFAULT_FREQUENZA_GIORNI)
            .idColtura(DEFAULT_ID_COLTURA)
            .idOperatore(DEFAULT_ID_OPERATORE)
            .idMezzo(DEFAULT_ID_MEZZO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return regOpcIrr;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcIrr createUpdatedEntity(EntityManager em) {
        RegOpcIrr regOpcIrr = new RegOpcIrr()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volumeAcqua(UPDATED_VOLUME_ACQUA)
            .dataInizioPeriod(UPDATED_DATA_INIZIO_PERIOD)
            .dataFinePeriod(UPDATED_DATA_FINE_PERIOD)
            .frequenzaGiorni(UPDATED_FREQUENZA_GIORNI)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return regOpcIrr;
    }

    @BeforeEach
    public void initTest() {
        regOpcIrr = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcIrr() throws Exception {
        int databaseSizeBeforeCreate = regOpcIrrRepository.findAll().size();

        // Create the RegOpcIrr
        restRegOpcIrrMockMvc.perform(post("/api/reg-opc-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrr)))
            .andExpect(status().isCreated());

        // Validate the RegOpcIrr in the database
        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcIrr testRegOpcIrr = regOpcIrrList.get(regOpcIrrList.size() - 1);
        assertThat(testRegOpcIrr.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcIrr.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcIrr.getDataOperazione()).isEqualTo(DEFAULT_DATA_OPERAZIONE);
        assertThat(testRegOpcIrr.getTempoImpiegato()).isEqualTo(DEFAULT_TEMPO_IMPIEGATO);
        assertThat(testRegOpcIrr.getVolumeAcqua()).isEqualTo(DEFAULT_VOLUME_ACQUA);
        assertThat(testRegOpcIrr.getDataInizioPeriod()).isEqualTo(DEFAULT_DATA_INIZIO_PERIOD);
        assertThat(testRegOpcIrr.getDataFinePeriod()).isEqualTo(DEFAULT_DATA_FINE_PERIOD);
        assertThat(testRegOpcIrr.getFrequenzaGiorni()).isEqualTo(DEFAULT_FREQUENZA_GIORNI);
        assertThat(testRegOpcIrr.getIdColtura()).isEqualTo(DEFAULT_ID_COLTURA);
        assertThat(testRegOpcIrr.getIdOperatore()).isEqualTo(DEFAULT_ID_OPERATORE);
        assertThat(testRegOpcIrr.getIdMezzo()).isEqualTo(DEFAULT_ID_MEZZO);
        assertThat(testRegOpcIrr.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcIrr.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcIrr.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcIrr.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRegOpcIrr.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRegOpcIrrWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcIrrRepository.findAll().size();

        // Create the RegOpcIrr with an existing ID
        regOpcIrr.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcIrrMockMvc.perform(post("/api/reg-opc-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcIrr in the database
        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcIrrRepository.findAll().size();
        // set the field null
        regOpcIrr.setIdAzienda(null);

        // Create the RegOpcIrr, which fails.

        restRegOpcIrrMockMvc.perform(post("/api/reg-opc-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrr)))
            .andExpect(status().isBadRequest());

        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrs() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO.toString())))
            .andExpect(jsonPath("$.[*].volumeAcqua").value(hasItem(DEFAULT_VOLUME_ACQUA.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizioPeriod").value(hasItem(DEFAULT_DATA_INIZIO_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].dataFinePeriod").value(hasItem(DEFAULT_DATA_FINE_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].frequenzaGiorni").value(hasItem(DEFAULT_FREQUENZA_GIORNI)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRegOpcIrr() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get the regOpcIrr
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs/{id}", regOpcIrr.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcIrr.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.dataOperazione").value(DEFAULT_DATA_OPERAZIONE.toString()))
            .andExpect(jsonPath("$.tempoImpiegato").value(DEFAULT_TEMPO_IMPIEGATO.toString()))
            .andExpect(jsonPath("$.volumeAcqua").value(DEFAULT_VOLUME_ACQUA.doubleValue()))
            .andExpect(jsonPath("$.dataInizioPeriod").value(DEFAULT_DATA_INIZIO_PERIOD.toString()))
            .andExpect(jsonPath("$.dataFinePeriod").value(DEFAULT_DATA_FINE_PERIOD.toString()))
            .andExpect(jsonPath("$.frequenzaGiorni").value(DEFAULT_FREQUENZA_GIORNI))
            .andExpect(jsonPath("$.idColtura").value(DEFAULT_ID_COLTURA.intValue()))
            .andExpect(jsonPath("$.idOperatore").value(DEFAULT_ID_OPERATORE.intValue()))
            .andExpect(jsonPath("$.idMezzo").value(DEFAULT_ID_MEZZO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda is not null
        defaultRegOpcIrrShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcIrrList where idAzienda is null
        defaultRegOpcIrrShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcIrrShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcIrrShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd is not null
        defaultRegOpcIrrShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcIrrList where idUnitaProd is null
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcIrrShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione equals to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.equals=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.equals=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione in DEFAULT_DATA_OPERAZIONE or UPDATED_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.in=" + DEFAULT_DATA_OPERAZIONE + "," + UPDATED_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione equals to UPDATED_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.in=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione is not null
        defaultRegOpcIrrShouldBeFound("dataOperazione.specified=true");

        // Get all the regOpcIrrList where dataOperazione is null
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione is greater than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.greaterThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione is greater than or equal to UPDATED_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.greaterThanOrEqual=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione is less than or equal to DEFAULT_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.lessThanOrEqual=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione is less than or equal to SMALLER_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.lessThanOrEqual=" + SMALLER_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione is less than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.lessThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione is less than UPDATED_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.lessThan=" + UPDATED_DATA_OPERAZIONE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataOperazioneIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataOperazione is greater than DEFAULT_DATA_OPERAZIONE
        defaultRegOpcIrrShouldNotBeFound("dataOperazione.greaterThan=" + DEFAULT_DATA_OPERAZIONE);

        // Get all the regOpcIrrList where dataOperazione is greater than SMALLER_DATA_OPERAZIONE
        defaultRegOpcIrrShouldBeFound("dataOperazione.greaterThan=" + SMALLER_DATA_OPERAZIONE);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByTempoImpiegatoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where tempoImpiegato equals to DEFAULT_TEMPO_IMPIEGATO
        defaultRegOpcIrrShouldBeFound("tempoImpiegato.equals=" + DEFAULT_TEMPO_IMPIEGATO);

        // Get all the regOpcIrrList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcIrrShouldNotBeFound("tempoImpiegato.equals=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByTempoImpiegatoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where tempoImpiegato in DEFAULT_TEMPO_IMPIEGATO or UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcIrrShouldBeFound("tempoImpiegato.in=" + DEFAULT_TEMPO_IMPIEGATO + "," + UPDATED_TEMPO_IMPIEGATO);

        // Get all the regOpcIrrList where tempoImpiegato equals to UPDATED_TEMPO_IMPIEGATO
        defaultRegOpcIrrShouldNotBeFound("tempoImpiegato.in=" + UPDATED_TEMPO_IMPIEGATO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByTempoImpiegatoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where tempoImpiegato is not null
        defaultRegOpcIrrShouldBeFound("tempoImpiegato.specified=true");

        // Get all the regOpcIrrList where tempoImpiegato is null
        defaultRegOpcIrrShouldNotBeFound("tempoImpiegato.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua equals to DEFAULT_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.equals=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua equals to UPDATED_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.equals=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua in DEFAULT_VOLUME_ACQUA or UPDATED_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.in=" + DEFAULT_VOLUME_ACQUA + "," + UPDATED_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua equals to UPDATED_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.in=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua is not null
        defaultRegOpcIrrShouldBeFound("volumeAcqua.specified=true");

        // Get all the regOpcIrrList where volumeAcqua is null
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua is greater than or equal to DEFAULT_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.greaterThanOrEqual=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua is greater than or equal to UPDATED_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.greaterThanOrEqual=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua is less than or equal to DEFAULT_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.lessThanOrEqual=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua is less than or equal to SMALLER_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.lessThanOrEqual=" + SMALLER_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua is less than DEFAULT_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.lessThan=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua is less than UPDATED_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.lessThan=" + UPDATED_VOLUME_ACQUA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByVolumeAcquaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where volumeAcqua is greater than DEFAULT_VOLUME_ACQUA
        defaultRegOpcIrrShouldNotBeFound("volumeAcqua.greaterThan=" + DEFAULT_VOLUME_ACQUA);

        // Get all the regOpcIrrList where volumeAcqua is greater than SMALLER_VOLUME_ACQUA
        defaultRegOpcIrrShouldBeFound("volumeAcqua.greaterThan=" + SMALLER_VOLUME_ACQUA);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod equals to DEFAULT_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.equals=" + DEFAULT_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod equals to UPDATED_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.equals=" + UPDATED_DATA_INIZIO_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod in DEFAULT_DATA_INIZIO_PERIOD or UPDATED_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.in=" + DEFAULT_DATA_INIZIO_PERIOD + "," + UPDATED_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod equals to UPDATED_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.in=" + UPDATED_DATA_INIZIO_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod is not null
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.specified=true");

        // Get all the regOpcIrrList where dataInizioPeriod is null
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod is greater than or equal to DEFAULT_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.greaterThanOrEqual=" + DEFAULT_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod is greater than or equal to UPDATED_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.greaterThanOrEqual=" + UPDATED_DATA_INIZIO_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod is less than or equal to DEFAULT_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.lessThanOrEqual=" + DEFAULT_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod is less than or equal to SMALLER_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.lessThanOrEqual=" + SMALLER_DATA_INIZIO_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod is less than DEFAULT_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.lessThan=" + DEFAULT_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod is less than UPDATED_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.lessThan=" + UPDATED_DATA_INIZIO_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizioPeriodIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizioPeriod is greater than DEFAULT_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataInizioPeriod.greaterThan=" + DEFAULT_DATA_INIZIO_PERIOD);

        // Get all the regOpcIrrList where dataInizioPeriod is greater than SMALLER_DATA_INIZIO_PERIOD
        defaultRegOpcIrrShouldBeFound("dataInizioPeriod.greaterThan=" + SMALLER_DATA_INIZIO_PERIOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod equals to DEFAULT_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.equals=" + DEFAULT_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod equals to UPDATED_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.equals=" + UPDATED_DATA_FINE_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod in DEFAULT_DATA_FINE_PERIOD or UPDATED_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.in=" + DEFAULT_DATA_FINE_PERIOD + "," + UPDATED_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod equals to UPDATED_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.in=" + UPDATED_DATA_FINE_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod is not null
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.specified=true");

        // Get all the regOpcIrrList where dataFinePeriod is null
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod is greater than or equal to DEFAULT_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.greaterThanOrEqual=" + DEFAULT_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod is greater than or equal to UPDATED_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.greaterThanOrEqual=" + UPDATED_DATA_FINE_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod is less than or equal to DEFAULT_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.lessThanOrEqual=" + DEFAULT_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod is less than or equal to SMALLER_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.lessThanOrEqual=" + SMALLER_DATA_FINE_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod is less than DEFAULT_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.lessThan=" + DEFAULT_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod is less than UPDATED_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.lessThan=" + UPDATED_DATA_FINE_PERIOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFinePeriodIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFinePeriod is greater than DEFAULT_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldNotBeFound("dataFinePeriod.greaterThan=" + DEFAULT_DATA_FINE_PERIOD);

        // Get all the regOpcIrrList where dataFinePeriod is greater than SMALLER_DATA_FINE_PERIOD
        defaultRegOpcIrrShouldBeFound("dataFinePeriod.greaterThan=" + SMALLER_DATA_FINE_PERIOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni equals to DEFAULT_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.equals=" + DEFAULT_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni equals to UPDATED_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.equals=" + UPDATED_FREQUENZA_GIORNI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni in DEFAULT_FREQUENZA_GIORNI or UPDATED_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.in=" + DEFAULT_FREQUENZA_GIORNI + "," + UPDATED_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni equals to UPDATED_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.in=" + UPDATED_FREQUENZA_GIORNI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni is not null
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.specified=true");

        // Get all the regOpcIrrList where frequenzaGiorni is null
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni is greater than or equal to DEFAULT_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.greaterThanOrEqual=" + DEFAULT_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni is greater than or equal to UPDATED_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.greaterThanOrEqual=" + UPDATED_FREQUENZA_GIORNI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni is less than or equal to DEFAULT_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.lessThanOrEqual=" + DEFAULT_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni is less than or equal to SMALLER_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.lessThanOrEqual=" + SMALLER_FREQUENZA_GIORNI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni is less than DEFAULT_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.lessThan=" + DEFAULT_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni is less than UPDATED_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.lessThan=" + UPDATED_FREQUENZA_GIORNI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByFrequenzaGiorniIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where frequenzaGiorni is greater than DEFAULT_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldNotBeFound("frequenzaGiorni.greaterThan=" + DEFAULT_FREQUENZA_GIORNI);

        // Get all the regOpcIrrList where frequenzaGiorni is greater than SMALLER_FREQUENZA_GIORNI
        defaultRegOpcIrrShouldBeFound("frequenzaGiorni.greaterThan=" + SMALLER_FREQUENZA_GIORNI);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura equals to DEFAULT_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.equals=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.equals=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura in DEFAULT_ID_COLTURA or UPDATED_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.in=" + DEFAULT_ID_COLTURA + "," + UPDATED_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura equals to UPDATED_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.in=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura is not null
        defaultRegOpcIrrShouldBeFound("idColtura.specified=true");

        // Get all the regOpcIrrList where idColtura is null
        defaultRegOpcIrrShouldNotBeFound("idColtura.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura is greater than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.greaterThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura is greater than or equal to UPDATED_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.greaterThanOrEqual=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura is less than or equal to DEFAULT_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.lessThanOrEqual=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura is less than or equal to SMALLER_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.lessThanOrEqual=" + SMALLER_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura is less than DEFAULT_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.lessThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura is less than UPDATED_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.lessThan=" + UPDATED_ID_COLTURA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdColturaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idColtura is greater than DEFAULT_ID_COLTURA
        defaultRegOpcIrrShouldNotBeFound("idColtura.greaterThan=" + DEFAULT_ID_COLTURA);

        // Get all the regOpcIrrList where idColtura is greater than SMALLER_ID_COLTURA
        defaultRegOpcIrrShouldBeFound("idColtura.greaterThan=" + SMALLER_ID_COLTURA);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore equals to DEFAULT_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.equals=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.equals=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore in DEFAULT_ID_OPERATORE or UPDATED_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.in=" + DEFAULT_ID_OPERATORE + "," + UPDATED_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore equals to UPDATED_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.in=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore is not null
        defaultRegOpcIrrShouldBeFound("idOperatore.specified=true");

        // Get all the regOpcIrrList where idOperatore is null
        defaultRegOpcIrrShouldNotBeFound("idOperatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore is greater than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.greaterThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore is greater than or equal to UPDATED_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.greaterThanOrEqual=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore is less than or equal to DEFAULT_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.lessThanOrEqual=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore is less than or equal to SMALLER_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.lessThanOrEqual=" + SMALLER_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore is less than DEFAULT_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.lessThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore is less than UPDATED_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.lessThan=" + UPDATED_ID_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdOperatoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idOperatore is greater than DEFAULT_ID_OPERATORE
        defaultRegOpcIrrShouldNotBeFound("idOperatore.greaterThan=" + DEFAULT_ID_OPERATORE);

        // Get all the regOpcIrrList where idOperatore is greater than SMALLER_ID_OPERATORE
        defaultRegOpcIrrShouldBeFound("idOperatore.greaterThan=" + SMALLER_ID_OPERATORE);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo equals to DEFAULT_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.equals=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.equals=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo in DEFAULT_ID_MEZZO or UPDATED_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.in=" + DEFAULT_ID_MEZZO + "," + UPDATED_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo equals to UPDATED_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.in=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo is not null
        defaultRegOpcIrrShouldBeFound("idMezzo.specified=true");

        // Get all the regOpcIrrList where idMezzo is null
        defaultRegOpcIrrShouldNotBeFound("idMezzo.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo is greater than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.greaterThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo is greater than or equal to UPDATED_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.greaterThanOrEqual=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo is less than or equal to DEFAULT_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.lessThanOrEqual=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo is less than or equal to SMALLER_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.lessThanOrEqual=" + SMALLER_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo is less than DEFAULT_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.lessThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo is less than UPDATED_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.lessThan=" + UPDATED_ID_MEZZO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByIdMezzoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where idMezzo is greater than DEFAULT_ID_MEZZO
        defaultRegOpcIrrShouldNotBeFound("idMezzo.greaterThan=" + DEFAULT_ID_MEZZO);

        // Get all the regOpcIrrList where idMezzo is greater than SMALLER_ID_MEZZO
        defaultRegOpcIrrShouldBeFound("idMezzo.greaterThan=" + SMALLER_ID_MEZZO);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali is not null
        defaultRegOpcIrrShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcIrrList where dataInizVali is null
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcIrrShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali is not null
        defaultRegOpcIrrShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcIrrList where dataFineVali is null
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcIrrShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator is not null
        defaultRegOpcIrrShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcIrrList where userIdCreator is null
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcIrrShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod is not null
        defaultRegOpcIrrShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcIrrList where userIdLastMod is null
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);

        // Get all the regOpcIrrList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcIrrShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByOpcIrrToAppzIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        RegOpcIrrAppz opcIrrToAppz = RegOpcIrrAppzResourceIT.createEntity(em);
        em.persist(opcIrrToAppz);
        em.flush();
        regOpcIrr.addOpcIrrToAppz(opcIrrToAppz);
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        Long opcIrrToAppzId = opcIrrToAppz.getId();

        // Get all the regOpcIrrList where opcIrrToAppz equals to opcIrrToAppzId
        defaultRegOpcIrrShouldBeFound("opcIrrToAppzId.equals=" + opcIrrToAppzId);

        // Get all the regOpcIrrList where opcIrrToAppz equals to opcIrrToAppzId + 1
        defaultRegOpcIrrShouldNotBeFound("opcIrrToAppzId.equals=" + (opcIrrToAppzId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByOpcIrrToTipoIrrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        TipoIrr opcIrrToTipoIrr = TipoIrrResourceIT.createEntity(em);
        em.persist(opcIrrToTipoIrr);
        em.flush();
        regOpcIrr.setOpcIrrToTipoIrr(opcIrrToTipoIrr);
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        Long opcIrrToTipoIrrId = opcIrrToTipoIrr.getId();

        // Get all the regOpcIrrList where opcIrrToTipoIrr equals to opcIrrToTipoIrrId
        defaultRegOpcIrrShouldBeFound("opcIrrToTipoIrrId.equals=" + opcIrrToTipoIrrId);

        // Get all the regOpcIrrList where opcIrrToTipoIrr equals to opcIrrToTipoIrrId + 1
        defaultRegOpcIrrShouldNotBeFound("opcIrrToTipoIrrId.equals=" + (opcIrrToTipoIrrId + 1));
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrsByOpcIrrToMotivoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        MotivoIrr opcIrrToMotivo = MotivoIrrResourceIT.createEntity(em);
        em.persist(opcIrrToMotivo);
        em.flush();
        regOpcIrr.setOpcIrrToMotivo(opcIrrToMotivo);
        regOpcIrrRepository.saveAndFlush(regOpcIrr);
        Long opcIrrToMotivoId = opcIrrToMotivo.getId();

        // Get all the regOpcIrrList where opcIrrToMotivo equals to opcIrrToMotivoId
        defaultRegOpcIrrShouldBeFound("opcIrrToMotivoId.equals=" + opcIrrToMotivoId);

        // Get all the regOpcIrrList where opcIrrToMotivo equals to opcIrrToMotivoId + 1
        defaultRegOpcIrrShouldNotBeFound("opcIrrToMotivoId.equals=" + (opcIrrToMotivoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcIrrShouldBeFound(String filter) throws Exception {
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcIrr.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].dataOperazione").value(hasItem(DEFAULT_DATA_OPERAZIONE.toString())))
            .andExpect(jsonPath("$.[*].tempoImpiegato").value(hasItem(DEFAULT_TEMPO_IMPIEGATO)))
            .andExpect(jsonPath("$.[*].volumeAcqua").value(hasItem(DEFAULT_VOLUME_ACQUA.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizioPeriod").value(hasItem(DEFAULT_DATA_INIZIO_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].dataFinePeriod").value(hasItem(DEFAULT_DATA_FINE_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].frequenzaGiorni").value(hasItem(DEFAULT_FREQUENZA_GIORNI)))
            .andExpect(jsonPath("$.[*].idColtura").value(hasItem(DEFAULT_ID_COLTURA.intValue())))
            .andExpect(jsonPath("$.[*].idOperatore").value(hasItem(DEFAULT_ID_OPERATORE.intValue())))
            .andExpect(jsonPath("$.[*].idMezzo").value(hasItem(DEFAULT_ID_MEZZO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcIrrShouldNotBeFound(String filter) throws Exception {
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcIrr() throws Exception {
        // Get the regOpcIrr
        restRegOpcIrrMockMvc.perform(get("/api/reg-opc-irrs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcIrr() throws Exception {
        // Initialize the database
        regOpcIrrService.save(regOpcIrr);

        int databaseSizeBeforeUpdate = regOpcIrrRepository.findAll().size();

        // Update the regOpcIrr
        RegOpcIrr updatedRegOpcIrr = regOpcIrrRepository.findById(regOpcIrr.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcIrr are not directly saved in db
        em.detach(updatedRegOpcIrr);
        updatedRegOpcIrr
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .dataOperazione(UPDATED_DATA_OPERAZIONE)
            .tempoImpiegato(UPDATED_TEMPO_IMPIEGATO)
            .volumeAcqua(UPDATED_VOLUME_ACQUA)
            .dataInizioPeriod(UPDATED_DATA_INIZIO_PERIOD)
            .dataFinePeriod(UPDATED_DATA_FINE_PERIOD)
            .frequenzaGiorni(UPDATED_FREQUENZA_GIORNI)
            .idColtura(UPDATED_ID_COLTURA)
            .idOperatore(UPDATED_ID_OPERATORE)
            .idMezzo(UPDATED_ID_MEZZO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRegOpcIrrMockMvc.perform(put("/api/reg-opc-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcIrr)))
            .andExpect(status().isOk());

        // Validate the RegOpcIrr in the database
        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeUpdate);
        RegOpcIrr testRegOpcIrr = regOpcIrrList.get(regOpcIrrList.size() - 1);
        assertThat(testRegOpcIrr.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcIrr.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcIrr.getDataOperazione()).isEqualTo(UPDATED_DATA_OPERAZIONE);
        assertThat(testRegOpcIrr.getTempoImpiegato()).isEqualTo(UPDATED_TEMPO_IMPIEGATO);
        assertThat(testRegOpcIrr.getVolumeAcqua()).isEqualTo(UPDATED_VOLUME_ACQUA);
        assertThat(testRegOpcIrr.getDataInizioPeriod()).isEqualTo(UPDATED_DATA_INIZIO_PERIOD);
        assertThat(testRegOpcIrr.getDataFinePeriod()).isEqualTo(UPDATED_DATA_FINE_PERIOD);
        assertThat(testRegOpcIrr.getFrequenzaGiorni()).isEqualTo(UPDATED_FREQUENZA_GIORNI);
        assertThat(testRegOpcIrr.getIdColtura()).isEqualTo(UPDATED_ID_COLTURA);
        assertThat(testRegOpcIrr.getIdOperatore()).isEqualTo(UPDATED_ID_OPERATORE);
        assertThat(testRegOpcIrr.getIdMezzo()).isEqualTo(UPDATED_ID_MEZZO);
        assertThat(testRegOpcIrr.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcIrr.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcIrr.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcIrr.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRegOpcIrr.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcIrr() throws Exception {
        int databaseSizeBeforeUpdate = regOpcIrrRepository.findAll().size();

        // Create the RegOpcIrr

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcIrrMockMvc.perform(put("/api/reg-opc-irrs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrr)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcIrr in the database
        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcIrr() throws Exception {
        // Initialize the database
        regOpcIrrService.save(regOpcIrr);

        int databaseSizeBeforeDelete = regOpcIrrRepository.findAll().size();

        // Delete the regOpcIrr
        restRegOpcIrrMockMvc.perform(delete("/api/reg-opc-irrs/{id}", regOpcIrr.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcIrr> regOpcIrrList = regOpcIrrRepository.findAll();
        assertThat(regOpcIrrList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcIrr.class);
        RegOpcIrr regOpcIrr1 = new RegOpcIrr();
        regOpcIrr1.setId(1L);
        RegOpcIrr regOpcIrr2 = new RegOpcIrr();
        regOpcIrr2.setId(regOpcIrr1.getId());
        assertThat(regOpcIrr1).isEqualTo(regOpcIrr2);
        regOpcIrr2.setId(2L);
        assertThat(regOpcIrr1).isNotEqualTo(regOpcIrr2);
        regOpcIrr1.setId(null);
        assertThat(regOpcIrr1).isNotEqualTo(regOpcIrr2);
    }
}
