package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecMagUbi;
import com.terrambe.domain.MagUbicazione;
import com.terrambe.repository.RecMagUbiRepository;
import com.terrambe.service.RecMagUbiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecMagUbiCriteria;
import com.terrambe.service.RecMagUbiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecMagUbiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecMagUbiResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecMagUbiRepository recMagUbiRepository;

    @Autowired
    private RecMagUbiService recMagUbiService;

    @Autowired
    private RecMagUbiQueryService recMagUbiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecMagUbiMockMvc;

    private RecMagUbi recMagUbi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecMagUbiResource recMagUbiResource = new RecMagUbiResource(recMagUbiService, recMagUbiQueryService);
        this.restRecMagUbiMockMvc = MockMvcBuilders.standaloneSetup(recMagUbiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecMagUbi createEntity(EntityManager em) {
        RecMagUbi recMagUbi = new RecMagUbi()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recMagUbi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecMagUbi createUpdatedEntity(EntityManager em) {
        RecMagUbi recMagUbi = new RecMagUbi()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recMagUbi;
    }

    @BeforeEach
    public void initTest() {
        recMagUbi = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecMagUbi() throws Exception {
        int databaseSizeBeforeCreate = recMagUbiRepository.findAll().size();

        // Create the RecMagUbi
        restRecMagUbiMockMvc.perform(post("/api/rec-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recMagUbi)))
            .andExpect(status().isCreated());

        // Validate the RecMagUbi in the database
        List<RecMagUbi> recMagUbiList = recMagUbiRepository.findAll();
        assertThat(recMagUbiList).hasSize(databaseSizeBeforeCreate + 1);
        RecMagUbi testRecMagUbi = recMagUbiList.get(recMagUbiList.size() - 1);
        assertThat(testRecMagUbi.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecMagUbi.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecMagUbi.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecMagUbi.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecMagUbi.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecMagUbi.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecMagUbi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecMagUbi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecMagUbi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecMagUbi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecMagUbi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecMagUbiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recMagUbiRepository.findAll().size();

        // Create the RecMagUbi with an existing ID
        recMagUbi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecMagUbiMockMvc.perform(post("/api/rec-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recMagUbi)))
            .andExpect(status().isBadRequest());

        // Validate the RecMagUbi in the database
        List<RecMagUbi> recMagUbiList = recMagUbiRepository.findAll();
        assertThat(recMagUbiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecMagUbis() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recMagUbi.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecMagUbi() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get the recMagUbi
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis/{id}", recMagUbi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recMagUbi.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where email equals to DEFAULT_EMAIL
        defaultRecMagUbiShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recMagUbiList where email equals to UPDATED_EMAIL
        defaultRecMagUbiShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecMagUbiShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recMagUbiList where email equals to UPDATED_EMAIL
        defaultRecMagUbiShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where email is not null
        defaultRecMagUbiShouldBeFound("email.specified=true");

        // Get all the recMagUbiList where email is null
        defaultRecMagUbiShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where pec equals to DEFAULT_PEC
        defaultRecMagUbiShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recMagUbiList where pec equals to UPDATED_PEC
        defaultRecMagUbiShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecMagUbiShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recMagUbiList where pec equals to UPDATED_PEC
        defaultRecMagUbiShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where pec is not null
        defaultRecMagUbiShouldBeFound("pec.specified=true");

        // Get all the recMagUbiList where pec is null
        defaultRecMagUbiShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where telefono equals to DEFAULT_TELEFONO
        defaultRecMagUbiShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recMagUbiList where telefono equals to UPDATED_TELEFONO
        defaultRecMagUbiShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecMagUbiShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recMagUbiList where telefono equals to UPDATED_TELEFONO
        defaultRecMagUbiShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where telefono is not null
        defaultRecMagUbiShouldBeFound("telefono.specified=true");

        // Get all the recMagUbiList where telefono is null
        defaultRecMagUbiShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where fax equals to DEFAULT_FAX
        defaultRecMagUbiShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recMagUbiList where fax equals to UPDATED_FAX
        defaultRecMagUbiShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecMagUbiShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recMagUbiList where fax equals to UPDATED_FAX
        defaultRecMagUbiShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where fax is not null
        defaultRecMagUbiShouldBeFound("fax.specified=true");

        // Get all the recMagUbiList where fax is null
        defaultRecMagUbiShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell equals to DEFAULT_CELL
        defaultRecMagUbiShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recMagUbiList where cell equals to UPDATED_CELL
        defaultRecMagUbiShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecMagUbiShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recMagUbiList where cell equals to UPDATED_CELL
        defaultRecMagUbiShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell is not null
        defaultRecMagUbiShouldBeFound("cell.specified=true");

        // Get all the recMagUbiList where cell is null
        defaultRecMagUbiShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell2 equals to DEFAULT_CELL_2
        defaultRecMagUbiShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recMagUbiList where cell2 equals to UPDATED_CELL_2
        defaultRecMagUbiShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecMagUbiShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recMagUbiList where cell2 equals to UPDATED_CELL_2
        defaultRecMagUbiShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where cell2 is not null
        defaultRecMagUbiShouldBeFound("cell2.specified=true");

        // Get all the recMagUbiList where cell2 is null
        defaultRecMagUbiShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali is not null
        defaultRecMagUbiShouldBeFound("dataInizVali.specified=true");

        // Get all the recMagUbiList where dataInizVali is null
        defaultRecMagUbiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecMagUbiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recMagUbiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecMagUbiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali is not null
        defaultRecMagUbiShouldBeFound("dataFineVali.specified=true");

        // Get all the recMagUbiList where dataFineVali is null
        defaultRecMagUbiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecMagUbiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recMagUbiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecMagUbiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator is not null
        defaultRecMagUbiShouldBeFound("userIdCreator.specified=true");

        // Get all the recMagUbiList where userIdCreator is null
        defaultRecMagUbiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecMagUbiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recMagUbiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecMagUbiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod is not null
        defaultRecMagUbiShouldBeFound("userIdLastMod.specified=true");

        // Get all the recMagUbiList where userIdLastMod is null
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecMagUbisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);

        // Get all the recMagUbiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecMagUbiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recMagUbiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecMagUbiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecMagUbisByRecToMagUbiIsEqualToSomething() throws Exception {
        // Initialize the database
        recMagUbiRepository.saveAndFlush(recMagUbi);
        MagUbicazione recToMagUbi = MagUbicazioneResourceIT.createEntity(em);
        em.persist(recToMagUbi);
        em.flush();
        recMagUbi.setRecToMagUbi(recToMagUbi);
        recMagUbiRepository.saveAndFlush(recMagUbi);
        Long recToMagUbiId = recToMagUbi.getId();

        // Get all the recMagUbiList where recToMagUbi equals to recToMagUbiId
        defaultRecMagUbiShouldBeFound("recToMagUbiId.equals=" + recToMagUbiId);

        // Get all the recMagUbiList where recToMagUbi equals to recToMagUbiId + 1
        defaultRecMagUbiShouldNotBeFound("recToMagUbiId.equals=" + (recToMagUbiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecMagUbiShouldBeFound(String filter) throws Exception {
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recMagUbi.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecMagUbiShouldNotBeFound(String filter) throws Exception {
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecMagUbi() throws Exception {
        // Get the recMagUbi
        restRecMagUbiMockMvc.perform(get("/api/rec-mag-ubis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecMagUbi() throws Exception {
        // Initialize the database
        recMagUbiService.save(recMagUbi);

        int databaseSizeBeforeUpdate = recMagUbiRepository.findAll().size();

        // Update the recMagUbi
        RecMagUbi updatedRecMagUbi = recMagUbiRepository.findById(recMagUbi.getId()).get();
        // Disconnect from session so that the updates on updatedRecMagUbi are not directly saved in db
        em.detach(updatedRecMagUbi);
        updatedRecMagUbi
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecMagUbiMockMvc.perform(put("/api/rec-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecMagUbi)))
            .andExpect(status().isOk());

        // Validate the RecMagUbi in the database
        List<RecMagUbi> recMagUbiList = recMagUbiRepository.findAll();
        assertThat(recMagUbiList).hasSize(databaseSizeBeforeUpdate);
        RecMagUbi testRecMagUbi = recMagUbiList.get(recMagUbiList.size() - 1);
        assertThat(testRecMagUbi.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecMagUbi.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecMagUbi.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecMagUbi.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecMagUbi.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecMagUbi.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecMagUbi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecMagUbi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecMagUbi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecMagUbi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecMagUbi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecMagUbi() throws Exception {
        int databaseSizeBeforeUpdate = recMagUbiRepository.findAll().size();

        // Create the RecMagUbi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecMagUbiMockMvc.perform(put("/api/rec-mag-ubis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recMagUbi)))
            .andExpect(status().isBadRequest());

        // Validate the RecMagUbi in the database
        List<RecMagUbi> recMagUbiList = recMagUbiRepository.findAll();
        assertThat(recMagUbiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecMagUbi() throws Exception {
        // Initialize the database
        recMagUbiService.save(recMagUbi);

        int databaseSizeBeforeDelete = recMagUbiRepository.findAll().size();

        // Delete the recMagUbi
        restRecMagUbiMockMvc.perform(delete("/api/rec-mag-ubis/{id}", recMagUbi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecMagUbi> recMagUbiList = recMagUbiRepository.findAll();
        assertThat(recMagUbiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecMagUbi.class);
        RecMagUbi recMagUbi1 = new RecMagUbi();
        recMagUbi1.setId(1L);
        RecMagUbi recMagUbi2 = new RecMagUbi();
        recMagUbi2.setId(recMagUbi1.getId());
        assertThat(recMagUbi1).isEqualTo(recMagUbi2);
        recMagUbi2.setId(2L);
        assertThat(recMagUbi1).isNotEqualTo(recMagUbi2);
        recMagUbi1.setId(null);
        assertThat(recMagUbi1).isNotEqualTo(recMagUbi2);
    }
}
