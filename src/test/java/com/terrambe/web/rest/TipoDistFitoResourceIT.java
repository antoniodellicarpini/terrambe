package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TipoDistFito;
import com.terrambe.repository.TipoDistFitoRepository;
import com.terrambe.service.TipoDistFitoService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TipoDistFitoCriteria;
import com.terrambe.service.TipoDistFitoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TipoDistFitoResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TipoDistFitoResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private TipoDistFitoRepository tipoDistFitoRepository;

    @Autowired
    private TipoDistFitoService tipoDistFitoService;

    @Autowired
    private TipoDistFitoQueryService tipoDistFitoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoDistFitoMockMvc;

    private TipoDistFito tipoDistFito;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoDistFitoResource tipoDistFitoResource = new TipoDistFitoResource(tipoDistFitoService, tipoDistFitoQueryService);
        this.restTipoDistFitoMockMvc = MockMvcBuilders.standaloneSetup(tipoDistFitoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoDistFito createEntity(EntityManager em) {
        TipoDistFito tipoDistFito = new TipoDistFito()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return tipoDistFito;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoDistFito createUpdatedEntity(EntityManager em) {
        TipoDistFito tipoDistFito = new TipoDistFito()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return tipoDistFito;
    }

    @BeforeEach
    public void initTest() {
        tipoDistFito = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoDistFito() throws Exception {
        int databaseSizeBeforeCreate = tipoDistFitoRepository.findAll().size();

        // Create the TipoDistFito
        restTipoDistFitoMockMvc.perform(post("/api/tipo-dist-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistFito)))
            .andExpect(status().isCreated());

        // Validate the TipoDistFito in the database
        List<TipoDistFito> tipoDistFitoList = tipoDistFitoRepository.findAll();
        assertThat(tipoDistFitoList).hasSize(databaseSizeBeforeCreate + 1);
        TipoDistFito testTipoDistFito = tipoDistFitoList.get(tipoDistFitoList.size() - 1);
        assertThat(testTipoDistFito.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoDistFito.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTipoDistFito.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTipoDistFito.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTipoDistFito.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createTipoDistFitoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoDistFitoRepository.findAll().size();

        // Create the TipoDistFito with an existing ID
        tipoDistFito.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoDistFitoMockMvc.perform(post("/api/tipo-dist-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistFito)))
            .andExpect(status().isBadRequest());

        // Validate the TipoDistFito in the database
        List<TipoDistFito> tipoDistFitoList = tipoDistFitoRepository.findAll();
        assertThat(tipoDistFitoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipoDistFitos() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoDistFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getTipoDistFito() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get the tipoDistFito
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos/{id}", tipoDistFito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoDistFito.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultTipoDistFitoShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the tipoDistFitoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoDistFitoShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultTipoDistFitoShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the tipoDistFitoList where descrizione equals to UPDATED_DESCRIZIONE
        defaultTipoDistFitoShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where descrizione is not null
        defaultTipoDistFitoShouldBeFound("descrizione.specified=true");

        // Get all the tipoDistFitoList where descrizione is null
        defaultTipoDistFitoShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali is not null
        defaultTipoDistFitoShouldBeFound("dataInizVali.specified=true");

        // Get all the tipoDistFitoList where dataInizVali is null
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTipoDistFitoShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the tipoDistFitoList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTipoDistFitoShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali is not null
        defaultTipoDistFitoShouldBeFound("dataFineVali.specified=true");

        // Get all the tipoDistFitoList where dataFineVali is null
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTipoDistFitoShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the tipoDistFitoList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTipoDistFitoShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator is not null
        defaultTipoDistFitoShouldBeFound("userIdCreator.specified=true");

        // Get all the tipoDistFitoList where userIdCreator is null
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTipoDistFitoShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the tipoDistFitoList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTipoDistFitoShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod is not null
        defaultTipoDistFitoShouldBeFound("userIdLastMod.specified=true");

        // Get all the tipoDistFitoList where userIdLastMod is null
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTipoDistFitosByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tipoDistFitoRepository.saveAndFlush(tipoDistFito);

        // Get all the tipoDistFitoList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the tipoDistFitoList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTipoDistFitoShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoDistFitoShouldBeFound(String filter) throws Exception {
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoDistFito.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoDistFitoShouldNotBeFound(String filter) throws Exception {
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipoDistFito() throws Exception {
        // Get the tipoDistFito
        restTipoDistFitoMockMvc.perform(get("/api/tipo-dist-fitos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoDistFito() throws Exception {
        // Initialize the database
        tipoDistFitoService.save(tipoDistFito);

        int databaseSizeBeforeUpdate = tipoDistFitoRepository.findAll().size();

        // Update the tipoDistFito
        TipoDistFito updatedTipoDistFito = tipoDistFitoRepository.findById(tipoDistFito.getId()).get();
        // Disconnect from session so that the updates on updatedTipoDistFito are not directly saved in db
        em.detach(updatedTipoDistFito);
        updatedTipoDistFito
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restTipoDistFitoMockMvc.perform(put("/api/tipo-dist-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoDistFito)))
            .andExpect(status().isOk());

        // Validate the TipoDistFito in the database
        List<TipoDistFito> tipoDistFitoList = tipoDistFitoRepository.findAll();
        assertThat(tipoDistFitoList).hasSize(databaseSizeBeforeUpdate);
        TipoDistFito testTipoDistFito = tipoDistFitoList.get(tipoDistFitoList.size() - 1);
        assertThat(testTipoDistFito.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoDistFito.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTipoDistFito.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTipoDistFito.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTipoDistFito.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoDistFito() throws Exception {
        int databaseSizeBeforeUpdate = tipoDistFitoRepository.findAll().size();

        // Create the TipoDistFito

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoDistFitoMockMvc.perform(put("/api/tipo-dist-fitos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDistFito)))
            .andExpect(status().isBadRequest());

        // Validate the TipoDistFito in the database
        List<TipoDistFito> tipoDistFitoList = tipoDistFitoRepository.findAll();
        assertThat(tipoDistFitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipoDistFito() throws Exception {
        // Initialize the database
        tipoDistFitoService.save(tipoDistFito);

        int databaseSizeBeforeDelete = tipoDistFitoRepository.findAll().size();

        // Delete the tipoDistFito
        restTipoDistFitoMockMvc.perform(delete("/api/tipo-dist-fitos/{id}", tipoDistFito.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoDistFito> tipoDistFitoList = tipoDistFitoRepository.findAll();
        assertThat(tipoDistFitoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoDistFito.class);
        TipoDistFito tipoDistFito1 = new TipoDistFito();
        tipoDistFito1.setId(1L);
        TipoDistFito tipoDistFito2 = new TipoDistFito();
        tipoDistFito2.setId(tipoDistFito1.getId());
        assertThat(tipoDistFito1).isEqualTo(tipoDistFito2);
        tipoDistFito2.setId(2L);
        assertThat(tipoDistFito1).isNotEqualTo(tipoDistFito2);
        tipoDistFito1.setId(null);
        assertThat(tipoDistFito1).isNotEqualTo(tipoDistFito2);
    }
}
