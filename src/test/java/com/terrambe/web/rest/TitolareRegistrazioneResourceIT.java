package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.TitolareRegistrazione;
import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.TitolareRegistrazioneRepository;
import com.terrambe.service.TitolareRegistrazioneService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.TitolareRegistrazioneCriteria;
import com.terrambe.service.TitolareRegistrazioneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TitolareRegistrazioneResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class TitolareRegistrazioneResourceIT {

    private static final String DEFAULT_DITTA_PRODUTTRICE = "AAAAAAAAAA";
    private static final String UPDATED_DITTA_PRODUTTRICE = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_IMPORT = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_IMPORT = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATORE = "AAAAAAAAAA";
    private static final String UPDATED_OPERATORE = "BBBBBBBBBB";

    private static final Instant DEFAULT_TS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TS = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_TS = Instant.ofEpochMilli(-1L);

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private TitolareRegistrazioneRepository titolareRegistrazioneRepository;

    @Autowired
    private TitolareRegistrazioneService titolareRegistrazioneService;

    @Autowired
    private TitolareRegistrazioneQueryService titolareRegistrazioneQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTitolareRegistrazioneMockMvc;

    private TitolareRegistrazione titolareRegistrazione;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TitolareRegistrazioneResource titolareRegistrazioneResource = new TitolareRegistrazioneResource(titolareRegistrazioneService, titolareRegistrazioneQueryService);
        this.restTitolareRegistrazioneMockMvc = MockMvcBuilders.standaloneSetup(titolareRegistrazioneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TitolareRegistrazione createEntity(EntityManager em) {
        TitolareRegistrazione titolareRegistrazione = new TitolareRegistrazione()
            .dittaProduttrice(DEFAULT_DITTA_PRODUTTRICE)
            .tipoImport(DEFAULT_TIPO_IMPORT)
            .operatore(DEFAULT_OPERATORE)
            .ts(DEFAULT_TS)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return titolareRegistrazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TitolareRegistrazione createUpdatedEntity(EntityManager em) {
        TitolareRegistrazione titolareRegistrazione = new TitolareRegistrazione()
            .dittaProduttrice(UPDATED_DITTA_PRODUTTRICE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return titolareRegistrazione;
    }

    @BeforeEach
    public void initTest() {
        titolareRegistrazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createTitolareRegistrazione() throws Exception {
        int databaseSizeBeforeCreate = titolareRegistrazioneRepository.findAll().size();

        // Create the TitolareRegistrazione
        restTitolareRegistrazioneMockMvc.perform(post("/api/titolare-registraziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(titolareRegistrazione)))
            .andExpect(status().isCreated());

        // Validate the TitolareRegistrazione in the database
        List<TitolareRegistrazione> titolareRegistrazioneList = titolareRegistrazioneRepository.findAll();
        assertThat(titolareRegistrazioneList).hasSize(databaseSizeBeforeCreate + 1);
        TitolareRegistrazione testTitolareRegistrazione = titolareRegistrazioneList.get(titolareRegistrazioneList.size() - 1);
        assertThat(testTitolareRegistrazione.getDittaProduttrice()).isEqualTo(DEFAULT_DITTA_PRODUTTRICE);
        assertThat(testTitolareRegistrazione.getTipoImport()).isEqualTo(DEFAULT_TIPO_IMPORT);
        assertThat(testTitolareRegistrazione.getOperatore()).isEqualTo(DEFAULT_OPERATORE);
        assertThat(testTitolareRegistrazione.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testTitolareRegistrazione.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testTitolareRegistrazione.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testTitolareRegistrazione.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testTitolareRegistrazione.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testTitolareRegistrazione.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createTitolareRegistrazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = titolareRegistrazioneRepository.findAll().size();

        // Create the TitolareRegistrazione with an existing ID
        titolareRegistrazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTitolareRegistrazioneMockMvc.perform(post("/api/titolare-registraziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(titolareRegistrazione)))
            .andExpect(status().isBadRequest());

        // Validate the TitolareRegistrazione in the database
        List<TitolareRegistrazione> titolareRegistrazioneList = titolareRegistrazioneRepository.findAll();
        assertThat(titolareRegistrazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTitolareRegistraziones() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(titolareRegistrazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].dittaProduttrice").value(hasItem(DEFAULT_DITTA_PRODUTTRICE.toString())))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT.toString())))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getTitolareRegistrazione() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get the titolareRegistrazione
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones/{id}", titolareRegistrazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(titolareRegistrazione.getId().intValue()))
            .andExpect(jsonPath("$.dittaProduttrice").value(DEFAULT_DITTA_PRODUTTRICE.toString()))
            .andExpect(jsonPath("$.tipoImport").value(DEFAULT_TIPO_IMPORT.toString()))
            .andExpect(jsonPath("$.operatore").value(DEFAULT_OPERATORE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDittaProduttriceIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dittaProduttrice equals to DEFAULT_DITTA_PRODUTTRICE
        defaultTitolareRegistrazioneShouldBeFound("dittaProduttrice.equals=" + DEFAULT_DITTA_PRODUTTRICE);

        // Get all the titolareRegistrazioneList where dittaProduttrice equals to UPDATED_DITTA_PRODUTTRICE
        defaultTitolareRegistrazioneShouldNotBeFound("dittaProduttrice.equals=" + UPDATED_DITTA_PRODUTTRICE);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDittaProduttriceIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dittaProduttrice in DEFAULT_DITTA_PRODUTTRICE or UPDATED_DITTA_PRODUTTRICE
        defaultTitolareRegistrazioneShouldBeFound("dittaProduttrice.in=" + DEFAULT_DITTA_PRODUTTRICE + "," + UPDATED_DITTA_PRODUTTRICE);

        // Get all the titolareRegistrazioneList where dittaProduttrice equals to UPDATED_DITTA_PRODUTTRICE
        defaultTitolareRegistrazioneShouldNotBeFound("dittaProduttrice.in=" + UPDATED_DITTA_PRODUTTRICE);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDittaProduttriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dittaProduttrice is not null
        defaultTitolareRegistrazioneShouldBeFound("dittaProduttrice.specified=true");

        // Get all the titolareRegistrazioneList where dittaProduttrice is null
        defaultTitolareRegistrazioneShouldNotBeFound("dittaProduttrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTipoImportIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where tipoImport equals to DEFAULT_TIPO_IMPORT
        defaultTitolareRegistrazioneShouldBeFound("tipoImport.equals=" + DEFAULT_TIPO_IMPORT);

        // Get all the titolareRegistrazioneList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTitolareRegistrazioneShouldNotBeFound("tipoImport.equals=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTipoImportIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where tipoImport in DEFAULT_TIPO_IMPORT or UPDATED_TIPO_IMPORT
        defaultTitolareRegistrazioneShouldBeFound("tipoImport.in=" + DEFAULT_TIPO_IMPORT + "," + UPDATED_TIPO_IMPORT);

        // Get all the titolareRegistrazioneList where tipoImport equals to UPDATED_TIPO_IMPORT
        defaultTitolareRegistrazioneShouldNotBeFound("tipoImport.in=" + UPDATED_TIPO_IMPORT);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTipoImportIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where tipoImport is not null
        defaultTitolareRegistrazioneShouldBeFound("tipoImport.specified=true");

        // Get all the titolareRegistrazioneList where tipoImport is null
        defaultTitolareRegistrazioneShouldNotBeFound("tipoImport.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByOperatoreIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where operatore equals to DEFAULT_OPERATORE
        defaultTitolareRegistrazioneShouldBeFound("operatore.equals=" + DEFAULT_OPERATORE);

        // Get all the titolareRegistrazioneList where operatore equals to UPDATED_OPERATORE
        defaultTitolareRegistrazioneShouldNotBeFound("operatore.equals=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByOperatoreIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where operatore in DEFAULT_OPERATORE or UPDATED_OPERATORE
        defaultTitolareRegistrazioneShouldBeFound("operatore.in=" + DEFAULT_OPERATORE + "," + UPDATED_OPERATORE);

        // Get all the titolareRegistrazioneList where operatore equals to UPDATED_OPERATORE
        defaultTitolareRegistrazioneShouldNotBeFound("operatore.in=" + UPDATED_OPERATORE);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByOperatoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where operatore is not null
        defaultTitolareRegistrazioneShouldBeFound("operatore.specified=true");

        // Get all the titolareRegistrazioneList where operatore is null
        defaultTitolareRegistrazioneShouldNotBeFound("operatore.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where ts equals to DEFAULT_TS
        defaultTitolareRegistrazioneShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the titolareRegistrazioneList where ts equals to UPDATED_TS
        defaultTitolareRegistrazioneShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTsIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where ts in DEFAULT_TS or UPDATED_TS
        defaultTitolareRegistrazioneShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the titolareRegistrazioneList where ts equals to UPDATED_TS
        defaultTitolareRegistrazioneShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where ts is not null
        defaultTitolareRegistrazioneShouldBeFound("ts.specified=true");

        // Get all the titolareRegistrazioneList where ts is null
        defaultTitolareRegistrazioneShouldNotBeFound("ts.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali is not null
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.specified=true");

        // Get all the titolareRegistrazioneList where dataInizVali is null
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the titolareRegistrazioneList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali is not null
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.specified=true");

        // Get all the titolareRegistrazioneList where dataFineVali is null
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the titolareRegistrazioneList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultTitolareRegistrazioneShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator is not null
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.specified=true");

        // Get all the titolareRegistrazioneList where userIdCreator is null
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the titolareRegistrazioneList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultTitolareRegistrazioneShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod is not null
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.specified=true");

        // Get all the titolareRegistrazioneList where userIdLastMod is null
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);

        // Get all the titolareRegistrazioneList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the titolareRegistrazioneList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultTitolareRegistrazioneShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllTitolareRegistrazionesByEtichettaFitoIsEqualToSomething() throws Exception {
        // Initialize the database
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);
        EtichettaFito etichettaFito = EtichettaFitoResourceIT.createEntity(em);
        em.persist(etichettaFito);
        em.flush();
        titolareRegistrazione.addEtichettaFito(etichettaFito);
        titolareRegistrazioneRepository.saveAndFlush(titolareRegistrazione);
        Long etichettaFitoId = etichettaFito.getId();

        // Get all the titolareRegistrazioneList where etichettaFito equals to etichettaFitoId
        defaultTitolareRegistrazioneShouldBeFound("etichettaFitoId.equals=" + etichettaFitoId);

        // Get all the titolareRegistrazioneList where etichettaFito equals to etichettaFitoId + 1
        defaultTitolareRegistrazioneShouldNotBeFound("etichettaFitoId.equals=" + (etichettaFitoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTitolareRegistrazioneShouldBeFound(String filter) throws Exception {
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(titolareRegistrazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].dittaProduttrice").value(hasItem(DEFAULT_DITTA_PRODUTTRICE)))
            .andExpect(jsonPath("$.[*].tipoImport").value(hasItem(DEFAULT_TIPO_IMPORT)))
            .andExpect(jsonPath("$.[*].operatore").value(hasItem(DEFAULT_OPERATORE)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTitolareRegistrazioneShouldNotBeFound(String filter) throws Exception {
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTitolareRegistrazione() throws Exception {
        // Get the titolareRegistrazione
        restTitolareRegistrazioneMockMvc.perform(get("/api/titolare-registraziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTitolareRegistrazione() throws Exception {
        // Initialize the database
        titolareRegistrazioneService.save(titolareRegistrazione);

        int databaseSizeBeforeUpdate = titolareRegistrazioneRepository.findAll().size();

        // Update the titolareRegistrazione
        TitolareRegistrazione updatedTitolareRegistrazione = titolareRegistrazioneRepository.findById(titolareRegistrazione.getId()).get();
        // Disconnect from session so that the updates on updatedTitolareRegistrazione are not directly saved in db
        em.detach(updatedTitolareRegistrazione);
        updatedTitolareRegistrazione
            .dittaProduttrice(UPDATED_DITTA_PRODUTTRICE)
            .tipoImport(UPDATED_TIPO_IMPORT)
            .operatore(UPDATED_OPERATORE)
            .ts(UPDATED_TS)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restTitolareRegistrazioneMockMvc.perform(put("/api/titolare-registraziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTitolareRegistrazione)))
            .andExpect(status().isOk());

        // Validate the TitolareRegistrazione in the database
        List<TitolareRegistrazione> titolareRegistrazioneList = titolareRegistrazioneRepository.findAll();
        assertThat(titolareRegistrazioneList).hasSize(databaseSizeBeforeUpdate);
        TitolareRegistrazione testTitolareRegistrazione = titolareRegistrazioneList.get(titolareRegistrazioneList.size() - 1);
        assertThat(testTitolareRegistrazione.getDittaProduttrice()).isEqualTo(UPDATED_DITTA_PRODUTTRICE);
        assertThat(testTitolareRegistrazione.getTipoImport()).isEqualTo(UPDATED_TIPO_IMPORT);
        assertThat(testTitolareRegistrazione.getOperatore()).isEqualTo(UPDATED_OPERATORE);
        assertThat(testTitolareRegistrazione.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testTitolareRegistrazione.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testTitolareRegistrazione.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testTitolareRegistrazione.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testTitolareRegistrazione.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testTitolareRegistrazione.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingTitolareRegistrazione() throws Exception {
        int databaseSizeBeforeUpdate = titolareRegistrazioneRepository.findAll().size();

        // Create the TitolareRegistrazione

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTitolareRegistrazioneMockMvc.perform(put("/api/titolare-registraziones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(titolareRegistrazione)))
            .andExpect(status().isBadRequest());

        // Validate the TitolareRegistrazione in the database
        List<TitolareRegistrazione> titolareRegistrazioneList = titolareRegistrazioneRepository.findAll();
        assertThat(titolareRegistrazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTitolareRegistrazione() throws Exception {
        // Initialize the database
        titolareRegistrazioneService.save(titolareRegistrazione);

        int databaseSizeBeforeDelete = titolareRegistrazioneRepository.findAll().size();

        // Delete the titolareRegistrazione
        restTitolareRegistrazioneMockMvc.perform(delete("/api/titolare-registraziones/{id}", titolareRegistrazione.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TitolareRegistrazione> titolareRegistrazioneList = titolareRegistrazioneRepository.findAll();
        assertThat(titolareRegistrazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TitolareRegistrazione.class);
        TitolareRegistrazione titolareRegistrazione1 = new TitolareRegistrazione();
        titolareRegistrazione1.setId(1L);
        TitolareRegistrazione titolareRegistrazione2 = new TitolareRegistrazione();
        titolareRegistrazione2.setId(titolareRegistrazione1.getId());
        assertThat(titolareRegistrazione1).isEqualTo(titolareRegistrazione2);
        titolareRegistrazione2.setId(2L);
        assertThat(titolareRegistrazione1).isNotEqualTo(titolareRegistrazione2);
        titolareRegistrazione1.setId(null);
        assertThat(titolareRegistrazione1).isNotEqualTo(titolareRegistrazione2);
    }
}
