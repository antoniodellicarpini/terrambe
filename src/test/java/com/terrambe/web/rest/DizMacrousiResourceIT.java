package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.DizMacrousi;
import com.terrambe.repository.DizMacrousiRepository;
import com.terrambe.service.DizMacrousiService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.DizMacrousiCriteria;
import com.terrambe.service.DizMacrousiQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DizMacrousiResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class DizMacrousiResourceIT {

    private static final String DEFAULT_CODICE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private DizMacrousiRepository dizMacrousiRepository;

    @Autowired
    private DizMacrousiService dizMacrousiService;

    @Autowired
    private DizMacrousiQueryService dizMacrousiQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDizMacrousiMockMvc;

    private DizMacrousi dizMacrousi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DizMacrousiResource dizMacrousiResource = new DizMacrousiResource(dizMacrousiService, dizMacrousiQueryService);
        this.restDizMacrousiMockMvc = MockMvcBuilders.standaloneSetup(dizMacrousiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizMacrousi createEntity(EntityManager em) {
        DizMacrousi dizMacrousi = new DizMacrousi()
            .codice(DEFAULT_CODICE)
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return dizMacrousi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DizMacrousi createUpdatedEntity(EntityManager em) {
        DizMacrousi dizMacrousi = new DizMacrousi()
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return dizMacrousi;
    }

    @BeforeEach
    public void initTest() {
        dizMacrousi = createEntity(em);
    }

    @Test
    @Transactional
    public void createDizMacrousi() throws Exception {
        int databaseSizeBeforeCreate = dizMacrousiRepository.findAll().size();

        // Create the DizMacrousi
        restDizMacrousiMockMvc.perform(post("/api/diz-macrousis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizMacrousi)))
            .andExpect(status().isCreated());

        // Validate the DizMacrousi in the database
        List<DizMacrousi> dizMacrousiList = dizMacrousiRepository.findAll();
        assertThat(dizMacrousiList).hasSize(databaseSizeBeforeCreate + 1);
        DizMacrousi testDizMacrousi = dizMacrousiList.get(dizMacrousiList.size() - 1);
        assertThat(testDizMacrousi.getCodice()).isEqualTo(DEFAULT_CODICE);
        assertThat(testDizMacrousi.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testDizMacrousi.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testDizMacrousi.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testDizMacrousi.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testDizMacrousi.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testDizMacrousi.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createDizMacrousiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dizMacrousiRepository.findAll().size();

        // Create the DizMacrousi with an existing ID
        dizMacrousi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDizMacrousiMockMvc.perform(post("/api/diz-macrousis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizMacrousi)))
            .andExpect(status().isBadRequest());

        // Validate the DizMacrousi in the database
        List<DizMacrousi> dizMacrousiList = dizMacrousiRepository.findAll();
        assertThat(dizMacrousiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDizMacrousis() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizMacrousi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getDizMacrousi() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get the dizMacrousi
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis/{id}", dizMacrousi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dizMacrousi.getId().intValue()))
            .andExpect(jsonPath("$.codice").value(DEFAULT_CODICE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByCodiceIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where codice equals to DEFAULT_CODICE
        defaultDizMacrousiShouldBeFound("codice.equals=" + DEFAULT_CODICE);

        // Get all the dizMacrousiList where codice equals to UPDATED_CODICE
        defaultDizMacrousiShouldNotBeFound("codice.equals=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByCodiceIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where codice in DEFAULT_CODICE or UPDATED_CODICE
        defaultDizMacrousiShouldBeFound("codice.in=" + DEFAULT_CODICE + "," + UPDATED_CODICE);

        // Get all the dizMacrousiList where codice equals to UPDATED_CODICE
        defaultDizMacrousiShouldNotBeFound("codice.in=" + UPDATED_CODICE);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByCodiceIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where codice is not null
        defaultDizMacrousiShouldBeFound("codice.specified=true");

        // Get all the dizMacrousiList where codice is null
        defaultDizMacrousiShouldNotBeFound("codice.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultDizMacrousiShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the dizMacrousiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizMacrousiShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultDizMacrousiShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the dizMacrousiList where descrizione equals to UPDATED_DESCRIZIONE
        defaultDizMacrousiShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where descrizione is not null
        defaultDizMacrousiShouldBeFound("descrizione.specified=true");

        // Get all the dizMacrousiList where descrizione is null
        defaultDizMacrousiShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali is not null
        defaultDizMacrousiShouldBeFound("dataInizVali.specified=true");

        // Get all the dizMacrousiList where dataInizVali is null
        defaultDizMacrousiShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultDizMacrousiShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the dizMacrousiList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultDizMacrousiShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali is not null
        defaultDizMacrousiShouldBeFound("dataFineVali.specified=true");

        // Get all the dizMacrousiList where dataFineVali is null
        defaultDizMacrousiShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultDizMacrousiShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the dizMacrousiList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultDizMacrousiShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator is not null
        defaultDizMacrousiShouldBeFound("userIdCreator.specified=true");

        // Get all the dizMacrousiList where userIdCreator is null
        defaultDizMacrousiShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultDizMacrousiShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the dizMacrousiList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultDizMacrousiShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod is not null
        defaultDizMacrousiShouldBeFound("userIdLastMod.specified=true");

        // Get all the dizMacrousiList where userIdLastMod is null
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllDizMacrousisByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dizMacrousiRepository.saveAndFlush(dizMacrousi);

        // Get all the dizMacrousiList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultDizMacrousiShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the dizMacrousiList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultDizMacrousiShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDizMacrousiShouldBeFound(String filter) throws Exception {
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dizMacrousi.getId().intValue())))
            .andExpect(jsonPath("$.[*].codice").value(hasItem(DEFAULT_CODICE)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDizMacrousiShouldNotBeFound(String filter) throws Exception {
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDizMacrousi() throws Exception {
        // Get the dizMacrousi
        restDizMacrousiMockMvc.perform(get("/api/diz-macrousis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDizMacrousi() throws Exception {
        // Initialize the database
        dizMacrousiService.save(dizMacrousi);

        int databaseSizeBeforeUpdate = dizMacrousiRepository.findAll().size();

        // Update the dizMacrousi
        DizMacrousi updatedDizMacrousi = dizMacrousiRepository.findById(dizMacrousi.getId()).get();
        // Disconnect from session so that the updates on updatedDizMacrousi are not directly saved in db
        em.detach(updatedDizMacrousi);
        updatedDizMacrousi
            .codice(UPDATED_CODICE)
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restDizMacrousiMockMvc.perform(put("/api/diz-macrousis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDizMacrousi)))
            .andExpect(status().isOk());

        // Validate the DizMacrousi in the database
        List<DizMacrousi> dizMacrousiList = dizMacrousiRepository.findAll();
        assertThat(dizMacrousiList).hasSize(databaseSizeBeforeUpdate);
        DizMacrousi testDizMacrousi = dizMacrousiList.get(dizMacrousiList.size() - 1);
        assertThat(testDizMacrousi.getCodice()).isEqualTo(UPDATED_CODICE);
        assertThat(testDizMacrousi.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testDizMacrousi.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testDizMacrousi.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testDizMacrousi.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testDizMacrousi.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testDizMacrousi.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingDizMacrousi() throws Exception {
        int databaseSizeBeforeUpdate = dizMacrousiRepository.findAll().size();

        // Create the DizMacrousi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDizMacrousiMockMvc.perform(put("/api/diz-macrousis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dizMacrousi)))
            .andExpect(status().isBadRequest());

        // Validate the DizMacrousi in the database
        List<DizMacrousi> dizMacrousiList = dizMacrousiRepository.findAll();
        assertThat(dizMacrousiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDizMacrousi() throws Exception {
        // Initialize the database
        dizMacrousiService.save(dizMacrousi);

        int databaseSizeBeforeDelete = dizMacrousiRepository.findAll().size();

        // Delete the dizMacrousi
        restDizMacrousiMockMvc.perform(delete("/api/diz-macrousis/{id}", dizMacrousi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DizMacrousi> dizMacrousiList = dizMacrousiRepository.findAll();
        assertThat(dizMacrousiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DizMacrousi.class);
        DizMacrousi dizMacrousi1 = new DizMacrousi();
        dizMacrousi1.setId(1L);
        DizMacrousi dizMacrousi2 = new DizMacrousi();
        dizMacrousi2.setId(dizMacrousi1.getId());
        assertThat(dizMacrousi1).isEqualTo(dizMacrousi2);
        dizMacrousi2.setId(2L);
        assertThat(dizMacrousi1).isNotEqualTo(dizMacrousi2);
        dizMacrousi1.setId(null);
        assertThat(dizMacrousi1).isNotEqualTo(dizMacrousi2);
    }
}
