package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RegOpcIrrAppz;
import com.terrambe.domain.RegOpcIrr;
import com.terrambe.repository.RegOpcIrrAppzRepository;
import com.terrambe.service.RegOpcIrrAppzService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RegOpcIrrAppzCriteria;
import com.terrambe.service.RegOpcIrrAppzQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegOpcIrrAppzResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RegOpcIrrAppzResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final Long DEFAULT_ID_UNITA_PROD = 1L;
    private static final Long UPDATED_ID_UNITA_PROD = 2L;
    private static final Long SMALLER_ID_UNITA_PROD = 1L - 1L;

    private static final Float DEFAULT_PERCENT_IRRIGATA = 1F;
    private static final Float UPDATED_PERCENT_IRRIGATA = 2F;
    private static final Float SMALLER_PERCENT_IRRIGATA = 1F - 1F;

    private static final Long DEFAULT_ID_APPEZZAMENTO = 1L;
    private static final Long UPDATED_ID_APPEZZAMENTO = 2L;
    private static final Long SMALLER_ID_APPEZZAMENTO = 1L - 1L;

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    @Autowired
    private RegOpcIrrAppzRepository regOpcIrrAppzRepository;

    @Autowired
    private RegOpcIrrAppzService regOpcIrrAppzService;

    @Autowired
    private RegOpcIrrAppzQueryService regOpcIrrAppzQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegOpcIrrAppzMockMvc;

    private RegOpcIrrAppz regOpcIrrAppz;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegOpcIrrAppzResource regOpcIrrAppzResource = new RegOpcIrrAppzResource(regOpcIrrAppzService, regOpcIrrAppzQueryService);
        this.restRegOpcIrrAppzMockMvc = MockMvcBuilders.standaloneSetup(regOpcIrrAppzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcIrrAppz createEntity(EntityManager em) {
        RegOpcIrrAppz regOpcIrrAppz = new RegOpcIrrAppz()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .idUnitaProd(DEFAULT_ID_UNITA_PROD)
            .percentIrrigata(DEFAULT_PERCENT_IRRIGATA)
            .idAppezzamento(DEFAULT_ID_APPEZZAMENTO)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD);
        return regOpcIrrAppz;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegOpcIrrAppz createUpdatedEntity(EntityManager em) {
        RegOpcIrrAppz regOpcIrrAppz = new RegOpcIrrAppz()
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentIrrigata(UPDATED_PERCENT_IRRIGATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);
        return regOpcIrrAppz;
    }

    @BeforeEach
    public void initTest() {
        regOpcIrrAppz = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegOpcIrrAppz() throws Exception {
        int databaseSizeBeforeCreate = regOpcIrrAppzRepository.findAll().size();

        // Create the RegOpcIrrAppz
        restRegOpcIrrAppzMockMvc.perform(post("/api/reg-opc-irr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrrAppz)))
            .andExpect(status().isCreated());

        // Validate the RegOpcIrrAppz in the database
        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeCreate + 1);
        RegOpcIrrAppz testRegOpcIrrAppz = regOpcIrrAppzList.get(regOpcIrrAppzList.size() - 1);
        assertThat(testRegOpcIrrAppz.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testRegOpcIrrAppz.getIdUnitaProd()).isEqualTo(DEFAULT_ID_UNITA_PROD);
        assertThat(testRegOpcIrrAppz.getPercentIrrigata()).isEqualTo(DEFAULT_PERCENT_IRRIGATA);
        assertThat(testRegOpcIrrAppz.getIdAppezzamento()).isEqualTo(DEFAULT_ID_APPEZZAMENTO);
        assertThat(testRegOpcIrrAppz.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRegOpcIrrAppz.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRegOpcIrrAppz.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRegOpcIrrAppz.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void createRegOpcIrrAppzWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regOpcIrrAppzRepository.findAll().size();

        // Create the RegOpcIrrAppz with an existing ID
        regOpcIrrAppz.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegOpcIrrAppzMockMvc.perform(post("/api/reg-opc-irr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrrAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcIrrAppz in the database
        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regOpcIrrAppzRepository.findAll().size();
        // set the field null
        regOpcIrrAppz.setIdAzienda(null);

        // Create the RegOpcIrrAppz, which fails.

        restRegOpcIrrAppzMockMvc.perform(post("/api/reg-opc-irr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrrAppz)))
            .andExpect(status().isBadRequest());

        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzs() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcIrrAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentIrrigata").value(hasItem(DEFAULT_PERCENT_IRRIGATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));
    }
    
    @Test
    @Transactional
    public void getRegOpcIrrAppz() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get the regOpcIrrAppz
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs/{id}", regOpcIrrAppz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regOpcIrrAppz.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.idUnitaProd").value(DEFAULT_ID_UNITA_PROD.intValue()))
            .andExpect(jsonPath("$.percentIrrigata").value(DEFAULT_PERCENT_IRRIGATA.doubleValue()))
            .andExpect(jsonPath("$.idAppezzamento").value(DEFAULT_ID_APPEZZAMENTO.intValue()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()));
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda is not null
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.specified=true");

        // Get all the regOpcIrrAppzList where idAzienda is null
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultRegOpcIrrAppzShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the regOpcIrrAppzList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultRegOpcIrrAppzShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd equals to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.equals=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.equals=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd in DEFAULT_ID_UNITA_PROD or UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.in=" + DEFAULT_ID_UNITA_PROD + "," + UPDATED_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd equals to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.in=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd is not null
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.specified=true");

        // Get all the regOpcIrrAppzList where idUnitaProd is null
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd is greater than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.greaterThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd is greater than or equal to UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.greaterThanOrEqual=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd is less than or equal to DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.lessThanOrEqual=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd is less than or equal to SMALLER_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.lessThanOrEqual=" + SMALLER_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd is less than DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.lessThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd is less than UPDATED_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.lessThan=" + UPDATED_ID_UNITA_PROD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdUnitaProdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idUnitaProd is greater than DEFAULT_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldNotBeFound("idUnitaProd.greaterThan=" + DEFAULT_ID_UNITA_PROD);

        // Get all the regOpcIrrAppzList where idUnitaProd is greater than SMALLER_ID_UNITA_PROD
        defaultRegOpcIrrAppzShouldBeFound("idUnitaProd.greaterThan=" + SMALLER_ID_UNITA_PROD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata equals to DEFAULT_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.equals=" + DEFAULT_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata equals to UPDATED_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.equals=" + UPDATED_PERCENT_IRRIGATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata in DEFAULT_PERCENT_IRRIGATA or UPDATED_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.in=" + DEFAULT_PERCENT_IRRIGATA + "," + UPDATED_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata equals to UPDATED_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.in=" + UPDATED_PERCENT_IRRIGATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata is not null
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.specified=true");

        // Get all the regOpcIrrAppzList where percentIrrigata is null
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata is greater than or equal to DEFAULT_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.greaterThanOrEqual=" + DEFAULT_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata is greater than or equal to UPDATED_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.greaterThanOrEqual=" + UPDATED_PERCENT_IRRIGATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata is less than or equal to DEFAULT_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.lessThanOrEqual=" + DEFAULT_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata is less than or equal to SMALLER_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.lessThanOrEqual=" + SMALLER_PERCENT_IRRIGATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata is less than DEFAULT_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.lessThan=" + DEFAULT_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata is less than UPDATED_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.lessThan=" + UPDATED_PERCENT_IRRIGATA);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByPercentIrrigataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where percentIrrigata is greater than DEFAULT_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldNotBeFound("percentIrrigata.greaterThan=" + DEFAULT_PERCENT_IRRIGATA);

        // Get all the regOpcIrrAppzList where percentIrrigata is greater than SMALLER_PERCENT_IRRIGATA
        defaultRegOpcIrrAppzShouldBeFound("percentIrrigata.greaterThan=" + SMALLER_PERCENT_IRRIGATA);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento equals to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.equals=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.equals=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento in DEFAULT_ID_APPEZZAMENTO or UPDATED_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.in=" + DEFAULT_ID_APPEZZAMENTO + "," + UPDATED_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento equals to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.in=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento is not null
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.specified=true");

        // Get all the regOpcIrrAppzList where idAppezzamento is null
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento is greater than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.greaterThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento is greater than or equal to UPDATED_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.greaterThanOrEqual=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento is less than or equal to DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.lessThanOrEqual=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento is less than or equal to SMALLER_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.lessThanOrEqual=" + SMALLER_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento is less than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.lessThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento is less than UPDATED_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.lessThan=" + UPDATED_ID_APPEZZAMENTO);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByIdAppezzamentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where idAppezzamento is greater than DEFAULT_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldNotBeFound("idAppezzamento.greaterThan=" + DEFAULT_ID_APPEZZAMENTO);

        // Get all the regOpcIrrAppzList where idAppezzamento is greater than SMALLER_ID_APPEZZAMENTO
        defaultRegOpcIrrAppzShouldBeFound("idAppezzamento.greaterThan=" + SMALLER_ID_APPEZZAMENTO);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali is not null
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.specified=true");

        // Get all the regOpcIrrAppzList where dataInizVali is null
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the regOpcIrrAppzList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali is not null
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.specified=true");

        // Get all the regOpcIrrAppzList where dataFineVali is null
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the regOpcIrrAppzList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRegOpcIrrAppzShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator is not null
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.specified=true");

        // Get all the regOpcIrrAppzList where userIdCreator is null
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the regOpcIrrAppzList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRegOpcIrrAppzShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod is not null
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.specified=true");

        // Get all the regOpcIrrAppzList where userIdLastMod is null
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);

        // Get all the regOpcIrrAppzList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the regOpcIrrAppzList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRegOpcIrrAppzShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRegOpcIrrAppzsByAppzToOpcIrrIsEqualToSomething() throws Exception {
        // Initialize the database
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);
        RegOpcIrr appzToOpcIrr = RegOpcIrrResourceIT.createEntity(em);
        em.persist(appzToOpcIrr);
        em.flush();
        regOpcIrrAppz.setAppzToOpcIrr(appzToOpcIrr);
        regOpcIrrAppzRepository.saveAndFlush(regOpcIrrAppz);
        Long appzToOpcIrrId = appzToOpcIrr.getId();

        // Get all the regOpcIrrAppzList where appzToOpcIrr equals to appzToOpcIrrId
        defaultRegOpcIrrAppzShouldBeFound("appzToOpcIrrId.equals=" + appzToOpcIrrId);

        // Get all the regOpcIrrAppzList where appzToOpcIrr equals to appzToOpcIrrId + 1
        defaultRegOpcIrrAppzShouldNotBeFound("appzToOpcIrrId.equals=" + (appzToOpcIrrId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRegOpcIrrAppzShouldBeFound(String filter) throws Exception {
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regOpcIrrAppz.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].idUnitaProd").value(hasItem(DEFAULT_ID_UNITA_PROD.intValue())))
            .andExpect(jsonPath("$.[*].percentIrrigata").value(hasItem(DEFAULT_PERCENT_IRRIGATA.doubleValue())))
            .andExpect(jsonPath("$.[*].idAppezzamento").value(hasItem(DEFAULT_ID_APPEZZAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())));

        // Check, that the count call also returns 1
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRegOpcIrrAppzShouldNotBeFound(String filter) throws Exception {
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRegOpcIrrAppz() throws Exception {
        // Get the regOpcIrrAppz
        restRegOpcIrrAppzMockMvc.perform(get("/api/reg-opc-irr-appzs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegOpcIrrAppz() throws Exception {
        // Initialize the database
        regOpcIrrAppzService.save(regOpcIrrAppz);

        int databaseSizeBeforeUpdate = regOpcIrrAppzRepository.findAll().size();

        // Update the regOpcIrrAppz
        RegOpcIrrAppz updatedRegOpcIrrAppz = regOpcIrrAppzRepository.findById(regOpcIrrAppz.getId()).get();
        // Disconnect from session so that the updates on updatedRegOpcIrrAppz are not directly saved in db
        em.detach(updatedRegOpcIrrAppz);
        updatedRegOpcIrrAppz
            .idAzienda(UPDATED_ID_AZIENDA)
            .idUnitaProd(UPDATED_ID_UNITA_PROD)
            .percentIrrigata(UPDATED_PERCENT_IRRIGATA)
            .idAppezzamento(UPDATED_ID_APPEZZAMENTO)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD);

        restRegOpcIrrAppzMockMvc.perform(put("/api/reg-opc-irr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegOpcIrrAppz)))
            .andExpect(status().isOk());

        // Validate the RegOpcIrrAppz in the database
        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeUpdate);
        RegOpcIrrAppz testRegOpcIrrAppz = regOpcIrrAppzList.get(regOpcIrrAppzList.size() - 1);
        assertThat(testRegOpcIrrAppz.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testRegOpcIrrAppz.getIdUnitaProd()).isEqualTo(UPDATED_ID_UNITA_PROD);
        assertThat(testRegOpcIrrAppz.getPercentIrrigata()).isEqualTo(UPDATED_PERCENT_IRRIGATA);
        assertThat(testRegOpcIrrAppz.getIdAppezzamento()).isEqualTo(UPDATED_ID_APPEZZAMENTO);
        assertThat(testRegOpcIrrAppz.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRegOpcIrrAppz.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRegOpcIrrAppz.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRegOpcIrrAppz.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegOpcIrrAppz() throws Exception {
        int databaseSizeBeforeUpdate = regOpcIrrAppzRepository.findAll().size();

        // Create the RegOpcIrrAppz

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegOpcIrrAppzMockMvc.perform(put("/api/reg-opc-irr-appzs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regOpcIrrAppz)))
            .andExpect(status().isBadRequest());

        // Validate the RegOpcIrrAppz in the database
        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegOpcIrrAppz() throws Exception {
        // Initialize the database
        regOpcIrrAppzService.save(regOpcIrrAppz);

        int databaseSizeBeforeDelete = regOpcIrrAppzRepository.findAll().size();

        // Delete the regOpcIrrAppz
        restRegOpcIrrAppzMockMvc.perform(delete("/api/reg-opc-irr-appzs/{id}", regOpcIrrAppz.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegOpcIrrAppz> regOpcIrrAppzList = regOpcIrrAppzRepository.findAll();
        assertThat(regOpcIrrAppzList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegOpcIrrAppz.class);
        RegOpcIrrAppz regOpcIrrAppz1 = new RegOpcIrrAppz();
        regOpcIrrAppz1.setId(1L);
        RegOpcIrrAppz regOpcIrrAppz2 = new RegOpcIrrAppz();
        regOpcIrrAppz2.setId(regOpcIrrAppz1.getId());
        assertThat(regOpcIrrAppz1).isEqualTo(regOpcIrrAppz2);
        regOpcIrrAppz2.setId(2L);
        assertThat(regOpcIrrAppz1).isNotEqualTo(regOpcIrrAppz2);
        regOpcIrrAppz1.setId(null);
        assertThat(regOpcIrrAppz1).isNotEqualTo(regOpcIrrAppz2);
    }
}
