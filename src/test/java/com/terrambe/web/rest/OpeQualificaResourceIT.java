package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.OpeQualifica;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.OpeQualificaRepository;
import com.terrambe.service.OpeQualificaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.OpeQualificaCriteria;
import com.terrambe.service.OpeQualificaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OpeQualificaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class OpeQualificaResourceIT {

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private OpeQualificaRepository opeQualificaRepository;

    @Autowired
    private OpeQualificaService opeQualificaService;

    @Autowired
    private OpeQualificaQueryService opeQualificaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOpeQualificaMockMvc;

    private OpeQualifica opeQualifica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OpeQualificaResource opeQualificaResource = new OpeQualificaResource(opeQualificaService, opeQualificaQueryService);
        this.restOpeQualificaMockMvc = MockMvcBuilders.standaloneSetup(opeQualificaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpeQualifica createEntity(EntityManager em) {
        OpeQualifica opeQualifica = new OpeQualifica()
            .descrizione(DEFAULT_DESCRIZIONE)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return opeQualifica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpeQualifica createUpdatedEntity(EntityManager em) {
        OpeQualifica opeQualifica = new OpeQualifica()
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return opeQualifica;
    }

    @BeforeEach
    public void initTest() {
        opeQualifica = createEntity(em);
    }

    @Test
    @Transactional
    public void createOpeQualifica() throws Exception {
        int databaseSizeBeforeCreate = opeQualificaRepository.findAll().size();

        // Create the OpeQualifica
        restOpeQualificaMockMvc.perform(post("/api/ope-qualificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeQualifica)))
            .andExpect(status().isCreated());

        // Validate the OpeQualifica in the database
        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeCreate + 1);
        OpeQualifica testOpeQualifica = opeQualificaList.get(opeQualificaList.size() - 1);
        assertThat(testOpeQualifica.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testOpeQualifica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testOpeQualifica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testOpeQualifica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testOpeQualifica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testOpeQualifica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createOpeQualificaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = opeQualificaRepository.findAll().size();

        // Create the OpeQualifica with an existing ID
        opeQualifica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOpeQualificaMockMvc.perform(post("/api/ope-qualificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeQualifica)))
            .andExpect(status().isBadRequest());

        // Validate the OpeQualifica in the database
        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescrizioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = opeQualificaRepository.findAll().size();
        // set the field null
        opeQualifica.setDescrizione(null);

        // Create the OpeQualifica, which fails.

        restOpeQualificaMockMvc.perform(post("/api/ope-qualificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeQualifica)))
            .andExpect(status().isBadRequest());

        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOpeQualificas() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opeQualifica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getOpeQualifica() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get the opeQualifica
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas/{id}", opeQualifica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(opeQualifica.getId().intValue()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDescrizioneIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where descrizione equals to DEFAULT_DESCRIZIONE
        defaultOpeQualificaShouldBeFound("descrizione.equals=" + DEFAULT_DESCRIZIONE);

        // Get all the opeQualificaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultOpeQualificaShouldNotBeFound("descrizione.equals=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDescrizioneIsInShouldWork() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where descrizione in DEFAULT_DESCRIZIONE or UPDATED_DESCRIZIONE
        defaultOpeQualificaShouldBeFound("descrizione.in=" + DEFAULT_DESCRIZIONE + "," + UPDATED_DESCRIZIONE);

        // Get all the opeQualificaList where descrizione equals to UPDATED_DESCRIZIONE
        defaultOpeQualificaShouldNotBeFound("descrizione.in=" + UPDATED_DESCRIZIONE);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDescrizioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where descrizione is not null
        defaultOpeQualificaShouldBeFound("descrizione.specified=true");

        // Get all the opeQualificaList where descrizione is null
        defaultOpeQualificaShouldNotBeFound("descrizione.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali is not null
        defaultOpeQualificaShouldBeFound("dataInizVali.specified=true");

        // Get all the opeQualificaList where dataInizVali is null
        defaultOpeQualificaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultOpeQualificaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the opeQualificaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultOpeQualificaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali is not null
        defaultOpeQualificaShouldBeFound("dataFineVali.specified=true");

        // Get all the opeQualificaList where dataFineVali is null
        defaultOpeQualificaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultOpeQualificaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the opeQualificaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultOpeQualificaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator is not null
        defaultOpeQualificaShouldBeFound("userIdCreator.specified=true");

        // Get all the opeQualificaList where userIdCreator is null
        defaultOpeQualificaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultOpeQualificaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the opeQualificaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultOpeQualificaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod is not null
        defaultOpeQualificaShouldBeFound("userIdLastMod.specified=true");

        // Get all the opeQualificaList where userIdLastMod is null
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllOpeQualificasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);

        // Get all the opeQualificaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultOpeQualificaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the opeQualificaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultOpeQualificaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllOpeQualificasByOpeQualToOpeAnagIsEqualToSomething() throws Exception {
        // Initialize the database
        opeQualificaRepository.saveAndFlush(opeQualifica);
        OpeAnagrafica opeQualToOpeAnag = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(opeQualToOpeAnag);
        em.flush();
        opeQualifica.addOpeQualToOpeAnag(opeQualToOpeAnag);
        opeQualificaRepository.saveAndFlush(opeQualifica);
        Long opeQualToOpeAnagId = opeQualToOpeAnag.getId();

        // Get all the opeQualificaList where opeQualToOpeAnag equals to opeQualToOpeAnagId
        defaultOpeQualificaShouldBeFound("opeQualToOpeAnagId.equals=" + opeQualToOpeAnagId);

        // Get all the opeQualificaList where opeQualToOpeAnag equals to opeQualToOpeAnagId + 1
        defaultOpeQualificaShouldNotBeFound("opeQualToOpeAnagId.equals=" + (opeQualToOpeAnagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOpeQualificaShouldBeFound(String filter) throws Exception {
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(opeQualifica.getId().intValue())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOpeQualificaShouldNotBeFound(String filter) throws Exception {
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOpeQualifica() throws Exception {
        // Get the opeQualifica
        restOpeQualificaMockMvc.perform(get("/api/ope-qualificas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOpeQualifica() throws Exception {
        // Initialize the database
        opeQualificaService.save(opeQualifica);

        int databaseSizeBeforeUpdate = opeQualificaRepository.findAll().size();

        // Update the opeQualifica
        OpeQualifica updatedOpeQualifica = opeQualificaRepository.findById(opeQualifica.getId()).get();
        // Disconnect from session so that the updates on updatedOpeQualifica are not directly saved in db
        em.detach(updatedOpeQualifica);
        updatedOpeQualifica
            .descrizione(UPDATED_DESCRIZIONE)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restOpeQualificaMockMvc.perform(put("/api/ope-qualificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOpeQualifica)))
            .andExpect(status().isOk());

        // Validate the OpeQualifica in the database
        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeUpdate);
        OpeQualifica testOpeQualifica = opeQualificaList.get(opeQualificaList.size() - 1);
        assertThat(testOpeQualifica.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testOpeQualifica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testOpeQualifica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testOpeQualifica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testOpeQualifica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testOpeQualifica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingOpeQualifica() throws Exception {
        int databaseSizeBeforeUpdate = opeQualificaRepository.findAll().size();

        // Create the OpeQualifica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOpeQualificaMockMvc.perform(put("/api/ope-qualificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(opeQualifica)))
            .andExpect(status().isBadRequest());

        // Validate the OpeQualifica in the database
        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOpeQualifica() throws Exception {
        // Initialize the database
        opeQualificaService.save(opeQualifica);

        int databaseSizeBeforeDelete = opeQualificaRepository.findAll().size();

        // Delete the opeQualifica
        restOpeQualificaMockMvc.perform(delete("/api/ope-qualificas/{id}", opeQualifica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OpeQualifica> opeQualificaList = opeQualificaRepository.findAll();
        assertThat(opeQualificaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpeQualifica.class);
        OpeQualifica opeQualifica1 = new OpeQualifica();
        opeQualifica1.setId(1L);
        OpeQualifica opeQualifica2 = new OpeQualifica();
        opeQualifica2.setId(opeQualifica1.getId());
        assertThat(opeQualifica1).isEqualTo(opeQualifica2);
        opeQualifica2.setId(2L);
        assertThat(opeQualifica1).isNotEqualTo(opeQualifica2);
        opeQualifica1.setId(null);
        assertThat(opeQualifica1).isNotEqualTo(opeQualifica2);
    }
}
