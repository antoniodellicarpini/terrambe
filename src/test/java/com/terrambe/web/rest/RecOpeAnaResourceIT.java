package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.RecOpeAna;
import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.RecOpeAnaRepository;
import com.terrambe.service.RecOpeAnaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.RecOpeAnaCriteria;
import com.terrambe.service.RecOpeAnaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecOpeAnaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class RecOpeAnaResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PEC = "AAAAAAAAAA";
    private static final String UPDATED_PEC = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_CELL = "AAAAAAAAAA";
    private static final String UPDATED_CELL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_2 = "AAAAAAAAAA";
    private static final String UPDATED_CELL_2 = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private RecOpeAnaRepository recOpeAnaRepository;

    @Autowired
    private RecOpeAnaService recOpeAnaService;

    @Autowired
    private RecOpeAnaQueryService recOpeAnaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecOpeAnaMockMvc;

    private RecOpeAna recOpeAna;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecOpeAnaResource recOpeAnaResource = new RecOpeAnaResource(recOpeAnaService, recOpeAnaQueryService);
        this.restRecOpeAnaMockMvc = MockMvcBuilders.standaloneSetup(recOpeAnaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecOpeAna createEntity(EntityManager em) {
        RecOpeAna recOpeAna = new RecOpeAna()
            .email(DEFAULT_EMAIL)
            .pec(DEFAULT_PEC)
            .telefono(DEFAULT_TELEFONO)
            .fax(DEFAULT_FAX)
            .cell(DEFAULT_CELL)
            .cell2(DEFAULT_CELL_2)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return recOpeAna;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecOpeAna createUpdatedEntity(EntityManager em) {
        RecOpeAna recOpeAna = new RecOpeAna()
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return recOpeAna;
    }

    @BeforeEach
    public void initTest() {
        recOpeAna = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecOpeAna() throws Exception {
        int databaseSizeBeforeCreate = recOpeAnaRepository.findAll().size();

        // Create the RecOpeAna
        restRecOpeAnaMockMvc.perform(post("/api/rec-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recOpeAna)))
            .andExpect(status().isCreated());

        // Validate the RecOpeAna in the database
        List<RecOpeAna> recOpeAnaList = recOpeAnaRepository.findAll();
        assertThat(recOpeAnaList).hasSize(databaseSizeBeforeCreate + 1);
        RecOpeAna testRecOpeAna = recOpeAnaList.get(recOpeAnaList.size() - 1);
        assertThat(testRecOpeAna.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRecOpeAna.getPec()).isEqualTo(DEFAULT_PEC);
        assertThat(testRecOpeAna.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testRecOpeAna.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testRecOpeAna.getCell()).isEqualTo(DEFAULT_CELL);
        assertThat(testRecOpeAna.getCell2()).isEqualTo(DEFAULT_CELL_2);
        assertThat(testRecOpeAna.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testRecOpeAna.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testRecOpeAna.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testRecOpeAna.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testRecOpeAna.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createRecOpeAnaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recOpeAnaRepository.findAll().size();

        // Create the RecOpeAna with an existing ID
        recOpeAna.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecOpeAnaMockMvc.perform(post("/api/rec-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the RecOpeAna in the database
        List<RecOpeAna> recOpeAnaList = recOpeAnaRepository.findAll();
        assertThat(recOpeAnaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRecOpeAnas() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL.toString())))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getRecOpeAna() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get the recOpeAna
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas/{id}", recOpeAna.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(recOpeAna.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.pec").value(DEFAULT_PEC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.cell").value(DEFAULT_CELL.toString()))
            .andExpect(jsonPath("$.cell2").value(DEFAULT_CELL_2.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where email equals to DEFAULT_EMAIL
        defaultRecOpeAnaShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the recOpeAnaList where email equals to UPDATED_EMAIL
        defaultRecOpeAnaShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultRecOpeAnaShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the recOpeAnaList where email equals to UPDATED_EMAIL
        defaultRecOpeAnaShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where email is not null
        defaultRecOpeAnaShouldBeFound("email.specified=true");

        // Get all the recOpeAnaList where email is null
        defaultRecOpeAnaShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByPecIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where pec equals to DEFAULT_PEC
        defaultRecOpeAnaShouldBeFound("pec.equals=" + DEFAULT_PEC);

        // Get all the recOpeAnaList where pec equals to UPDATED_PEC
        defaultRecOpeAnaShouldNotBeFound("pec.equals=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByPecIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where pec in DEFAULT_PEC or UPDATED_PEC
        defaultRecOpeAnaShouldBeFound("pec.in=" + DEFAULT_PEC + "," + UPDATED_PEC);

        // Get all the recOpeAnaList where pec equals to UPDATED_PEC
        defaultRecOpeAnaShouldNotBeFound("pec.in=" + UPDATED_PEC);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByPecIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where pec is not null
        defaultRecOpeAnaShouldBeFound("pec.specified=true");

        // Get all the recOpeAnaList where pec is null
        defaultRecOpeAnaShouldNotBeFound("pec.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where telefono equals to DEFAULT_TELEFONO
        defaultRecOpeAnaShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the recOpeAnaList where telefono equals to UPDATED_TELEFONO
        defaultRecOpeAnaShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultRecOpeAnaShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the recOpeAnaList where telefono equals to UPDATED_TELEFONO
        defaultRecOpeAnaShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where telefono is not null
        defaultRecOpeAnaShouldBeFound("telefono.specified=true");

        // Get all the recOpeAnaList where telefono is null
        defaultRecOpeAnaShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where fax equals to DEFAULT_FAX
        defaultRecOpeAnaShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the recOpeAnaList where fax equals to UPDATED_FAX
        defaultRecOpeAnaShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultRecOpeAnaShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the recOpeAnaList where fax equals to UPDATED_FAX
        defaultRecOpeAnaShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where fax is not null
        defaultRecOpeAnaShouldBeFound("fax.specified=true");

        // Get all the recOpeAnaList where fax is null
        defaultRecOpeAnaShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCellIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell equals to DEFAULT_CELL
        defaultRecOpeAnaShouldBeFound("cell.equals=" + DEFAULT_CELL);

        // Get all the recOpeAnaList where cell equals to UPDATED_CELL
        defaultRecOpeAnaShouldNotBeFound("cell.equals=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCellIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell in DEFAULT_CELL or UPDATED_CELL
        defaultRecOpeAnaShouldBeFound("cell.in=" + DEFAULT_CELL + "," + UPDATED_CELL);

        // Get all the recOpeAnaList where cell equals to UPDATED_CELL
        defaultRecOpeAnaShouldNotBeFound("cell.in=" + UPDATED_CELL);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCellIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell is not null
        defaultRecOpeAnaShouldBeFound("cell.specified=true");

        // Get all the recOpeAnaList where cell is null
        defaultRecOpeAnaShouldNotBeFound("cell.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCell2IsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell2 equals to DEFAULT_CELL_2
        defaultRecOpeAnaShouldBeFound("cell2.equals=" + DEFAULT_CELL_2);

        // Get all the recOpeAnaList where cell2 equals to UPDATED_CELL_2
        defaultRecOpeAnaShouldNotBeFound("cell2.equals=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCell2IsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell2 in DEFAULT_CELL_2 or UPDATED_CELL_2
        defaultRecOpeAnaShouldBeFound("cell2.in=" + DEFAULT_CELL_2 + "," + UPDATED_CELL_2);

        // Get all the recOpeAnaList where cell2 equals to UPDATED_CELL_2
        defaultRecOpeAnaShouldNotBeFound("cell2.in=" + UPDATED_CELL_2);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByCell2IsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where cell2 is not null
        defaultRecOpeAnaShouldBeFound("cell2.specified=true");

        // Get all the recOpeAnaList where cell2 is null
        defaultRecOpeAnaShouldNotBeFound("cell2.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali is not null
        defaultRecOpeAnaShouldBeFound("dataInizVali.specified=true");

        // Get all the recOpeAnaList where dataInizVali is null
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultRecOpeAnaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the recOpeAnaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultRecOpeAnaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali is not null
        defaultRecOpeAnaShouldBeFound("dataFineVali.specified=true");

        // Get all the recOpeAnaList where dataFineVali is null
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultRecOpeAnaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the recOpeAnaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultRecOpeAnaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator is not null
        defaultRecOpeAnaShouldBeFound("userIdCreator.specified=true");

        // Get all the recOpeAnaList where userIdCreator is null
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultRecOpeAnaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the recOpeAnaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultRecOpeAnaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod is not null
        defaultRecOpeAnaShouldBeFound("userIdLastMod.specified=true");

        // Get all the recOpeAnaList where userIdLastMod is null
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllRecOpeAnasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);

        // Get all the recOpeAnaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the recOpeAnaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultRecOpeAnaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllRecOpeAnasByRecToOpeAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        recOpeAnaRepository.saveAndFlush(recOpeAna);
        OpeAnagrafica recToOpeAna = OpeAnagraficaResourceIT.createEntity(em);
        em.persist(recToOpeAna);
        em.flush();
        recOpeAna.setRecToOpeAna(recToOpeAna);
        recOpeAnaRepository.saveAndFlush(recOpeAna);
        Long recToOpeAnaId = recToOpeAna.getId();

        // Get all the recOpeAnaList where recToOpeAna equals to recToOpeAnaId
        defaultRecOpeAnaShouldBeFound("recToOpeAnaId.equals=" + recToOpeAnaId);

        // Get all the recOpeAnaList where recToOpeAna equals to recToOpeAnaId + 1
        defaultRecOpeAnaShouldNotBeFound("recToOpeAnaId.equals=" + (recToOpeAnaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecOpeAnaShouldBeFound(String filter) throws Exception {
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recOpeAna.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pec").value(hasItem(DEFAULT_PEC)))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].cell").value(hasItem(DEFAULT_CELL)))
            .andExpect(jsonPath("$.[*].cell2").value(hasItem(DEFAULT_CELL_2)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecOpeAnaShouldNotBeFound(String filter) throws Exception {
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRecOpeAna() throws Exception {
        // Get the recOpeAna
        restRecOpeAnaMockMvc.perform(get("/api/rec-ope-anas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecOpeAna() throws Exception {
        // Initialize the database
        recOpeAnaService.save(recOpeAna);

        int databaseSizeBeforeUpdate = recOpeAnaRepository.findAll().size();

        // Update the recOpeAna
        RecOpeAna updatedRecOpeAna = recOpeAnaRepository.findById(recOpeAna.getId()).get();
        // Disconnect from session so that the updates on updatedRecOpeAna are not directly saved in db
        em.detach(updatedRecOpeAna);
        updatedRecOpeAna
            .email(UPDATED_EMAIL)
            .pec(UPDATED_PEC)
            .telefono(UPDATED_TELEFONO)
            .fax(UPDATED_FAX)
            .cell(UPDATED_CELL)
            .cell2(UPDATED_CELL_2)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restRecOpeAnaMockMvc.perform(put("/api/rec-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecOpeAna)))
            .andExpect(status().isOk());

        // Validate the RecOpeAna in the database
        List<RecOpeAna> recOpeAnaList = recOpeAnaRepository.findAll();
        assertThat(recOpeAnaList).hasSize(databaseSizeBeforeUpdate);
        RecOpeAna testRecOpeAna = recOpeAnaList.get(recOpeAnaList.size() - 1);
        assertThat(testRecOpeAna.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRecOpeAna.getPec()).isEqualTo(UPDATED_PEC);
        assertThat(testRecOpeAna.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testRecOpeAna.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testRecOpeAna.getCell()).isEqualTo(UPDATED_CELL);
        assertThat(testRecOpeAna.getCell2()).isEqualTo(UPDATED_CELL_2);
        assertThat(testRecOpeAna.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testRecOpeAna.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testRecOpeAna.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testRecOpeAna.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testRecOpeAna.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingRecOpeAna() throws Exception {
        int databaseSizeBeforeUpdate = recOpeAnaRepository.findAll().size();

        // Create the RecOpeAna

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecOpeAnaMockMvc.perform(put("/api/rec-ope-anas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(recOpeAna)))
            .andExpect(status().isBadRequest());

        // Validate the RecOpeAna in the database
        List<RecOpeAna> recOpeAnaList = recOpeAnaRepository.findAll();
        assertThat(recOpeAnaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecOpeAna() throws Exception {
        // Initialize the database
        recOpeAnaService.save(recOpeAna);

        int databaseSizeBeforeDelete = recOpeAnaRepository.findAll().size();

        // Delete the recOpeAna
        restRecOpeAnaMockMvc.perform(delete("/api/rec-ope-anas/{id}", recOpeAna.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecOpeAna> recOpeAnaList = recOpeAnaRepository.findAll();
        assertThat(recOpeAnaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecOpeAna.class);
        RecOpeAna recOpeAna1 = new RecOpeAna();
        recOpeAna1.setId(1L);
        RecOpeAna recOpeAna2 = new RecOpeAna();
        recOpeAna2.setId(recOpeAna1.getId());
        assertThat(recOpeAna1).isEqualTo(recOpeAna2);
        recOpeAna2.setId(2L);
        assertThat(recOpeAna1).isNotEqualTo(recOpeAna2);
        recOpeAna1.setId(null);
        assertThat(recOpeAna1).isNotEqualTo(recOpeAna2);
    }
}
