package com.terrambe.web.rest;

import com.terrambe.TerrambeApp;
import com.terrambe.domain.AtrAnagrafica;
import com.terrambe.domain.UniAnagrafica;
import com.terrambe.domain.AtrTipologia;
import com.terrambe.domain.AtrMarchio;
import com.terrambe.repository.AtrAnagraficaRepository;
import com.terrambe.service.AtrAnagraficaService;
import com.terrambe.web.rest.errors.ExceptionTranslator;
import com.terrambe.service.dto.AtrAnagraficaCriteria;
import com.terrambe.service.AtrAnagraficaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.terrambe.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AtrAnagraficaResource} REST controller.
 */
@SpringBootTest(classes = TerrambeApp.class)
public class AtrAnagraficaResourceIT {

    private static final Long DEFAULT_ID_AZIENDA = 1L;
    private static final Long UPDATED_ID_AZIENDA = 2L;
    private static final Long SMALLER_ID_AZIENDA = 1L - 1L;

    private static final String DEFAULT_MODELLO = "AAAAAAAAAA";
    private static final String UPDATED_MODELLO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_REVISIONE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_REVISIONE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_REVISIONE = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_CAVALLI = 1;
    private static final Integer UPDATED_CAVALLI = 2;
    private static final Integer SMALLER_CAVALLI = 1 - 1;

    private static final Integer DEFAULT_CAPACITA = 1;
    private static final Integer UPDATED_CAPACITA = 2;
    private static final Integer SMALLER_CAPACITA = 1 - 1;

    private static final String DEFAULT_TARGA = "AAAAAAAAAA";
    private static final String UPDATED_TARGA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_INIZ_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZ_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_INIZ_VALI = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATA_FINE_VALI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE_VALI = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_FINE_VALI = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_USER_ID_CREATOR = 1L;
    private static final Long UPDATED_USER_ID_CREATOR = 2L;
    private static final Long SMALLER_USER_ID_CREATOR = 1L - 1L;

    private static final Long DEFAULT_USER_ID_LAST_MOD = 1L;
    private static final Long UPDATED_USER_ID_LAST_MOD = 2L;
    private static final Long SMALLER_USER_ID_LAST_MOD = 1L - 1L;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AtrAnagraficaRepository atrAnagraficaRepository;

    @Autowired
    private AtrAnagraficaService atrAnagraficaService;

    @Autowired
    private AtrAnagraficaQueryService atrAnagraficaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAtrAnagraficaMockMvc;

    private AtrAnagrafica atrAnagrafica;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtrAnagraficaResource atrAnagraficaResource = new AtrAnagraficaResource(atrAnagraficaService, atrAnagraficaQueryService);
        this.restAtrAnagraficaMockMvc = MockMvcBuilders.standaloneSetup(atrAnagraficaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrAnagrafica createEntity(EntityManager em) {
        AtrAnagrafica atrAnagrafica = new AtrAnagrafica()
            .idAzienda(DEFAULT_ID_AZIENDA)
            .modello(DEFAULT_MODELLO)
            .dataRevisione(DEFAULT_DATA_REVISIONE)
            .cavalli(DEFAULT_CAVALLI)
            .capacita(DEFAULT_CAPACITA)
            .targa(DEFAULT_TARGA)
            .dataInizVali(DEFAULT_DATA_INIZ_VALI)
            .dataFineVali(DEFAULT_DATA_FINE_VALI)
            .userIdCreator(DEFAULT_USER_ID_CREATOR)
            .userIdLastMod(DEFAULT_USER_ID_LAST_MOD)
            .note(DEFAULT_NOTE);
        return atrAnagrafica;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtrAnagrafica createUpdatedEntity(EntityManager em) {
        AtrAnagrafica atrAnagrafica = new AtrAnagrafica()
            .idAzienda(UPDATED_ID_AZIENDA)
            .modello(UPDATED_MODELLO)
            .dataRevisione(UPDATED_DATA_REVISIONE)
            .cavalli(UPDATED_CAVALLI)
            .capacita(UPDATED_CAPACITA)
            .targa(UPDATED_TARGA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);
        return atrAnagrafica;
    }

    @BeforeEach
    public void initTest() {
        atrAnagrafica = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtrAnagrafica() throws Exception {
        int databaseSizeBeforeCreate = atrAnagraficaRepository.findAll().size();

        // Create the AtrAnagrafica
        restAtrAnagraficaMockMvc.perform(post("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrAnagrafica)))
            .andExpect(status().isCreated());

        // Validate the AtrAnagrafica in the database
        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeCreate + 1);
        AtrAnagrafica testAtrAnagrafica = atrAnagraficaList.get(atrAnagraficaList.size() - 1);
        assertThat(testAtrAnagrafica.getIdAzienda()).isEqualTo(DEFAULT_ID_AZIENDA);
        assertThat(testAtrAnagrafica.getModello()).isEqualTo(DEFAULT_MODELLO);
        assertThat(testAtrAnagrafica.getDataRevisione()).isEqualTo(DEFAULT_DATA_REVISIONE);
        assertThat(testAtrAnagrafica.getCavalli()).isEqualTo(DEFAULT_CAVALLI);
        assertThat(testAtrAnagrafica.getCapacita()).isEqualTo(DEFAULT_CAPACITA);
        assertThat(testAtrAnagrafica.getTarga()).isEqualTo(DEFAULT_TARGA);
        assertThat(testAtrAnagrafica.getDataInizVali()).isEqualTo(DEFAULT_DATA_INIZ_VALI);
        assertThat(testAtrAnagrafica.getDataFineVali()).isEqualTo(DEFAULT_DATA_FINE_VALI);
        assertThat(testAtrAnagrafica.getUserIdCreator()).isEqualTo(DEFAULT_USER_ID_CREATOR);
        assertThat(testAtrAnagrafica.getUserIdLastMod()).isEqualTo(DEFAULT_USER_ID_LAST_MOD);
        assertThat(testAtrAnagrafica.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void createAtrAnagraficaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atrAnagraficaRepository.findAll().size();

        // Create the AtrAnagrafica with an existing ID
        atrAnagrafica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtrAnagraficaMockMvc.perform(post("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the AtrAnagrafica in the database
        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdAziendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = atrAnagraficaRepository.findAll().size();
        // set the field null
        atrAnagrafica.setIdAzienda(null);

        // Create the AtrAnagrafica, which fails.

        restAtrAnagraficaMockMvc.perform(post("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModelloIsRequired() throws Exception {
        int databaseSizeBeforeTest = atrAnagraficaRepository.findAll().size();
        // set the field null
        atrAnagrafica.setModello(null);

        // Create the AtrAnagrafica, which fails.

        restAtrAnagraficaMockMvc.perform(post("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrAnagrafica)))
            .andExpect(status().isBadRequest());

        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficas() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].modello").value(hasItem(DEFAULT_MODELLO.toString())))
            .andExpect(jsonPath("$.[*].dataRevisione").value(hasItem(DEFAULT_DATA_REVISIONE.toString())))
            .andExpect(jsonPath("$.[*].cavalli").value(hasItem(DEFAULT_CAVALLI)))
            .andExpect(jsonPath("$.[*].capacita").value(hasItem(DEFAULT_CAPACITA)))
            .andExpect(jsonPath("$.[*].targa").value(hasItem(DEFAULT_TARGA.toString())))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getAtrAnagrafica() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get the atrAnagrafica
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas/{id}", atrAnagrafica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(atrAnagrafica.getId().intValue()))
            .andExpect(jsonPath("$.idAzienda").value(DEFAULT_ID_AZIENDA.intValue()))
            .andExpect(jsonPath("$.modello").value(DEFAULT_MODELLO.toString()))
            .andExpect(jsonPath("$.dataRevisione").value(DEFAULT_DATA_REVISIONE.toString()))
            .andExpect(jsonPath("$.cavalli").value(DEFAULT_CAVALLI))
            .andExpect(jsonPath("$.capacita").value(DEFAULT_CAPACITA))
            .andExpect(jsonPath("$.targa").value(DEFAULT_TARGA.toString()))
            .andExpect(jsonPath("$.dataInizVali").value(DEFAULT_DATA_INIZ_VALI.toString()))
            .andExpect(jsonPath("$.dataFineVali").value(DEFAULT_DATA_FINE_VALI.toString()))
            .andExpect(jsonPath("$.userIdCreator").value(DEFAULT_USER_ID_CREATOR.intValue()))
            .andExpect(jsonPath("$.userIdLastMod").value(DEFAULT_USER_ID_LAST_MOD.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda equals to DEFAULT_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.equals=" + DEFAULT_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.equals=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda in DEFAULT_ID_AZIENDA or UPDATED_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.in=" + DEFAULT_ID_AZIENDA + "," + UPDATED_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda equals to UPDATED_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.in=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda is not null
        defaultAtrAnagraficaShouldBeFound("idAzienda.specified=true");

        // Get all the atrAnagraficaList where idAzienda is null
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda is greater than or equal to DEFAULT_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.greaterThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda is greater than or equal to UPDATED_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.greaterThanOrEqual=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda is less than or equal to DEFAULT_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.lessThanOrEqual=" + DEFAULT_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda is less than or equal to SMALLER_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.lessThanOrEqual=" + SMALLER_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda is less than DEFAULT_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.lessThan=" + DEFAULT_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda is less than UPDATED_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.lessThan=" + UPDATED_ID_AZIENDA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByIdAziendaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where idAzienda is greater than DEFAULT_ID_AZIENDA
        defaultAtrAnagraficaShouldNotBeFound("idAzienda.greaterThan=" + DEFAULT_ID_AZIENDA);

        // Get all the atrAnagraficaList where idAzienda is greater than SMALLER_ID_AZIENDA
        defaultAtrAnagraficaShouldBeFound("idAzienda.greaterThan=" + SMALLER_ID_AZIENDA);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByModelloIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where modello equals to DEFAULT_MODELLO
        defaultAtrAnagraficaShouldBeFound("modello.equals=" + DEFAULT_MODELLO);

        // Get all the atrAnagraficaList where modello equals to UPDATED_MODELLO
        defaultAtrAnagraficaShouldNotBeFound("modello.equals=" + UPDATED_MODELLO);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByModelloIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where modello in DEFAULT_MODELLO or UPDATED_MODELLO
        defaultAtrAnagraficaShouldBeFound("modello.in=" + DEFAULT_MODELLO + "," + UPDATED_MODELLO);

        // Get all the atrAnagraficaList where modello equals to UPDATED_MODELLO
        defaultAtrAnagraficaShouldNotBeFound("modello.in=" + UPDATED_MODELLO);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByModelloIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where modello is not null
        defaultAtrAnagraficaShouldBeFound("modello.specified=true");

        // Get all the atrAnagraficaList where modello is null
        defaultAtrAnagraficaShouldNotBeFound("modello.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione equals to DEFAULT_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.equals=" + DEFAULT_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione equals to UPDATED_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.equals=" + UPDATED_DATA_REVISIONE);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione in DEFAULT_DATA_REVISIONE or UPDATED_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.in=" + DEFAULT_DATA_REVISIONE + "," + UPDATED_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione equals to UPDATED_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.in=" + UPDATED_DATA_REVISIONE);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione is not null
        defaultAtrAnagraficaShouldBeFound("dataRevisione.specified=true");

        // Get all the atrAnagraficaList where dataRevisione is null
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione is greater than or equal to DEFAULT_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.greaterThanOrEqual=" + DEFAULT_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione is greater than or equal to UPDATED_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.greaterThanOrEqual=" + UPDATED_DATA_REVISIONE);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione is less than or equal to DEFAULT_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.lessThanOrEqual=" + DEFAULT_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione is less than or equal to SMALLER_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.lessThanOrEqual=" + SMALLER_DATA_REVISIONE);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione is less than DEFAULT_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.lessThan=" + DEFAULT_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione is less than UPDATED_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.lessThan=" + UPDATED_DATA_REVISIONE);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataRevisioneIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataRevisione is greater than DEFAULT_DATA_REVISIONE
        defaultAtrAnagraficaShouldNotBeFound("dataRevisione.greaterThan=" + DEFAULT_DATA_REVISIONE);

        // Get all the atrAnagraficaList where dataRevisione is greater than SMALLER_DATA_REVISIONE
        defaultAtrAnagraficaShouldBeFound("dataRevisione.greaterThan=" + SMALLER_DATA_REVISIONE);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli equals to DEFAULT_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.equals=" + DEFAULT_CAVALLI);

        // Get all the atrAnagraficaList where cavalli equals to UPDATED_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.equals=" + UPDATED_CAVALLI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli in DEFAULT_CAVALLI or UPDATED_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.in=" + DEFAULT_CAVALLI + "," + UPDATED_CAVALLI);

        // Get all the atrAnagraficaList where cavalli equals to UPDATED_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.in=" + UPDATED_CAVALLI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli is not null
        defaultAtrAnagraficaShouldBeFound("cavalli.specified=true");

        // Get all the atrAnagraficaList where cavalli is null
        defaultAtrAnagraficaShouldNotBeFound("cavalli.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli is greater than or equal to DEFAULT_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.greaterThanOrEqual=" + DEFAULT_CAVALLI);

        // Get all the atrAnagraficaList where cavalli is greater than or equal to UPDATED_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.greaterThanOrEqual=" + UPDATED_CAVALLI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli is less than or equal to DEFAULT_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.lessThanOrEqual=" + DEFAULT_CAVALLI);

        // Get all the atrAnagraficaList where cavalli is less than or equal to SMALLER_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.lessThanOrEqual=" + SMALLER_CAVALLI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli is less than DEFAULT_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.lessThan=" + DEFAULT_CAVALLI);

        // Get all the atrAnagraficaList where cavalli is less than UPDATED_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.lessThan=" + UPDATED_CAVALLI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCavalliIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where cavalli is greater than DEFAULT_CAVALLI
        defaultAtrAnagraficaShouldNotBeFound("cavalli.greaterThan=" + DEFAULT_CAVALLI);

        // Get all the atrAnagraficaList where cavalli is greater than SMALLER_CAVALLI
        defaultAtrAnagraficaShouldBeFound("cavalli.greaterThan=" + SMALLER_CAVALLI);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita equals to DEFAULT_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.equals=" + DEFAULT_CAPACITA);

        // Get all the atrAnagraficaList where capacita equals to UPDATED_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.equals=" + UPDATED_CAPACITA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita in DEFAULT_CAPACITA or UPDATED_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.in=" + DEFAULT_CAPACITA + "," + UPDATED_CAPACITA);

        // Get all the atrAnagraficaList where capacita equals to UPDATED_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.in=" + UPDATED_CAPACITA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita is not null
        defaultAtrAnagraficaShouldBeFound("capacita.specified=true");

        // Get all the atrAnagraficaList where capacita is null
        defaultAtrAnagraficaShouldNotBeFound("capacita.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita is greater than or equal to DEFAULT_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.greaterThanOrEqual=" + DEFAULT_CAPACITA);

        // Get all the atrAnagraficaList where capacita is greater than or equal to UPDATED_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.greaterThanOrEqual=" + UPDATED_CAPACITA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita is less than or equal to DEFAULT_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.lessThanOrEqual=" + DEFAULT_CAPACITA);

        // Get all the atrAnagraficaList where capacita is less than or equal to SMALLER_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.lessThanOrEqual=" + SMALLER_CAPACITA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita is less than DEFAULT_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.lessThan=" + DEFAULT_CAPACITA);

        // Get all the atrAnagraficaList where capacita is less than UPDATED_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.lessThan=" + UPDATED_CAPACITA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByCapacitaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where capacita is greater than DEFAULT_CAPACITA
        defaultAtrAnagraficaShouldNotBeFound("capacita.greaterThan=" + DEFAULT_CAPACITA);

        // Get all the atrAnagraficaList where capacita is greater than SMALLER_CAPACITA
        defaultAtrAnagraficaShouldBeFound("capacita.greaterThan=" + SMALLER_CAPACITA);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByTargaIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where targa equals to DEFAULT_TARGA
        defaultAtrAnagraficaShouldBeFound("targa.equals=" + DEFAULT_TARGA);

        // Get all the atrAnagraficaList where targa equals to UPDATED_TARGA
        defaultAtrAnagraficaShouldNotBeFound("targa.equals=" + UPDATED_TARGA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByTargaIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where targa in DEFAULT_TARGA or UPDATED_TARGA
        defaultAtrAnagraficaShouldBeFound("targa.in=" + DEFAULT_TARGA + "," + UPDATED_TARGA);

        // Get all the atrAnagraficaList where targa equals to UPDATED_TARGA
        defaultAtrAnagraficaShouldNotBeFound("targa.in=" + UPDATED_TARGA);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByTargaIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where targa is not null
        defaultAtrAnagraficaShouldBeFound("targa.specified=true");

        // Get all the atrAnagraficaList where targa is null
        defaultAtrAnagraficaShouldNotBeFound("targa.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali equals to DEFAULT_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.equals=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.equals=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali in DEFAULT_DATA_INIZ_VALI or UPDATED_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.in=" + DEFAULT_DATA_INIZ_VALI + "," + UPDATED_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali equals to UPDATED_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.in=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali is not null
        defaultAtrAnagraficaShouldBeFound("dataInizVali.specified=true");

        // Get all the atrAnagraficaList where dataInizVali is null
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali is greater than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.greaterThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali is greater than or equal to UPDATED_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.greaterThanOrEqual=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali is less than or equal to DEFAULT_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.lessThanOrEqual=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali is less than or equal to SMALLER_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.lessThanOrEqual=" + SMALLER_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali is less than DEFAULT_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.lessThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali is less than UPDATED_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.lessThan=" + UPDATED_DATA_INIZ_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataInizValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataInizVali is greater than DEFAULT_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataInizVali.greaterThan=" + DEFAULT_DATA_INIZ_VALI);

        // Get all the atrAnagraficaList where dataInizVali is greater than SMALLER_DATA_INIZ_VALI
        defaultAtrAnagraficaShouldBeFound("dataInizVali.greaterThan=" + SMALLER_DATA_INIZ_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali equals to DEFAULT_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.equals=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.equals=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali in DEFAULT_DATA_FINE_VALI or UPDATED_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.in=" + DEFAULT_DATA_FINE_VALI + "," + UPDATED_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali equals to UPDATED_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.in=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali is not null
        defaultAtrAnagraficaShouldBeFound("dataFineVali.specified=true");

        // Get all the atrAnagraficaList where dataFineVali is null
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali is greater than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.greaterThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali is greater than or equal to UPDATED_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.greaterThanOrEqual=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali is less than or equal to DEFAULT_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.lessThanOrEqual=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali is less than or equal to SMALLER_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.lessThanOrEqual=" + SMALLER_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali is less than DEFAULT_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.lessThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali is less than UPDATED_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.lessThan=" + UPDATED_DATA_FINE_VALI);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByDataFineValiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where dataFineVali is greater than DEFAULT_DATA_FINE_VALI
        defaultAtrAnagraficaShouldNotBeFound("dataFineVali.greaterThan=" + DEFAULT_DATA_FINE_VALI);

        // Get all the atrAnagraficaList where dataFineVali is greater than SMALLER_DATA_FINE_VALI
        defaultAtrAnagraficaShouldBeFound("dataFineVali.greaterThan=" + SMALLER_DATA_FINE_VALI);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator equals to DEFAULT_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.equals=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.equals=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator in DEFAULT_USER_ID_CREATOR or UPDATED_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.in=" + DEFAULT_USER_ID_CREATOR + "," + UPDATED_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator equals to UPDATED_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.in=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator is not null
        defaultAtrAnagraficaShouldBeFound("userIdCreator.specified=true");

        // Get all the atrAnagraficaList where userIdCreator is null
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator is greater than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.greaterThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator is greater than or equal to UPDATED_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.greaterThanOrEqual=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator is less than or equal to DEFAULT_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.lessThanOrEqual=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator is less than or equal to SMALLER_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.lessThanOrEqual=" + SMALLER_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator is less than DEFAULT_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.lessThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator is less than UPDATED_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.lessThan=" + UPDATED_USER_ID_CREATOR);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdCreatorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdCreator is greater than DEFAULT_USER_ID_CREATOR
        defaultAtrAnagraficaShouldNotBeFound("userIdCreator.greaterThan=" + DEFAULT_USER_ID_CREATOR);

        // Get all the atrAnagraficaList where userIdCreator is greater than SMALLER_USER_ID_CREATOR
        defaultAtrAnagraficaShouldBeFound("userIdCreator.greaterThan=" + SMALLER_USER_ID_CREATOR);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod equals to DEFAULT_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.equals=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.equals=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsInShouldWork() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod in DEFAULT_USER_ID_LAST_MOD or UPDATED_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.in=" + DEFAULT_USER_ID_LAST_MOD + "," + UPDATED_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod equals to UPDATED_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.in=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsNullOrNotNull() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod is not null
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.specified=true");

        // Get all the atrAnagraficaList where userIdLastMod is null
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.specified=false");
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod is greater than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.greaterThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod is greater than or equal to UPDATED_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.greaterThanOrEqual=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod is less than or equal to DEFAULT_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.lessThanOrEqual=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod is less than or equal to SMALLER_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.lessThanOrEqual=" + SMALLER_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsLessThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod is less than DEFAULT_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.lessThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod is less than UPDATED_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.lessThan=" + UPDATED_USER_ID_LAST_MOD);
    }

    @Test
    @Transactional
    public void getAllAtrAnagraficasByUserIdLastModIsGreaterThanSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);

        // Get all the atrAnagraficaList where userIdLastMod is greater than DEFAULT_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldNotBeFound("userIdLastMod.greaterThan=" + DEFAULT_USER_ID_LAST_MOD);

        // Get all the atrAnagraficaList where userIdLastMod is greater than SMALLER_USER_ID_LAST_MOD
        defaultAtrAnagraficaShouldBeFound("userIdLastMod.greaterThan=" + SMALLER_USER_ID_LAST_MOD);
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByAtrAnaToUniAnaIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        UniAnagrafica atrAnaToUniAna = UniAnagraficaResourceIT.createEntity(em);
        em.persist(atrAnaToUniAna);
        em.flush();
        atrAnagrafica.setAtrAnaToUniAna(atrAnaToUniAna);
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        Long atrAnaToUniAnaId = atrAnaToUniAna.getId();

        // Get all the atrAnagraficaList where atrAnaToUniAna equals to atrAnaToUniAnaId
        defaultAtrAnagraficaShouldBeFound("atrAnaToUniAnaId.equals=" + atrAnaToUniAnaId);

        // Get all the atrAnagraficaList where atrAnaToUniAna equals to atrAnaToUniAnaId + 1
        defaultAtrAnagraficaShouldNotBeFound("atrAnaToUniAnaId.equals=" + (atrAnaToUniAnaId + 1));
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByAtrAnaToAtrTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        AtrTipologia atrAnaToAtrTipo = AtrTipologiaResourceIT.createEntity(em);
        em.persist(atrAnaToAtrTipo);
        em.flush();
        atrAnagrafica.setAtrAnaToAtrTipo(atrAnaToAtrTipo);
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        Long atrAnaToAtrTipoId = atrAnaToAtrTipo.getId();

        // Get all the atrAnagraficaList where atrAnaToAtrTipo equals to atrAnaToAtrTipoId
        defaultAtrAnagraficaShouldBeFound("atrAnaToAtrTipoId.equals=" + atrAnaToAtrTipoId);

        // Get all the atrAnagraficaList where atrAnaToAtrTipo equals to atrAnaToAtrTipoId + 1
        defaultAtrAnagraficaShouldNotBeFound("atrAnaToAtrTipoId.equals=" + (atrAnaToAtrTipoId + 1));
    }


    @Test
    @Transactional
    public void getAllAtrAnagraficasByAtrAnagToAtrMarcIsEqualToSomething() throws Exception {
        // Initialize the database
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        AtrMarchio atrAnagToAtrMarc = AtrMarchioResourceIT.createEntity(em);
        em.persist(atrAnagToAtrMarc);
        em.flush();
        atrAnagrafica.setAtrAnagToAtrMarc(atrAnagToAtrMarc);
        atrAnagraficaRepository.saveAndFlush(atrAnagrafica);
        Long atrAnagToAtrMarcId = atrAnagToAtrMarc.getId();

        // Get all the atrAnagraficaList where atrAnagToAtrMarc equals to atrAnagToAtrMarcId
        defaultAtrAnagraficaShouldBeFound("atrAnagToAtrMarcId.equals=" + atrAnagToAtrMarcId);

        // Get all the atrAnagraficaList where atrAnagToAtrMarc equals to atrAnagToAtrMarcId + 1
        defaultAtrAnagraficaShouldNotBeFound("atrAnagToAtrMarcId.equals=" + (atrAnagToAtrMarcId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAtrAnagraficaShouldBeFound(String filter) throws Exception {
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrAnagrafica.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAzienda").value(hasItem(DEFAULT_ID_AZIENDA.intValue())))
            .andExpect(jsonPath("$.[*].modello").value(hasItem(DEFAULT_MODELLO)))
            .andExpect(jsonPath("$.[*].dataRevisione").value(hasItem(DEFAULT_DATA_REVISIONE.toString())))
            .andExpect(jsonPath("$.[*].cavalli").value(hasItem(DEFAULT_CAVALLI)))
            .andExpect(jsonPath("$.[*].capacita").value(hasItem(DEFAULT_CAPACITA)))
            .andExpect(jsonPath("$.[*].targa").value(hasItem(DEFAULT_TARGA)))
            .andExpect(jsonPath("$.[*].dataInizVali").value(hasItem(DEFAULT_DATA_INIZ_VALI.toString())))
            .andExpect(jsonPath("$.[*].dataFineVali").value(hasItem(DEFAULT_DATA_FINE_VALI.toString())))
            .andExpect(jsonPath("$.[*].userIdCreator").value(hasItem(DEFAULT_USER_ID_CREATOR.intValue())))
            .andExpect(jsonPath("$.[*].userIdLastMod").value(hasItem(DEFAULT_USER_ID_LAST_MOD.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));

        // Check, that the count call also returns 1
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAtrAnagraficaShouldNotBeFound(String filter) throws Exception {
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAtrAnagrafica() throws Exception {
        // Get the atrAnagrafica
        restAtrAnagraficaMockMvc.perform(get("/api/atr-anagraficas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtrAnagrafica() throws Exception {
        // Initialize the database
        atrAnagraficaService.save(atrAnagrafica);

        int databaseSizeBeforeUpdate = atrAnagraficaRepository.findAll().size();

        // Update the atrAnagrafica
        AtrAnagrafica updatedAtrAnagrafica = atrAnagraficaRepository.findById(atrAnagrafica.getId()).get();
        // Disconnect from session so that the updates on updatedAtrAnagrafica are not directly saved in db
        em.detach(updatedAtrAnagrafica);
        updatedAtrAnagrafica
            .idAzienda(UPDATED_ID_AZIENDA)
            .modello(UPDATED_MODELLO)
            .dataRevisione(UPDATED_DATA_REVISIONE)
            .cavalli(UPDATED_CAVALLI)
            .capacita(UPDATED_CAPACITA)
            .targa(UPDATED_TARGA)
            .dataInizVali(UPDATED_DATA_INIZ_VALI)
            .dataFineVali(UPDATED_DATA_FINE_VALI)
            .userIdCreator(UPDATED_USER_ID_CREATOR)
            .userIdLastMod(UPDATED_USER_ID_LAST_MOD)
            .note(UPDATED_NOTE);

        restAtrAnagraficaMockMvc.perform(put("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAtrAnagrafica)))
            .andExpect(status().isOk());

        // Validate the AtrAnagrafica in the database
        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeUpdate);
        AtrAnagrafica testAtrAnagrafica = atrAnagraficaList.get(atrAnagraficaList.size() - 1);
        assertThat(testAtrAnagrafica.getIdAzienda()).isEqualTo(UPDATED_ID_AZIENDA);
        assertThat(testAtrAnagrafica.getModello()).isEqualTo(UPDATED_MODELLO);
        assertThat(testAtrAnagrafica.getDataRevisione()).isEqualTo(UPDATED_DATA_REVISIONE);
        assertThat(testAtrAnagrafica.getCavalli()).isEqualTo(UPDATED_CAVALLI);
        assertThat(testAtrAnagrafica.getCapacita()).isEqualTo(UPDATED_CAPACITA);
        assertThat(testAtrAnagrafica.getTarga()).isEqualTo(UPDATED_TARGA);
        assertThat(testAtrAnagrafica.getDataInizVali()).isEqualTo(UPDATED_DATA_INIZ_VALI);
        assertThat(testAtrAnagrafica.getDataFineVali()).isEqualTo(UPDATED_DATA_FINE_VALI);
        assertThat(testAtrAnagrafica.getUserIdCreator()).isEqualTo(UPDATED_USER_ID_CREATOR);
        assertThat(testAtrAnagrafica.getUserIdLastMod()).isEqualTo(UPDATED_USER_ID_LAST_MOD);
        assertThat(testAtrAnagrafica.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingAtrAnagrafica() throws Exception {
        int databaseSizeBeforeUpdate = atrAnagraficaRepository.findAll().size();

        // Create the AtrAnagrafica

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtrAnagraficaMockMvc.perform(put("/api/atr-anagraficas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrAnagrafica)))
            .andExpect(status().isBadRequest());

        // Validate the AtrAnagrafica in the database
        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAtrAnagrafica() throws Exception {
        // Initialize the database
        atrAnagraficaService.save(atrAnagrafica);

        int databaseSizeBeforeDelete = atrAnagraficaRepository.findAll().size();

        // Delete the atrAnagrafica
        restAtrAnagraficaMockMvc.perform(delete("/api/atr-anagraficas/{id}", atrAnagrafica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AtrAnagrafica> atrAnagraficaList = atrAnagraficaRepository.findAll();
        assertThat(atrAnagraficaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtrAnagrafica.class);
        AtrAnagrafica atrAnagrafica1 = new AtrAnagrafica();
        atrAnagrafica1.setId(1L);
        AtrAnagrafica atrAnagrafica2 = new AtrAnagrafica();
        atrAnagrafica2.setId(atrAnagrafica1.getId());
        assertThat(atrAnagrafica1).isEqualTo(atrAnagrafica2);
        atrAnagrafica2.setId(2L);
        assertThat(atrAnagrafica1).isNotEqualTo(atrAnagrafica2);
        atrAnagrafica1.setId(null);
        assertThat(atrAnagrafica1).isNotEqualTo(atrAnagrafica2);
    }
}
