package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

//MODIFICA -----------------------------------------
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Fornitori.
 */
@Entity
@Table(name = "fornitori")
@DynamicUpdate
public class Fornitori implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "registro_imprese")
    private String registroImprese;

    @Column(name = "num_iscrizione")
    private String numIscrizione;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @NotNull
    @Column(name = "nome_fornitore", nullable = false)
    private String nomeFornitore;

    @NotNull
    @Size(min = 11, max = 11)
    @Column(name = "piva", length = 11, nullable = false)
    private String piva;

    @Size(min = 16, max = 16)
    @Column(name = "codi_fisc", length = 16)
    private String codiFisc;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "ind_to_forn_id")
	@JsonIgnoreProperties("indToForn")
	//@OneToMany(mappedBy = "indToForn")
	private Set<IndForn> fornToInds = new HashSet<>();

//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "rec_to_forn_id")
	@JsonIgnoreProperties("recToForn")
	//@OneToMany(mappedBy = "recToForn")
	private Set<RecForn> fornToRecs = new HashSet<>();

//MODIFICA ------------------------------------------
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "fornitori_fornitoritipologia",
               joinColumns = @JoinColumn(name = "fornitori_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "fornitoritipologia_id", referencedColumnName = "id"))
    private Set<FornitoriTipologia> fornitoritipologias = new HashSet<>();

    @OneToMany(mappedBy = "caricoFertToFornitori")
    private Set<RegCaricoFertilizzante> fornitoriToCaricoFerts = new HashSet<>();

    @OneToMany(mappedBy = "caricoFitoToFornitori")
    private Set<RegCaricoFitoFarmaco> fornitoriToCaricoFitos = new HashSet<>();

    @OneToMany(mappedBy = "caricoTrapToFornitori")
    private Set<RegCaricoTrappole> fornitoriToCaricoTraps = new HashSet<>();

    @ManyToMany(mappedBy = "opeAnaToForns")
    @JsonIgnore
    private Set<OpeAnagrafica> fornToOpeAnas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistroImprese() {
        return registroImprese;
    }

    public Fornitori registroImprese(String registroImprese) {
        this.registroImprese = registroImprese;
        return this;
    }

    public void setRegistroImprese(String registroImprese) {
        this.registroImprese = registroImprese;
    }

    public String getNumIscrizione() {
        return numIscrizione;
    }

    public Fornitori numIscrizione(String numIscrizione) {
        this.numIscrizione = numIscrizione;
        return this;
    }

    public void setNumIscrizione(String numIscrizione) {
        this.numIscrizione = numIscrizione;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public Fornitori idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public String getNomeFornitore() {
        return nomeFornitore;
    }

    public Fornitori nomeFornitore(String nomeFornitore) {
        this.nomeFornitore = nomeFornitore;
        return this;
    }

    public void setNomeFornitore(String nomeFornitore) {
        this.nomeFornitore = nomeFornitore;
    }

    public String getPiva() {
        return piva;
    }

    public Fornitori piva(String piva) {
        this.piva = piva;
        return this;
    }

    public void setPiva(String piva) {
        this.piva = piva;
    }

    public String getCodiFisc() {
        return codiFisc;
    }

    public Fornitori codiFisc(String codiFisc) {
        this.codiFisc = codiFisc;
        return this;
    }

    public void setCodiFisc(String codiFisc) {
        this.codiFisc = codiFisc;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Fornitori dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Fornitori dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Fornitori userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Fornitori userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Fornitori note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IndForn> getFornToInds() {
        return fornToInds;
    }

    public Fornitori fornToInds(Set<IndForn> indForns) {
        this.fornToInds = indForns;
        return this;
    }

    public Fornitori addFornToInd(IndForn indForn) {
        this.fornToInds.add(indForn);
        indForn.setIndToForn(this);
        return this;
    }

    public Fornitori removeFornToInd(IndForn indForn) {
        this.fornToInds.remove(indForn);
        indForn.setIndToForn(null);
        return this;
    }

    public void setFornToInds(Set<IndForn> indForns) {
        this.fornToInds = indForns;
    }

    public Set<RecForn> getFornToRecs() {
        return fornToRecs;
    }

    public Fornitori fornToRecs(Set<RecForn> recForns) {
        this.fornToRecs = recForns;
        return this;
    }

    public Fornitori addFornToRec(RecForn recForn) {
        this.fornToRecs.add(recForn);
        recForn.setRecToForn(this);
        return this;
    }

    public Fornitori removeFornToRec(RecForn recForn) {
        this.fornToRecs.remove(recForn);
        recForn.setRecToForn(null);
        return this;
    }

    public void setFornToRecs(Set<RecForn> recForns) {
        this.fornToRecs = recForns;
    }

    public Set<FornitoriTipologia> getFornitoritipologias() {
        return fornitoritipologias;
    }

    public Fornitori fornitoritipologias(Set<FornitoriTipologia> fornitoriTipologias) {
        this.fornitoritipologias = fornitoriTipologias;
        return this;
    }

    public Fornitori addFornitoritipologia(FornitoriTipologia fornitoriTipologia) {
        this.fornitoritipologias.add(fornitoriTipologia);
        fornitoriTipologia.getFornitoris().add(this);
        return this;
    }

    public Fornitori removeFornitoritipologia(FornitoriTipologia fornitoriTipologia) {
        this.fornitoritipologias.remove(fornitoriTipologia);
        fornitoriTipologia.getFornitoris().remove(this);
        return this;
    }

    public void setFornitoritipologias(Set<FornitoriTipologia> fornitoriTipologias) {
        this.fornitoritipologias = fornitoriTipologias;
    }

    public Set<RegCaricoFertilizzante> getFornitoriToCaricoFerts() {
        return fornitoriToCaricoFerts;
    }

    public Fornitori fornitoriToCaricoFerts(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.fornitoriToCaricoFerts = regCaricoFertilizzantes;
        return this;
    }

    public Fornitori addFornitoriToCaricoFert(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.fornitoriToCaricoFerts.add(regCaricoFertilizzante);
        regCaricoFertilizzante.setCaricoFertToFornitori(this);
        return this;
    }

    public Fornitori removeFornitoriToCaricoFert(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.fornitoriToCaricoFerts.remove(regCaricoFertilizzante);
        regCaricoFertilizzante.setCaricoFertToFornitori(null);
        return this;
    }

    public void setFornitoriToCaricoFerts(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.fornitoriToCaricoFerts = regCaricoFertilizzantes;
    }

    public Set<RegCaricoFitoFarmaco> getFornitoriToCaricoFitos() {
        return fornitoriToCaricoFitos;
    }

    public Fornitori fornitoriToCaricoFitos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.fornitoriToCaricoFitos = regCaricoFitoFarmacos;
        return this;
    }

    public Fornitori addFornitoriToCaricoFito(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.fornitoriToCaricoFitos.add(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setCaricoFitoToFornitori(this);
        return this;
    }

    public Fornitori removeFornitoriToCaricoFito(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.fornitoriToCaricoFitos.remove(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setCaricoFitoToFornitori(null);
        return this;
    }

    public void setFornitoriToCaricoFitos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.fornitoriToCaricoFitos = regCaricoFitoFarmacos;
    }

    public Set<RegCaricoTrappole> getFornitoriToCaricoTraps() {
        return fornitoriToCaricoTraps;
    }

    public Fornitori fornitoriToCaricoTraps(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.fornitoriToCaricoTraps = regCaricoTrappoles;
        return this;
    }

    public Fornitori addFornitoriToCaricoTrap(RegCaricoTrappole regCaricoTrappole) {
        this.fornitoriToCaricoTraps.add(regCaricoTrappole);
        regCaricoTrappole.setCaricoTrapToFornitori(this);
        return this;
    }

    public Fornitori removeFornitoriToCaricoTrap(RegCaricoTrappole regCaricoTrappole) {
        this.fornitoriToCaricoTraps.remove(regCaricoTrappole);
        regCaricoTrappole.setCaricoTrapToFornitori(null);
        return this;
    }

    public void setFornitoriToCaricoTraps(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.fornitoriToCaricoTraps = regCaricoTrappoles;
    }

    public Set<OpeAnagrafica> getFornToOpeAnas() {
        return fornToOpeAnas;
    }

    public Fornitori fornToOpeAnas(Set<OpeAnagrafica> opeAnagraficas) {
        this.fornToOpeAnas = opeAnagraficas;
        return this;
    }

    public Fornitori addFornToOpeAna(OpeAnagrafica opeAnagrafica) {
        this.fornToOpeAnas.add(opeAnagrafica);
        opeAnagrafica.getOpeAnaToForns().add(this);
        return this;
    }

    public Fornitori removeFornToOpeAna(OpeAnagrafica opeAnagrafica) {
        this.fornToOpeAnas.remove(opeAnagrafica);
        opeAnagrafica.getOpeAnaToForns().remove(this);
        return this;
    }

    public void setFornToOpeAnas(Set<OpeAnagrafica> opeAnagraficas) {
        this.fornToOpeAnas = opeAnagraficas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fornitori)) {
            return false;
        }
        return id != null && id.equals(((Fornitori) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Fornitori{" +
            "id=" + getId() +
            ", registroImprese='" + getRegistroImprese() + "'" +
            ", numIscrizione='" + getNumIscrizione() + "'" +
            ", idAzienda=" + getIdAzienda() +
            ", nomeFornitore='" + getNomeFornitore() + "'" +
            ", piva='" + getPiva() + "'" +
            ", codiFisc='" + getCodiFisc() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
