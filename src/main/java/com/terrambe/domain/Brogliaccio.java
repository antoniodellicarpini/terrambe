package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A Brogliaccio.
 */
@Entity
@Table(name = "brogliaccio")
@DynamicUpdate
public class Brogliaccio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome_comune", nullable = false)
    private String nomeComune;

    @NotNull
    @Column(name = "cod_naz", nullable = false)
    private String codNaz;

    @NotNull
    @Column(name = "sez", nullable = false)
    private String sez;

    @NotNull
    @Column(name = "foglio", nullable = false)
    private String foglio;

    @NotNull
    @Column(name = "part", nullable = false)
    private String part;

    @Column(name = "sub")
    private String sub;

    @NotNull
    @Column(name = "sup_cat", nullable = false)
    private String supCat;

    @NotNull
    @Column(name = "sup_gra", nullable = false)
    private String supGra;

    @NotNull
    @Column(name = "conduz", nullable = false)
    private String conduz;

    @NotNull
    @Column(name = "c_p", nullable = false)
    private String cP;

    @NotNull
    @Column(name = "data_inizio", nullable = false)
    private LocalDate dataInizio;

    @NotNull
    @Column(name = "data_fine", nullable = false)
    private LocalDate dataFine;

    @NotNull
    @Column(name = "sup_util", nullable = false)
    private String supUtil;

    @NotNull
    @Column(name = "sup_eleg", nullable = false)
    private String supEleg;

    @NotNull
    @Column(name = "macrouso", nullable = false)
    private String macrouso;

    @NotNull
    @Column(name = "occupazione", nullable = false)
    private String occupazione;

    @NotNull
    @Column(name = "destinazione", nullable = false)
    private String destinazione;

    @NotNull
    @Column(name = "uso", nullable = false)
    private String uso;

    @NotNull
    @Column(name = "qualita", nullable = false)
    private String qualita;

    @NotNull
    @Column(name = "varieta", nullable = false)
    private String varieta;

    @NotNull
    @Column(name = "data_importazione", nullable = false)
    private Instant dataImportazione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("aziAnaToBrogls")
    private AziAnagrafica broglToAziAna;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeComune() {
        return nomeComune;
    }

    public Brogliaccio nomeComune(String nomeComune) {
        this.nomeComune = nomeComune;
        return this;
    }

    public void setNomeComune(String nomeComune) {
        this.nomeComune = nomeComune;
    }

    public String getCodNaz() {
        return codNaz;
    }

    public Brogliaccio codNaz(String codNaz) {
        this.codNaz = codNaz;
        return this;
    }

    public void setCodNaz(String codNaz) {
        this.codNaz = codNaz;
    }

    public String getSez() {
        return sez;
    }

    public Brogliaccio sez(String sez) {
        this.sez = sez;
        return this;
    }

    public void setSez(String sez) {
        this.sez = sez;
    }

    public String getFoglio() {
        return foglio;
    }

    public Brogliaccio foglio(String foglio) {
        this.foglio = foglio;
        return this;
    }

    public void setFoglio(String foglio) {
        this.foglio = foglio;
    }

    public String getPart() {
        return part;
    }

    public Brogliaccio part(String part) {
        this.part = part;
        return this;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getSub() {
        return sub;
    }

    public Brogliaccio sub(String sub) {
        this.sub = sub;
        return this;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getSupCat() {
        return supCat;
    }

    public Brogliaccio supCat(String supCat) {
        this.supCat = supCat;
        return this;
    }

    public void setSupCat(String supCat) {
        this.supCat = supCat;
    }

    public String getSupGra() {
        return supGra;
    }

    public Brogliaccio supGra(String supGra) {
        this.supGra = supGra;
        return this;
    }

    public void setSupGra(String supGra) {
        this.supGra = supGra;
    }

    public String getConduz() {
        return conduz;
    }

    public Brogliaccio conduz(String conduz) {
        this.conduz = conduz;
        return this;
    }

    public void setConduz(String conduz) {
        this.conduz = conduz;
    }

    public String getcP() {
        return cP;
    }

    public Brogliaccio cP(String cP) {
        this.cP = cP;
        return this;
    }

    public void setcP(String cP) {
        this.cP = cP;
    }

    public LocalDate getDataInizio() {
        return dataInizio;
    }

    public Brogliaccio dataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
        return this;
    }

    public void setDataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDate getDataFine() {
        return dataFine;
    }

    public Brogliaccio dataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
        return this;
    }

    public void setDataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
    }

    public String getSupUtil() {
        return supUtil;
    }

    public Brogliaccio supUtil(String supUtil) {
        this.supUtil = supUtil;
        return this;
    }

    public void setSupUtil(String supUtil) {
        this.supUtil = supUtil;
    }

    public String getSupEleg() {
        return supEleg;
    }

    public Brogliaccio supEleg(String supEleg) {
        this.supEleg = supEleg;
        return this;
    }

    public void setSupEleg(String supEleg) {
        this.supEleg = supEleg;
    }

    public String getMacrouso() {
        return macrouso;
    }

    public Brogliaccio macrouso(String macrouso) {
        this.macrouso = macrouso;
        return this;
    }

    public void setMacrouso(String macrouso) {
        this.macrouso = macrouso;
    }

    public String getOccupazione() {
        return occupazione;
    }

    public Brogliaccio occupazione(String occupazione) {
        this.occupazione = occupazione;
        return this;
    }

    public void setOccupazione(String occupazione) {
        this.occupazione = occupazione;
    }

    public String getDestinazione() {
        return destinazione;
    }

    public Brogliaccio destinazione(String destinazione) {
        this.destinazione = destinazione;
        return this;
    }

    public void setDestinazione(String destinazione) {
        this.destinazione = destinazione;
    }

    public String getUso() {
        return uso;
    }

    public Brogliaccio uso(String uso) {
        this.uso = uso;
        return this;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public String getQualita() {
        return qualita;
    }

    public Brogliaccio qualita(String qualita) {
        this.qualita = qualita;
        return this;
    }

    public void setQualita(String qualita) {
        this.qualita = qualita;
    }

    public String getVarieta() {
        return varieta;
    }

    public Brogliaccio varieta(String varieta) {
        this.varieta = varieta;
        return this;
    }

    public void setVarieta(String varieta) {
        this.varieta = varieta;
    }

    public Instant getDataImportazione() {
        return dataImportazione;
    }

    public Brogliaccio dataImportazione(Instant dataImportazione) {
        this.dataImportazione = dataImportazione;
        return this;
    }

    public void setDataImportazione(Instant dataImportazione) {
        this.dataImportazione = dataImportazione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Brogliaccio dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Brogliaccio dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Brogliaccio userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Brogliaccio userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Brogliaccio note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public AziAnagrafica getBroglToAziAna() {
        return broglToAziAna;
    }

    public Brogliaccio broglToAziAna(AziAnagrafica aziAnagrafica) {
        this.broglToAziAna = aziAnagrafica;
        return this;
    }

    public void setBroglToAziAna(AziAnagrafica aziAnagrafica) {
        this.broglToAziAna = aziAnagrafica;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Brogliaccio)) {
            return false;
        }
        return id != null && id.equals(((Brogliaccio) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Brogliaccio{" +
            "id=" + getId() +
            ", nomeComune='" + getNomeComune() + "'" +
            ", codNaz='" + getCodNaz() + "'" +
            ", sez='" + getSez() + "'" +
            ", foglio='" + getFoglio() + "'" +
            ", part='" + getPart() + "'" +
            ", sub='" + getSub() + "'" +
            ", supCat='" + getSupCat() + "'" +
            ", supGra='" + getSupGra() + "'" +
            ", conduz='" + getConduz() + "'" +
            ", cP='" + getcP() + "'" +
            ", dataInizio='" + getDataInizio() + "'" +
            ", dataFine='" + getDataFine() + "'" +
            ", supUtil='" + getSupUtil() + "'" +
            ", supEleg='" + getSupEleg() + "'" +
            ", macrouso='" + getMacrouso() + "'" +
            ", occupazione='" + getOccupazione() + "'" +
            ", destinazione='" + getDestinazione() + "'" +
            ", uso='" + getUso() + "'" +
            ", qualita='" + getQualita() + "'" +
            ", varieta='" + getVarieta() + "'" +
            ", dataImportazione='" + getDataImportazione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
