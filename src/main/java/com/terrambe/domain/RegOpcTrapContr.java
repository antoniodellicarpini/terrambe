package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegOpcTrapContr.
 */
@Entity
@Table(name = "reg_opc_trap_contr")
@DynamicUpdate
public class RegOpcTrapContr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_controllo")
    private LocalDate dataControllo;

    @Column(name = "data_sosti_ferom")
    private LocalDate dataSostiFerom;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "parametri")
    private String parametri;

    @ManyToOne
    @JsonIgnoreProperties("opcTrapToTrapContrs")
    private RegOpcTrap trapContrToOpcTrap;

    @ManyToOne
    @JsonIgnoreProperties("trapContrToTrapContrTipos")
    private TrapContrTipo trapContrTipoToTrapContr;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcTrapContr idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcTrapContr idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataControllo() {
        return dataControllo;
    }

    public RegOpcTrapContr dataControllo(LocalDate dataControllo) {
        this.dataControllo = dataControllo;
        return this;
    }

    public void setDataControllo(LocalDate dataControllo) {
        this.dataControllo = dataControllo;
    }

    public LocalDate getDataSostiFerom() {
        return dataSostiFerom;
    }

    public RegOpcTrapContr dataSostiFerom(LocalDate dataSostiFerom) {
        this.dataSostiFerom = dataSostiFerom;
        return this;
    }

    public void setDataSostiFerom(LocalDate dataSostiFerom) {
        this.dataSostiFerom = dataSostiFerom;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcTrapContr dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcTrapContr dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcTrapContr userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcTrapContr userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcTrapContr note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getParametri() {
        return parametri;
    }

    public RegOpcTrapContr parametri(String parametri) {
        this.parametri = parametri;
        return this;
    }

    public void setParametri(String parametri) {
        this.parametri = parametri;
    }

    public RegOpcTrap getTrapContrToOpcTrap() {
        return trapContrToOpcTrap;
    }

    public RegOpcTrapContr trapContrToOpcTrap(RegOpcTrap regOpcTrap) {
        this.trapContrToOpcTrap = regOpcTrap;
        return this;
    }

    public void setTrapContrToOpcTrap(RegOpcTrap regOpcTrap) {
        this.trapContrToOpcTrap = regOpcTrap;
    }

    public TrapContrTipo getTrapContrTipoToTrapContr() {
        return trapContrTipoToTrapContr;
    }

    public RegOpcTrapContr trapContrTipoToTrapContr(TrapContrTipo trapContrTipo) {
        this.trapContrTipoToTrapContr = trapContrTipo;
        return this;
    }

    public void setTrapContrTipoToTrapContr(TrapContrTipo trapContrTipo) {
        this.trapContrTipoToTrapContr = trapContrTipo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcTrapContr)) {
            return false;
        }
        return id != null && id.equals(((RegOpcTrapContr) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcTrapContr{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataControllo='" + getDataControllo() + "'" +
            ", dataSostiFerom='" + getDataSostiFerom() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            ", parametri='" + getParametri() + "'" +
            "}";
    }
}
