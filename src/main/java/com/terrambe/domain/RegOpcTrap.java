package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

//MODIFICA -----------------------------------------
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcTrap.
 */
@Entity
@Table(name = "reg_opc_trap")
@DynamicUpdate
public class RegOpcTrap implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_operazione")
    private LocalDate dataOperazione;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "id_coltura")
    private Long idColtura;

    @Column(name = "id_operatore")
    private Long idOperatore;

    @Column(name = "id_mezzo")
    private Long idMezzo;

    @Column(name = "id_trappola")
    private Long idTrappola;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "appz_to_opc_trap_id")
@JsonIgnoreProperties("appzToOpcTrap")
//@OneToMany(mappedBy = "appzToOpcTrap")
private Set<RegOpcTrapAppz> opcTrapToAppzs = new HashSet<>();

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "trap_contr_to_opc_trap_id")
@JsonIgnoreProperties("trapContrToOpcTrap")
//@OneToMany(mappedBy = "trapContrToOpcTrap")
private Set<RegOpcTrapContr> opcTrapToTrapContrs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcTrap idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcTrap idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataOperazione() {
        return dataOperazione;
    }

    public RegOpcTrap dataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
        return this;
    }

    public void setDataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcTrap tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcTrap idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public Long getIdOperatore() {
        return idOperatore;
    }

    public RegOpcTrap idOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
        return this;
    }

    public void setIdOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
    }

    public Long getIdMezzo() {
        return idMezzo;
    }

    public RegOpcTrap idMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
        return this;
    }

    public void setIdMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
    }

    public Long getIdTrappola() {
        return idTrappola;
    }

    public RegOpcTrap idTrappola(Long idTrappola) {
        this.idTrappola = idTrappola;
        return this;
    }

    public void setIdTrappola(Long idTrappola) {
        this.idTrappola = idTrappola;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcTrap dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcTrap dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcTrap userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcTrap userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcTrap note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcTrapAppz> getOpcTrapToAppzs() {
        return opcTrapToAppzs;
    }

    public RegOpcTrap opcTrapToAppzs(Set<RegOpcTrapAppz> regOpcTrapAppzs) {
        this.opcTrapToAppzs = regOpcTrapAppzs;
        return this;
    }

    public RegOpcTrap addOpcTrapToAppz(RegOpcTrapAppz regOpcTrapAppz) {
        this.opcTrapToAppzs.add(regOpcTrapAppz);
        regOpcTrapAppz.setAppzToOpcTrap(this);
        return this;
    }

    public RegOpcTrap removeOpcTrapToAppz(RegOpcTrapAppz regOpcTrapAppz) {
        this.opcTrapToAppzs.remove(regOpcTrapAppz);
        regOpcTrapAppz.setAppzToOpcTrap(null);
        return this;
    }

    public void setOpcTrapToAppzs(Set<RegOpcTrapAppz> regOpcTrapAppzs) {
        this.opcTrapToAppzs = regOpcTrapAppzs;
    }

    public Set<RegOpcTrapContr> getOpcTrapToTrapContrs() {
        return opcTrapToTrapContrs;
    }

    public RegOpcTrap opcTrapToTrapContrs(Set<RegOpcTrapContr> regOpcTrapContrs) {
        this.opcTrapToTrapContrs = regOpcTrapContrs;
        return this;
    }

    public RegOpcTrap addOpcTrapToTrapContr(RegOpcTrapContr regOpcTrapContr) {
        this.opcTrapToTrapContrs.add(regOpcTrapContr);
        regOpcTrapContr.setTrapContrToOpcTrap(this);
        return this;
    }

    public RegOpcTrap removeOpcTrapToTrapContr(RegOpcTrapContr regOpcTrapContr) {
        this.opcTrapToTrapContrs.remove(regOpcTrapContr);
        regOpcTrapContr.setTrapContrToOpcTrap(null);
        return this;
    }

    public void setOpcTrapToTrapContrs(Set<RegOpcTrapContr> regOpcTrapContrs) {
        this.opcTrapToTrapContrs = regOpcTrapContrs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcTrap)) {
            return false;
        }
        return id != null && id.equals(((RegOpcTrap) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcTrap{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataOperazione='" + getDataOperazione() + "'" +
            ", tempoImpiegato='" + getTempoImpiegato() + "'" +
            ", idColtura=" + getIdColtura() +
            ", idOperatore=" + getIdOperatore() +
            ", idMezzo=" + getIdMezzo() +
            ", idTrappola=" + getIdTrappola() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
