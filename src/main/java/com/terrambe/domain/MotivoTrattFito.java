package com.terrambe.domain;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A MotivoTrattFito.
 */
@Entity
@Table(name = "motivo_tratt_fito")
@DynamicUpdate
public class MotivoTrattFito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descrizione")
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @OneToMany(mappedBy = "trattFitoToMotivo")
    private Set<RegOpcFito> motivoToTrattFitos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public MotivoTrattFito descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public MotivoTrattFito dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public MotivoTrattFito dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public MotivoTrattFito userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public MotivoTrattFito userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public Set<RegOpcFito> getMotivoToTrattFitos() {
        return motivoToTrattFitos;
    }

    public MotivoTrattFito motivoToTrattFitos(Set<RegOpcFito> regOpcFitos) {
        this.motivoToTrattFitos = regOpcFitos;
        return this;
    }

    public MotivoTrattFito addMotivoToTrattFito(RegOpcFito regOpcFito) {
        this.motivoToTrattFitos.add(regOpcFito);
        regOpcFito.setTrattFitoToMotivo(this);
        return this;
    }

    public MotivoTrattFito removeMotivoToTrattFito(RegOpcFito regOpcFito) {
        this.motivoToTrattFitos.remove(regOpcFito);
        regOpcFito.setTrattFitoToMotivo(null);
        return this;
    }

    public void setMotivoToTrattFitos(Set<RegOpcFito> regOpcFitos) {
        this.motivoToTrattFitos = regOpcFitos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MotivoTrattFito)) {
            return false;
        }
        return id != null && id.equals(((MotivoTrattFito) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MotivoTrattFito{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            "}";
    }
}
