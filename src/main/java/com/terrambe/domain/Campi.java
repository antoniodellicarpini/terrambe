package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Campi.
 */
@Entity
@Table(name = "campi")
@DynamicUpdate
public class Campi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @NotNull
    @Column(name = "nome_campo", nullable = false)
    private String nomeCampo;

    @NotNull
    @Column(name = "superficie_utilizzata", nullable = false)
    private Float superficieUtilizzata;

    @NotNull
    @Column(name = "sup_catastale", nullable = false)
    private Float supCatastale;

    @NotNull
    @Column(name = "sup_utilizzabile", nullable = false)
    private Float supUtilizzabile;

    @NotNull
    @Column(name = "uuid_esri", nullable = false)
    private String uuidEsri;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    //MODIFICA -----------------------------------------
    @OneToMany(mappedBy = "aziSupToCampi", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("appezToCampi")
    private Set<AziSuperficieParticella> campiToAziSups = new HashSet<>();

    //MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "appez_to_campi_id")
    @JsonIgnoreProperties("appezToCampi")
    //@OneToMany(mappedBy = "appezToCampi")
    private Set<Appezzamenti> campiToAppezs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("campis")
    private UniAnagrafica campiuni;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public Campi idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public String getNomeCampo() {
        return nomeCampo;
    }

    public Campi nomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
        return this;
    }

    public void setNomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public Float getSuperficieUtilizzata() {
        return superficieUtilizzata;
    }

    public Campi superficieUtilizzata(Float superficieUtilizzata) {
        this.superficieUtilizzata = superficieUtilizzata;
        return this;
    }

    public void setSuperficieUtilizzata(Float superficieUtilizzata) {
        this.superficieUtilizzata = superficieUtilizzata;
    }

    public Float getSupCatastale() {
        return supCatastale;
    }

    public Campi supCatastale(Float supCatastale) {
        this.supCatastale = supCatastale;
        return this;
    }

    public void setSupCatastale(Float supCatastale) {
        this.supCatastale = supCatastale;
    }

    public Float getSupUtilizzabile() {
        return supUtilizzabile;
    }

    public Campi supUtilizzabile(Float supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
        return this;
    }

    public void setSupUtilizzabile(Float supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
    }

    public String getUuidEsri() {
        return uuidEsri;
    }

    public Campi uuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
        return this;
    }

    public void setUuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Campi dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Campi dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Campi userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Campi userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Campi note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<AziSuperficieParticella> getCampiToAziSups() {
        return campiToAziSups;
    }

    public Campi campiToAziSups(Set<AziSuperficieParticella> aziSuperficieParticellas) {
        this.campiToAziSups = aziSuperficieParticellas;
        return this;
    }

    public Campi addCampiToAziSup(AziSuperficieParticella aziSuperficieParticella) {
        this.campiToAziSups.add(aziSuperficieParticella);
        aziSuperficieParticella.setAziSupToCampi(this);
        return this;
    }

    public Campi removeCampiToAziSup(AziSuperficieParticella aziSuperficieParticella) {
        this.campiToAziSups.remove(aziSuperficieParticella);
        aziSuperficieParticella.setAziSupToCampi(null);
        return this;
    }

    public void setCampiToAziSups(Set<AziSuperficieParticella> aziSuperficieParticellas) {
        this.campiToAziSups = aziSuperficieParticellas;
    }

    public Set<Appezzamenti> getCampiToAppezs() {
        return campiToAppezs;
    }

    public Campi campiToAppezs(Set<Appezzamenti> appezzamentis) {
        this.campiToAppezs = appezzamentis;
        return this;
    }

    public Campi addCampiToAppez(Appezzamenti appezzamenti) {
        this.campiToAppezs.add(appezzamenti);
        appezzamenti.setAppezToCampi(this);
        return this;
    }

    public Campi removeCampiToAppez(Appezzamenti appezzamenti) {
        this.campiToAppezs.remove(appezzamenti);
        appezzamenti.setAppezToCampi(null);
        return this;
    }

    public void setCampiToAppezs(Set<Appezzamenti> appezzamentis) {
        this.campiToAppezs = appezzamentis;
    }

    public UniAnagrafica getCampiuni() {
        return campiuni;
    }

    public Campi campiuni(UniAnagrafica uniAnagrafica) {
        this.campiuni = uniAnagrafica;
        return this;
    }

    public void setCampiuni(UniAnagrafica uniAnagrafica) {
        this.campiuni = uniAnagrafica;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Campi)) {
            return false;
        }
        return id != null && id.equals(((Campi) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Campi{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", nomeCampo='" + getNomeCampo() + "'" +
            ", superficieUtilizzata=" + getSuperficieUtilizzata() +
            ", supCatastale=" + getSupCatastale() +
            ", supUtilizzabile=" + getSupUtilizzabile() +
            ", uuidEsri='" + getUuidEsri() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
