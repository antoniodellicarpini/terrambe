package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cliente.
 */
@Entity
@Table(name = "cliente")
@DynamicUpdate
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @NotNull
    @Size(min = 11, max = 16)
    @Column(name = "cuaa", length = 16, nullable = false)
    private String cuaa;

    @NotNull
    @Size(min = 11, max = 11)
    @Column(name = "partitaiva", length = 11, nullable = false)
    private String partitaiva;

    @NotNull
    @Size(min = 16, max = 16)
    @Column(name = "codicefiscale", length = 16, nullable = false)
    private String codicefiscale;

    @NotNull
    @Column(name = "codice_ateco", nullable = false)
    private String codiceAteco;

    @NotNull
    @Column(name = "ragione_sociale", nullable = false)
    private String ragioneSociale;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali  = LocalDate.of(9999,12,31);

    @Column(name = "attivo")
    private Boolean attivo;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "ind_to_cli_id")
@JsonIgnoreProperties("indToCli")
//@OneToMany(mappedBy = "indToCli")
private Set<IndAziendaCli> cliToInds = new HashSet<>();

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "rec_to_cli_id")
@JsonIgnoreProperties("recToCli")
//@OneToMany(mappedBy = "recToCli")
private Set<RecAziendaCli> cliToRecs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("clientes")
    private AziFormaGiuridica naturaGiuridica;

    @ManyToOne
    @JsonIgnoreProperties("aziAnaToClis")
    private AziAnagrafica cliToAziAna;

    @ManyToOne
    @JsonIgnoreProperties("cliRapToCliAnas")
    private AziRappresentanteCli cliAnaToCliRap;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public Cliente idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public Cliente idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public String getCuaa() {
        return cuaa;
    }

    public Cliente cuaa(String cuaa) {
        this.cuaa = cuaa;
        return this;
    }

    public void setCuaa(String cuaa) {
        this.cuaa = cuaa;
    }

    public String getPartitaiva() {
        return partitaiva;
    }

    public Cliente partitaiva(String partitaiva) {
        this.partitaiva = partitaiva;
        return this;
    }

    public void setPartitaiva(String partitaiva) {
        this.partitaiva = partitaiva;
    }

    public String getCodicefiscale() {
        return codicefiscale;
    }

    public Cliente codicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
        return this;
    }

    public void setCodicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public String getCodiceAteco() {
        return codiceAteco;
    }

    public Cliente codiceAteco(String codiceAteco) {
        this.codiceAteco = codiceAteco;
        return this;
    }

    public void setCodiceAteco(String codiceAteco) {
        this.codiceAteco = codiceAteco;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public Cliente ragioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
        return this;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Cliente dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Cliente dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Boolean isAttivo() {
        return attivo;
    }

    public Cliente attivo(Boolean attivo) {
        this.attivo = attivo;
        return this;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Cliente userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Cliente userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Cliente note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IndAziendaCli> getCliToInds() {
        return cliToInds;
    }

    public Cliente cliToInds(Set<IndAziendaCli> indAziendaClis) {
        this.cliToInds = indAziendaClis;
        return this;
    }

    public Cliente addCliToInd(IndAziendaCli indAziendaCli) {
        this.cliToInds.add(indAziendaCli);
        indAziendaCli.setIndToCli(this);
        return this;
    }

    public Cliente removeCliToInd(IndAziendaCli indAziendaCli) {
        this.cliToInds.remove(indAziendaCli);
        indAziendaCli.setIndToCli(null);
        return this;
    }

    public void setCliToInds(Set<IndAziendaCli> indAziendaClis) {
        this.cliToInds = indAziendaClis;
    }

    public Set<RecAziendaCli> getCliToRecs() {
        return cliToRecs;
    }

    public Cliente cliToRecs(Set<RecAziendaCli> recAziendaClis) {
        this.cliToRecs = recAziendaClis;
        return this;
    }

    public Cliente addCliToRec(RecAziendaCli recAziendaCli) {
        this.cliToRecs.add(recAziendaCli);
        recAziendaCli.setRecToCli(this);
        return this;
    }

    public Cliente removeCliToRec(RecAziendaCli recAziendaCli) {
        this.cliToRecs.remove(recAziendaCli);
        recAziendaCli.setRecToCli(null);
        return this;
    }

    public void setCliToRecs(Set<RecAziendaCli> recAziendaClis) {
        this.cliToRecs = recAziendaClis;
    }

    public AziFormaGiuridica getNaturaGiuridica() {
        return naturaGiuridica;
    }

    public Cliente naturaGiuridica(AziFormaGiuridica aziFormaGiuridica) {
        this.naturaGiuridica = aziFormaGiuridica;
        return this;
    }

    public void setNaturaGiuridica(AziFormaGiuridica aziFormaGiuridica) {
        this.naturaGiuridica = aziFormaGiuridica;
    }

    public AziAnagrafica getCliToAziAna() {
        return cliToAziAna;
    }

    public Cliente cliToAziAna(AziAnagrafica aziAnagrafica) {
        this.cliToAziAna = aziAnagrafica;
        return this;
    }

    public void setCliToAziAna(AziAnagrafica aziAnagrafica) {
        this.cliToAziAna = aziAnagrafica;
    }

    public AziRappresentanteCli getCliAnaToCliRap() {
        return cliAnaToCliRap;
    }

    public Cliente cliAnaToCliRap(AziRappresentanteCli aziRappresentanteCli) {
        this.cliAnaToCliRap = aziRappresentanteCli;
        return this;
    }

    public void setCliAnaToCliRap(AziRappresentanteCli aziRappresentanteCli) {
        this.cliAnaToCliRap = aziRappresentanteCli;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cliente)) {
            return false;
        }
        return id != null && id.equals(((Cliente) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cliente{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", cuaa='" + getCuaa() + "'" +
            ", partitaiva='" + getPartitaiva() + "'" +
            ", codicefiscale='" + getCodicefiscale() + "'" +
            ", codiceAteco='" + getCodiceAteco() + "'" +
            ", ragioneSociale='" + getRagioneSociale() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", attivo='" + isAttivo() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
