package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A TabellaRaccordo.
 */
@Entity
@Table(name = "tabella_raccordo")
@DynamicUpdate
public class TabellaRaccordo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("sitoToRaccordos")
    private Sito raccordoToSito;

    @ManyToOne
    @JsonIgnoreProperties("coltBdfToRaccordos")
    private ColtureBDF raccordoToColtBdf;

    @ManyToOne
    @JsonIgnoreProperties("dosagToRaccordos")
    private Dosaggio raccordoToDosag;

    @ManyToOne
    @JsonIgnoreProperties("etiFitoToRaccordos")
    private EtichettaFito raccordoToEtiFito;

    @ManyToOne
    @JsonIgnoreProperties("faseFenoToRaccordos")
    private FaseFenologica raccordoToFaseFeno;

    @ManyToOne
    @JsonIgnoreProperties("tipoDistrToRaccordos")
    private TipoDistConc raccordoToTipoDistr;

    @ManyToOne
    @JsonIgnoreProperties("avversToRaccordos")
    private Avversita raccordoToAvvers;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public TabellaRaccordo tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public TabellaRaccordo operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public TabellaRaccordo ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TabellaRaccordo dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TabellaRaccordo dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TabellaRaccordo userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TabellaRaccordo userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public TabellaRaccordo note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Sito getRaccordoToSito() {
        return raccordoToSito;
    }

    public TabellaRaccordo raccordoToSito(Sito sito) {
        this.raccordoToSito = sito;
        return this;
    }

    public void setRaccordoToSito(Sito sito) {
        this.raccordoToSito = sito;
    }

    public ColtureBDF getRaccordoToColtBdf() {
        return raccordoToColtBdf;
    }

    public TabellaRaccordo raccordoToColtBdf(ColtureBDF coltureBDF) {
        this.raccordoToColtBdf = coltureBDF;
        return this;
    }

    public void setRaccordoToColtBdf(ColtureBDF coltureBDF) {
        this.raccordoToColtBdf = coltureBDF;
    }

    public Dosaggio getRaccordoToDosag() {
        return raccordoToDosag;
    }

    public TabellaRaccordo raccordoToDosag(Dosaggio dosaggio) {
        this.raccordoToDosag = dosaggio;
        return this;
    }

    public void setRaccordoToDosag(Dosaggio dosaggio) {
        this.raccordoToDosag = dosaggio;
    }

    public EtichettaFito getRaccordoToEtiFito() {
        return raccordoToEtiFito;
    }

    public TabellaRaccordo raccordoToEtiFito(EtichettaFito etichettaFito) {
        this.raccordoToEtiFito = etichettaFito;
        return this;
    }

    public void setRaccordoToEtiFito(EtichettaFito etichettaFito) {
        this.raccordoToEtiFito = etichettaFito;
    }

    public FaseFenologica getRaccordoToFaseFeno() {
        return raccordoToFaseFeno;
    }

    public TabellaRaccordo raccordoToFaseFeno(FaseFenologica faseFenologica) {
        this.raccordoToFaseFeno = faseFenologica;
        return this;
    }

    public void setRaccordoToFaseFeno(FaseFenologica faseFenologica) {
        this.raccordoToFaseFeno = faseFenologica;
    }

    public TipoDistConc getRaccordoToTipoDistr() {
        return raccordoToTipoDistr;
    }

    public TabellaRaccordo raccordoToTipoDistr(TipoDistConc tipoDistConc) {
        this.raccordoToTipoDistr = tipoDistConc;
        return this;
    }

    public void setRaccordoToTipoDistr(TipoDistConc tipoDistConc) {
        this.raccordoToTipoDistr = tipoDistConc;
    }

    public Avversita getRaccordoToAvvers() {
        return raccordoToAvvers;
    }

    public TabellaRaccordo raccordoToAvvers(Avversita avversita) {
        this.raccordoToAvvers = avversita;
        return this;
    }

    public void setRaccordoToAvvers(Avversita avversita) {
        this.raccordoToAvvers = avversita;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TabellaRaccordo)) {
            return false;
        }
        return id != null && id.equals(((TabellaRaccordo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TabellaRaccordo{" +
            "id=" + getId() +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
