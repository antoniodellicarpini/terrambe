package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegCaricoFitoFarmaco.
 */
@Entity
@Table(name = "reg_carico_fito_farmaco")
@DynamicUpdate
public class RegCaricoFitoFarmaco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @NotNull
    @Column(name = "quantita", nullable = false)
    private Double quantita;

    @NotNull
    @Column(name = "prezzo_unitario", nullable = false)
    private Float prezzoUnitario;

    @NotNull
    @Column(name = "data_carico", nullable = false)
    private LocalDate dataCarico;

    @NotNull
    @Column(name = "ddt", nullable = false)
    private String ddt;

    @NotNull
    @Column(name = "data_scadenza", nullable = false)
    private LocalDate dataScadenza;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("regCaricoFitoFarmacos")
    private MagUbicazione magUbicazione;

    @ManyToOne
    @JsonIgnoreProperties("regCaricoFitoFarmacos")
    private EtichettaFito etichettafito;

    @ManyToOne
    @JsonIgnoreProperties("fornitoriToCaricoFitos")
    private Fornitori caricoFitoToFornitori;

    @ManyToOne
    @JsonIgnoreProperties("tipoRegToRegFitos")
    private TipoRegMag regFitoToTipoReg;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegCaricoFitoFarmaco idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegCaricoFitoFarmaco idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Double getQuantita() {
        return quantita;
    }

    public RegCaricoFitoFarmaco quantita(Double quantita) {
        this.quantita = quantita;
        return this;
    }

    public void setQuantita(Double quantita) {
        this.quantita = quantita;
    }

    public Float getPrezzoUnitario() {
        return prezzoUnitario;
    }

    public RegCaricoFitoFarmaco prezzoUnitario(Float prezzoUnitario) {
        this.prezzoUnitario = prezzoUnitario;
        return this;
    }

    public void setPrezzoUnitario(Float prezzoUnitario) {
        this.prezzoUnitario = prezzoUnitario;
    }

    public LocalDate getDataCarico() {
        return dataCarico;
    }

    public RegCaricoFitoFarmaco dataCarico(LocalDate dataCarico) {
        this.dataCarico = dataCarico;
        return this;
    }

    public void setDataCarico(LocalDate dataCarico) {
        this.dataCarico = dataCarico;
    }

    public String getDdt() {
        return ddt;
    }

    public RegCaricoFitoFarmaco ddt(String ddt) {
        this.ddt = ddt;
        return this;
    }

    public void setDdt(String ddt) {
        this.ddt = ddt;
    }

    public LocalDate getDataScadenza() {
        return dataScadenza;
    }

    public RegCaricoFitoFarmaco dataScadenza(LocalDate dataScadenza) {
        this.dataScadenza = dataScadenza;
        return this;
    }

    public void setDataScadenza(LocalDate dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegCaricoFitoFarmaco dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegCaricoFitoFarmaco dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegCaricoFitoFarmaco userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegCaricoFitoFarmaco userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegCaricoFitoFarmaco note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public MagUbicazione getMagUbicazione() {
        return magUbicazione;
    }

    public RegCaricoFitoFarmaco magUbicazione(MagUbicazione magUbicazione) {
        this.magUbicazione = magUbicazione;
        return this;
    }

    public void setMagUbicazione(MagUbicazione magUbicazione) {
        this.magUbicazione = magUbicazione;
    }

    public EtichettaFito getEtichettafito() {
        return etichettafito;
    }

    public RegCaricoFitoFarmaco etichettafito(EtichettaFito etichettaFito) {
        this.etichettafito = etichettaFito;
        return this;
    }

    public void setEtichettafito(EtichettaFito etichettaFito) {
        this.etichettafito = etichettaFito;
    }

    public Fornitori getCaricoFitoToFornitori() {
        return caricoFitoToFornitori;
    }

    public RegCaricoFitoFarmaco caricoFitoToFornitori(Fornitori fornitori) {
        this.caricoFitoToFornitori = fornitori;
        return this;
    }

    public void setCaricoFitoToFornitori(Fornitori fornitori) {
        this.caricoFitoToFornitori = fornitori;
    }

    public TipoRegMag getRegFitoToTipoReg() {
        return regFitoToTipoReg;
    }

    public RegCaricoFitoFarmaco regFitoToTipoReg(TipoRegMag tipoRegMag) {
        this.regFitoToTipoReg = tipoRegMag;
        return this;
    }

    public void setRegFitoToTipoReg(TipoRegMag tipoRegMag) {
        this.regFitoToTipoReg = tipoRegMag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegCaricoFitoFarmaco)) {
            return false;
        }
        return id != null && id.equals(((RegCaricoFitoFarmaco) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegCaricoFitoFarmaco{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", quantita=" + getQuantita() +
            ", prezzoUnitario=" + getPrezzoUnitario() +
            ", dataCarico='" + getDataCarico() + "'" +
            ", ddt='" + getDdt() + "'" +
            ", dataScadenza='" + getDataScadenza() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
