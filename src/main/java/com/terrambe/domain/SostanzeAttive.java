package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A SostanzeAttive.
 */
@Entity
@Table(name = "sostanze_attive")
@DynamicUpdate
public class SostanzeAttive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_sostanza")
    private String idSostanza;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "incompToSostAtt")
    private Set<IncompatibilitaSA> sostAttToIncomps = new HashSet<>();

    @OneToMany(mappedBy = "etiSostAttToSostAtt")
    private Set<EtichettaSostanzeAttive> sostAttToEtiSostAtts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdSostanza() {
        return idSostanza;
    }

    public SostanzeAttive idSostanza(String idSostanza) {
        this.idSostanza = idSostanza;
        return this;
    }

    public void setIdSostanza(String idSostanza) {
        this.idSostanza = idSostanza;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public SostanzeAttive descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public SostanzeAttive tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public SostanzeAttive operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public SostanzeAttive ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public SostanzeAttive dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public SostanzeAttive dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public SostanzeAttive userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public SostanzeAttive userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public SostanzeAttive note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IncompatibilitaSA> getSostAttToIncomps() {
        return sostAttToIncomps;
    }

    public SostanzeAttive sostAttToIncomps(Set<IncompatibilitaSA> incompatibilitaSAS) {
        this.sostAttToIncomps = incompatibilitaSAS;
        return this;
    }

    public SostanzeAttive addSostAttToIncomp(IncompatibilitaSA incompatibilitaSA) {
        this.sostAttToIncomps.add(incompatibilitaSA);
        incompatibilitaSA.setIncompToSostAtt(this);
        return this;
    }

    public SostanzeAttive removeSostAttToIncomp(IncompatibilitaSA incompatibilitaSA) {
        this.sostAttToIncomps.remove(incompatibilitaSA);
        incompatibilitaSA.setIncompToSostAtt(null);
        return this;
    }

    public void setSostAttToIncomps(Set<IncompatibilitaSA> incompatibilitaSAS) {
        this.sostAttToIncomps = incompatibilitaSAS;
    }

    public Set<EtichettaSostanzeAttive> getSostAttToEtiSostAtts() {
        return sostAttToEtiSostAtts;
    }

    public SostanzeAttive sostAttToEtiSostAtts(Set<EtichettaSostanzeAttive> etichettaSostanzeAttives) {
        this.sostAttToEtiSostAtts = etichettaSostanzeAttives;
        return this;
    }

    public SostanzeAttive addSostAttToEtiSostAtt(EtichettaSostanzeAttive etichettaSostanzeAttive) {
        this.sostAttToEtiSostAtts.add(etichettaSostanzeAttive);
        etichettaSostanzeAttive.setEtiSostAttToSostAtt(this);
        return this;
    }

    public SostanzeAttive removeSostAttToEtiSostAtt(EtichettaSostanzeAttive etichettaSostanzeAttive) {
        this.sostAttToEtiSostAtts.remove(etichettaSostanzeAttive);
        etichettaSostanzeAttive.setEtiSostAttToSostAtt(null);
        return this;
    }

    public void setSostAttToEtiSostAtts(Set<EtichettaSostanzeAttive> etichettaSostanzeAttives) {
        this.sostAttToEtiSostAtts = etichettaSostanzeAttives;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SostanzeAttive)) {
            return false;
        }
        return id != null && id.equals(((SostanzeAttive) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SostanzeAttive{" +
            "id=" + getId() +
            ", idSostanza='" + getIdSostanza() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
