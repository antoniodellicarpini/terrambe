package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Agea20152020BDF.
 */
@Entity
@Table(name = "agea_20152020_bdf")
public class Agea20152020BDF implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("coltBdfToAgeaBdfs")
    private ColtureBDF ageaBdfToColtBdf;

    @ManyToOne
    @JsonIgnoreProperties("matriceToBdfs")
    private Agea20152020Matrice bdfToMAtrice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ColtureBDF getAgeaBdfToColtBdf() {
        return ageaBdfToColtBdf;
    }

    public Agea20152020BDF ageaBdfToColtBdf(ColtureBDF coltureBDF) {
        this.ageaBdfToColtBdf = coltureBDF;
        return this;
    }

    public void setAgeaBdfToColtBdf(ColtureBDF coltureBDF) {
        this.ageaBdfToColtBdf = coltureBDF;
    }

    public Agea20152020Matrice getBdfToMAtrice() {
        return bdfToMAtrice;
    }

    public Agea20152020BDF bdfToMAtrice(Agea20152020Matrice agea20152020Matrice) {
        this.bdfToMAtrice = agea20152020Matrice;
        return this;
    }

    public void setBdfToMAtrice(Agea20152020Matrice agea20152020Matrice) {
        this.bdfToMAtrice = agea20152020Matrice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agea20152020BDF)) {
            return false;
        }
        return id != null && id.equals(((Agea20152020BDF) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Agea20152020BDF{" +
            "id=" + getId() +
            "}";
    }
}
