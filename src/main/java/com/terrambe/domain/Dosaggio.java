package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Dosaggio.
 */
@Entity
@Table(name = "dosaggio")
@DynamicUpdate
public class Dosaggio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "carenza_c")
    private String carenzaC;

    @Column(name = "carenza_p")
    private String carenzaP;

    @Column(name = "carenza_d")
    private String carenzaD;

    @Column(name = "carenza_o")
    private String carenzaO;

    @Column(name = "carenza_t")
    private String carenzaT;

    @Column(name = "carenza_nd")
    private String carenzaNd;

    @Column(name = "acqua_ha_min")
    private String acquaHaMin;

    @Column(name = "acqua_ha_max")
    private String acquaHaMax;

    @Column(name = "num_max_int")
    private String numMaxInt;

    @Column(name = "rif_max_tratt")
    private String rifMaxTratt;

    @Column(name = "interv_tratt")
    private String intervTratt;

    @Column(name = "interv_tratt_max")
    private String intervTrattMax;

    @Column(name = "scadenza_dosi")
    private String scadenzaDosi;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "epoca_intervento")
    private String epocaIntervento;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "doseToDosag")
    private Set<Dose> dosagToDoses = new HashSet<>();

    @OneToMany(mappedBy = "raccordoToDosag")
    private Set<TabellaRaccordo> dosagToRaccordos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarenzaC() {
        return carenzaC;
    }

    public Dosaggio carenzaC(String carenzaC) {
        this.carenzaC = carenzaC;
        return this;
    }

    public void setCarenzaC(String carenzaC) {
        this.carenzaC = carenzaC;
    }

    public String getCarenzaP() {
        return carenzaP;
    }

    public Dosaggio carenzaP(String carenzaP) {
        this.carenzaP = carenzaP;
        return this;
    }

    public void setCarenzaP(String carenzaP) {
        this.carenzaP = carenzaP;
    }

    public String getCarenzaD() {
        return carenzaD;
    }

    public Dosaggio carenzaD(String carenzaD) {
        this.carenzaD = carenzaD;
        return this;
    }

    public void setCarenzaD(String carenzaD) {
        this.carenzaD = carenzaD;
    }

    public String getCarenzaO() {
        return carenzaO;
    }

    public Dosaggio carenzaO(String carenzaO) {
        this.carenzaO = carenzaO;
        return this;
    }

    public void setCarenzaO(String carenzaO) {
        this.carenzaO = carenzaO;
    }

    public String getCarenzaT() {
        return carenzaT;
    }

    public Dosaggio carenzaT(String carenzaT) {
        this.carenzaT = carenzaT;
        return this;
    }

    public void setCarenzaT(String carenzaT) {
        this.carenzaT = carenzaT;
    }

    public String getCarenzaNd() {
        return carenzaNd;
    }

    public Dosaggio carenzaNd(String carenzaNd) {
        this.carenzaNd = carenzaNd;
        return this;
    }

    public void setCarenzaNd(String carenzaNd) {
        this.carenzaNd = carenzaNd;
    }

    public String getAcquaHaMin() {
        return acquaHaMin;
    }

    public Dosaggio acquaHaMin(String acquaHaMin) {
        this.acquaHaMin = acquaHaMin;
        return this;
    }

    public void setAcquaHaMin(String acquaHaMin) {
        this.acquaHaMin = acquaHaMin;
    }

    public String getAcquaHaMax() {
        return acquaHaMax;
    }

    public Dosaggio acquaHaMax(String acquaHaMax) {
        this.acquaHaMax = acquaHaMax;
        return this;
    }

    public void setAcquaHaMax(String acquaHaMax) {
        this.acquaHaMax = acquaHaMax;
    }

    public String getNumMaxInt() {
        return numMaxInt;
    }

    public Dosaggio numMaxInt(String numMaxInt) {
        this.numMaxInt = numMaxInt;
        return this;
    }

    public void setNumMaxInt(String numMaxInt) {
        this.numMaxInt = numMaxInt;
    }

    public String getRifMaxTratt() {
        return rifMaxTratt;
    }

    public Dosaggio rifMaxTratt(String rifMaxTratt) {
        this.rifMaxTratt = rifMaxTratt;
        return this;
    }

    public void setRifMaxTratt(String rifMaxTratt) {
        this.rifMaxTratt = rifMaxTratt;
    }

    public String getIntervTratt() {
        return intervTratt;
    }

    public Dosaggio intervTratt(String intervTratt) {
        this.intervTratt = intervTratt;
        return this;
    }

    public void setIntervTratt(String intervTratt) {
        this.intervTratt = intervTratt;
    }

    public String getIntervTrattMax() {
        return intervTrattMax;
    }

    public Dosaggio intervTrattMax(String intervTrattMax) {
        this.intervTrattMax = intervTrattMax;
        return this;
    }

    public void setIntervTrattMax(String intervTrattMax) {
        this.intervTrattMax = intervTrattMax;
    }

    public String getScadenzaDosi() {
        return scadenzaDosi;
    }

    public Dosaggio scadenzaDosi(String scadenzaDosi) {
        this.scadenzaDosi = scadenzaDosi;
        return this;
    }

    public void setScadenzaDosi(String scadenzaDosi) {
        this.scadenzaDosi = scadenzaDosi;
    }

    public String getEpocaIntervento() {
        return epocaIntervento;
    }

    public Dosaggio epocaIntervento(String epocaIntervento) {
        this.epocaIntervento = epocaIntervento;
        return this;
    }

    public void setEpocaIntervento(String epocaIntervento) {
        this.epocaIntervento = epocaIntervento;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public Dosaggio tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public Dosaggio operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public Dosaggio ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Dosaggio dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Dosaggio dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Dosaggio userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Dosaggio userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Dosaggio note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Dose> getDosagToDoses() {
        return dosagToDoses;
    }

    public Dosaggio dosagToDoses(Set<Dose> doses) {
        this.dosagToDoses = doses;
        return this;
    }

    public Dosaggio addDosagToDose(Dose dose) {
        this.dosagToDoses.add(dose);
        dose.setDoseToDosag(this);
        return this;
    }

    public Dosaggio removeDosagToDose(Dose dose) {
        this.dosagToDoses.remove(dose);
        dose.setDoseToDosag(null);
        return this;
    }

    public void setDosagToDoses(Set<Dose> doses) {
        this.dosagToDoses = doses;
    }

    public Set<TabellaRaccordo> getDosagToRaccordos() {
        return dosagToRaccordos;
    }

    public Dosaggio dosagToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.dosagToRaccordos = tabellaRaccordos;
        return this;
    }

    public Dosaggio addDosagToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.dosagToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToDosag(this);
        return this;
    }

    public Dosaggio removeDosagToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.dosagToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToDosag(null);
        return this;
    }

    public void setDosagToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.dosagToRaccordos = tabellaRaccordos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Dosaggio)) {
            return false;
        }
        return id != null && id.equals(((Dosaggio) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Dosaggio{" +
            "id=" + getId() +
            ", carenzaC='" + getCarenzaC() + "'" +
            ", carenzaP='" + getCarenzaP() + "'" +
            ", carenzaD='" + getCarenzaD() + "'" +
            ", carenzaO='" + getCarenzaO() + "'" +
            ", carenzaT='" + getCarenzaT() + "'" +
            ", carenzaNd='" + getCarenzaNd() + "'" +
            ", acquaHaMin='" + getAcquaHaMin() + "'" +
            ", acquaHaMax='" + getAcquaHaMax() + "'" +
            ", numMaxInt='" + getNumMaxInt() + "'" +
            ", rifMaxTratt='" + getRifMaxTratt() + "'" +
            ", intervTratt='" + getIntervTratt() + "'" +
            ", intervTrattMax='" + getIntervTrattMax() + "'" +
            ", scadenzaDosi='" + getScadenzaDosi() + "'" +
            ", epocaIntervento='" + getEpocaIntervento() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
