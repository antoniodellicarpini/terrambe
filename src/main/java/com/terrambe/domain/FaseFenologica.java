package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A FaseFenologica.
 */
@Entity
@Table(name = "fase_fenologica")
@DynamicUpdate
public class FaseFenologica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cod_stadio_colt")
    private String codStadioColt;

    @Column(name = "des_stadio_colt")
    private String desStadioColt;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali  = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "raccordoToFaseFeno")
    private Set<TabellaRaccordo> faseFenoToRaccordos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodStadioColt() {
        return codStadioColt;
    }

    public FaseFenologica codStadioColt(String codStadioColt) {
        this.codStadioColt = codStadioColt;
        return this;
    }

    public void setCodStadioColt(String codStadioColt) {
        this.codStadioColt = codStadioColt;
    }

    public String getDesStadioColt() {
        return desStadioColt;
    }

    public FaseFenologica desStadioColt(String desStadioColt) {
        this.desStadioColt = desStadioColt;
        return this;
    }

    public void setDesStadioColt(String desStadioColt) {
        this.desStadioColt = desStadioColt;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public FaseFenologica tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public FaseFenologica operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public FaseFenologica ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public FaseFenologica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public FaseFenologica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public FaseFenologica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public FaseFenologica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public FaseFenologica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<TabellaRaccordo> getFaseFenoToRaccordos() {
        return faseFenoToRaccordos;
    }

    public FaseFenologica faseFenoToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.faseFenoToRaccordos = tabellaRaccordos;
        return this;
    }

    public FaseFenologica addFaseFenoToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.faseFenoToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToFaseFeno(this);
        return this;
    }

    public FaseFenologica removeFaseFenoToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.faseFenoToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToFaseFeno(null);
        return this;
    }

    public void setFaseFenoToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.faseFenoToRaccordos = tabellaRaccordos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FaseFenologica)) {
            return false;
        }
        return id != null && id.equals(((FaseFenologica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FaseFenologica{" +
            "id=" + getId() +
            ", codStadioColt='" + getCodStadioColt() + "'" +
            ", desStadioColt='" + getDesStadioColt() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
