package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcAltro.
 */
@Entity
@Table(name = "reg_opc_altro")
@DynamicUpdate
public class RegOpcAltro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_iniz_opc")
    private LocalDate dataInizOpc;

    @Column(name = "data_fine_opc")
    private LocalDate dataFineOpc;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "id_coltura")
    private Long idColtura;

    @Column(name = "id_operatore")
    private Long idOperatore;

    @Column(name = "id_mezzo")
    private Long idMezzo;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "appz_to_opc_altro_id")
    @JsonIgnoreProperties("appzToOpcAltro")
    //@OneToMany(mappedBy = "appzToOpcAltro")
    private Set<RegOpcAltroAppz> opcAltroToAppzs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tipoToOpcAltros")
    private TipoAltro opcAltroToTipo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcAltro idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcAltro idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataInizOpc() {
        return dataInizOpc;
    }

    public RegOpcAltro dataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
        return this;
    }

    public void setDataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDate getDataFineOpc() {
        return dataFineOpc;
    }

    public RegOpcAltro dataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
        return this;
    }

    public void setDataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcAltro tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcAltro idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public Long getIdOperatore() {
        return idOperatore;
    }

    public RegOpcAltro idOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
        return this;
    }

    public void setIdOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
    }

    public Long getIdMezzo() {
        return idMezzo;
    }

    public RegOpcAltro idMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
        return this;
    }

    public void setIdMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcAltro dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcAltro dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcAltro userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcAltro userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcAltro note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcAltroAppz> getOpcAltroToAppzs() {
        return opcAltroToAppzs;
    }

    public RegOpcAltro opcAltroToAppzs(Set<RegOpcAltroAppz> regOpcAltroAppzs) {
        this.opcAltroToAppzs = regOpcAltroAppzs;
        return this;
    }

    public RegOpcAltro addOpcAltroToAppz(RegOpcAltroAppz regOpcAltroAppz) {
        this.opcAltroToAppzs.add(regOpcAltroAppz);
        regOpcAltroAppz.setAppzToOpcAltro(this);
        return this;
    }

    public RegOpcAltro removeOpcAltroToAppz(RegOpcAltroAppz regOpcAltroAppz) {
        this.opcAltroToAppzs.remove(regOpcAltroAppz);
        regOpcAltroAppz.setAppzToOpcAltro(null);
        return this;
    }

    public void setOpcAltroToAppzs(Set<RegOpcAltroAppz> regOpcAltroAppzs) {
        this.opcAltroToAppzs = regOpcAltroAppzs;
    }

    public TipoAltro getOpcAltroToTipo() {
        return opcAltroToTipo;
    }

    public RegOpcAltro opcAltroToTipo(TipoAltro tipoAltro) {
        this.opcAltroToTipo = tipoAltro;
        return this;
    }

    public void setOpcAltroToTipo(TipoAltro tipoAltro) {
        this.opcAltroToTipo = tipoAltro;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcAltro)) {
            return false;
        }
        return id != null && id.equals(((RegOpcAltro) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcAltro{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataInizOpc='" + getDataInizOpc() + "'" +
            ", dataFineOpc='" + getDataFineOpc() + "'" +
            ", tempoImpiegato='" + getTempoImpiegato() + "'" +
            ", idColtura=" + getIdColtura() +
            ", idOperatore=" + getIdOperatore() +
            ", idMezzo=" + getIdMezzo() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
