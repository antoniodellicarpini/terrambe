package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A TerramColtureMascherate.
 */
@Entity
@Table(name = "terram_colture_mascherate")
@DynamicUpdate
public class TerramColtureMascherate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "codice_colture_mascherata")
    private String codiceColtureMascherata;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("coltBdfToColtMascs")
    private ColtureBDF colMascToColtBdf;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodiceColtureMascherata() {
        return codiceColtureMascherata;
    }

    public TerramColtureMascherate codiceColtureMascherata(String codiceColtureMascherata) {
        this.codiceColtureMascherata = codiceColtureMascherata;
        return this;
    }

    public void setCodiceColtureMascherata(String codiceColtureMascherata) {
        this.codiceColtureMascherata = codiceColtureMascherata;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TerramColtureMascherate dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TerramColtureMascherate dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TerramColtureMascherate userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TerramColtureMascherate userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public TerramColtureMascherate note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ColtureBDF getColMascToColtBdf() {
        return colMascToColtBdf;
    }

    public TerramColtureMascherate colMascToColtBdf(ColtureBDF coltureBDF) {
        this.colMascToColtBdf = coltureBDF;
        return this;
    }

    public void setColMascToColtBdf(ColtureBDF coltureBDF) {
        this.colMascToColtBdf = coltureBDF;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TerramColtureMascherate)) {
            return false;
        }
        return id != null && id.equals(((TerramColtureMascherate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TerramColtureMascherate{" +
            "id=" + getId() +
            ", codiceColtureMascherata='" + getCodiceColtureMascherata() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
