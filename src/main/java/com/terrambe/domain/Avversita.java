package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Avversita.
 */
@Entity
@Table(name = "avversita")
@DynamicUpdate
public class Avversita implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cod_avversita")
    private String codAvversita;

    @Column(name = "nome_sci")
    private String nomeSci;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "raccordoToAvvers")
    private Set<TabellaRaccordo> avversToRaccordos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodAvversita() {
        return codAvversita;
    }

    public Avversita codAvversita(String codAvversita) {
        this.codAvversita = codAvversita;
        return this;
    }

    public void setCodAvversita(String codAvversita) {
        this.codAvversita = codAvversita;
    }

    public String getNomeSci() {
        return nomeSci;
    }

    public Avversita nomeSci(String nomeSci) {
        this.nomeSci = nomeSci;
        return this;
    }

    public void setNomeSci(String nomeSci) {
        this.nomeSci = nomeSci;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public Avversita tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public Avversita operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public Avversita ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Avversita dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Avversita dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Avversita userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Avversita userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Avversita note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<TabellaRaccordo> getAvversToRaccordos() {
        return avversToRaccordos;
    }

    public Avversita avversToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.avversToRaccordos = tabellaRaccordos;
        return this;
    }

    public Avversita addAvversToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.avversToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToAvvers(this);
        return this;
    }

    public Avversita removeAvversToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.avversToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToAvvers(null);
        return this;
    }

    public void setAvversToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.avversToRaccordos = tabellaRaccordos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Avversita)) {
            return false;
        }
        return id != null && id.equals(((Avversita) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Avversita{" +
            "id=" + getId() +
            ", codAvversita='" + getCodAvversita() + "'" +
            ", nomeSci='" + getNomeSci() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
