package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TipologiaPermessoOperatore.
 */
@Entity
@Table(name = "tipologia_permesso_operatore")
@DynamicUpdate
public class TipologiaPermessoOperatore implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;
     
	//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "perm_oper_to_tipo_perm_id")
    //@OneToMany(mappedBy = "permOperToTipoPerm")
    private Set<PermessiOperatore> tipPermToPermOpers = new HashSet<>();	

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public TipologiaPermessoOperatore descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TipologiaPermessoOperatore dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TipologiaPermessoOperatore dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TipologiaPermessoOperatore userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TipologiaPermessoOperatore userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public TipologiaPermessoOperatore note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<PermessiOperatore> getTipPermToPermOpers() {
        return tipPermToPermOpers;
    }

    public TipologiaPermessoOperatore tipPermToPermOpers(Set<PermessiOperatore> permessiOperatores) {
        this.tipPermToPermOpers = permessiOperatores;
        return this;
    }

    public TipologiaPermessoOperatore addTipPermToPermOper(PermessiOperatore permessiOperatore) {
        this.tipPermToPermOpers.add(permessiOperatore);
        permessiOperatore.setPermOperToTipoPerm(this);
        return this;
    }

    public TipologiaPermessoOperatore removeTipPermToPermOper(PermessiOperatore permessiOperatore) {
        this.tipPermToPermOpers.remove(permessiOperatore);
        permessiOperatore.setPermOperToTipoPerm(null);
        return this;
    }

    public void setTipPermToPermOpers(Set<PermessiOperatore> permessiOperatores) {
        this.tipPermToPermOpers = permessiOperatores;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipologiaPermessoOperatore)) {
            return false;
        }
        return id != null && id.equals(((TipologiaPermessoOperatore) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TipologiaPermessoOperatore{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
