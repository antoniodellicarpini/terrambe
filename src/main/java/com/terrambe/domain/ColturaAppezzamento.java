package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ColturaAppezzamento.
 */
@Entity
@Table(name = "coltura_appezzamento")
@DynamicUpdate
public class ColturaAppezzamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "primaria")
    private Boolean primaria;

    @Column(name = "id_azienda")
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @NotNull
    @Column(name = "data_inizio_coltura", nullable = false)
    private LocalDate dataInizioColtura;

    @NotNull
    @Column(name = "data_fine_coltura", nullable = false)
    private LocalDate dataFineColtura;

    @Column(name = "data_fioritura")
    private LocalDate dataFioritura;

    @Column(name = "data_fine_fioritura")
    private LocalDate dataFineFioritura;

    @Column(name = "data_raccolta")
    private LocalDate dataRaccolta;

    @Column(name = "numero_piante")
    private Integer numeroPiante;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    //@JsonIgnoreProperties(value = "appeToColts", allowSetters = true)
    private Appezzamenti coltToAppe;

    @ManyToOne
    //@JsonIgnoreProperties(value = "appeToColts", allowSetters = true)
    private Agea20152020Matrice coltToAgea;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isPrimaria() {
        return primaria;
    }

    public ColturaAppezzamento primaria(Boolean primaria) {
        this.primaria = primaria;
        return this;
    }

    public void setPrimaria(Boolean primaria) {
        this.primaria = primaria;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public ColturaAppezzamento idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public ColturaAppezzamento idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataInizioColtura() {
        return dataInizioColtura;
    }

    public ColturaAppezzamento dataInizioColtura(LocalDate dataInizioColtura) {
        this.dataInizioColtura = dataInizioColtura;
        return this;
    }

    public void setDataInizioColtura(LocalDate dataInizioColtura) {
        this.dataInizioColtura = dataInizioColtura;
    }

    public LocalDate getDataFineColtura() {
        return dataFineColtura;
    }

    public ColturaAppezzamento dataFineColtura(LocalDate dataFineColtura) {
        this.dataFineColtura = dataFineColtura;
        return this;
    }

    public void setDataFineColtura(LocalDate dataFineColtura) {
        this.dataFineColtura = dataFineColtura;
    }

    public LocalDate getDataFioritura() {
        return dataFioritura;
    }

    public ColturaAppezzamento dataFioritura(LocalDate dataFioritura) {
        this.dataFioritura = dataFioritura;
        return this;
    }

    public void setDataFioritura(LocalDate dataFioritura) {
        this.dataFioritura = dataFioritura;
    }

    public LocalDate getDataFineFioritura() {
        return dataFineFioritura;
    }

    public ColturaAppezzamento dataFineFioritura(LocalDate dataFineFioritura) {
        this.dataFineFioritura = dataFineFioritura;
        return this;
    }

    public void setDataFineFioritura(LocalDate dataFineFioritura) {
        this.dataFineFioritura = dataFineFioritura;
    }

    public LocalDate getDataRaccolta() {
        return dataRaccolta;
    }

    public ColturaAppezzamento dataRaccolta(LocalDate dataRaccolta) {
        this.dataRaccolta = dataRaccolta;
        return this;
    }

    public void setDataRaccolta(LocalDate dataRaccolta) {
        this.dataRaccolta = dataRaccolta;
    }

    public Integer getNumeroPiante() {
        return numeroPiante;
    }

    public ColturaAppezzamento numeroPiante(Integer numeroPiante) {
        this.numeroPiante = numeroPiante;
        return this;
    }

    public void setNumeroPiante(Integer numeroPiante) {
        this.numeroPiante = numeroPiante;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public ColturaAppezzamento dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public ColturaAppezzamento dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public ColturaAppezzamento userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public ColturaAppezzamento userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public ColturaAppezzamento note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Appezzamenti getColtToAppe() {
        return coltToAppe;
    }

    public ColturaAppezzamento coltToAppe(Appezzamenti appezzamenti) {
        this.coltToAppe = appezzamenti;
        return this;
    }

    public void setColtToAppe(Appezzamenti appezzamenti) {
        this.coltToAppe = appezzamenti;
    }

    public Agea20152020Matrice getColtToAgea() {
        return coltToAgea;
    }

    public ColturaAppezzamento coltToAgea(Agea20152020Matrice agea20152020Matrice) {
        this.coltToAgea = agea20152020Matrice;
        return this;
    }

    public void setColtToAgea(Agea20152020Matrice agea20152020Matrice) {
        this.coltToAgea = agea20152020Matrice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ColturaAppezzamento)) {
            return false;
        }
        return id != null && id.equals(((ColturaAppezzamento) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ColturaAppezzamento{" +
            "id=" + getId() +
            ", primaria='" + isPrimaria() + "'" +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataInizioColtura='" + getDataInizioColtura() + "'" +
            ", dataFineColtura='" + getDataFineColtura() + "'" +
            ", dataFioritura='" + getDataFioritura() + "'" +
            ", dataFineFioritura='" + getDataFineFioritura() + "'" +
            ", dataRaccolta='" + getDataRaccolta() + "'" +
            ", numeroPiante=" + getNumeroPiante() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
