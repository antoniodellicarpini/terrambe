package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A OpePatentinoTipologia.
 */
@Entity
@Table(name = "ope_patentino_tipologia")
@DynamicUpdate
public class OpePatentinoTipologia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "tpologia", nullable = false)
    private String tpologia;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

	//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "opepatentinotipologia_id")
    //@OneToMany(mappedBy = "opepatentinotipologia")
    private Set<OpePatentino> opePatTipToOpePates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTpologia() {
        return tpologia;
    }

    public OpePatentinoTipologia tpologia(String tpologia) {
        this.tpologia = tpologia;
        return this;
    }

    public void setTpologia(String tpologia) {
        this.tpologia = tpologia;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public OpePatentinoTipologia dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public OpePatentinoTipologia dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public OpePatentinoTipologia userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public OpePatentinoTipologia userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public OpePatentinoTipologia note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<OpePatentino> getOpePatTipToOpePates() {
        return opePatTipToOpePates;
    }

    public OpePatentinoTipologia opePatTipToOpePates(Set<OpePatentino> opePatentinos) {
        this.opePatTipToOpePates = opePatentinos;
        return this;
    }

    public OpePatentinoTipologia addOpePatTipToOpePate(OpePatentino opePatentino) {
        this.opePatTipToOpePates.add(opePatentino);
        opePatentino.setOpepatentinotipologia(this);
        return this;
    }

    public OpePatentinoTipologia removeOpePatTipToOpePate(OpePatentino opePatentino) {
        this.opePatTipToOpePates.remove(opePatentino);
        opePatentino.setOpepatentinotipologia(null);
        return this;
    }

    public void setOpePatTipToOpePates(Set<OpePatentino> opePatentinos) {
        this.opePatTipToOpePates = opePatentinos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OpePatentinoTipologia)) {
            return false;
        }
        return id != null && id.equals(((OpePatentinoTipologia) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OpePatentinoTipologia{" +
            "id=" + getId() +
            ", tpologia='" + getTpologia() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
