package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A UniAnagrafica.
 */
@Entity
@Table(name = "uni_anagrafica")
@DynamicUpdate
public class UniAnagrafica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
		@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
		@JoinColumn(name = "ind_to_uni_ana_id")
		@JsonIgnoreProperties("indToUniAna")
		//@OneToMany(mappedBy = "indToUniAna")
		private Set<IndUniAna> uniAnaToInds = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("aziAnaToUniAnas")
    private AziAnagrafica unianaaziana;

    @OneToMany(mappedBy = "atrAnaToUniAna")
    private Set<AtrAnagrafica> uniAnaToAtrAnas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public UniAnagrafica descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public UniAnagrafica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public UniAnagrafica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public UniAnagrafica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public UniAnagrafica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public UniAnagrafica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IndUniAna> getUniAnaToInds() {
        return uniAnaToInds;
    }

    public UniAnagrafica uniAnaToInds(Set<IndUniAna> indUniAnas) {
        this.uniAnaToInds = indUniAnas;
        return this;
    }

    public UniAnagrafica addUniAnaToInd(IndUniAna indUniAna) {
        this.uniAnaToInds.add(indUniAna);
        indUniAna.setIndToUniAna(this);
        return this;
    }

    public UniAnagrafica removeUniAnaToInd(IndUniAna indUniAna) {
        this.uniAnaToInds.remove(indUniAna);
        indUniAna.setIndToUniAna(null);
        return this;
    }

    public void setUniAnaToInds(Set<IndUniAna> indUniAnas) {
        this.uniAnaToInds = indUniAnas;
    }

    public AziAnagrafica getUnianaaziana() {
        return unianaaziana;
    }

    public UniAnagrafica unianaaziana(AziAnagrafica aziAnagrafica) {
        this.unianaaziana = aziAnagrafica;
        return this;
    }

    public void setUnianaaziana(AziAnagrafica aziAnagrafica) {
        this.unianaaziana = aziAnagrafica;
    }

    public Set<AtrAnagrafica> getUniAnaToAtrAnas() {
        return uniAnaToAtrAnas;
    }

    public UniAnagrafica uniAnaToAtrAnas(Set<AtrAnagrafica> atrAnagraficas) {
        this.uniAnaToAtrAnas = atrAnagraficas;
        return this;
    }

    public UniAnagrafica addUniAnaToAtrAna(AtrAnagrafica atrAnagrafica) {
        this.uniAnaToAtrAnas.add(atrAnagrafica);
        atrAnagrafica.setAtrAnaToUniAna(this);
        return this;
    }

    public UniAnagrafica removeUniAnaToAtrAna(AtrAnagrafica atrAnagrafica) {
        this.uniAnaToAtrAnas.remove(atrAnagrafica);
        atrAnagrafica.setAtrAnaToUniAna(null);
        return this;
    }

    public void setUniAnaToAtrAnas(Set<AtrAnagrafica> atrAnagraficas) {
        this.uniAnaToAtrAnas = atrAnagraficas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UniAnagrafica)) {
            return false;
        }
        return id != null && id.equals(((UniAnagrafica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UniAnagrafica{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
