package com.terrambe.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.terrambe.domain.enumeration.TipologiaUtente;
import org.hibernate.annotations.DynamicUpdate;

/**
 * A UserTerram.
 */
@Entity
@Table(name = "user_terram")
@DynamicUpdate
public class UserTerram implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_utente")
    private TipologiaUtente tipoUtente;

    @Column(name = "id_juser")
    private Integer idJuser;
    
	//MODIFICA ------------------------------------------
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_terram_usr_ter_to_azi_ana",
               joinColumns = @JoinColumn(name = "user_terram_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "usr_ter_to_azi_ana_id", referencedColumnName = "id"))
    private Set<AziAnagrafica> usrTerToAziAnas = new HashSet<>();
    
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipologiaUtente getTipoUtente() {
        return tipoUtente;
    }

    public UserTerram tipoUtente(TipologiaUtente tipoUtente) {
        this.tipoUtente = tipoUtente;
        return this;
    }

    public void setTipoUtente(TipologiaUtente tipoUtente) {
        this.tipoUtente = tipoUtente;
    }

    public Integer getIdJuser() {
        return idJuser;
    }

    public UserTerram idJuser(Integer idJuser) {
        this.idJuser = idJuser;
        return this;
    }

    public void setIdJuser(Integer idJuser) {
        this.idJuser = idJuser;
    }

    public Set<AziAnagrafica> getUsrTerToAziAnas() {
        return usrTerToAziAnas;
    }

    public UserTerram usrTerToAziAnas(Set<AziAnagrafica> aziAnagraficas) {
        this.usrTerToAziAnas = aziAnagraficas;
        return this;
    }

    public UserTerram addUsrTerToAziAna(AziAnagrafica aziAnagrafica) {
        this.usrTerToAziAnas.add(aziAnagrafica);
        aziAnagrafica.getAziAnaToUsrTers().add(this);
        return this;
    }

    public UserTerram removeUsrTerToAziAna(AziAnagrafica aziAnagrafica) {
        this.usrTerToAziAnas.remove(aziAnagrafica);
        aziAnagrafica.getAziAnaToUsrTers().remove(this);
        return this;
    }

    public void setUsrTerToAziAnas(Set<AziAnagrafica> aziAnagraficas) {
        this.usrTerToAziAnas = aziAnagraficas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserTerram)) {
            return false;
        }
        return id != null && id.equals(((UserTerram) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserTerram{" +
            "id=" + getId() +
            ", tipoUtente='" + getTipoUtente() + "'" +
            ", idJuser=" + getIdJuser() +
            "}";
    }
}
