package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A AziAnagrafica.
 */
@Entity
@Table(name = "azi_anagrafica")
@DynamicUpdate
public class AziAnagrafica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 11, max = 16)
    @Column(name = "cuaa", length = 16, nullable = false)
    private String cuaa;

    @NotNull
    @Size(min = 11, max = 11)
    @Column(name = "partitaiva", length = 11, nullable = false)
    private String partitaiva;

    @NotNull
    @Size(min = 16, max = 16)
    @Column(name = "codicefiscale", length = 16, nullable = false)
    private String codicefiscale;

    @NotNull
    @Column(name = "codice_ateco", nullable = false)
    private String codiceAteco;

    @NotNull
    @Column(name = "ragione_sociale", nullable = false)
    private String ragioneSociale;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "attivo")
    private Boolean attivo;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "cli_to_azi_ana_id")
	@JsonIgnoreProperties("cliToAziAna")
	//OneToMany(mappedBy = "cliToAziAna")
	private Set<Cliente> aziAnaToClis = new HashSet<>();

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "ind_to_azi_id")
	@JsonIgnoreProperties("indToAzi")
	//@OneToMany(mappedBy = "indToAzi")
	private Set<IndAzienda> aziToInds = new HashSet<>();

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "rec_to_azi_id")
	@JsonIgnoreProperties("recToAzi")
	//@OneToMany(mappedBy = "recToAzi")
	private Set<RecAzienda> aziToRecs = new HashSet<>();

    @OneToMany(mappedBy = "broglToAziAna")
    private Set<Brogliaccio> aziAnaToBrogls = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("aziAnagraficas")
    private AziFormaGiuridica naturaGiuridica;

	//MODIFICA -----------------------------------------
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonIgnoreProperties("aziRapToAziAnas")
	private AziRappresentante aziAnaToAziRap;

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "unianaaziana_id")
	@JsonIgnore
    //@JsonIgnoreProperties("unianaaziana")
	//@OneToMany(mappedBy = "unianaaziana")
	private Set<UniAnagrafica> aziAnaToUniAnas = new HashSet<>();

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "operatoriazi_id")
	@JsonIgnoreProperties("operatoriazi")
	//@OneToMany(mappedBy = "operatoriazi")
	private Set<OpeAnagrafica> aziAnaToOpeAnas = new HashSet<>();

    @ManyToMany(mappedBy = "usrTerToAziAnas")
    @JsonIgnore
    private Set<UserTerram> aziAnaToUsrTers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCuaa() {
        return cuaa;
    }

    public AziAnagrafica cuaa(String cuaa) {
        this.cuaa = cuaa;
        return this;
    }

    public void setCuaa(String cuaa) {
        this.cuaa = cuaa;
    }

    public String getPartitaiva() {
        return partitaiva;
    }

    public AziAnagrafica partitaiva(String partitaiva) {
        this.partitaiva = partitaiva;
        return this;
    }

    public void setPartitaiva(String partitaiva) {
        this.partitaiva = partitaiva;
    }

    public String getCodicefiscale() {
        return codicefiscale;
    }

    public AziAnagrafica codicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
        return this;
    }

    public void setCodicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public String getCodiceAteco() {
        return codiceAteco;
    }

    public AziAnagrafica codiceAteco(String codiceAteco) {
        this.codiceAteco = codiceAteco;
        return this;
    }

    public void setCodiceAteco(String codiceAteco) {
        this.codiceAteco = codiceAteco;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public AziAnagrafica ragioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
        return this;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AziAnagrafica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AziAnagrafica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Boolean isAttivo() {
        return attivo;
    }

    public AziAnagrafica attivo(Boolean attivo) {
        this.attivo = attivo;
        return this;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AziAnagrafica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AziAnagrafica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AziAnagrafica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Cliente> getAziAnaToClis() {
        return aziAnaToClis;
    }

    public AziAnagrafica aziAnaToClis(Set<Cliente> clientes) {
        this.aziAnaToClis = clientes;
        return this;
    }

    public AziAnagrafica addAziAnaToCli(Cliente cliente) {
        this.aziAnaToClis.add(cliente);
        cliente.setCliToAziAna(this);
        return this;
    }

    public AziAnagrafica removeAziAnaToCli(Cliente cliente) {
        this.aziAnaToClis.remove(cliente);
        cliente.setCliToAziAna(null);
        return this;
    }

    public void setAziAnaToClis(Set<Cliente> clientes) {
        this.aziAnaToClis = clientes;
    }

    public Set<IndAzienda> getAziToInds() {
        return aziToInds;
    }

    public AziAnagrafica aziToInds(Set<IndAzienda> indAziendas) {
        this.aziToInds = indAziendas;
        return this;
    }

    public AziAnagrafica addAziToInd(IndAzienda indAzienda) {
        this.aziToInds.add(indAzienda);
        indAzienda.setIndToAzi(this);
        return this;
    }

    public AziAnagrafica removeAziToInd(IndAzienda indAzienda) {
        this.aziToInds.remove(indAzienda);
        indAzienda.setIndToAzi(null);
        return this;
    }

    public void setAziToInds(Set<IndAzienda> indAziendas) {
        this.aziToInds = indAziendas;
    }

    public Set<RecAzienda> getAziToRecs() {
        return aziToRecs;
    }

    public AziAnagrafica aziToRecs(Set<RecAzienda> recAziendas) {
        this.aziToRecs = recAziendas;
        return this;
    }

    public AziAnagrafica addAziToRec(RecAzienda recAzienda) {
        this.aziToRecs.add(recAzienda);
        recAzienda.setRecToAzi(this);
        return this;
    }

    public AziAnagrafica removeAziToRec(RecAzienda recAzienda) {
        this.aziToRecs.remove(recAzienda);
        recAzienda.setRecToAzi(null);
        return this;
    }

    public void setAziToRecs(Set<RecAzienda> recAziendas) {
        this.aziToRecs = recAziendas;
    }

    public Set<Brogliaccio> getAziAnaToBrogls() {
        return aziAnaToBrogls;
    }

    public AziAnagrafica aziAnaToBrogls(Set<Brogliaccio> brogliaccios) {
        this.aziAnaToBrogls = brogliaccios;
        return this;
    }

    public AziAnagrafica addAziAnaToBrogl(Brogliaccio brogliaccio) {
        this.aziAnaToBrogls.add(brogliaccio);
        brogliaccio.setBroglToAziAna(this);
        return this;
    }

    public AziAnagrafica removeAziAnaToBrogl(Brogliaccio brogliaccio) {
        this.aziAnaToBrogls.remove(brogliaccio);
        brogliaccio.setBroglToAziAna(null);
        return this;
    }

    public void setAziAnaToBrogls(Set<Brogliaccio> brogliaccios) {
        this.aziAnaToBrogls = brogliaccios;
    }

    public AziFormaGiuridica getNaturaGiuridica() {
        return naturaGiuridica;
    }

    public AziAnagrafica naturaGiuridica(AziFormaGiuridica aziFormaGiuridica) {
        this.naturaGiuridica = aziFormaGiuridica;
        return this;
    }

    public void setNaturaGiuridica(AziFormaGiuridica aziFormaGiuridica) {
        this.naturaGiuridica = aziFormaGiuridica;
    }

    public AziRappresentante getAziAnaToAziRap() {
        return aziAnaToAziRap;
    }

    public AziAnagrafica aziAnaToAziRap(AziRappresentante aziRappresentante) {
        this.aziAnaToAziRap = aziRappresentante;
        return this;
    }

    public void setAziAnaToAziRap(AziRappresentante aziRappresentante) {
        this.aziAnaToAziRap = aziRappresentante;
    }

    public Set<UniAnagrafica> getAziAnaToUniAnas() {
        return aziAnaToUniAnas;
    }

    public AziAnagrafica aziAnaToUniAnas(Set<UniAnagrafica> uniAnagraficas) {
        this.aziAnaToUniAnas = uniAnagraficas;
        return this;
    }

    public AziAnagrafica addAziAnaToUniAna(UniAnagrafica uniAnagrafica) {
        this.aziAnaToUniAnas.add(uniAnagrafica);
        uniAnagrafica.setUnianaaziana(this);
        return this;
    }

    public AziAnagrafica removeAziAnaToUniAna(UniAnagrafica uniAnagrafica) {
        this.aziAnaToUniAnas.remove(uniAnagrafica);
        uniAnagrafica.setUnianaaziana(null);
        return this;
    }

    public void setAziAnaToUniAnas(Set<UniAnagrafica> uniAnagraficas) {
        this.aziAnaToUniAnas = uniAnagraficas;
    }

    public Set<OpeAnagrafica> getAziAnaToOpeAnas() {
        return aziAnaToOpeAnas;
    }

    public AziAnagrafica aziAnaToOpeAnas(Set<OpeAnagrafica> opeAnagraficas) {
        this.aziAnaToOpeAnas = opeAnagraficas;
        return this;
    }

    public AziAnagrafica addAziAnaToOpeAna(OpeAnagrafica opeAnagrafica) {
        this.aziAnaToOpeAnas.add(opeAnagrafica);
        opeAnagrafica.setOperatoriazi(this);
        return this;
    }

    public AziAnagrafica removeAziAnaToOpeAna(OpeAnagrafica opeAnagrafica) {
        this.aziAnaToOpeAnas.remove(opeAnagrafica);
        opeAnagrafica.setOperatoriazi(null);
        return this;
    }

    public void setAziAnaToOpeAnas(Set<OpeAnagrafica> opeAnagraficas) {
        this.aziAnaToOpeAnas = opeAnagraficas;
    }

    public Set<UserTerram> getAziAnaToUsrTers() {
        return aziAnaToUsrTers;
    }

    public AziAnagrafica aziAnaToUsrTers(Set<UserTerram> userTerrams) {
        this.aziAnaToUsrTers = userTerrams;
        return this;
    }

    public AziAnagrafica addAziAnaToUsrTer(UserTerram userTerram) {
        this.aziAnaToUsrTers.add(userTerram);
        userTerram.getUsrTerToAziAnas().add(this);
        return this;
    }

    public AziAnagrafica removeAziAnaToUsrTer(UserTerram userTerram) {
        this.aziAnaToUsrTers.remove(userTerram);
        userTerram.getUsrTerToAziAnas().remove(this);
        return this;
    }

    public void setAziAnaToUsrTers(Set<UserTerram> userTerrams) {
        this.aziAnaToUsrTers = userTerrams;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AziAnagrafica)) {
            return false;
        }
        return id != null && id.equals(((AziAnagrafica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AziAnagrafica{" +
            "id=" + getId() +
            ", cuaa='" + getCuaa() + "'" +
            ", partitaiva='" + getPartitaiva() + "'" +
            ", codicefiscale='" + getCodicefiscale() + "'" +
            ", codiceAteco='" + getCodiceAteco() + "'" +
            ", ragioneSociale='" + getRagioneSociale() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", attivo='" + isAttivo() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
