package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

//MODIFICA -----------------------------------------
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A AziRappresentante.
 */
@Entity
@Table(name = "azi_rappresentante")
@DynamicUpdate
public class AziRappresentante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "sesso")
    private String sesso;

    @Column(name = "data_nascita")
    private LocalDate dataNascita;

    @Size(min = 16, max = 16)
    @Column(name = "codice_fiscale", length = 16)
    private String codiceFiscale;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(unique = true)
private IndNascRap aziRapToIndNasc;

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "ind_to_azi_rap_id")
@JsonIgnoreProperties("indToAziRap")
//@OneToMany(mappedBy = "indToAziRap")
private Set<IndAziRap> aziRapToInds = new HashSet<>();

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "rec_to_azi_rap_id")
@JsonIgnoreProperties("recToAziRap")
//@OneToMany(mappedBy = "recToAziRap")
private Set<RecAziRap> aziRapToRecs = new HashSet<>();

@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "azi_ana_to_azi_rap_id")
@JsonIgnoreProperties("aziAnaToAziRap")
//@OneToMany(mappedBy = "azirappresentante")
private Set<AziAnagrafica> aziRapToAziAnas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public AziRappresentante nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public AziRappresentante cognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public AziRappresentante sesso(String sesso) {
        this.sesso = sesso;
        return this;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public LocalDate getDataNascita() {
        return dataNascita;
    }

    public AziRappresentante dataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
        return this;
    }

    public void setDataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public AziRappresentante codiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
        return this;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AziRappresentante dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AziRappresentante dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AziRappresentante userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AziRappresentante userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AziRappresentante note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public IndNascRap getAziRapToIndNasc() {
        return aziRapToIndNasc;
    }

    public AziRappresentante aziRapToIndNasc(IndNascRap indNascRap) {
        this.aziRapToIndNasc = indNascRap;
        return this;
    }

    public void setAziRapToIndNasc(IndNascRap indNascRap) {
        this.aziRapToIndNasc = indNascRap;
    }

    public Set<IndAziRap> getAziRapToInds() {
        return aziRapToInds;
    }

    public AziRappresentante aziRapToInds(Set<IndAziRap> indAziRaps) {
        this.aziRapToInds = indAziRaps;
        return this;
    }

    public AziRappresentante addAziRapToInd(IndAziRap indAziRap) {
        this.aziRapToInds.add(indAziRap);
        indAziRap.setIndToAziRap(this);
        return this;
    }

    public AziRappresentante removeAziRapToInd(IndAziRap indAziRap) {
        this.aziRapToInds.remove(indAziRap);
        indAziRap.setIndToAziRap(null);
        return this;
    }

    public void setAziRapToInds(Set<IndAziRap> indAziRaps) {
        this.aziRapToInds = indAziRaps;
    }

    public Set<RecAziRap> getAziRapToRecs() {
        return aziRapToRecs;
    }

    public AziRappresentante aziRapToRecs(Set<RecAziRap> recAziRaps) {
        this.aziRapToRecs = recAziRaps;
        return this;
    }

    public AziRappresentante addAziRapToRec(RecAziRap recAziRap) {
        this.aziRapToRecs.add(recAziRap);
        recAziRap.setRecToAziRap(this);
        return this;
    }

    public AziRappresentante removeAziRapToRec(RecAziRap recAziRap) {
        this.aziRapToRecs.remove(recAziRap);
        recAziRap.setRecToAziRap(null);
        return this;
    }

    public void setAziRapToRecs(Set<RecAziRap> recAziRaps) {
        this.aziRapToRecs = recAziRaps;
    }

    public Set<AziAnagrafica> getAziRapToAziAnas() {
        return aziRapToAziAnas;
    }

    public AziRappresentante aziRapToAziAnas(Set<AziAnagrafica> aziAnagraficas) {
        this.aziRapToAziAnas = aziAnagraficas;
        return this;
    }

    public AziRappresentante addAziRapToAziAna(AziAnagrafica aziAnagrafica) {
        this.aziRapToAziAnas.add(aziAnagrafica);
        aziAnagrafica.setAziAnaToAziRap(this);
        return this;
    }

    public AziRappresentante removeAziRapToAziAna(AziAnagrafica aziAnagrafica) {
        this.aziRapToAziAnas.remove(aziAnagrafica);
        aziAnagrafica.setAziAnaToAziRap(null);
        return this;
    }

    public void setAziRapToAziAnas(Set<AziAnagrafica> aziAnagraficas) {
        this.aziRapToAziAnas = aziAnagraficas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AziRappresentante)) {
            return false;
        }
        return id != null && id.equals(((AziRappresentante) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AziRappresentante{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", sesso='" + getSesso() + "'" +
            ", dataNascita='" + getDataNascita() + "'" +
            ", codiceFiscale='" + getCodiceFiscale() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
