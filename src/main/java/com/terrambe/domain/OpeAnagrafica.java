package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A OpeAnagrafica.
 */
@Entity
@Table(name = "ope_anagrafica")
@DynamicUpdate
public class OpeAnagrafica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "attivo")
    private Boolean attivo;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "cognome", nullable = false)
    private String cognome;

    @Column(name = "sesso")
    private String sesso;

    @NotNull
    @Column(name = "data_nascita", nullable = false)
    private LocalDate dataNascita;

    @NotNull
    @Size(min = 16, max = 16)
    @Column(name = "codice_fiscale", length = 16, nullable = false)
    private String codiceFiscale;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali= LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private IndNascOpeAna opeAnaToIndNasc;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ind_to_ope_ana_id")
    @JsonIgnoreProperties("indToOpeAna") 
    //@OneToMany(mappedBy = "indToOpeAna")
    private Set<IndOpeAna> opeAnaToInds = new HashSet<>();

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "rec_to_ope_ana_id")
    @JsonIgnoreProperties("recToOpeAna") 
    //@OneToMany(mappedBy = "recToOpeAna")
    private Set<RecOpeAna> opeAnaToRecs = new HashSet<>();

//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "per_oper_to_ope_anag_id")
	@JsonIgnoreProperties("perOperToOpeAnag")
	//@OneToMany(mappedBy = "opeAnagrafica")
	private Set<PermessiOperatore> permessioperatores = new HashSet<>();

//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "pate_to_ope_anag_id")
	@JsonIgnoreProperties("pateToOpeAnag")
	//@OneToMany(mappedBy = "pateToOpeAnag")
	private Set<OpePatentino> opeAnagToPates = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("aziAnaToOpeAnas")
    private AziAnagrafica operatoriazi;

//MODIFICA -----------------------------------------
    @NotNull
    @ManyToOne
    @JsonIgnoreProperties("opeQualToOpeAnags")
    private OpeQualifica opeAnagToOpeQual;


//MODIFICA ------------------------------------------
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ope_anagrafica_ope_ana_to_forn",
	joinColumns = @JoinColumn(name = "ope_anagrafica_id", referencedColumnName = "id"),
	inverseJoinColumns = @JoinColumn(name = "ope_ana_to_forn_id", referencedColumnName = "id"))
	private Set<Fornitori> opeAnaToForns = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tipoContratToOpes")
    private TipoContratto opeToTipoContrat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public OpeAnagrafica idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Boolean isAttivo() {
        return attivo;
    }

    public OpeAnagrafica attivo(Boolean attivo) {
        this.attivo = attivo;
        return this;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public String getNome() {
        return nome;
    }

    public OpeAnagrafica nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public OpeAnagrafica cognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public OpeAnagrafica sesso(String sesso) {
        this.sesso = sesso;
        return this;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public LocalDate getDataNascita() {
        return dataNascita;
    }

    public OpeAnagrafica dataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
        return this;
    }

    public void setDataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public OpeAnagrafica codiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
        return this;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public OpeAnagrafica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public OpeAnagrafica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public OpeAnagrafica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public OpeAnagrafica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public OpeAnagrafica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public IndNascOpeAna getOpeAnaToIndNasc() {
        return opeAnaToIndNasc;
    }

    public OpeAnagrafica opeAnaToIndNasc(IndNascOpeAna indNascOpeAna) {
        this.opeAnaToIndNasc = indNascOpeAna;
        return this;
    }

    public void setOpeAnaToIndNasc(IndNascOpeAna indNascOpeAna) {
        this.opeAnaToIndNasc = indNascOpeAna;
    }

    public Set<IndOpeAna> getOpeAnaToInds() {
        return opeAnaToInds;
    }

    public OpeAnagrafica opeAnaToInds(Set<IndOpeAna> indOpeAnas) {
        this.opeAnaToInds = indOpeAnas;
        return this;
    }

    public OpeAnagrafica addOpeAnaToInd(IndOpeAna indOpeAna) {
        this.opeAnaToInds.add(indOpeAna);
        indOpeAna.setIndToOpeAna(this);
        return this;
    }

    public OpeAnagrafica removeOpeAnaToInd(IndOpeAna indOpeAna) {
        this.opeAnaToInds.remove(indOpeAna);
        indOpeAna.setIndToOpeAna(null);
        return this;
    }

    public void setOpeAnaToInds(Set<IndOpeAna> indOpeAnas) {
        this.opeAnaToInds = indOpeAnas;
    }

    public Set<RecOpeAna> getOpeAnaToRecs() {
        return opeAnaToRecs;
    }

    public OpeAnagrafica opeAnaToRecs(Set<RecOpeAna> recOpeAnas) {
        this.opeAnaToRecs = recOpeAnas;
        return this;
    }

    public OpeAnagrafica addOpeAnaToRec(RecOpeAna recOpeAna) {
        this.opeAnaToRecs.add(recOpeAna);
        recOpeAna.setRecToOpeAna(this);
        return this;
    }

    public OpeAnagrafica removeOpeAnaToRec(RecOpeAna recOpeAna) {
        this.opeAnaToRecs.remove(recOpeAna);
        recOpeAna.setRecToOpeAna(null);
        return this;
    }

    public void setOpeAnaToRecs(Set<RecOpeAna> recOpeAnas) {
        this.opeAnaToRecs = recOpeAnas;
    }

    public Set<PermessiOperatore> getPermessioperatores() {
        return permessioperatores;
    }

    public OpeAnagrafica permessioperatores(Set<PermessiOperatore> permessiOperatores) {
        this.permessioperatores = permessiOperatores;
        return this;
    }

    public OpeAnagrafica addPermessioperatore(PermessiOperatore permessiOperatore) {
        this.permessioperatores.add(permessiOperatore);
        permessiOperatore.setPerOperToOpeAnag(this);
        return this;
    }

    public OpeAnagrafica removePermessioperatore(PermessiOperatore permessiOperatore) {
        this.permessioperatores.remove(permessiOperatore);
        permessiOperatore.setPerOperToOpeAnag(null);
        return this;
    }

    public void setPermessioperatores(Set<PermessiOperatore> permessiOperatores) {
        this.permessioperatores = permessiOperatores;
    }

    public Set<OpePatentino> getOpeAnagToPates() {
        return opeAnagToPates;
    }

    public OpeAnagrafica opeAnagToPates(Set<OpePatentino> opePatentinos) {
        this.opeAnagToPates = opePatentinos;
        return this;
    }

    public OpeAnagrafica addOpeAnagToPate(OpePatentino opePatentino) {
        this.opeAnagToPates.add(opePatentino);
        opePatentino.setPateToOpeAnag(this);
        return this;
    }

    public OpeAnagrafica removeOpeAnagToPate(OpePatentino opePatentino) {
        this.opeAnagToPates.remove(opePatentino);
        opePatentino.setPateToOpeAnag(null);
        return this;
    }

    public void setOpeAnagToPates(Set<OpePatentino> opePatentinos) {
        this.opeAnagToPates = opePatentinos;
    }

    public AziAnagrafica getOperatoriazi() {
        return operatoriazi;
    }

    public OpeAnagrafica operatoriazi(AziAnagrafica aziAnagrafica) {
        this.operatoriazi = aziAnagrafica;
        return this;
    }

    public void setOperatoriazi(AziAnagrafica aziAnagrafica) {
        this.operatoriazi = aziAnagrafica;
    }

    public OpeQualifica getOpeAnagToOpeQual() {
        return opeAnagToOpeQual;
    }

    public OpeAnagrafica opeAnagToOpeQual(OpeQualifica opeQualifica) {
        this.opeAnagToOpeQual = opeQualifica;
        return this;
    }

    public void setOpeAnagToOpeQual(OpeQualifica opeQualifica) {
        this.opeAnagToOpeQual = opeQualifica;
    }

    public Set<Fornitori> getOpeAnaToForns() {
        return opeAnaToForns;
    }

    public OpeAnagrafica opeAnaToForns(Set<Fornitori> fornitoris) {
        this.opeAnaToForns = fornitoris;
        return this;
    }

    public OpeAnagrafica addOpeAnaToForn(Fornitori fornitori) {
        this.opeAnaToForns.add(fornitori);
        fornitori.getFornToOpeAnas().add(this);
        return this;
    }

    public OpeAnagrafica removeOpeAnaToForn(Fornitori fornitori) {
        this.opeAnaToForns.remove(fornitori);
        fornitori.getFornToOpeAnas().remove(this);
        return this;
    }

    public void setOpeAnaToForns(Set<Fornitori> fornitoris) {
        this.opeAnaToForns = fornitoris;
    }

    public TipoContratto getOpeToTipoContrat() {
        return opeToTipoContrat;
    }

    public OpeAnagrafica opeToTipoContrat(TipoContratto tipoContratto) {
        this.opeToTipoContrat = tipoContratto;
        return this;
    }

    public void setOpeToTipoContrat(TipoContratto tipoContratto) {
        this.opeToTipoContrat = tipoContratto;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OpeAnagrafica)) {
            return false;
        }
        return id != null && id.equals(((OpeAnagrafica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OpeAnagrafica{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", attivo='" + isAttivo() + "'" +
            ", nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", sesso='" + getSesso() + "'" +
            ", dataNascita='" + getDataNascita() + "'" +
            ", codiceFiscale='" + getCodiceFiscale() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
