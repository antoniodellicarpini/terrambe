package com.terrambe.domain.enumeration;

/**
 * The TipologiaUtente enumeration.
 */
public enum TipologiaUtente {
    RAPPRESENTANTE, TERZISTA, DELEGATO, OPERATORE
}
