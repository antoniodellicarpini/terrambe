package com.terrambe.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * A EtichettaFitoFilter.
 */

 @JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorTerram {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cod;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String field;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public ErrorTerram(){
        this.cod= null;
        this.field= null;
        this.message= null;
    }

    public ErrorTerram(String cod, String field, String message){
        this.cod= cod;
        this.field= field;
        this.message= message;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorTerram{" + 
        "cod=" + cod + 
        ", field=" + field + 
        ", message=" + message + '}';
    }
}
