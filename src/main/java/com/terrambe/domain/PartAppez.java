package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A PartAppez.
 */
@Entity
@Table(name = "part_appez")
@DynamicUpdate
public class PartAppez implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "foglio", nullable = false)
    private String foglio;

    @NotNull
    @Column(name = "part", nullable = false)
    private String part;

    @Column(name = "sub")
    private String sub;

    @Column(name = "sezione")
    private String sezione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("appzToPartAppzs")
    private Appezzamenti partAppzToAppz;

    @ManyToOne
    @JsonIgnoreProperties("partToPartAppzs")
    private AziSuperficieParticella partAppzToPart;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoglio() {
        return foglio;
    }

    public PartAppez foglio(String foglio) {
        this.foglio = foglio;
        return this;
    }

    public void setFoglio(String foglio) {
        this.foglio = foglio;
    }

    public String getPart() {
        return part;
    }

    public PartAppez part(String part) {
        this.part = part;
        return this;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getSub() {
        return sub;
    }

    public PartAppez sub(String sub) {
        this.sub = sub;
        return this;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getSezione() {
        return sezione;
    }

    public PartAppez sezione(String sezione) {
        this.sezione = sezione;
        return this;
    }

    public void setSezione(String sezione) {
        this.sezione = sezione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public PartAppez dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public PartAppez dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public PartAppez userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public PartAppez userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public PartAppez note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Appezzamenti getPartAppzToAppz() {
        return partAppzToAppz;
    }

    public PartAppez partAppzToAppz(Appezzamenti appezzamenti) {
        this.partAppzToAppz = appezzamenti;
        return this;
    }

    public void setPartAppzToAppz(Appezzamenti appezzamenti) {
        this.partAppzToAppz = appezzamenti;
    }

    public AziSuperficieParticella getPartAppzToPart() {
        return partAppzToPart;
    }

    public PartAppez partAppzToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.partAppzToPart = aziSuperficieParticella;
        return this;
    }

    public void setPartAppzToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.partAppzToPart = aziSuperficieParticella;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartAppez)) {
            return false;
        }
        return id != null && id.equals(((PartAppez) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PartAppez{" +
            "id=" + getId() +
            ", foglio='" + getFoglio() + "'" +
            ", part='" + getPart() + "'" +
            ", sub='" + getSub() + "'" +
            ", sezione='" + getSezione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
