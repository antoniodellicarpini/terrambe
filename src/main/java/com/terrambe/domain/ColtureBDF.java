package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ColtureBDF.
 */
@Entity
@Table(name = "colture_bdf")
@DynamicUpdate
public class ColtureBDF implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "codice_pv")
    private String codicePv;

    @Column(name = "nome_coltura")
    private String nomeColtura;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "raccordoToColtBdf")
    private Set<TabellaRaccordo> coltBdfToRaccordos = new HashSet<>();

    @OneToMany(mappedBy = "colMascToColtBdf")
    private Set<TerramColtureMascherate> coltBdfToColtMascs = new HashSet<>();

    @OneToMany(mappedBy = "ageaBdfToColtBdf")
    private Set<Agea20152020BDF> coltBdfToAgeaBdfs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodicePv() {
        return codicePv;
    }

    public ColtureBDF codicePv(String codicePv) {
        this.codicePv = codicePv;
        return this;
    }

    public void setCodicePv(String codicePv) {
        this.codicePv = codicePv;
    }

    public String getNomeColtura() {
        return nomeColtura;
    }

    public ColtureBDF nomeColtura(String nomeColtura) {
        this.nomeColtura = nomeColtura;
        return this;
    }

    public void setNomeColtura(String nomeColtura) {
        this.nomeColtura = nomeColtura;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public ColtureBDF tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public ColtureBDF operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public ColtureBDF ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public ColtureBDF dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public ColtureBDF dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public ColtureBDF userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public ColtureBDF userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public ColtureBDF note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<TabellaRaccordo> getColtBdfToRaccordos() {
        return coltBdfToRaccordos;
    }

    public ColtureBDF coltBdfToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.coltBdfToRaccordos = tabellaRaccordos;
        return this;
    }

    public ColtureBDF addColtBdfToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.coltBdfToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToColtBdf(this);
        return this;
    }

    public ColtureBDF removeColtBdfToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.coltBdfToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToColtBdf(null);
        return this;
    }

    public void setColtBdfToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.coltBdfToRaccordos = tabellaRaccordos;
    }

    public Set<TerramColtureMascherate> getColtBdfToColtMascs() {
        return coltBdfToColtMascs;
    }

    public ColtureBDF coltBdfToColtMascs(Set<TerramColtureMascherate> terramColtureMascherates) {
        this.coltBdfToColtMascs = terramColtureMascherates;
        return this;
    }

    public ColtureBDF addColtBdfToColtMasc(TerramColtureMascherate terramColtureMascherate) {
        this.coltBdfToColtMascs.add(terramColtureMascherate);
        terramColtureMascherate.setColMascToColtBdf(this);
        return this;
    }

    public ColtureBDF removeColtBdfToColtMasc(TerramColtureMascherate terramColtureMascherate) {
        this.coltBdfToColtMascs.remove(terramColtureMascherate);
        terramColtureMascherate.setColMascToColtBdf(null);
        return this;
    }

    public void setColtBdfToColtMascs(Set<TerramColtureMascherate> terramColtureMascherates) {
        this.coltBdfToColtMascs = terramColtureMascherates;
    }

    public Set<Agea20152020BDF> getColtBdfToAgeaBdfs() {
        return coltBdfToAgeaBdfs;
    }

    public ColtureBDF coltBdfToAgeaBdfs(Set<Agea20152020BDF> agea20152020BDFS) {
        this.coltBdfToAgeaBdfs = agea20152020BDFS;
        return this;
    }

    public ColtureBDF addColtBdfToAgeaBdf(Agea20152020BDF agea20152020BDF) {
        this.coltBdfToAgeaBdfs.add(agea20152020BDF);
        agea20152020BDF.setAgeaBdfToColtBdf(this);
        return this;
    }

    public ColtureBDF removeColtBdfToAgeaBdf(Agea20152020BDF agea20152020BDF) {
        this.coltBdfToAgeaBdfs.remove(agea20152020BDF);
        agea20152020BDF.setAgeaBdfToColtBdf(null);
        return this;
    }

    public void setColtBdfToAgeaBdfs(Set<Agea20152020BDF> agea20152020BDFS) {
        this.coltBdfToAgeaBdfs = agea20152020BDFS;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ColtureBDF)) {
            return false;
        }
        return id != null && id.equals(((ColtureBDF) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ColtureBDF{" +
            "id=" + getId() +
            ", codicePv='" + getCodicePv() + "'" +
            ", nomeColtura='" + getNomeColtura() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
