package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcIrr.
 */
@Entity
@Table(name = "reg_opc_irr")
@DynamicUpdate
public class RegOpcIrr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_operazione")
    private LocalDate dataOperazione;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "volume_acqua")
    private Float volumeAcqua;

    @Column(name = "data_inizio_period")
    private LocalDate dataInizioPeriod;

    @Column(name = "data_fine_period")
    private LocalDate dataFinePeriod;

    @Column(name = "frequenza_giorni")
    private Integer frequenzaGiorni;

    @Column(name = "id_coltura")
    private Long idColtura;

    @Column(name = "id_operatore")
    private Long idOperatore;

    @Column(name = "id_mezzo")
    private Long idMezzo;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "appz_to_opc_irr_id")
@JsonIgnoreProperties("appzToOpcIrr")
//@OneToMany(mappedBy = "appzToOpcIrr")
private Set<RegOpcIrrAppz> opcIrrToAppzs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tipoIrrToOpcIrrs")
    private TipoIrr opcIrrToTipoIrr;

    @ManyToOne
    @JsonIgnoreProperties("motivoToOpcIrrs")
    private MotivoIrr opcIrrToMotivo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcIrr idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcIrr idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataOperazione() {
        return dataOperazione;
    }

    public RegOpcIrr dataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
        return this;
    }

    public void setDataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcIrr tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Float getVolumeAcqua() {
        return volumeAcqua;
    }

    public RegOpcIrr volumeAcqua(Float volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
        return this;
    }

    public void setVolumeAcqua(Float volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
    }

    public LocalDate getDataInizioPeriod() {
        return dataInizioPeriod;
    }

    public RegOpcIrr dataInizioPeriod(LocalDate dataInizioPeriod) {
        this.dataInizioPeriod = dataInizioPeriod;
        return this;
    }

    public void setDataInizioPeriod(LocalDate dataInizioPeriod) {
        this.dataInizioPeriod = dataInizioPeriod;
    }

    public LocalDate getDataFinePeriod() {
        return dataFinePeriod;
    }

    public RegOpcIrr dataFinePeriod(LocalDate dataFinePeriod) {
        this.dataFinePeriod = dataFinePeriod;
        return this;
    }

    public void setDataFinePeriod(LocalDate dataFinePeriod) {
        this.dataFinePeriod = dataFinePeriod;
    }

    public Integer getFrequenzaGiorni() {
        return frequenzaGiorni;
    }

    public RegOpcIrr frequenzaGiorni(Integer frequenzaGiorni) {
        this.frequenzaGiorni = frequenzaGiorni;
        return this;
    }

    public void setFrequenzaGiorni(Integer frequenzaGiorni) {
        this.frequenzaGiorni = frequenzaGiorni;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcIrr idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public Long getIdOperatore() {
        return idOperatore;
    }

    public RegOpcIrr idOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
        return this;
    }

    public void setIdOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
    }

    public Long getIdMezzo() {
        return idMezzo;
    }

    public RegOpcIrr idMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
        return this;
    }

    public void setIdMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcIrr dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcIrr dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcIrr userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcIrr userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcIrr note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcIrrAppz> getOpcIrrToAppzs() {
        return opcIrrToAppzs;
    }

    public RegOpcIrr opcIrrToAppzs(Set<RegOpcIrrAppz> regOpcIrrAppzs) {
        this.opcIrrToAppzs = regOpcIrrAppzs;
        return this;
    }

    public RegOpcIrr addOpcIrrToAppz(RegOpcIrrAppz regOpcIrrAppz) {
        this.opcIrrToAppzs.add(regOpcIrrAppz);
        regOpcIrrAppz.setAppzToOpcIrr(this);
        return this;
    }

    public RegOpcIrr removeOpcIrrToAppz(RegOpcIrrAppz regOpcIrrAppz) {
        this.opcIrrToAppzs.remove(regOpcIrrAppz);
        regOpcIrrAppz.setAppzToOpcIrr(null);
        return this;
    }

    public void setOpcIrrToAppzs(Set<RegOpcIrrAppz> regOpcIrrAppzs) {
        this.opcIrrToAppzs = regOpcIrrAppzs;
    }

    public TipoIrr getOpcIrrToTipoIrr() {
        return opcIrrToTipoIrr;
    }

    public RegOpcIrr opcIrrToTipoIrr(TipoIrr tipoIrr) {
        this.opcIrrToTipoIrr = tipoIrr;
        return this;
    }

    public void setOpcIrrToTipoIrr(TipoIrr tipoIrr) {
        this.opcIrrToTipoIrr = tipoIrr;
    }

    public MotivoIrr getOpcIrrToMotivo() {
        return opcIrrToMotivo;
    }

    public RegOpcIrr opcIrrToMotivo(MotivoIrr motivoIrr) {
        this.opcIrrToMotivo = motivoIrr;
        return this;
    }

    public void setOpcIrrToMotivo(MotivoIrr motivoIrr) {
        this.opcIrrToMotivo = motivoIrr;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcIrr)) {
            return false;
        }
        return id != null && id.equals(((RegOpcIrr) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcIrr{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataOperazione='" + getDataOperazione() + "'" +
            ", tempoImpiegato='" + getTempoImpiegato() + "'" +
            ", volumeAcqua=" + getVolumeAcqua() +
            ", dataInizioPeriod='" + getDataInizioPeriod() + "'" +
            ", dataFinePeriod='" + getDataFinePeriod() + "'" +
            ", frequenzaGiorni=" + getFrequenzaGiorni() +
            ", idColtura=" + getIdColtura() +
            ", idOperatore=" + getIdOperatore() +
            ", idMezzo=" + getIdMezzo() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
