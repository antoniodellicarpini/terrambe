package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcPrepterr.
 */
@Entity
@Table(name = "reg_opc_prepterr")
@DynamicUpdate
public class RegOpcPrepterr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_iniz_opc")
    private LocalDate dataInizOpc;

    @Column(name = "data_fine_opc")
    private LocalDate dataFineOpc;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "id_coltura")
    private Long idColtura;

    @Column(name = "id_operatore")
    private Long idOperatore;

    @Column(name = "id_mezzo")
    private Long idMezzo;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "appz_to_opc_prepterr_id")
    @JsonIgnoreProperties("appzToOpcPrepterr")
    //@OneToMany(mappedBy = "appzToOpcPrepterr")
    private Set<RegOpcPrepterrAppz> opcPrepterrToAppzs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tipoToOpcPrepterrs")
    private TipoPrepterr opcPrepterrToTipo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcPrepterr idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcPrepterr idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataInizOpc() {
        return dataInizOpc;
    }

    public RegOpcPrepterr dataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
        return this;
    }

    public void setDataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDate getDataFineOpc() {
        return dataFineOpc;
    }

    public RegOpcPrepterr dataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
        return this;
    }

    public void setDataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcPrepterr tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcPrepterr idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public Long getIdOperatore() {
        return idOperatore;
    }

    public RegOpcPrepterr idOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
        return this;
    }

    public void setIdOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
    }

    public Long getIdMezzo() {
        return idMezzo;
    }

    public RegOpcPrepterr idMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
        return this;
    }

    public void setIdMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcPrepterr dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcPrepterr dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcPrepterr userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcPrepterr userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcPrepterr note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcPrepterrAppz> getOpcPrepterrToAppzs() {
        return opcPrepterrToAppzs;
    }

    public RegOpcPrepterr opcPrepterrToAppzs(Set<RegOpcPrepterrAppz> regOpcPrepterrAppzs) {
        this.opcPrepterrToAppzs = regOpcPrepterrAppzs;
        return this;
    }

    public RegOpcPrepterr addOpcPrepterrToAppz(RegOpcPrepterrAppz regOpcPrepterrAppz) {
        this.opcPrepterrToAppzs.add(regOpcPrepterrAppz);
        regOpcPrepterrAppz.setAppzToOpcPrepterr(this);
        return this;
    }

    public RegOpcPrepterr removeOpcPrepterrToAppz(RegOpcPrepterrAppz regOpcPrepterrAppz) {
        this.opcPrepterrToAppzs.remove(regOpcPrepterrAppz);
        regOpcPrepterrAppz.setAppzToOpcPrepterr(null);
        return this;
    }

    public void setOpcPrepterrToAppzs(Set<RegOpcPrepterrAppz> regOpcPrepterrAppzs) {
        this.opcPrepterrToAppzs = regOpcPrepterrAppzs;
    }

    public TipoPrepterr getOpcPrepterrToTipo() {
        return opcPrepterrToTipo;
    }

    public RegOpcPrepterr opcPrepterrToTipo(TipoPrepterr tipoPrepterr) {
        this.opcPrepterrToTipo = tipoPrepterr;
        return this;
    }

    public void setOpcPrepterrToTipo(TipoPrepterr tipoPrepterr) {
        this.opcPrepterrToTipo = tipoPrepterr;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcPrepterr)) {
            return false;
        }
        return id != null && id.equals(((RegOpcPrepterr) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcPrepterr{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataInizOpc='" + getDataInizOpc() + "'" +
            ", dataFineOpc='" + getDataFineOpc() + "'" +
            ", tempoImpiegato='" + getTempoImpiegato() + "'" +
            ", idColtura=" + getIdColtura() +
            ", idOperatore=" + getIdOperatore() +
            ", idMezzo=" + getIdMezzo() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
