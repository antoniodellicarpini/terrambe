package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TipologiaVincoliTerritoriali.
 */
@Entity
@Table(name = "tipologia_vincoli_territoriali")
public class TipologiaVincoliTerritoriali implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToMany(mappedBy = "appezToVincoliTers")
    @JsonIgnore
    private Set<Appezzamenti> vincoliTerToAppezs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public TipologiaVincoliTerritoriali descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TipologiaVincoliTerritoriali dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TipologiaVincoliTerritoriali dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TipologiaVincoliTerritoriali userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TipologiaVincoliTerritoriali userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public TipologiaVincoliTerritoriali note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Appezzamenti> getVincoliTerToAppezs() {
        return vincoliTerToAppezs;
    }

    public TipologiaVincoliTerritoriali vincoliTerToAppezs(Set<Appezzamenti> appezzamentis) {
        this.vincoliTerToAppezs = appezzamentis;
        return this;
    }

    public TipologiaVincoliTerritoriali addVincoliTerToAppez(Appezzamenti appezzamenti) {
        this.vincoliTerToAppezs.add(appezzamenti);
        appezzamenti.getAppezToVincoliTers().add(this);
        return this;
    }

    public TipologiaVincoliTerritoriali removeVincoliTerToAppez(Appezzamenti appezzamenti) {
        this.vincoliTerToAppezs.remove(appezzamenti);
        appezzamenti.getAppezToVincoliTers().remove(this);
        return this;
    }

    public void setVincoliTerToAppezs(Set<Appezzamenti> appezzamentis) {
        this.vincoliTerToAppezs = appezzamentis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipologiaVincoliTerritoriali)) {
            return false;
        }
        return id != null && id.equals(((TipologiaVincoliTerritoriali) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TipologiaVincoliTerritoriali{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
