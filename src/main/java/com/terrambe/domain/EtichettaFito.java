package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A EtichettaFito.
 */
@Entity
@Table(name = "etichetta_fito")
@DynamicUpdate
public class EtichettaFito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "link_etichetta")
    private String linkEtichetta;

    @Column(name = "nome_commerciale")
    private String nomeCommerciale;

    @Column(name = "data_reg")
    private LocalDate dataReg;

    @Column(name = "registrazione_ministeriale")
    private String registrazioneMinisteriale;

    @Column(name = "sigla")
    private String sigla;

    @Column(name = "bio")
    private String bio;

    @Column(name = "ppo")
    private String ppo;

    @Column(name = "revocato")
    private String revocato;

    @Column(name = "revoca_autorizzazione")
    private String revocaAutorizzazione;

    @Column(name = "scadenza_commercio")
    private String scadenzaCommercio;

    @Column(name = "scadenza_utilizzo")
    private String scadenzaUtilizzo;

    @Column(name = "avv_clp")
    private String avvClp;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<IncompatibilitaSA> incompatibilitaetichettas = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<ProbabileFitoTossicita> probfitotossicitas = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<NoteAggiuntiveEtichetta> noteagguntiveetis = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<EtichettaSostanzeAttive> etisostanzeattives = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<EtichettaFrasi> etichettafrasis = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<EtichettaPittogrammi> etichettapittogrammis = new HashSet<>();

    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "etichetta_fito_id")
    @JsonIgnoreProperties("etichettaFito")
    //@OneToMany(mappedBy = "etichettaFito")
    private Set<EtiTipologiaFito> etichettatipofitofarmacos = new HashSet<>();

    @OneToMany(mappedBy = "raccordoToEtiFito")
    private Set<TabellaRaccordo> etiFitoToRaccordos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("etichettaFitos")
    private TitolareRegistrazione titolareRegistrazione;

    @ManyToOne
    @JsonIgnoreProperties("etichettaFitos")
    private Formulazione formulazione;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkEtichetta() {
        return linkEtichetta;
    }

    public EtichettaFito LinkEtichetta(String linkEtichetta) {
        this.linkEtichetta = linkEtichetta;
        return this;
    }

    public void setLinkEtichetta(String linkEtichetta) {
        this.linkEtichetta = linkEtichetta;
    }
    
    public String getNomeCommerciale() {
        return nomeCommerciale;
    }

    public EtichettaFito nomeCommerciale(String nomeCommerciale) {
        this.nomeCommerciale = nomeCommerciale;
        return this;
    }

    public void setNomeCommerciale(String nomeCommerciale) {
        this.nomeCommerciale = nomeCommerciale;
    }

    public LocalDate getDataReg() {
        return dataReg;
    }

    public EtichettaFito dataReg(LocalDate dataReg) {
        this.dataReg = dataReg;
        return this;
    }

    public void setDataReg(LocalDate dataReg) {
        this.dataReg = dataReg;
    }

    public String getRegistrazioneMinisteriale() {
        return registrazioneMinisteriale;
    }

    public EtichettaFito registrazioneMinisteriale(String registrazioneMinisteriale) {
        this.registrazioneMinisteriale = registrazioneMinisteriale;
        return this;
    }

    public void setRegistrazioneMinisteriale(String registrazioneMinisteriale) {
        this.registrazioneMinisteriale = registrazioneMinisteriale;
    }

    public String getSigla() {
        return sigla;
    }

    public EtichettaFito sigla(String sigla) {
        this.sigla = sigla;
        return this;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getBio() {
        return bio;
    }

    public EtichettaFito bio(String bio) {
        this.bio = bio;
        return this;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPpo() {
        return ppo;
    }

    public EtichettaFito ppo(String ppo) {
        this.ppo = ppo;
        return this;
    }

    public void setPpo(String ppo) {
        this.ppo = ppo;
    }

    public String getRevocato() {
        return revocato;
    }

    public EtichettaFito revocato(String revocato) {
        this.revocato = revocato;
        return this;
    }

    public void setRevocato(String revocato) {
        this.revocato = revocato;
    }

    public String getRevocaAutorizzazione() {
        return revocaAutorizzazione;
    }

    public EtichettaFito revocaAutorizzazione(String revocaAutorizzazione) {
        this.revocaAutorizzazione = revocaAutorizzazione;
        return this;
    }

    public void setRevocaAutorizzazione(String revocaAutorizzazione) {
        this.revocaAutorizzazione = revocaAutorizzazione;
    }

    public String getScadenzaCommercio() {
        return scadenzaCommercio;
    }

    public EtichettaFito scadenzaCommercio(String scadenzaCommercio) {
        this.scadenzaCommercio = scadenzaCommercio;
        return this;
    }

    public void setScadenzaCommercio(String scadenzaCommercio) {
        this.scadenzaCommercio = scadenzaCommercio;
    }

    public String getScadenzaUtilizzo() {
        return scadenzaUtilizzo;
    }

    public EtichettaFito scadenzaUtilizzo(String scadenzaUtilizzo) {
        this.scadenzaUtilizzo = scadenzaUtilizzo;
        return this;
    }

    public void setScadenzaUtilizzo(String scadenzaUtilizzo) {
        this.scadenzaUtilizzo = scadenzaUtilizzo;
    }

    public String getAvvClp() {
        return avvClp;
    }

    public EtichettaFito avvClp(String avvClp) {
        this.avvClp = avvClp;
        return this;
    }

    public void setAvvClp(String avvClp) {
        this.avvClp = avvClp;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public EtichettaFito tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public EtichettaFito operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public EtichettaFito ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public EtichettaFito dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public EtichettaFito dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public EtichettaFito userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public EtichettaFito userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public EtichettaFito note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IncompatibilitaSA> getIncompatibilitaetichettas() {
        return incompatibilitaetichettas;
    }

    public EtichettaFito incompatibilitaetichettas(Set<IncompatibilitaSA> incompatibilitaSAS) {
        this.incompatibilitaetichettas = incompatibilitaSAS;
        return this;
    }

    public EtichettaFito addIncompatibilitaetichetta(IncompatibilitaSA incompatibilitaSA) {
        this.incompatibilitaetichettas.add(incompatibilitaSA);
        incompatibilitaSA.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeIncompatibilitaetichetta(IncompatibilitaSA incompatibilitaSA) {
        this.incompatibilitaetichettas.remove(incompatibilitaSA);
        incompatibilitaSA.setEtichettaFito(null);
        return this;
    }

    public void setIncompatibilitaetichettas(Set<IncompatibilitaSA> incompatibilitaSAS) {
        this.incompatibilitaetichettas = incompatibilitaSAS;
    }

    public Set<ProbabileFitoTossicita> getProbfitotossicitas() {
        return probfitotossicitas;
    }

    public EtichettaFito probfitotossicitas(Set<ProbabileFitoTossicita> probabileFitoTossicitas) {
        this.probfitotossicitas = probabileFitoTossicitas;
        return this;
    }

    public EtichettaFito addProbfitotossicita(ProbabileFitoTossicita probabileFitoTossicita) {
        this.probfitotossicitas.add(probabileFitoTossicita);
        probabileFitoTossicita.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeProbfitotossicita(ProbabileFitoTossicita probabileFitoTossicita) {
        this.probfitotossicitas.remove(probabileFitoTossicita);
        probabileFitoTossicita.setEtichettaFito(null);
        return this;
    }

    public void setProbfitotossicitas(Set<ProbabileFitoTossicita> probabileFitoTossicitas) {
        this.probfitotossicitas = probabileFitoTossicitas;
    }

    public Set<NoteAggiuntiveEtichetta> getNoteagguntiveetis() {
        return noteagguntiveetis;
    }

    public EtichettaFito noteagguntiveetis(Set<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettas) {
        this.noteagguntiveetis = noteAggiuntiveEtichettas;
        return this;
    }

    public EtichettaFito addNoteagguntiveeti(NoteAggiuntiveEtichetta noteAggiuntiveEtichetta) {
        this.noteagguntiveetis.add(noteAggiuntiveEtichetta);
        noteAggiuntiveEtichetta.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeNoteagguntiveeti(NoteAggiuntiveEtichetta noteAggiuntiveEtichetta) {
        this.noteagguntiveetis.remove(noteAggiuntiveEtichetta);
        noteAggiuntiveEtichetta.setEtichettaFito(null);
        return this;
    }

    public void setNoteagguntiveetis(Set<NoteAggiuntiveEtichetta> noteAggiuntiveEtichettas) {
        this.noteagguntiveetis = noteAggiuntiveEtichettas;
    }

    public Set<EtichettaSostanzeAttive> getEtisostanzeattives() {
        return etisostanzeattives;
    }

    public EtichettaFito etisostanzeattives(Set<EtichettaSostanzeAttive> etichettaSostanzeAttives) {
        this.etisostanzeattives = etichettaSostanzeAttives;
        return this;
    }

    public EtichettaFito addEtisostanzeattive(EtichettaSostanzeAttive etichettaSostanzeAttive) {
        this.etisostanzeattives.add(etichettaSostanzeAttive);
        etichettaSostanzeAttive.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeEtisostanzeattive(EtichettaSostanzeAttive etichettaSostanzeAttive) {
        this.etisostanzeattives.remove(etichettaSostanzeAttive);
        etichettaSostanzeAttive.setEtichettaFito(null);
        return this;
    }

    public void setEtisostanzeattives(Set<EtichettaSostanzeAttive> etichettaSostanzeAttives) {
        this.etisostanzeattives = etichettaSostanzeAttives;
    }

    public Set<EtichettaFrasi> getEtichettafrasis() {
        return etichettafrasis;
    }

    public EtichettaFito etichettafrasis(Set<EtichettaFrasi> etichettaFrasis) {
        this.etichettafrasis = etichettaFrasis;
        return this;
    }

    public EtichettaFito addEtichettafrasi(EtichettaFrasi etichettaFrasi) {
        this.etichettafrasis.add(etichettaFrasi);
        etichettaFrasi.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeEtichettafrasi(EtichettaFrasi etichettaFrasi) {
        this.etichettafrasis.remove(etichettaFrasi);
        etichettaFrasi.setEtichettaFito(null);
        return this;
    }

    public void setEtichettafrasis(Set<EtichettaFrasi> etichettaFrasis) {
        this.etichettafrasis = etichettaFrasis;
    }

    public Set<EtichettaPittogrammi> getEtichettapittogrammis() {
        return etichettapittogrammis;
    }

    public EtichettaFito etichettapittogrammis(Set<EtichettaPittogrammi> etichettaPittogrammis) {
        this.etichettapittogrammis = etichettaPittogrammis;
        return this;
    }

    public EtichettaFito addEtichettapittogrammi(EtichettaPittogrammi etichettaPittogrammi) {
        this.etichettapittogrammis.add(etichettaPittogrammi);
        etichettaPittogrammi.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeEtichettapittogrammi(EtichettaPittogrammi etichettaPittogrammi) {
        this.etichettapittogrammis.remove(etichettaPittogrammi);
        etichettaPittogrammi.setEtichettaFito(null);
        return this;
    }

    public void setEtichettapittogrammis(Set<EtichettaPittogrammi> etichettaPittogrammis) {
        this.etichettapittogrammis = etichettaPittogrammis;
    }

    public Set<EtiTipologiaFito> getEtichettatipofitofarmacos() {
        return etichettatipofitofarmacos;
    }

    public EtichettaFito etichettatipofitofarmacos(Set<EtiTipologiaFito> etiTipologiaFitos) {
        this.etichettatipofitofarmacos = etiTipologiaFitos;
        return this;
    }

    public EtichettaFito addEtichettatipofitofarmaco(EtiTipologiaFito etiTipologiaFito) {
        this.etichettatipofitofarmacos.add(etiTipologiaFito);
        etiTipologiaFito.setEtichettaFito(this);
        return this;
    }

    public EtichettaFito removeEtichettatipofitofarmaco(EtiTipologiaFito etiTipologiaFito) {
        this.etichettatipofitofarmacos.remove(etiTipologiaFito);
        etiTipologiaFito.setEtichettaFito(null);
        return this;
    }

    public void setEtichettatipofitofarmacos(Set<EtiTipologiaFito> etiTipologiaFitos) {
        this.etichettatipofitofarmacos = etiTipologiaFitos;
    }

    public Set<TabellaRaccordo> getEtiFitoToRaccordos() {
        return etiFitoToRaccordos;
    }

    public EtichettaFito etiFitoToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.etiFitoToRaccordos = tabellaRaccordos;
        return this;
    }

    public EtichettaFito addEtiFitoToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.etiFitoToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToEtiFito(this);
        return this;
    }

    public EtichettaFito removeEtiFitoToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.etiFitoToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToEtiFito(null);
        return this;
    }

    public void setEtiFitoToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.etiFitoToRaccordos = tabellaRaccordos;
    }

    public TitolareRegistrazione getTitolareRegistrazione() {
        return titolareRegistrazione;
    }

    public EtichettaFito titolareRegistrazione(TitolareRegistrazione titolareRegistrazione) {
        this.titolareRegistrazione = titolareRegistrazione;
        return this;
    }

    public void setTitolareRegistrazione(TitolareRegistrazione titolareRegistrazione) {
        this.titolareRegistrazione = titolareRegistrazione;
    }

    public Formulazione getFormulazione() {
        return formulazione;
    }

    public EtichettaFito formulazione(Formulazione formulazione) {
        this.formulazione = formulazione;
        return this;
    }

    public void setFormulazione(Formulazione formulazione) {
        this.formulazione = formulazione;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtichettaFito)) {
            return false;
        }
        return id != null && id.equals(((EtichettaFito) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EtichettaFito{" +
            "id=" + getId() +
            ", nomeCommerciale='" + getNomeCommerciale() + "'" +
            ", linkEtichetta='" + getLinkEtichetta() + "'" +
            ", dataReg='" + getDataReg() + "'" +
            ", registrazioneMinisteriale='" + getRegistrazioneMinisteriale() + "'" +
            ", sigla='" + getSigla() + "'" +
            ", bio='" + getBio() + "'" +
            ", ppo='" + getPpo() + "'" +
            ", revocato='" + getRevocato() + "'" +
            ", revocaAutorizzazione='" + getRevocaAutorizzazione() + "'" +
            ", scadenzaCommercio='" + getScadenzaCommercio() + "'" +
            ", scadenzaUtilizzo='" + getScadenzaUtilizzo() + "'" +
            ", avvClp='" + getAvvClp() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
