package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DosaggioTrattamento.
 */
@Entity
@Table(name = "dosaggio_trattamento")
@DynamicUpdate
public class DosaggioTrattamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "totale_prodotto_utilizzato")
    private Double totaleProdottoUtilizzato;

    @ManyToOne
    @JsonIgnoreProperties("dosaggioTrattamentos")
    private EtichettaFito etichettafito;

    @OneToMany(mappedBy = "regOpcFitoToDosTratt")
    private Set<RegOpcFito> dosTrattToRegOpcFitos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotaleProdottoUtilizzato() {
        return totaleProdottoUtilizzato;
    }

    public DosaggioTrattamento totaleProdottoUtilizzato(Double totaleProdottoUtilizzato) {
        this.totaleProdottoUtilizzato = totaleProdottoUtilizzato;
        return this;
    }

    public void setTotaleProdottoUtilizzato(Double totaleProdottoUtilizzato) {
        this.totaleProdottoUtilizzato = totaleProdottoUtilizzato;
    }

    public EtichettaFito getEtichettafito() {
        return etichettafito;
    }

    public DosaggioTrattamento etichettafito(EtichettaFito etichettaFito) {
        this.etichettafito = etichettaFito;
        return this;
    }

    public void setEtichettafito(EtichettaFito etichettaFito) {
        this.etichettafito = etichettaFito;
    }

    public Set<RegOpcFito> getDosTrattToRegOpcFitos() {
        return dosTrattToRegOpcFitos;
    }

    public DosaggioTrattamento dosTrattToRegOpcFitos(Set<RegOpcFito> regOpcFitos) {
        this.dosTrattToRegOpcFitos = regOpcFitos;
        return this;
    }

    public DosaggioTrattamento addDosTrattToRegOpcFito(RegOpcFito regOpcFito) {
        this.dosTrattToRegOpcFitos.add(regOpcFito);
        regOpcFito.setRegOpcFitoToDosTratt(this);
        return this;
    }

    public DosaggioTrattamento removeDosTrattToRegOpcFito(RegOpcFito regOpcFito) {
        this.dosTrattToRegOpcFitos.remove(regOpcFito);
        regOpcFito.setRegOpcFitoToDosTratt(null);
        return this;
    }

    public void setDosTrattToRegOpcFitos(Set<RegOpcFito> regOpcFitos) {
        this.dosTrattToRegOpcFitos = regOpcFitos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DosaggioTrattamento)) {
            return false;
        }
        return id != null && id.equals(((DosaggioTrattamento) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DosaggioTrattamento{" +
            "id=" + getId() +
            ", totaleProdottoUtilizzato=" + getTotaleProdottoUtilizzato() +
            "}";
    }
}
