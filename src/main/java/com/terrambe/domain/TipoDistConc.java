package com.terrambe.domain;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TipoDistConc.
 */
@Entity
@Table(name = "tipo_dist_conc")
@DynamicUpdate
public class TipoDistConc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descrizione")
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @OneToMany(mappedBy = "opcConcToTipoDist")
    private Set<RegOpcConc> tipoDistToOpcConcs = new HashSet<>();

    @OneToMany(mappedBy = "raccordoToTipoDistr")
    private Set<TabellaRaccordo> tipoDistrToRaccordos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public TipoDistConc descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TipoDistConc dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TipoDistConc dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TipoDistConc userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TipoDistConc userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public Set<RegOpcConc> getTipoDistToOpcConcs() {
        return tipoDistToOpcConcs;
    }

    public TipoDistConc tipoDistToOpcConcs(Set<RegOpcConc> regOpcConcs) {
        this.tipoDistToOpcConcs = regOpcConcs;
        return this;
    }

    public TipoDistConc addTipoDistToOpcConc(RegOpcConc regOpcConc) {
        this.tipoDistToOpcConcs.add(regOpcConc);
        regOpcConc.setOpcConcToTipoDist(this);
        return this;
    }

    public TipoDistConc removeTipoDistToOpcConc(RegOpcConc regOpcConc) {
        this.tipoDistToOpcConcs.remove(regOpcConc);
        regOpcConc.setOpcConcToTipoDist(null);
        return this;
    }

    public void setTipoDistToOpcConcs(Set<RegOpcConc> regOpcConcs) {
        this.tipoDistToOpcConcs = regOpcConcs;
    }

    public Set<TabellaRaccordo> getTipoDistrToRaccordos() {
        return tipoDistrToRaccordos;
    }

    public TipoDistConc tipoDistrToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.tipoDistrToRaccordos = tabellaRaccordos;
        return this;
    }

    public TipoDistConc addTipoDistrToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.tipoDistrToRaccordos.add(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToTipoDistr(this);
        return this;
    }

    public TipoDistConc removeTipoDistrToRaccordo(TabellaRaccordo tabellaRaccordo) {
        this.tipoDistrToRaccordos.remove(tabellaRaccordo);
        tabellaRaccordo.setRaccordoToTipoDistr(null);
        return this;
    }

    public void setTipoDistrToRaccordos(Set<TabellaRaccordo> tabellaRaccordos) {
        this.tipoDistrToRaccordos = tabellaRaccordos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoDistConc)) {
            return false;
        }
        return id != null && id.equals(((TipoDistConc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TipoDistConc{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            "}";
    }
}
