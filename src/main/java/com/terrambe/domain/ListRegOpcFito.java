package com.terrambe.domain;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * A EtichettaFitoFilter.
 */

public class ListRegOpcFito extends RegOpcFito implements Serializable {

//mod2
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private LocalDate dataOperazione;
    private String descOperatore;
    private String descColtura;
    private String descAvversita;
    private String descProdotto;
    private String descAppezzamento;
    private String tempoImpiegato;
    private String unitaProduttiva;
    private LocalDate dataInizioOperazione;
    private LocalDate dataFineOperazione;
    private Float volumeAcqua;
    private String unitaMisura;
    private String descMezzo;
    private String motiTrattamenti;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataOperazione() {
        return dataOperazione;
    }

    public void setDataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
    }

    public String getDescOperatore() {
        return descOperatore;
    }

    public void setDescOperatore(String descOperatore) {
        this.descOperatore = descOperatore;
    }

    public String getDescColtura() {
        return descColtura;
    }

    public void setDescColtura(String descColtura) {
        this.descColtura = descColtura;
    }

    public String getDescAvversita() {
        return descAvversita;
    }

    public void setDescAvversita(String descAvversita) {
        this.descAvversita = descAvversita;
    }

    public String getDescProdotto() {
        return descProdotto;
    }

    public void setDescProdotto(String descProdotto) {
        this.descProdotto = descProdotto;
    }

    public String getDescAppezzamento() {
        return descAppezzamento;
    }

    public void setDescAppezzamento(String descAppezzamento) {
        this.descAppezzamento = descAppezzamento;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public String getUnitaProduttiva() {
        return unitaProduttiva;
    }

    public void setUnitaProduttiva(String unitaProduttiva) {
        this.unitaProduttiva = unitaProduttiva;
    }

    public LocalDate getDataInizioOperazione() {
        return dataInizioOperazione;
    }

    public void setDataInizioOperazione(LocalDate dataInizioOperazione) {
        this.dataInizioOperazione = dataInizioOperazione;
    }

    public LocalDate getDataFineOperazione() {
        return dataFineOperazione;
    }

    public void setDataFineOperazione(LocalDate dataFineOperazione) {
        this.dataFineOperazione = dataFineOperazione;
    }

    public Float getVolumeAcqua() {
        return volumeAcqua;
    }

    public void setVolumeAcqua(Float volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
    }

    public String getUnitaMisura() {
        return unitaMisura;
    }

    public void setUnitaMisura(String unitaMisura) {
        this.unitaMisura = unitaMisura;
    }

    public String getDescMezzo() {
        return descMezzo;
    }

    public void setDescMezzo(String descMezzo) {
        this.descMezzo = descMezzo;
    }

    public String getMotiTrattamenti() {
        return motiTrattamenti;
    }

    public void setMotiTrattamenti(String motiTrattamenti) {
        this.motiTrattamenti = motiTrattamenti;
    }

    @Override
    public String toString() {
        return "ListRegOpcFito{" +
            "id=" + id +
            ", dataOperazione=" + dataOperazione +
            ", descOperatore='" + descOperatore + '\'' +
            ", descColtura='" + descColtura + '\'' +
            ", descAvversita='" + descAvversita + '\'' +
            ", descProdotto='" + descProdotto + '\'' +
            ", descAppezzamento='" + descAppezzamento + '\'' +
            ", tempoImpiegato='" + tempoImpiegato + '\'' +
            ", unitaProduttiva='" + unitaProduttiva + '\'' +
            ", dataInizioOperazione=" + dataInizioOperazione +
            ", dataFineOperazione=" + dataFineOperazione +
            ", volumeAcqua=" + volumeAcqua +
            ", unitaMisura='" + unitaMisura + '\'' +
            ", descMezzo='" + descMezzo + '\'' +
            ", motiTrattamenti='" + motiTrattamenti + '\'' +
            '}';
    }
}
