package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import javax.cache.annotation.CacheRemoveAll;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A AtrAnagrafica.
 */
@Entity
@Table(name = "atr_anagrafica")
@DynamicUpdate
public class AtrAnagrafica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @NotNull
    @Column(name = "modello", nullable = false)
    private String modello;

    @Column(name = "data_revisione")
    private LocalDate dataRevisione;

    @Column(name = "cavalli")
    private Integer cavalli;

    @Column(name = "capacita")
    private Integer capacita;

    @Column(name = "targa")
    private String targa;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("uniAnaToAtrAnas")
    private UniAnagrafica atrAnaToUniAna;

    @ManyToOne
    @JsonIgnoreProperties("atrAnagraficas")
    private AtrTipologia atrAnaToAtrTipo;

    @ManyToOne
    @JsonIgnoreProperties("atrAnagraficas")
    private AtrMarchio atrAnagToAtrMarc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public AtrAnagrafica idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public String getModello() {
        return modello;
    }

    public AtrAnagrafica modello(String modello) {
        this.modello = modello;
        return this;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public LocalDate getDataRevisione() {
        return dataRevisione;
    }

    public AtrAnagrafica dataRevisione(LocalDate dataRevisione) {
        this.dataRevisione = dataRevisione;
        return this;
    }

    public void setDataRevisione(LocalDate dataRevisione) {
        this.dataRevisione = dataRevisione;
    }

    public Integer getCavalli() {
        return cavalli;
    }

    public AtrAnagrafica cavalli(Integer cavalli) {
        this.cavalli = cavalli;
        return this;
    }

    public void setCavalli(Integer cavalli) {
        this.cavalli = cavalli;
    }

    public Integer getCapacita() {
        return capacita;
    }

    public AtrAnagrafica capacita(Integer capacita) {
        this.capacita = capacita;
        return this;
    }

    public void setCapacita(Integer capacita) {
        this.capacita = capacita;
    }

    public String getTarga() {
        return targa;
    }

    public AtrAnagrafica targa(String targa) {
        this.targa = targa;
        return this;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AtrAnagrafica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AtrAnagrafica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AtrAnagrafica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AtrAnagrafica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AtrAnagrafica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UniAnagrafica getAtrAnaToUniAna() {
        return atrAnaToUniAna;
    }

    public AtrAnagrafica atrAnaToUniAna(UniAnagrafica uniAnagrafica) {
        this.atrAnaToUniAna = uniAnagrafica;
        return this;
    }

    public void setAtrAnaToUniAna(UniAnagrafica uniAnagrafica) {
        this.atrAnaToUniAna = uniAnagrafica;
    }

    public AtrTipologia getAtrAnaToAtrTipo() {
        return atrAnaToAtrTipo;
    }

    public AtrAnagrafica atrAnaToAtrTipo(AtrTipologia atrTipologia) {
        this.atrAnaToAtrTipo = atrTipologia;
        return this;
    }

    public void setAtrAnaToAtrTipo(AtrTipologia atrTipologia) {
        this.atrAnaToAtrTipo = atrTipologia;
    }

    public AtrMarchio getAtrAnagToAtrMarc() {
        return atrAnagToAtrMarc;
    }

    public AtrAnagrafica atrAnagToAtrMarc(AtrMarchio atrMarchio) {
        this.atrAnagToAtrMarc = atrMarchio;
        return this;
    }

    public void setAtrAnagToAtrMarc(AtrMarchio atrMarchio) {
        this.atrAnagToAtrMarc = atrMarchio;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AtrAnagrafica)) {
            return false;
        }
        return id != null && id.equals(((AtrAnagrafica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AtrAnagrafica{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", modello='" + getModello() + "'" +
            ", dataRevisione='" + getDataRevisione() + "'" +
            ", cavalli=" + getCavalli() +
            ", capacita=" + getCapacita() +
            ", targa='" + getTarga() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
