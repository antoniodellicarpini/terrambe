package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A MagUbicazione.
 */
@Entity
@Table(name = "mag_ubicazione")
@DynamicUpdate
public class MagUbicazione implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @NotNull
    @Column(name = "data_inizio_mag", nullable = false)
    private LocalDate dataInizioMag;

    @NotNull
    @Column(name = "data_fine_mag", nullable = false)
    private LocalDate dataFineMag;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ind_to_mag_ubi_id")
    @JsonIgnoreProperties("indToMagUbi") 
    //@OneToMany(mappedBy = "indToMagUbi")
    private Set<IndMagUbi> magUbiToInds = new HashSet<>();

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "rec_to_mag_ubi_id")
    @JsonIgnoreProperties("recToMagUbi") 
    //@OneToMany(mappedBy = "recToMagUbi")
    private Set<RecMagUbi> magUbiToRecs = new HashSet<>();

    @OneToMany(mappedBy = "magUbicazione")
    private Set<RegCaricoTrappole> regcaricotrappoles = new HashSet<>();

    @OneToMany(mappedBy = "magUbicazione")
    private Set<RegCaricoFitoFarmaco> regcaricofitofarmacos = new HashSet<>();

    @OneToMany(mappedBy = "magUbicazione")
    private Set<RegCaricoFertilizzante> regcaricofertilizzantes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("magUbicaziones")
    private UniAnagrafica unianagrafica;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public MagUbicazione idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public MagUbicazione descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizioMag() {
        return dataInizioMag;
    }

    public MagUbicazione dataInizioMag(LocalDate dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
        return this;
    }

    public void setDataInizioMag(LocalDate dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
    }

    public LocalDate getDataFineMag() {
        return dataFineMag;
    }

    public MagUbicazione dataFineMag(LocalDate dataFineMag) {
        this.dataFineMag = dataFineMag;
        return this;
    }

    public void setDataFineMag(LocalDate dataFineMag) {
        this.dataFineMag = dataFineMag;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public MagUbicazione userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public MagUbicazione userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public MagUbicazione note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<IndMagUbi> getMagUbiToInds() {
        return magUbiToInds;
    }

    public MagUbicazione magUbiToInds(Set<IndMagUbi> indMagUbis) {
        this.magUbiToInds = indMagUbis;
        return this;
    }

    public MagUbicazione addMagUbiToInd(IndMagUbi indMagUbi) {
        this.magUbiToInds.add(indMagUbi);
        indMagUbi.setIndToMagUbi(this);
        return this;
    }

    public MagUbicazione removeMagUbiToInd(IndMagUbi indMagUbi) {
        this.magUbiToInds.remove(indMagUbi);
        indMagUbi.setIndToMagUbi(null);
        return this;
    }

    public void setMagUbiToInds(Set<IndMagUbi> indMagUbis) {
        this.magUbiToInds = indMagUbis;
    }

    public Set<RecMagUbi> getMagUbiToRecs() {
        return magUbiToRecs;
    }

    public MagUbicazione magUbiToRecs(Set<RecMagUbi> recMagUbis) {
        this.magUbiToRecs = recMagUbis;
        return this;
    }

    public MagUbicazione addMagUbiToRec(RecMagUbi recMagUbi) {
        this.magUbiToRecs.add(recMagUbi);
        recMagUbi.setRecToMagUbi(this);
        return this;
    }

    public MagUbicazione removeMagUbiToRec(RecMagUbi recMagUbi) {
        this.magUbiToRecs.remove(recMagUbi);
        recMagUbi.setRecToMagUbi(null);
        return this;
    }

    public void setMagUbiToRecs(Set<RecMagUbi> recMagUbis) {
        this.magUbiToRecs = recMagUbis;
    }

    public Set<RegCaricoTrappole> getRegcaricotrappoles() {
        return regcaricotrappoles;
    }

    public MagUbicazione regcaricotrappoles(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.regcaricotrappoles = regCaricoTrappoles;
        return this;
    }

    public MagUbicazione addRegcaricotrappole(RegCaricoTrappole regCaricoTrappole) {
        this.regcaricotrappoles.add(regCaricoTrappole);
        regCaricoTrappole.setMagUbicazione(this);
        return this;
    }

    public MagUbicazione removeRegcaricotrappole(RegCaricoTrappole regCaricoTrappole) {
        this.regcaricotrappoles.remove(regCaricoTrappole);
        regCaricoTrappole.setMagUbicazione(null);
        return this;
    }

    public void setRegcaricotrappoles(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.regcaricotrappoles = regCaricoTrappoles;
    }

    public Set<RegCaricoFitoFarmaco> getRegcaricofitofarmacos() {
        return regcaricofitofarmacos;
    }

    public MagUbicazione regcaricofitofarmacos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.regcaricofitofarmacos = regCaricoFitoFarmacos;
        return this;
    }

    public MagUbicazione addRegcaricofitofarmaco(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.regcaricofitofarmacos.add(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setMagUbicazione(this);
        return this;
    }

    public MagUbicazione removeRegcaricofitofarmaco(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.regcaricofitofarmacos.remove(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setMagUbicazione(null);
        return this;
    }

    public void setRegcaricofitofarmacos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.regcaricofitofarmacos = regCaricoFitoFarmacos;
    }

    public Set<RegCaricoFertilizzante> getRegcaricofertilizzantes() {
        return regcaricofertilizzantes;
    }

    public MagUbicazione regcaricofertilizzantes(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.regcaricofertilizzantes = regCaricoFertilizzantes;
        return this;
    }

    public MagUbicazione addRegcaricofertilizzante(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.regcaricofertilizzantes.add(regCaricoFertilizzante);
        regCaricoFertilizzante.setMagUbicazione(this);
        return this;
    }

    public MagUbicazione removeRegcaricofertilizzante(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.regcaricofertilizzantes.remove(regCaricoFertilizzante);
        regCaricoFertilizzante.setMagUbicazione(null);
        return this;
    }

    public void setRegcaricofertilizzantes(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.regcaricofertilizzantes = regCaricoFertilizzantes;
    }

    public UniAnagrafica getUnianagrafica() {
        return unianagrafica;
    }

    public MagUbicazione unianagrafica(UniAnagrafica uniAnagrafica) {
        this.unianagrafica = uniAnagrafica;
        return this;
    }

    public void setUnianagrafica(UniAnagrafica uniAnagrafica) {
        this.unianagrafica = uniAnagrafica;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MagUbicazione)) {
            return false;
        }
        return id != null && id.equals(((MagUbicazione) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MagUbicazione{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizioMag='" + getDataInizioMag() + "'" +
            ", dataFineMag='" + getDataFineMag() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
