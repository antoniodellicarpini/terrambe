package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A AziSupPartDettaglio.
 */
@Entity
@Table(name = "azi_sup_part_dettaglio")
@DynamicUpdate
public class AziSupPartDettaglio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "tara")
    private Float tara;

    @Column(name = "uuid_esri")
    private String uuidEsri;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("partToDettaglios")
    private AziSuperficieParticella dettaglioToPart;

    @ManyToOne
    @JsonIgnoreProperties("ageaToDetts")
    private Agea20152020Matrice dettToAgea;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public AziSupPartDettaglio idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public AziSupPartDettaglio idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Float getTara() {
        return tara;
    }

    public AziSupPartDettaglio tara(Float tara) {
        this.tara = tara;
        return this;
    }

    public void setTara(Float tara) {
        this.tara = tara;
    }

    public String getUuidEsri() {
        return uuidEsri;
    }

    public AziSupPartDettaglio uuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
        return this;
    }

    public void setUuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AziSupPartDettaglio dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AziSupPartDettaglio dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AziSupPartDettaglio userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AziSupPartDettaglio userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AziSupPartDettaglio note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public AziSuperficieParticella getDettaglioToPart() {
        return dettaglioToPart;
    }

    public AziSupPartDettaglio dettaglioToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.dettaglioToPart = aziSuperficieParticella;
        return this;
    }

    public void setDettaglioToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.dettaglioToPart = aziSuperficieParticella;
    }

    public Agea20152020Matrice getDettToAgea() {
        return dettToAgea;
    }

    public AziSupPartDettaglio dettToAgea(Agea20152020Matrice agea20152020Matrice) {
        this.dettToAgea = agea20152020Matrice;
        return this;
    }

    public void setDettToAgea(Agea20152020Matrice agea20152020Matrice) {
        this.dettToAgea = agea20152020Matrice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AziSupPartDettaglio)) {
            return false;
        }
        return id != null && id.equals(((AziSupPartDettaglio) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AziSupPartDettaglio{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", tara=" + getTara() +
            ", uuidEsri='" + getUuidEsri() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
