package com.terrambe.domain;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FertTipo.
 */
@Entity
@Table(name = "fert_tipo")
@DynamicUpdate
public class FertTipo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descrizione")
    private String descrizione;

    @OneToMany(mappedBy = "fertTipo")
    private Set<FerAnagrafica> ferAnagraficas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public FertTipo descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Set<FerAnagrafica> getFerAnagraficas() {
        return ferAnagraficas;
    }

    public FertTipo ferAnagraficas(Set<FerAnagrafica> ferAnagraficas) {
        this.ferAnagraficas = ferAnagraficas;
        return this;
    }

    public FertTipo addFerAnagrafica(FerAnagrafica ferAnagrafica) {
        this.ferAnagraficas.add(ferAnagrafica);
        ferAnagrafica.setFertTipo(this);
        return this;
    }

    public FertTipo removeFerAnagrafica(FerAnagrafica ferAnagrafica) {
        this.ferAnagraficas.remove(ferAnagrafica);
        ferAnagrafica.setFertTipo(null);
        return this;
    }

    public void setFerAnagraficas(Set<FerAnagrafica> ferAnagraficas) {
        this.ferAnagraficas = ferAnagraficas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FertTipo)) {
            return false;
        }
        return id != null && id.equals(((FertTipo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FertTipo{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            "}";
    }
}
