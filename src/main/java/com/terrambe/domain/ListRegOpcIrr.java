package com.terrambe.domain;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * A EtichettaFitoFilter.
 */

public class ListRegOpcIrr implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private LocalDate data_operazione;
    private String desc_tipo;
    private String desc_coltura;
    private String desc_operatore;
    private Float volume_acqua;
    private String desc_appezzamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataOperazione() {
        return data_operazione;
    }

    public void setDataOperazione(LocalDate data_operazione) {
        this.data_operazione = data_operazione;
    }

    public String getDescOperatore() {
        return desc_operatore;
    }

    public void setDescOperatore(String desc_operatore) {
        this.desc_operatore = desc_operatore;
    }

    public String getDescColtura() {
        return desc_coltura;
    }

    public void setDescColtura(String desc_coltura) {
        this.desc_coltura = desc_coltura;
    }

    public Float getVolumeacqua() {
        return volume_acqua;
    }

    public void setVolumeacqua(Float volume_acqua) {
        this.volume_acqua = volume_acqua;
    }

    public String getDescTipo() {
        return desc_tipo;
    }

    public void setDescTipo(String desc_prepterr) {
        this.desc_tipo = desc_prepterr;
    }

    public String getDescAppezzamento() {
        return desc_appezzamento;
    }

    public void setDescAppezzamento(String desc_appezzamento) {
        this.desc_appezzamento = desc_appezzamento;
    }


    @Override
    public String toString() {
        return "ListRegOpcIrr{" +
            "id=" + id +
            ", data_operazione=" + data_operazione +
            ", desc_operatore=" + desc_operatore +
            ", desc_coltura=" + desc_coltura +
            ", volume_acqua=" + volume_acqua +
            ", desc_tipo=" + desc_tipo +
            ", desc_appezzamento=" + desc_appezzamento +
            '}';
    }
}

