package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

	//MODIFICA -----------------------------------------
    import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A AziRappresentanteCli.
 */
@Entity
@Table(name = "azi_rappresentante_cli")
@DynamicUpdate
public class AziRappresentanteCli implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "sesso")
    private String sesso;

    @Column(name = "data_nascita")
    private LocalDate dataNascita;

    @Size(min = 16, max = 16)
    @Column(name = "codice_fiscale", length = 16)
    private String codiceFiscale;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

	//MODIFICA -----------------------------------------
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(unique = true)
	private IndNascRapCli cliRapToIndNasc;

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "cli_ana_to_cli_rap_id")
	@JsonIgnoreProperties("cliAnaToCliRap")
	//@OneToMany(mappedBy = "cliAnaToCliRap")
	private Set<Cliente> cliRapToCliAnas = new HashSet<>();

	//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "ind_to_cli_rap_id")
	@JsonIgnoreProperties("indToCliRap")
	//@OneToMany(mappedBy = "indToCliRap")
    private Set<IndAziRapCli> cliRapToInds = new HashSet<>();
    
    //MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "rec_to_cli_rap_id")
	@JsonIgnoreProperties("recToCliRap")
	//@OneToMany(mappedBy = "recToCliRap")
	private Set<RecAziRapCli> cliRapToRecs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public AziRappresentanteCli nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public AziRappresentanteCli cognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public AziRappresentanteCli sesso(String sesso) {
        this.sesso = sesso;
        return this;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public LocalDate getDataNascita() {
        return dataNascita;
    }

    public AziRappresentanteCli dataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
        return this;
    }

    public void setDataNascita(LocalDate dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public AziRappresentanteCli codiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
        return this;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AziRappresentanteCli dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AziRappresentanteCli dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AziRappresentanteCli userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AziRappresentanteCli userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AziRappresentanteCli note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public IndNascRapCli getCliRapToIndNasc() {
        return cliRapToIndNasc;
    }

    public AziRappresentanteCli cliRapToIndNasc(IndNascRapCli indNascRapCli) {
        this.cliRapToIndNasc = indNascRapCli;
        return this;
    }

    public void setCliRapToIndNasc(IndNascRapCli indNascRapCli) {
        this.cliRapToIndNasc = indNascRapCli;
    }

    public Set<Cliente> getCliRapToCliAnas() {
        return cliRapToCliAnas;
    }

    public AziRappresentanteCli cliRapToCliAnas(Set<Cliente> clientes) {
        this.cliRapToCliAnas = clientes;
        return this;
    }

    public AziRappresentanteCli addCliRapToCliAna(Cliente cliente) {
        this.cliRapToCliAnas.add(cliente);
        cliente.setCliAnaToCliRap(this);
        return this;
    }

    public AziRappresentanteCli removeCliRapToCliAna(Cliente cliente) {
        this.cliRapToCliAnas.remove(cliente);
        cliente.setCliAnaToCliRap(null);
        return this;
    }

    public void setCliRapToCliAnas(Set<Cliente> clientes) {
        this.cliRapToCliAnas = clientes;
    }

    public Set<IndAziRapCli> getCliRapToInds() {
        return cliRapToInds;
    }

    public AziRappresentanteCli cliRapToInds(Set<IndAziRapCli> indAziRapClis) {
        this.cliRapToInds = indAziRapClis;
        return this;
    }

    public AziRappresentanteCli addCliRapToInd(IndAziRapCli indAziRapCli) {
        this.cliRapToInds.add(indAziRapCli);
        indAziRapCli.setIndToCliRap(this);
        return this;
    }

    public AziRappresentanteCli removeCliRapToInd(IndAziRapCli indAziRapCli) {
        this.cliRapToInds.remove(indAziRapCli);
        indAziRapCli.setIndToCliRap(null);
        return this;
    }

    public void setCliRapToInds(Set<IndAziRapCli> indAziRapClis) {
        this.cliRapToInds = indAziRapClis;
    }

    public Set<RecAziRapCli> getCliRapToRecs() {
        return cliRapToRecs;
    }

    public AziRappresentanteCli cliRapToRecs(Set<RecAziRapCli> recAziRapClis) {
        this.cliRapToRecs = recAziRapClis;
        return this;
    }

    public AziRappresentanteCli addCliRapToRec(RecAziRapCli recAziRapCli) {
        this.cliRapToRecs.add(recAziRapCli);
        recAziRapCli.setRecToCliRap(this);
        return this;
    }

    public AziRappresentanteCli removeCliRapToRec(RecAziRapCli recAziRapCli) {
        this.cliRapToRecs.remove(recAziRapCli);
        recAziRapCli.setRecToCliRap(null);
        return this;
    }

    public void setCliRapToRecs(Set<RecAziRapCli> recAziRapClis) {
        this.cliRapToRecs = recAziRapClis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AziRappresentanteCli)) {
            return false;
        }
        return id != null && id.equals(((AziRappresentanteCli) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AziRappresentanteCli{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", sesso='" + getSesso() + "'" +
            ", dataNascita='" + getDataNascita() + "'" +
            ", codiceFiscale='" + getCodiceFiscale() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
