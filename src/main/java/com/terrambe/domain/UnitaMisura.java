package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A UnitaMisura.
 */
@Entity
@Table(name = "unita_misura")
@DynamicUpdate
public class UnitaMisura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cod_um_dose")
    private String codUmDose;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "doseToUniMisu")
    private Set<Dose> uniMisuToDoses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodUmDose() {
        return codUmDose;
    }

    public UnitaMisura codUmDose(String codUmDose) {
        this.codUmDose = codUmDose;
        return this;
    }

    public void setCodUmDose(String codUmDose) {
        this.codUmDose = codUmDose;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public UnitaMisura descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public UnitaMisura tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public UnitaMisura operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public UnitaMisura ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public UnitaMisura dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public UnitaMisura dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public UnitaMisura userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public UnitaMisura userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public UnitaMisura note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Dose> getUniMisuToDoses() {
        return uniMisuToDoses;
    }

    public UnitaMisura uniMisuToDoses(Set<Dose> doses) {
        this.uniMisuToDoses = doses;
        return this;
    }

    public UnitaMisura addUniMisuToDose(Dose dose) {
        this.uniMisuToDoses.add(dose);
        dose.setDoseToUniMisu(this);
        return this;
    }

    public UnitaMisura removeUniMisuToDose(Dose dose) {
        this.uniMisuToDoses.remove(dose);
        dose.setDoseToUniMisu(null);
        return this;
    }

    public void setUniMisuToDoses(Set<Dose> doses) {
        this.uniMisuToDoses = doses;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UnitaMisura)) {
            return false;
        }
        return id != null && id.equals(((UnitaMisura) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UnitaMisura{" +
            "id=" + getId() +
            ", codUmDose='" + getCodUmDose() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
