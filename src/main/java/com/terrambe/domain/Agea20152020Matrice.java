package com.terrambe.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Agea20152020Matrice.
 */
@Entity
@Table(name = "agea_20152020_matrice")
public class Agea20152020Matrice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "desc_coltura")
    private String descColtura;

    @Column(name = "cod_macro_uso")
    private String codMacroUso;

    @Column(name = "macro_uso")
    private String macroUso;

    @Column(name = "cod_occupazione_suolo")
    private String codOccupazioneSuolo;

    @Column(name = "occupazione_suolo")
    private String occupazioneSuolo;

    @Column(name = "cod_destinazione")
    private String codDestinazione;

    @Column(name = "destinazione")
    private String destinazione;

    @Column(name = "cod_uso")
    private String codUso;

    @Column(name = "uso")
    private String uso;

    @Column(name = "cod_qualita")
    private String codQualita;

    @Column(name = "qualita")
    private String qualita;

    @Column(name = "cod_varieta")
    private String codVarieta;

    @Column(name = "varieta")
    private String varieta;

    @Column(name = "cod_famiglia")
    private String codFamiglia;

    @Column(name = "famiglia")
    private String famiglia;

    @Column(name = "cod_genere")
    private String codGenere;

    @Column(name = "genere")
    private String genere;

    @Column(name = "cod_specie")
    private String codSpecie;

    @Column(name = "specie")
    private String specie;

    @Column(name = "versione_inserimento")
    private String versioneInserimento;

    @Column(name = "codice_composto_agea")
    private String codiceCompostoAgea;

    @OneToMany(mappedBy = "dettToAgea")
    private Set<AziSupPartDettaglio> ageaToDetts = new HashSet<>();

    @OneToMany(mappedBy = "coltToAgea")
    private Set<ColturaAppezzamento> ageaToColts = new HashSet<>();

    @OneToMany(mappedBy = "bdfToMAtrice")
    private Set<Agea20152020BDF> matriceToBdfs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescColtura() {
        return descColtura;
    }

    public Agea20152020Matrice descColtura(String descColtura) {
        this.descColtura = descColtura;
        return this;
    }

    public void setDescColtura(String descColtura) {
        this.descColtura = descColtura;
    }

    public String getCodMacroUso() {
        return codMacroUso;
    }

    public Agea20152020Matrice codMacroUso(String codMacroUso) {
        this.codMacroUso = codMacroUso;
        return this;
    }

    public void setCodMacroUso(String codMacroUso) {
        this.codMacroUso = codMacroUso;
    }

    public String getMacroUso() {
        return macroUso;
    }

    public Agea20152020Matrice macroUso(String macroUso) {
        this.macroUso = macroUso;
        return this;
    }

    public void setMacroUso(String macroUso) {
        this.macroUso = macroUso;
    }

    public String getCodOccupazioneSuolo() {
        return codOccupazioneSuolo;
    }

    public Agea20152020Matrice codOccupazioneSuolo(String codOccupazioneSuolo) {
        this.codOccupazioneSuolo = codOccupazioneSuolo;
        return this;
    }

    public void setCodOccupazioneSuolo(String codOccupazioneSuolo) {
        this.codOccupazioneSuolo = codOccupazioneSuolo;
    }

    public String getOccupazioneSuolo() {
        return occupazioneSuolo;
    }

    public Agea20152020Matrice occupazioneSuolo(String occupazioneSuolo) {
        this.occupazioneSuolo = occupazioneSuolo;
        return this;
    }

    public void setOccupazioneSuolo(String occupazioneSuolo) {
        this.occupazioneSuolo = occupazioneSuolo;
    }

    public String getCodDestinazione() {
        return codDestinazione;
    }

    public Agea20152020Matrice codDestinazione(String codDestinazione) {
        this.codDestinazione = codDestinazione;
        return this;
    }

    public void setCodDestinazione(String codDestinazione) {
        this.codDestinazione = codDestinazione;
    }

    public String getDestinazione() {
        return destinazione;
    }

    public Agea20152020Matrice destinazione(String destinazione) {
        this.destinazione = destinazione;
        return this;
    }

    public void setDestinazione(String destinazione) {
        this.destinazione = destinazione;
    }

    public String getCodUso() {
        return codUso;
    }

    public Agea20152020Matrice codUso(String codUso) {
        this.codUso = codUso;
        return this;
    }

    public void setCodUso(String codUso) {
        this.codUso = codUso;
    }

    public String getUso() {
        return uso;
    }

    public Agea20152020Matrice uso(String uso) {
        this.uso = uso;
        return this;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public String getCodQualita() {
        return codQualita;
    }

    public Agea20152020Matrice codQualita(String codQualita) {
        this.codQualita = codQualita;
        return this;
    }

    public void setCodQualita(String codQualita) {
        this.codQualita = codQualita;
    }

    public String getQualita() {
        return qualita;
    }

    public Agea20152020Matrice qualita(String qualita) {
        this.qualita = qualita;
        return this;
    }

    public void setQualita(String qualita) {
        this.qualita = qualita;
    }

    public String getCodVarieta() {
        return codVarieta;
    }

    public Agea20152020Matrice codVarieta(String codVarieta) {
        this.codVarieta = codVarieta;
        return this;
    }

    public void setCodVarieta(String codVarieta) {
        this.codVarieta = codVarieta;
    }

    public String getVarieta() {
        return varieta;
    }

    public Agea20152020Matrice varieta(String varieta) {
        this.varieta = varieta;
        return this;
    }

    public void setVarieta(String varieta) {
        this.varieta = varieta;
    }

    public String getCodFamiglia() {
        return codFamiglia;
    }

    public Agea20152020Matrice codFamiglia(String codFamiglia) {
        this.codFamiglia = codFamiglia;
        return this;
    }

    public void setCodFamiglia(String codFamiglia) {
        this.codFamiglia = codFamiglia;
    }

    public String getFamiglia() {
        return famiglia;
    }

    public Agea20152020Matrice famiglia(String famiglia) {
        this.famiglia = famiglia;
        return this;
    }

    public void setFamiglia(String famiglia) {
        this.famiglia = famiglia;
    }

    public String getCodGenere() {
        return codGenere;
    }

    public Agea20152020Matrice codGenere(String codGenere) {
        this.codGenere = codGenere;
        return this;
    }

    public void setCodGenere(String codGenere) {
        this.codGenere = codGenere;
    }

    public String getGenere() {
        return genere;
    }

    public Agea20152020Matrice genere(String genere) {
        this.genere = genere;
        return this;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public String getCodSpecie() {
        return codSpecie;
    }

    public Agea20152020Matrice codSpecie(String codSpecie) {
        this.codSpecie = codSpecie;
        return this;
    }

    public void setCodSpecie(String codSpecie) {
        this.codSpecie = codSpecie;
    }

    public String getSpecie() {
        return specie;
    }

    public Agea20152020Matrice specie(String specie) {
        this.specie = specie;
        return this;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getVersioneInserimento() {
        return versioneInserimento;
    }

    public Agea20152020Matrice versioneInserimento(String versioneInserimento) {
        this.versioneInserimento = versioneInserimento;
        return this;
    }

    public void setVersioneInserimento(String versioneInserimento) {
        this.versioneInserimento = versioneInserimento;
    }

    public String getCodiceCompostoAgea() {
        return codiceCompostoAgea;
    }

    public Agea20152020Matrice codiceCompostoAgea(String codiceCompostoAgea) {
        this.codiceCompostoAgea = codiceCompostoAgea;
        return this;
    }

    public void setCodiceCompostoAgea(String codiceCompostoAgea) {
        this.codiceCompostoAgea = codiceCompostoAgea;
    }

    public Set<AziSupPartDettaglio> getAgeaToDetts() {
        return ageaToDetts;
    }

    public Agea20152020Matrice ageaToDetts(Set<AziSupPartDettaglio> aziSupPartDettaglios) {
        this.ageaToDetts = aziSupPartDettaglios;
        return this;
    }

    public Agea20152020Matrice addAgeaToDett(AziSupPartDettaglio aziSupPartDettaglio) {
        this.ageaToDetts.add(aziSupPartDettaglio);
        aziSupPartDettaglio.setDettToAgea(this);
        return this;
    }

    public Agea20152020Matrice removeAgeaToDett(AziSupPartDettaglio aziSupPartDettaglio) {
        this.ageaToDetts.remove(aziSupPartDettaglio);
        aziSupPartDettaglio.setDettToAgea(null);
        return this;
    }

    public void setAgeaToDetts(Set<AziSupPartDettaglio> aziSupPartDettaglios) {
        this.ageaToDetts = aziSupPartDettaglios;
    }

    public Set<ColturaAppezzamento> getAgeaToColts() {
        return ageaToColts;
    }

    public Agea20152020Matrice ageaToColts(Set<ColturaAppezzamento> colturaAppezzamentos) {
        this.ageaToColts = colturaAppezzamentos;
        return this;
    }

    public Agea20152020Matrice addAgeaToColt(ColturaAppezzamento colturaAppezzamento) {
        this.ageaToColts.add(colturaAppezzamento);
        colturaAppezzamento.setColtToAgea(this);
        return this;
    }

    public Agea20152020Matrice removeAgeaToColt(ColturaAppezzamento colturaAppezzamento) {
        this.ageaToColts.remove(colturaAppezzamento);
        colturaAppezzamento.setColtToAgea(null);
        return this;
    }

    public void setAgeaToColts(Set<ColturaAppezzamento> colturaAppezzamentos) {
        this.ageaToColts = colturaAppezzamentos;
    }

    public Set<Agea20152020BDF> getMatriceToBdfs() {
        return matriceToBdfs;
    }

    public Agea20152020Matrice matriceToBdfs(Set<Agea20152020BDF> agea20152020BDFS) {
        this.matriceToBdfs = agea20152020BDFS;
        return this;
    }

    public Agea20152020Matrice addMatriceToBdf(Agea20152020BDF agea20152020BDF) {
        this.matriceToBdfs.add(agea20152020BDF);
        agea20152020BDF.setBdfToMAtrice(this);
        return this;
    }

    public Agea20152020Matrice removeMatriceToBdf(Agea20152020BDF agea20152020BDF) {
        this.matriceToBdfs.remove(agea20152020BDF);
        agea20152020BDF.setBdfToMAtrice(null);
        return this;
    }

    public void setMatriceToBdfs(Set<Agea20152020BDF> agea20152020BDFS) {
        this.matriceToBdfs = agea20152020BDFS;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agea20152020Matrice)) {
            return false;
        }
        return id != null && id.equals(((Agea20152020Matrice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Agea20152020Matrice{" +
            "id=" + getId() +
            ", descColtura='" + getDescColtura() + "'" +
            ", codMacroUso='" + getCodMacroUso() + "'" +
            ", macroUso='" + getMacroUso() + "'" +
            ", codOccupazioneSuolo='" + getCodOccupazioneSuolo() + "'" +
            ", occupazioneSuolo='" + getOccupazioneSuolo() + "'" +
            ", codDestinazione='" + getCodDestinazione() + "'" +
            ", destinazione='" + getDestinazione() + "'" +
            ", codUso='" + getCodUso() + "'" +
            ", uso='" + getUso() + "'" +
            ", codQualita='" + getCodQualita() + "'" +
            ", qualita='" + getQualita() + "'" +
            ", codVarieta='" + getCodVarieta() + "'" +
            ", varieta='" + getVarieta() + "'" +
            ", codFamiglia='" + getCodFamiglia() + "'" +
            ", famiglia='" + getFamiglia() + "'" +
            ", codGenere='" + getCodGenere() + "'" +
            ", genere='" + getGenere() + "'" +
            ", codSpecie='" + getCodSpecie() + "'" +
            ", specie='" + getSpecie() + "'" +
            ", versioneInserimento='" + getVersioneInserimento() + "'" +
            ", codiceCompostoAgea='" + getCodiceCompostoAgea() + "'" +
            "}";
    }
}
