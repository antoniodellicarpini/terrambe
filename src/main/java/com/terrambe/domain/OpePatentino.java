package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A OpePatentino.
 */
@Entity
@Table(name = "ope_patentino")
@DynamicUpdate
public class OpePatentino implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @NotNull
    @Column(name = "data_rilascio", nullable = false)
    private LocalDate dataRilascio;

    @NotNull
    @Column(name = "data_scadenza", nullable = false)
    private LocalDate dataScadenza;

    @Column(name = "ente_rilasciato")
    private String enteRilasciato;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali= LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("opeAnagToPates")
    private OpeAnagrafica pateToOpeAnag;

    @ManyToOne
    @JsonIgnoreProperties("opePatTipToOpePates")
    private OpePatentinoTipologia opepatentinotipologia;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public OpePatentino numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDataRilascio() {
        return dataRilascio;
    }

    public OpePatentino dataRilascio(LocalDate dataRilascio) {
        this.dataRilascio = dataRilascio;
        return this;
    }

    public void setDataRilascio(LocalDate dataRilascio) {
        this.dataRilascio = dataRilascio;
    }

    public LocalDate getDataScadenza() {
        return dataScadenza;
    }

    public OpePatentino dataScadenza(LocalDate dataScadenza) {
        this.dataScadenza = dataScadenza;
        return this;
    }

    public void setDataScadenza(LocalDate dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public String getEnteRilasciato() {
        return enteRilasciato;
    }

    public OpePatentino enteRilasciato(String enteRilasciato) {
        this.enteRilasciato = enteRilasciato;
        return this;
    }

    public void setEnteRilasciato(String enteRilasciato) {
        this.enteRilasciato = enteRilasciato;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public OpePatentino dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public OpePatentino dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public OpePatentino userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public OpePatentino userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public OpePatentino note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public OpeAnagrafica getPateToOpeAnag() {
        return pateToOpeAnag;
    }

    public OpePatentino pateToOpeAnag(OpeAnagrafica opeAnagrafica) {
        this.pateToOpeAnag = opeAnagrafica;
        return this;
    }

    public void setPateToOpeAnag(OpeAnagrafica opeAnagrafica) {
        this.pateToOpeAnag = opeAnagrafica;
    }

    public OpePatentinoTipologia getOpepatentinotipologia() {
        return opepatentinotipologia;
    }

    public OpePatentino opepatentinotipologia(OpePatentinoTipologia opePatentinoTipologia) {
        this.opepatentinotipologia = opePatentinoTipologia;
        return this;
    }

    public void setOpepatentinotipologia(OpePatentinoTipologia opePatentinoTipologia) {
        this.opepatentinotipologia = opePatentinoTipologia;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OpePatentino)) {
            return false;
        }
        return id != null && id.equals(((OpePatentino) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OpePatentino{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", dataRilascio='" + getDataRilascio() + "'" +
            ", dataScadenza='" + getDataScadenza() + "'" +
            ", enteRilasciato='" + getEnteRilasciato() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
