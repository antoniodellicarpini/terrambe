package com.terrambe.domain;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * A EtichettaFitoFilter.
 */

public class ListRegOpcAltro implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private LocalDate data_operazione;
    private String desc_tipo;
    private String desc_coltura;
    private String desc_operatore;
    private String desc_mezzo;
    private String desc_appezzamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataOperazione() {
        return data_operazione;
    }

    public void setDataOperazione(LocalDate data_operazione) {
        this.data_operazione = data_operazione;
    }

    public String getDescOperatore() {
        return desc_operatore;
    }

    public void setDescOperatore(String desc_operatore) {
        this.desc_operatore = desc_operatore;
    }

    public String getDescColtura() {
        return desc_coltura;
    }

    public void setDescColtura(String desc_coltura) {
        this.desc_coltura = desc_coltura;
    }

    public String getDescMezzo() {
        return desc_mezzo;
    }

    public void setDescMezzo(String desc_mezzo) {
        this.desc_mezzo = desc_mezzo;
    }

    public String getDescTipo() {
        return desc_tipo;
    }

    public void setDescTipo(String desc_tipo) {
        this.desc_tipo = desc_tipo;
    }
    
    public String getDescAppezzamento() {
        return desc_appezzamento;
    }

    public void setDescAppezzamento(String desc_appezzamento) {
        this.desc_appezzamento = desc_appezzamento;
    }

    @Override
    public String toString() {
        return "ListRegOpcAltro{" +
            "id=" + id +
            ", data_operazione=" + data_operazione +
            ", desc_operatore=" + desc_operatore +
            ", desc_coltura=" + desc_coltura +
            ", desc_mezzo=" + desc_mezzo +
            ", desc_tipo=" + desc_tipo +
            ", desc_appezzamento=" + desc_appezzamento +
            '}';
    }
}

