package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TipoRegMag.
 */
@Entity
@Table(name = "tipo_reg_mag")
@DynamicUpdate
public class TipoRegMag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @Column(name = "moltiplicatore")
    private Integer moltiplicatore;

    @NotNull
    @Column(name = "data_inizio_mag", nullable = false)
    private LocalDate dataInizioMag;

    @NotNull
    @Column(name = "data_fine_mag", nullable = false)
    private LocalDate dataFineMag;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "regFitoToTipoReg")
    private Set<RegCaricoFitoFarmaco> tipoRegToRegFitos = new HashSet<>();

    @OneToMany(mappedBy = "regFertToTipoReg")
    private Set<RegCaricoFertilizzante> tipoRegToRegFerts = new HashSet<>();

    @OneToMany(mappedBy = "regTrapToTipoReg")
    private Set<RegCaricoTrappole> tipoRegToRegTraps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public TipoRegMag descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Integer getMoltiplicatore() {
        return moltiplicatore;
    }

    public TipoRegMag moltiplicatore(Integer moltiplicatore) {
        this.moltiplicatore = moltiplicatore;
        return this;
    }

    public void setMoltiplicatore(Integer moltiplicatore) {
        this.moltiplicatore = moltiplicatore;
    }

    public LocalDate getDataInizioMag() {
        return dataInizioMag;
    }

    public TipoRegMag dataInizioMag(LocalDate dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
        return this;
    }

    public void setDataInizioMag(LocalDate dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
    }

    public LocalDate getDataFineMag() {
        return dataFineMag;
    }

    public TipoRegMag dataFineMag(LocalDate dataFineMag) {
        this.dataFineMag = dataFineMag;
        return this;
    }

    public void setDataFineMag(LocalDate dataFineMag) {
        this.dataFineMag = dataFineMag;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TipoRegMag userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TipoRegMag userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public TipoRegMag note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegCaricoFitoFarmaco> getTipoRegToRegFitos() {
        return tipoRegToRegFitos;
    }

    public TipoRegMag tipoRegToRegFitos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.tipoRegToRegFitos = regCaricoFitoFarmacos;
        return this;
    }

    public TipoRegMag addTipoRegToRegFito(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.tipoRegToRegFitos.add(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setRegFitoToTipoReg(this);
        return this;
    }

    public TipoRegMag removeTipoRegToRegFito(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        this.tipoRegToRegFitos.remove(regCaricoFitoFarmaco);
        regCaricoFitoFarmaco.setRegFitoToTipoReg(null);
        return this;
    }

    public void setTipoRegToRegFitos(Set<RegCaricoFitoFarmaco> regCaricoFitoFarmacos) {
        this.tipoRegToRegFitos = regCaricoFitoFarmacos;
    }

    public Set<RegCaricoFertilizzante> getTipoRegToRegFerts() {
        return tipoRegToRegFerts;
    }

    public TipoRegMag tipoRegToRegFerts(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.tipoRegToRegFerts = regCaricoFertilizzantes;
        return this;
    }

    public TipoRegMag addTipoRegToRegFert(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.tipoRegToRegFerts.add(regCaricoFertilizzante);
        regCaricoFertilizzante.setRegFertToTipoReg(this);
        return this;
    }

    public TipoRegMag removeTipoRegToRegFert(RegCaricoFertilizzante regCaricoFertilizzante) {
        this.tipoRegToRegFerts.remove(regCaricoFertilizzante);
        regCaricoFertilizzante.setRegFertToTipoReg(null);
        return this;
    }

    public void setTipoRegToRegFerts(Set<RegCaricoFertilizzante> regCaricoFertilizzantes) {
        this.tipoRegToRegFerts = regCaricoFertilizzantes;
    }

    public Set<RegCaricoTrappole> getTipoRegToRegTraps() {
        return tipoRegToRegTraps;
    }

    public TipoRegMag tipoRegToRegTraps(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.tipoRegToRegTraps = regCaricoTrappoles;
        return this;
    }

    public TipoRegMag addTipoRegToRegTrap(RegCaricoTrappole regCaricoTrappole) {
        this.tipoRegToRegTraps.add(regCaricoTrappole);
        regCaricoTrappole.setRegTrapToTipoReg(this);
        return this;
    }

    public TipoRegMag removeTipoRegToRegTrap(RegCaricoTrappole regCaricoTrappole) {
        this.tipoRegToRegTraps.remove(regCaricoTrappole);
        regCaricoTrappole.setRegTrapToTipoReg(null);
        return this;
    }

    public void setTipoRegToRegTraps(Set<RegCaricoTrappole> regCaricoTrappoles) {
        this.tipoRegToRegTraps = regCaricoTrappoles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoRegMag)) {
            return false;
        }
        return id != null && id.equals(((TipoRegMag) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TipoRegMag{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", moltiplicatore=" + getMoltiplicatore() +
            ", dataInizioMag='" + getDataInizioMag() + "'" +
            ", dataFineMag='" + getDataFineMag() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
