package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Appezzamenti.
 */
@Entity
@Table(name = "appezzamenti")
public class Appezzamenti implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @NotNull
    @Column(name = "nome_appezzamento", nullable = false)
    private String nomeAppezzamento;

    @NotNull
    @Column(name = "superfice_ha", nullable = false)
    private Float superficeHa;

    @NotNull
    @Column(name = "rotazione", nullable = false)
    private Boolean rotazione;

    @NotNull
    @Column(name = "serra", nullable = false)
    private Boolean serra;

    @NotNull
    @Column(name = "irrigabile", nullable = false)
    private Boolean irrigabile;

    @NotNull
    @Column(name = "uuid_esri", nullable = false)
    private String uuidEsri;

    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToMany
    @JoinTable(name = "appezzamenti_appz_to_part",
               joinColumns = @JoinColumn(name = "appezzamenti_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "appz_to_part_id", referencedColumnName = "id"))
    private Set<AziSuperficieParticella> appzToParts = new HashSet<>();

//MODIFICA ------------------------------------------
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "appezzamenti_appez_to_vincoli_ter",
               joinColumns = @JoinColumn(name = "appezzamenti_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "appez_to_vincoli_ter_id", referencedColumnName = "id"))
    private Set<TipologiaVincoliTerritoriali> appezToVincoliTers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("campiToAppezs")
    private Campi appezToCampi;

    @ManyToOne
    @JsonIgnoreProperties("bioAppzTipoToAppzs")
    private BioAppzTipo appzToBioAppzTipo;


//MODIFICA -----------------------------------------
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "colt_to_appe_id")
	@JsonIgnoreProperties(value = {"coltToAppe"}, allowSetters = true)
	//@OneToMany(mappedBy = "coltToAppe")
	private Set<ColturaAppezzamento> appeToColts = new HashSet<>();


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public Appezzamenti idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public Appezzamenti idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public String getNomeAppezzamento() {
        return nomeAppezzamento;
    }

    public Appezzamenti nomeAppezzamento(String nomeAppezzamento) {
        this.nomeAppezzamento = nomeAppezzamento;
        return this;
    }

    public void setNomeAppezzamento(String nomeAppezzamento) {
        this.nomeAppezzamento = nomeAppezzamento;
    }

    public Float getSuperficeHa() {
        return superficeHa;
    }

    public Appezzamenti superficeHa(Float superficeHa) {
        this.superficeHa = superficeHa;
        return this;
    }

    public void setSuperficeHa(Float superficeHa) {
        this.superficeHa = superficeHa;
    }

    public Boolean isRotazione() {
        return rotazione;
    }

    public Appezzamenti rotazione(Boolean rotazione) {
        this.rotazione = rotazione;
        return this;
    }

    public void setRotazione(Boolean rotazione) {
        this.rotazione = rotazione;
    }

    public Boolean isSerra() {
        return serra;
    }

    public Appezzamenti serra(Boolean serra) {
        this.serra = serra;
        return this;
    }

    public void setSerra(Boolean serra) {
        this.serra = serra;
    }

    public Boolean isIrrigabile() {
        return irrigabile;
    }

    public Appezzamenti irrigabile(Boolean irrigabile) {
        this.irrigabile = irrigabile;
        return this;
    }

    public void setIrrigabile(Boolean irrigabile) {
        this.irrigabile = irrigabile;
    }

    public String getUuidEsri() {
        return uuidEsri;
    }

    public Appezzamenti uuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
        return this;
    }

    public void setUuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Appezzamenti dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Appezzamenti dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Appezzamenti userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Appezzamenti userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Appezzamenti note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<AziSuperficieParticella> getAppzToParts() {
        return appzToParts;
    }

    public Appezzamenti appzToParts(Set<AziSuperficieParticella> aziSuperficieParticellas) {
        this.appzToParts = aziSuperficieParticellas;
        return this;
    }

    public Appezzamenti addAppzToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.appzToParts.add(aziSuperficieParticella);
        aziSuperficieParticella.getPartToAppzs().add(this);
        return this;
    }

    public Appezzamenti removeAppzToPart(AziSuperficieParticella aziSuperficieParticella) {
        this.appzToParts.remove(aziSuperficieParticella);
        aziSuperficieParticella.getPartToAppzs().remove(this);
        return this;
    }

    public void setAppzToParts(Set<AziSuperficieParticella> aziSuperficieParticellas) {
        this.appzToParts = aziSuperficieParticellas;
    }

    public Set<TipologiaVincoliTerritoriali> getAppezToVincoliTers() {
        return appezToVincoliTers;
    }

    public Appezzamenti appezToVincoliTers(Set<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialis) {
        this.appezToVincoliTers = tipologiaVincoliTerritorialis;
        return this;
    }

    public Appezzamenti addAppezToVincoliTer(TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali) {
        this.appezToVincoliTers.add(tipologiaVincoliTerritoriali);
        tipologiaVincoliTerritoriali.getVincoliTerToAppezs().add(this);
        return this;
    }

    public Appezzamenti removeAppezToVincoliTer(TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali) {
        this.appezToVincoliTers.remove(tipologiaVincoliTerritoriali);
        tipologiaVincoliTerritoriali.getVincoliTerToAppezs().remove(this);
        return this;
    }

    public void setAppezToVincoliTers(Set<TipologiaVincoliTerritoriali> tipologiaVincoliTerritorialis) {
        this.appezToVincoliTers = tipologiaVincoliTerritorialis;
    }

    public Campi getAppezToCampi() {
        return appezToCampi;
    }

    public Appezzamenti appezToCampi(Campi campi) {
        this.appezToCampi = campi;
        return this;
    }

    public void setAppezToCampi(Campi campi) {
        this.appezToCampi = campi;
    }

    public BioAppzTipo getAppzToBioAppzTipo() {
        return appzToBioAppzTipo;
    }

    public Appezzamenti appzToBioAppzTipo(BioAppzTipo bioAppzTipo) {
        this.appzToBioAppzTipo = bioAppzTipo;
        return this;
    }

    public void setAppzToBioAppzTipo(BioAppzTipo bioAppzTipo) {
        this.appzToBioAppzTipo = bioAppzTipo;
    }

    public Set<ColturaAppezzamento> getAppeToColts() {
        return appeToColts;
    }

    public Appezzamenti appeToColts(Set<ColturaAppezzamento> colturaAppezzamentos) {
        this.appeToColts = colturaAppezzamentos;
        return this;
    }

    public Appezzamenti addAppeToColt(ColturaAppezzamento colturaAppezzamento) {
        this.appeToColts.add(colturaAppezzamento);
        colturaAppezzamento.setColtToAppe(this);
        return this;
    }

    public Appezzamenti removeAppeToColt(ColturaAppezzamento colturaAppezzamento) {
        this.appeToColts.remove(colturaAppezzamento);
        colturaAppezzamento.setColtToAppe(null);
        return this;
    }

    public void setAppeToColts(Set<ColturaAppezzamento> colturaAppezzamentos) {
        this.appeToColts = colturaAppezzamentos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Appezzamenti)) {
            return false;
        }
        return id != null && id.equals(((Appezzamenti) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Appezzamenti{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", nomeAppezzamento='" + getNomeAppezzamento() + "'" +
            ", superficeHa=" + getSuperficeHa() +
            ", rotazione='" + isRotazione() + "'" +
            ", serra='" + isSerra() + "'" +
            ", irrigabile='" + isIrrigabile() + "'" +
            ", uuidEsri='" + getUuidEsri() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
