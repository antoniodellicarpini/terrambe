package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

//MODIFICA -----------------------------------------
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcPio.
 */
@Entity
@Table(name = "reg_opc_pio")
@DynamicUpdate
public class RegOpcPio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_operazione")
    private LocalDate dataOperazione;

    @Column(name = "id_coltura")
    private Long idColtura;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
@JoinColumn(name = "appz_to_opc_pio_id")
@JsonIgnoreProperties("appzToOpcPio")
//@OneToMany(mappedBy = "appzToOpcPio")
private Set<RegOpcPioAppz> opcPioToAppzs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcPio idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcPio idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataOperazione() {
        return dataOperazione;
    }

    public RegOpcPio dataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
        return this;
    }

    public void setDataOperazione(LocalDate dataOperazione) {
        this.dataOperazione = dataOperazione;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcPio idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcPio dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcPio dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcPio userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcPio userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcPio note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcPioAppz> getOpcPioToAppzs() {
        return opcPioToAppzs;
    }

    public RegOpcPio opcPioToAppzs(Set<RegOpcPioAppz> regOpcPioAppzs) {
        this.opcPioToAppzs = regOpcPioAppzs;
        return this;
    }

    public RegOpcPio addOpcPioToAppz(RegOpcPioAppz regOpcPioAppz) {
        this.opcPioToAppzs.add(regOpcPioAppz);
        regOpcPioAppz.setAppzToOpcPio(this);
        return this;
    }

    public RegOpcPio removeOpcPioToAppz(RegOpcPioAppz regOpcPioAppz) {
        this.opcPioToAppzs.remove(regOpcPioAppz);
        regOpcPioAppz.setAppzToOpcPio(null);
        return this;
    }

    public void setOpcPioToAppzs(Set<RegOpcPioAppz> regOpcPioAppzs) {
        this.opcPioToAppzs = regOpcPioAppzs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcPio)) {
            return false;
        }
        return id != null && id.equals(((RegOpcPio) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcPio{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataOperazione='" + getDataOperazione() + "'" +
            ", idColtura=" + getIdColtura() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
