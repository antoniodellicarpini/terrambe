package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Frasi.
 */
@Entity
@Table(name = "frasi")
@DynamicUpdate
public class Frasi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "codice")
    private String codice;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "frasi")
    private Set<EtichettaFrasi> frasietichettas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodice() {
        return codice;
    }

    public Frasi codice(String codice) {
        this.codice = codice;
        return this;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Frasi descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public Frasi tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public Frasi operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public Frasi ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Frasi dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Frasi dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Frasi userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Frasi userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Frasi note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<EtichettaFrasi> getFrasietichettas() {
        return frasietichettas;
    }

    public Frasi frasietichettas(Set<EtichettaFrasi> etichettaFrasis) {
        this.frasietichettas = etichettaFrasis;
        return this;
    }

    public Frasi addFrasietichetta(EtichettaFrasi etichettaFrasi) {
        this.frasietichettas.add(etichettaFrasi);
        etichettaFrasi.setFrasi(this);
        return this;
    }

    public Frasi removeFrasietichetta(EtichettaFrasi etichettaFrasi) {
        this.frasietichettas.remove(etichettaFrasi);
        etichettaFrasi.setFrasi(null);
        return this;
    }

    public void setFrasietichettas(Set<EtichettaFrasi> etichettaFrasis) {
        this.frasietichettas = etichettaFrasis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Frasi)) {
            return false;
        }
        return id != null && id.equals(((Frasi) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Frasi{" +
            "id=" + getId() +
            ", codice='" + getCodice() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
