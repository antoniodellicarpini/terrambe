package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Comuni.
 */
@Entity
@Table(name = "comuni")
@DynamicUpdate
public class Comuni implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "codi_prov")
    private String codiProv;

    @Column(name = "codi_comu")
    private String codiComu;

    @Column(name = "desc_comu")
    private String descComu;

    @Column(name = "desc_prov")
    private String descProv;

    @Column(name = "codi_sigl_prov")
    private String codiSiglProv;

    @Column(name = "codi_fisc_luna")
    private String codiFiscLuna;

    @Column(name = "desc_regi")
    private String descRegi;

    @Column(name = "codice_belfiore")
    private String codiceBelfiore;

    @Column(name = "data_inizio")
    private LocalDate dataInizio;

    @Column(name = "data_fine")
    private LocalDate dataFine;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodiProv() {
        return codiProv;
    }

    public Comuni codiProv(String codiProv) {
        this.codiProv = codiProv;
        return this;
    }

    public void setCodiProv(String codiProv) {
        this.codiProv = codiProv;
    }

    public String getCodiComu() {
        return codiComu;
    }

    public Comuni codiComu(String codiComu) {
        this.codiComu = codiComu;
        return this;
    }

    public void setCodiComu(String codiComu) {
        this.codiComu = codiComu;
    }

    public String getDescComu() {
        return descComu;
    }

    public Comuni descComu(String descComu) {
        this.descComu = descComu;
        return this;
    }

    public void setDescComu(String descComu) {
        this.descComu = descComu;
    }

    public String getDescProv() {
        return descProv;
    }

    public Comuni descProv(String descProv) {
        this.descProv = descProv;
        return this;
    }

    public void setDescProv(String descProv) {
        this.descProv = descProv;
    }

    public String getCodiSiglProv() {
        return codiSiglProv;
    }

    public Comuni codiSiglProv(String codiSiglProv) {
        this.codiSiglProv = codiSiglProv;
        return this;
    }

    public void setCodiSiglProv(String codiSiglProv) {
        this.codiSiglProv = codiSiglProv;
    }

    public String getCodiFiscLuna() {
        return codiFiscLuna;
    }

    public Comuni codiFiscLuna(String codiFiscLuna) {
        this.codiFiscLuna = codiFiscLuna;
        return this;
    }

    public void setCodiFiscLuna(String codiFiscLuna) {
        this.codiFiscLuna = codiFiscLuna;
    }

    public String getDescRegi() {
        return descRegi;
    }

    public Comuni descRegi(String descRegi) {
        this.descRegi = descRegi;
        return this;
    }

    public void setDescRegi(String descRegi) {
        this.descRegi = descRegi;
    }

    public String getCodiceBelfiore() {
        return codiceBelfiore;
    }

    public Comuni codiceBelfiore(String codiceBelfiore) {
        this.codiceBelfiore = codiceBelfiore;
        return this;
    }

    public void setCodiceBelfiore(String codiceBelfiore) {
        this.codiceBelfiore = codiceBelfiore;
    }

    public LocalDate getDataInizio() {
        return dataInizio;
    }

    public Comuni dataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
        return this;
    }

    public void setDataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDate getDataFine() {
        return dataFine;
    }

    public Comuni dataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
        return this;
    }

    public void setDataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public Comuni dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public Comuni dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public Comuni userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public Comuni userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public Comuni note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Comuni)) {
            return false;
        }
        return id != null && id.equals(((Comuni) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Comuni{" +
            "id=" + getId() +
            ", codiProv='" + getCodiProv() + "'" +
            ", codiComu='" + getCodiComu() + "'" +
            ", descComu='" + getDescComu() + "'" +
            ", descProv='" + getDescProv() + "'" +
            ", codiSiglProv='" + getCodiSiglProv() + "'" +
            ", codiFiscLuna='" + getCodiFiscLuna() + "'" +
            ", descRegi='" + getDescRegi() + "'" +
            ", codiceBelfiore='" + getCodiceBelfiore() + "'" +
            ", dataInizio='" + getDataInizio() + "'" +
            ", dataFine='" + getDataFine() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
