package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A FerAnagrafica.
 */
@Entity
@Table(name = "fer_anagrafica")
@DynamicUpdate
public class FerAnagrafica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "denominazione", nullable = false)
    private String denominazione;

    @Column(name = "ditta_produttrice")
    private String dittaProduttrice;

    @Column(name = "dato_n")
    private Double datoN;

    @Column(name = "dato_p")
    private Double datoP;

    @Column(name = "dato_k")
    private Double datoK;

    @Column(name = "tipo_concime")
    private String tipoConcime;

    @Column(name = "bio")
    private Boolean bio;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("ferAnagraficas")
    private FertTipo fertTipo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDenominazione() {
        return denominazione;
    }

    public FerAnagrafica denominazione(String denominazione) {
        this.denominazione = denominazione;
        return this;
    }

    public void setDenominazione(String denominazione) {
        this.denominazione = denominazione;
    }

    public String getDittaProduttrice() {
        return dittaProduttrice;
    }

    public FerAnagrafica dittaProduttrice(String dittaProduttrice) {
        this.dittaProduttrice = dittaProduttrice;
        return this;
    }

    public void setDittaProduttrice(String dittaProduttrice) {
        this.dittaProduttrice = dittaProduttrice;
    }

    public Double getDatoN() {
        return datoN;
    }

    public FerAnagrafica datoN(Double datoN) {
        this.datoN = datoN;
        return this;
    }

    public void setDatoN(Double datoN) {
        this.datoN = datoN;
    }

    public Double getDatoP() {
        return datoP;
    }

    public FerAnagrafica datoP(Double datoP) {
        this.datoP = datoP;
        return this;
    }

    public void setDatoP(Double datoP) {
        this.datoP = datoP;
    }

    public Double getDatoK() {
        return datoK;
    }

    public FerAnagrafica datoK(Double datoK) {
        this.datoK = datoK;
        return this;
    }

    public void setDatoK(Double datoK) {
        this.datoK = datoK;
    }

    public String getTipoConcime() {
        return tipoConcime;
    }

    public FerAnagrafica tipoConcime(String tipoConcime) {
        this.tipoConcime = tipoConcime;
        return this;
    }

    public void setTipoConcime(String tipoConcime) {
        this.tipoConcime = tipoConcime;
    }

    public Boolean isBio() {
        return bio;
    }

    public FerAnagrafica bio(Boolean bio) {
        this.bio = bio;
        return this;
    }

    public void setBio(Boolean bio) {
        this.bio = bio;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public FerAnagrafica dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public FerAnagrafica dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public FerAnagrafica userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public FerAnagrafica userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public FerAnagrafica note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public FertTipo getFertTipo() {
        return fertTipo;
    }

    public FerAnagrafica fertTipo(FertTipo fertTipo) {
        this.fertTipo = fertTipo;
        return this;
    }

    public void setFertTipo(FertTipo fertTipo) {
        this.fertTipo = fertTipo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FerAnagrafica)) {
            return false;
        }
        return id != null && id.equals(((FerAnagrafica) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FerAnagrafica{" +
            "id=" + getId() +
            ", denominazione='" + getDenominazione() + "'" +
            ", dittaProduttrice='" + getDittaProduttrice() + "'" +
            ", datoN=" + getDatoN() +
            ", datoP=" + getDatoP() +
            ", datoK=" + getDatoK() +
            ", tipoConcime='" + getTipoConcime() + "'" +
            ", bio='" + isBio() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
