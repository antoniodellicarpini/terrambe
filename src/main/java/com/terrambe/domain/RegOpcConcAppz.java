package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegOpcConcAppz.
 */
@Entity
@Table(name = "reg_opc_conc_appz")
@DynamicUpdate
public class RegOpcConcAppz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "percent_lavorata")
    private Float percentLavorata;

    @Column(name = "val_n")
    private Float valN;

    @Column(name = "val_p")
    private Float valP;

    @Column(name = "val_k")
    private Float valK;

    @Column(name = "id_appezzamento")
    private Long idAppezzamento;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @ManyToOne
    @JsonIgnoreProperties("opcConcToAppzs")
    private RegOpcConc appzToOpcConc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcConcAppz idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcConcAppz idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Float getPercentLavorata() {
        return percentLavorata;
    }

    public RegOpcConcAppz percentLavorata(Float percentLavorata) {
        this.percentLavorata = percentLavorata;
        return this;
    }

    public void setPercentLavorata(Float percentLavorata) {
        this.percentLavorata = percentLavorata;
    }

    public Float getValN() {
        return valN;
    }

    public RegOpcConcAppz valN(Float valN) {
        this.valN = valN;
        return this;
    }

    public void setValN(Float valN) {
        this.valN = valN;
    }

    public Float getValP() {
        return valP;
    }

    public RegOpcConcAppz valP(Float valP) {
        this.valP = valP;
        return this;
    }

    public void setValP(Float valP) {
        this.valP = valP;
    }

    public Float getValK() {
        return valK;
    }

    public RegOpcConcAppz valK(Float valK) {
        this.valK = valK;
        return this;
    }

    public void setValK(Float valK) {
        this.valK = valK;
    }

    public Long getIdAppezzamento() {
        return idAppezzamento;
    }

    public RegOpcConcAppz idAppezzamento(Long idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
        return this;
    }

    public void setIdAppezzamento(Long idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcConcAppz dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcConcAppz dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcConcAppz userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcConcAppz userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public RegOpcConc getAppzToOpcConc() {
        return appzToOpcConc;
    }

    public RegOpcConcAppz appzToOpcConc(RegOpcConc regOpcConc) {
        this.appzToOpcConc = regOpcConc;
        return this;
    }

    public void setAppzToOpcConc(RegOpcConc regOpcConc) {
        this.appzToOpcConc = regOpcConc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcConcAppz)) {
            return false;
        }
        return id != null && id.equals(((RegOpcConcAppz) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcConcAppz{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", percentLavorata=" + getPercentLavorata() +
            ", valN=" + getValN() +
            ", valP=" + getValP() +
            ", valK=" + getValK() +
            ", idAppezzamento=" + getIdAppezzamento() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            "}";
    }
}
