package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A AziSuperficieParticella.
 */
@Entity
@Table(name = "azi_superficie_particella")
@DynamicUpdate
public class AziSuperficieParticella implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "foglio", nullable = false)
    private String foglio;

    @NotNull
    @Column(name = "part", nullable = false)
    private String part;

    @Column(name = "sub")
    private String sub;

    @Column(name = "sezione")
    private String sezione;

    @NotNull
    @Column(name = "superficie", nullable = false)
    private Float superficie;

    @NotNull
    @Column(name = "sup_catastale", nullable = false)
    private Float supCatastale;

    @NotNull
    @Column(name = "sup_utilizzabile", nullable = false)
    private Float supUtilizzabile;

    @NotNull
    @Column(name = "sup_tara", nullable = false)
    private Float supTara;

    @NotNull
    @Column(name = "uuid_esri", nullable = false)
    private String uuidEsri;

    @NotNull
    @Column(name = "nazione", nullable = false)
    private String nazione;

    @NotNull
    @Column(name = "regione", nullable = false)
    private String regione;

    @NotNull
    @Column(name = "provincia", nullable = false)
    private String provincia;

    @NotNull
    @Column(name = "comune", nullable = false)
    private String comune;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "dettaglio_to_part_id")
    @JsonIgnoreProperties("dettaglioToPart")
    //@OneToMany(mappedBy = "dettaglioToPart")
    private Set<AziSupPartDettaglio> partToDettaglios = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("aziSuperficieParticellas")
    private AziSupPartConduzione conduzione;

    @ManyToOne
    @JsonIgnoreProperties("aziSuperficieParticellas")
    private AziAnagrafica partToAziAna;

    @ManyToOne
    @JsonIgnoreProperties("campiToAziSups")
    private Campi aziSupToCampi;

    @ManyToMany(mappedBy = "appzToParts")
    @JsonIgnore
    private Set<Appezzamenti> partToAppzs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoglio() {
        return foglio;
    }

    public AziSuperficieParticella foglio(String foglio) {
        this.foglio = foglio;
        return this;
    }

    public void setFoglio(String foglio) {
        this.foglio = foglio;
    }

    public String getPart() {
        return part;
    }

    public AziSuperficieParticella part(String part) {
        this.part = part;
        return this;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getSub() {
        return sub;
    }

    public AziSuperficieParticella sub(String sub) {
        this.sub = sub;
        return this;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getSezione() {
        return sezione;
    }

    public AziSuperficieParticella sezione(String sezione) {
        this.sezione = sezione;
        return this;
    }

    public void setSezione(String sezione) {
        this.sezione = sezione;
    }

    public Float getSuperficie() {
        return superficie;
    }

    public AziSuperficieParticella superficie(Float superficie) {
        this.superficie = superficie;
        return this;
    }

    public void setSuperficie(Float superficie) {
        this.superficie = superficie;
    }

    public Float getSupCatastale() {
        return supCatastale;
    }

    public AziSuperficieParticella supCatastale(Float supCatastale) {
        this.supCatastale = supCatastale;
        return this;
    }

    public void setSupCatastale(Float supCatastale) {
        this.supCatastale = supCatastale;
    }

    public Float getSupUtilizzabile() {
        return supUtilizzabile;
    }

    public AziSuperficieParticella supUtilizzabile(Float supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
        return this;
    }

    public void setSupUtilizzabile(Float supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
    }

    public Float getSupTara() {
        return supTara;
    }

    public AziSuperficieParticella supTara(Float supTara) {
        this.supTara = supTara;
        return this;
    }

    public void setSupTara(Float supTara) {
        this.supTara = supTara;
    }

    public String getUuidEsri() {
        return uuidEsri;
    }

    public AziSuperficieParticella uuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
        return this;
    }

    public void setUuidEsri(String uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public String getNazione() {
        return nazione;
    }

    public AziSuperficieParticella nazione(String nazione) {
        this.nazione = nazione;
        return this;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getRegione() {
        return regione;
    }

    public AziSuperficieParticella regione(String regione) {
        this.regione = regione;
        return this;
    }

    public void setRegione(String regione) {
        this.regione = regione;
    }

    public String getProvincia() {
        return provincia;
    }

    public AziSuperficieParticella provincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getComune() {
        return comune;
    }

    public AziSuperficieParticella comune(String comune) {
        this.comune = comune;
        return this;
    }

    public void setComune(String comune) {
        this.comune = comune;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AziSuperficieParticella dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AziSuperficieParticella dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AziSuperficieParticella userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AziSuperficieParticella userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AziSuperficieParticella note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<AziSupPartDettaglio> getPartToDettaglios() {
        return partToDettaglios;
    }

    public AziSuperficieParticella partToDettaglios(Set<AziSupPartDettaglio> aziSupPartDettaglios) {
        this.partToDettaglios = aziSupPartDettaglios;
        return this;
    }

    public AziSuperficieParticella addPartToDettaglio(AziSupPartDettaglio aziSupPartDettaglio) {
        this.partToDettaglios.add(aziSupPartDettaglio);
        aziSupPartDettaglio.setDettaglioToPart(this);
        return this;
    }

    public AziSuperficieParticella removePartToDettaglio(AziSupPartDettaglio aziSupPartDettaglio) {
        this.partToDettaglios.remove(aziSupPartDettaglio);
        aziSupPartDettaglio.setDettaglioToPart(null);
        return this;
    }

    public void setPartToDettaglios(Set<AziSupPartDettaglio> aziSupPartDettaglios) {
        this.partToDettaglios = aziSupPartDettaglios;
    }

    public AziSupPartConduzione getConduzione() {
        return conduzione;
    }

    public AziSuperficieParticella conduzione(AziSupPartConduzione aziSupPartConduzione) {
        this.conduzione = aziSupPartConduzione;
        return this;
    }

    public void setConduzione(AziSupPartConduzione aziSupPartConduzione) {
        this.conduzione = aziSupPartConduzione;
    }

    public AziAnagrafica getPartToAziAna() {
        return partToAziAna;
    }

    public AziSuperficieParticella partToAziAna(AziAnagrafica aziAnagrafica) {
        this.partToAziAna = aziAnagrafica;
        return this;
    }

    public void setPartToAziAna(AziAnagrafica aziAnagrafica) {
        this.partToAziAna = aziAnagrafica;
    }

    public Campi getAziSupToCampi() {
        return aziSupToCampi;
    }

    public AziSuperficieParticella aziSupToCampi(Campi campi) {
        this.aziSupToCampi = campi;
        return this;
    }

    public void setAziSupToCampi(Campi campi) {
        this.aziSupToCampi = campi;
    }

    public Set<Appezzamenti> getPartToAppzs() {
        return partToAppzs;
    }

    public AziSuperficieParticella partToAppzs(Set<Appezzamenti> appezzamentis) {
        this.partToAppzs = appezzamentis;
        return this;
    }

    public AziSuperficieParticella addPartToAppz(Appezzamenti appezzamenti) {
        this.partToAppzs.add(appezzamenti);
        appezzamenti.getAppzToParts().add(this);
        return this;
    }

    public AziSuperficieParticella removePartToAppz(Appezzamenti appezzamenti) {
        this.partToAppzs.remove(appezzamenti);
        appezzamenti.getAppzToParts().remove(this);
        return this;
    }

    public void setPartToAppzs(Set<Appezzamenti> appezzamentis) {
        this.partToAppzs = appezzamentis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AziSuperficieParticella)) {
            return false;
        }
        return id != null && id.equals(((AziSuperficieParticella) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AziSuperficieParticella{" +
            "id=" + getId() +
            ", foglio='" + getFoglio() + "'" +
            ", part='" + getPart() + "'" +
            ", sub='" + getSub() + "'" +
            ", sezione='" + getSezione() + "'" +
            ", superficie=" + getSuperficie() +
            ", supCatastale=" + getSupCatastale() +
            ", supUtilizzabile=" + getSupUtilizzabile() +
            ", supTara=" + getSupTara() +
            ", uuidEsri='" + getUuidEsri() + "'" +
            ", nazione='" + getNazione() + "'" +
            ", regione='" + getRegione() + "'" +
            ", provincia='" + getProvincia() + "'" +
            ", comune='" + getComune() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
