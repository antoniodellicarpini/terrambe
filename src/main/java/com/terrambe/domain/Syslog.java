package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Syslog.
 */
@Entity
@Table(name = "syslog")
@DynamicUpdate
public class Syslog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome_tabella", nullable = false)
    private String nomeTabella;

    @NotNull
    @Column(name = "id_juser", nullable = false)
    private Integer idJuser;

    @NotNull
    @Column(name = "data_modifica", nullable = false)
    private LocalDate dataModifica;

    @NotNull
    @Column(name = "nome_campo", nullable = false)
    private String nomeCampo;

    @NotNull
    @Column(name = "vecchio_valore", nullable = false)
    private String vecchioValore;

    @NotNull
    @Column(name = "nuovo_valore", nullable = false)
    private String nuovoValore;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeTabella() {
        return nomeTabella;
    }

    public Syslog nomeTabella(String nomeTabella) {
        this.nomeTabella = nomeTabella;
        return this;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = nomeTabella;
    }

    public Integer getIdJuser() {
        return idJuser;
    }

    public Syslog idJuser(Integer idJuser) {
        this.idJuser = idJuser;
        return this;
    }

    public void setIdJuser(Integer idJuser) {
        this.idJuser = idJuser;
    }

    public LocalDate getDataModifica() {
        return dataModifica;
    }

    public Syslog dataModifica(LocalDate dataModifica) {
        this.dataModifica = dataModifica;
        return this;
    }

    public void setDataModifica(LocalDate dataModifica) {
        this.dataModifica = dataModifica;
    }

    public String getNomeCampo() {
        return nomeCampo;
    }

    public Syslog nomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
        return this;
    }

    public void setNomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public String getVecchioValore() {
        return vecchioValore;
    }

    public Syslog vecchioValore(String vecchioValore) {
        this.vecchioValore = vecchioValore;
        return this;
    }

    public void setVecchioValore(String vecchioValore) {
        this.vecchioValore = vecchioValore;
    }

    public String getNuovoValore() {
        return nuovoValore;
    }

    public Syslog nuovoValore(String nuovoValore) {
        this.nuovoValore = nuovoValore;
        return this;
    }

    public void setNuovoValore(String nuovoValore) {
        this.nuovoValore = nuovoValore;
    }

    public String getNote() {
        return note;
    }

    public Syslog note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Syslog)) {
            return false;
        }
        return id != null && id.equals(((Syslog) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Syslog{" +
            "id=" + getId() +
            ", nomeTabella='" + getNomeTabella() + "'" +
            ", idJuser=" + getIdJuser() +
            ", dataModifica='" + getDataModifica() + "'" +
            ", nomeCampo='" + getNomeCampo() + "'" +
            ", vecchioValore='" + getVecchioValore() + "'" +
            ", nuovoValore='" + getNuovoValore() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
