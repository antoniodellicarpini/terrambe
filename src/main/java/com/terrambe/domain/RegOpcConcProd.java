package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegOpcConcProd.
 */
@Entity
@Table(name = "reg_opc_conc_prod")
@DynamicUpdate
public class RegOpcConcProd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "id_concime")
    private Long idConcime;

    @Column(name = "quantita")
    private Float quantita;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("opcConcToProds")
    private RegOpcConc prodToOpcConc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcConcProd idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcConcProd idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Long getIdConcime() {
        return idConcime;
    }

    public RegOpcConcProd idConcime(Long idConcime) {
        this.idConcime = idConcime;
        return this;
    }

    public void setIdConcime(Long idConcime) {
        this.idConcime = idConcime;
    }

    public Float getQuantita() {
        return quantita;
    }

    public RegOpcConcProd quantita(Float quantita) {
        this.quantita = quantita;
        return this;
    }

    public void setQuantita(Float quantita) {
        this.quantita = quantita;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcConcProd dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcConcProd dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcConcProd userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcConcProd userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcConcProd note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public RegOpcConc getProdToOpcConc() {
        return prodToOpcConc;
    }

    public RegOpcConcProd prodToOpcConc(RegOpcConc regOpcConc) {
        this.prodToOpcConc = regOpcConc;
        return this;
    }

    public void setProdToOpcConc(RegOpcConc regOpcConc) {
        this.prodToOpcConc = regOpcConc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcConcProd)) {
            return false;
        }
        return id != null && id.equals(((RegOpcConcProd) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcConcProd{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", idConcime=" + getIdConcime() +
            ", quantita=" + getQuantita() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
