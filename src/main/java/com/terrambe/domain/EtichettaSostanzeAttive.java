package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A EtichettaSostanzeAttive.
 */
@Entity
@Table(name = "etichetta_sostanze_attive")
@DynamicUpdate
public class EtichettaSostanzeAttive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "grammi_litro")
    private Float grammiLitro;

    @Column(name = "percentuale")
    private Float percentuale;

    @Column(name = "tipo_import")
    private String tipoImport;

    @Column(name = "operatore")
    private String operatore;

    @Column(name = "ts")
    private Instant ts;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("etichettaSostanzeAttives")
    private EtichettaFito etichettaFito;

    @ManyToOne
    @JsonIgnoreProperties("sostAttToEtiSostAtts")
    private SostanzeAttive etiSostAttToSostAtt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getGrammiLitro() {
        return grammiLitro;
    }

    public EtichettaSostanzeAttive grammiLitro(Float grammiLitro) {
        this.grammiLitro = grammiLitro;
        return this;
    }

    public void setGrammiLitro(Float grammiLitro) {
        this.grammiLitro = grammiLitro;
    }

    public Float getPercentuale() {
        return percentuale;
    }

    public EtichettaSostanzeAttive percentuale(Float percentuale) {
        this.percentuale = percentuale;
        return this;
    }

    public void setPercentuale(Float percentuale) {
        this.percentuale = percentuale;
    }

    public String getTipoImport() {
        return tipoImport;
    }

    public EtichettaSostanzeAttive tipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
        return this;
    }

    public void setTipoImport(String tipoImport) {
        this.tipoImport = tipoImport;
    }

    public String getOperatore() {
        return operatore;
    }

    public EtichettaSostanzeAttive operatore(String operatore) {
        this.operatore = operatore;
        return this;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    public Instant getTs() {
        return ts;
    }

    public EtichettaSostanzeAttive ts(Instant ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(Instant ts) {
        this.ts = ts;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public EtichettaSostanzeAttive dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public EtichettaSostanzeAttive dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public EtichettaSostanzeAttive userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public EtichettaSostanzeAttive userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public EtichettaSostanzeAttive note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public EtichettaFito getEtichettaFito() {
        return etichettaFito;
    }

    public EtichettaSostanzeAttive etichettaFito(EtichettaFito etichettaFito) {
        this.etichettaFito = etichettaFito;
        return this;
    }

    public void setEtichettaFito(EtichettaFito etichettaFito) {
        this.etichettaFito = etichettaFito;
    }

    public SostanzeAttive getEtiSostAttToSostAtt() {
        return etiSostAttToSostAtt;
    }

    public EtichettaSostanzeAttive etiSostAttToSostAtt(SostanzeAttive sostanzeAttive) {
        this.etiSostAttToSostAtt = sostanzeAttive;
        return this;
    }

    public void setEtiSostAttToSostAtt(SostanzeAttive sostanzeAttive) {
        this.etiSostAttToSostAtt = sostanzeAttive;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtichettaSostanzeAttive)) {
            return false;
        }
        return id != null && id.equals(((EtichettaSostanzeAttive) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EtichettaSostanzeAttive{" +
            "id=" + getId() +
            ", grammiLitro=" + getGrammiLitro() +
            ", percentuale=" + getPercentuale() +
            ", tipoImport='" + getTipoImport() + "'" +
            ", operatore='" + getOperatore() + "'" +
            ", ts='" + getTs() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
