package com.terrambe.domain;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * A RegOpcFitoSpec.
 */

public class RegOpcSpec implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private LocalDate dataInizOpc;

    private String TipoTratt;

    private String tempoImpiegato;

    private String descColtura;

    private String descFaseFenologica;

    private String nominativoOperatore;

    private String descMezzo;

    private String tipoPrepTerr;

    private String appezzamento;

    private String ProdottoConc;

    private String npkOper;

    private String npkAppe;

    @Override
    public String toString() {
        return "RegOpcSpec{" +
            "id=" + id +
            ", dataInizOpc=" + dataInizOpc +
            ", TipoTratt='" + TipoTratt + '\'' +
            ", tempoImpiegato='" + tempoImpiegato + '\'' +
            ", descColtura='" + descColtura + '\'' +
            ", descFaseFenologica='" + descFaseFenologica + '\'' +
            ", nominativoOperatore='" + nominativoOperatore + '\'' +
            ", descMezzo='" + descMezzo + '\'' +
            ", tipoPrepTerr='" + tipoPrepTerr + '\'' +
            ", appezzamento='" + appezzamento + '\'' +
            ", ProdottoConc='" + ProdottoConc + '\'' +
            ", npkOper='" + npkOper + '\'' +
            ", npkAppe='" + npkAppe + '\'' +
            '}';
    }

    public String getAppezzamento() {
        return appezzamento;
    }

    public void setAppezzamento(String appezzamento) {
        this.appezzamento = appezzamento;
    }

    public String getProdottoConc() {
        return ProdottoConc;
    }

    public void setProdottoConc(String prodottoConc) {
        ProdottoConc = prodottoConc;
    }

    public String getNpkOper() {
        return npkOper;
    }

    public void setNpkOper(String npkOper) {
        this.npkOper = npkOper;
    }

    public String getNpkAppe() {
        return npkAppe;
    }

    public void setNpkAppe(String npkAppe) {
        this.npkAppe = npkAppe;
    }

    public String getNominativoOperatore() {
        return nominativoOperatore;
    }

    public void setNominativoOperatore(String nominativoOperatore) {
        this.nominativoOperatore = nominativoOperatore;
    }

    public String getTipoPrepTerr() {
        return tipoPrepTerr;
    }

    public void setTipoPrepTerr(String tipoPrepTerr) {
        this.tipoPrepTerr = tipoPrepTerr;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataInizOpc() {
        return dataInizOpc;
    }

    public void setDataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public String getDescColtura() {
        return descColtura;
    }

    public void setDescColtura(String descColtura) {
        this.descColtura = descColtura;
    }

    public String getDescFaseFenologica() {
        return descFaseFenologica;
    }

    public void setDescFaseFenologica(String descFaseFenologica) {
        this.descFaseFenologica = descFaseFenologica;
    }

    public String getNominativo() {
        return nominativoOperatore;
    }

    public void setNominativo(String nominativo) {
        this.nominativoOperatore = nominativo;
    }

    public String getDescMezzo() {
        return descMezzo;
    }

    public void setDescMezzo(String descMezzo) {
        this.descMezzo = descMezzo;
    }

    public String getTipoTratt() {
        return TipoTratt;
    }

    public void setTipoTratt(String tipoTratt) {
        TipoTratt = tipoTratt;
    }

}

