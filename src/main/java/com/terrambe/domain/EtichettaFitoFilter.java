package com.terrambe.domain;

import java.io.Serializable;


/**
 * A EtichettaFitoFilter.
 */

public class EtichettaFitoFilter implements Serializable {


    private Long id;

    private String nome_commerciale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeCommerciale() {
        return nome_commerciale;
    }

    public void setNomeCommerciale(String nome_commerciale) {
        this.nome_commerciale = nome_commerciale;
    }


    @Override
    public String toString() {
        return "EtichettaFitoFilter{" +
            "id=" + id +
            ", nome_commerciale=" + nome_commerciale +
            '}';
    }
}

