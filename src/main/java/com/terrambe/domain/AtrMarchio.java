package com.terrambe.domain;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.CreationTimestamp;
import javax.cache.annotation.CacheRemoveAll;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A AtrMarchio.
 */
@Entity
@Table(name = "atr_marchio")
@DynamicUpdate
public class AtrMarchio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "descrizione", nullable = false)
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali  = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToMany
    @JoinTable(name = "atr_marchio_atr_tipologia",
               joinColumns = @JoinColumn(name = "atr_marchio_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "atr_tipologia_id", referencedColumnName = "id"))
    private Set<AtrTipologia> atrTipologias = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public AtrMarchio descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public AtrMarchio dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public AtrMarchio dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public AtrMarchio userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public AtrMarchio userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public AtrMarchio note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<AtrTipologia> getAtrTipologias() {
        return atrTipologias;
    }

    public AtrMarchio atrTipologias(Set<AtrTipologia> atrTipologias) {
        this.atrTipologias = atrTipologias;
        return this;
    }

    public AtrMarchio addAtrTipologia(AtrTipologia atrTipologia) {
        this.atrTipologias.add(atrTipologia);
        atrTipologia.getAtrMarchios().add(this);
        return this;
    }

    public AtrMarchio removeAtrTipologia(AtrTipologia atrTipologia) {
        this.atrTipologias.remove(atrTipologia);
        atrTipologia.getAtrMarchios().remove(this);
        return this;
    }

    public void setAtrTipologias(Set<AtrTipologia> atrTipologias) {
        this.atrTipologias = atrTipologias;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AtrMarchio)) {
            return false;
        }
        return id != null && id.equals(((AtrMarchio) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AtrMarchio{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
