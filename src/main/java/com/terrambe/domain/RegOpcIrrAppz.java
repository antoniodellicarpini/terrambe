package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegOpcIrrAppz.
 */
@Entity
@Table(name = "reg_opc_irr_appz")
@DynamicUpdate
public class RegOpcIrrAppz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "percent_irrigata")
    private Float percentIrrigata;

    @Column(name = "id_appezzamento")
    private Long idAppezzamento;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @ManyToOne
    @JsonIgnoreProperties("opcIrrToAppzs")
    private RegOpcIrr appzToOpcIrr;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcIrrAppz idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcIrrAppz idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Float getPercentIrrigata() {
        return percentIrrigata;
    }

    public RegOpcIrrAppz percentIrrigata(Float percentIrrigata) {
        this.percentIrrigata = percentIrrigata;
        return this;
    }

    public void setPercentIrrigata(Float percentIrrigata) {
        this.percentIrrigata = percentIrrigata;
    }

    public Long getIdAppezzamento() {
        return idAppezzamento;
    }

    public RegOpcIrrAppz idAppezzamento(Long idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
        return this;
    }

    public void setIdAppezzamento(Long idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcIrrAppz dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcIrrAppz dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcIrrAppz userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcIrrAppz userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public RegOpcIrr getAppzToOpcIrr() {
        return appzToOpcIrr;
    }

    public RegOpcIrrAppz appzToOpcIrr(RegOpcIrr regOpcIrr) {
        this.appzToOpcIrr = regOpcIrr;
        return this;
    }

    public void setAppzToOpcIrr(RegOpcIrr regOpcIrr) {
        this.appzToOpcIrr = regOpcIrr;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcIrrAppz)) {
            return false;
        }
        return id != null && id.equals(((RegOpcIrrAppz) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcIrrAppz{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", percentIrrigata=" + getPercentIrrigata() +
            ", idAppezzamento=" + getIdAppezzamento() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            "}";
    }
}
