package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A PermessiOperatore.
 */
@Entity
@Table(name = "permessi_operatore")
@DynamicUpdate
public class PermessiOperatore implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "flag_modifica", nullable = false)
    private Boolean flagModifica;

    @NotNull
    @Column(name = "flag_visualizza", nullable = false)
    private Boolean flagVisualizza;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;
 
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("tipPermToPermOpers")
    private TipologiaPermessoOperatore permOperToTipoPerm;

    @ManyToOne
    @JsonIgnoreProperties("permessioperatores")
    private OpeAnagrafica perOperToOpeAnag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isFlagModifica() {
        return flagModifica;
    }

    public PermessiOperatore flagModifica(Boolean flagModifica) {
        this.flagModifica = flagModifica;
        return this;
    }

    public void setFlagModifica(Boolean flagModifica) {
        this.flagModifica = flagModifica;
    }

    public Boolean isFlagVisualizza() {
        return flagVisualizza;
    }

    public PermessiOperatore flagVisualizza(Boolean flagVisualizza) {
        this.flagVisualizza = flagVisualizza;
        return this;
    }

    public void setFlagVisualizza(Boolean flagVisualizza) {
        this.flagVisualizza = flagVisualizza;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public PermessiOperatore dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public PermessiOperatore dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public PermessiOperatore userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public PermessiOperatore userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public PermessiOperatore note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public TipologiaPermessoOperatore getPermOperToTipoPerm() {
        return permOperToTipoPerm;
    }

    public PermessiOperatore permOperToTipoPerm(TipologiaPermessoOperatore tipologiaPermessoOperatore) {
        this.permOperToTipoPerm = tipologiaPermessoOperatore;
        return this;
    }

    public void setPermOperToTipoPerm(TipologiaPermessoOperatore tipologiaPermessoOperatore) {
        this.permOperToTipoPerm = tipologiaPermessoOperatore;
    }

    public OpeAnagrafica getPerOperToOpeAnag() {
        return perOperToOpeAnag;
    }

    public PermessiOperatore perOperToOpeAnag(OpeAnagrafica opeAnagrafica) {
        this.perOperToOpeAnag = opeAnagrafica;
        return this;
    }

    public void setPerOperToOpeAnag(OpeAnagrafica opeAnagrafica) {
        this.perOperToOpeAnag = opeAnagrafica;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PermessiOperatore)) {
            return false;
        }
        return id != null && id.equals(((PermessiOperatore) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PermessiOperatore{" +
            "id=" + getId() +
            ", flagModifica='" + isFlagModifica() + "'" +
            ", flagVisualizza='" + isFlagVisualizza() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
