package com.terrambe.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TipoPrepterr.
 */
@Entity
@Table(name = "tipo_prepterr")
@DynamicUpdate
public class TipoPrepterr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descrizione")
    private String descrizione;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @OneToMany(mappedBy = "opcPrepterrToTipo")
    private Set<RegOpcPrepterr> tipoToOpcPrepterrs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public TipoPrepterr descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public TipoPrepterr dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public TipoPrepterr dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public TipoPrepterr userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public TipoPrepterr userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public Set<RegOpcPrepterr> getTipoToOpcPrepterrs() {
        return tipoToOpcPrepterrs;
    }

    public TipoPrepterr tipoToOpcPrepterrs(Set<RegOpcPrepterr> regOpcPrepterrs) {
        this.tipoToOpcPrepterrs = regOpcPrepterrs;
        return this;
    }

    public TipoPrepterr addTipoToOpcPrepterr(RegOpcPrepterr regOpcPrepterr) {
        this.tipoToOpcPrepterrs.add(regOpcPrepterr);
        regOpcPrepterr.setOpcPrepterrToTipo(this);
        return this;
    }

    public TipoPrepterr removeTipoToOpcPrepterr(RegOpcPrepterr regOpcPrepterr) {
        this.tipoToOpcPrepterrs.remove(regOpcPrepterr);
        regOpcPrepterr.setOpcPrepterrToTipo(null);
        return this;
    }

    public void setTipoToOpcPrepterrs(Set<RegOpcPrepterr> regOpcPrepterrs) {
        this.tipoToOpcPrepterrs = regOpcPrepterrs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoPrepterr)) {
            return false;
        }
        return id != null && id.equals(((TipoPrepterr) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TipoPrepterr{" +
            "id=" + getId() +
            ", descrizione='" + getDescrizione() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            "}";
    }
}
