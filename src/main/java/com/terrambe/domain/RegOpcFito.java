package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcFito.
 */
@Entity
@Table(name = "reg_opc_fito")
@DynamicUpdate
public class RegOpcFito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;


    @Column(name = "data_iniz_opc")
    private LocalDate dataInizOpc;

    @Column(name = "data_fine_opc")
    private LocalDate dataFineOpc;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "volume_acqua")
    private Float volumeAcqua;

    /*
    @Column(name = "id_coltura")
    private Long idColtura;
    */

    /*
    @Column(name = "id_fase_fenologica")
    private Long idFaseFenologica;
    */

    /*
    @Column(name = "id_operatore")
    private Long idOperatore;
    */
    /*
    @Column(name = "id_mezzo")
    private Long idMezzo;
    */

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @OneToOne
    @JoinColumn(name = "id_azienda")
    private AziAnagrafica aziAnagrafica;

    @OneToOne
    @JoinColumn(name = "id_unita_prod")
    private UniAnagrafica uniAnagrafica;

    @OneToOne
    @JoinColumn(name = "id_coltura")
    private Agea20152020Matrice agea20152020Matrice;

    @OneToOne
    @JoinColumn(name = "id_fase_fenologica")
    private FaseFenologica faseFenologica;

    @OneToOne
    @JoinColumn(name = "id_operatore")
    private OpeAnagrafica opeAnagrafica;

    @OneToOne
    @JoinColumn(name = "id_mezzo")
    private AtrAnagrafica atrAnagrafica;

    @OneToOne
    @JoinColumn(name = "id_avversita")
    private Avversita avversita;

    @OneToOne
    @JoinColumn(name = "id_prodotto")
    private EtichettaFito etichettaFito;

    @OneToOne
    @JoinColumn(name = "id_magazzino")
    private MagUbicazione magUbicazione;

    //MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "appz_to_opc_fito_id")
    @JsonIgnoreProperties("appzToOpcFito")
    //@OneToMany(mappedBy = "appzToOpcFito")
    private Set<RegOpcFitoAppz> opcFitoToAppzs = new HashSet<>();

    //MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "prod_to_opc_fito_id")
    @JsonIgnoreProperties("prodToOpcFito")
    //@OneToMany(mappedBy = "prodToOpcFito")
    private Set<RegOpcFitoProd> opcFitoToProds = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("dosTrattToRegOpcFitos")
    private DosaggioTrattamento regOpcFitoToDosTratt;

    @ManyToOne
    @JsonIgnoreProperties("motivoToTrattFitos")
    private MotivoTrattFito trattFitoToMotivo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UniAnagrafica getUniAnagrafica() {
        return uniAnagrafica;
    }

    public void setUniAnagrafica(UniAnagrafica uniAnagrafica) {
        this.uniAnagrafica = uniAnagrafica;
    }

    public Agea20152020Matrice getAgea20152020Matrice() {
        return agea20152020Matrice;
    }

    public void setAgea20152020Matrice(Agea20152020Matrice agea20152020Matrice) {
        this.agea20152020Matrice = agea20152020Matrice;
    }

    public FaseFenologica getFaseFenologica() {
        return faseFenologica;
    }

    public void setFaseFenologica(FaseFenologica faseFenologica) {
        this.faseFenologica = faseFenologica;
    }


    public OpeAnagrafica getOpeAnagrafica() {
        return opeAnagrafica;
    }

    public void setOpeAnagrafica(OpeAnagrafica opeAnagrafica) {
        this.opeAnagrafica = opeAnagrafica;
    }

    public AtrAnagrafica getAtrAnagrafica() {
        return atrAnagrafica;
    }

    public void setAtrAnagrafica(AtrAnagrafica atrAnagrafica) {
        this.atrAnagrafica = atrAnagrafica;
    }

    public Avversita getAvversita() {
        return avversita;
    }

    public void setAvversita(Avversita avversita) {
        this.avversita = avversita;
    }

    public EtichettaFito getEtichettaFito() {
        return etichettaFito;
    }

    public void setEtichettaFito(EtichettaFito etichettaFito) {
        this.etichettaFito = etichettaFito;
    }

    public MagUbicazione getMagUbicazione() {
        return magUbicazione;
    }

    public void setMagUbicazione(MagUbicazione magUbicazione) {
        this.magUbicazione = magUbicazione;
    }

    public LocalDate getDataInizOpc() {
        return dataInizOpc;
    }

    public RegOpcFito dataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
        return this;
    }

    public void setDataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDate getDataFineOpc() {
        return dataFineOpc;
    }

    public RegOpcFito dataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
        return this;
    }

    public void setDataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcFito tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Float getVolumeAcqua() {
        return volumeAcqua;
    }

    public RegOpcFito volumeAcqua(Float volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
        return this;
    }

    public void setVolumeAcqua(Float volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
    }


    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcFito dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcFito dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcFito userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcFito userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcFito note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcFitoAppz> getOpcFitoToAppzs() {
        return opcFitoToAppzs;
    }

    public RegOpcFito opcFitoToAppzs(Set<RegOpcFitoAppz> regOpcFitoAppzs) {
        this.opcFitoToAppzs = regOpcFitoAppzs;
        return this;
    }

    public RegOpcFito addOpcFitoToAppz(RegOpcFitoAppz regOpcFitoAppz) {
        this.opcFitoToAppzs.add(regOpcFitoAppz);
        regOpcFitoAppz.setAppzToOpcFito(this);
        return this;
    }

    public RegOpcFito removeOpcFitoToAppz(RegOpcFitoAppz regOpcFitoAppz) {
        this.opcFitoToAppzs.remove(regOpcFitoAppz);
        regOpcFitoAppz.setAppzToOpcFito(null);
        return this;
    }

    public void setOpcFitoToAppzs(Set<RegOpcFitoAppz> regOpcFitoAppzs) {
        this.opcFitoToAppzs = regOpcFitoAppzs;
    }

    public Set<RegOpcFitoProd> getOpcFitoToProds() {
        return opcFitoToProds;
    }

    public RegOpcFito opcFitoToProds(Set<RegOpcFitoProd> regOpcFitoProds) {
        this.opcFitoToProds = regOpcFitoProds;
        return this;
    }

    public RegOpcFito addOpcFitoToProd(RegOpcFitoProd regOpcFitoProd) {
        this.opcFitoToProds.add(regOpcFitoProd);
        regOpcFitoProd.setProdToOpcFito(this);
        return this;
    }

    public RegOpcFito removeOpcFitoToProd(RegOpcFitoProd regOpcFitoProd) {
        this.opcFitoToProds.remove(regOpcFitoProd);
        regOpcFitoProd.setProdToOpcFito(null);
        return this;
    }

    public void setOpcFitoToProds(Set<RegOpcFitoProd> regOpcFitoProds) {
        this.opcFitoToProds = regOpcFitoProds;
    }

    public DosaggioTrattamento getRegOpcFitoToDosTratt() {
        return regOpcFitoToDosTratt;
    }

    public RegOpcFito regOpcFitoToDosTratt(DosaggioTrattamento dosaggioTrattamento) {
        this.regOpcFitoToDosTratt = dosaggioTrattamento;
        return this;
    }

    public void setRegOpcFitoToDosTratt(DosaggioTrattamento dosaggioTrattamento) {
        this.regOpcFitoToDosTratt = dosaggioTrattamento;
    }

    public MotivoTrattFito getTrattFitoToMotivo() {
        return trattFitoToMotivo;
    }

    public RegOpcFito trattFitoToMotivo(MotivoTrattFito motivoTrattFito) {
        this.trattFitoToMotivo = motivoTrattFito;
        return this;
    }

    public AziAnagrafica getAziAnagrafica() {
        return aziAnagrafica;
    }

    public void setAziAnagrafica(AziAnagrafica aziAnagrafica) {
        this.aziAnagrafica = aziAnagrafica;
    }

    public void setTrattFitoToMotivo(MotivoTrattFito motivoTrattFito) {
        this.trattFitoToMotivo = motivoTrattFito;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcFito)) {
            return false;
        }
        return id != null && id.equals(((RegOpcFito) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcFito{" +
            "id=" + id +
            ", dataInizOpc=" + dataInizOpc +
            ", dataFineOpc=" + dataFineOpc +
            ", tempoImpiegato='" + tempoImpiegato + '\'' +
            ", volumeAcqua=" + volumeAcqua +
            ", dataInizVali=" + dataInizVali +
            ", dataFineVali=" + dataFineVali +
            ", userIdCreator=" + userIdCreator +
            ", userIdLastMod=" + userIdLastMod +
            ", note='" + note + '\'' +
            ", aziAnagrafica=" + aziAnagrafica +
            ", uniAnagrafica=" + uniAnagrafica +
            ", agea20152020Matrice=" + agea20152020Matrice +
            ", faseFenologica=" + faseFenologica +
            ", opeAnagrafica=" + opeAnagrafica +
            ", atrAnagrafica=" + atrAnagrafica +
            ", avversita=" + avversita +
            ", etichettaFito=" + etichettaFito +
            ", magUbicazione=" + magUbicazione +
            ", opcFitoToAppzs=" + opcFitoToAppzs +
            ", opcFitoToProds=" + opcFitoToProds +
            ", regOpcFitoToDosTratt=" + regOpcFitoToDosTratt +
            ", trattFitoToMotivo=" + trattFitoToMotivo +
            '}';
    }
}
