package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RecAziendaCli.
 */
@Entity
@Table(name = "rec_azienda_cli")
@DynamicUpdate
public class RecAziendaCli implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "pec")
    private String pec;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "fax")
    private String fax;

    @Column(name = "cell")
    private String cell;

    @Column(name = "cell_2")
    private String cell2;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("cliToRecs")
    private Cliente recToCli;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public RecAziendaCli email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPec() {
        return pec;
    }

    public RecAziendaCli pec(String pec) {
        this.pec = pec;
        return this;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getTelefono() {
        return telefono;
    }

    public RecAziendaCli telefono(String telefono) {
        this.telefono = telefono;
        return this;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public RecAziendaCli fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCell() {
        return cell;
    }

    public RecAziendaCli cell(String cell) {
        this.cell = cell;
        return this;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCell2() {
        return cell2;
    }

    public RecAziendaCli cell2(String cell2) {
        this.cell2 = cell2;
        return this;
    }

    public void setCell2(String cell2) {
        this.cell2 = cell2;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RecAziendaCli dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RecAziendaCli dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RecAziendaCli userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RecAziendaCli userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RecAziendaCli note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Cliente getRecToCli() {
        return recToCli;
    }

    public RecAziendaCli recToCli(Cliente cliente) {
        this.recToCli = cliente;
        return this;
    }

    public void setRecToCli(Cliente cliente) {
        this.recToCli = cliente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecAziendaCli)) {
            return false;
        }
        return id != null && id.equals(((RecAziendaCli) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RecAziendaCli{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", pec='" + getPec() + "'" +
            ", telefono='" + getTelefono() + "'" +
            ", fax='" + getFax() + "'" +
            ", cell='" + getCell() + "'" +
            ", cell2='" + getCell2() + "'" +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
