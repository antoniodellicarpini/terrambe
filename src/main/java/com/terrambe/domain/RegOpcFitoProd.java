package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RegOpcFitoProd.
 */
@Entity
@Table(name = "reg_opc_fito_prod")
public class RegOpcFitoProd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "quantita_fito")
    private Float quantitaFito;

    @Column(name = "id_fitofarmaco")
    private Long idFitofarmaco;

    @Column(name = "id_magazzino")
    private Long idMagazzino;

    @Column(name = "id_avversita")
    private Long idAvversita;

    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali;

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("opcFitoToProds")
    private RegOpcFito prodToOpcFito;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcFitoProd idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcFitoProd idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public Float getQuantitaFito() {
        return quantitaFito;
    }

    public RegOpcFitoProd quantitaFito(Float quantitaFito) {
        this.quantitaFito = quantitaFito;
        return this;
    }

    public void setQuantitaFito(Float quantitaFito) {
        this.quantitaFito = quantitaFito;
    }

    public Long getIdFitofarmaco() {
        return idFitofarmaco;
    }

    public RegOpcFitoProd idFitofarmaco(Long idFitofarmaco) {
        this.idFitofarmaco = idFitofarmaco;
        return this;
    }

    public void setIdFitofarmaco(Long idFitofarmaco) {
        this.idFitofarmaco = idFitofarmaco;
    }

    public Long getIdMagazzino() {
        return idMagazzino;
    }

    public RegOpcFitoProd idMagazzino(Long idMagazzino) {
        this.idMagazzino = idMagazzino;
        return this;
    }

    public void setIdMagazzino(Long idMagazzino) {
        this.idMagazzino = idMagazzino;
    }

    public Long getIdAvversita() {
        return idAvversita;
    }

    public RegOpcFitoProd idAvversita(Long idAvversita) {
        this.idAvversita = idAvversita;
        return this;
    }

    public void setIdAvversita(Long idAvversita) {
        this.idAvversita = idAvversita;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcFitoProd dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcFitoProd dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcFitoProd userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcFitoProd userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcFitoProd note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public RegOpcFito getProdToOpcFito() {
        return prodToOpcFito;
    }

    public RegOpcFitoProd prodToOpcFito(RegOpcFito regOpcFito) {
        this.prodToOpcFito = regOpcFito;
        return this;
    }

    public void setProdToOpcFito(RegOpcFito regOpcFito) {
        this.prodToOpcFito = regOpcFito;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcFitoProd)) {
            return false;
        }
        return id != null && id.equals(((RegOpcFitoProd) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcFitoProd{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", quantitaFito=" + getQuantitaFito() +
            ", idFitofarmaco=" + getIdFitofarmaco() +
            ", idMagazzino=" + getIdMagazzino() +
            ", idAvversita=" + getIdAvversita() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
