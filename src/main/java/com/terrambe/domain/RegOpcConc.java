package com.terrambe.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RegOpcConc.
 */
@Entity
@Table(name = "reg_opc_conc")
@DynamicUpdate
public class RegOpcConc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_azienda", nullable = false)
    private Long idAzienda;

    @Column(name = "id_unita_prod")
    private Long idUnitaProd;

    @Column(name = "data_iniz_opc")
    private LocalDate dataInizOpc;

    @Column(name = "data_fine_opc")
    private LocalDate dataFineOpc;

    @Column(name = "tempo_impiegato")
    private String tempoImpiegato;

    @Column(name = "vol_acqua_fert_irr")
    private Float volAcquaFertIrr;

    @Column(name = "id_coltura")
    private Long idColtura;

    @Column(name = "id_operatore")
    private Long idOperatore;

    @Column(name = "id_mezzo")
    private Long idMezzo;

    @CreationTimestamp
    @Column(name = "data_iniz_vali")
    private LocalDate dataInizVali;

    @Column(name = "data_fine_vali")
    private LocalDate dataFineVali = LocalDate.of(9999,12,31);

    @Column(name = "user_id_creator")
    private Long userIdCreator;

    @Column(name = "user_id_last_mod")
    private Long userIdLastMod;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "note")
    private String note;
    
//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "appz_to_opc_conc_id")
    @JsonIgnoreProperties("appzToOpcConc")
    //@OneToMany(mappedBy = "appzToOpcConc")
    private Set<RegOpcConcAppz> opcConcToAppzs = new HashSet<>();

//MODIFICA -----------------------------------------
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "prod_to_opc_conc_id")
    @JsonIgnoreProperties("prodToOpcConc")
    //@OneToMany(mappedBy = "prodToOpcConc")
    private Set<RegOpcConcProd> opcConcToProds = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("motivoToOpcConcs")
    private MotivoConc opcConcToMotivo;

    @ManyToOne
    @JsonIgnoreProperties("tipoDistToOpcConcs")
    private TipoDistConc opcConcToTipoDist;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAzienda() {
        return idAzienda;
    }

    public RegOpcConc idAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
        return this;
    }

    public void setIdAzienda(Long idAzienda) {
        this.idAzienda = idAzienda;
    }

    public Long getIdUnitaProd() {
        return idUnitaProd;
    }

    public RegOpcConc idUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
        return this;
    }

    public void setIdUnitaProd(Long idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDate getDataInizOpc() {
        return dataInizOpc;
    }

    public RegOpcConc dataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
        return this;
    }

    public void setDataInizOpc(LocalDate dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDate getDataFineOpc() {
        return dataFineOpc;
    }

    public RegOpcConc dataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
        return this;
    }

    public void setDataFineOpc(LocalDate dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public String getTempoImpiegato() {
        return tempoImpiegato;
    }

    public RegOpcConc tempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
        return this;
    }

    public void setTempoImpiegato(String tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public Float getVolAcquaFertIrr() {
        return volAcquaFertIrr;
    }

    public RegOpcConc volAcquaFertIrr(Float volAcquaFertIrr) {
        this.volAcquaFertIrr = volAcquaFertIrr;
        return this;
    }

    public void setVolAcquaFertIrr(Float volAcquaFertIrr) {
        this.volAcquaFertIrr = volAcquaFertIrr;
    }

    public Long getIdColtura() {
        return idColtura;
    }

    public RegOpcConc idColtura(Long idColtura) {
        this.idColtura = idColtura;
        return this;
    }

    public void setIdColtura(Long idColtura) {
        this.idColtura = idColtura;
    }

    public Long getIdOperatore() {
        return idOperatore;
    }

    public RegOpcConc idOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
        return this;
    }

    public void setIdOperatore(Long idOperatore) {
        this.idOperatore = idOperatore;
    }

    public Long getIdMezzo() {
        return idMezzo;
    }

    public RegOpcConc idMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
        return this;
    }

    public void setIdMezzo(Long idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDate getDataInizVali() {
        return dataInizVali;
    }

    public RegOpcConc dataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
        return this;
    }

    public void setDataInizVali(LocalDate dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDate getDataFineVali() {
        return dataFineVali;
    }

    public RegOpcConc dataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
        return this;
    }

    public void setDataFineVali(LocalDate dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public Long getUserIdCreator() {
        return userIdCreator;
    }

    public RegOpcConc userIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
        return this;
    }

    public void setUserIdCreator(Long userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public Long getUserIdLastMod() {
        return userIdLastMod;
    }

    public RegOpcConc userIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
        return this;
    }

    public void setUserIdLastMod(Long userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public String getNote() {
        return note;
    }

    public RegOpcConc note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<RegOpcConcAppz> getOpcConcToAppzs() {
        return opcConcToAppzs;
    }

    public RegOpcConc opcConcToAppzs(Set<RegOpcConcAppz> regOpcConcAppzs) {
        this.opcConcToAppzs = regOpcConcAppzs;
        return this;
    }

    public RegOpcConc addOpcConcToAppz(RegOpcConcAppz regOpcConcAppz) {
        this.opcConcToAppzs.add(regOpcConcAppz);
        regOpcConcAppz.setAppzToOpcConc(this);
        return this;
    }

    public RegOpcConc removeOpcConcToAppz(RegOpcConcAppz regOpcConcAppz) {
        this.opcConcToAppzs.remove(regOpcConcAppz);
        regOpcConcAppz.setAppzToOpcConc(null);
        return this;
    }

    public void setOpcConcToAppzs(Set<RegOpcConcAppz> regOpcConcAppzs) {
        this.opcConcToAppzs = regOpcConcAppzs;
    }

    public Set<RegOpcConcProd> getOpcConcToProds() {
        return opcConcToProds;
    }

    public RegOpcConc opcConcToProds(Set<RegOpcConcProd> regOpcConcProds) {
        this.opcConcToProds = regOpcConcProds;
        return this;
    }

    public RegOpcConc addOpcConcToProd(RegOpcConcProd regOpcConcProd) {
        this.opcConcToProds.add(regOpcConcProd);
        regOpcConcProd.setProdToOpcConc(this);
        return this;
    }

    public RegOpcConc removeOpcConcToProd(RegOpcConcProd regOpcConcProd) {
        this.opcConcToProds.remove(regOpcConcProd);
        regOpcConcProd.setProdToOpcConc(null);
        return this;
    }

    public void setOpcConcToProds(Set<RegOpcConcProd> regOpcConcProds) {
        this.opcConcToProds = regOpcConcProds;
    }

    public MotivoConc getOpcConcToMotivo() {
        return opcConcToMotivo;
    }

    public RegOpcConc opcConcToMotivo(MotivoConc motivoConc) {
        this.opcConcToMotivo = motivoConc;
        return this;
    }

    public void setOpcConcToMotivo(MotivoConc motivoConc) {
        this.opcConcToMotivo = motivoConc;
    }

    public TipoDistConc getOpcConcToTipoDist() {
        return opcConcToTipoDist;
    }

    public RegOpcConc opcConcToTipoDist(TipoDistConc tipoDistConc) {
        this.opcConcToTipoDist = tipoDistConc;
        return this;
    }

    public void setOpcConcToTipoDist(TipoDistConc tipoDistConc) {
        this.opcConcToTipoDist = tipoDistConc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegOpcConc)) {
            return false;
        }
        return id != null && id.equals(((RegOpcConc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RegOpcConc{" +
            "id=" + getId() +
            ", idAzienda=" + getIdAzienda() +
            ", idUnitaProd=" + getIdUnitaProd() +
            ", dataInizOpc='" + getDataInizOpc() + "'" +
            ", dataFineOpc='" + getDataFineOpc() + "'" +
            ", tempoImpiegato='" + getTempoImpiegato() + "'" +
            ", volAcquaFertIrr=" + getVolAcquaFertIrr() +
            ", idColtura=" + getIdColtura() +
            ", idOperatore=" + getIdOperatore() +
            ", idMezzo=" + getIdMezzo() +
            ", dataInizVali='" + getDataInizVali() + "'" +
            ", dataFineVali='" + getDataFineVali() + "'" +
            ", userIdCreator=" + getUserIdCreator() +
            ", userIdLastMod=" + getUserIdLastMod() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
