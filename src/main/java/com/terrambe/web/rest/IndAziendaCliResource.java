package com.terrambe.web.rest;

import com.terrambe.domain.IndAziendaCli;
import com.terrambe.service.IndAziendaCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndAziendaCliCriteria;
import com.terrambe.service.IndAziendaCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndAziendaCli}.
 */
@RestController
@RequestMapping("/api")
public class IndAziendaCliResource {

    private final Logger log = LoggerFactory.getLogger(IndAziendaCliResource.class);

    private static final String ENTITY_NAME = "indAziendaCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndAziendaCliService indAziendaCliService;

    private final IndAziendaCliQueryService indAziendaCliQueryService;

    public IndAziendaCliResource(IndAziendaCliService indAziendaCliService, IndAziendaCliQueryService indAziendaCliQueryService) {
        this.indAziendaCliService = indAziendaCliService;
        this.indAziendaCliQueryService = indAziendaCliQueryService;
    }

    /**
     * {@code POST  /ind-azienda-clis} : Create a new indAziendaCli.
     *
     * @param indAziendaCli the indAziendaCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indAziendaCli, or with status {@code 400 (Bad Request)} if the indAziendaCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-azienda-clis")
    public ResponseEntity<IndAziendaCli> createIndAziendaCli(@Valid @RequestBody IndAziendaCli indAziendaCli) throws URISyntaxException {
        log.debug("REST request to save IndAziendaCli : {}", indAziendaCli);
        if (indAziendaCli.getId() != null) {
            throw new BadRequestAlertException("A new indAziendaCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndAziendaCli result = indAziendaCliService.save(indAziendaCli);
        return ResponseEntity.created(new URI("/api/ind-azienda-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-azienda-clis} : Updates an existing indAziendaCli.
     *
     * @param indAziendaCli the indAziendaCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indAziendaCli,
     * or with status {@code 400 (Bad Request)} if the indAziendaCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indAziendaCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-azienda-clis")
    public ResponseEntity<IndAziendaCli> updateIndAziendaCli(@Valid @RequestBody IndAziendaCli indAziendaCli) throws URISyntaxException {
        log.debug("REST request to update IndAziendaCli : {}", indAziendaCli);
        if (indAziendaCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndAziendaCli result = indAziendaCliService.save(indAziendaCli);
        IndAziendaCli result2 = indAziendaCliService.save(indAziendaCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indAziendaCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-azienda-clis} : get all the indAziendaClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indAziendaClis in body.
     */
    @GetMapping("/ind-azienda-clis")
    public ResponseEntity<List<IndAziendaCli>> getAllIndAziendaClis(IndAziendaCliCriteria criteria) {
        log.debug("REST request to get IndAziendaClis by criteria: {}", criteria);
        List<IndAziendaCli> entityList = indAziendaCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-azienda-clis/count} : count all the indAziendaClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-azienda-clis/count")
    public ResponseEntity<Long> countIndAziendaClis(IndAziendaCliCriteria criteria) {
        log.debug("REST request to count IndAziendaClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(indAziendaCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-azienda-clis/:id} : get the "id" indAziendaCli.
     *
     * @param id the id of the indAziendaCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indAziendaCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-azienda-clis/{id}")
    public ResponseEntity<IndAziendaCli> getIndAziendaCli(@PathVariable Long id) {
        log.debug("REST request to get IndAziendaCli : {}", id);
        Optional<IndAziendaCli> indAziendaCli = indAziendaCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indAziendaCli);
    }

    /**
     * {@code DELETE  /ind-azienda-clis/:id} : delete the "id" indAziendaCli.
     *
     * @param id the id of the indAziendaCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-azienda-clis/{id}")
    public ResponseEntity<Void> deleteIndAziendaCli(@PathVariable Long id) {
        log.debug("REST request to delete IndAziendaCli : {}", id);
        indAziendaCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
