package com.terrambe.web.rest;

import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.service.AziFormaGiuridicaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziFormaGiuridicaCriteria;
import com.terrambe.service.AziFormaGiuridicaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziFormaGiuridica}.
 */
@RestController
@RequestMapping("/api")
public class AziFormaGiuridicaResource {

    private final Logger log = LoggerFactory.getLogger(AziFormaGiuridicaResource.class);

    private static final String ENTITY_NAME = "aziFormaGiuridica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziFormaGiuridicaService aziFormaGiuridicaService;

    private final AziFormaGiuridicaQueryService aziFormaGiuridicaQueryService;

    public AziFormaGiuridicaResource(AziFormaGiuridicaService aziFormaGiuridicaService, AziFormaGiuridicaQueryService aziFormaGiuridicaQueryService) {
        this.aziFormaGiuridicaService = aziFormaGiuridicaService;
        this.aziFormaGiuridicaQueryService = aziFormaGiuridicaQueryService;
    }

    /**
     * {@code POST  /azi-forma-giuridicas} : Create a new aziFormaGiuridica.
     *
     * @param aziFormaGiuridica the aziFormaGiuridica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziFormaGiuridica, or with status {@code 400 (Bad Request)} if the aziFormaGiuridica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-forma-giuridicas")
    public ResponseEntity<AziFormaGiuridica> createAziFormaGiuridica(@Valid @RequestBody AziFormaGiuridica aziFormaGiuridica) throws URISyntaxException {
        log.debug("REST request to save AziFormaGiuridica : {}", aziFormaGiuridica);
        if (aziFormaGiuridica.getId() != null) {
            throw new BadRequestAlertException("A new aziFormaGiuridica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziFormaGiuridica result = aziFormaGiuridicaService.save(aziFormaGiuridica);
        return ResponseEntity.created(new URI("/api/azi-forma-giuridicas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-forma-giuridicas} : Updates an existing aziFormaGiuridica.
     *
     * @param aziFormaGiuridica the aziFormaGiuridica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziFormaGiuridica,
     * or with status {@code 400 (Bad Request)} if the aziFormaGiuridica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziFormaGiuridica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-forma-giuridicas")
    public ResponseEntity<AziFormaGiuridica> updateAziFormaGiuridica(@Valid @RequestBody AziFormaGiuridica aziFormaGiuridica) throws URISyntaxException {
        log.debug("REST request to update AziFormaGiuridica : {}", aziFormaGiuridica);
        if (aziFormaGiuridica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziFormaGiuridica result = aziFormaGiuridicaService.save(aziFormaGiuridica);
        AziFormaGiuridica result2 = aziFormaGiuridicaService.save(aziFormaGiuridica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziFormaGiuridica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-forma-giuridicas} : get all the aziFormaGiuridicas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziFormaGiuridicas in body.
     */
    @GetMapping("/azi-forma-giuridicas")
    public ResponseEntity<List<AziFormaGiuridica>> getAllAziFormaGiuridicas(AziFormaGiuridicaCriteria criteria) {
        log.debug("REST request to get AziFormaGiuridicas by criteria: {}", criteria);
        List<AziFormaGiuridica> entityList = aziFormaGiuridicaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-forma-giuridicas/count} : count all the aziFormaGiuridicas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-forma-giuridicas/count")
    public ResponseEntity<Long> countAziFormaGiuridicas(AziFormaGiuridicaCriteria criteria) {
        log.debug("REST request to count AziFormaGiuridicas by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziFormaGiuridicaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-forma-giuridicas/:id} : get the "id" aziFormaGiuridica.
     *
     * @param id the id of the aziFormaGiuridica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziFormaGiuridica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-forma-giuridicas/{id}")
    public ResponseEntity<AziFormaGiuridica> getAziFormaGiuridica(@PathVariable Long id) {
        log.debug("REST request to get AziFormaGiuridica : {}", id);
        Optional<AziFormaGiuridica> aziFormaGiuridica = aziFormaGiuridicaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziFormaGiuridica);
    }

    /**
     * {@code DELETE  /azi-forma-giuridicas/:id} : delete the "id" aziFormaGiuridica.
     *
     * @param id the id of the aziFormaGiuridica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-forma-giuridicas/{id}")
    public ResponseEntity<Void> deleteAziFormaGiuridica(@PathVariable Long id) {
        log.debug("REST request to delete AziFormaGiuridica : {}", id);
        aziFormaGiuridicaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
