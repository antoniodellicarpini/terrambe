package com.terrambe.web.rest;

import com.terrambe.domain.RecForn;
import com.terrambe.service.RecFornService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecFornCriteria;
import com.terrambe.service.RecFornQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecForn}.
 */
@RestController
@RequestMapping("/api")
public class RecFornResource {

    private final Logger log = LoggerFactory.getLogger(RecFornResource.class);

    private static final String ENTITY_NAME = "recForn";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecFornService recFornService;

    private final RecFornQueryService recFornQueryService;

    public RecFornResource(RecFornService recFornService, RecFornQueryService recFornQueryService) {
        this.recFornService = recFornService;
        this.recFornQueryService = recFornQueryService;
    }

    /**
     * {@code POST  /rec-forns} : Create a new recForn.
     *
     * @param recForn the recForn to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recForn, or with status {@code 400 (Bad Request)} if the recForn has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-forns")
    public ResponseEntity<RecForn> createRecForn(@RequestBody RecForn recForn) throws URISyntaxException {
        log.debug("REST request to save RecForn : {}", recForn);
        if (recForn.getId() != null) {
            throw new BadRequestAlertException("A new recForn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecForn result = recFornService.save(recForn);
        return ResponseEntity.created(new URI("/api/rec-forns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-forns} : Updates an existing recForn.
     *
     * @param recForn the recForn to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recForn,
     * or with status {@code 400 (Bad Request)} if the recForn is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recForn couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-forns")
    public ResponseEntity<RecForn> updateRecForn(@RequestBody RecForn recForn) throws URISyntaxException {
        log.debug("REST request to update RecForn : {}", recForn);
        if (recForn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecForn result = recFornService.save(recForn);
        RecForn result2 = recFornService.save(recForn);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recForn.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-forns} : get all the recForns.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recForns in body.
     */
    @GetMapping("/rec-forns")
    public ResponseEntity<List<RecForn>> getAllRecForns(RecFornCriteria criteria) {
        log.debug("REST request to get RecForns by criteria: {}", criteria);
        List<RecForn> entityList = recFornQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-forns/count} : count all the recForns.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-forns/count")
    public ResponseEntity<Long> countRecForns(RecFornCriteria criteria) {
        log.debug("REST request to count RecForns by criteria: {}", criteria);
        return ResponseEntity.ok().body(recFornQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-forns/:id} : get the "id" recForn.
     *
     * @param id the id of the recForn to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recForn, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-forns/{id}")
    public ResponseEntity<RecForn> getRecForn(@PathVariable Long id) {
        log.debug("REST request to get RecForn : {}", id);
        Optional<RecForn> recForn = recFornService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recForn);
    }

    /**
     * {@code DELETE  /rec-forns/:id} : delete the "id" recForn.
     *
     * @param id the id of the recForn to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-forns/{id}")
    public ResponseEntity<Void> deleteRecForn(@PathVariable Long id) {
        log.debug("REST request to delete RecForn : {}", id);
        recFornService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
