package com.terrambe.web.rest;

import com.terrambe.domain.TipoTrappola;
import com.terrambe.service.TipoTrappolaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoTrappolaCriteria;
import com.terrambe.service.TipoTrappolaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoTrappola}.
 */
@RestController
@RequestMapping("/api")
public class TipoTrappolaResource {

    private final Logger log = LoggerFactory.getLogger(TipoTrappolaResource.class);

    private static final String ENTITY_NAME = "tipoTrappola";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoTrappolaService tipoTrappolaService;

    private final TipoTrappolaQueryService tipoTrappolaQueryService;

    public TipoTrappolaResource(TipoTrappolaService tipoTrappolaService, TipoTrappolaQueryService tipoTrappolaQueryService) {
        this.tipoTrappolaService = tipoTrappolaService;
        this.tipoTrappolaQueryService = tipoTrappolaQueryService;
    }

    /**
     * {@code POST  /tipo-trappolas} : Create a new tipoTrappola.
     *
     * @param tipoTrappola the tipoTrappola to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoTrappola, or with status {@code 400 (Bad Request)} if the tipoTrappola has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-trappolas")
    public ResponseEntity<TipoTrappola> createTipoTrappola(@Valid @RequestBody TipoTrappola tipoTrappola) throws URISyntaxException {
        log.debug("REST request to save TipoTrappola : {}", tipoTrappola);
        if (tipoTrappola.getId() != null) {
            throw new BadRequestAlertException("A new tipoTrappola cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoTrappola result = tipoTrappolaService.save(tipoTrappola);
        return ResponseEntity.created(new URI("/api/tipo-trappolas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-trappolas} : Updates an existing tipoTrappola.
     *
     * @param tipoTrappola the tipoTrappola to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoTrappola,
     * or with status {@code 400 (Bad Request)} if the tipoTrappola is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoTrappola couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-trappolas")
    public ResponseEntity<TipoTrappola> updateTipoTrappola(@Valid @RequestBody TipoTrappola tipoTrappola) throws URISyntaxException {
        log.debug("REST request to update TipoTrappola : {}", tipoTrappola);
        if (tipoTrappola.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoTrappola result = tipoTrappolaService.save(tipoTrappola);
        TipoTrappola result2 = tipoTrappolaService.save(tipoTrappola);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoTrappola.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-trappolas} : get all the tipoTrappolas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoTrappolas in body.
     */
    @GetMapping("/tipo-trappolas")
    public ResponseEntity<List<TipoTrappola>> getAllTipoTrappolas(TipoTrappolaCriteria criteria) {
        log.debug("REST request to get TipoTrappolas by criteria: {}", criteria);
        List<TipoTrappola> entityList = tipoTrappolaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-trappolas/count} : count all the tipoTrappolas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-trappolas/count")
    public ResponseEntity<Long> countTipoTrappolas(TipoTrappolaCriteria criteria) {
        log.debug("REST request to count TipoTrappolas by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoTrappolaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-trappolas/:id} : get the "id" tipoTrappola.
     *
     * @param id the id of the tipoTrappola to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoTrappola, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-trappolas/{id}")
    public ResponseEntity<TipoTrappola> getTipoTrappola(@PathVariable Long id) {
        log.debug("REST request to get TipoTrappola : {}", id);
        Optional<TipoTrappola> tipoTrappola = tipoTrappolaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoTrappola);
    }

    /**
     * {@code DELETE  /tipo-trappolas/:id} : delete the "id" tipoTrappola.
     *
     * @param id the id of the tipoTrappola to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-trappolas/{id}")
    public ResponseEntity<Void> deleteTipoTrappola(@PathVariable Long id) {
        log.debug("REST request to delete TipoTrappola : {}", id);
        tipoTrappolaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
