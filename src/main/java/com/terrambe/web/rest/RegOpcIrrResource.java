package com.terrambe.web.rest;

import com.terrambe.domain.ListRegOpcIrr;
import com.terrambe.domain.RegOpcIrr;
import com.terrambe.service.RegOpcIrrService;
import com.terrambe.service.UtilityJpaQueriesService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcIrrCriteria;
import com.terrambe.service.RegOpcIrrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcIrr}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcIrrResource {

    @Autowired
    private DataSource dataSource;
    
    private final Logger log = LoggerFactory.getLogger(RegOpcIrrResource.class);

    private static final String ENTITY_NAME = "regOpcIrr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcIrrService regOpcIrrService;

    private final RegOpcIrrQueryService regOpcIrrQueryService;

    public RegOpcIrrResource(RegOpcIrrService regOpcIrrService, RegOpcIrrQueryService regOpcIrrQueryService) {
        this.regOpcIrrService = regOpcIrrService;
        this.regOpcIrrQueryService = regOpcIrrQueryService;
    }

    /**
     * {@code POST  /reg-opc-irrs} : Create a new regOpcIrr.
     *
     * @param regOpcIrr the regOpcIrr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcIrr, or with status {@code 400 (Bad Request)} if the regOpcIrr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-irrs")
    public ResponseEntity<RegOpcIrr> createRegOpcIrr(@Valid @RequestBody RegOpcIrr regOpcIrr) throws URISyntaxException {
        log.debug("REST request to save RegOpcIrr : {}", regOpcIrr);
        if (regOpcIrr.getId() != null) {
            throw new BadRequestAlertException("A new regOpcIrr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcIrr result = regOpcIrrService.save(regOpcIrr);
        return ResponseEntity.created(new URI("/api/reg-opc-irrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-irrs} : Updates an existing regOpcIrr.
     *
     * @param regOpcIrr the regOpcIrr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcIrr,
     * or with status {@code 400 (Bad Request)} if the regOpcIrr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcIrr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-irrs")
    public ResponseEntity<RegOpcIrr> updateRegOpcIrr(@Valid @RequestBody RegOpcIrr regOpcIrr) throws URISyntaxException {
        log.debug("REST request to update RegOpcIrr : {}", regOpcIrr);
        if (regOpcIrr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcIrr result = regOpcIrrService.save(regOpcIrr);
        RegOpcIrr result2 = regOpcIrrService.save(regOpcIrr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcIrr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-irrs} : get all the regOpcIrrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcIrrs in body.
     */
    @GetMapping("/reg-opc-irrs")
    public ResponseEntity<List<RegOpcIrr>> getAllRegOpcIrrs(RegOpcIrrCriteria criteria) {
        log.debug("REST request to get RegOpcIrrs by criteria: {}", criteria);
        List<RegOpcIrr> entityList = regOpcIrrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    @GetMapping("/reg-opc-irrigazione-list")
    public ResponseEntity<List<ListRegOpcIrr>> getListRegOpcIrrigazione(@RequestParam(value = "azienda_id.equals") Long azienda_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<ListRegOpcIrr> result_listregopc = utility.getListRegOpcIrrigazione(azienda_id, dataSource);
        return ResponseEntity.ok().body(result_listregopc);
    }

    /**
    * {@code GET  /reg-opc-irrs/count} : count all the regOpcIrrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-irrs/count")
    public ResponseEntity<Long> countRegOpcIrrs(RegOpcIrrCriteria criteria) {
        log.debug("REST request to count RegOpcIrrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcIrrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-irrs/:id} : get the "id" regOpcIrr.
     *
     * @param id the id of the regOpcIrr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcIrr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-irrs/{id}")
    public ResponseEntity<RegOpcIrr> getRegOpcIrr(@PathVariable Long id) {
        log.debug("REST request to get RegOpcIrr : {}", id);
        Optional<RegOpcIrr> regOpcIrr = regOpcIrrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcIrr);
    }

    /**
     * {@code DELETE  /reg-opc-irrs/:id} : delete the "id" regOpcIrr.
     *
     * @param id the id of the regOpcIrr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-irrs/{id}")
    public ResponseEntity<Void> deleteRegOpcIrr(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcIrr : {}", id);
        regOpcIrrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
