package com.terrambe.web.rest;

import com.terrambe.domain.AziSupPartConduzione;
import com.terrambe.service.AziSupPartConduzioneService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziSupPartConduzioneCriteria;
import com.terrambe.service.AziSupPartConduzioneQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziSupPartConduzione}.
 */
@RestController
@RequestMapping("/api")
public class AziSupPartConduzioneResource {

    private final Logger log = LoggerFactory.getLogger(AziSupPartConduzioneResource.class);

    private static final String ENTITY_NAME = "aziSupPartConduzione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziSupPartConduzioneService aziSupPartConduzioneService;

    private final AziSupPartConduzioneQueryService aziSupPartConduzioneQueryService;

    public AziSupPartConduzioneResource(AziSupPartConduzioneService aziSupPartConduzioneService, AziSupPartConduzioneQueryService aziSupPartConduzioneQueryService) {
        this.aziSupPartConduzioneService = aziSupPartConduzioneService;
        this.aziSupPartConduzioneQueryService = aziSupPartConduzioneQueryService;
    }

    /**
     * {@code POST  /azi-sup-part-conduziones} : Create a new aziSupPartConduzione.
     *
     * @param aziSupPartConduzione the aziSupPartConduzione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziSupPartConduzione, or with status {@code 400 (Bad Request)} if the aziSupPartConduzione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-sup-part-conduziones")
    public ResponseEntity<AziSupPartConduzione> createAziSupPartConduzione(@Valid @RequestBody AziSupPartConduzione aziSupPartConduzione) throws URISyntaxException {
        log.debug("REST request to save AziSupPartConduzione : {}", aziSupPartConduzione);
        if (aziSupPartConduzione.getId() != null) {
            throw new BadRequestAlertException("A new aziSupPartConduzione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziSupPartConduzione result = aziSupPartConduzioneService.save(aziSupPartConduzione);
        return ResponseEntity.created(new URI("/api/azi-sup-part-conduziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-sup-part-conduziones} : Updates an existing aziSupPartConduzione.
     *
     * @param aziSupPartConduzione the aziSupPartConduzione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziSupPartConduzione,
     * or with status {@code 400 (Bad Request)} if the aziSupPartConduzione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziSupPartConduzione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-sup-part-conduziones")
    public ResponseEntity<AziSupPartConduzione> updateAziSupPartConduzione(@Valid @RequestBody AziSupPartConduzione aziSupPartConduzione) throws URISyntaxException {
        log.debug("REST request to update AziSupPartConduzione : {}", aziSupPartConduzione);
        if (aziSupPartConduzione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziSupPartConduzione result = aziSupPartConduzioneService.save(aziSupPartConduzione);
        AziSupPartConduzione result2 = aziSupPartConduzioneService.save(aziSupPartConduzione);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziSupPartConduzione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-sup-part-conduziones} : get all the aziSupPartConduziones.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziSupPartConduziones in body.
     */
    @GetMapping("/azi-sup-part-conduziones")
    public ResponseEntity<List<AziSupPartConduzione>> getAllAziSupPartConduziones(AziSupPartConduzioneCriteria criteria) {
        log.debug("REST request to get AziSupPartConduziones by criteria: {}", criteria);
        List<AziSupPartConduzione> entityList = aziSupPartConduzioneQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-sup-part-conduziones/count} : count all the aziSupPartConduziones.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-sup-part-conduziones/count")
    public ResponseEntity<Long> countAziSupPartConduziones(AziSupPartConduzioneCriteria criteria) {
        log.debug("REST request to count AziSupPartConduziones by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziSupPartConduzioneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-sup-part-conduziones/:id} : get the "id" aziSupPartConduzione.
     *
     * @param id the id of the aziSupPartConduzione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziSupPartConduzione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-sup-part-conduziones/{id}")
    public ResponseEntity<AziSupPartConduzione> getAziSupPartConduzione(@PathVariable Long id) {
        log.debug("REST request to get AziSupPartConduzione : {}", id);
        Optional<AziSupPartConduzione> aziSupPartConduzione = aziSupPartConduzioneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziSupPartConduzione);
    }

    /**
     * {@code DELETE  /azi-sup-part-conduziones/:id} : delete the "id" aziSupPartConduzione.
     *
     * @param id the id of the aziSupPartConduzione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-sup-part-conduziones/{id}")
    public ResponseEntity<Void> deleteAziSupPartConduzione(@PathVariable Long id) {
        log.debug("REST request to delete AziSupPartConduzione : {}", id);
        aziSupPartConduzioneService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
