package com.terrambe.web.rest;

import com.terrambe.domain.IndAziRap;
import com.terrambe.service.IndAziRapService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndAziRapCriteria;
import com.terrambe.service.IndAziRapQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndAziRap}.
 */
@RestController
@RequestMapping("/api")
public class IndAziRapResource {

    private final Logger log = LoggerFactory.getLogger(IndAziRapResource.class);

    private static final String ENTITY_NAME = "indAziRap";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndAziRapService indAziRapService;

    private final IndAziRapQueryService indAziRapQueryService;

    public IndAziRapResource(IndAziRapService indAziRapService, IndAziRapQueryService indAziRapQueryService) {
        this.indAziRapService = indAziRapService;
        this.indAziRapQueryService = indAziRapQueryService;
    }

    /**
     * {@code POST  /ind-azi-raps} : Create a new indAziRap.
     *
     * @param indAziRap the indAziRap to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indAziRap, or with status {@code 400 (Bad Request)} if the indAziRap has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-azi-raps")
    public ResponseEntity<IndAziRap> createIndAziRap(@Valid @RequestBody IndAziRap indAziRap) throws URISyntaxException {
        log.debug("REST request to save IndAziRap : {}", indAziRap);
        if (indAziRap.getId() != null) {
            throw new BadRequestAlertException("A new indAziRap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndAziRap result = indAziRapService.save(indAziRap);
        return ResponseEntity.created(new URI("/api/ind-azi-raps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-azi-raps} : Updates an existing indAziRap.
     *
     * @param indAziRap the indAziRap to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indAziRap,
     * or with status {@code 400 (Bad Request)} if the indAziRap is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indAziRap couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-azi-raps")
    public ResponseEntity<IndAziRap> updateIndAziRap(@Valid @RequestBody IndAziRap indAziRap) throws URISyntaxException {
        log.debug("REST request to update IndAziRap : {}", indAziRap);
        if (indAziRap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndAziRap result = indAziRapService.save(indAziRap);
        IndAziRap result2 = indAziRapService.save(indAziRap);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indAziRap.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-azi-raps} : get all the indAziRaps.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indAziRaps in body.
     */
    @GetMapping("/ind-azi-raps")
    public ResponseEntity<List<IndAziRap>> getAllIndAziRaps(IndAziRapCriteria criteria) {
        log.debug("REST request to get IndAziRaps by criteria: {}", criteria);
        List<IndAziRap> entityList = indAziRapQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-azi-raps/count} : count all the indAziRaps.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-azi-raps/count")
    public ResponseEntity<Long> countIndAziRaps(IndAziRapCriteria criteria) {
        log.debug("REST request to count IndAziRaps by criteria: {}", criteria);
        return ResponseEntity.ok().body(indAziRapQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-azi-raps/:id} : get the "id" indAziRap.
     *
     * @param id the id of the indAziRap to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indAziRap, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-azi-raps/{id}")
    public ResponseEntity<IndAziRap> getIndAziRap(@PathVariable Long id) {
        log.debug("REST request to get IndAziRap : {}", id);
        Optional<IndAziRap> indAziRap = indAziRapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indAziRap);
    }

    /**
     * {@code DELETE  /ind-azi-raps/:id} : delete the "id" indAziRap.
     *
     * @param id the id of the indAziRap to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-azi-raps/{id}")
    public ResponseEntity<Void> deleteIndAziRap(@PathVariable Long id) {
        log.debug("REST request to delete IndAziRap : {}", id);
        indAziRapService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
