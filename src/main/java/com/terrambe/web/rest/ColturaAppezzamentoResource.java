package com.terrambe.web.rest;

import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.service.ColturaAppezzamentoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.ColturaAppezzamentoCriteria;
import com.terrambe.service.ColturaAppezzamentoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.ColturaAppezzamento}.
 */
@RestController
@RequestMapping("/api")
public class ColturaAppezzamentoResource {

    private final Logger log = LoggerFactory.getLogger(ColturaAppezzamentoResource.class);

    private static final String ENTITY_NAME = "colturaAppezzamento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ColturaAppezzamentoService colturaAppezzamentoService;

    private final ColturaAppezzamentoQueryService colturaAppezzamentoQueryService;

    public ColturaAppezzamentoResource(ColturaAppezzamentoService colturaAppezzamentoService, ColturaAppezzamentoQueryService colturaAppezzamentoQueryService) {
        this.colturaAppezzamentoService = colturaAppezzamentoService;
        this.colturaAppezzamentoQueryService = colturaAppezzamentoQueryService;
    }

    /**
     * {@code POST  /coltura-appezzamentos} : Create a new colturaAppezzamento.
     *
     * @param colturaAppezzamento the colturaAppezzamento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new colturaAppezzamento, or with status {@code 400 (Bad Request)} if the colturaAppezzamento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coltura-appezzamentos")
    public ResponseEntity<ColturaAppezzamento> createColturaAppezzamento(@Valid @RequestBody ColturaAppezzamento colturaAppezzamento) throws URISyntaxException {
        log.debug("REST request to save ColturaAppezzamento : {}", colturaAppezzamento);
        if (colturaAppezzamento.getId() != null) {
            throw new BadRequestAlertException("A new colturaAppezzamento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ColturaAppezzamento result = colturaAppezzamentoService.save(colturaAppezzamento);
        return ResponseEntity.created(new URI("/api/coltura-appezzamentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coltura-appezzamentos} : Updates an existing colturaAppezzamento.
     *
     * @param colturaAppezzamento the colturaAppezzamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated colturaAppezzamento,
     * or with status {@code 400 (Bad Request)} if the colturaAppezzamento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the colturaAppezzamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coltura-appezzamentos")
    public ResponseEntity<ColturaAppezzamento> updateColturaAppezzamento(@Valid @RequestBody ColturaAppezzamento colturaAppezzamento) throws URISyntaxException {
        log.debug("REST request to update ColturaAppezzamento : {}", colturaAppezzamento);
        if (colturaAppezzamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ColturaAppezzamento result = colturaAppezzamentoService.save(colturaAppezzamento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, colturaAppezzamento.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /coltura-appezzamentos} : get all the colturaAppezzamentos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of colturaAppezzamentos in body.
     */
    @GetMapping("/coltura-appezzamentos")
    public ResponseEntity<List<ColturaAppezzamento>> getAllColturaAppezzamentos(ColturaAppezzamentoCriteria criteria) {
        log.debug("REST request to get ColturaAppezzamentos by criteria: {}", criteria);
        List<ColturaAppezzamento> entityList = colturaAppezzamentoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /coltura-appezzamentos/count} : count all the colturaAppezzamentos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/coltura-appezzamentos/count")
    public ResponseEntity<Long> countColturaAppezzamentos(ColturaAppezzamentoCriteria criteria) {
        log.debug("REST request to count ColturaAppezzamentos by criteria: {}", criteria);
        return ResponseEntity.ok().body(colturaAppezzamentoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /coltura-appezzamentos/:id} : get the "id" colturaAppezzamento.
     *
     * @param id the id of the colturaAppezzamento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the colturaAppezzamento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coltura-appezzamentos/{id}")
    public ResponseEntity<ColturaAppezzamento> getColturaAppezzamento(@PathVariable Long id) {
        log.debug("REST request to get ColturaAppezzamento : {}", id);
        Optional<ColturaAppezzamento> colturaAppezzamento = colturaAppezzamentoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(colturaAppezzamento);
    }

    /**
     * {@code DELETE  /coltura-appezzamentos/:id} : delete the "id" colturaAppezzamento.
     *
     * @param id the id of the colturaAppezzamento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coltura-appezzamentos/{id}")
    public ResponseEntity<Void> deleteColturaAppezzamento(@PathVariable Long id) {
        log.debug("REST request to delete ColturaAppezzamento : {}", id);
        colturaAppezzamentoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
