package com.terrambe.web.rest;

import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.service.IncompatibilitaSAService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IncompatibilitaSACriteria;
import com.terrambe.service.IncompatibilitaSAQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IncompatibilitaSA}.
 */
@RestController
@RequestMapping("/api")
public class IncompatibilitaSAResource {

    private final Logger log = LoggerFactory.getLogger(IncompatibilitaSAResource.class);

    private static final String ENTITY_NAME = "incompatibilitaSA";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IncompatibilitaSAService incompatibilitaSAService;

    private final IncompatibilitaSAQueryService incompatibilitaSAQueryService;

    public IncompatibilitaSAResource(IncompatibilitaSAService incompatibilitaSAService, IncompatibilitaSAQueryService incompatibilitaSAQueryService) {
        this.incompatibilitaSAService = incompatibilitaSAService;
        this.incompatibilitaSAQueryService = incompatibilitaSAQueryService;
    }

    /**
     * {@code POST  /incompatibilita-sas} : Create a new incompatibilitaSA.
     *
     * @param incompatibilitaSA the incompatibilitaSA to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new incompatibilitaSA, or with status {@code 400 (Bad Request)} if the incompatibilitaSA has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/incompatibilita-sas")
    public ResponseEntity<IncompatibilitaSA> createIncompatibilitaSA(@RequestBody IncompatibilitaSA incompatibilitaSA) throws URISyntaxException {
        log.debug("REST request to save IncompatibilitaSA : {}", incompatibilitaSA);
        if (incompatibilitaSA.getId() != null) {
            throw new BadRequestAlertException("A new incompatibilitaSA cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IncompatibilitaSA result = incompatibilitaSAService.save(incompatibilitaSA);
        return ResponseEntity.created(new URI("/api/incompatibilita-sas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /incompatibilita-sas} : Updates an existing incompatibilitaSA.
     *
     * @param incompatibilitaSA the incompatibilitaSA to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated incompatibilitaSA,
     * or with status {@code 400 (Bad Request)} if the incompatibilitaSA is not valid,
     * or with status {@code 500 (Internal Server Error)} if the incompatibilitaSA couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/incompatibilita-sas")
    public ResponseEntity<IncompatibilitaSA> updateIncompatibilitaSA(@RequestBody IncompatibilitaSA incompatibilitaSA) throws URISyntaxException {
        log.debug("REST request to update IncompatibilitaSA : {}", incompatibilitaSA);
        if (incompatibilitaSA.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IncompatibilitaSA result = incompatibilitaSAService.save(incompatibilitaSA);
        IncompatibilitaSA result2 = incompatibilitaSAService.save(incompatibilitaSA);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, incompatibilitaSA.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /incompatibilita-sas} : get all the incompatibilitaSAS.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of incompatibilitaSAS in body.
     */
    @GetMapping("/incompatibilita-sas")
    public ResponseEntity<List<IncompatibilitaSA>> getAllIncompatibilitaSAS(IncompatibilitaSACriteria criteria) {
        log.debug("REST request to get IncompatibilitaSAS by criteria: {}", criteria);
        List<IncompatibilitaSA> entityList = incompatibilitaSAQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /incompatibilita-sas/count} : count all the incompatibilitaSAS.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/incompatibilita-sas/count")
    public ResponseEntity<Long> countIncompatibilitaSAS(IncompatibilitaSACriteria criteria) {
        log.debug("REST request to count IncompatibilitaSAS by criteria: {}", criteria);
        return ResponseEntity.ok().body(incompatibilitaSAQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /incompatibilita-sas/:id} : get the "id" incompatibilitaSA.
     *
     * @param id the id of the incompatibilitaSA to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the incompatibilitaSA, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/incompatibilita-sas/{id}")
    public ResponseEntity<IncompatibilitaSA> getIncompatibilitaSA(@PathVariable Long id) {
        log.debug("REST request to get IncompatibilitaSA : {}", id);
        Optional<IncompatibilitaSA> incompatibilitaSA = incompatibilitaSAService.findOne(id);
        return ResponseUtil.wrapOrNotFound(incompatibilitaSA);
    }

    /**
     * {@code DELETE  /incompatibilita-sas/:id} : delete the "id" incompatibilitaSA.
     *
     * @param id the id of the incompatibilitaSA to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/incompatibilita-sas/{id}")
    public ResponseEntity<Void> deleteIncompatibilitaSA(@PathVariable Long id) {
        log.debug("REST request to delete IncompatibilitaSA : {}", id);
        incompatibilitaSAService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
