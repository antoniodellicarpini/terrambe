package com.terrambe.web.rest;

import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.domain.EtichettaFitoFilter;
import com.terrambe.service.TabellaRaccordoService;
import com.terrambe.service.UtilityJpaQueriesService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TabellaRaccordoCriteria;
import com.terrambe.service.TabellaRaccordoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

/**
 * REST controller for managing {@link com.terrambe.domain.TabellaRaccordo}.
 */
@RestController
@RequestMapping("/api")
public class TabellaRaccordoResource {

    @Autowired
    private DataSource dataSource;
    
    private final Logger log = LoggerFactory.getLogger(TabellaRaccordoResource.class);

    private static final String ENTITY_NAME = "tabellaRaccordo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TabellaRaccordoService tabellaRaccordoService;

    private final TabellaRaccordoQueryService tabellaRaccordoQueryService;

    public TabellaRaccordoResource(TabellaRaccordoService tabellaRaccordoService, TabellaRaccordoQueryService tabellaRaccordoQueryService) {
        this.tabellaRaccordoService = tabellaRaccordoService;
        this.tabellaRaccordoQueryService = tabellaRaccordoQueryService;
    }

    /**
     * {@code POST  /tabella-raccordos} : Create a new tabellaRaccordo.
     *
     * @param tabellaRaccordo the tabellaRaccordo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tabellaRaccordo, or with status {@code 400 (Bad Request)} if the tabellaRaccordo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tabella-raccordos")
    public ResponseEntity<TabellaRaccordo> createTabellaRaccordo(@RequestBody TabellaRaccordo tabellaRaccordo) throws URISyntaxException {
        log.debug("REST request to save TabellaRaccordo : {}", tabellaRaccordo);
        if (tabellaRaccordo.getId() != null) {
            throw new BadRequestAlertException("A new tabellaRaccordo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TabellaRaccordo result = tabellaRaccordoService.save(tabellaRaccordo);
        return ResponseEntity.created(new URI("/api/tabella-raccordos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tabella-raccordos} : Updates an existing tabellaRaccordo.
     *
     * @param tabellaRaccordo the tabellaRaccordo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tabellaRaccordo,
     * or with status {@code 400 (Bad Request)} if the tabellaRaccordo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tabellaRaccordo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tabella-raccordos")
    public ResponseEntity<TabellaRaccordo> updateTabellaRaccordo(@RequestBody TabellaRaccordo tabellaRaccordo) throws URISyntaxException {
        log.debug("REST request to update TabellaRaccordo : {}", tabellaRaccordo);
        if (tabellaRaccordo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TabellaRaccordo result = tabellaRaccordoService.save(tabellaRaccordo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tabellaRaccordo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tabella-raccordos} : get all the tabellaRaccordos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tabellaRaccordos in body.
     */
    @GetMapping("/tabella-raccordos")
    public ResponseEntity<List<TabellaRaccordo>> getAllTabellaRaccordos(TabellaRaccordoCriteria criteria) {
        log.debug("REST request to get TabellaRaccordos by criteria: {}", criteria);
        List<TabellaRaccordo> entityList = tabellaRaccordoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    @GetMapping("/etichetta-fito-opcfitos")
    public ResponseEntity<List<EtichettaFitoFilter>> getAllTabellaRaccordosOpcFitos(@RequestParam(value = "coltura_id.equals") Long coltura_id,@RequestParam(value = "avversita_id.equals") Long avversita_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<EtichettaFitoFilter> result_list_etichettafitofilter = utility.getFitoFromAvversitaAndColtura(coltura_id,avversita_id, dataSource);
        return ResponseEntity.ok().body(result_list_etichettafitofilter);
    }

    /**
    * {@code GET  /tabella-raccordos/count} : count all the tabellaRaccordos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tabella-raccordos/count")
    public ResponseEntity<Long> countTabellaRaccordos(TabellaRaccordoCriteria criteria) {
        log.debug("REST request to count TabellaRaccordos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tabellaRaccordoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tabella-raccordos/:id} : get the "id" tabellaRaccordo.
     *
     * @param id the id of the tabellaRaccordo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tabellaRaccordo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tabella-raccordos/{id}")
    public ResponseEntity<TabellaRaccordo> getTabellaRaccordo(@PathVariable Long id) {
        log.debug("REST request to get TabellaRaccordo : {}", id);
        Optional<TabellaRaccordo> tabellaRaccordo = tabellaRaccordoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tabellaRaccordo);
    }

    /**
     * {@code DELETE  /tabella-raccordos/:id} : delete the "id" tabellaRaccordo.
     *
     * @param id the id of the tabellaRaccordo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tabella-raccordos/{id}")
    public ResponseEntity<Void> deleteTabellaRaccordo(@PathVariable Long id) {
        log.debug("REST request to delete TabellaRaccordo : {}", id);
        tabellaRaccordoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
