package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcPioAppz;
import com.terrambe.service.RegOpcPioAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcPioAppzCriteria;
import com.terrambe.service.RegOpcPioAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcPioAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcPioAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioAppzResource.class);

    private static final String ENTITY_NAME = "regOpcPioAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcPioAppzService regOpcPioAppzService;

    private final RegOpcPioAppzQueryService regOpcPioAppzQueryService;

    public RegOpcPioAppzResource(RegOpcPioAppzService regOpcPioAppzService, RegOpcPioAppzQueryService regOpcPioAppzQueryService) {
        this.regOpcPioAppzService = regOpcPioAppzService;
        this.regOpcPioAppzQueryService = regOpcPioAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-pio-appzs} : Create a new regOpcPioAppz.
     *
     * @param regOpcPioAppz the regOpcPioAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcPioAppz, or with status {@code 400 (Bad Request)} if the regOpcPioAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-pio-appzs")
    public ResponseEntity<RegOpcPioAppz> createRegOpcPioAppz(@Valid @RequestBody RegOpcPioAppz regOpcPioAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcPioAppz : {}", regOpcPioAppz);
        if (regOpcPioAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcPioAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcPioAppz result = regOpcPioAppzService.save(regOpcPioAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-pio-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-pio-appzs} : Updates an existing regOpcPioAppz.
     *
     * @param regOpcPioAppz the regOpcPioAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcPioAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcPioAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcPioAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-pio-appzs")
    public ResponseEntity<RegOpcPioAppz> updateRegOpcPioAppz(@Valid @RequestBody RegOpcPioAppz regOpcPioAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcPioAppz : {}", regOpcPioAppz);
        if (regOpcPioAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcPioAppz result = regOpcPioAppzService.save(regOpcPioAppz);
        RegOpcPioAppz result2 = regOpcPioAppzService.save(regOpcPioAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcPioAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-pio-appzs} : get all the regOpcPioAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcPioAppzs in body.
     */
    @GetMapping("/reg-opc-pio-appzs")
    public ResponseEntity<List<RegOpcPioAppz>> getAllRegOpcPioAppzs(RegOpcPioAppzCriteria criteria) {
        log.debug("REST request to get RegOpcPioAppzs by criteria: {}", criteria);
        List<RegOpcPioAppz> entityList = regOpcPioAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-pio-appzs/count} : count all the regOpcPioAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-pio-appzs/count")
    public ResponseEntity<Long> countRegOpcPioAppzs(RegOpcPioAppzCriteria criteria) {
        log.debug("REST request to count RegOpcPioAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcPioAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-pio-appzs/:id} : get the "id" regOpcPioAppz.
     *
     * @param id the id of the regOpcPioAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcPioAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-pio-appzs/{id}")
    public ResponseEntity<RegOpcPioAppz> getRegOpcPioAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcPioAppz : {}", id);
        Optional<RegOpcPioAppz> regOpcPioAppz = regOpcPioAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcPioAppz);
    }

    /**
     * {@code DELETE  /reg-opc-pio-appzs/:id} : delete the "id" regOpcPioAppz.
     *
     * @param id the id of the regOpcPioAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-pio-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcPioAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcPioAppz : {}", id);
        regOpcPioAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
