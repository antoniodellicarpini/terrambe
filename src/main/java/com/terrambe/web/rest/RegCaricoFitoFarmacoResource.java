package com.terrambe.web.rest;

import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.service.RegCaricoFitoFarmacoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegCaricoFitoFarmacoCriteria;
import com.terrambe.service.RegCaricoFitoFarmacoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegCaricoFitoFarmaco}.
 */
@RestController
@RequestMapping("/api")
public class RegCaricoFitoFarmacoResource {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFitoFarmacoResource.class);

    private static final String ENTITY_NAME = "regCaricoFitoFarmaco";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegCaricoFitoFarmacoService regCaricoFitoFarmacoService;

    private final RegCaricoFitoFarmacoQueryService regCaricoFitoFarmacoQueryService;

    public RegCaricoFitoFarmacoResource(RegCaricoFitoFarmacoService regCaricoFitoFarmacoService, RegCaricoFitoFarmacoQueryService regCaricoFitoFarmacoQueryService) {
        this.regCaricoFitoFarmacoService = regCaricoFitoFarmacoService;
        this.regCaricoFitoFarmacoQueryService = regCaricoFitoFarmacoQueryService;
    }

    /**
     * {@code POST  /reg-carico-fito-farmacos} : Create a new regCaricoFitoFarmaco.
     *
     * @param regCaricoFitoFarmaco the regCaricoFitoFarmaco to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regCaricoFitoFarmaco, or with status {@code 400 (Bad Request)} if the regCaricoFitoFarmaco has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-carico-fito-farmacos")
    public ResponseEntity<RegCaricoFitoFarmaco> createRegCaricoFitoFarmaco(@Valid @RequestBody RegCaricoFitoFarmaco regCaricoFitoFarmaco) throws URISyntaxException {
        log.debug("REST request to save RegCaricoFitoFarmaco : {}", regCaricoFitoFarmaco);
        if (regCaricoFitoFarmaco.getId() != null) {
            throw new BadRequestAlertException("A new regCaricoFitoFarmaco cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegCaricoFitoFarmaco result = regCaricoFitoFarmacoService.save(regCaricoFitoFarmaco);
        return ResponseEntity.created(new URI("/api/reg-carico-fito-farmacos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-carico-fito-farmacos} : Updates an existing regCaricoFitoFarmaco.
     *
     * @param regCaricoFitoFarmaco the regCaricoFitoFarmaco to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regCaricoFitoFarmaco,
     * or with status {@code 400 (Bad Request)} if the regCaricoFitoFarmaco is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regCaricoFitoFarmaco couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-carico-fito-farmacos")
    public ResponseEntity<RegCaricoFitoFarmaco> updateRegCaricoFitoFarmaco(@Valid @RequestBody RegCaricoFitoFarmaco regCaricoFitoFarmaco) throws URISyntaxException {
        log.debug("REST request to update RegCaricoFitoFarmaco : {}", regCaricoFitoFarmaco);
        if (regCaricoFitoFarmaco.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegCaricoFitoFarmaco result = regCaricoFitoFarmacoService.save(regCaricoFitoFarmaco);
        RegCaricoFitoFarmaco result2 = regCaricoFitoFarmacoService.save(regCaricoFitoFarmaco);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regCaricoFitoFarmaco.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-carico-fito-farmacos} : get all the regCaricoFitoFarmacos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regCaricoFitoFarmacos in body.
     */
    @GetMapping("/reg-carico-fito-farmacos")
    public ResponseEntity<List<RegCaricoFitoFarmaco>> getAllRegCaricoFitoFarmacos(RegCaricoFitoFarmacoCriteria criteria) {
        log.debug("REST request to get RegCaricoFitoFarmacos by criteria: {}", criteria);
        List<RegCaricoFitoFarmaco> entityList = regCaricoFitoFarmacoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-carico-fito-farmacos/count} : count all the regCaricoFitoFarmacos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-carico-fito-farmacos/count")
    public ResponseEntity<Long> countRegCaricoFitoFarmacos(RegCaricoFitoFarmacoCriteria criteria) {
        log.debug("REST request to count RegCaricoFitoFarmacos by criteria: {}", criteria);
        return ResponseEntity.ok().body(regCaricoFitoFarmacoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-carico-fito-farmacos/:id} : get the "id" regCaricoFitoFarmaco.
     *
     * @param id the id of the regCaricoFitoFarmaco to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regCaricoFitoFarmaco, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-carico-fito-farmacos/{id}")
    public ResponseEntity<RegCaricoFitoFarmaco> getRegCaricoFitoFarmaco(@PathVariable Long id) {
        log.debug("REST request to get RegCaricoFitoFarmaco : {}", id);
        Optional<RegCaricoFitoFarmaco> regCaricoFitoFarmaco = regCaricoFitoFarmacoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regCaricoFitoFarmaco);
    }

    /**
     * {@code DELETE  /reg-carico-fito-farmacos/:id} : delete the "id" regCaricoFitoFarmaco.
     *
     * @param id the id of the regCaricoFitoFarmaco to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-carico-fito-farmacos/{id}")
    public ResponseEntity<Void> deleteRegCaricoFitoFarmaco(@PathVariable Long id) {
        log.debug("REST request to delete RegCaricoFitoFarmaco : {}", id);
        regCaricoFitoFarmacoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
