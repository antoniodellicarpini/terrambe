package com.terrambe.web.rest;

import com.terrambe.domain.RecAziendaCli;
import com.terrambe.service.RecAziendaCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecAziendaCliCriteria;
import com.terrambe.service.RecAziendaCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecAziendaCli}.
 */
@RestController
@RequestMapping("/api")
public class RecAziendaCliResource {

    private final Logger log = LoggerFactory.getLogger(RecAziendaCliResource.class);

    private static final String ENTITY_NAME = "recAziendaCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecAziendaCliService recAziendaCliService;

    private final RecAziendaCliQueryService recAziendaCliQueryService;

    public RecAziendaCliResource(RecAziendaCliService recAziendaCliService, RecAziendaCliQueryService recAziendaCliQueryService) {
        this.recAziendaCliService = recAziendaCliService;
        this.recAziendaCliQueryService = recAziendaCliQueryService;
    }

    /**
     * {@code POST  /rec-azienda-clis} : Create a new recAziendaCli.
     *
     * @param recAziendaCli the recAziendaCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recAziendaCli, or with status {@code 400 (Bad Request)} if the recAziendaCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-azienda-clis")
    public ResponseEntity<RecAziendaCli> createRecAziendaCli(@RequestBody RecAziendaCli recAziendaCli) throws URISyntaxException {
        log.debug("REST request to save RecAziendaCli : {}", recAziendaCli);
        if (recAziendaCli.getId() != null) {
            throw new BadRequestAlertException("A new recAziendaCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecAziendaCli result = recAziendaCliService.save(recAziendaCli);
        return ResponseEntity.created(new URI("/api/rec-azienda-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-azienda-clis} : Updates an existing recAziendaCli.
     *
     * @param recAziendaCli the recAziendaCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recAziendaCli,
     * or with status {@code 400 (Bad Request)} if the recAziendaCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recAziendaCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-azienda-clis")
    public ResponseEntity<RecAziendaCli> updateRecAziendaCli(@RequestBody RecAziendaCli recAziendaCli) throws URISyntaxException {
        log.debug("REST request to update RecAziendaCli : {}", recAziendaCli);
        if (recAziendaCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecAziendaCli result = recAziendaCliService.save(recAziendaCli);
        RecAziendaCli result2 = recAziendaCliService.save(recAziendaCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recAziendaCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-azienda-clis} : get all the recAziendaClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recAziendaClis in body.
     */
    @GetMapping("/rec-azienda-clis")
    public ResponseEntity<List<RecAziendaCli>> getAllRecAziendaClis(RecAziendaCliCriteria criteria) {
        log.debug("REST request to get RecAziendaClis by criteria: {}", criteria);
        List<RecAziendaCli> entityList = recAziendaCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-azienda-clis/count} : count all the recAziendaClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-azienda-clis/count")
    public ResponseEntity<Long> countRecAziendaClis(RecAziendaCliCriteria criteria) {
        log.debug("REST request to count RecAziendaClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(recAziendaCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-azienda-clis/:id} : get the "id" recAziendaCli.
     *
     * @param id the id of the recAziendaCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recAziendaCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-azienda-clis/{id}")
    public ResponseEntity<RecAziendaCli> getRecAziendaCli(@PathVariable Long id) {
        log.debug("REST request to get RecAziendaCli : {}", id);
        Optional<RecAziendaCli> recAziendaCli = recAziendaCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recAziendaCli);
    }

    /**
     * {@code DELETE  /rec-azienda-clis/:id} : delete the "id" recAziendaCli.
     *
     * @param id the id of the recAziendaCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-azienda-clis/{id}")
    public ResponseEntity<Void> deleteRecAziendaCli(@PathVariable Long id) {
        log.debug("REST request to delete RecAziendaCli : {}", id);
        recAziendaCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
