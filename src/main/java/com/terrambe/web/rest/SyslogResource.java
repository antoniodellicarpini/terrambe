package com.terrambe.web.rest;

import com.terrambe.domain.Syslog;
import com.terrambe.service.SyslogService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.SyslogCriteria;
import com.terrambe.service.SyslogQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Syslog}.
 */
@RestController
@RequestMapping("/api")
public class SyslogResource {

    private final Logger log = LoggerFactory.getLogger(SyslogResource.class);

    private static final String ENTITY_NAME = "syslog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SyslogService syslogService;

    private final SyslogQueryService syslogQueryService;

    public SyslogResource(SyslogService syslogService, SyslogQueryService syslogQueryService) {
        this.syslogService = syslogService;
        this.syslogQueryService = syslogQueryService;
    }

    /**
     * {@code POST  /syslogs} : Create a new syslog.
     *
     * @param syslog the syslog to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new syslog, or with status {@code 400 (Bad Request)} if the syslog has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/syslogs")
    public ResponseEntity<Syslog> createSyslog(@Valid @RequestBody Syslog syslog) throws URISyntaxException {
        log.debug("REST request to save Syslog : {}", syslog);
        if (syslog.getId() != null) {
            throw new BadRequestAlertException("A new syslog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Syslog result = syslogService.save(syslog);
        return ResponseEntity.created(new URI("/api/syslogs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /syslogs} : Updates an existing syslog.
     *
     * @param syslog the syslog to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated syslog,
     * or with status {@code 400 (Bad Request)} if the syslog is not valid,
     * or with status {@code 500 (Internal Server Error)} if the syslog couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/syslogs")
    public ResponseEntity<Syslog> updateSyslog(@Valid @RequestBody Syslog syslog) throws URISyntaxException {
        log.debug("REST request to update Syslog : {}", syslog);
        if (syslog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Syslog result = syslogService.save(syslog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, syslog.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /syslogs} : get all the syslogs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of syslogs in body.
     */
    @GetMapping("/syslogs")
    public ResponseEntity<List<Syslog>> getAllSyslogs(SyslogCriteria criteria) {
        log.debug("REST request to get Syslogs by criteria: {}", criteria);
        List<Syslog> entityList = syslogQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /syslogs/count} : count all the syslogs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/syslogs/count")
    public ResponseEntity<Long> countSyslogs(SyslogCriteria criteria) {
        log.debug("REST request to count Syslogs by criteria: {}", criteria);
        return ResponseEntity.ok().body(syslogQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /syslogs/:id} : get the "id" syslog.
     *
     * @param id the id of the syslog to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the syslog, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/syslogs/{id}")
    public ResponseEntity<Syslog> getSyslog(@PathVariable Long id) {
        log.debug("REST request to get Syslog : {}", id);
        Optional<Syslog> syslog = syslogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(syslog);
    }

    /**
     * {@code DELETE  /syslogs/:id} : delete the "id" syslog.
     *
     * @param id the id of the syslog to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/syslogs/{id}")
    public ResponseEntity<Void> deleteSyslog(@PathVariable Long id) {
        log.debug("REST request to delete Syslog : {}", id);
        syslogService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
