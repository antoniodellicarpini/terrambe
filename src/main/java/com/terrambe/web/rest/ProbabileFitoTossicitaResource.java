package com.terrambe.web.rest;

import com.terrambe.domain.ProbabileFitoTossicita;
import com.terrambe.service.ProbabileFitoTossicitaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.ProbabileFitoTossicitaCriteria;
import com.terrambe.service.ProbabileFitoTossicitaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.ProbabileFitoTossicita}.
 */
@RestController
@RequestMapping("/api")
public class ProbabileFitoTossicitaResource {

    private final Logger log = LoggerFactory.getLogger(ProbabileFitoTossicitaResource.class);

    private static final String ENTITY_NAME = "probabileFitoTossicita";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProbabileFitoTossicitaService probabileFitoTossicitaService;

    private final ProbabileFitoTossicitaQueryService probabileFitoTossicitaQueryService;

    public ProbabileFitoTossicitaResource(ProbabileFitoTossicitaService probabileFitoTossicitaService, ProbabileFitoTossicitaQueryService probabileFitoTossicitaQueryService) {
        this.probabileFitoTossicitaService = probabileFitoTossicitaService;
        this.probabileFitoTossicitaQueryService = probabileFitoTossicitaQueryService;
    }

    /**
     * {@code POST  /probabile-fito-tossicitas} : Create a new probabileFitoTossicita.
     *
     * @param probabileFitoTossicita the probabileFitoTossicita to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new probabileFitoTossicita, or with status {@code 400 (Bad Request)} if the probabileFitoTossicita has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/probabile-fito-tossicitas")
    public ResponseEntity<ProbabileFitoTossicita> createProbabileFitoTossicita(@RequestBody ProbabileFitoTossicita probabileFitoTossicita) throws URISyntaxException {
        log.debug("REST request to save ProbabileFitoTossicita : {}", probabileFitoTossicita);
        if (probabileFitoTossicita.getId() != null) {
            throw new BadRequestAlertException("A new probabileFitoTossicita cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProbabileFitoTossicita result = probabileFitoTossicitaService.save(probabileFitoTossicita);
        return ResponseEntity.created(new URI("/api/probabile-fito-tossicitas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /probabile-fito-tossicitas} : Updates an existing probabileFitoTossicita.
     *
     * @param probabileFitoTossicita the probabileFitoTossicita to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated probabileFitoTossicita,
     * or with status {@code 400 (Bad Request)} if the probabileFitoTossicita is not valid,
     * or with status {@code 500 (Internal Server Error)} if the probabileFitoTossicita couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/probabile-fito-tossicitas")
    public ResponseEntity<ProbabileFitoTossicita> updateProbabileFitoTossicita(@RequestBody ProbabileFitoTossicita probabileFitoTossicita) throws URISyntaxException {
        log.debug("REST request to update ProbabileFitoTossicita : {}", probabileFitoTossicita);
        if (probabileFitoTossicita.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProbabileFitoTossicita result = probabileFitoTossicitaService.save(probabileFitoTossicita);
        ProbabileFitoTossicita result2 = probabileFitoTossicitaService.save(probabileFitoTossicita);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, probabileFitoTossicita.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /probabile-fito-tossicitas} : get all the probabileFitoTossicitas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of probabileFitoTossicitas in body.
     */
    @GetMapping("/probabile-fito-tossicitas")
    public ResponseEntity<List<ProbabileFitoTossicita>> getAllProbabileFitoTossicitas(ProbabileFitoTossicitaCriteria criteria) {
        log.debug("REST request to get ProbabileFitoTossicitas by criteria: {}", criteria);
        List<ProbabileFitoTossicita> entityList = probabileFitoTossicitaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /probabile-fito-tossicitas/count} : count all the probabileFitoTossicitas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/probabile-fito-tossicitas/count")
    public ResponseEntity<Long> countProbabileFitoTossicitas(ProbabileFitoTossicitaCriteria criteria) {
        log.debug("REST request to count ProbabileFitoTossicitas by criteria: {}", criteria);
        return ResponseEntity.ok().body(probabileFitoTossicitaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /probabile-fito-tossicitas/:id} : get the "id" probabileFitoTossicita.
     *
     * @param id the id of the probabileFitoTossicita to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the probabileFitoTossicita, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/probabile-fito-tossicitas/{id}")
    public ResponseEntity<ProbabileFitoTossicita> getProbabileFitoTossicita(@PathVariable Long id) {
        log.debug("REST request to get ProbabileFitoTossicita : {}", id);
        Optional<ProbabileFitoTossicita> probabileFitoTossicita = probabileFitoTossicitaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(probabileFitoTossicita);
    }

    /**
     * {@code DELETE  /probabile-fito-tossicitas/:id} : delete the "id" probabileFitoTossicita.
     *
     * @param id the id of the probabileFitoTossicita to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/probabile-fito-tossicitas/{id}")
    public ResponseEntity<Void> deleteProbabileFitoTossicita(@PathVariable Long id) {
        log.debug("REST request to delete ProbabileFitoTossicita : {}", id);
        probabileFitoTossicitaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
