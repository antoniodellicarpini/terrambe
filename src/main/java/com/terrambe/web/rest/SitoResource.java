package com.terrambe.web.rest;

import com.terrambe.domain.Sito;
import com.terrambe.service.SitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.SitoCriteria;
import com.terrambe.service.SitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Sito}.
 */
@RestController
@RequestMapping("/api")
public class SitoResource {

    private final Logger log = LoggerFactory.getLogger(SitoResource.class);

    private static final String ENTITY_NAME = "sito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SitoService sitoService;

    private final SitoQueryService sitoQueryService;

    public SitoResource(SitoService sitoService, SitoQueryService sitoQueryService) {
        this.sitoService = sitoService;
        this.sitoQueryService = sitoQueryService;
    }

    /**
     * {@code POST  /sitos} : Create a new sito.
     *
     * @param sito the sito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sito, or with status {@code 400 (Bad Request)} if the sito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sitos")
    public ResponseEntity<Sito> createSito(@RequestBody Sito sito) throws URISyntaxException {
        log.debug("REST request to save Sito : {}", sito);
        if (sito.getId() != null) {
            throw new BadRequestAlertException("A new sito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sito result = sitoService.save(sito);
        return ResponseEntity.created(new URI("/api/sitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sitos} : Updates an existing sito.
     *
     * @param sito the sito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sito,
     * or with status {@code 400 (Bad Request)} if the sito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sitos")
    public ResponseEntity<Sito> updateSito(@RequestBody Sito sito) throws URISyntaxException {
        log.debug("REST request to update Sito : {}", sito);
        if (sito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Sito result = sitoService.save(sito);
        Sito result2 = sitoService.save(sito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sitos} : get all the sitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sitos in body.
     */
    @GetMapping("/sitos")
    public ResponseEntity<List<Sito>> getAllSitos(SitoCriteria criteria) {
        log.debug("REST request to get Sitos by criteria: {}", criteria);
        List<Sito> entityList = sitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /sitos/count} : count all the sitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/sitos/count")
    public ResponseEntity<Long> countSitos(SitoCriteria criteria) {
        log.debug("REST request to count Sitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(sitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /sitos/:id} : get the "id" sito.
     *
     * @param id the id of the sito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sitos/{id}")
    public ResponseEntity<Sito> getSito(@PathVariable Long id) {
        log.debug("REST request to get Sito : {}", id);
        Optional<Sito> sito = sitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sito);
    }

    /**
     * {@code DELETE  /sitos/:id} : delete the "id" sito.
     *
     * @param id the id of the sito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sitos/{id}")
    public ResponseEntity<Void> deleteSito(@PathVariable Long id) {
        log.debug("REST request to delete Sito : {}", id);
        sitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
