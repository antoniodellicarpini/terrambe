package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcAltroAppz;
import com.terrambe.service.RegOpcAltroAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcAltroAppzCriteria;
import com.terrambe.service.RegOpcAltroAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcAltroAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcAltroAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcAltroAppzResource.class);

    private static final String ENTITY_NAME = "regOpcAltroAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcAltroAppzService regOpcAltroAppzService;

    private final RegOpcAltroAppzQueryService regOpcAltroAppzQueryService;

    public RegOpcAltroAppzResource(RegOpcAltroAppzService regOpcAltroAppzService, RegOpcAltroAppzQueryService regOpcAltroAppzQueryService) {
        this.regOpcAltroAppzService = regOpcAltroAppzService;
        this.regOpcAltroAppzQueryService = regOpcAltroAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-altro-appzs} : Create a new regOpcAltroAppz.
     *
     * @param regOpcAltroAppz the regOpcAltroAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcAltroAppz, or with status {@code 400 (Bad Request)} if the regOpcAltroAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-altro-appzs")
    public ResponseEntity<RegOpcAltroAppz> createRegOpcAltroAppz(@Valid @RequestBody RegOpcAltroAppz regOpcAltroAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcAltroAppz : {}", regOpcAltroAppz);
        if (regOpcAltroAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcAltroAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcAltroAppz result = regOpcAltroAppzService.save(regOpcAltroAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-altro-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-altro-appzs} : Updates an existing regOpcAltroAppz.
     *
     * @param regOpcAltroAppz the regOpcAltroAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcAltroAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcAltroAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcAltroAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-altro-appzs")
    public ResponseEntity<RegOpcAltroAppz> updateRegOpcAltroAppz(@Valid @RequestBody RegOpcAltroAppz regOpcAltroAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcAltroAppz : {}", regOpcAltroAppz);
        if (regOpcAltroAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcAltroAppz result = regOpcAltroAppzService.save(regOpcAltroAppz);
        RegOpcAltroAppz result2 = regOpcAltroAppzService.save(regOpcAltroAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcAltroAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-altro-appzs} : get all the regOpcAltroAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcAltroAppzs in body.
     */
    @GetMapping("/reg-opc-altro-appzs")
    public ResponseEntity<List<RegOpcAltroAppz>> getAllRegOpcAltroAppzs(RegOpcAltroAppzCriteria criteria) {
        log.debug("REST request to get RegOpcAltroAppzs by criteria: {}", criteria);
        List<RegOpcAltroAppz> entityList = regOpcAltroAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-altro-appzs/count} : count all the regOpcAltroAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-altro-appzs/count")
    public ResponseEntity<Long> countRegOpcAltroAppzs(RegOpcAltroAppzCriteria criteria) {
        log.debug("REST request to count RegOpcAltroAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcAltroAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-altro-appzs/:id} : get the "id" regOpcAltroAppz.
     *
     * @param id the id of the regOpcAltroAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcAltroAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-altro-appzs/{id}")
    public ResponseEntity<RegOpcAltroAppz> getRegOpcAltroAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcAltroAppz : {}", id);
        Optional<RegOpcAltroAppz> regOpcAltroAppz = regOpcAltroAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcAltroAppz);
    }

    /**
     * {@code DELETE  /reg-opc-altro-appzs/:id} : delete the "id" regOpcAltroAppz.
     *
     * @param id the id of the regOpcAltroAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-altro-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcAltroAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcAltroAppz : {}", id);
        regOpcAltroAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
