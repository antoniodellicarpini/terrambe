package com.terrambe.web.rest;

import com.terrambe.domain.Comuni;
import com.terrambe.service.ComuniService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.ComuniCriteria;
import com.terrambe.service.ComuniQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Comuni}.
 */
@RestController
@RequestMapping("/api")
public class ComuniResource {

    private final Logger log = LoggerFactory.getLogger(ComuniResource.class);

    private static final String ENTITY_NAME = "comuni";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ComuniService comuniService;

    private final ComuniQueryService comuniQueryService;

    public ComuniResource(ComuniService comuniService, ComuniQueryService comuniQueryService) {
        this.comuniService = comuniService;
        this.comuniQueryService = comuniQueryService;
    }

    /**
     * {@code POST  /comunis} : Create a new comuni.
     *
     * @param comuni the comuni to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new comuni, or with status {@code 400 (Bad Request)} if the comuni has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/comunis")
    public ResponseEntity<Comuni> createComuni(@RequestBody Comuni comuni) throws URISyntaxException {
        log.debug("REST request to save Comuni : {}", comuni);
        if (comuni.getId() != null) {
            throw new BadRequestAlertException("A new comuni cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Comuni result = comuniService.save(comuni);
        return ResponseEntity.created(new URI("/api/comunis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /comunis} : Updates an existing comuni.
     *
     * @param comuni the comuni to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated comuni,
     * or with status {@code 400 (Bad Request)} if the comuni is not valid,
     * or with status {@code 500 (Internal Server Error)} if the comuni couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/comunis")
    public ResponseEntity<Comuni> updateComuni(@RequestBody Comuni comuni) throws URISyntaxException {
        log.debug("REST request to update Comuni : {}", comuni);
        if (comuni.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Comuni result = comuniService.save(comuni);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, comuni.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /comunis} : get all the comunis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of comunis in body.
     */
    @GetMapping("/comunis")
    public ResponseEntity<List<Comuni>> getAllComunis(ComuniCriteria criteria) {
        log.debug("REST request to get Comunis by criteria: {}", criteria);
        List<Comuni> entityList = comuniQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /comunis/count} : count all the comunis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/comunis/count")
    public ResponseEntity<Long> countComunis(ComuniCriteria criteria) {
        log.debug("REST request to count Comunis by criteria: {}", criteria);
        return ResponseEntity.ok().body(comuniQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /comunis/:id} : get the "id" comuni.
     *
     * @param id the id of the comuni to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the comuni, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/comunis/{id}")
    public ResponseEntity<Comuni> getComuni(@PathVariable Long id) {
        log.debug("REST request to get Comuni : {}", id);
        Optional<Comuni> comuni = comuniService.findOne(id);
        return ResponseUtil.wrapOrNotFound(comuni);
    }

    /**
     * {@code DELETE  /comunis/:id} : delete the "id" comuni.
     *
     * @param id the id of the comuni to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/comunis/{id}")
    public ResponseEntity<Void> deleteComuni(@PathVariable Long id) {
        log.debug("REST request to delete Comuni : {}", id);
        comuniService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
