package com.terrambe.web.rest;

import com.terrambe.domain.Trappole;
import com.terrambe.service.TrappoleService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TrappoleCriteria;
import com.terrambe.service.TrappoleQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Trappole}.
 */
@RestController
@RequestMapping("/api")
public class TrappoleResource {

    private final Logger log = LoggerFactory.getLogger(TrappoleResource.class);

    private static final String ENTITY_NAME = "trappole";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TrappoleService trappoleService;

    private final TrappoleQueryService trappoleQueryService;

    public TrappoleResource(TrappoleService trappoleService, TrappoleQueryService trappoleQueryService) {
        this.trappoleService = trappoleService;
        this.trappoleQueryService = trappoleQueryService;
    }

    /**
     * {@code POST  /trappoles} : Create a new trappole.
     *
     * @param trappole the trappole to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new trappole, or with status {@code 400 (Bad Request)} if the trappole has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/trappoles")
    public ResponseEntity<Trappole> createTrappole(@Valid @RequestBody Trappole trappole) throws URISyntaxException {
        log.debug("REST request to save Trappole : {}", trappole);
        if (trappole.getId() != null) {
            throw new BadRequestAlertException("A new trappole cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Trappole result = trappoleService.save(trappole);
        return ResponseEntity.created(new URI("/api/trappoles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /trappoles} : Updates an existing trappole.
     *
     * @param trappole the trappole to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated trappole,
     * or with status {@code 400 (Bad Request)} if the trappole is not valid,
     * or with status {@code 500 (Internal Server Error)} if the trappole couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/trappoles")
    public ResponseEntity<Trappole> updateTrappole(@Valid @RequestBody Trappole trappole) throws URISyntaxException {
        log.debug("REST request to update Trappole : {}", trappole);
        if (trappole.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Trappole result = trappoleService.save(trappole);
        Trappole result2 = trappoleService.save(trappole);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, trappole.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /trappoles} : get all the trappoles.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trappoles in body.
     */
    @GetMapping("/trappoles")
    public ResponseEntity<List<Trappole>> getAllTrappoles(TrappoleCriteria criteria) {
        log.debug("REST request to get Trappoles by criteria: {}", criteria);
        List<Trappole> entityList = trappoleQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /trappoles/count} : count all the trappoles.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/trappoles/count")
    public ResponseEntity<Long> countTrappoles(TrappoleCriteria criteria) {
        log.debug("REST request to count Trappoles by criteria: {}", criteria);
        return ResponseEntity.ok().body(trappoleQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /trappoles/:id} : get the "id" trappole.
     *
     * @param id the id of the trappole to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trappole, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trappoles/{id}")
    public ResponseEntity<Trappole> getTrappole(@PathVariable Long id) {
        log.debug("REST request to get Trappole : {}", id);
        Optional<Trappole> trappole = trappoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trappole);
    }

    /**
     * {@code DELETE  /trappoles/:id} : delete the "id" trappole.
     *
     * @param id the id of the trappole to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/trappoles/{id}")
    public ResponseEntity<Void> deleteTrappole(@PathVariable Long id) {
        log.debug("REST request to delete Trappole : {}", id);
        trappoleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
