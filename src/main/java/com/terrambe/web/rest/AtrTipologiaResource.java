package com.terrambe.web.rest;

import com.terrambe.domain.AtrTipologia;
import com.terrambe.service.AtrTipologiaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AtrTipologiaCriteria;
import com.terrambe.service.AtrTipologiaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AtrTipologia}.
 */
@RestController
@RequestMapping("/api")
public class AtrTipologiaResource {

    private final Logger log = LoggerFactory.getLogger(AtrTipologiaResource.class);

    private static final String ENTITY_NAME = "atrTipologia";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtrTipologiaService atrTipologiaService;

    private final AtrTipologiaQueryService atrTipologiaQueryService;

    public AtrTipologiaResource(AtrTipologiaService atrTipologiaService, AtrTipologiaQueryService atrTipologiaQueryService) {
        this.atrTipologiaService = atrTipologiaService;
        this.atrTipologiaQueryService = atrTipologiaQueryService;
    }

    /**
     * {@code POST  /atr-tipologias} : Create a new atrTipologia.
     *
     * @param atrTipologia the atrTipologia to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atrTipologia, or with status {@code 400 (Bad Request)} if the atrTipologia has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/atr-tipologias")
    public ResponseEntity<AtrTipologia> createAtrTipologia(@Valid @RequestBody AtrTipologia atrTipologia) throws URISyntaxException {
        log.debug("REST request to save AtrTipologia : {}", atrTipologia);
        if (atrTipologia.getId() != null) {
            throw new BadRequestAlertException("A new atrTipologia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtrTipologia result = atrTipologiaService.save(atrTipologia);
        return ResponseEntity.created(new URI("/api/atr-tipologias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /atr-tipologias} : Updates an existing atrTipologia.
     *
     * @param atrTipologia the atrTipologia to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atrTipologia,
     * or with status {@code 400 (Bad Request)} if the atrTipologia is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atrTipologia couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/atr-tipologias")
    public ResponseEntity<AtrTipologia> updateAtrTipologia(@Valid @RequestBody AtrTipologia atrTipologia) throws URISyntaxException {
        log.debug("REST request to update AtrTipologia : {}", atrTipologia);
        if (atrTipologia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtrTipologia result = atrTipologiaService.save(atrTipologia);
        AtrTipologia result2 = atrTipologiaService.save(atrTipologia);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, atrTipologia.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /atr-tipologias} : get all the atrTipologias.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atrTipologias in body.
     */
    @GetMapping("/atr-tipologias")
    public ResponseEntity<List<AtrTipologia>> getAllAtrTipologias(AtrTipologiaCriteria criteria) {
        log.debug("REST request to get AtrTipologias by criteria: {}", criteria);
        List<AtrTipologia> entityList = atrTipologiaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /atr-tipologias/count} : count all the atrTipologias.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/atr-tipologias/count")
    public ResponseEntity<Long> countAtrTipologias(AtrTipologiaCriteria criteria) {
        log.debug("REST request to count AtrTipologias by criteria: {}", criteria);
        return ResponseEntity.ok().body(atrTipologiaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /atr-tipologias/:id} : get the "id" atrTipologia.
     *
     * @param id the id of the atrTipologia to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atrTipologia, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/atr-tipologias/{id}")
    public ResponseEntity<AtrTipologia> getAtrTipologia(@PathVariable Long id) {
        log.debug("REST request to get AtrTipologia : {}", id);
        Optional<AtrTipologia> atrTipologia = atrTipologiaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atrTipologia);
    }

    /**
     * {@code DELETE  /atr-tipologias/:id} : delete the "id" atrTipologia.
     *
     * @param id the id of the atrTipologia to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/atr-tipologias/{id}")
    public ResponseEntity<Void> deleteAtrTipologia(@PathVariable Long id) {
        log.debug("REST request to delete AtrTipologia : {}", id);
        atrTipologiaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
