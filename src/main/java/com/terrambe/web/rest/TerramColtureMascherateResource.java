package com.terrambe.web.rest;

import com.terrambe.domain.TerramColtureMascherate;
import com.terrambe.service.TerramColtureMascherateService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TerramColtureMascherateCriteria;
import com.terrambe.service.TerramColtureMascherateQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TerramColtureMascherate}.
 */
@RestController
@RequestMapping("/api")
public class TerramColtureMascherateResource {

    private final Logger log = LoggerFactory.getLogger(TerramColtureMascherateResource.class);

    private static final String ENTITY_NAME = "terramColtureMascherate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TerramColtureMascherateService terramColtureMascherateService;

    private final TerramColtureMascherateQueryService terramColtureMascherateQueryService;

    public TerramColtureMascherateResource(TerramColtureMascherateService terramColtureMascherateService, TerramColtureMascherateQueryService terramColtureMascherateQueryService) {
        this.terramColtureMascherateService = terramColtureMascherateService;
        this.terramColtureMascherateQueryService = terramColtureMascherateQueryService;
    }

    /**
     * {@code POST  /terram-colture-mascherates} : Create a new terramColtureMascherate.
     *
     * @param terramColtureMascherate the terramColtureMascherate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new terramColtureMascherate, or with status {@code 400 (Bad Request)} if the terramColtureMascherate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/terram-colture-mascherates")
    public ResponseEntity<TerramColtureMascherate> createTerramColtureMascherate(@RequestBody TerramColtureMascherate terramColtureMascherate) throws URISyntaxException {
        log.debug("REST request to save TerramColtureMascherate : {}", terramColtureMascherate);
        if (terramColtureMascherate.getId() != null) {
            throw new BadRequestAlertException("A new terramColtureMascherate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TerramColtureMascherate result = terramColtureMascherateService.save(terramColtureMascherate);
        return ResponseEntity.created(new URI("/api/terram-colture-mascherates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /terram-colture-mascherates} : Updates an existing terramColtureMascherate.
     *
     * @param terramColtureMascherate the terramColtureMascherate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated terramColtureMascherate,
     * or with status {@code 400 (Bad Request)} if the terramColtureMascherate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the terramColtureMascherate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/terram-colture-mascherates")
    public ResponseEntity<TerramColtureMascherate> updateTerramColtureMascherate(@RequestBody TerramColtureMascherate terramColtureMascherate) throws URISyntaxException {
        log.debug("REST request to update TerramColtureMascherate : {}", terramColtureMascherate);
        if (terramColtureMascherate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TerramColtureMascherate result = terramColtureMascherateService.save(terramColtureMascherate);
        TerramColtureMascherate result2 = terramColtureMascherateService.save(terramColtureMascherate);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, terramColtureMascherate.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /terram-colture-mascherates} : get all the terramColtureMascherates.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of terramColtureMascherates in body.
     */
    @GetMapping("/terram-colture-mascherates")
    public ResponseEntity<List<TerramColtureMascherate>> getAllTerramColtureMascherates(TerramColtureMascherateCriteria criteria) {
        log.debug("REST request to get TerramColtureMascherates by criteria: {}", criteria);
        List<TerramColtureMascherate> entityList = terramColtureMascherateQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /terram-colture-mascherates/count} : count all the terramColtureMascherates.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/terram-colture-mascherates/count")
    public ResponseEntity<Long> countTerramColtureMascherates(TerramColtureMascherateCriteria criteria) {
        log.debug("REST request to count TerramColtureMascherates by criteria: {}", criteria);
        return ResponseEntity.ok().body(terramColtureMascherateQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /terram-colture-mascherates/:id} : get the "id" terramColtureMascherate.
     *
     * @param id the id of the terramColtureMascherate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the terramColtureMascherate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/terram-colture-mascherates/{id}")
    public ResponseEntity<TerramColtureMascherate> getTerramColtureMascherate(@PathVariable Long id) {
        log.debug("REST request to get TerramColtureMascherate : {}", id);
        Optional<TerramColtureMascherate> terramColtureMascherate = terramColtureMascherateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(terramColtureMascherate);
    }

    /**
     * {@code DELETE  /terram-colture-mascherates/:id} : delete the "id" terramColtureMascherate.
     *
     * @param id the id of the terramColtureMascherate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/terram-colture-mascherates/{id}")
    public ResponseEntity<Void> deleteTerramColtureMascherate(@PathVariable Long id) {
        log.debug("REST request to delete TerramColtureMascherate : {}", id);
        terramColtureMascherateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
