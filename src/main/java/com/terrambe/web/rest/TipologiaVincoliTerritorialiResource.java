package com.terrambe.web.rest;

import com.terrambe.domain.TipologiaVincoliTerritoriali;
import com.terrambe.service.TipologiaVincoliTerritorialiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipologiaVincoliTerritorialiCriteria;
import com.terrambe.service.TipologiaVincoliTerritorialiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipologiaVincoliTerritoriali}.
 */
@RestController
@RequestMapping("/api")
public class TipologiaVincoliTerritorialiResource {

    private final Logger log = LoggerFactory.getLogger(TipologiaVincoliTerritorialiResource.class);

    private static final String ENTITY_NAME = "tipologiaVincoliTerritoriali";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipologiaVincoliTerritorialiService tipologiaVincoliTerritorialiService;

    private final TipologiaVincoliTerritorialiQueryService tipologiaVincoliTerritorialiQueryService;

    public TipologiaVincoliTerritorialiResource(TipologiaVincoliTerritorialiService tipologiaVincoliTerritorialiService, TipologiaVincoliTerritorialiQueryService tipologiaVincoliTerritorialiQueryService) {
        this.tipologiaVincoliTerritorialiService = tipologiaVincoliTerritorialiService;
        this.tipologiaVincoliTerritorialiQueryService = tipologiaVincoliTerritorialiQueryService;
    }

    /**
     * {@code POST  /tipologia-vincoli-territorialis} : Create a new tipologiaVincoliTerritoriali.
     *
     * @param tipologiaVincoliTerritoriali the tipologiaVincoliTerritoriali to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipologiaVincoliTerritoriali, or with status {@code 400 (Bad Request)} if the tipologiaVincoliTerritoriali has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipologia-vincoli-territorialis")
    public ResponseEntity<TipologiaVincoliTerritoriali> createTipologiaVincoliTerritoriali(@Valid @RequestBody TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali) throws URISyntaxException {
        log.debug("REST request to save TipologiaVincoliTerritoriali : {}", tipologiaVincoliTerritoriali);
        if (tipologiaVincoliTerritoriali.getId() != null) {
            throw new BadRequestAlertException("A new tipologiaVincoliTerritoriali cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipologiaVincoliTerritoriali result = tipologiaVincoliTerritorialiService.save(tipologiaVincoliTerritoriali);
        return ResponseEntity.created(new URI("/api/tipologia-vincoli-territorialis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipologia-vincoli-territorialis} : Updates an existing tipologiaVincoliTerritoriali.
     *
     * @param tipologiaVincoliTerritoriali the tipologiaVincoliTerritoriali to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipologiaVincoliTerritoriali,
     * or with status {@code 400 (Bad Request)} if the tipologiaVincoliTerritoriali is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipologiaVincoliTerritoriali couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipologia-vincoli-territorialis")
    public ResponseEntity<TipologiaVincoliTerritoriali> updateTipologiaVincoliTerritoriali(@Valid @RequestBody TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali) throws URISyntaxException {
        log.debug("REST request to update TipologiaVincoliTerritoriali : {}", tipologiaVincoliTerritoriali);
        if (tipologiaVincoliTerritoriali.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipologiaVincoliTerritoriali result = tipologiaVincoliTerritorialiService.save(tipologiaVincoliTerritoriali);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipologiaVincoliTerritoriali.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipologia-vincoli-territorialis} : get all the tipologiaVincoliTerritorialis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipologiaVincoliTerritorialis in body.
     */
    @GetMapping("/tipologia-vincoli-territorialis")
    public ResponseEntity<List<TipologiaVincoliTerritoriali>> getAllTipologiaVincoliTerritorialis(TipologiaVincoliTerritorialiCriteria criteria) {
        log.debug("REST request to get TipologiaVincoliTerritorialis by criteria: {}", criteria);
        List<TipologiaVincoliTerritoriali> entityList = tipologiaVincoliTerritorialiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipologia-vincoli-territorialis/count} : count all the tipologiaVincoliTerritorialis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipologia-vincoli-territorialis/count")
    public ResponseEntity<Long> countTipologiaVincoliTerritorialis(TipologiaVincoliTerritorialiCriteria criteria) {
        log.debug("REST request to count TipologiaVincoliTerritorialis by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipologiaVincoliTerritorialiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipologia-vincoli-territorialis/:id} : get the "id" tipologiaVincoliTerritoriali.
     *
     * @param id the id of the tipologiaVincoliTerritoriali to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipologiaVincoliTerritoriali, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipologia-vincoli-territorialis/{id}")
    public ResponseEntity<TipologiaVincoliTerritoriali> getTipologiaVincoliTerritoriali(@PathVariable Long id) {
        log.debug("REST request to get TipologiaVincoliTerritoriali : {}", id);
        Optional<TipologiaVincoliTerritoriali> tipologiaVincoliTerritoriali = tipologiaVincoliTerritorialiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipologiaVincoliTerritoriali);
    }

    /**
     * {@code DELETE  /tipologia-vincoli-territorialis/:id} : delete the "id" tipologiaVincoliTerritoriali.
     *
     * @param id the id of the tipologiaVincoliTerritoriali to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipologia-vincoli-territorialis/{id}")
    public ResponseEntity<Void> deleteTipologiaVincoliTerritoriali(@PathVariable Long id) {
        log.debug("REST request to delete TipologiaVincoliTerritoriali : {}", id);
        tipologiaVincoliTerritorialiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
