package com.terrambe.web.rest;

import com.terrambe.domain.RecMagUbi;
import com.terrambe.service.RecMagUbiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecMagUbiCriteria;
import com.terrambe.service.RecMagUbiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecMagUbi}.
 */
@RestController
@RequestMapping("/api")
public class RecMagUbiResource {

    private final Logger log = LoggerFactory.getLogger(RecMagUbiResource.class);

    private static final String ENTITY_NAME = "recMagUbi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecMagUbiService recMagUbiService;

    private final RecMagUbiQueryService recMagUbiQueryService;

    public RecMagUbiResource(RecMagUbiService recMagUbiService, RecMagUbiQueryService recMagUbiQueryService) {
        this.recMagUbiService = recMagUbiService;
        this.recMagUbiQueryService = recMagUbiQueryService;
    }

    /**
     * {@code POST  /rec-mag-ubis} : Create a new recMagUbi.
     *
     * @param recMagUbi the recMagUbi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recMagUbi, or with status {@code 400 (Bad Request)} if the recMagUbi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-mag-ubis")
    public ResponseEntity<RecMagUbi> createRecMagUbi(@RequestBody RecMagUbi recMagUbi) throws URISyntaxException {
        log.debug("REST request to save RecMagUbi : {}", recMagUbi);
        if (recMagUbi.getId() != null) {
            throw new BadRequestAlertException("A new recMagUbi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecMagUbi result = recMagUbiService.save(recMagUbi);
        return ResponseEntity.created(new URI("/api/rec-mag-ubis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-mag-ubis} : Updates an existing recMagUbi.
     *
     * @param recMagUbi the recMagUbi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recMagUbi,
     * or with status {@code 400 (Bad Request)} if the recMagUbi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recMagUbi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-mag-ubis")
    public ResponseEntity<RecMagUbi> updateRecMagUbi(@RequestBody RecMagUbi recMagUbi) throws URISyntaxException {
        log.debug("REST request to update RecMagUbi : {}", recMagUbi);
        if (recMagUbi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecMagUbi result = recMagUbiService.save(recMagUbi);
        RecMagUbi result2 = recMagUbiService.save(recMagUbi);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recMagUbi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-mag-ubis} : get all the recMagUbis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recMagUbis in body.
     */
    @GetMapping("/rec-mag-ubis")
    public ResponseEntity<List<RecMagUbi>> getAllRecMagUbis(RecMagUbiCriteria criteria) {
        log.debug("REST request to get RecMagUbis by criteria: {}", criteria);
        List<RecMagUbi> entityList = recMagUbiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-mag-ubis/count} : count all the recMagUbis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-mag-ubis/count")
    public ResponseEntity<Long> countRecMagUbis(RecMagUbiCriteria criteria) {
        log.debug("REST request to count RecMagUbis by criteria: {}", criteria);
        return ResponseEntity.ok().body(recMagUbiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-mag-ubis/:id} : get the "id" recMagUbi.
     *
     * @param id the id of the recMagUbi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recMagUbi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-mag-ubis/{id}")
    public ResponseEntity<RecMagUbi> getRecMagUbi(@PathVariable Long id) {
        log.debug("REST request to get RecMagUbi : {}", id);
        Optional<RecMagUbi> recMagUbi = recMagUbiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recMagUbi);
    }

    /**
     * {@code DELETE  /rec-mag-ubis/:id} : delete the "id" recMagUbi.
     *
     * @param id the id of the recMagUbi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-mag-ubis/{id}")
    public ResponseEntity<Void> deleteRecMagUbi(@PathVariable Long id) {
        log.debug("REST request to delete RecMagUbi : {}", id);
        recMagUbiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
