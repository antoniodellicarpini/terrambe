package com.terrambe.web.rest;

import com.terrambe.domain.IndOpeAna;
import com.terrambe.service.IndOpeAnaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndOpeAnaCriteria;
import com.terrambe.service.IndOpeAnaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndOpeAna}.
 */
@RestController
@RequestMapping("/api")
public class IndOpeAnaResource {

    private final Logger log = LoggerFactory.getLogger(IndOpeAnaResource.class);

    private static final String ENTITY_NAME = "indOpeAna";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndOpeAnaService indOpeAnaService;

    private final IndOpeAnaQueryService indOpeAnaQueryService;

    public IndOpeAnaResource(IndOpeAnaService indOpeAnaService, IndOpeAnaQueryService indOpeAnaQueryService) {
        this.indOpeAnaService = indOpeAnaService;
        this.indOpeAnaQueryService = indOpeAnaQueryService;
    }

    /**
     * {@code POST  /ind-ope-anas} : Create a new indOpeAna.
     *
     * @param indOpeAna the indOpeAna to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indOpeAna, or with status {@code 400 (Bad Request)} if the indOpeAna has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-ope-anas")
    public ResponseEntity<IndOpeAna> createIndOpeAna(@Valid @RequestBody IndOpeAna indOpeAna) throws URISyntaxException {
        log.debug("REST request to save IndOpeAna : {}", indOpeAna);
        if (indOpeAna.getId() != null) {
            throw new BadRequestAlertException("A new indOpeAna cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndOpeAna result = indOpeAnaService.save(indOpeAna);
        return ResponseEntity.created(new URI("/api/ind-ope-anas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-ope-anas} : Updates an existing indOpeAna.
     *
     * @param indOpeAna the indOpeAna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indOpeAna,
     * or with status {@code 400 (Bad Request)} if the indOpeAna is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indOpeAna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-ope-anas")
    public ResponseEntity<IndOpeAna> updateIndOpeAna(@Valid @RequestBody IndOpeAna indOpeAna) throws URISyntaxException {
        log.debug("REST request to update IndOpeAna : {}", indOpeAna);
        if (indOpeAna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndOpeAna result = indOpeAnaService.save(indOpeAna);
        IndOpeAna result2 = indOpeAnaService.save(indOpeAna);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indOpeAna.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-ope-anas} : get all the indOpeAnas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indOpeAnas in body.
     */
    @GetMapping("/ind-ope-anas")
    public ResponseEntity<List<IndOpeAna>> getAllIndOpeAnas(IndOpeAnaCriteria criteria) {
        log.debug("REST request to get IndOpeAnas by criteria: {}", criteria);
        List<IndOpeAna> entityList = indOpeAnaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-ope-anas/count} : count all the indOpeAnas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-ope-anas/count")
    public ResponseEntity<Long> countIndOpeAnas(IndOpeAnaCriteria criteria) {
        log.debug("REST request to count IndOpeAnas by criteria: {}", criteria);
        return ResponseEntity.ok().body(indOpeAnaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-ope-anas/:id} : get the "id" indOpeAna.
     *
     * @param id the id of the indOpeAna to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indOpeAna, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-ope-anas/{id}")
    public ResponseEntity<IndOpeAna> getIndOpeAna(@PathVariable Long id) {
        log.debug("REST request to get IndOpeAna : {}", id);
        Optional<IndOpeAna> indOpeAna = indOpeAnaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indOpeAna);
    }

    /**
     * {@code DELETE  /ind-ope-anas/:id} : delete the "id" indOpeAna.
     *
     * @param id the id of the indOpeAna to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-ope-anas/{id}")
    public ResponseEntity<Void> deleteIndOpeAna(@PathVariable Long id) {
        log.debug("REST request to delete IndOpeAna : {}", id);
        indOpeAnaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
