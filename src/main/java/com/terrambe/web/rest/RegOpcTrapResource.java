package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcTrap;
import com.terrambe.service.RegOpcTrapService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcTrapCriteria;
import com.terrambe.service.RegOpcTrapQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcTrap}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcTrapResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapResource.class);

    private static final String ENTITY_NAME = "regOpcTrap";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcTrapService regOpcTrapService;

    private final RegOpcTrapQueryService regOpcTrapQueryService;

    public RegOpcTrapResource(RegOpcTrapService regOpcTrapService, RegOpcTrapQueryService regOpcTrapQueryService) {
        this.regOpcTrapService = regOpcTrapService;
        this.regOpcTrapQueryService = regOpcTrapQueryService;
    }

    /**
     * {@code POST  /reg-opc-traps} : Create a new regOpcTrap.
     *
     * @param regOpcTrap the regOpcTrap to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcTrap, or with status {@code 400 (Bad Request)} if the regOpcTrap has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-traps")
    public ResponseEntity<RegOpcTrap> createRegOpcTrap(@Valid @RequestBody RegOpcTrap regOpcTrap) throws URISyntaxException {
        log.debug("REST request to save RegOpcTrap : {}", regOpcTrap);
        if (regOpcTrap.getId() != null) {
            throw new BadRequestAlertException("A new regOpcTrap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcTrap result = regOpcTrapService.save(regOpcTrap);
        return ResponseEntity.created(new URI("/api/reg-opc-traps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-traps} : Updates an existing regOpcTrap.
     *
     * @param regOpcTrap the regOpcTrap to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcTrap,
     * or with status {@code 400 (Bad Request)} if the regOpcTrap is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcTrap couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-traps")
    public ResponseEntity<RegOpcTrap> updateRegOpcTrap(@Valid @RequestBody RegOpcTrap regOpcTrap) throws URISyntaxException {
        log.debug("REST request to update RegOpcTrap : {}", regOpcTrap);
        if (regOpcTrap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcTrap result = regOpcTrapService.save(regOpcTrap);
        RegOpcTrap result2 = regOpcTrapService.save(regOpcTrap);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcTrap.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-traps} : get all the regOpcTraps.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcTraps in body.
     */
    @GetMapping("/reg-opc-traps")
    public ResponseEntity<List<RegOpcTrap>> getAllRegOpcTraps(RegOpcTrapCriteria criteria) {
        log.debug("REST request to get RegOpcTraps by criteria: {}", criteria);
        List<RegOpcTrap> entityList = regOpcTrapQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-traps/count} : count all the regOpcTraps.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-traps/count")
    public ResponseEntity<Long> countRegOpcTraps(RegOpcTrapCriteria criteria) {
        log.debug("REST request to count RegOpcTraps by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcTrapQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-traps/:id} : get the "id" regOpcTrap.
     *
     * @param id the id of the regOpcTrap to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcTrap, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-traps/{id}")
    public ResponseEntity<RegOpcTrap> getRegOpcTrap(@PathVariable Long id) {
        log.debug("REST request to get RegOpcTrap : {}", id);
        Optional<RegOpcTrap> regOpcTrap = regOpcTrapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcTrap);
    }

    /**
     * {@code DELETE  /reg-opc-traps/:id} : delete the "id" regOpcTrap.
     *
     * @param id the id of the regOpcTrap to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-traps/{id}")
    public ResponseEntity<Void> deleteRegOpcTrap(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcTrap : {}", id);
        regOpcTrapService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
