package com.terrambe.web.rest;

import com.terrambe.domain.SostanzeAttive;
import com.terrambe.service.SostanzeAttiveService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.SostanzeAttiveCriteria;
import com.terrambe.service.SostanzeAttiveQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.SostanzeAttive}.
 */
@RestController
@RequestMapping("/api")
public class SostanzeAttiveResource {

    private final Logger log = LoggerFactory.getLogger(SostanzeAttiveResource.class);

    private static final String ENTITY_NAME = "sostanzeAttive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SostanzeAttiveService sostanzeAttiveService;

    private final SostanzeAttiveQueryService sostanzeAttiveQueryService;

    public SostanzeAttiveResource(SostanzeAttiveService sostanzeAttiveService, SostanzeAttiveQueryService sostanzeAttiveQueryService) {
        this.sostanzeAttiveService = sostanzeAttiveService;
        this.sostanzeAttiveQueryService = sostanzeAttiveQueryService;
    }

    /**
     * {@code POST  /sostanze-attives} : Create a new sostanzeAttive.
     *
     * @param sostanzeAttive the sostanzeAttive to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sostanzeAttive, or with status {@code 400 (Bad Request)} if the sostanzeAttive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sostanze-attives")
    public ResponseEntity<SostanzeAttive> createSostanzeAttive(@RequestBody SostanzeAttive sostanzeAttive) throws URISyntaxException {
        log.debug("REST request to save SostanzeAttive : {}", sostanzeAttive);
        if (sostanzeAttive.getId() != null) {
            throw new BadRequestAlertException("A new sostanzeAttive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SostanzeAttive result = sostanzeAttiveService.save(sostanzeAttive);
        return ResponseEntity.created(new URI("/api/sostanze-attives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sostanze-attives} : Updates an existing sostanzeAttive.
     *
     * @param sostanzeAttive the sostanzeAttive to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sostanzeAttive,
     * or with status {@code 400 (Bad Request)} if the sostanzeAttive is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sostanzeAttive couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sostanze-attives")
    public ResponseEntity<SostanzeAttive> updateSostanzeAttive(@RequestBody SostanzeAttive sostanzeAttive) throws URISyntaxException {
        log.debug("REST request to update SostanzeAttive : {}", sostanzeAttive);
        if (sostanzeAttive.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SostanzeAttive result = sostanzeAttiveService.save(sostanzeAttive);
        SostanzeAttive result2 = sostanzeAttiveService.save(sostanzeAttive);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sostanzeAttive.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sostanze-attives} : get all the sostanzeAttives.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sostanzeAttives in body.
     */
    @GetMapping("/sostanze-attives")
    public ResponseEntity<List<SostanzeAttive>> getAllSostanzeAttives(SostanzeAttiveCriteria criteria) {
        log.debug("REST request to get SostanzeAttives by criteria: {}", criteria);
        List<SostanzeAttive> entityList = sostanzeAttiveQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /sostanze-attives/count} : count all the sostanzeAttives.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/sostanze-attives/count")
    public ResponseEntity<Long> countSostanzeAttives(SostanzeAttiveCriteria criteria) {
        log.debug("REST request to count SostanzeAttives by criteria: {}", criteria);
        return ResponseEntity.ok().body(sostanzeAttiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /sostanze-attives/:id} : get the "id" sostanzeAttive.
     *
     * @param id the id of the sostanzeAttive to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sostanzeAttive, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sostanze-attives/{id}")
    public ResponseEntity<SostanzeAttive> getSostanzeAttive(@PathVariable Long id) {
        log.debug("REST request to get SostanzeAttive : {}", id);
        Optional<SostanzeAttive> sostanzeAttive = sostanzeAttiveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sostanzeAttive);
    }

    /**
     * {@code DELETE  /sostanze-attives/:id} : delete the "id" sostanzeAttive.
     *
     * @param id the id of the sostanzeAttive to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sostanze-attives/{id}")
    public ResponseEntity<Void> deleteSostanzeAttive(@PathVariable Long id) {
        log.debug("REST request to delete SostanzeAttive : {}", id);
        sostanzeAttiveService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
