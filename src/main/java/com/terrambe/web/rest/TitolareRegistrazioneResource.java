package com.terrambe.web.rest;

import com.terrambe.domain.TitolareRegistrazione;
import com.terrambe.service.TitolareRegistrazioneService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TitolareRegistrazioneCriteria;
import com.terrambe.service.TitolareRegistrazioneQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TitolareRegistrazione}.
 */
@RestController
@RequestMapping("/api")
public class TitolareRegistrazioneResource {

    private final Logger log = LoggerFactory.getLogger(TitolareRegistrazioneResource.class);

    private static final String ENTITY_NAME = "titolareRegistrazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TitolareRegistrazioneService titolareRegistrazioneService;

    private final TitolareRegistrazioneQueryService titolareRegistrazioneQueryService;

    public TitolareRegistrazioneResource(TitolareRegistrazioneService titolareRegistrazioneService, TitolareRegistrazioneQueryService titolareRegistrazioneQueryService) {
        this.titolareRegistrazioneService = titolareRegistrazioneService;
        this.titolareRegistrazioneQueryService = titolareRegistrazioneQueryService;
    }

    /**
     * {@code POST  /titolare-registraziones} : Create a new titolareRegistrazione.
     *
     * @param titolareRegistrazione the titolareRegistrazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new titolareRegistrazione, or with status {@code 400 (Bad Request)} if the titolareRegistrazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/titolare-registraziones")
    public ResponseEntity<TitolareRegistrazione> createTitolareRegistrazione(@RequestBody TitolareRegistrazione titolareRegistrazione) throws URISyntaxException {
        log.debug("REST request to save TitolareRegistrazione : {}", titolareRegistrazione);
        if (titolareRegistrazione.getId() != null) {
            throw new BadRequestAlertException("A new titolareRegistrazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TitolareRegistrazione result = titolareRegistrazioneService.save(titolareRegistrazione);
        return ResponseEntity.created(new URI("/api/titolare-registraziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /titolare-registraziones} : Updates an existing titolareRegistrazione.
     *
     * @param titolareRegistrazione the titolareRegistrazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated titolareRegistrazione,
     * or with status {@code 400 (Bad Request)} if the titolareRegistrazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the titolareRegistrazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/titolare-registraziones")
    public ResponseEntity<TitolareRegistrazione> updateTitolareRegistrazione(@RequestBody TitolareRegistrazione titolareRegistrazione) throws URISyntaxException {
        log.debug("REST request to update TitolareRegistrazione : {}", titolareRegistrazione);
        if (titolareRegistrazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TitolareRegistrazione result = titolareRegistrazioneService.save(titolareRegistrazione);
        TitolareRegistrazione result2 = titolareRegistrazioneService.save(titolareRegistrazione);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, titolareRegistrazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /titolare-registraziones} : get all the titolareRegistraziones.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of titolareRegistraziones in body.
     */
    @GetMapping("/titolare-registraziones")
    public ResponseEntity<List<TitolareRegistrazione>> getAllTitolareRegistraziones(TitolareRegistrazioneCriteria criteria) {
        log.debug("REST request to get TitolareRegistraziones by criteria: {}", criteria);
        List<TitolareRegistrazione> entityList = titolareRegistrazioneQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /titolare-registraziones/count} : count all the titolareRegistraziones.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/titolare-registraziones/count")
    public ResponseEntity<Long> countTitolareRegistraziones(TitolareRegistrazioneCriteria criteria) {
        log.debug("REST request to count TitolareRegistraziones by criteria: {}", criteria);
        return ResponseEntity.ok().body(titolareRegistrazioneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /titolare-registraziones/:id} : get the "id" titolareRegistrazione.
     *
     * @param id the id of the titolareRegistrazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the titolareRegistrazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/titolare-registraziones/{id}")
    public ResponseEntity<TitolareRegistrazione> getTitolareRegistrazione(@PathVariable Long id) {
        log.debug("REST request to get TitolareRegistrazione : {}", id);
        Optional<TitolareRegistrazione> titolareRegistrazione = titolareRegistrazioneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(titolareRegistrazione);
    }

    /**
     * {@code DELETE  /titolare-registraziones/:id} : delete the "id" titolareRegistrazione.
     *
     * @param id the id of the titolareRegistrazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/titolare-registraziones/{id}")
    public ResponseEntity<Void> deleteTitolareRegistrazione(@PathVariable Long id) {
        log.debug("REST request to delete TitolareRegistrazione : {}", id);
        titolareRegistrazioneService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
