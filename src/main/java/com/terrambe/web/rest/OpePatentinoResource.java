package com.terrambe.web.rest;

import com.terrambe.domain.OpePatentino;
import com.terrambe.service.OpePatentinoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.OpePatentinoCriteria;
import com.terrambe.service.OpePatentinoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.OpePatentino}.
 */
@RestController
@RequestMapping("/api")
public class OpePatentinoResource {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoResource.class);

    private static final String ENTITY_NAME = "opePatentino";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OpePatentinoService opePatentinoService;

    private final OpePatentinoQueryService opePatentinoQueryService;

    public OpePatentinoResource(OpePatentinoService opePatentinoService, OpePatentinoQueryService opePatentinoQueryService) {
        this.opePatentinoService = opePatentinoService;
        this.opePatentinoQueryService = opePatentinoQueryService;
    }

    /**
     * {@code POST  /ope-patentinos} : Create a new opePatentino.
     *
     * @param opePatentino the opePatentino to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new opePatentino, or with status {@code 400 (Bad Request)} if the opePatentino has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ope-patentinos")
    public ResponseEntity<OpePatentino> createOpePatentino(@Valid @RequestBody OpePatentino opePatentino) throws URISyntaxException {
        log.debug("REST request to save OpePatentino : {}", opePatentino);
        if (opePatentino.getId() != null) {
            throw new BadRequestAlertException("A new opePatentino cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpePatentino result = opePatentinoService.save(opePatentino);
        return ResponseEntity.created(new URI("/api/ope-patentinos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ope-patentinos} : Updates an existing opePatentino.
     *
     * @param opePatentino the opePatentino to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated opePatentino,
     * or with status {@code 400 (Bad Request)} if the opePatentino is not valid,
     * or with status {@code 500 (Internal Server Error)} if the opePatentino couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ope-patentinos")
    public ResponseEntity<OpePatentino> updateOpePatentino(@Valid @RequestBody OpePatentino opePatentino) throws URISyntaxException {
        log.debug("REST request to update OpePatentino : {}", opePatentino);
        if (opePatentino.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OpePatentino result = opePatentinoService.save(opePatentino);
        OpePatentino result2 = opePatentinoService.save(opePatentino);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, opePatentino.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ope-patentinos} : get all the opePatentinos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of opePatentinos in body.
     */
    @GetMapping("/ope-patentinos")
    public ResponseEntity<List<OpePatentino>> getAllOpePatentinos(OpePatentinoCriteria criteria) {
        log.debug("REST request to get OpePatentinos by criteria: {}", criteria);
        List<OpePatentino> entityList = opePatentinoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ope-patentinos/count} : count all the opePatentinos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ope-patentinos/count")
    public ResponseEntity<Long> countOpePatentinos(OpePatentinoCriteria criteria) {
        log.debug("REST request to count OpePatentinos by criteria: {}", criteria);
        return ResponseEntity.ok().body(opePatentinoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ope-patentinos/:id} : get the "id" opePatentino.
     *
     * @param id the id of the opePatentino to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the opePatentino, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ope-patentinos/{id}")
    public ResponseEntity<OpePatentino> getOpePatentino(@PathVariable Long id) {
        log.debug("REST request to get OpePatentino : {}", id);
        Optional<OpePatentino> opePatentino = opePatentinoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(opePatentino);
    }

    /**
     * {@code DELETE  /ope-patentinos/:id} : delete the "id" opePatentino.
     *
     * @param id the id of the opePatentino to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ope-patentinos/{id}")
    public ResponseEntity<Void> deleteOpePatentino(@PathVariable Long id) {
        log.debug("REST request to delete OpePatentino : {}", id);
        opePatentinoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
