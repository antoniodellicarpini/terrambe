package com.terrambe.web.rest;

import com.terrambe.domain.TipoAltro;
import com.terrambe.service.TipoAltroService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoAltroCriteria;
import com.terrambe.service.TipoAltroQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoAltro}.
 */
@RestController
@RequestMapping("/api")
public class TipoAltroResource {

    private final Logger log = LoggerFactory.getLogger(TipoAltroResource.class);

    private static final String ENTITY_NAME = "tipoAltro";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoAltroService tipoAltroService;

    private final TipoAltroQueryService tipoAltroQueryService;

    public TipoAltroResource(TipoAltroService tipoAltroService, TipoAltroQueryService tipoAltroQueryService) {
        this.tipoAltroService = tipoAltroService;
        this.tipoAltroQueryService = tipoAltroQueryService;
    }

    /**
     * {@code POST  /tipo-altros} : Create a new tipoAltro.
     *
     * @param tipoAltro the tipoAltro to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoAltro, or with status {@code 400 (Bad Request)} if the tipoAltro has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-altros")
    public ResponseEntity<TipoAltro> createTipoAltro(@RequestBody TipoAltro tipoAltro) throws URISyntaxException {
        log.debug("REST request to save TipoAltro : {}", tipoAltro);
        if (tipoAltro.getId() != null) {
            throw new BadRequestAlertException("A new tipoAltro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoAltro result = tipoAltroService.save(tipoAltro);
        return ResponseEntity.created(new URI("/api/tipo-altros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-altros} : Updates an existing tipoAltro.
     *
     * @param tipoAltro the tipoAltro to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoAltro,
     * or with status {@code 400 (Bad Request)} if the tipoAltro is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoAltro couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-altros")
    public ResponseEntity<TipoAltro> updateTipoAltro(@RequestBody TipoAltro tipoAltro) throws URISyntaxException {
        log.debug("REST request to update TipoAltro : {}", tipoAltro);
        if (tipoAltro.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoAltro result = tipoAltroService.save(tipoAltro);
        TipoAltro result2 = tipoAltroService.save(tipoAltro);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoAltro.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-altros} : get all the tipoAltros.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoAltros in body.
     */
    @GetMapping("/tipo-altros")
    public ResponseEntity<List<TipoAltro>> getAllTipoAltros(TipoAltroCriteria criteria) {
        log.debug("REST request to get TipoAltros by criteria: {}", criteria);
        List<TipoAltro> entityList = tipoAltroQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-altros/count} : count all the tipoAltros.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-altros/count")
    public ResponseEntity<Long> countTipoAltros(TipoAltroCriteria criteria) {
        log.debug("REST request to count TipoAltros by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoAltroQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-altros/:id} : get the "id" tipoAltro.
     *
     * @param id the id of the tipoAltro to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoAltro, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-altros/{id}")
    public ResponseEntity<TipoAltro> getTipoAltro(@PathVariable Long id) {
        log.debug("REST request to get TipoAltro : {}", id);
        Optional<TipoAltro> tipoAltro = tipoAltroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoAltro);
    }

    /**
     * {@code DELETE  /tipo-altros/:id} : delete the "id" tipoAltro.
     *
     * @param id the id of the tipoAltro to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-altros/{id}")
    public ResponseEntity<Void> deleteTipoAltro(@PathVariable Long id) {
        log.debug("REST request to delete TipoAltro : {}", id);
        tipoAltroService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
