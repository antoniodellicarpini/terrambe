package com.terrambe.web.rest;

import com.terrambe.domain.Frasi;
import com.terrambe.service.FrasiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FrasiCriteria;
import com.terrambe.service.FrasiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Frasi}.
 */
@RestController
@RequestMapping("/api")
public class FrasiResource {

    private final Logger log = LoggerFactory.getLogger(FrasiResource.class);

    private static final String ENTITY_NAME = "frasi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FrasiService frasiService;

    private final FrasiQueryService frasiQueryService;

    public FrasiResource(FrasiService frasiService, FrasiQueryService frasiQueryService) {
        this.frasiService = frasiService;
        this.frasiQueryService = frasiQueryService;
    }

    /**
     * {@code POST  /frasis} : Create a new frasi.
     *
     * @param frasi the frasi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new frasi, or with status {@code 400 (Bad Request)} if the frasi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/frasis")
    public ResponseEntity<Frasi> createFrasi(@RequestBody Frasi frasi) throws URISyntaxException {
        log.debug("REST request to save Frasi : {}", frasi);
        if (frasi.getId() != null) {
            throw new BadRequestAlertException("A new frasi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Frasi result = frasiService.save(frasi);
        return ResponseEntity.created(new URI("/api/frasis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /frasis} : Updates an existing frasi.
     *
     * @param frasi the frasi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated frasi,
     * or with status {@code 400 (Bad Request)} if the frasi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the frasi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/frasis")
    public ResponseEntity<Frasi> updateFrasi(@RequestBody Frasi frasi) throws URISyntaxException {
        log.debug("REST request to update Frasi : {}", frasi);
        if (frasi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Frasi result = frasiService.save(frasi);
        Frasi result2 = frasiService.save(frasi);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, frasi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /frasis} : get all the frasis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of frasis in body.
     */
    @GetMapping("/frasis")
    public ResponseEntity<List<Frasi>> getAllFrasis(FrasiCriteria criteria) {
        log.debug("REST request to get Frasis by criteria: {}", criteria);
        List<Frasi> entityList = frasiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /frasis/count} : count all the frasis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/frasis/count")
    public ResponseEntity<Long> countFrasis(FrasiCriteria criteria) {
        log.debug("REST request to count Frasis by criteria: {}", criteria);
        return ResponseEntity.ok().body(frasiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /frasis/:id} : get the "id" frasi.
     *
     * @param id the id of the frasi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the frasi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/frasis/{id}")
    public ResponseEntity<Frasi> getFrasi(@PathVariable Long id) {
        log.debug("REST request to get Frasi : {}", id);
        Optional<Frasi> frasi = frasiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(frasi);
    }

    /**
     * {@code DELETE  /frasis/:id} : delete the "id" frasi.
     *
     * @param id the id of the frasi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/frasis/{id}")
    public ResponseEntity<Void> deleteFrasi(@PathVariable Long id) {
        log.debug("REST request to delete Frasi : {}", id);
        frasiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
