package com.terrambe.web.rest;

import com.terrambe.domain.TipoDistFito;
import com.terrambe.service.TipoDistFitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoDistFitoCriteria;
import com.terrambe.service.TipoDistFitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoDistFito}.
 */
@RestController
@RequestMapping("/api")
public class TipoDistFitoResource {

    private final Logger log = LoggerFactory.getLogger(TipoDistFitoResource.class);

    private static final String ENTITY_NAME = "tipoDistFito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoDistFitoService tipoDistFitoService;

    private final TipoDistFitoQueryService tipoDistFitoQueryService;

    public TipoDistFitoResource(TipoDistFitoService tipoDistFitoService, TipoDistFitoQueryService tipoDistFitoQueryService) {
        this.tipoDistFitoService = tipoDistFitoService;
        this.tipoDistFitoQueryService = tipoDistFitoQueryService;
    }

    /**
     * {@code POST  /tipo-dist-fitos} : Create a new tipoDistFito.
     *
     * @param tipoDistFito the tipoDistFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoDistFito, or with status {@code 400 (Bad Request)} if the tipoDistFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-dist-fitos")
    public ResponseEntity<TipoDistFito> createTipoDistFito(@RequestBody TipoDistFito tipoDistFito) throws URISyntaxException {
        log.debug("REST request to save TipoDistFito : {}", tipoDistFito);
        if (tipoDistFito.getId() != null) {
            throw new BadRequestAlertException("A new tipoDistFito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoDistFito result = tipoDistFitoService.save(tipoDistFito);
        return ResponseEntity.created(new URI("/api/tipo-dist-fitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-dist-fitos} : Updates an existing tipoDistFito.
     *
     * @param tipoDistFito the tipoDistFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoDistFito,
     * or with status {@code 400 (Bad Request)} if the tipoDistFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoDistFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-dist-fitos")
    public ResponseEntity<TipoDistFito> updateTipoDistFito(@RequestBody TipoDistFito tipoDistFito) throws URISyntaxException {
        log.debug("REST request to update TipoDistFito : {}", tipoDistFito);
        if (tipoDistFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoDistFito result = tipoDistFitoService.save(tipoDistFito);
        TipoDistFito result2 = tipoDistFitoService.save(tipoDistFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoDistFito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-dist-fitos} : get all the tipoDistFitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoDistFitos in body.
     */
    @GetMapping("/tipo-dist-fitos")
    public ResponseEntity<List<TipoDistFito>> getAllTipoDistFitos(TipoDistFitoCriteria criteria) {
        log.debug("REST request to get TipoDistFitos by criteria: {}", criteria);
        List<TipoDistFito> entityList = tipoDistFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-dist-fitos/count} : count all the tipoDistFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-dist-fitos/count")
    public ResponseEntity<Long> countTipoDistFitos(TipoDistFitoCriteria criteria) {
        log.debug("REST request to count TipoDistFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoDistFitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-dist-fitos/:id} : get the "id" tipoDistFito.
     *
     * @param id the id of the tipoDistFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoDistFito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-dist-fitos/{id}")
    public ResponseEntity<TipoDistFito> getTipoDistFito(@PathVariable Long id) {
        log.debug("REST request to get TipoDistFito : {}", id);
        Optional<TipoDistFito> tipoDistFito = tipoDistFitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoDistFito);
    }

    /**
     * {@code DELETE  /tipo-dist-fitos/:id} : delete the "id" tipoDistFito.
     *
     * @param id the id of the tipoDistFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-dist-fitos/{id}")
    public ResponseEntity<Void> deleteTipoDistFito(@PathVariable Long id) {
        log.debug("REST request to delete TipoDistFito : {}", id);
        tipoDistFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
