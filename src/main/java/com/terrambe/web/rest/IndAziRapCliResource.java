package com.terrambe.web.rest;

import com.terrambe.domain.IndAziRapCli;
import com.terrambe.service.IndAziRapCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndAziRapCliCriteria;
import com.terrambe.service.IndAziRapCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndAziRapCli}.
 */
@RestController
@RequestMapping("/api")
public class IndAziRapCliResource {

    private final Logger log = LoggerFactory.getLogger(IndAziRapCliResource.class);

    private static final String ENTITY_NAME = "indAziRapCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndAziRapCliService indAziRapCliService;

    private final IndAziRapCliQueryService indAziRapCliQueryService;

    public IndAziRapCliResource(IndAziRapCliService indAziRapCliService, IndAziRapCliQueryService indAziRapCliQueryService) {
        this.indAziRapCliService = indAziRapCliService;
        this.indAziRapCliQueryService = indAziRapCliQueryService;
    }

    /**
     * {@code POST  /ind-azi-rap-clis} : Create a new indAziRapCli.
     *
     * @param indAziRapCli the indAziRapCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indAziRapCli, or with status {@code 400 (Bad Request)} if the indAziRapCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-azi-rap-clis")
    public ResponseEntity<IndAziRapCli> createIndAziRapCli(@Valid @RequestBody IndAziRapCli indAziRapCli) throws URISyntaxException {
        log.debug("REST request to save IndAziRapCli : {}", indAziRapCli);
        if (indAziRapCli.getId() != null) {
            throw new BadRequestAlertException("A new indAziRapCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndAziRapCli result = indAziRapCliService.save(indAziRapCli);
        return ResponseEntity.created(new URI("/api/ind-azi-rap-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-azi-rap-clis} : Updates an existing indAziRapCli.
     *
     * @param indAziRapCli the indAziRapCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indAziRapCli,
     * or with status {@code 400 (Bad Request)} if the indAziRapCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indAziRapCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-azi-rap-clis")
    public ResponseEntity<IndAziRapCli> updateIndAziRapCli(@Valid @RequestBody IndAziRapCli indAziRapCli) throws URISyntaxException {
        log.debug("REST request to update IndAziRapCli : {}", indAziRapCli);
        if (indAziRapCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndAziRapCli result = indAziRapCliService.save(indAziRapCli);
        IndAziRapCli result2 = indAziRapCliService.save(indAziRapCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indAziRapCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-azi-rap-clis} : get all the indAziRapClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indAziRapClis in body.
     */
    @GetMapping("/ind-azi-rap-clis")
    public ResponseEntity<List<IndAziRapCli>> getAllIndAziRapClis(IndAziRapCliCriteria criteria) {
        log.debug("REST request to get IndAziRapClis by criteria: {}", criteria);
        List<IndAziRapCli> entityList = indAziRapCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-azi-rap-clis/count} : count all the indAziRapClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-azi-rap-clis/count")
    public ResponseEntity<Long> countIndAziRapClis(IndAziRapCliCriteria criteria) {
        log.debug("REST request to count IndAziRapClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(indAziRapCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-azi-rap-clis/:id} : get the "id" indAziRapCli.
     *
     * @param id the id of the indAziRapCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indAziRapCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-azi-rap-clis/{id}")
    public ResponseEntity<IndAziRapCli> getIndAziRapCli(@PathVariable Long id) {
        log.debug("REST request to get IndAziRapCli : {}", id);
        Optional<IndAziRapCli> indAziRapCli = indAziRapCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indAziRapCli);
    }

    /**
     * {@code DELETE  /ind-azi-rap-clis/:id} : delete the "id" indAziRapCli.
     *
     * @param id the id of the indAziRapCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-azi-rap-clis/{id}")
    public ResponseEntity<Void> deleteIndAziRapCli(@PathVariable Long id) {
        log.debug("REST request to delete IndAziRapCli : {}", id);
        indAziRapCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
