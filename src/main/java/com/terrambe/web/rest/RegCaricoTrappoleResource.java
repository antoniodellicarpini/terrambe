package com.terrambe.web.rest;

import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.service.RegCaricoTrappoleService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegCaricoTrappoleCriteria;
import com.terrambe.service.RegCaricoTrappoleQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegCaricoTrappole}.
 */
@RestController
@RequestMapping("/api")
public class RegCaricoTrappoleResource {

    private final Logger log = LoggerFactory.getLogger(RegCaricoTrappoleResource.class);

    private static final String ENTITY_NAME = "regCaricoTrappole";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegCaricoTrappoleService regCaricoTrappoleService;

    private final RegCaricoTrappoleQueryService regCaricoTrappoleQueryService;

    public RegCaricoTrappoleResource(RegCaricoTrappoleService regCaricoTrappoleService, RegCaricoTrappoleQueryService regCaricoTrappoleQueryService) {
        this.regCaricoTrappoleService = regCaricoTrappoleService;
        this.regCaricoTrappoleQueryService = regCaricoTrappoleQueryService;
    }

    /**
     * {@code POST  /reg-carico-trappoles} : Create a new regCaricoTrappole.
     *
     * @param regCaricoTrappole the regCaricoTrappole to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regCaricoTrappole, or with status {@code 400 (Bad Request)} if the regCaricoTrappole has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-carico-trappoles")
    public ResponseEntity<RegCaricoTrappole> createRegCaricoTrappole(@Valid @RequestBody RegCaricoTrappole regCaricoTrappole) throws URISyntaxException {
        log.debug("REST request to save RegCaricoTrappole : {}", regCaricoTrappole);
        if (regCaricoTrappole.getId() != null) {
            throw new BadRequestAlertException("A new regCaricoTrappole cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegCaricoTrappole result = regCaricoTrappoleService.save(regCaricoTrappole);
        return ResponseEntity.created(new URI("/api/reg-carico-trappoles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-carico-trappoles} : Updates an existing regCaricoTrappole.
     *
     * @param regCaricoTrappole the regCaricoTrappole to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regCaricoTrappole,
     * or with status {@code 400 (Bad Request)} if the regCaricoTrappole is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regCaricoTrappole couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-carico-trappoles")
    public ResponseEntity<RegCaricoTrappole> updateRegCaricoTrappole(@Valid @RequestBody RegCaricoTrappole regCaricoTrappole) throws URISyntaxException {
        log.debug("REST request to update RegCaricoTrappole : {}", regCaricoTrappole);
        if (regCaricoTrappole.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegCaricoTrappole result = regCaricoTrappoleService.save(regCaricoTrappole);
        RegCaricoTrappole result2 = regCaricoTrappoleService.save(regCaricoTrappole);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regCaricoTrappole.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-carico-trappoles} : get all the regCaricoTrappoles.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regCaricoTrappoles in body.
     */
    @GetMapping("/reg-carico-trappoles")
    public ResponseEntity<List<RegCaricoTrappole>> getAllRegCaricoTrappoles(RegCaricoTrappoleCriteria criteria) {
        log.debug("REST request to get RegCaricoTrappoles by criteria: {}", criteria);
        List<RegCaricoTrappole> entityList = regCaricoTrappoleQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-carico-trappoles/count} : count all the regCaricoTrappoles.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-carico-trappoles/count")
    public ResponseEntity<Long> countRegCaricoTrappoles(RegCaricoTrappoleCriteria criteria) {
        log.debug("REST request to count RegCaricoTrappoles by criteria: {}", criteria);
        return ResponseEntity.ok().body(regCaricoTrappoleQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-carico-trappoles/:id} : get the "id" regCaricoTrappole.
     *
     * @param id the id of the regCaricoTrappole to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regCaricoTrappole, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-carico-trappoles/{id}")
    public ResponseEntity<RegCaricoTrappole> getRegCaricoTrappole(@PathVariable Long id) {
        log.debug("REST request to get RegCaricoTrappole : {}", id);
        Optional<RegCaricoTrappole> regCaricoTrappole = regCaricoTrappoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regCaricoTrappole);
    }

    /**
     * {@code DELETE  /reg-carico-trappoles/:id} : delete the "id" regCaricoTrappole.
     *
     * @param id the id of the regCaricoTrappole to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-carico-trappoles/{id}")
    public ResponseEntity<Void> deleteRegCaricoTrappole(@PathVariable Long id) {
        log.debug("REST request to delete RegCaricoTrappole : {}", id);
        regCaricoTrappoleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
