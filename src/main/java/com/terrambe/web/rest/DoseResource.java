package com.terrambe.web.rest;

import com.terrambe.domain.Dose;
import com.terrambe.service.DoseService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DoseCriteria;
import com.terrambe.service.DoseQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Dose}.
 */
@RestController
@RequestMapping("/api")
public class DoseResource {

    private final Logger log = LoggerFactory.getLogger(DoseResource.class);

    private static final String ENTITY_NAME = "dose";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DoseService doseService;

    private final DoseQueryService doseQueryService;

    public DoseResource(DoseService doseService, DoseQueryService doseQueryService) {
        this.doseService = doseService;
        this.doseQueryService = doseQueryService;
    }

    /**
     * {@code POST  /doses} : Create a new dose.
     *
     * @param dose the dose to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the dose has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/doses")
    public ResponseEntity<Dose> createDose(@RequestBody Dose dose) throws URISyntaxException {
        log.debug("REST request to save Dose : {}", dose);
        if (dose.getId() != null) {
            throw new BadRequestAlertException("A new dose cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Dose result = doseService.save(dose);
        return ResponseEntity.created(new URI("/api/doses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /doses} : Updates an existing dose.
     *
     * @param dose the dose to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dose,
     * or with status {@code 400 (Bad Request)} if the dose is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dose couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/doses")
    public ResponseEntity<Dose> updateDose(@RequestBody Dose dose) throws URISyntaxException {
        log.debug("REST request to update Dose : {}", dose);
        if (dose.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Dose result = doseService.save(dose);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dose.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /doses} : get all the doses.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of doses in body.
     */
    @GetMapping("/doses")
    public ResponseEntity<List<Dose>> getAllDoses(DoseCriteria criteria) {
        log.debug("REST request to get Doses by criteria: {}", criteria);
        List<Dose> entityList = doseQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /doses/count} : count all the doses.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/doses/count")
    public ResponseEntity<Long> countDoses(DoseCriteria criteria) {
        log.debug("REST request to count Doses by criteria: {}", criteria);
        return ResponseEntity.ok().body(doseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /doses/:id} : get the "id" dose.
     *
     * @param id the id of the dose to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dose, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/doses/{id}")
    public ResponseEntity<Dose> getDose(@PathVariable Long id) {
        log.debug("REST request to get Dose : {}", id);
        Optional<Dose> dose = doseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dose);
    }

    /**
     * {@code DELETE  /doses/:id} : delete the "id" dose.
     *
     * @param id the id of the dose to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/doses/{id}")
    public ResponseEntity<Void> deleteDose(@PathVariable Long id) {
        log.debug("REST request to delete Dose : {}", id);
        doseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
