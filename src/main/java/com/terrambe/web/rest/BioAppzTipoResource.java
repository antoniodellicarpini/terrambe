package com.terrambe.web.rest;

import com.terrambe.domain.BioAppzTipo;
import com.terrambe.service.BioAppzTipoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.BioAppzTipoCriteria;
import com.terrambe.service.BioAppzTipoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.BioAppzTipo}.
 */
@RestController
@RequestMapping("/api")
public class BioAppzTipoResource {

    private final Logger log = LoggerFactory.getLogger(BioAppzTipoResource.class);

    private static final String ENTITY_NAME = "bioAppzTipo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BioAppzTipoService bioAppzTipoService;

    private final BioAppzTipoQueryService bioAppzTipoQueryService;

    public BioAppzTipoResource(BioAppzTipoService bioAppzTipoService, BioAppzTipoQueryService bioAppzTipoQueryService) {
        this.bioAppzTipoService = bioAppzTipoService;
        this.bioAppzTipoQueryService = bioAppzTipoQueryService;
    }

    /**
     * {@code POST  /bio-appz-tipos} : Create a new bioAppzTipo.
     *
     * @param bioAppzTipo the bioAppzTipo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bioAppzTipo, or with status {@code 400 (Bad Request)} if the bioAppzTipo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bio-appz-tipos")
    public ResponseEntity<BioAppzTipo> createBioAppzTipo(@Valid @RequestBody BioAppzTipo bioAppzTipo) throws URISyntaxException {
        log.debug("REST request to save BioAppzTipo : {}", bioAppzTipo);
        if (bioAppzTipo.getId() != null) {
            throw new BadRequestAlertException("A new bioAppzTipo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BioAppzTipo result = bioAppzTipoService.save(bioAppzTipo);
        return ResponseEntity.created(new URI("/api/bio-appz-tipos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bio-appz-tipos} : Updates an existing bioAppzTipo.
     *
     * @param bioAppzTipo the bioAppzTipo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bioAppzTipo,
     * or with status {@code 400 (Bad Request)} if the bioAppzTipo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bioAppzTipo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bio-appz-tipos")
    public ResponseEntity<BioAppzTipo> updateBioAppzTipo(@Valid @RequestBody BioAppzTipo bioAppzTipo) throws URISyntaxException {
        log.debug("REST request to update BioAppzTipo : {}", bioAppzTipo);
        if (bioAppzTipo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BioAppzTipo result = bioAppzTipoService.save(bioAppzTipo);
        BioAppzTipo result2 = bioAppzTipoService.save(bioAppzTipo);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bioAppzTipo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bio-appz-tipos} : get all the bioAppzTipos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bioAppzTipos in body.
     */
    @GetMapping("/bio-appz-tipos")
    public ResponseEntity<List<BioAppzTipo>> getAllBioAppzTipos(BioAppzTipoCriteria criteria) {
        log.debug("REST request to get BioAppzTipos by criteria: {}", criteria);
        List<BioAppzTipo> entityList = bioAppzTipoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /bio-appz-tipos/count} : count all the bioAppzTipos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/bio-appz-tipos/count")
    public ResponseEntity<Long> countBioAppzTipos(BioAppzTipoCriteria criteria) {
        log.debug("REST request to count BioAppzTipos by criteria: {}", criteria);
        return ResponseEntity.ok().body(bioAppzTipoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bio-appz-tipos/:id} : get the "id" bioAppzTipo.
     *
     * @param id the id of the bioAppzTipo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bioAppzTipo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bio-appz-tipos/{id}")
    public ResponseEntity<BioAppzTipo> getBioAppzTipo(@PathVariable Long id) {
        log.debug("REST request to get BioAppzTipo : {}", id);
        Optional<BioAppzTipo> bioAppzTipo = bioAppzTipoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bioAppzTipo);
    }

    /**
     * {@code DELETE  /bio-appz-tipos/:id} : delete the "id" bioAppzTipo.
     *
     * @param id the id of the bioAppzTipo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bio-appz-tipos/{id}")
    public ResponseEntity<Void> deleteBioAppzTipo(@PathVariable Long id) {
        log.debug("REST request to delete BioAppzTipo : {}", id);
        bioAppzTipoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
