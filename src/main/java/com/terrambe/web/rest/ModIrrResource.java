package com.terrambe.web.rest;

import com.terrambe.domain.ModIrr;
import com.terrambe.service.ModIrrService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.ModIrrCriteria;
import com.terrambe.service.ModIrrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.ModIrr}.
 */
@RestController
@RequestMapping("/api")
public class ModIrrResource {

    private final Logger log = LoggerFactory.getLogger(ModIrrResource.class);

    private static final String ENTITY_NAME = "modIrr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ModIrrService modIrrService;

    private final ModIrrQueryService modIrrQueryService;

    public ModIrrResource(ModIrrService modIrrService, ModIrrQueryService modIrrQueryService) {
        this.modIrrService = modIrrService;
        this.modIrrQueryService = modIrrQueryService;
    }

    /**
     * {@code POST  /mod-irrs} : Create a new modIrr.
     *
     * @param modIrr the modIrr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new modIrr, or with status {@code 400 (Bad Request)} if the modIrr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mod-irrs")
    public ResponseEntity<ModIrr> createModIrr(@RequestBody ModIrr modIrr) throws URISyntaxException {
        log.debug("REST request to save ModIrr : {}", modIrr);
        if (modIrr.getId() != null) {
            throw new BadRequestAlertException("A new modIrr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModIrr result = modIrrService.save(modIrr);
        return ResponseEntity.created(new URI("/api/mod-irrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mod-irrs} : Updates an existing modIrr.
     *
     * @param modIrr the modIrr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modIrr,
     * or with status {@code 400 (Bad Request)} if the modIrr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the modIrr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mod-irrs")
    public ResponseEntity<ModIrr> updateModIrr(@RequestBody ModIrr modIrr) throws URISyntaxException {
        log.debug("REST request to update ModIrr : {}", modIrr);
        if (modIrr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModIrr result = modIrrService.save(modIrr);
        ModIrr result2 = modIrrService.save(modIrr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, modIrr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mod-irrs} : get all the modIrrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of modIrrs in body.
     */
    @GetMapping("/mod-irrs")
    public ResponseEntity<List<ModIrr>> getAllModIrrs(ModIrrCriteria criteria) {
        log.debug("REST request to get ModIrrs by criteria: {}", criteria);
        List<ModIrr> entityList = modIrrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /mod-irrs/count} : count all the modIrrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/mod-irrs/count")
    public ResponseEntity<Long> countModIrrs(ModIrrCriteria criteria) {
        log.debug("REST request to count ModIrrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(modIrrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /mod-irrs/:id} : get the "id" modIrr.
     *
     * @param id the id of the modIrr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the modIrr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mod-irrs/{id}")
    public ResponseEntity<ModIrr> getModIrr(@PathVariable Long id) {
        log.debug("REST request to get ModIrr : {}", id);
        Optional<ModIrr> modIrr = modIrrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modIrr);
    }

    /**
     * {@code DELETE  /mod-irrs/:id} : delete the "id" modIrr.
     *
     * @param id the id of the modIrr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mod-irrs/{id}")
    public ResponseEntity<Void> deleteModIrr(@PathVariable Long id) {
        log.debug("REST request to delete ModIrr : {}", id);
        modIrrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
