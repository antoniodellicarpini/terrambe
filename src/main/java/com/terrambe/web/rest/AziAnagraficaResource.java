package com.terrambe.web.rest;

import com.terrambe.domain.AziAnagrafica;
import com.terrambe.service.AziAnagraficaService;
import com.terrambe.service.UtilityJpaQueriesService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziAnagraficaCriteria;
import com.terrambe.service.AziAnagraficaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziAnagrafica}.
 */
@RestController
@RequestMapping("/api")
public class AziAnagraficaResource {

    private final Logger log = LoggerFactory.getLogger(AziAnagraficaResource.class);

    private static final String ENTITY_NAME = "aziAnagrafica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    //EspGio Eseguo la dependency injection della classe di servizio per le query jpa
    @Autowired
    private UtilityJpaQueriesService utilityJpaQueriesService;

    private final AziAnagraficaService aziAnagraficaService;

    private final AziAnagraficaQueryService aziAnagraficaQueryService;

    public AziAnagraficaResource(AziAnagraficaService aziAnagraficaService, AziAnagraficaQueryService aziAnagraficaQueryService) {
        this.aziAnagraficaService = aziAnagraficaService;
        this.aziAnagraficaQueryService = aziAnagraficaQueryService;
    }

    /**
     * {@code POST  /azi-anagraficas} : Create a new aziAnagrafica.
     *
     * @param aziAnagrafica the aziAnagrafica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziAnagrafica, or with status {@code 400 (Bad Request)} if the aziAnagrafica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-anagraficas")
    public ResponseEntity<AziAnagrafica> createAziAnagrafica(@Valid @RequestBody AziAnagrafica aziAnagrafica) throws URISyntaxException {
        log.debug("REST request to save AziAnagrafica : {}", aziAnagrafica);
        if (aziAnagrafica.getId() != null) {
            throw new BadRequestAlertException("A new aziAnagrafica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziAnagrafica result = aziAnagraficaService.save(aziAnagrafica);
      /*
        int isStored = utilityJpaQueriesService.addCurrentUserToEntityCreate("AziAnagrafica", result.getId().toString());
        //IsStord ritorna il numero di entity modificate
       if (isStored <= 0) {
            throw new BadRequestAlertException("Impossibile aggiungere l'utente che ha eseguito la modifica", ENTITY_NAME, "idexists");
       }
       */
        return ResponseEntity.created(new URI("/api/azi-anagraficas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-anagraficas} : Updates an existing aziAnagrafica.
     *
     * @param aziAnagrafica the aziAnagrafica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziAnagrafica,
     * or with status {@code 400 (Bad Request)} if the aziAnagrafica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziAnagrafica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-anagraficas")
    public ResponseEntity<AziAnagrafica> updateAziAnagrafica(@Valid @RequestBody AziAnagrafica aziAnagrafica) throws URISyntaxException {
        log.debug("REST request to update AziAnagrafica : {}", aziAnagrafica);

        //controllo che l'azienda esista
        if (aziAnagrafica.getId() == null) {
            throw new BadRequestAlertException("Id non dell'azienda da modificare non valido", ENTITY_NAME, "idnull");
        }

        /*
        //controllo se il record è stato precedentemente chiuso
        if (utilityJpaQueriesService.isADeadRecord("AziAnagrafica", aziAnagrafica.getId().toString())) {
            throw new BadRequestAlertException("Si sta cercando di referenziare un record gia chiuso", ENTITY_NAME, "idnull");
        }

        //Superati i due controll salvo l'informazione dell'utente che sta eseguendo la modfica nel record inziale.
        int youLastOne = utilityJpaQueriesService.addCurrentUserToEntityMod("AziAnagrafica", aziAnagrafica.getId().toString());
        //controllo che il salvataggio sia avvenuto correttamente
        if (youLastOne > 0) {
            throw new BadRequestAlertException("Invalidazione record non riuscita in fase di update logico", ENTITY_NAME, "idnull");
        }
        //aggiorno la data fine validità ad oggi
        int timeIsDone = utilityJpaQueriesService.addClosingDateToEntity("AziAnagrafica", aziAnagrafica.getId().toString());
        //Controllo se l'operazione è stata eseguita realmente
        if (timeIsDone > 0) {
            throw new BadRequestAlertException("Invalidazione record non riuscita in fase di update logico", ENTITY_NAME, "idnull");
        }
        //Rimuovo l'id per generare un nuovo record (il save così diventa come quello della post)
        aziAnagrafica.setId(null);
        //inserisco il nuovo record
        */

        AziAnagrafica result = aziAnagraficaService.save(aziAnagrafica);
        AziAnagrafica result2 = aziAnagraficaService.save(aziAnagrafica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-anagraficas} : get all the aziAnagraficas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziAnagraficas in body.
     */
    @GetMapping("/azi-anagraficas")
    public ResponseEntity<List<AziAnagrafica>> getAllAziAnagraficas(AziAnagraficaCriteria criteria) {
        log.debug("REST request to get AziAnagraficas by criteria: {}", criteria);
        List<AziAnagrafica> entityList = aziAnagraficaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-anagraficas/count} : count all the aziAnagraficas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-anagraficas/count")
    public ResponseEntity<Long> countAziAnagraficas(AziAnagraficaCriteria criteria) {
        log.debug("REST request to count AziAnagraficas by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziAnagraficaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-anagraficas/:id} : get the "id" aziAnagrafica.
     *
     * @param id the id of the aziAnagrafica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziAnagrafica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-anagraficas/{id}")
    public ResponseEntity<AziAnagrafica> getAziAnagrafica(@PathVariable Long id) {
        log.debug("REST request to get AziAnagrafica : {}", id);
        Optional<AziAnagrafica> aziAnagrafica = aziAnagraficaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziAnagrafica);
    }

    /**
     * {@code DELETE  /azi-anagraficas/:id} : delete the "id" aziAnagrafica.
     *
     * @param id the id of the aziAnagrafica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-anagraficas/{id}")
    public ResponseEntity<Void> deleteAziAnagrafica(@PathVariable Long id) {
        if (id == null){
            throw new BadRequestAlertException("Eliminazione logica non riuscita il parametro id non può essere null", ENTITY_NAME, "idnull");
        }
        log.debug("REST request to delete AziAnagrafica : {}", id);
        int timeIsDone = utilityJpaQueriesService.addClosingDateToEntity("AziAnagrafica", id.toString());
        //Controllo se l'operazione è stata eseguita realmente
        if (timeIsDone > 0) {
            throw new BadRequestAlertException("Invalidazione record non riuscita in fase di update logico", ENTITY_NAME, "idnull");
        }
        //aziAnagraficaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
