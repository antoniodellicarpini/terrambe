package com.terrambe.web.rest;

import com.terrambe.domain.ListRegOpcAltro;
import com.terrambe.domain.RegOpcAltro;
import com.terrambe.service.RegOpcAltroService;
import com.terrambe.service.UtilityJpaQueriesService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcAltroCriteria;
import com.terrambe.service.RegOpcAltroQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcAltro}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcAltroResource {

    @Autowired
    private DataSource dataSource;
    
    private final Logger log = LoggerFactory.getLogger(RegOpcAltroResource.class);

    private static final String ENTITY_NAME = "regOpcAltro";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcAltroService regOpcAltroService;

    private final RegOpcAltroQueryService regOpcAltroQueryService;

    public RegOpcAltroResource(RegOpcAltroService regOpcAltroService, RegOpcAltroQueryService regOpcAltroQueryService) {
        this.regOpcAltroService = regOpcAltroService;
        this.regOpcAltroQueryService = regOpcAltroQueryService;
    }

    /**
     * {@code POST  /reg-opc-altros} : Create a new regOpcAltro.
     *
     * @param regOpcAltro the regOpcAltro to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcAltro, or with status {@code 400 (Bad Request)} if the regOpcAltro has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-altros")
    public ResponseEntity<RegOpcAltro> createRegOpcAltro(@Valid @RequestBody RegOpcAltro regOpcAltro) throws URISyntaxException {
        log.debug("REST request to save RegOpcAltro : {}", regOpcAltro);
        if (regOpcAltro.getId() != null) {
            throw new BadRequestAlertException("A new regOpcAltro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcAltro result = regOpcAltroService.save(regOpcAltro);
        return ResponseEntity.created(new URI("/api/reg-opc-altros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-altros} : Updates an existing regOpcAltro.
     *
     * @param regOpcAltro the regOpcAltro to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcAltro,
     * or with status {@code 400 (Bad Request)} if the regOpcAltro is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcAltro couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-altros")
    public ResponseEntity<RegOpcAltro> updateRegOpcAltro(@Valid @RequestBody RegOpcAltro regOpcAltro) throws URISyntaxException {
        log.debug("REST request to update RegOpcAltro : {}", regOpcAltro);
        if (regOpcAltro.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcAltro result = regOpcAltroService.save(regOpcAltro);
        RegOpcAltro result2 = regOpcAltroService.save(regOpcAltro);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcAltro.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-altros} : get all the regOpcAltros.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcAltros in body.
     */
    @GetMapping("/reg-opc-altros")
    public ResponseEntity<List<RegOpcAltro>> getAllRegOpcAltros(RegOpcAltroCriteria criteria) {
        log.debug("REST request to get RegOpcAltros by criteria: {}", criteria);
        List<RegOpcAltro> entityList = regOpcAltroQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }


    @GetMapping("/reg-opc-altro-list")
    public ResponseEntity<List<ListRegOpcAltro>> getListRegOpcIrrigazione(@RequestParam(value = "azienda_id.equals") Long azienda_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<ListRegOpcAltro> result_listregopc = utility.getListRegOpcAltro(azienda_id, dataSource);
        return ResponseEntity.ok().body(result_listregopc);
    }


    /**
    * {@code GET  /reg-opc-altros/count} : count all the regOpcAltros.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-altros/count")
    public ResponseEntity<Long> countRegOpcAltros(RegOpcAltroCriteria criteria) {
        log.debug("REST request to count RegOpcAltros by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcAltroQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-altros/:id} : get the "id" regOpcAltro.
     *
     * @param id the id of the regOpcAltro to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcAltro, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-altros/{id}")
    public ResponseEntity<RegOpcAltro> getRegOpcAltro(@PathVariable Long id) {
        log.debug("REST request to get RegOpcAltro : {}", id);
        Optional<RegOpcAltro> regOpcAltro = regOpcAltroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcAltro);
    }

    /**
     * {@code DELETE  /reg-opc-altros/:id} : delete the "id" regOpcAltro.
     *
     * @param id the id of the regOpcAltro to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-altros/{id}")
    public ResponseEntity<Void> deleteRegOpcAltro(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcAltro : {}", id);
        regOpcAltroService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
