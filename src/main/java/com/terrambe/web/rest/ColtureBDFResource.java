package com.terrambe.web.rest;

import com.terrambe.domain.ColtureBDF;
import com.terrambe.service.ColtureBDFService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.ColtureBDFCriteria;
import com.terrambe.service.ColtureBDFQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.ColtureBDF}.
 */
@RestController
@RequestMapping("/api")
public class ColtureBDFResource {

    private final Logger log = LoggerFactory.getLogger(ColtureBDFResource.class);

    private static final String ENTITY_NAME = "coltureBDF";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ColtureBDFService coltureBDFService;

    private final ColtureBDFQueryService coltureBDFQueryService;

    public ColtureBDFResource(ColtureBDFService coltureBDFService, ColtureBDFQueryService coltureBDFQueryService) {
        this.coltureBDFService = coltureBDFService;
        this.coltureBDFQueryService = coltureBDFQueryService;
    }

    /**
     * {@code POST  /colture-bdfs} : Create a new coltureBDF.
     *
     * @param coltureBDF the coltureBDF to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coltureBDF, or with status {@code 400 (Bad Request)} if the coltureBDF has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/colture-bdfs")
    public ResponseEntity<ColtureBDF> createColtureBDF(@RequestBody ColtureBDF coltureBDF) throws URISyntaxException {
        log.debug("REST request to save ColtureBDF : {}", coltureBDF);
        if (coltureBDF.getId() != null) {
            throw new BadRequestAlertException("A new coltureBDF cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ColtureBDF result = coltureBDFService.save(coltureBDF);
        return ResponseEntity.created(new URI("/api/colture-bdfs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /colture-bdfs} : Updates an existing coltureBDF.
     *
     * @param coltureBDF the coltureBDF to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coltureBDF,
     * or with status {@code 400 (Bad Request)} if the coltureBDF is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coltureBDF couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/colture-bdfs")
    public ResponseEntity<ColtureBDF> updateColtureBDF(@RequestBody ColtureBDF coltureBDF) throws URISyntaxException {
        log.debug("REST request to update ColtureBDF : {}", coltureBDF);
        if (coltureBDF.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ColtureBDF result = coltureBDFService.save(coltureBDF);
        ColtureBDF result2 = coltureBDFService.save(coltureBDF);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, coltureBDF.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /colture-bdfs} : get all the coltureBDFS.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coltureBDFS in body.
     */
    @GetMapping("/colture-bdfs")
    public ResponseEntity<List<ColtureBDF>> getAllColtureBDFS(ColtureBDFCriteria criteria) {
        log.debug("REST request to get ColtureBDFS by criteria: {}", criteria);
        List<ColtureBDF> entityList = coltureBDFQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /colture-bdfs/count} : count all the coltureBDFS.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/colture-bdfs/count")
    public ResponseEntity<Long> countColtureBDFS(ColtureBDFCriteria criteria) {
        log.debug("REST request to count ColtureBDFS by criteria: {}", criteria);
        return ResponseEntity.ok().body(coltureBDFQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /colture-bdfs/:id} : get the "id" coltureBDF.
     *
     * @param id the id of the coltureBDF to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coltureBDF, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/colture-bdfs/{id}")
    public ResponseEntity<ColtureBDF> getColtureBDF(@PathVariable Long id) {
        log.debug("REST request to get ColtureBDF : {}", id);
        Optional<ColtureBDF> coltureBDF = coltureBDFService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coltureBDF);
    }

    /**
     * {@code DELETE  /colture-bdfs/:id} : delete the "id" coltureBDF.
     *
     * @param id the id of the coltureBDF to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/colture-bdfs/{id}")
    public ResponseEntity<Void> deleteColtureBDF(@PathVariable Long id) {
        log.debug("REST request to delete ColtureBDF : {}", id);
        coltureBDFService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
