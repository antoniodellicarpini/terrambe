package com.terrambe.web.rest;

import com.terrambe.domain.TipologiaFito;
import com.terrambe.service.TipologiaFitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipologiaFitoCriteria;
import com.terrambe.service.TipologiaFitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipologiaFito}.
 */
@RestController
@RequestMapping("/api")
public class TipologiaFitoResource {

    private final Logger log = LoggerFactory.getLogger(TipologiaFitoResource.class);

    private static final String ENTITY_NAME = "tipologiaFito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipologiaFitoService tipologiaFitoService;

    private final TipologiaFitoQueryService tipologiaFitoQueryService;

    public TipologiaFitoResource(TipologiaFitoService tipologiaFitoService, TipologiaFitoQueryService tipologiaFitoQueryService) {
        this.tipologiaFitoService = tipologiaFitoService;
        this.tipologiaFitoQueryService = tipologiaFitoQueryService;
    }

    /**
     * {@code POST  /tipologia-fitos} : Create a new tipologiaFito.
     *
     * @param tipologiaFito the tipologiaFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipologiaFito, or with status {@code 400 (Bad Request)} if the tipologiaFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipologia-fitos")
    public ResponseEntity<TipologiaFito> createTipologiaFito(@RequestBody TipologiaFito tipologiaFito) throws URISyntaxException {
        log.debug("REST request to save TipologiaFito : {}", tipologiaFito);
        if (tipologiaFito.getId() != null) {
            throw new BadRequestAlertException("A new tipologiaFito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipologiaFito result = tipologiaFitoService.save(tipologiaFito);
        return ResponseEntity.created(new URI("/api/tipologia-fitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipologia-fitos} : Updates an existing tipologiaFito.
     *
     * @param tipologiaFito the tipologiaFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipologiaFito,
     * or with status {@code 400 (Bad Request)} if the tipologiaFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipologiaFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipologia-fitos")
    public ResponseEntity<TipologiaFito> updateTipologiaFito(@RequestBody TipologiaFito tipologiaFito) throws URISyntaxException {
        log.debug("REST request to update TipologiaFito : {}", tipologiaFito);
        if (tipologiaFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipologiaFito result = tipologiaFitoService.save(tipologiaFito);
        TipologiaFito result2 = tipologiaFitoService.save(tipologiaFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipologiaFito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipologia-fitos} : get all the tipologiaFitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipologiaFitos in body.
     */
    @GetMapping("/tipologia-fitos")
    public ResponseEntity<List<TipologiaFito>> getAllTipologiaFitos(TipologiaFitoCriteria criteria) {
        log.debug("REST request to get TipologiaFitos by criteria: {}", criteria);
        List<TipologiaFito> entityList = tipologiaFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipologia-fitos/count} : count all the tipologiaFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipologia-fitos/count")
    public ResponseEntity<Long> countTipologiaFitos(TipologiaFitoCriteria criteria) {
        log.debug("REST request to count TipologiaFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipologiaFitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipologia-fitos/:id} : get the "id" tipologiaFito.
     *
     * @param id the id of the tipologiaFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipologiaFito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipologia-fitos/{id}")
    public ResponseEntity<TipologiaFito> getTipologiaFito(@PathVariable Long id) {
        log.debug("REST request to get TipologiaFito : {}", id);
        Optional<TipologiaFito> tipologiaFito = tipologiaFitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipologiaFito);
    }

    /**
     * {@code DELETE  /tipologia-fitos/:id} : delete the "id" tipologiaFito.
     *
     * @param id the id of the tipologiaFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipologia-fitos/{id}")
    public ResponseEntity<Void> deleteTipologiaFito(@PathVariable Long id) {
        log.debug("REST request to delete TipologiaFito : {}", id);
        tipologiaFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
