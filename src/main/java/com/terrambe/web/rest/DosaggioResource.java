package com.terrambe.web.rest;

import com.terrambe.domain.Dosaggio;
import com.terrambe.service.DosaggioService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DosaggioCriteria;
import com.terrambe.service.DosaggioQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Dosaggio}.
 */
@RestController
@RequestMapping("/api")
public class DosaggioResource {

    private final Logger log = LoggerFactory.getLogger(DosaggioResource.class);

    private static final String ENTITY_NAME = "dosaggio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DosaggioService dosaggioService;

    private final DosaggioQueryService dosaggioQueryService;

    public DosaggioResource(DosaggioService dosaggioService, DosaggioQueryService dosaggioQueryService) {
        this.dosaggioService = dosaggioService;
        this.dosaggioQueryService = dosaggioQueryService;
    }

    /**
     * {@code POST  /dosaggios} : Create a new dosaggio.
     *
     * @param dosaggio the dosaggio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dosaggio, or with status {@code 400 (Bad Request)} if the dosaggio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dosaggios")
    public ResponseEntity<Dosaggio> createDosaggio(@RequestBody Dosaggio dosaggio) throws URISyntaxException {
        log.debug("REST request to save Dosaggio : {}", dosaggio);
        if (dosaggio.getId() != null) {
            throw new BadRequestAlertException("A new dosaggio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Dosaggio result = dosaggioService.save(dosaggio);
        return ResponseEntity.created(new URI("/api/dosaggios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dosaggios} : Updates an existing dosaggio.
     *
     * @param dosaggio the dosaggio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dosaggio,
     * or with status {@code 400 (Bad Request)} if the dosaggio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dosaggio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dosaggios")
    public ResponseEntity<Dosaggio> updateDosaggio(@RequestBody Dosaggio dosaggio) throws URISyntaxException {
        log.debug("REST request to update Dosaggio : {}", dosaggio);
        if (dosaggio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Dosaggio result = dosaggioService.save(dosaggio);
        Dosaggio result2 = dosaggioService.save(dosaggio);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dosaggio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dosaggios} : get all the dosaggios.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dosaggios in body.
     */
    @GetMapping("/dosaggios")
    public ResponseEntity<List<Dosaggio>> getAllDosaggios(DosaggioCriteria criteria) {
        log.debug("REST request to get Dosaggios by criteria: {}", criteria);
        List<Dosaggio> entityList = dosaggioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /dosaggios/count} : count all the dosaggios.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/dosaggios/count")
    public ResponseEntity<Long> countDosaggios(DosaggioCriteria criteria) {
        log.debug("REST request to count Dosaggios by criteria: {}", criteria);
        return ResponseEntity.ok().body(dosaggioQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dosaggios/:id} : get the "id" dosaggio.
     *
     * @param id the id of the dosaggio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dosaggio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dosaggios/{id}")
    public ResponseEntity<Dosaggio> getDosaggio(@PathVariable Long id) {
        log.debug("REST request to get Dosaggio : {}", id);
        Optional<Dosaggio> dosaggio = dosaggioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dosaggio);
    }

    /**
     * {@code DELETE  /dosaggios/:id} : delete the "id" dosaggio.
     *
     * @param id the id of the dosaggio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dosaggios/{id}")
    public ResponseEntity<Void> deleteDosaggio(@PathVariable Long id) {
        log.debug("REST request to delete Dosaggio : {}", id);
        dosaggioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
