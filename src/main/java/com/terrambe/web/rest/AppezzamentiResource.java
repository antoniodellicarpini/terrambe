package com.terrambe.web.rest;

import com.terrambe.domain.Appezzamenti;
import com.terrambe.service.AppezzamentiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AppezzamentiCriteria;
import com.terrambe.service.AppezzamentiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Appezzamenti}.
 */
@RestController
@RequestMapping("/api")
public class AppezzamentiResource {

    private final Logger log = LoggerFactory.getLogger(AppezzamentiResource.class);

    private static final String ENTITY_NAME = "appezzamenti";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AppezzamentiService appezzamentiService;

    private final AppezzamentiQueryService appezzamentiQueryService;

    public AppezzamentiResource(AppezzamentiService appezzamentiService, AppezzamentiQueryService appezzamentiQueryService) {
        this.appezzamentiService = appezzamentiService;
        this.appezzamentiQueryService = appezzamentiQueryService;
    }

    /**
     * {@code POST  /appezzamentis} : Create a new appezzamenti.
     *
     * @param appezzamenti the appezzamenti to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new appezzamenti, or with status {@code 400 (Bad Request)} if the appezzamenti has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/appezzamentis")
    public ResponseEntity<Appezzamenti> createAppezzamenti(@Valid @RequestBody Appezzamenti appezzamenti) throws URISyntaxException {
        log.debug("REST request to save Appezzamenti : {}", appezzamenti);
        if (appezzamenti.getId() != null) {
            throw new BadRequestAlertException("A new appezzamenti cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Appezzamenti result = appezzamentiService.save(appezzamenti);
        return ResponseEntity.created(new URI("/api/appezzamentis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /appezzamentis} : Updates an existing appezzamenti.
     *
     * @param appezzamenti the appezzamenti to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated appezzamenti,
     * or with status {@code 400 (Bad Request)} if the appezzamenti is not valid,
     * or with status {@code 500 (Internal Server Error)} if the appezzamenti couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/appezzamentis")
    public ResponseEntity<Appezzamenti> updateAppezzamenti(@Valid @RequestBody Appezzamenti appezzamenti) throws URISyntaxException {
        log.debug("REST request to update Appezzamenti : {}", appezzamenti);
        if (appezzamenti.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Appezzamenti result = appezzamentiService.save(appezzamenti);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, appezzamenti.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /appezzamentis} : get all the appezzamentis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of appezzamentis in body.
     */
    @GetMapping("/appezzamentis")
    public ResponseEntity<List<Appezzamenti>> getAllAppezzamentis(AppezzamentiCriteria criteria) {
        log.debug("REST request to get Appezzamentis by criteria: {}", criteria);
        List<Appezzamenti> entityList = appezzamentiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /appezzamentis/count} : count all the appezzamentis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/appezzamentis/count")
    public ResponseEntity<Long> countAppezzamentis(AppezzamentiCriteria criteria) {
        log.debug("REST request to count Appezzamentis by criteria: {}", criteria);
        return ResponseEntity.ok().body(appezzamentiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /appezzamentis/:id} : get the "id" appezzamenti.
     *
     * @param id the id of the appezzamenti to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the appezzamenti, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/appezzamentis/{id}")
    public ResponseEntity<Appezzamenti> getAppezzamenti(@PathVariable Long id) {
        log.debug("REST request to get Appezzamenti : {}", id);
        Optional<Appezzamenti> appezzamenti = appezzamentiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(appezzamenti);
    }

    /**
     * {@code DELETE  /appezzamentis/:id} : delete the "id" appezzamenti.
     *
     * @param id the id of the appezzamenti to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/appezzamentis/{id}")
    public ResponseEntity<Void> deleteAppezzamenti(@PathVariable Long id) {
        log.debug("REST request to delete Appezzamenti : {}", id);
        appezzamentiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
