package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcTrapAppz;
import com.terrambe.service.RegOpcTrapAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcTrapAppzCriteria;
import com.terrambe.service.RegOpcTrapAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcTrapAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcTrapAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapAppzResource.class);

    private static final String ENTITY_NAME = "regOpcTrapAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcTrapAppzService regOpcTrapAppzService;

    private final RegOpcTrapAppzQueryService regOpcTrapAppzQueryService;

    public RegOpcTrapAppzResource(RegOpcTrapAppzService regOpcTrapAppzService, RegOpcTrapAppzQueryService regOpcTrapAppzQueryService) {
        this.regOpcTrapAppzService = regOpcTrapAppzService;
        this.regOpcTrapAppzQueryService = regOpcTrapAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-trap-appzs} : Create a new regOpcTrapAppz.
     *
     * @param regOpcTrapAppz the regOpcTrapAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcTrapAppz, or with status {@code 400 (Bad Request)} if the regOpcTrapAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-trap-appzs")
    public ResponseEntity<RegOpcTrapAppz> createRegOpcTrapAppz(@Valid @RequestBody RegOpcTrapAppz regOpcTrapAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcTrapAppz : {}", regOpcTrapAppz);
        if (regOpcTrapAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcTrapAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcTrapAppz result = regOpcTrapAppzService.save(regOpcTrapAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-trap-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-trap-appzs} : Updates an existing regOpcTrapAppz.
     *
     * @param regOpcTrapAppz the regOpcTrapAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcTrapAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcTrapAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcTrapAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-trap-appzs")
    public ResponseEntity<RegOpcTrapAppz> updateRegOpcTrapAppz(@Valid @RequestBody RegOpcTrapAppz regOpcTrapAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcTrapAppz : {}", regOpcTrapAppz);
        if (regOpcTrapAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcTrapAppz result = regOpcTrapAppzService.save(regOpcTrapAppz);
        RegOpcTrapAppz result2 = regOpcTrapAppzService.save(regOpcTrapAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcTrapAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-trap-appzs} : get all the regOpcTrapAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcTrapAppzs in body.
     */
    @GetMapping("/reg-opc-trap-appzs")
    public ResponseEntity<List<RegOpcTrapAppz>> getAllRegOpcTrapAppzs(RegOpcTrapAppzCriteria criteria) {
        log.debug("REST request to get RegOpcTrapAppzs by criteria: {}", criteria);
        List<RegOpcTrapAppz> entityList = regOpcTrapAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-trap-appzs/count} : count all the regOpcTrapAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-trap-appzs/count")
    public ResponseEntity<Long> countRegOpcTrapAppzs(RegOpcTrapAppzCriteria criteria) {
        log.debug("REST request to count RegOpcTrapAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcTrapAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-trap-appzs/:id} : get the "id" regOpcTrapAppz.
     *
     * @param id the id of the regOpcTrapAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcTrapAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-trap-appzs/{id}")
    public ResponseEntity<RegOpcTrapAppz> getRegOpcTrapAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcTrapAppz : {}", id);
        Optional<RegOpcTrapAppz> regOpcTrapAppz = regOpcTrapAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcTrapAppz);
    }

    /**
     * {@code DELETE  /reg-opc-trap-appzs/:id} : delete the "id" regOpcTrapAppz.
     *
     * @param id the id of the regOpcTrapAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-trap-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcTrapAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcTrapAppz : {}", id);
        regOpcTrapAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
