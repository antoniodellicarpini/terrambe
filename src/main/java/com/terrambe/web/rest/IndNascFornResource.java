package com.terrambe.web.rest;

import com.terrambe.domain.IndNascForn;
import com.terrambe.service.IndNascFornService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndNascFornCriteria;
import com.terrambe.service.IndNascFornQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndNascForn}.
 */
@RestController
@RequestMapping("/api")
public class IndNascFornResource {

    private final Logger log = LoggerFactory.getLogger(IndNascFornResource.class);

    private static final String ENTITY_NAME = "indNascForn";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndNascFornService indNascFornService;

    private final IndNascFornQueryService indNascFornQueryService;

    public IndNascFornResource(IndNascFornService indNascFornService, IndNascFornQueryService indNascFornQueryService) {
        this.indNascFornService = indNascFornService;
        this.indNascFornQueryService = indNascFornQueryService;
    }

    /**
     * {@code POST  /ind-nasc-forns} : Create a new indNascForn.
     *
     * @param indNascForn the indNascForn to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indNascForn, or with status {@code 400 (Bad Request)} if the indNascForn has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-nasc-forns")
    public ResponseEntity<IndNascForn> createIndNascForn(@Valid @RequestBody IndNascForn indNascForn) throws URISyntaxException {
        log.debug("REST request to save IndNascForn : {}", indNascForn);
        if (indNascForn.getId() != null) {
            throw new BadRequestAlertException("A new indNascForn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndNascForn result = indNascFornService.save(indNascForn);
        return ResponseEntity.created(new URI("/api/ind-nasc-forns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-nasc-forns} : Updates an existing indNascForn.
     *
     * @param indNascForn the indNascForn to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indNascForn,
     * or with status {@code 400 (Bad Request)} if the indNascForn is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indNascForn couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-nasc-forns")
    public ResponseEntity<IndNascForn> updateIndNascForn(@Valid @RequestBody IndNascForn indNascForn) throws URISyntaxException {
        log.debug("REST request to update IndNascForn : {}", indNascForn);
        if (indNascForn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndNascForn result = indNascFornService.save(indNascForn);
        IndNascForn result2 = indNascFornService.save(indNascForn);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indNascForn.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-nasc-forns} : get all the indNascForns.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indNascForns in body.
     */
    @GetMapping("/ind-nasc-forns")
    public ResponseEntity<List<IndNascForn>> getAllIndNascForns(IndNascFornCriteria criteria, Pageable pageable) {
        log.debug("REST request to get IndNascForns by criteria: {}", criteria);
        Page<IndNascForn> page = indNascFornQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /ind-nasc-forns/count} : count all the indNascForns.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-nasc-forns/count")
    public ResponseEntity<Long> countIndNascForns(IndNascFornCriteria criteria) {
        log.debug("REST request to count IndNascForns by criteria: {}", criteria);
        return ResponseEntity.ok().body(indNascFornQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-nasc-forns/:id} : get the "id" indNascForn.
     *
     * @param id the id of the indNascForn to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indNascForn, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-nasc-forns/{id}")
    public ResponseEntity<IndNascForn> getIndNascForn(@PathVariable Long id) {
        log.debug("REST request to get IndNascForn : {}", id);
        Optional<IndNascForn> indNascForn = indNascFornService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indNascForn);
    }

    /**
     * {@code DELETE  /ind-nasc-forns/:id} : delete the "id" indNascForn.
     *
     * @param id the id of the indNascForn to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-nasc-forns/{id}")
    public ResponseEntity<Void> deleteIndNascForn(@PathVariable Long id) {
        log.debug("REST request to delete IndNascForn : {}", id);
        indNascFornService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
