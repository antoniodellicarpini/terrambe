package com.terrambe.web.rest;

import com.terrambe.domain.IndForn;
import com.terrambe.service.IndFornService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndFornCriteria;
import com.terrambe.service.IndFornQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndForn}.
 */
@RestController
@RequestMapping("/api")
public class IndFornResource {

    private final Logger log = LoggerFactory.getLogger(IndFornResource.class);

    private static final String ENTITY_NAME = "indForn";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndFornService indFornService;

    private final IndFornQueryService indFornQueryService;

    public IndFornResource(IndFornService indFornService, IndFornQueryService indFornQueryService) {
        this.indFornService = indFornService;
        this.indFornQueryService = indFornQueryService;
    }

    /**
     * {@code POST  /ind-forns} : Create a new indForn.
     *
     * @param indForn the indForn to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indForn, or with status {@code 400 (Bad Request)} if the indForn has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-forns")
    public ResponseEntity<IndForn> createIndForn(@Valid @RequestBody IndForn indForn) throws URISyntaxException {
        log.debug("REST request to save IndForn : {}", indForn);
        if (indForn.getId() != null) {
            throw new BadRequestAlertException("A new indForn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndForn result = indFornService.save(indForn);
        return ResponseEntity.created(new URI("/api/ind-forns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-forns} : Updates an existing indForn.
     *
     * @param indForn the indForn to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indForn,
     * or with status {@code 400 (Bad Request)} if the indForn is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indForn couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-forns")
    public ResponseEntity<IndForn> updateIndForn(@Valid @RequestBody IndForn indForn) throws URISyntaxException {
        log.debug("REST request to update IndForn : {}", indForn);
        if (indForn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndForn result = indFornService.save(indForn);
        IndForn result2 = indFornService.save(indForn);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indForn.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-forns} : get all the indForns.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indForns in body.
     */
    @GetMapping("/ind-forns")
    public ResponseEntity<List<IndForn>> getAllIndForns(IndFornCriteria criteria) {
        log.debug("REST request to get IndForns by criteria: {}", criteria);
        List<IndForn> entityList = indFornQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-forns/count} : count all the indForns.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-forns/count")
    public ResponseEntity<Long> countIndForns(IndFornCriteria criteria) {
        log.debug("REST request to count IndForns by criteria: {}", criteria);
        return ResponseEntity.ok().body(indFornQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-forns/:id} : get the "id" indForn.
     *
     * @param id the id of the indForn to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indForn, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-forns/{id}")
    public ResponseEntity<IndForn> getIndForn(@PathVariable Long id) {
        log.debug("REST request to get IndForn : {}", id);
        Optional<IndForn> indForn = indFornService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indForn);
    }

    /**
     * {@code DELETE  /ind-forns/:id} : delete the "id" indForn.
     *
     * @param id the id of the indForn to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-forns/{id}")
    public ResponseEntity<Void> deleteIndForn(@PathVariable Long id) {
        log.debug("REST request to delete IndForn : {}", id);
        indFornService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
