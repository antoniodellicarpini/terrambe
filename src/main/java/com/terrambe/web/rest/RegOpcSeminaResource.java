package com.terrambe.web.rest;

import com.terrambe.domain.*;
import com.terrambe.service.*;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcSeminaCriteria;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcSemina}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcSeminaResource {

    @Autowired
    private DataSource dataSource;

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaResource.class);

    private static final String ENTITY_NAME = "regOpcSemina";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcSeminaService regOpcSeminaService;

    private final RegOpcSeminaQueryService regOpcSeminaQueryService;

    @Autowired
    private Agea20152020MatriceService ageaMatServ;

    @Autowired
    private AtrAnagraficaService atrServ;

    @Autowired
    private OpeAnagraficaService opeAnServ;

    public RegOpcSeminaResource(RegOpcSeminaService regOpcSeminaService, RegOpcSeminaQueryService regOpcSeminaQueryService) {
        this.regOpcSeminaService = regOpcSeminaService;
        this.regOpcSeminaQueryService = regOpcSeminaQueryService;
    }

    /**
     * {@code POST  /reg-opc-seminas} : Create a new regOpcSemina.
     *
     * @param regOpcSemina the regOpcSemina to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcSemina, or with status {@code 400 (Bad Request)} if the regOpcSemina has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-seminas")
    public ResponseEntity<RegOpcSemina> createRegOpcSemina(@Valid @RequestBody RegOpcSemina regOpcSemina) throws URISyntaxException {
        log.debug("REST request to save RegOpcSemina : {}", regOpcSemina);
        if (regOpcSemina.getId() != null) {
            throw new BadRequestAlertException("A new regOpcSemina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcSemina result = regOpcSeminaService.save(regOpcSemina);
        return ResponseEntity.created(new URI("/api/reg-opc-seminas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-seminas} : Updates an existing regOpcSemina.
     *
     * @param regOpcSemina the regOpcSemina to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcSemina,
     * or with status {@code 400 (Bad Request)} if the regOpcSemina is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcSemina couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-seminas")
    public ResponseEntity<RegOpcSemina> updateRegOpcSemina(@Valid @RequestBody RegOpcSemina regOpcSemina) throws URISyntaxException {
        log.debug("REST request to update RegOpcSemina : {}", regOpcSemina);
        if (regOpcSemina.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcSemina result = regOpcSeminaService.save(regOpcSemina);
        RegOpcSemina result2 = regOpcSeminaService.save(regOpcSemina);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcSemina.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-seminas} : get all the regOpcSeminas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcSeminas in body.
     */
    @GetMapping("/reg-opc-seminas")
    public ResponseEntity<List<RegOpcSemina>> getAllRegOpcSeminas(RegOpcSeminaCriteria criteria) {
        log.debug("REST request to get RegOpcSeminas by criteria: {}", criteria);
        List<RegOpcSemina> entityList = regOpcSeminaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    @GetMapping("/reg-opc-semina-list")
    public ResponseEntity<List<ListRegOpcSemina>> getListRegOpcSemina(@RequestParam(value = "azienda_id.equals") Long azienda_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<ListRegOpcSemina> result_listregopc = utility.getListRegOpcSemina(azienda_id, dataSource);
        return ResponseEntity.ok().body(result_listregopc);
    }


    /**
     * {@code GET  /reg-opc-seminas} : get all the regOpcSeminas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcSeminas in body.
     */
    // @GetMapping("/reg-opc-seminas-spec")
    // public ResponseEntity<List<RegOpcSpec>> getAllRegOpcSeminasSpec(RegOpcSeminaCriteria criteria) {
    //     log.debug("REST request to get RegOpcSeminas by criteria: {}", criteria);
    //     List<RegOpcSemina> entityList = regOpcSeminaQueryService.findByCriteria(criteria);

    //     List<RegOpcSpec> regOpcSemina = new ArrayList<>();

    //     for (RegOpcSemina entity: entityList){

    //         RegOpcSpec spec = new RegOpcSpec();
    //         spec.setId(entity.getId());
    //         spec.setDataInizOpc(entity.getDataInizOpc());
    //         spec.setTipoTratt(entity.getOpcSeminaToTipo().getDescrizione());
    //         //ottengo i dati dal dal db
    //         Optional<Agea20152020Matrice> ageMat = ageaMatServ.findOne(entity.getIdColtura());
    //         spec.setDescColtura(ageMat.get().getDescColtura());

    //         Optional<OpeAnagrafica> ope = opeAnServ.findOne(entity.getIdOperatore());
    //         spec.setNominativoOperatore(ope.get().getCognome() + " " + ope.get().getNome());

    //         Optional<AtrAnagrafica> atr = atrServ.findOne(entity.getIdMezzo());
    //         spec.setDescMezzo(atr.get().getModello());

    //         regOpcSemina.add(spec);

    //     }

    //     return ResponseEntity.ok().body(regOpcSemina);
    // }

    /**
    * {@code GET  /reg-opc-seminas/count} : count all the regOpcSeminas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-seminas/count")
    public ResponseEntity<Long> countRegOpcSeminas(RegOpcSeminaCriteria criteria) {
        log.debug("REST request to count RegOpcSeminas by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcSeminaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-seminas/:id} : get the "id" regOpcSemina.
     *
     * @param id the id of the regOpcSemina to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcSemina, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-seminas/{id}")
    public ResponseEntity<RegOpcSemina> getRegOpcSemina(@PathVariable Long id) {
        log.debug("REST request to get RegOpcSemina : {}", id);
        Optional<RegOpcSemina> regOpcSemina = regOpcSeminaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcSemina);
    }

    /**
     * {@code DELETE  /reg-opc-seminas/:id} : delete the "id" regOpcSemina.
     *
     * @param id the id of the regOpcSemina to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-seminas/{id}")
    public ResponseEntity<Void> deleteRegOpcSemina(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcSemina : {}", id);
        regOpcSeminaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
