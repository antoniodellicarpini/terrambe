package com.terrambe.web.rest;

import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.service.OpePatentinoTipologiaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.OpePatentinoTipologiaCriteria;
import com.terrambe.service.OpePatentinoTipologiaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.OpePatentinoTipologia}.
 */
@RestController
@RequestMapping("/api")
public class OpePatentinoTipologiaResource {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoTipologiaResource.class);

    private static final String ENTITY_NAME = "opePatentinoTipologia";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OpePatentinoTipologiaService opePatentinoTipologiaService;

    private final OpePatentinoTipologiaQueryService opePatentinoTipologiaQueryService;

    public OpePatentinoTipologiaResource(OpePatentinoTipologiaService opePatentinoTipologiaService, OpePatentinoTipologiaQueryService opePatentinoTipologiaQueryService) {
        this.opePatentinoTipologiaService = opePatentinoTipologiaService;
        this.opePatentinoTipologiaQueryService = opePatentinoTipologiaQueryService;
    }

    /**
     * {@code POST  /ope-patentino-tipologias} : Create a new opePatentinoTipologia.
     *
     * @param opePatentinoTipologia the opePatentinoTipologia to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new opePatentinoTipologia, or with status {@code 400 (Bad Request)} if the opePatentinoTipologia has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ope-patentino-tipologias")
    public ResponseEntity<OpePatentinoTipologia> createOpePatentinoTipologia(@Valid @RequestBody OpePatentinoTipologia opePatentinoTipologia) throws URISyntaxException {
        log.debug("REST request to save OpePatentinoTipologia : {}", opePatentinoTipologia);
        if (opePatentinoTipologia.getId() != null) {
            throw new BadRequestAlertException("A new opePatentinoTipologia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpePatentinoTipologia result = opePatentinoTipologiaService.save(opePatentinoTipologia);
        return ResponseEntity.created(new URI("/api/ope-patentino-tipologias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ope-patentino-tipologias} : Updates an existing opePatentinoTipologia.
     *
     * @param opePatentinoTipologia the opePatentinoTipologia to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated opePatentinoTipologia,
     * or with status {@code 400 (Bad Request)} if the opePatentinoTipologia is not valid,
     * or with status {@code 500 (Internal Server Error)} if the opePatentinoTipologia couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ope-patentino-tipologias")
    public ResponseEntity<OpePatentinoTipologia> updateOpePatentinoTipologia(@Valid @RequestBody OpePatentinoTipologia opePatentinoTipologia) throws URISyntaxException {
        log.debug("REST request to update OpePatentinoTipologia : {}", opePatentinoTipologia);
        if (opePatentinoTipologia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OpePatentinoTipologia result = opePatentinoTipologiaService.save(opePatentinoTipologia);
        OpePatentinoTipologia result2 = opePatentinoTipologiaService.save(opePatentinoTipologia);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, opePatentinoTipologia.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ope-patentino-tipologias} : get all the opePatentinoTipologias.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of opePatentinoTipologias in body.
     */
    @GetMapping("/ope-patentino-tipologias")
    public ResponseEntity<List<OpePatentinoTipologia>> getAllOpePatentinoTipologias(OpePatentinoTipologiaCriteria criteria) {
        log.debug("REST request to get OpePatentinoTipologias by criteria: {}", criteria);
        List<OpePatentinoTipologia> entityList = opePatentinoTipologiaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ope-patentino-tipologias/count} : count all the opePatentinoTipologias.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ope-patentino-tipologias/count")
    public ResponseEntity<Long> countOpePatentinoTipologias(OpePatentinoTipologiaCriteria criteria) {
        log.debug("REST request to count OpePatentinoTipologias by criteria: {}", criteria);
        return ResponseEntity.ok().body(opePatentinoTipologiaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ope-patentino-tipologias/:id} : get the "id" opePatentinoTipologia.
     *
     * @param id the id of the opePatentinoTipologia to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the opePatentinoTipologia, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ope-patentino-tipologias/{id}")
    public ResponseEntity<OpePatentinoTipologia> getOpePatentinoTipologia(@PathVariable Long id) {
        log.debug("REST request to get OpePatentinoTipologia : {}", id);
        Optional<OpePatentinoTipologia> opePatentinoTipologia = opePatentinoTipologiaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(opePatentinoTipologia);
    }

    /**
     * {@code DELETE  /ope-patentino-tipologias/:id} : delete the "id" opePatentinoTipologia.
     *
     * @param id the id of the opePatentinoTipologia to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ope-patentino-tipologias/{id}")
    public ResponseEntity<Void> deleteOpePatentinoTipologia(@PathVariable Long id) {
        log.debug("REST request to delete OpePatentinoTipologia : {}", id);
        opePatentinoTipologiaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
