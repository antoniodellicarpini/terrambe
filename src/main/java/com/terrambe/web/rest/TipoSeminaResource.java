package com.terrambe.web.rest;

import com.terrambe.domain.TipoSemina;
import com.terrambe.service.TipoSeminaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoSeminaCriteria;
import com.terrambe.service.TipoSeminaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoSemina}.
 */
@RestController
@RequestMapping("/api")
public class TipoSeminaResource {

    private final Logger log = LoggerFactory.getLogger(TipoSeminaResource.class);

    private static final String ENTITY_NAME = "tipoSemina";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoSeminaService tipoSeminaService;

    private final TipoSeminaQueryService tipoSeminaQueryService;

    public TipoSeminaResource(TipoSeminaService tipoSeminaService, TipoSeminaQueryService tipoSeminaQueryService) {
        this.tipoSeminaService = tipoSeminaService;
        this.tipoSeminaQueryService = tipoSeminaQueryService;
    }

    /**
     * {@code POST  /tipo-seminas} : Create a new tipoSemina.
     *
     * @param tipoSemina the tipoSemina to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoSemina, or with status {@code 400 (Bad Request)} if the tipoSemina has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-seminas")
    public ResponseEntity<TipoSemina> createTipoSemina(@RequestBody TipoSemina tipoSemina) throws URISyntaxException {
        log.debug("REST request to save TipoSemina : {}", tipoSemina);
        if (tipoSemina.getId() != null) {
            throw new BadRequestAlertException("A new tipoSemina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoSemina result = tipoSeminaService.save(tipoSemina);
        return ResponseEntity.created(new URI("/api/tipo-seminas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-seminas} : Updates an existing tipoSemina.
     *
     * @param tipoSemina the tipoSemina to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoSemina,
     * or with status {@code 400 (Bad Request)} if the tipoSemina is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoSemina couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-seminas")
    public ResponseEntity<TipoSemina> updateTipoSemina(@RequestBody TipoSemina tipoSemina) throws URISyntaxException {
        log.debug("REST request to update TipoSemina : {}", tipoSemina);
        if (tipoSemina.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoSemina result = tipoSeminaService.save(tipoSemina);
        TipoSemina result2 = tipoSeminaService.save(tipoSemina);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoSemina.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-seminas} : get all the tipoSeminas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoSeminas in body.
     */
    @GetMapping("/tipo-seminas")
    public ResponseEntity<List<TipoSemina>> getAllTipoSeminas(TipoSeminaCriteria criteria) {
        log.debug("REST request to get TipoSeminas by criteria: {}", criteria);
        List<TipoSemina> entityList = tipoSeminaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-seminas/count} : count all the tipoSeminas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-seminas/count")
    public ResponseEntity<Long> countTipoSeminas(TipoSeminaCriteria criteria) {
        log.debug("REST request to count TipoSeminas by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoSeminaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-seminas/:id} : get the "id" tipoSemina.
     *
     * @param id the id of the tipoSemina to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoSemina, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-seminas/{id}")
    public ResponseEntity<TipoSemina> getTipoSemina(@PathVariable Long id) {
        log.debug("REST request to get TipoSemina : {}", id);
        Optional<TipoSemina> tipoSemina = tipoSeminaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoSemina);
    }

    /**
     * {@code DELETE  /tipo-seminas/:id} : delete the "id" tipoSemina.
     *
     * @param id the id of the tipoSemina to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-seminas/{id}")
    public ResponseEntity<Void> deleteTipoSemina(@PathVariable Long id) {
        log.debug("REST request to delete TipoSemina : {}", id);
        tipoSeminaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
