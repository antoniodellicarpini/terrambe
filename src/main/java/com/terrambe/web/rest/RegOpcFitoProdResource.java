package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcFitoProd;
import com.terrambe.service.RegOpcFitoProdService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcFitoProdCriteria;
import com.terrambe.service.RegOpcFitoProdQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcFitoProd}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcFitoProdResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoProdResource.class);

    private static final String ENTITY_NAME = "regOpcFitoProd";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcFitoProdService regOpcFitoProdService;

    private final RegOpcFitoProdQueryService regOpcFitoProdQueryService;

    public RegOpcFitoProdResource(RegOpcFitoProdService regOpcFitoProdService, RegOpcFitoProdQueryService regOpcFitoProdQueryService) {
        this.regOpcFitoProdService = regOpcFitoProdService;
        this.regOpcFitoProdQueryService = regOpcFitoProdQueryService;
    }

    /**
     * {@code POST  /reg-opc-fito-prods} : Create a new regOpcFitoProd.
     *
     * @param regOpcFitoProd the regOpcFitoProd to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcFitoProd, or with status {@code 400 (Bad Request)} if the regOpcFitoProd has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-fito-prods")
    public ResponseEntity<RegOpcFitoProd> createRegOpcFitoProd(@Valid @RequestBody RegOpcFitoProd regOpcFitoProd) throws URISyntaxException {
        log.debug("REST request to save RegOpcFitoProd : {}", regOpcFitoProd);
        if (regOpcFitoProd.getId() != null) {
            throw new BadRequestAlertException("A new regOpcFitoProd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcFitoProd result = regOpcFitoProdService.save(regOpcFitoProd);
        return ResponseEntity.created(new URI("/api/reg-opc-fito-prods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-fito-prods} : Updates an existing regOpcFitoProd.
     *
     * @param regOpcFitoProd the regOpcFitoProd to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcFitoProd,
     * or with status {@code 400 (Bad Request)} if the regOpcFitoProd is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcFitoProd couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-fito-prods")
    public ResponseEntity<RegOpcFitoProd> updateRegOpcFitoProd(@Valid @RequestBody RegOpcFitoProd regOpcFitoProd) throws URISyntaxException {
        log.debug("REST request to update RegOpcFitoProd : {}", regOpcFitoProd);
        if (regOpcFitoProd.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcFitoProd result = regOpcFitoProdService.save(regOpcFitoProd);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcFitoProd.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-fito-prods} : get all the regOpcFitoProds.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcFitoProds in body.
     */
    @GetMapping("/reg-opc-fito-prods")
    public ResponseEntity<List<RegOpcFitoProd>> getAllRegOpcFitoProds(RegOpcFitoProdCriteria criteria) {
        log.debug("REST request to get RegOpcFitoProds by criteria: {}", criteria);
        List<RegOpcFitoProd> entityList = regOpcFitoProdQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-fito-prods/count} : count all the regOpcFitoProds.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-fito-prods/count")
    public ResponseEntity<Long> countRegOpcFitoProds(RegOpcFitoProdCriteria criteria) {
        log.debug("REST request to count RegOpcFitoProds by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcFitoProdQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-fito-prods/:id} : get the "id" regOpcFitoProd.
     *
     * @param id the id of the regOpcFitoProd to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcFitoProd, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-fito-prods/{id}")
    public ResponseEntity<RegOpcFitoProd> getRegOpcFitoProd(@PathVariable Long id) {
        log.debug("REST request to get RegOpcFitoProd : {}", id);
        Optional<RegOpcFitoProd> regOpcFitoProd = regOpcFitoProdService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcFitoProd);
    }

    /**
     * {@code DELETE  /reg-opc-fito-prods/:id} : delete the "id" regOpcFitoProd.
     *
     * @param id the id of the regOpcFitoProd to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-fito-prods/{id}")
    public ResponseEntity<Void> deleteRegOpcFitoProd(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcFitoProd : {}", id);
        regOpcFitoProdService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
