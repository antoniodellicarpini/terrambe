package com.terrambe.web.rest;

import com.terrambe.domain.PermessiOperatore;
import com.terrambe.service.PermessiOperatoreService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.PermessiOperatoreCriteria;
import com.terrambe.service.PermessiOperatoreQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.PermessiOperatore}.
 */
@RestController
@RequestMapping("/api")
public class PermessiOperatoreResource {

    private final Logger log = LoggerFactory.getLogger(PermessiOperatoreResource.class);

    private static final String ENTITY_NAME = "permessiOperatore";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PermessiOperatoreService permessiOperatoreService;

    private final PermessiOperatoreQueryService permessiOperatoreQueryService;

    public PermessiOperatoreResource(PermessiOperatoreService permessiOperatoreService, PermessiOperatoreQueryService permessiOperatoreQueryService) {
        this.permessiOperatoreService = permessiOperatoreService;
        this.permessiOperatoreQueryService = permessiOperatoreQueryService;
    }

    /**
     * {@code POST  /permessi-operatores} : Create a new permessiOperatore.
     *
     * @param permessiOperatore the permessiOperatore to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new permessiOperatore, or with status {@code 400 (Bad Request)} if the permessiOperatore has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/permessi-operatores")
    public ResponseEntity<PermessiOperatore> createPermessiOperatore(@Valid @RequestBody PermessiOperatore permessiOperatore) throws URISyntaxException {
        log.debug("REST request to save PermessiOperatore : {}", permessiOperatore);
        if (permessiOperatore.getId() != null) {
            throw new BadRequestAlertException("A new permessiOperatore cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PermessiOperatore result = permessiOperatoreService.save(permessiOperatore);
        return ResponseEntity.created(new URI("/api/permessi-operatores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /permessi-operatores} : Updates an existing permessiOperatore.
     *
     * @param permessiOperatore the permessiOperatore to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated permessiOperatore,
     * or with status {@code 400 (Bad Request)} if the permessiOperatore is not valid,
     * or with status {@code 500 (Internal Server Error)} if the permessiOperatore couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/permessi-operatores")
    public ResponseEntity<PermessiOperatore> updatePermessiOperatore(@Valid @RequestBody PermessiOperatore permessiOperatore) throws URISyntaxException {
        log.debug("REST request to update PermessiOperatore : {}", permessiOperatore);
        if (permessiOperatore.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PermessiOperatore result = permessiOperatoreService.save(permessiOperatore);
        PermessiOperatore result2 = permessiOperatoreService.save(permessiOperatore);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, permessiOperatore.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /permessi-operatores} : get all the permessiOperatores.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of permessiOperatores in body.
     */
    @GetMapping("/permessi-operatores")
    public ResponseEntity<List<PermessiOperatore>> getAllPermessiOperatores(PermessiOperatoreCriteria criteria) {
        log.debug("REST request to get PermessiOperatores by criteria: {}", criteria);
        List<PermessiOperatore> entityList = permessiOperatoreQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /permessi-operatores/count} : count all the permessiOperatores.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/permessi-operatores/count")
    public ResponseEntity<Long> countPermessiOperatores(PermessiOperatoreCriteria criteria) {
        log.debug("REST request to count PermessiOperatores by criteria: {}", criteria);
        return ResponseEntity.ok().body(permessiOperatoreQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /permessi-operatores/:id} : get the "id" permessiOperatore.
     *
     * @param id the id of the permessiOperatore to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the permessiOperatore, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/permessi-operatores/{id}")
    public ResponseEntity<PermessiOperatore> getPermessiOperatore(@PathVariable Long id) {
        log.debug("REST request to get PermessiOperatore : {}", id);
        Optional<PermessiOperatore> permessiOperatore = permessiOperatoreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(permessiOperatore);
    }

    /**
     * {@code DELETE  /permessi-operatores/:id} : delete the "id" permessiOperatore.
     *
     * @param id the id of the permessiOperatore to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/permessi-operatores/{id}")
    public ResponseEntity<Void> deletePermessiOperatore(@PathVariable Long id) {
        log.debug("REST request to delete PermessiOperatore : {}", id);
        permessiOperatoreService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
