package com.terrambe.web.rest;

import com.terrambe.domain.IndMagUbi;
import com.terrambe.service.IndMagUbiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndMagUbiCriteria;
import com.terrambe.service.IndMagUbiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndMagUbi}.
 */
@RestController
@RequestMapping("/api")
public class IndMagUbiResource {

    private final Logger log = LoggerFactory.getLogger(IndMagUbiResource.class);

    private static final String ENTITY_NAME = "indMagUbi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndMagUbiService indMagUbiService;

    private final IndMagUbiQueryService indMagUbiQueryService;

    public IndMagUbiResource(IndMagUbiService indMagUbiService, IndMagUbiQueryService indMagUbiQueryService) {
        this.indMagUbiService = indMagUbiService;
        this.indMagUbiQueryService = indMagUbiQueryService;
    }

    /**
     * {@code POST  /ind-mag-ubis} : Create a new indMagUbi.
     *
     * @param indMagUbi the indMagUbi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indMagUbi, or with status {@code 400 (Bad Request)} if the indMagUbi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-mag-ubis")
    public ResponseEntity<IndMagUbi> createIndMagUbi(@Valid @RequestBody IndMagUbi indMagUbi) throws URISyntaxException {
        log.debug("REST request to save IndMagUbi : {}", indMagUbi);
        if (indMagUbi.getId() != null) {
            throw new BadRequestAlertException("A new indMagUbi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndMagUbi result = indMagUbiService.save(indMagUbi);
        return ResponseEntity.created(new URI("/api/ind-mag-ubis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-mag-ubis} : Updates an existing indMagUbi.
     *
     * @param indMagUbi the indMagUbi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indMagUbi,
     * or with status {@code 400 (Bad Request)} if the indMagUbi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indMagUbi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-mag-ubis")
    public ResponseEntity<IndMagUbi> updateIndMagUbi(@Valid @RequestBody IndMagUbi indMagUbi) throws URISyntaxException {
        log.debug("REST request to update IndMagUbi : {}", indMagUbi);
        if (indMagUbi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndMagUbi result = indMagUbiService.save(indMagUbi);
        IndMagUbi result2 = indMagUbiService.save(indMagUbi);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indMagUbi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-mag-ubis} : get all the indMagUbis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indMagUbis in body.
     */
    @GetMapping("/ind-mag-ubis")
    public ResponseEntity<List<IndMagUbi>> getAllIndMagUbis(IndMagUbiCriteria criteria) {
        log.debug("REST request to get IndMagUbis by criteria: {}", criteria);
        List<IndMagUbi> entityList = indMagUbiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-mag-ubis/count} : count all the indMagUbis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-mag-ubis/count")
    public ResponseEntity<Long> countIndMagUbis(IndMagUbiCriteria criteria) {
        log.debug("REST request to count IndMagUbis by criteria: {}", criteria);
        return ResponseEntity.ok().body(indMagUbiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-mag-ubis/:id} : get the "id" indMagUbi.
     *
     * @param id the id of the indMagUbi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indMagUbi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-mag-ubis/{id}")
    public ResponseEntity<IndMagUbi> getIndMagUbi(@PathVariable Long id) {
        log.debug("REST request to get IndMagUbi : {}", id);
        Optional<IndMagUbi> indMagUbi = indMagUbiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indMagUbi);
    }

    /**
     * {@code DELETE  /ind-mag-ubis/:id} : delete the "id" indMagUbi.
     *
     * @param id the id of the indMagUbi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-mag-ubis/{id}")
    public ResponseEntity<Void> deleteIndMagUbi(@PathVariable Long id) {
        log.debug("REST request to delete IndMagUbi : {}", id);
        indMagUbiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
