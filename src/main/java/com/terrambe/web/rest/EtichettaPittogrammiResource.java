package com.terrambe.web.rest;

import com.terrambe.domain.EtichettaPittogrammi;
import com.terrambe.service.EtichettaPittogrammiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.EtichettaPittogrammiCriteria;
import com.terrambe.service.EtichettaPittogrammiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.EtichettaPittogrammi}.
 */
@RestController
@RequestMapping("/api")
public class EtichettaPittogrammiResource {

    private final Logger log = LoggerFactory.getLogger(EtichettaPittogrammiResource.class);

    private static final String ENTITY_NAME = "etichettaPittogrammi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtichettaPittogrammiService etichettaPittogrammiService;

    private final EtichettaPittogrammiQueryService etichettaPittogrammiQueryService;

    public EtichettaPittogrammiResource(EtichettaPittogrammiService etichettaPittogrammiService, EtichettaPittogrammiQueryService etichettaPittogrammiQueryService) {
        this.etichettaPittogrammiService = etichettaPittogrammiService;
        this.etichettaPittogrammiQueryService = etichettaPittogrammiQueryService;
    }

    /**
     * {@code POST  /etichetta-pittogrammis} : Create a new etichettaPittogrammi.
     *
     * @param etichettaPittogrammi the etichettaPittogrammi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etichettaPittogrammi, or with status {@code 400 (Bad Request)} if the etichettaPittogrammi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etichetta-pittogrammis")
    public ResponseEntity<EtichettaPittogrammi> createEtichettaPittogrammi(@RequestBody EtichettaPittogrammi etichettaPittogrammi) throws URISyntaxException {
        log.debug("REST request to save EtichettaPittogrammi : {}", etichettaPittogrammi);
        if (etichettaPittogrammi.getId() != null) {
            throw new BadRequestAlertException("A new etichettaPittogrammi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtichettaPittogrammi result = etichettaPittogrammiService.save(etichettaPittogrammi);
        return ResponseEntity.created(new URI("/api/etichetta-pittogrammis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etichetta-pittogrammis} : Updates an existing etichettaPittogrammi.
     *
     * @param etichettaPittogrammi the etichettaPittogrammi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etichettaPittogrammi,
     * or with status {@code 400 (Bad Request)} if the etichettaPittogrammi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etichettaPittogrammi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etichetta-pittogrammis")
    public ResponseEntity<EtichettaPittogrammi> updateEtichettaPittogrammi(@RequestBody EtichettaPittogrammi etichettaPittogrammi) throws URISyntaxException {
        log.debug("REST request to update EtichettaPittogrammi : {}", etichettaPittogrammi);
        if (etichettaPittogrammi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtichettaPittogrammi result = etichettaPittogrammiService.save(etichettaPittogrammi);
        EtichettaPittogrammi result2 = etichettaPittogrammiService.save(etichettaPittogrammi);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, etichettaPittogrammi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etichetta-pittogrammis} : get all the etichettaPittogrammis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etichettaPittogrammis in body.
     */
    @GetMapping("/etichetta-pittogrammis")
    public ResponseEntity<List<EtichettaPittogrammi>> getAllEtichettaPittogrammis(EtichettaPittogrammiCriteria criteria) {
        log.debug("REST request to get EtichettaPittogrammis by criteria: {}", criteria);
        List<EtichettaPittogrammi> entityList = etichettaPittogrammiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /etichetta-pittogrammis/count} : count all the etichettaPittogrammis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/etichetta-pittogrammis/count")
    public ResponseEntity<Long> countEtichettaPittogrammis(EtichettaPittogrammiCriteria criteria) {
        log.debug("REST request to count EtichettaPittogrammis by criteria: {}", criteria);
        return ResponseEntity.ok().body(etichettaPittogrammiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etichetta-pittogrammis/:id} : get the "id" etichettaPittogrammi.
     *
     * @param id the id of the etichettaPittogrammi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etichettaPittogrammi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etichetta-pittogrammis/{id}")
    public ResponseEntity<EtichettaPittogrammi> getEtichettaPittogrammi(@PathVariable Long id) {
        log.debug("REST request to get EtichettaPittogrammi : {}", id);
        Optional<EtichettaPittogrammi> etichettaPittogrammi = etichettaPittogrammiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etichettaPittogrammi);
    }

    /**
     * {@code DELETE  /etichetta-pittogrammis/:id} : delete the "id" etichettaPittogrammi.
     *
     * @param id the id of the etichettaPittogrammi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etichetta-pittogrammis/{id}")
    public ResponseEntity<Void> deleteEtichettaPittogrammi(@PathVariable Long id) {
        log.debug("REST request to delete EtichettaPittogrammi : {}", id);
        etichettaPittogrammiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
