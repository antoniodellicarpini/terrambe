package com.terrambe.web.rest;

import com.terrambe.domain.AtrAnagrafica;
import com.terrambe.service.AtrAnagraficaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AtrAnagraficaCriteria;
import com.terrambe.service.AtrAnagraficaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AtrAnagrafica}.
 */
@RestController
@RequestMapping("/api")
public class AtrAnagraficaResource {

    private final Logger log = LoggerFactory.getLogger(AtrAnagraficaResource.class);

    private static final String ENTITY_NAME = "atrAnagrafica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtrAnagraficaService atrAnagraficaService;

    private final AtrAnagraficaQueryService atrAnagraficaQueryService;

    public AtrAnagraficaResource(AtrAnagraficaService atrAnagraficaService, AtrAnagraficaQueryService atrAnagraficaQueryService) {
        this.atrAnagraficaService = atrAnagraficaService;
        this.atrAnagraficaQueryService = atrAnagraficaQueryService;
    }

    /**
     * {@code POST  /atr-anagraficas} : Create a new atrAnagrafica.
     *
     * @param atrAnagrafica the atrAnagrafica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atrAnagrafica, or with status {@code 400 (Bad Request)} if the atrAnagrafica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/atr-anagraficas")
    public ResponseEntity<AtrAnagrafica> createAtrAnagrafica(@Valid @RequestBody AtrAnagrafica atrAnagrafica) throws URISyntaxException {
        log.debug("REST request to save AtrAnagrafica : {}", atrAnagrafica);
        if (atrAnagrafica.getId() != null) {
            throw new BadRequestAlertException("A new atrAnagrafica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtrAnagrafica result = atrAnagraficaService.save(atrAnagrafica);
        return ResponseEntity.created(new URI("/api/atr-anagraficas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /atr-anagraficas} : Updates an existing atrAnagrafica.
     *
     * @param atrAnagrafica the atrAnagrafica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atrAnagrafica,
     * or with status {@code 400 (Bad Request)} if the atrAnagrafica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atrAnagrafica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/atr-anagraficas")
    public ResponseEntity<AtrAnagrafica> updateAtrAnagrafica(@Valid @RequestBody AtrAnagrafica atrAnagrafica) throws URISyntaxException {
        log.debug("REST request to update AtrAnagrafica : {}", atrAnagrafica);
        if (atrAnagrafica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtrAnagrafica result = atrAnagraficaService.save(atrAnagrafica);
        AtrAnagrafica result2 = atrAnagraficaService.save(atrAnagrafica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, atrAnagrafica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /atr-anagraficas} : get all the atrAnagraficas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atrAnagraficas in body.
     */
    @GetMapping("/atr-anagraficas")
    public ResponseEntity<List<AtrAnagrafica>> getAllAtrAnagraficas(AtrAnagraficaCriteria criteria) {
        log.debug("REST request to get AtrAnagraficas by criteria: {}", criteria);
        List<AtrAnagrafica> entityList = atrAnagraficaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /atr-anagraficas/count} : count all the atrAnagraficas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/atr-anagraficas/count")
    public ResponseEntity<Long> countAtrAnagraficas(AtrAnagraficaCriteria criteria) {
        log.debug("REST request to count AtrAnagraficas by criteria: {}", criteria);
        return ResponseEntity.ok().body(atrAnagraficaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /atr-anagraficas/:id} : get the "id" atrAnagrafica.
     *
     * @param id the id of the atrAnagrafica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atrAnagrafica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/atr-anagraficas/{id}")
    public ResponseEntity<AtrAnagrafica> getAtrAnagrafica(@PathVariable Long id) {
        log.debug("REST request to get AtrAnagrafica : {}", id);
        Optional<AtrAnagrafica> atrAnagrafica = atrAnagraficaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atrAnagrafica);
    }

    /**
     * {@code DELETE  /atr-anagraficas/:id} : delete the "id" atrAnagrafica.
     *
     * @param id the id of the atrAnagrafica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/atr-anagraficas/{id}")
    public ResponseEntity<Void> deleteAtrAnagrafica(@PathVariable Long id) {
        log.debug("REST request to delete AtrAnagrafica : {}", id);
        atrAnagraficaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
