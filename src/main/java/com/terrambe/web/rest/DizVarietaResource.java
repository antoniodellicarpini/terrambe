package com.terrambe.web.rest;

import com.terrambe.domain.DizVarieta;
import com.terrambe.service.DizVarietaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizVarietaCriteria;
import com.terrambe.service.DizVarietaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizVarieta}.
 */
@RestController
@RequestMapping("/api")
public class DizVarietaResource {

    private final Logger log = LoggerFactory.getLogger(DizVarietaResource.class);

    private static final String ENTITY_NAME = "dizVarieta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizVarietaService dizVarietaService;

    private final DizVarietaQueryService dizVarietaQueryService;

    public DizVarietaResource(DizVarietaService dizVarietaService, DizVarietaQueryService dizVarietaQueryService) {
        this.dizVarietaService = dizVarietaService;
        this.dizVarietaQueryService = dizVarietaQueryService;
    }

    /**
     * {@code POST  /diz-varietas} : Create a new dizVarieta.
     *
     * @param dizVarieta the dizVarieta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizVarieta, or with status {@code 400 (Bad Request)} if the dizVarieta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-varietas")
    public ResponseEntity<DizVarieta> createDizVarieta(@RequestBody DizVarieta dizVarieta) throws URISyntaxException {
        log.debug("REST request to save DizVarieta : {}", dizVarieta);
        if (dizVarieta.getId() != null) {
            throw new BadRequestAlertException("A new dizVarieta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizVarieta result = dizVarietaService.save(dizVarieta);
        return ResponseEntity.created(new URI("/api/diz-varietas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-varietas} : Updates an existing dizVarieta.
     *
     * @param dizVarieta the dizVarieta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizVarieta,
     * or with status {@code 400 (Bad Request)} if the dizVarieta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizVarieta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-varietas")
    public ResponseEntity<DizVarieta> updateDizVarieta(@RequestBody DizVarieta dizVarieta) throws URISyntaxException {
        log.debug("REST request to update DizVarieta : {}", dizVarieta);
        if (dizVarieta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizVarieta result = dizVarietaService.save(dizVarieta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizVarieta.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-varietas} : get all the dizVarietas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizVarietas in body.
     */
    @GetMapping("/diz-varietas")
    public ResponseEntity<List<DizVarieta>> getAllDizVarietas(DizVarietaCriteria criteria) {
        log.debug("REST request to get DizVarietas by criteria: {}", criteria);
        List<DizVarieta> entityList = dizVarietaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-varietas/count} : count all the dizVarietas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-varietas/count")
    public ResponseEntity<Long> countDizVarietas(DizVarietaCriteria criteria) {
        log.debug("REST request to count DizVarietas by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizVarietaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-varietas/:id} : get the "id" dizVarieta.
     *
     * @param id the id of the dizVarieta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizVarieta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-varietas/{id}")
    public ResponseEntity<DizVarieta> getDizVarieta(@PathVariable Long id) {
        log.debug("REST request to get DizVarieta : {}", id);
        Optional<DizVarieta> dizVarieta = dizVarietaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizVarieta);
    }

    /**
     * {@code DELETE  /diz-varietas/:id} : delete the "id" dizVarieta.
     *
     * @param id the id of the dizVarieta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-varietas/{id}")
    public ResponseEntity<Void> deleteDizVarieta(@PathVariable Long id) {
        log.debug("REST request to delete DizVarieta : {}", id);
        dizVarietaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
