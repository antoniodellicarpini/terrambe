package com.terrambe.web.rest;

import com.terrambe.domain.TipoIrr;
import com.terrambe.service.TipoIrrService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoIrrCriteria;
import com.terrambe.service.TipoIrrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoIrr}.
 */
@RestController
@RequestMapping("/api")
public class TipoIrrResource {

    private final Logger log = LoggerFactory.getLogger(TipoIrrResource.class);

    private static final String ENTITY_NAME = "tipoIrr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoIrrService tipoIrrService;

    private final TipoIrrQueryService tipoIrrQueryService;

    public TipoIrrResource(TipoIrrService tipoIrrService, TipoIrrQueryService tipoIrrQueryService) {
        this.tipoIrrService = tipoIrrService;
        this.tipoIrrQueryService = tipoIrrQueryService;
    }

    /**
     * {@code POST  /tipo-irrs} : Create a new tipoIrr.
     *
     * @param tipoIrr the tipoIrr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoIrr, or with status {@code 400 (Bad Request)} if the tipoIrr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-irrs")
    public ResponseEntity<TipoIrr> createTipoIrr(@RequestBody TipoIrr tipoIrr) throws URISyntaxException {
        log.debug("REST request to save TipoIrr : {}", tipoIrr);
        if (tipoIrr.getId() != null) {
            throw new BadRequestAlertException("A new tipoIrr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoIrr result = tipoIrrService.save(tipoIrr);
        return ResponseEntity.created(new URI("/api/tipo-irrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-irrs} : Updates an existing tipoIrr.
     *
     * @param tipoIrr the tipoIrr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoIrr,
     * or with status {@code 400 (Bad Request)} if the tipoIrr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoIrr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-irrs")
    public ResponseEntity<TipoIrr> updateTipoIrr(@RequestBody TipoIrr tipoIrr) throws URISyntaxException {
        log.debug("REST request to update TipoIrr : {}", tipoIrr);
        if (tipoIrr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoIrr result = tipoIrrService.save(tipoIrr);
        TipoIrr result2 = tipoIrrService.save(tipoIrr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoIrr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-irrs} : get all the tipoIrrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoIrrs in body.
     */
    @GetMapping("/tipo-irrs")
    public ResponseEntity<List<TipoIrr>> getAllTipoIrrs(TipoIrrCriteria criteria) {
        log.debug("REST request to get TipoIrrs by criteria: {}", criteria);
        List<TipoIrr> entityList = tipoIrrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-irrs/count} : count all the tipoIrrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-irrs/count")
    public ResponseEntity<Long> countTipoIrrs(TipoIrrCriteria criteria) {
        log.debug("REST request to count TipoIrrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoIrrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-irrs/:id} : get the "id" tipoIrr.
     *
     * @param id the id of the tipoIrr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoIrr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-irrs/{id}")
    public ResponseEntity<TipoIrr> getTipoIrr(@PathVariable Long id) {
        log.debug("REST request to get TipoIrr : {}", id);
        Optional<TipoIrr> tipoIrr = tipoIrrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoIrr);
    }

    /**
     * {@code DELETE  /tipo-irrs/:id} : delete the "id" tipoIrr.
     *
     * @param id the id of the tipoIrr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-irrs/{id}")
    public ResponseEntity<Void> deleteTipoIrr(@PathVariable Long id) {
        log.debug("REST request to delete TipoIrr : {}", id);
        tipoIrrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
