package com.terrambe.web.rest;

import com.terrambe.domain.AtrMarchio;
import com.terrambe.service.AtrMarchioService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AtrMarchioCriteria;
import com.terrambe.service.AtrMarchioQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AtrMarchio}.
 */
@RestController
@RequestMapping("/api")
public class AtrMarchioResource {

    private final Logger log = LoggerFactory.getLogger(AtrMarchioResource.class);

    private static final String ENTITY_NAME = "atrMarchio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtrMarchioService atrMarchioService;

    private final AtrMarchioQueryService atrMarchioQueryService;

    public AtrMarchioResource(AtrMarchioService atrMarchioService, AtrMarchioQueryService atrMarchioQueryService) {
        this.atrMarchioService = atrMarchioService;
        this.atrMarchioQueryService = atrMarchioQueryService;
    }

    /**
     * {@code POST  /atr-marchios} : Create a new atrMarchio.
     *
     * @param atrMarchio the atrMarchio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atrMarchio, or with status {@code 400 (Bad Request)} if the atrMarchio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/atr-marchios")
    public ResponseEntity<AtrMarchio> createAtrMarchio(@Valid @RequestBody AtrMarchio atrMarchio) throws URISyntaxException {
        log.debug("REST request to save AtrMarchio : {}", atrMarchio);
        if (atrMarchio.getId() != null) {
            throw new BadRequestAlertException("A new atrMarchio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtrMarchio result = atrMarchioService.save(atrMarchio);
        return ResponseEntity.created(new URI("/api/atr-marchios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /atr-marchios} : Updates an existing atrMarchio.
     *
     * @param atrMarchio the atrMarchio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atrMarchio,
     * or with status {@code 400 (Bad Request)} if the atrMarchio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atrMarchio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/atr-marchios")
    public ResponseEntity<AtrMarchio> updateAtrMarchio(@Valid @RequestBody AtrMarchio atrMarchio) throws URISyntaxException {
        log.debug("REST request to update AtrMarchio : {}", atrMarchio);
        if (atrMarchio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtrMarchio result = atrMarchioService.save(atrMarchio);
        AtrMarchio result2 = atrMarchioService.save(atrMarchio);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, atrMarchio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /atr-marchios} : get all the atrMarchios.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atrMarchios in body.
     */
    @GetMapping("/atr-marchios")
    public ResponseEntity<List<AtrMarchio>> getAllAtrMarchios(AtrMarchioCriteria criteria) {
        log.debug("REST request to get AtrMarchios by criteria: {}", criteria);
        List<AtrMarchio> entityList = atrMarchioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /atr-marchios/count} : count all the atrMarchios.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/atr-marchios/count")
    public ResponseEntity<Long> countAtrMarchios(AtrMarchioCriteria criteria) {
        log.debug("REST request to count AtrMarchios by criteria: {}", criteria);
        return ResponseEntity.ok().body(atrMarchioQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /atr-marchios/:id} : get the "id" atrMarchio.
     *
     * @param id the id of the atrMarchio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atrMarchio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/atr-marchios/{id}")
    public ResponseEntity<AtrMarchio> getAtrMarchio(@PathVariable Long id) {
        log.debug("REST request to get AtrMarchio : {}", id);
        Optional<AtrMarchio> atrMarchio = atrMarchioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atrMarchio);
    }

    /**
     * {@code DELETE  /atr-marchios/:id} : delete the "id" atrMarchio.
     *
     * @param id the id of the atrMarchio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/atr-marchios/{id}")
    public ResponseEntity<Void> deleteAtrMarchio(@PathVariable Long id) {
        log.debug("REST request to delete AtrMarchio : {}", id);
        atrMarchioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
