package com.terrambe.web.rest;

import com.terrambe.domain.IndAzienda;
import com.terrambe.service.IndAziendaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndAziendaCriteria;
import com.terrambe.service.IndAziendaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndAzienda}.
 */
@RestController
@RequestMapping("/api")
public class IndAziendaResource {

    private final Logger log = LoggerFactory.getLogger(IndAziendaResource.class);

    private static final String ENTITY_NAME = "indAzienda";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndAziendaService indAziendaService;

    private final IndAziendaQueryService indAziendaQueryService;

    public IndAziendaResource(IndAziendaService indAziendaService, IndAziendaQueryService indAziendaQueryService) {
        this.indAziendaService = indAziendaService;
        this.indAziendaQueryService = indAziendaQueryService;
    }

    /**
     * {@code POST  /ind-aziendas} : Create a new indAzienda.
     *
     * @param indAzienda the indAzienda to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indAzienda, or with status {@code 400 (Bad Request)} if the indAzienda has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-aziendas")
    public ResponseEntity<IndAzienda> createIndAzienda(@Valid @RequestBody IndAzienda indAzienda) throws URISyntaxException {
        log.debug("REST request to save IndAzienda : {}", indAzienda);
        if (indAzienda.getId() != null) {
            throw new BadRequestAlertException("A new indAzienda cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndAzienda result = indAziendaService.save(indAzienda);
        return ResponseEntity.created(new URI("/api/ind-aziendas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-aziendas} : Updates an existing indAzienda.
     *
     * @param indAzienda the indAzienda to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indAzienda,
     * or with status {@code 400 (Bad Request)} if the indAzienda is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indAzienda couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-aziendas")
    public ResponseEntity<IndAzienda> updateIndAzienda(@Valid @RequestBody IndAzienda indAzienda) throws URISyntaxException {
        log.debug("REST request to update IndAzienda : {}", indAzienda);
        if (indAzienda.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndAzienda result = indAziendaService.save(indAzienda);
        IndAzienda result2 = indAziendaService.save(indAzienda);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indAzienda.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-aziendas} : get all the indAziendas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indAziendas in body.
     */
    @GetMapping("/ind-aziendas")
    public ResponseEntity<List<IndAzienda>> getAllIndAziendas(IndAziendaCriteria criteria) {
        log.debug("REST request to get IndAziendas by criteria: {}", criteria);
        List<IndAzienda> entityList = indAziendaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-aziendas/count} : count all the indAziendas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-aziendas/count")
    public ResponseEntity<Long> countIndAziendas(IndAziendaCriteria criteria) {
        log.debug("REST request to count IndAziendas by criteria: {}", criteria);
        return ResponseEntity.ok().body(indAziendaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-aziendas/:id} : get the "id" indAzienda.
     *
     * @param id the id of the indAzienda to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indAzienda, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-aziendas/{id}")
    public ResponseEntity<IndAzienda> getIndAzienda(@PathVariable Long id) {
        log.debug("REST request to get IndAzienda : {}", id);
        Optional<IndAzienda> indAzienda = indAziendaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indAzienda);
    }

    /**
     * {@code DELETE  /ind-aziendas/:id} : delete the "id" indAzienda.
     *
     * @param id the id of the indAzienda to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-aziendas/{id}")
    public ResponseEntity<Void> deleteIndAzienda(@PathVariable Long id) {
        log.debug("REST request to delete IndAzienda : {}", id);
        indAziendaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
