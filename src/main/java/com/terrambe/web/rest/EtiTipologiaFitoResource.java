package com.terrambe.web.rest;

import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.service.EtiTipologiaFitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.EtiTipologiaFitoCriteria;
import com.terrambe.service.EtiTipologiaFitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.EtiTipologiaFito}.
 */
@RestController
@RequestMapping("/api")
public class EtiTipologiaFitoResource {

    private final Logger log = LoggerFactory.getLogger(EtiTipologiaFitoResource.class);

    private static final String ENTITY_NAME = "etiTipologiaFito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtiTipologiaFitoService etiTipologiaFitoService;

    private final EtiTipologiaFitoQueryService etiTipologiaFitoQueryService;

    public EtiTipologiaFitoResource(EtiTipologiaFitoService etiTipologiaFitoService, EtiTipologiaFitoQueryService etiTipologiaFitoQueryService) {
        this.etiTipologiaFitoService = etiTipologiaFitoService;
        this.etiTipologiaFitoQueryService = etiTipologiaFitoQueryService;
    }

    /**
     * {@code POST  /eti-tipologia-fitos} : Create a new etiTipologiaFito.
     *
     * @param etiTipologiaFito the etiTipologiaFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etiTipologiaFito, or with status {@code 400 (Bad Request)} if the etiTipologiaFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/eti-tipologia-fitos")
    public ResponseEntity<EtiTipologiaFito> createEtiTipologiaFito(@RequestBody EtiTipologiaFito etiTipologiaFito) throws URISyntaxException {
        log.debug("REST request to save EtiTipologiaFito : {}", etiTipologiaFito);
        if (etiTipologiaFito.getId() != null) {
            throw new BadRequestAlertException("A new etiTipologiaFito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtiTipologiaFito result = etiTipologiaFitoService.save(etiTipologiaFito);
        return ResponseEntity.created(new URI("/api/eti-tipologia-fitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /eti-tipologia-fitos} : Updates an existing etiTipologiaFito.
     *
     * @param etiTipologiaFito the etiTipologiaFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etiTipologiaFito,
     * or with status {@code 400 (Bad Request)} if the etiTipologiaFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etiTipologiaFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/eti-tipologia-fitos")
    public ResponseEntity<EtiTipologiaFito> updateEtiTipologiaFito(@RequestBody EtiTipologiaFito etiTipologiaFito) throws URISyntaxException {
        log.debug("REST request to update EtiTipologiaFito : {}", etiTipologiaFito);
        if (etiTipologiaFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtiTipologiaFito result = etiTipologiaFitoService.save(etiTipologiaFito);
        EtiTipologiaFito result2 = etiTipologiaFitoService.save(etiTipologiaFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, etiTipologiaFito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /eti-tipologia-fitos} : get all the etiTipologiaFitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etiTipologiaFitos in body.
     */
    @GetMapping("/eti-tipologia-fitos")
    public ResponseEntity<List<EtiTipologiaFito>> getAllEtiTipologiaFitos(EtiTipologiaFitoCriteria criteria) {
        log.debug("REST request to get EtiTipologiaFitos by criteria: {}", criteria);
        List<EtiTipologiaFito> entityList = etiTipologiaFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /eti-tipologia-fitos/count} : count all the etiTipologiaFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/eti-tipologia-fitos/count")
    public ResponseEntity<Long> countEtiTipologiaFitos(EtiTipologiaFitoCriteria criteria) {
        log.debug("REST request to count EtiTipologiaFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(etiTipologiaFitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /eti-tipologia-fitos/:id} : get the "id" etiTipologiaFito.
     *
     * @param id the id of the etiTipologiaFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etiTipologiaFito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/eti-tipologia-fitos/{id}")
    public ResponseEntity<EtiTipologiaFito> getEtiTipologiaFito(@PathVariable Long id) {
        log.debug("REST request to get EtiTipologiaFito : {}", id);
        Optional<EtiTipologiaFito> etiTipologiaFito = etiTipologiaFitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etiTipologiaFito);
    }

    /**
     * {@code DELETE  /eti-tipologia-fitos/:id} : delete the "id" etiTipologiaFito.
     *
     * @param id the id of the etiTipologiaFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/eti-tipologia-fitos/{id}")
    public ResponseEntity<Void> deleteEtiTipologiaFito(@PathVariable Long id) {
        log.debug("REST request to delete EtiTipologiaFito : {}", id);
        etiTipologiaFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
