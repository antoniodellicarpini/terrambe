package com.terrambe.web.rest;

import com.terrambe.domain.DosaggioTrattamento;
import com.terrambe.service.DosaggioTrattamentoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DosaggioTrattamentoCriteria;
import com.terrambe.service.DosaggioTrattamentoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DosaggioTrattamento}.
 */
@RestController
@RequestMapping("/api")
public class DosaggioTrattamentoResource {

    private final Logger log = LoggerFactory.getLogger(DosaggioTrattamentoResource.class);

    private static final String ENTITY_NAME = "dosaggioTrattamento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DosaggioTrattamentoService dosaggioTrattamentoService;

    private final DosaggioTrattamentoQueryService dosaggioTrattamentoQueryService;

    public DosaggioTrattamentoResource(DosaggioTrattamentoService dosaggioTrattamentoService, DosaggioTrattamentoQueryService dosaggioTrattamentoQueryService) {
        this.dosaggioTrattamentoService = dosaggioTrattamentoService;
        this.dosaggioTrattamentoQueryService = dosaggioTrattamentoQueryService;
    }

    /**
     * {@code POST  /dosaggio-trattamentos} : Create a new dosaggioTrattamento.
     *
     * @param dosaggioTrattamento the dosaggioTrattamento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dosaggioTrattamento, or with status {@code 400 (Bad Request)} if the dosaggioTrattamento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dosaggio-trattamentos")
    public ResponseEntity<DosaggioTrattamento> createDosaggioTrattamento(@RequestBody DosaggioTrattamento dosaggioTrattamento) throws URISyntaxException {
        log.debug("REST request to save DosaggioTrattamento : {}", dosaggioTrattamento);
        if (dosaggioTrattamento.getId() != null) {
            throw new BadRequestAlertException("A new dosaggioTrattamento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DosaggioTrattamento result = dosaggioTrattamentoService.save(dosaggioTrattamento);
        return ResponseEntity.created(new URI("/api/dosaggio-trattamentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dosaggio-trattamentos} : Updates an existing dosaggioTrattamento.
     *
     * @param dosaggioTrattamento the dosaggioTrattamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dosaggioTrattamento,
     * or with status {@code 400 (Bad Request)} if the dosaggioTrattamento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dosaggioTrattamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dosaggio-trattamentos")
    public ResponseEntity<DosaggioTrattamento> updateDosaggioTrattamento(@RequestBody DosaggioTrattamento dosaggioTrattamento) throws URISyntaxException {
        log.debug("REST request to update DosaggioTrattamento : {}", dosaggioTrattamento);
        if (dosaggioTrattamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DosaggioTrattamento result = dosaggioTrattamentoService.save(dosaggioTrattamento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dosaggioTrattamento.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dosaggio-trattamentos} : get all the dosaggioTrattamentos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dosaggioTrattamentos in body.
     */
    @GetMapping("/dosaggio-trattamentos")
    public ResponseEntity<List<DosaggioTrattamento>> getAllDosaggioTrattamentos(DosaggioTrattamentoCriteria criteria) {
        log.debug("REST request to get DosaggioTrattamentos by criteria: {}", criteria);
        List<DosaggioTrattamento> entityList = dosaggioTrattamentoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /dosaggio-trattamentos/count} : count all the dosaggioTrattamentos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/dosaggio-trattamentos/count")
    public ResponseEntity<Long> countDosaggioTrattamentos(DosaggioTrattamentoCriteria criteria) {
        log.debug("REST request to count DosaggioTrattamentos by criteria: {}", criteria);
        return ResponseEntity.ok().body(dosaggioTrattamentoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dosaggio-trattamentos/:id} : get the "id" dosaggioTrattamento.
     *
     * @param id the id of the dosaggioTrattamento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dosaggioTrattamento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dosaggio-trattamentos/{id}")
    public ResponseEntity<DosaggioTrattamento> getDosaggioTrattamento(@PathVariable Long id) {
        log.debug("REST request to get DosaggioTrattamento : {}", id);
        Optional<DosaggioTrattamento> dosaggioTrattamento = dosaggioTrattamentoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dosaggioTrattamento);
    }

    /**
     * {@code DELETE  /dosaggio-trattamentos/:id} : delete the "id" dosaggioTrattamento.
     *
     * @param id the id of the dosaggioTrattamento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dosaggio-trattamentos/{id}")
    public ResponseEntity<Void> deleteDosaggioTrattamento(@PathVariable Long id) {
        log.debug("REST request to delete DosaggioTrattamento : {}", id);
        dosaggioTrattamentoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
