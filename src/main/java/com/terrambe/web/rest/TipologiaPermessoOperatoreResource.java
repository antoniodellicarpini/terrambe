package com.terrambe.web.rest;

import com.terrambe.domain.TipologiaPermessoOperatore;
import com.terrambe.service.TipologiaPermessoOperatoreService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipologiaPermessoOperatoreCriteria;
import com.terrambe.service.TipologiaPermessoOperatoreQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipologiaPermessoOperatore}.
 */
@RestController
@RequestMapping("/api")
public class TipologiaPermessoOperatoreResource {

    private final Logger log = LoggerFactory.getLogger(TipologiaPermessoOperatoreResource.class);

    private static final String ENTITY_NAME = "tipologiaPermessoOperatore";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipologiaPermessoOperatoreService tipologiaPermessoOperatoreService;

    private final TipologiaPermessoOperatoreQueryService tipologiaPermessoOperatoreQueryService;

    public TipologiaPermessoOperatoreResource(TipologiaPermessoOperatoreService tipologiaPermessoOperatoreService, TipologiaPermessoOperatoreQueryService tipologiaPermessoOperatoreQueryService) {
        this.tipologiaPermessoOperatoreService = tipologiaPermessoOperatoreService;
        this.tipologiaPermessoOperatoreQueryService = tipologiaPermessoOperatoreQueryService;
    }

    /**
     * {@code POST  /tipologia-permesso-operatores} : Create a new tipologiaPermessoOperatore.
     *
     * @param tipologiaPermessoOperatore the tipologiaPermessoOperatore to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipologiaPermessoOperatore, or with status {@code 400 (Bad Request)} if the tipologiaPermessoOperatore has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipologia-permesso-operatores")
    public ResponseEntity<TipologiaPermessoOperatore> createTipologiaPermessoOperatore(@Valid @RequestBody TipologiaPermessoOperatore tipologiaPermessoOperatore) throws URISyntaxException {
        log.debug("REST request to save TipologiaPermessoOperatore : {}", tipologiaPermessoOperatore);
        if (tipologiaPermessoOperatore.getId() != null) {
            throw new BadRequestAlertException("A new tipologiaPermessoOperatore cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipologiaPermessoOperatore result = tipologiaPermessoOperatoreService.save(tipologiaPermessoOperatore);
        return ResponseEntity.created(new URI("/api/tipologia-permesso-operatores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipologia-permesso-operatores} : Updates an existing tipologiaPermessoOperatore.
     *
     * @param tipologiaPermessoOperatore the tipologiaPermessoOperatore to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipologiaPermessoOperatore,
     * or with status {@code 400 (Bad Request)} if the tipologiaPermessoOperatore is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipologiaPermessoOperatore couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipologia-permesso-operatores")
    public ResponseEntity<TipologiaPermessoOperatore> updateTipologiaPermessoOperatore(@Valid @RequestBody TipologiaPermessoOperatore tipologiaPermessoOperatore) throws URISyntaxException {
        log.debug("REST request to update TipologiaPermessoOperatore : {}", tipologiaPermessoOperatore);
        if (tipologiaPermessoOperatore.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipologiaPermessoOperatore result = tipologiaPermessoOperatoreService.save(tipologiaPermessoOperatore);
        TipologiaPermessoOperatore result2 = tipologiaPermessoOperatoreService.save(tipologiaPermessoOperatore);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipologiaPermessoOperatore.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipologia-permesso-operatores} : get all the tipologiaPermessoOperatores.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipologiaPermessoOperatores in body.
     */
    @GetMapping("/tipologia-permesso-operatores")
    public ResponseEntity<List<TipologiaPermessoOperatore>> getAllTipologiaPermessoOperatores(TipologiaPermessoOperatoreCriteria criteria) {
        log.debug("REST request to get TipologiaPermessoOperatores by criteria: {}", criteria);
        List<TipologiaPermessoOperatore> entityList = tipologiaPermessoOperatoreQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipologia-permesso-operatores/count} : count all the tipologiaPermessoOperatores.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipologia-permesso-operatores/count")
    public ResponseEntity<Long> countTipologiaPermessoOperatores(TipologiaPermessoOperatoreCriteria criteria) {
        log.debug("REST request to count TipologiaPermessoOperatores by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipologiaPermessoOperatoreQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipologia-permesso-operatores/:id} : get the "id" tipologiaPermessoOperatore.
     *
     * @param id the id of the tipologiaPermessoOperatore to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipologiaPermessoOperatore, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipologia-permesso-operatores/{id}")
    public ResponseEntity<TipologiaPermessoOperatore> getTipologiaPermessoOperatore(@PathVariable Long id) {
        log.debug("REST request to get TipologiaPermessoOperatore : {}", id);
        Optional<TipologiaPermessoOperatore> tipologiaPermessoOperatore = tipologiaPermessoOperatoreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipologiaPermessoOperatore);
    }

    /**
     * {@code DELETE  /tipologia-permesso-operatores/:id} : delete the "id" tipologiaPermessoOperatore.
     *
     * @param id the id of the tipologiaPermessoOperatore to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipologia-permesso-operatores/{id}")
    public ResponseEntity<Void> deleteTipologiaPermessoOperatore(@PathVariable Long id) {
        log.debug("REST request to delete TipologiaPermessoOperatore : {}", id);
        tipologiaPermessoOperatoreService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
