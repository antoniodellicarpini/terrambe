package com.terrambe.web.rest;

import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.service.EtichettaFrasiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.EtichettaFrasiCriteria;
import com.terrambe.service.EtichettaFrasiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.EtichettaFrasi}.
 */
@RestController
@RequestMapping("/api")
public class EtichettaFrasiResource {

    private final Logger log = LoggerFactory.getLogger(EtichettaFrasiResource.class);

    private static final String ENTITY_NAME = "etichettaFrasi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtichettaFrasiService etichettaFrasiService;

    private final EtichettaFrasiQueryService etichettaFrasiQueryService;

    public EtichettaFrasiResource(EtichettaFrasiService etichettaFrasiService, EtichettaFrasiQueryService etichettaFrasiQueryService) {
        this.etichettaFrasiService = etichettaFrasiService;
        this.etichettaFrasiQueryService = etichettaFrasiQueryService;
    }

    /**
     * {@code POST  /etichetta-frasis} : Create a new etichettaFrasi.
     *
     * @param etichettaFrasi the etichettaFrasi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etichettaFrasi, or with status {@code 400 (Bad Request)} if the etichettaFrasi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etichetta-frasis")
    public ResponseEntity<EtichettaFrasi> createEtichettaFrasi(@RequestBody EtichettaFrasi etichettaFrasi) throws URISyntaxException {
        log.debug("REST request to save EtichettaFrasi : {}", etichettaFrasi);
        if (etichettaFrasi.getId() != null) {
            throw new BadRequestAlertException("A new etichettaFrasi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtichettaFrasi result = etichettaFrasiService.save(etichettaFrasi);
        return ResponseEntity.created(new URI("/api/etichetta-frasis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etichetta-frasis} : Updates an existing etichettaFrasi.
     *
     * @param etichettaFrasi the etichettaFrasi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etichettaFrasi,
     * or with status {@code 400 (Bad Request)} if the etichettaFrasi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etichettaFrasi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etichetta-frasis")
    public ResponseEntity<EtichettaFrasi> updateEtichettaFrasi(@RequestBody EtichettaFrasi etichettaFrasi) throws URISyntaxException {
        log.debug("REST request to update EtichettaFrasi : {}", etichettaFrasi);
        if (etichettaFrasi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtichettaFrasi result = etichettaFrasiService.save(etichettaFrasi);
        EtichettaFrasi result2 = etichettaFrasiService.save(etichettaFrasi);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, etichettaFrasi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etichetta-frasis} : get all the etichettaFrasis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etichettaFrasis in body.
     */
    @GetMapping("/etichetta-frasis")
    public ResponseEntity<List<EtichettaFrasi>> getAllEtichettaFrasis(EtichettaFrasiCriteria criteria) {
        log.debug("REST request to get EtichettaFrasis by criteria: {}", criteria);
        List<EtichettaFrasi> entityList = etichettaFrasiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /etichetta-frasis/count} : count all the etichettaFrasis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/etichetta-frasis/count")
    public ResponseEntity<Long> countEtichettaFrasis(EtichettaFrasiCriteria criteria) {
        log.debug("REST request to count EtichettaFrasis by criteria: {}", criteria);
        return ResponseEntity.ok().body(etichettaFrasiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etichetta-frasis/:id} : get the "id" etichettaFrasi.
     *
     * @param id the id of the etichettaFrasi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etichettaFrasi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etichetta-frasis/{id}")
    public ResponseEntity<EtichettaFrasi> getEtichettaFrasi(@PathVariable Long id) {
        log.debug("REST request to get EtichettaFrasi : {}", id);
        Optional<EtichettaFrasi> etichettaFrasi = etichettaFrasiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etichettaFrasi);
    }

    /**
     * {@code DELETE  /etichetta-frasis/:id} : delete the "id" etichettaFrasi.
     *
     * @param id the id of the etichettaFrasi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etichetta-frasis/{id}")
    public ResponseEntity<Void> deleteEtichettaFrasi(@PathVariable Long id) {
        log.debug("REST request to delete EtichettaFrasi : {}", id);
        etichettaFrasiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
