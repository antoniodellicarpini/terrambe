package com.terrambe.web.rest;

import com.terrambe.domain.TipoRegMag;
import com.terrambe.service.TipoRegMagService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoRegMagCriteria;
import com.terrambe.service.TipoRegMagQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoRegMag}.
 */
@RestController
@RequestMapping("/api")
public class TipoRegMagResource {

    private final Logger log = LoggerFactory.getLogger(TipoRegMagResource.class);

    private static final String ENTITY_NAME = "tipoRegMag";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoRegMagService tipoRegMagService;

    private final TipoRegMagQueryService tipoRegMagQueryService;

    public TipoRegMagResource(TipoRegMagService tipoRegMagService, TipoRegMagQueryService tipoRegMagQueryService) {
        this.tipoRegMagService = tipoRegMagService;
        this.tipoRegMagQueryService = tipoRegMagQueryService;
    }

    /**
     * {@code POST  /tipo-reg-mags} : Create a new tipoRegMag.
     *
     * @param tipoRegMag the tipoRegMag to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoRegMag, or with status {@code 400 (Bad Request)} if the tipoRegMag has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-reg-mags")
    public ResponseEntity<TipoRegMag> createTipoRegMag(@Valid @RequestBody TipoRegMag tipoRegMag) throws URISyntaxException {
        log.debug("REST request to save TipoRegMag : {}", tipoRegMag);
        if (tipoRegMag.getId() != null) {
            throw new BadRequestAlertException("A new tipoRegMag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoRegMag result = tipoRegMagService.save(tipoRegMag);
        return ResponseEntity.created(new URI("/api/tipo-reg-mags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-reg-mags} : Updates an existing tipoRegMag.
     *
     * @param tipoRegMag the tipoRegMag to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoRegMag,
     * or with status {@code 400 (Bad Request)} if the tipoRegMag is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoRegMag couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-reg-mags")
    public ResponseEntity<TipoRegMag> updateTipoRegMag(@Valid @RequestBody TipoRegMag tipoRegMag) throws URISyntaxException {
        log.debug("REST request to update TipoRegMag : {}", tipoRegMag);
        if (tipoRegMag.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoRegMag result = tipoRegMagService.save(tipoRegMag);
        TipoRegMag result2 = tipoRegMagService.save(tipoRegMag);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoRegMag.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-reg-mags} : get all the tipoRegMags.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoRegMags in body.
     */
    @GetMapping("/tipo-reg-mags")
    public ResponseEntity<List<TipoRegMag>> getAllTipoRegMags(TipoRegMagCriteria criteria) {
        log.debug("REST request to get TipoRegMags by criteria: {}", criteria);
        List<TipoRegMag> entityList = tipoRegMagQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-reg-mags/count} : count all the tipoRegMags.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-reg-mags/count")
    public ResponseEntity<Long> countTipoRegMags(TipoRegMagCriteria criteria) {
        log.debug("REST request to count TipoRegMags by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoRegMagQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-reg-mags/:id} : get the "id" tipoRegMag.
     *
     * @param id the id of the tipoRegMag to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoRegMag, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-reg-mags/{id}")
    public ResponseEntity<TipoRegMag> getTipoRegMag(@PathVariable Long id) {
        log.debug("REST request to get TipoRegMag : {}", id);
        Optional<TipoRegMag> tipoRegMag = tipoRegMagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoRegMag);
    }

    /**
     * {@code DELETE  /tipo-reg-mags/:id} : delete the "id" tipoRegMag.
     *
     * @param id the id of the tipoRegMag to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-reg-mags/{id}")
    public ResponseEntity<Void> deleteTipoRegMag(@PathVariable Long id) {
        log.debug("REST request to delete TipoRegMag : {}", id);
        tipoRegMagService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
