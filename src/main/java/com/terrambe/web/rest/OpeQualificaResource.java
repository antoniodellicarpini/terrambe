package com.terrambe.web.rest;

import com.terrambe.domain.OpeQualifica;
import com.terrambe.service.OpeQualificaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.OpeQualificaCriteria;
import com.terrambe.service.OpeQualificaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.OpeQualifica}.
 */
@RestController
@RequestMapping("/api")
public class OpeQualificaResource {

    private final Logger log = LoggerFactory.getLogger(OpeQualificaResource.class);

    private static final String ENTITY_NAME = "opeQualifica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OpeQualificaService opeQualificaService;

    private final OpeQualificaQueryService opeQualificaQueryService;

    public OpeQualificaResource(OpeQualificaService opeQualificaService, OpeQualificaQueryService opeQualificaQueryService) {
        this.opeQualificaService = opeQualificaService;
        this.opeQualificaQueryService = opeQualificaQueryService;
    }

    /**
     * {@code POST  /ope-qualificas} : Create a new opeQualifica.
     *
     * @param opeQualifica the opeQualifica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new opeQualifica, or with status {@code 400 (Bad Request)} if the opeQualifica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ope-qualificas")
    public ResponseEntity<OpeQualifica> createOpeQualifica(@Valid @RequestBody OpeQualifica opeQualifica) throws URISyntaxException {
        log.debug("REST request to save OpeQualifica : {}", opeQualifica);
        if (opeQualifica.getId() != null) {
            throw new BadRequestAlertException("A new opeQualifica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpeQualifica result = opeQualificaService.save(opeQualifica);
        return ResponseEntity.created(new URI("/api/ope-qualificas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ope-qualificas} : Updates an existing opeQualifica.
     *
     * @param opeQualifica the opeQualifica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated opeQualifica,
     * or with status {@code 400 (Bad Request)} if the opeQualifica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the opeQualifica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ope-qualificas")
    public ResponseEntity<OpeQualifica> updateOpeQualifica(@Valid @RequestBody OpeQualifica opeQualifica) throws URISyntaxException {
        log.debug("REST request to update OpeQualifica : {}", opeQualifica);
        if (opeQualifica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OpeQualifica result = opeQualificaService.save(opeQualifica);
        OpeQualifica result2 = opeQualificaService.save(opeQualifica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, opeQualifica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ope-qualificas} : get all the opeQualificas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of opeQualificas in body.
     */
    @GetMapping("/ope-qualificas")
    public ResponseEntity<List<OpeQualifica>> getAllOpeQualificas(OpeQualificaCriteria criteria) {
        log.debug("REST request to get OpeQualificas by criteria: {}", criteria);
        List<OpeQualifica> entityList = opeQualificaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ope-qualificas/count} : count all the opeQualificas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ope-qualificas/count")
    public ResponseEntity<Long> countOpeQualificas(OpeQualificaCriteria criteria) {
        log.debug("REST request to count OpeQualificas by criteria: {}", criteria);
        return ResponseEntity.ok().body(opeQualificaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ope-qualificas/:id} : get the "id" opeQualifica.
     *
     * @param id the id of the opeQualifica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the opeQualifica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ope-qualificas/{id}")
    public ResponseEntity<OpeQualifica> getOpeQualifica(@PathVariable Long id) {
        log.debug("REST request to get OpeQualifica : {}", id);
        Optional<OpeQualifica> opeQualifica = opeQualificaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(opeQualifica);
    }

    /**
     * {@code DELETE  /ope-qualificas/:id} : delete the "id" opeQualifica.
     *
     * @param id the id of the opeQualifica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ope-qualificas/{id}")
    public ResponseEntity<Void> deleteOpeQualifica(@PathVariable Long id) {
        log.debug("REST request to delete OpeQualifica : {}", id);
        opeQualificaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
