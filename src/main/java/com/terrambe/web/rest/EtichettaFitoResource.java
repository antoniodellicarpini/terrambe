package com.terrambe.web.rest;

import com.terrambe.domain.EtichettaFito;
import com.terrambe.service.EtichettaFitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.EtichettaFitoCriteria;
import com.terrambe.service.EtichettaFitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.EtichettaFito}.
 */
@RestController
@RequestMapping("/api")
public class EtichettaFitoResource {

    private final Logger log = LoggerFactory.getLogger(EtichettaFitoResource.class);

    private static final String ENTITY_NAME = "etichettaFito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtichettaFitoService etichettaFitoService;

    private final EtichettaFitoQueryService etichettaFitoQueryService;

    public EtichettaFitoResource(EtichettaFitoService etichettaFitoService, EtichettaFitoQueryService etichettaFitoQueryService) {
        this.etichettaFitoService = etichettaFitoService;
        this.etichettaFitoQueryService = etichettaFitoQueryService;
    }

    /**
     * {@code POST  /etichetta-fitos} : Create a new etichettaFito.
     *
     * @param etichettaFito the etichettaFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etichettaFito, or with status {@code 400 (Bad Request)} if the etichettaFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etichetta-fitos")
    public ResponseEntity<EtichettaFito> createEtichettaFito(@RequestBody EtichettaFito etichettaFito) throws URISyntaxException {
        log.debug("REST request to save EtichettaFito : {}", etichettaFito);
        if (etichettaFito.getId() != null) {
            throw new BadRequestAlertException("A new etichettaFito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtichettaFito result = etichettaFitoService.save(etichettaFito);
        return ResponseEntity.created(new URI("/api/etichetta-fitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etichetta-fitos} : Updates an existing etichettaFito.
     *
     * @param etichettaFito the etichettaFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etichettaFito,
     * or with status {@code 400 (Bad Request)} if the etichettaFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etichettaFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etichetta-fitos")
    public ResponseEntity<EtichettaFito> updateEtichettaFito(@RequestBody EtichettaFito etichettaFito) throws URISyntaxException {
        log.debug("REST request to update EtichettaFito : {}", etichettaFito);
        if (etichettaFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtichettaFito result = etichettaFitoService.save(etichettaFito);
        EtichettaFito result2 = etichettaFitoService.save(etichettaFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, etichettaFito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etichetta-fitos} : get all the etichettaFitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etichettaFitos in body.
     */
    @GetMapping("/etichetta-fitos")
    public ResponseEntity<List<EtichettaFito>> getAllEtichettaFitos(EtichettaFitoCriteria criteria) {
        log.debug("REST request to get EtichettaFitos by criteria: {}", criteria);
        List<EtichettaFito> entityList = etichettaFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /etichetta-fitos/count} : count all the etichettaFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/etichetta-fitos/count")
    public ResponseEntity<Long> countEtichettaFitos(EtichettaFitoCriteria criteria) {
        log.debug("REST request to count EtichettaFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(etichettaFitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etichetta-fitos/:id} : get the "id" etichettaFito.
     *
     * @param id the id of the etichettaFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etichettaFito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etichetta-fitos/{id}")
    public ResponseEntity<EtichettaFito> getEtichettaFito(@PathVariable Long id) {
        log.debug("REST request to get EtichettaFito : {}", id);
        Optional<EtichettaFito> etichettaFito = etichettaFitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etichettaFito);
    }

    /**
     * {@code DELETE  /etichetta-fitos/:id} : delete the "id" etichettaFito.
     *
     * @param id the id of the etichettaFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etichetta-fitos/{id}")
    public ResponseEntity<Void> deleteEtichettaFito(@PathVariable Long id) {
        log.debug("REST request to delete EtichettaFito : {}", id);
        etichettaFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
