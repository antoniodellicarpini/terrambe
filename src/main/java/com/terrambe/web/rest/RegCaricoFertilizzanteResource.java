package com.terrambe.web.rest;

import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.service.RegCaricoFertilizzanteService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegCaricoFertilizzanteCriteria;
import com.terrambe.service.RegCaricoFertilizzanteQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegCaricoFertilizzante}.
 */
@RestController
@RequestMapping("/api")
public class RegCaricoFertilizzanteResource {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFertilizzanteResource.class);

    private static final String ENTITY_NAME = "regCaricoFertilizzante";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegCaricoFertilizzanteService regCaricoFertilizzanteService;

    private final RegCaricoFertilizzanteQueryService regCaricoFertilizzanteQueryService;

    public RegCaricoFertilizzanteResource(RegCaricoFertilizzanteService regCaricoFertilizzanteService, RegCaricoFertilizzanteQueryService regCaricoFertilizzanteQueryService) {
        this.regCaricoFertilizzanteService = regCaricoFertilizzanteService;
        this.regCaricoFertilizzanteQueryService = regCaricoFertilizzanteQueryService;
    }

    /**
     * {@code POST  /reg-carico-fertilizzantes} : Create a new regCaricoFertilizzante.
     *
     * @param regCaricoFertilizzante the regCaricoFertilizzante to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regCaricoFertilizzante, or with status {@code 400 (Bad Request)} if the regCaricoFertilizzante has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-carico-fertilizzantes")
    public ResponseEntity<RegCaricoFertilizzante> createRegCaricoFertilizzante(@Valid @RequestBody RegCaricoFertilizzante regCaricoFertilizzante) throws URISyntaxException {
        log.debug("REST request to save RegCaricoFertilizzante : {}", regCaricoFertilizzante);
        if (regCaricoFertilizzante.getId() != null) {
            throw new BadRequestAlertException("A new regCaricoFertilizzante cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegCaricoFertilizzante result = regCaricoFertilizzanteService.save(regCaricoFertilizzante);
        return ResponseEntity.created(new URI("/api/reg-carico-fertilizzantes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-carico-fertilizzantes} : Updates an existing regCaricoFertilizzante.
     *
     * @param regCaricoFertilizzante the regCaricoFertilizzante to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regCaricoFertilizzante,
     * or with status {@code 400 (Bad Request)} if the regCaricoFertilizzante is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regCaricoFertilizzante couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-carico-fertilizzantes")
    public ResponseEntity<RegCaricoFertilizzante> updateRegCaricoFertilizzante(@Valid @RequestBody RegCaricoFertilizzante regCaricoFertilizzante) throws URISyntaxException {
        log.debug("REST request to update RegCaricoFertilizzante : {}", regCaricoFertilizzante);
        if (regCaricoFertilizzante.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegCaricoFertilizzante result = regCaricoFertilizzanteService.save(regCaricoFertilizzante);
        RegCaricoFertilizzante result2 = regCaricoFertilizzanteService.save(regCaricoFertilizzante);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regCaricoFertilizzante.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-carico-fertilizzantes} : get all the regCaricoFertilizzantes.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regCaricoFertilizzantes in body.
     */
    @GetMapping("/reg-carico-fertilizzantes")
    public ResponseEntity<List<RegCaricoFertilizzante>> getAllRegCaricoFertilizzantes(RegCaricoFertilizzanteCriteria criteria) {
        log.debug("REST request to get RegCaricoFertilizzantes by criteria: {}", criteria);
        List<RegCaricoFertilizzante> entityList = regCaricoFertilizzanteQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-carico-fertilizzantes/count} : count all the regCaricoFertilizzantes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-carico-fertilizzantes/count")
    public ResponseEntity<Long> countRegCaricoFertilizzantes(RegCaricoFertilizzanteCriteria criteria) {
        log.debug("REST request to count RegCaricoFertilizzantes by criteria: {}", criteria);
        return ResponseEntity.ok().body(regCaricoFertilizzanteQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-carico-fertilizzantes/:id} : get the "id" regCaricoFertilizzante.
     *
     * @param id the id of the regCaricoFertilizzante to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regCaricoFertilizzante, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-carico-fertilizzantes/{id}")
    public ResponseEntity<RegCaricoFertilizzante> getRegCaricoFertilizzante(@PathVariable Long id) {
        log.debug("REST request to get RegCaricoFertilizzante : {}", id);
        Optional<RegCaricoFertilizzante> regCaricoFertilizzante = regCaricoFertilizzanteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regCaricoFertilizzante);
    }

    /**
     * {@code DELETE  /reg-carico-fertilizzantes/:id} : delete the "id" regCaricoFertilizzante.
     *
     * @param id the id of the regCaricoFertilizzante to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-carico-fertilizzantes/{id}")
    public ResponseEntity<Void> deleteRegCaricoFertilizzante(@PathVariable Long id) {
        log.debug("REST request to delete RegCaricoFertilizzante : {}", id);
        regCaricoFertilizzanteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
