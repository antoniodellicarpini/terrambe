package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcFitoAppz;
import com.terrambe.service.RegOpcFitoAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcFitoAppzCriteria;
import com.terrambe.service.RegOpcFitoAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcFitoAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcFitoAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoAppzResource.class);

    private static final String ENTITY_NAME = "regOpcFitoAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcFitoAppzService regOpcFitoAppzService;

    private final RegOpcFitoAppzQueryService regOpcFitoAppzQueryService;

    public RegOpcFitoAppzResource(RegOpcFitoAppzService regOpcFitoAppzService, RegOpcFitoAppzQueryService regOpcFitoAppzQueryService) {
        this.regOpcFitoAppzService = regOpcFitoAppzService;
        this.regOpcFitoAppzQueryService = regOpcFitoAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-fito-appzs} : Create a new regOpcFitoAppz.
     *
     * @param regOpcFitoAppz the regOpcFitoAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcFitoAppz, or with status {@code 400 (Bad Request)} if the regOpcFitoAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-fito-appzs")
    public ResponseEntity<RegOpcFitoAppz> createRegOpcFitoAppz(@Valid @RequestBody RegOpcFitoAppz regOpcFitoAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcFitoAppz : {}", regOpcFitoAppz);
        if (regOpcFitoAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcFitoAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcFitoAppz result = regOpcFitoAppzService.save(regOpcFitoAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-fito-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-fito-appzs} : Updates an existing regOpcFitoAppz.
     *
     * @param regOpcFitoAppz the regOpcFitoAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcFitoAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcFitoAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcFitoAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-fito-appzs")
    public ResponseEntity<RegOpcFitoAppz> updateRegOpcFitoAppz(@Valid @RequestBody RegOpcFitoAppz regOpcFitoAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcFitoAppz : {}", regOpcFitoAppz);
        if (regOpcFitoAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcFitoAppz result = regOpcFitoAppzService.save(regOpcFitoAppz);
        RegOpcFitoAppz result2 = regOpcFitoAppzService.save(regOpcFitoAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcFitoAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-fito-appzs} : get all the regOpcFitoAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcFitoAppzs in body.
     */
    @GetMapping("/reg-opc-fito-appzs")
    public ResponseEntity<List<RegOpcFitoAppz>> getAllRegOpcFitoAppzs(RegOpcFitoAppzCriteria criteria) {
        log.debug("REST request to get RegOpcFitoAppzs by criteria: {}", criteria);
        List<RegOpcFitoAppz> entityList = regOpcFitoAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-fito-appzs/count} : count all the regOpcFitoAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-fito-appzs/count")
    public ResponseEntity<Long> countRegOpcFitoAppzs(RegOpcFitoAppzCriteria criteria) {
        log.debug("REST request to count RegOpcFitoAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcFitoAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-fito-appzs/:id} : get the "id" regOpcFitoAppz.
     *
     * @param id the id of the regOpcFitoAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcFitoAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-fito-appzs/{id}")
    public ResponseEntity<RegOpcFitoAppz> getRegOpcFitoAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcFitoAppz : {}", id);
        Optional<RegOpcFitoAppz> regOpcFitoAppz = regOpcFitoAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcFitoAppz);
    }

    /**
     * {@code DELETE  /reg-opc-fito-appzs/:id} : delete the "id" regOpcFitoAppz.
     *
     * @param id the id of the regOpcFitoAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-fito-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcFitoAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcFitoAppz : {}", id);
        regOpcFitoAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
