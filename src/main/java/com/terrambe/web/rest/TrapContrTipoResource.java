package com.terrambe.web.rest;

import com.terrambe.domain.TrapContrTipo;
import com.terrambe.service.TrapContrTipoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TrapContrTipoCriteria;
import com.terrambe.service.TrapContrTipoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TrapContrTipo}.
 */
@RestController
@RequestMapping("/api")
public class TrapContrTipoResource {

    private final Logger log = LoggerFactory.getLogger(TrapContrTipoResource.class);

    private static final String ENTITY_NAME = "trapContrTipo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TrapContrTipoService trapContrTipoService;

    private final TrapContrTipoQueryService trapContrTipoQueryService;

    public TrapContrTipoResource(TrapContrTipoService trapContrTipoService, TrapContrTipoQueryService trapContrTipoQueryService) {
        this.trapContrTipoService = trapContrTipoService;
        this.trapContrTipoQueryService = trapContrTipoQueryService;
    }

    /**
     * {@code POST  /trap-contr-tipos} : Create a new trapContrTipo.
     *
     * @param trapContrTipo the trapContrTipo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new trapContrTipo, or with status {@code 400 (Bad Request)} if the trapContrTipo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/trap-contr-tipos")
    public ResponseEntity<TrapContrTipo> createTrapContrTipo(@RequestBody TrapContrTipo trapContrTipo) throws URISyntaxException {
        log.debug("REST request to save TrapContrTipo : {}", trapContrTipo);
        if (trapContrTipo.getId() != null) {
            throw new BadRequestAlertException("A new trapContrTipo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TrapContrTipo result = trapContrTipoService.save(trapContrTipo);
        return ResponseEntity.created(new URI("/api/trap-contr-tipos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /trap-contr-tipos} : Updates an existing trapContrTipo.
     *
     * @param trapContrTipo the trapContrTipo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated trapContrTipo,
     * or with status {@code 400 (Bad Request)} if the trapContrTipo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the trapContrTipo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/trap-contr-tipos")
    public ResponseEntity<TrapContrTipo> updateTrapContrTipo(@RequestBody TrapContrTipo trapContrTipo) throws URISyntaxException {
        log.debug("REST request to update TrapContrTipo : {}", trapContrTipo);
        if (trapContrTipo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TrapContrTipo result = trapContrTipoService.save(trapContrTipo);
        TrapContrTipo result2 = trapContrTipoService.save(trapContrTipo);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, trapContrTipo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /trap-contr-tipos} : get all the trapContrTipos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trapContrTipos in body.
     */
    @GetMapping("/trap-contr-tipos")
    public ResponseEntity<List<TrapContrTipo>> getAllTrapContrTipos(TrapContrTipoCriteria criteria) {
        log.debug("REST request to get TrapContrTipos by criteria: {}", criteria);
        List<TrapContrTipo> entityList = trapContrTipoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /trap-contr-tipos/count} : count all the trapContrTipos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/trap-contr-tipos/count")
    public ResponseEntity<Long> countTrapContrTipos(TrapContrTipoCriteria criteria) {
        log.debug("REST request to count TrapContrTipos by criteria: {}", criteria);
        return ResponseEntity.ok().body(trapContrTipoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /trap-contr-tipos/:id} : get the "id" trapContrTipo.
     *
     * @param id the id of the trapContrTipo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trapContrTipo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trap-contr-tipos/{id}")
    public ResponseEntity<TrapContrTipo> getTrapContrTipo(@PathVariable Long id) {
        log.debug("REST request to get TrapContrTipo : {}", id);
        Optional<TrapContrTipo> trapContrTipo = trapContrTipoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trapContrTipo);
    }

    /**
     * {@code DELETE  /trap-contr-tipos/:id} : delete the "id" trapContrTipo.
     *
     * @param id the id of the trapContrTipo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/trap-contr-tipos/{id}")
    public ResponseEntity<Void> deleteTrapContrTipo(@PathVariable Long id) {
        log.debug("REST request to delete TrapContrTipo : {}", id);
        trapContrTipoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
