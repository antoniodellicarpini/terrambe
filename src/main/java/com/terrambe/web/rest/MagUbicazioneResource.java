package com.terrambe.web.rest;

import com.terrambe.domain.MagUbicazione;
import com.terrambe.service.MagUbicazioneService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.MagUbicazioneCriteria;
import com.terrambe.service.MagUbicazioneQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.MagUbicazione}.
 */
@RestController
@RequestMapping("/api")
public class MagUbicazioneResource {

    private final Logger log = LoggerFactory.getLogger(MagUbicazioneResource.class);

    private static final String ENTITY_NAME = "magUbicazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MagUbicazioneService magUbicazioneService;

    private final MagUbicazioneQueryService magUbicazioneQueryService;

    public MagUbicazioneResource(MagUbicazioneService magUbicazioneService, MagUbicazioneQueryService magUbicazioneQueryService) {
        this.magUbicazioneService = magUbicazioneService;
        this.magUbicazioneQueryService = magUbicazioneQueryService;
    }

    /**
     * {@code POST  /mag-ubicaziones} : Create a new magUbicazione.
     *
     * @param magUbicazione the magUbicazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new magUbicazione, or with status {@code 400 (Bad Request)} if the magUbicazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mag-ubicaziones")
    public ResponseEntity<MagUbicazione> createMagUbicazione(@Valid @RequestBody MagUbicazione magUbicazione) throws URISyntaxException {
        log.debug("REST request to save MagUbicazione : {}", magUbicazione);
        if (magUbicazione.getId() != null) {
            throw new BadRequestAlertException("A new magUbicazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MagUbicazione result = magUbicazioneService.save(magUbicazione);
        return ResponseEntity.created(new URI("/api/mag-ubicaziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mag-ubicaziones} : Updates an existing magUbicazione.
     *
     * @param magUbicazione the magUbicazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated magUbicazione,
     * or with status {@code 400 (Bad Request)} if the magUbicazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the magUbicazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mag-ubicaziones")
    public ResponseEntity<MagUbicazione> updateMagUbicazione(@Valid @RequestBody MagUbicazione magUbicazione) throws URISyntaxException {
        log.debug("REST request to update MagUbicazione : {}", magUbicazione);
        if (magUbicazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        //EspGio
        MagUbicazione result = magUbicazioneService.save(magUbicazione);
        MagUbicazione result2 =  magUbicazioneService.save(magUbicazione);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, magUbicazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mag-ubicaziones} : get all the magUbicaziones.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of magUbicaziones in body.
     */
    @GetMapping("/mag-ubicaziones")
    public ResponseEntity<List<MagUbicazione>> getAllMagUbicaziones(MagUbicazioneCriteria criteria) {
        log.debug("REST request to get MagUbicaziones by criteria: {}", criteria);
        List<MagUbicazione> entityList = magUbicazioneQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /mag-ubicaziones/count} : count all the magUbicaziones.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/mag-ubicaziones/count")
    public ResponseEntity<Long> countMagUbicaziones(MagUbicazioneCriteria criteria) {
        log.debug("REST request to count MagUbicaziones by criteria: {}", criteria);
        return ResponseEntity.ok().body(magUbicazioneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /mag-ubicaziones/:id} : get the "id" magUbicazione.
     *
     * @param id the id of the magUbicazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the magUbicazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mag-ubicaziones/{id}")
    public ResponseEntity<MagUbicazione> getMagUbicazione(@PathVariable Long id) {
        log.debug("REST request to get MagUbicazione : {}", id);
        Optional<MagUbicazione> magUbicazione = magUbicazioneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(magUbicazione);
    }

    /**
     * {@code DELETE  /mag-ubicaziones/:id} : delete the "id" magUbicazione.
     *
     * @param id the id of the magUbicazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mag-ubicaziones/{id}")
    public ResponseEntity<Void> deleteMagUbicazione(@PathVariable Long id) {
        log.debug("REST request to delete MagUbicazione : {}", id);
        magUbicazioneService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
