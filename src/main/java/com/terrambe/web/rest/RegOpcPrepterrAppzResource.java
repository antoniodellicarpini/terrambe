package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcPrepterrAppz;
import com.terrambe.service.RegOpcPrepterrAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcPrepterrAppzCriteria;
import com.terrambe.service.RegOpcPrepterrAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcPrepterrAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcPrepterrAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrAppzResource.class);

    private static final String ENTITY_NAME = "regOpcPrepterrAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcPrepterrAppzService regOpcPrepterrAppzService;

    private final RegOpcPrepterrAppzQueryService regOpcPrepterrAppzQueryService;

    public RegOpcPrepterrAppzResource(RegOpcPrepterrAppzService regOpcPrepterrAppzService, RegOpcPrepterrAppzQueryService regOpcPrepterrAppzQueryService) {
        this.regOpcPrepterrAppzService = regOpcPrepterrAppzService;
        this.regOpcPrepterrAppzQueryService = regOpcPrepterrAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-prepterr-appzs} : Create a new regOpcPrepterrAppz.
     *
     * @param regOpcPrepterrAppz the regOpcPrepterrAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcPrepterrAppz, or with status {@code 400 (Bad Request)} if the regOpcPrepterrAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-prepterr-appzs")
    public ResponseEntity<RegOpcPrepterrAppz> createRegOpcPrepterrAppz(@Valid @RequestBody RegOpcPrepterrAppz regOpcPrepterrAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcPrepterrAppz : {}", regOpcPrepterrAppz);
        if (regOpcPrepterrAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcPrepterrAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcPrepterrAppz result = regOpcPrepterrAppzService.save(regOpcPrepterrAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-prepterr-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-prepterr-appzs} : Updates an existing regOpcPrepterrAppz.
     *
     * @param regOpcPrepterrAppz the regOpcPrepterrAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcPrepterrAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcPrepterrAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcPrepterrAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-prepterr-appzs")
    public ResponseEntity<RegOpcPrepterrAppz> updateRegOpcPrepterrAppz(@Valid @RequestBody RegOpcPrepterrAppz regOpcPrepterrAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcPrepterrAppz : {}", regOpcPrepterrAppz);
        if (regOpcPrepterrAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcPrepterrAppz result = regOpcPrepterrAppzService.save(regOpcPrepterrAppz);
        RegOpcPrepterrAppz result2 = regOpcPrepterrAppzService.save(regOpcPrepterrAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcPrepterrAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-prepterr-appzs} : get all the regOpcPrepterrAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcPrepterrAppzs in body.
     */
    @GetMapping("/reg-opc-prepterr-appzs")
    public ResponseEntity<List<RegOpcPrepterrAppz>> getAllRegOpcPrepterrAppzs(RegOpcPrepterrAppzCriteria criteria) {
        log.debug("REST request to get RegOpcPrepterrAppzs by criteria: {}", criteria);
        List<RegOpcPrepterrAppz> entityList = regOpcPrepterrAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-prepterr-appzs/count} : count all the regOpcPrepterrAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-prepterr-appzs/count")
    public ResponseEntity<Long> countRegOpcPrepterrAppzs(RegOpcPrepterrAppzCriteria criteria) {
        log.debug("REST request to count RegOpcPrepterrAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcPrepterrAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-prepterr-appzs/:id} : get the "id" regOpcPrepterrAppz.
     *
     * @param id the id of the regOpcPrepterrAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcPrepterrAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-prepterr-appzs/{id}")
    public ResponseEntity<RegOpcPrepterrAppz> getRegOpcPrepterrAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcPrepterrAppz : {}", id);
        Optional<RegOpcPrepterrAppz> regOpcPrepterrAppz = regOpcPrepterrAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcPrepterrAppz);
    }

    /**
     * {@code DELETE  /reg-opc-prepterr-appzs/:id} : delete the "id" regOpcPrepterrAppz.
     *
     * @param id the id of the regOpcPrepterrAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-prepterr-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcPrepterrAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcPrepterrAppz : {}", id);
        regOpcPrepterrAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
