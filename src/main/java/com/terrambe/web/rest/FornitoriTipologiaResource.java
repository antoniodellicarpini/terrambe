package com.terrambe.web.rest;

import com.terrambe.domain.FornitoriTipologia;
import com.terrambe.service.FornitoriTipologiaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FornitoriTipologiaCriteria;
import com.terrambe.service.FornitoriTipologiaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.FornitoriTipologia}.
 */
@RestController
@RequestMapping("/api")
public class FornitoriTipologiaResource {

    private final Logger log = LoggerFactory.getLogger(FornitoriTipologiaResource.class);

    private static final String ENTITY_NAME = "fornitoriTipologia";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FornitoriTipologiaService fornitoriTipologiaService;

    private final FornitoriTipologiaQueryService fornitoriTipologiaQueryService;

    public FornitoriTipologiaResource(FornitoriTipologiaService fornitoriTipologiaService, FornitoriTipologiaQueryService fornitoriTipologiaQueryService) {
        this.fornitoriTipologiaService = fornitoriTipologiaService;
        this.fornitoriTipologiaQueryService = fornitoriTipologiaQueryService;
    }

    /**
     * {@code POST  /fornitori-tipologias} : Create a new fornitoriTipologia.
     *
     * @param fornitoriTipologia the fornitoriTipologia to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fornitoriTipologia, or with status {@code 400 (Bad Request)} if the fornitoriTipologia has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fornitori-tipologias")
    public ResponseEntity<FornitoriTipologia> createFornitoriTipologia(@Valid @RequestBody FornitoriTipologia fornitoriTipologia) throws URISyntaxException {
        log.debug("REST request to save FornitoriTipologia : {}", fornitoriTipologia);
        if (fornitoriTipologia.getId() != null) {
            throw new BadRequestAlertException("A new fornitoriTipologia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FornitoriTipologia result = fornitoriTipologiaService.save(fornitoriTipologia);
        return ResponseEntity.created(new URI("/api/fornitori-tipologias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fornitori-tipologias} : Updates an existing fornitoriTipologia.
     *
     * @param fornitoriTipologia the fornitoriTipologia to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fornitoriTipologia,
     * or with status {@code 400 (Bad Request)} if the fornitoriTipologia is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fornitoriTipologia couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fornitori-tipologias")
    public ResponseEntity<FornitoriTipologia> updateFornitoriTipologia(@Valid @RequestBody FornitoriTipologia fornitoriTipologia) throws URISyntaxException {
        log.debug("REST request to update FornitoriTipologia : {}", fornitoriTipologia);
        if (fornitoriTipologia.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FornitoriTipologia result = fornitoriTipologiaService.save(fornitoriTipologia);
        FornitoriTipologia result2 = fornitoriTipologiaService.save(fornitoriTipologia);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fornitoriTipologia.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fornitori-tipologias} : get all the fornitoriTipologias.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fornitoriTipologias in body.
     */
    @GetMapping("/fornitori-tipologias")
    public ResponseEntity<List<FornitoriTipologia>> getAllFornitoriTipologias(FornitoriTipologiaCriteria criteria) {
        log.debug("REST request to get FornitoriTipologias by criteria: {}", criteria);
        List<FornitoriTipologia> entityList = fornitoriTipologiaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /fornitori-tipologias/count} : count all the fornitoriTipologias.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fornitori-tipologias/count")
    public ResponseEntity<Long> countFornitoriTipologias(FornitoriTipologiaCriteria criteria) {
        log.debug("REST request to count FornitoriTipologias by criteria: {}", criteria);
        return ResponseEntity.ok().body(fornitoriTipologiaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fornitori-tipologias/:id} : get the "id" fornitoriTipologia.
     *
     * @param id the id of the fornitoriTipologia to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fornitoriTipologia, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fornitori-tipologias/{id}")
    public ResponseEntity<FornitoriTipologia> getFornitoriTipologia(@PathVariable Long id) {
        log.debug("REST request to get FornitoriTipologia : {}", id);
        Optional<FornitoriTipologia> fornitoriTipologia = fornitoriTipologiaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fornitoriTipologia);
    }

    /**
     * {@code DELETE  /fornitori-tipologias/:id} : delete the "id" fornitoriTipologia.
     *
     * @param id the id of the fornitoriTipologia to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fornitori-tipologias/{id}")
    public ResponseEntity<Void> deleteFornitoriTipologia(@PathVariable Long id) {
        log.debug("REST request to delete FornitoriTipologia : {}", id);
        fornitoriTipologiaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
