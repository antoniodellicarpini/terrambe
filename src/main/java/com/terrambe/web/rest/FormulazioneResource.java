package com.terrambe.web.rest;

import com.terrambe.domain.Formulazione;
import com.terrambe.service.FormulazioneService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FormulazioneCriteria;
import com.terrambe.service.FormulazioneQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Formulazione}.
 */
@RestController
@RequestMapping("/api")
public class FormulazioneResource {

    private final Logger log = LoggerFactory.getLogger(FormulazioneResource.class);

    private static final String ENTITY_NAME = "formulazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FormulazioneService formulazioneService;

    private final FormulazioneQueryService formulazioneQueryService;

    public FormulazioneResource(FormulazioneService formulazioneService, FormulazioneQueryService formulazioneQueryService) {
        this.formulazioneService = formulazioneService;
        this.formulazioneQueryService = formulazioneQueryService;
    }

    /**
     * {@code POST  /formulaziones} : Create a new formulazione.
     *
     * @param formulazione the formulazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new formulazione, or with status {@code 400 (Bad Request)} if the formulazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/formulaziones")
    public ResponseEntity<Formulazione> createFormulazione(@RequestBody Formulazione formulazione) throws URISyntaxException {
        log.debug("REST request to save Formulazione : {}", formulazione);
        if (formulazione.getId() != null) {
            throw new BadRequestAlertException("A new formulazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Formulazione result = formulazioneService.save(formulazione);
        return ResponseEntity.created(new URI("/api/formulaziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /formulaziones} : Updates an existing formulazione.
     *
     * @param formulazione the formulazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated formulazione,
     * or with status {@code 400 (Bad Request)} if the formulazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the formulazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/formulaziones")
    public ResponseEntity<Formulazione> updateFormulazione(@RequestBody Formulazione formulazione) throws URISyntaxException {
        log.debug("REST request to update Formulazione : {}", formulazione);
        if (formulazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Formulazione result = formulazioneService.save(formulazione);
        Formulazione result2 = formulazioneService.save(formulazione);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, formulazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /formulaziones} : get all the formulaziones.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of formulaziones in body.
     */
    @GetMapping("/formulaziones")
    public ResponseEntity<List<Formulazione>> getAllFormulaziones(FormulazioneCriteria criteria) {
        log.debug("REST request to get Formulaziones by criteria: {}", criteria);
        List<Formulazione> entityList = formulazioneQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /formulaziones/count} : count all the formulaziones.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/formulaziones/count")
    public ResponseEntity<Long> countFormulaziones(FormulazioneCriteria criteria) {
        log.debug("REST request to count Formulaziones by criteria: {}", criteria);
        return ResponseEntity.ok().body(formulazioneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /formulaziones/:id} : get the "id" formulazione.
     *
     * @param id the id of the formulazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the formulazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/formulaziones/{id}")
    public ResponseEntity<Formulazione> getFormulazione(@PathVariable Long id) {
        log.debug("REST request to get Formulazione : {}", id);
        Optional<Formulazione> formulazione = formulazioneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(formulazione);
    }

    /**
     * {@code DELETE  /formulaziones/:id} : delete the "id" formulazione.
     *
     * @param id the id of the formulazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/formulaziones/{id}")
    public ResponseEntity<Void> deleteFormulazione(@PathVariable Long id) {
        log.debug("REST request to delete Formulazione : {}", id);
        formulazioneService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
