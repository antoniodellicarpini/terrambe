package com.terrambe.web.rest;

import com.terrambe.domain.FertTipo;
import com.terrambe.service.FertTipoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FertTipoCriteria;
import com.terrambe.service.FertTipoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.FertTipo}.
 */
@RestController
@RequestMapping("/api")
public class FertTipoResource {

    private final Logger log = LoggerFactory.getLogger(FertTipoResource.class);

    private static final String ENTITY_NAME = "fertTipo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FertTipoService fertTipoService;

    private final FertTipoQueryService fertTipoQueryService;

    public FertTipoResource(FertTipoService fertTipoService, FertTipoQueryService fertTipoQueryService) {
        this.fertTipoService = fertTipoService;
        this.fertTipoQueryService = fertTipoQueryService;
    }

    /**
     * {@code POST  /fert-tipos} : Create a new fertTipo.
     *
     * @param fertTipo the fertTipo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fertTipo, or with status {@code 400 (Bad Request)} if the fertTipo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fert-tipos")
    public ResponseEntity<FertTipo> createFertTipo(@RequestBody FertTipo fertTipo) throws URISyntaxException {
        log.debug("REST request to save FertTipo : {}", fertTipo);
        if (fertTipo.getId() != null) {
            throw new BadRequestAlertException("A new fertTipo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FertTipo result = fertTipoService.save(fertTipo);
        return ResponseEntity.created(new URI("/api/fert-tipos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fert-tipos} : Updates an existing fertTipo.
     *
     * @param fertTipo the fertTipo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fertTipo,
     * or with status {@code 400 (Bad Request)} if the fertTipo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fertTipo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fert-tipos")
    public ResponseEntity<FertTipo> updateFertTipo(@RequestBody FertTipo fertTipo) throws URISyntaxException {
        log.debug("REST request to update FertTipo : {}", fertTipo);
        if (fertTipo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FertTipo result = fertTipoService.save(fertTipo);
        FertTipo result2 = fertTipoService.save(fertTipo);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fertTipo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fert-tipos} : get all the fertTipos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fertTipos in body.
     */
    @GetMapping("/fert-tipos")
    public ResponseEntity<List<FertTipo>> getAllFertTipos(FertTipoCriteria criteria) {
        log.debug("REST request to get FertTipos by criteria: {}", criteria);
        List<FertTipo> entityList = fertTipoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /fert-tipos/count} : count all the fertTipos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fert-tipos/count")
    public ResponseEntity<Long> countFertTipos(FertTipoCriteria criteria) {
        log.debug("REST request to count FertTipos by criteria: {}", criteria);
        return ResponseEntity.ok().body(fertTipoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fert-tipos/:id} : get the "id" fertTipo.
     *
     * @param id the id of the fertTipo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fertTipo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fert-tipos/{id}")
    public ResponseEntity<FertTipo> getFertTipo(@PathVariable Long id) {
        log.debug("REST request to get FertTipo : {}", id);
        Optional<FertTipo> fertTipo = fertTipoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fertTipo);
    }

    /**
     * {@code DELETE  /fert-tipos/:id} : delete the "id" fertTipo.
     *
     * @param id the id of the fertTipo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fert-tipos/{id}")
    public ResponseEntity<Void> deleteFertTipo(@PathVariable Long id) {
        log.debug("REST request to delete FertTipo : {}", id);
        fertTipoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
