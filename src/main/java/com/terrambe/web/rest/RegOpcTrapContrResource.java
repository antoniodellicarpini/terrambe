package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.service.RegOpcTrapContrService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcTrapContrCriteria;
import com.terrambe.service.RegOpcTrapContrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcTrapContr}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcTrapContrResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapContrResource.class);

    private static final String ENTITY_NAME = "regOpcTrapContr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcTrapContrService regOpcTrapContrService;

    private final RegOpcTrapContrQueryService regOpcTrapContrQueryService;

    public RegOpcTrapContrResource(RegOpcTrapContrService regOpcTrapContrService, RegOpcTrapContrQueryService regOpcTrapContrQueryService) {
        this.regOpcTrapContrService = regOpcTrapContrService;
        this.regOpcTrapContrQueryService = regOpcTrapContrQueryService;
    }

    /**
     * {@code POST  /reg-opc-trap-contrs} : Create a new regOpcTrapContr.
     *
     * @param regOpcTrapContr the regOpcTrapContr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcTrapContr, or with status {@code 400 (Bad Request)} if the regOpcTrapContr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-trap-contrs")
    public ResponseEntity<RegOpcTrapContr> createRegOpcTrapContr(@Valid @RequestBody RegOpcTrapContr regOpcTrapContr) throws URISyntaxException {
        log.debug("REST request to save RegOpcTrapContr : {}", regOpcTrapContr);
        if (regOpcTrapContr.getId() != null) {
            throw new BadRequestAlertException("A new regOpcTrapContr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcTrapContr result = regOpcTrapContrService.save(regOpcTrapContr);
        return ResponseEntity.created(new URI("/api/reg-opc-trap-contrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-trap-contrs} : Updates an existing regOpcTrapContr.
     *
     * @param regOpcTrapContr the regOpcTrapContr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcTrapContr,
     * or with status {@code 400 (Bad Request)} if the regOpcTrapContr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcTrapContr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-trap-contrs")
    public ResponseEntity<RegOpcTrapContr> updateRegOpcTrapContr(@Valid @RequestBody RegOpcTrapContr regOpcTrapContr) throws URISyntaxException {
        log.debug("REST request to update RegOpcTrapContr : {}", regOpcTrapContr);
        if (regOpcTrapContr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcTrapContr result = regOpcTrapContrService.save(regOpcTrapContr);
        RegOpcTrapContr result2 = regOpcTrapContrService.save(regOpcTrapContr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcTrapContr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-trap-contrs} : get all the regOpcTrapContrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcTrapContrs in body.
     */
    @GetMapping("/reg-opc-trap-contrs")
    public ResponseEntity<List<RegOpcTrapContr>> getAllRegOpcTrapContrs(RegOpcTrapContrCriteria criteria) {
        log.debug("REST request to get RegOpcTrapContrs by criteria: {}", criteria);
        List<RegOpcTrapContr> entityList = regOpcTrapContrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-trap-contrs/count} : count all the regOpcTrapContrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-trap-contrs/count")
    public ResponseEntity<Long> countRegOpcTrapContrs(RegOpcTrapContrCriteria criteria) {
        log.debug("REST request to count RegOpcTrapContrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcTrapContrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-trap-contrs/:id} : get the "id" regOpcTrapContr.
     *
     * @param id the id of the regOpcTrapContr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcTrapContr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-trap-contrs/{id}")
    public ResponseEntity<RegOpcTrapContr> getRegOpcTrapContr(@PathVariable Long id) {
        log.debug("REST request to get RegOpcTrapContr : {}", id);
        Optional<RegOpcTrapContr> regOpcTrapContr = regOpcTrapContrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcTrapContr);
    }

    /**
     * {@code DELETE  /reg-opc-trap-contrs/:id} : delete the "id" regOpcTrapContr.
     *
     * @param id the id of the regOpcTrapContr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-trap-contrs/{id}")
    public ResponseEntity<Void> deleteRegOpcTrapContr(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcTrapContr : {}", id);
        regOpcTrapContrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
