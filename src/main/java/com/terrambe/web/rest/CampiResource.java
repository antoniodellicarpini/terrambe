package com.terrambe.web.rest;

    import com.terrambe.domain.Campi;
    import com.terrambe.domain.AziSuperficieParticella;
    import com.terrambe.service.CampiService;
    import com.terrambe.service.AziSuperficieParticellaService;
    import com.terrambe.web.rest.errors.BadRequestAlertException;
    import com.terrambe.service.dto.CampiCriteria;
    import com.terrambe.service.CampiQueryService;

    import io.github.jhipster.web.util.HeaderUtil;
    import io.github.jhipster.web.util.ResponseUtil;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.boot.configurationprocessor.json.JSONException;
    import org.springframework.boot.configurationprocessor.json.JSONObject;
    import org.springframework.http.ResponseEntity;
    import org.springframework.web.bind.annotation.*;

    import javax.validation.Valid;
    import java.net.URI;
    import java.net.URISyntaxException;
    import java.util.Set;

    import java.util.List;
    import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Campi}.
 */
@RestController
@RequestMapping("/api")
public class CampiResource {

    private final Logger log = LoggerFactory.getLogger(CampiResource.class);

    private static final String ENTITY_NAME = "campi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CampiService campiService;

    private final CampiQueryService campiQueryService;

    @Autowired
    private AziSuperficieParticellaService particellaService;


    public CampiResource(CampiService campiService, CampiQueryService campiQueryService) {
        this.campiService = campiService;
        this.campiQueryService = campiQueryService;
    }

    /**
     * {@code POST  /campis} : Create a new campi.
     *
     * @param campi the campi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new campi, or with status {@code 400 (Bad Request)} if the campi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/campis")
    public ResponseEntity<Campi> createCampi(@Valid @RequestBody Campi campi) throws URISyntaxException {
        log.debug("REST request to save Campi : {}", campi);
        if (campi.getId() != null) {
            throw new BadRequestAlertException("A new campi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        //EspGio
        //Creo una collezione di particelle per contenere quelle passate in fase di post come array nel json
        Set<AziSuperficieParticella> superfici = campi.getCampiToAziSups();
        Campi result = campiService.save(campi);

        log.debug("id del campo" +campi.getId());

        //Scorro la collezione di particelle per poter ottenere i diversi id inseriti
        for (AziSuperficieParticella superfice :superfici){
            //
            log.debug("superficie id" +superfice.getId());
            log.debug("campo id " +result.getId());
            //
            String idCampo = result.getId().toString();
            String idParticella = superfice.getId().toString();
            //richiamo il metodo per eseguire l'update (aggiunta dell'id del campo nell iesima particella)
            //le due variabili sono state create solo per semplificare la lettura a qualcuno meno esperto, ovviamente potevo passare anche il get al metodo
            particellaService.addCampiIdToExistingParticella(idCampo,idParticella);
        }
        return ResponseEntity.created(new URI("/api/campis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /campis} : Updates an existing campi.
     *
     * @param campi the campi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated campi,
     * or with status {@code 400 (Bad Request)} if the campi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the campi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/campis")
    public ResponseEntity<Campi> updateCampi(@Valid @RequestBody Campi campi) throws URISyntaxException {
        log.debug("REST request to update Campi : {}", campi);
        if (campi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Campi result = campiService.save(campi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, campi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /campis} : get all the campis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of campis in body.
     */
    @GetMapping("/campis")
    public ResponseEntity<List<Campi>> getAllCampis(CampiCriteria criteria) {
        log.debug("REST request to get Campis by criteria: {}", criteria);
        List<Campi> entityList = campiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /campis/count} : count all the campis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/campis/count")
    public ResponseEntity<Long> countCampis(CampiCriteria criteria) {
        log.debug("REST request to count Campis by criteria: {}", criteria);
        return ResponseEntity.ok().body(campiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /campis/:id} : get the "id" campi.
     *
     * @param id the id of the campi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the campi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/campis/{id}")
    public ResponseEntity<Campi> getCampi(@PathVariable Long id) {
        log.debug("REST request to get Campi : {}", id);
        Optional<Campi> campi = campiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(campi);
    }

    /**
     * {@code DELETE  /campis/:id} : delete the "id" campi.
     *
     * @param id the id of the campi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/campis/{id}")
    public ResponseEntity<Void> deleteCampi(@PathVariable Long id) {
        log.debug("REST request to delete Campi : {}", id);

        Set<AziSuperficieParticella> superfici = campiService.findOne(id).get().getCampiToAziSups();
        for (AziSuperficieParticella superfice :superfici){
            log.debug("Sto selgando la particella" +superfice.getId() + "dal campo");
            superfice.setAziSupToCampi(null);
        }
        campiService.delete(id);

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
