package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcPio;
import com.terrambe.service.RegOpcPioService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcPioCriteria;
import com.terrambe.service.RegOpcPioQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcPio}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcPioResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioResource.class);

    private static final String ENTITY_NAME = "regOpcPio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcPioService regOpcPioService;

    private final RegOpcPioQueryService regOpcPioQueryService;

    public RegOpcPioResource(RegOpcPioService regOpcPioService, RegOpcPioQueryService regOpcPioQueryService) {
        this.regOpcPioService = regOpcPioService;
        this.regOpcPioQueryService = regOpcPioQueryService;
    }

    /**
     * {@code POST  /reg-opc-pios} : Create a new regOpcPio.
     *
     * @param regOpcPio the regOpcPio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcPio, or with status {@code 400 (Bad Request)} if the regOpcPio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-pios")
    public ResponseEntity<RegOpcPio> createRegOpcPio(@Valid @RequestBody RegOpcPio regOpcPio) throws URISyntaxException {
        log.debug("REST request to save RegOpcPio : {}", regOpcPio);
        if (regOpcPio.getId() != null) {
            throw new BadRequestAlertException("A new regOpcPio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcPio result = regOpcPioService.save(regOpcPio);
        return ResponseEntity.created(new URI("/api/reg-opc-pios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-pios} : Updates an existing regOpcPio.
     *
     * @param regOpcPio the regOpcPio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcPio,
     * or with status {@code 400 (Bad Request)} if the regOpcPio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcPio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-pios")
    public ResponseEntity<RegOpcPio> updateRegOpcPio(@Valid @RequestBody RegOpcPio regOpcPio) throws URISyntaxException {
        log.debug("REST request to update RegOpcPio : {}", regOpcPio);
        if (regOpcPio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcPio result = regOpcPioService.save(regOpcPio);
        RegOpcPio result2 = regOpcPioService.save(regOpcPio);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcPio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-pios} : get all the regOpcPios.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcPios in body.
     */
    @GetMapping("/reg-opc-pios")
    public ResponseEntity<List<RegOpcPio>> getAllRegOpcPios(RegOpcPioCriteria criteria) {
        log.debug("REST request to get RegOpcPios by criteria: {}", criteria);
        List<RegOpcPio> entityList = regOpcPioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-pios/count} : count all the regOpcPios.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-pios/count")
    public ResponseEntity<Long> countRegOpcPios(RegOpcPioCriteria criteria) {
        log.debug("REST request to count RegOpcPios by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcPioQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-pios/:id} : get the "id" regOpcPio.
     *
     * @param id the id of the regOpcPio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcPio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-pios/{id}")
    public ResponseEntity<RegOpcPio> getRegOpcPio(@PathVariable Long id) {
        log.debug("REST request to get RegOpcPio : {}", id);
        Optional<RegOpcPio> regOpcPio = regOpcPioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcPio);
    }

    /**
     * {@code DELETE  /reg-opc-pios/:id} : delete the "id" regOpcPio.
     *
     * @param id the id of the regOpcPio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-pios/{id}")
    public ResponseEntity<Void> deleteRegOpcPio(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcPio : {}", id);
        regOpcPioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
