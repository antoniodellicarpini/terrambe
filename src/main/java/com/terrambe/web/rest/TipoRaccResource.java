package com.terrambe.web.rest;

import com.terrambe.domain.TipoRacc;
import com.terrambe.service.TipoRaccService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoRaccCriteria;
import com.terrambe.service.TipoRaccQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoRacc}.
 */
@RestController
@RequestMapping("/api")
public class TipoRaccResource {

    private final Logger log = LoggerFactory.getLogger(TipoRaccResource.class);

    private static final String ENTITY_NAME = "tipoRacc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoRaccService tipoRaccService;

    private final TipoRaccQueryService tipoRaccQueryService;

    public TipoRaccResource(TipoRaccService tipoRaccService, TipoRaccQueryService tipoRaccQueryService) {
        this.tipoRaccService = tipoRaccService;
        this.tipoRaccQueryService = tipoRaccQueryService;
    }

    /**
     * {@code POST  /tipo-raccs} : Create a new tipoRacc.
     *
     * @param tipoRacc the tipoRacc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoRacc, or with status {@code 400 (Bad Request)} if the tipoRacc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-raccs")
    public ResponseEntity<TipoRacc> createTipoRacc(@RequestBody TipoRacc tipoRacc) throws URISyntaxException {
        log.debug("REST request to save TipoRacc : {}", tipoRacc);
        if (tipoRacc.getId() != null) {
            throw new BadRequestAlertException("A new tipoRacc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoRacc result = tipoRaccService.save(tipoRacc);
        return ResponseEntity.created(new URI("/api/tipo-raccs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-raccs} : Updates an existing tipoRacc.
     *
     * @param tipoRacc the tipoRacc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoRacc,
     * or with status {@code 400 (Bad Request)} if the tipoRacc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoRacc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-raccs")
    public ResponseEntity<TipoRacc> updateTipoRacc(@RequestBody TipoRacc tipoRacc) throws URISyntaxException {
        log.debug("REST request to update TipoRacc : {}", tipoRacc);
        if (tipoRacc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoRacc result = tipoRaccService.save(tipoRacc);
        TipoRacc result2 = tipoRaccService.save(tipoRacc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoRacc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-raccs} : get all the tipoRaccs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoRaccs in body.
     */
    @GetMapping("/tipo-raccs")
    public ResponseEntity<List<TipoRacc>> getAllTipoRaccs(TipoRaccCriteria criteria) {
        log.debug("REST request to get TipoRaccs by criteria: {}", criteria);
        List<TipoRacc> entityList = tipoRaccQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-raccs/count} : count all the tipoRaccs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-raccs/count")
    public ResponseEntity<Long> countTipoRaccs(TipoRaccCriteria criteria) {
        log.debug("REST request to count TipoRaccs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoRaccQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-raccs/:id} : get the "id" tipoRacc.
     *
     * @param id the id of the tipoRacc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoRacc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-raccs/{id}")
    public ResponseEntity<TipoRacc> getTipoRacc(@PathVariable Long id) {
        log.debug("REST request to get TipoRacc : {}", id);
        Optional<TipoRacc> tipoRacc = tipoRaccService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoRacc);
    }

    /**
     * {@code DELETE  /tipo-raccs/:id} : delete the "id" tipoRacc.
     *
     * @param id the id of the tipoRacc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-raccs/{id}")
    public ResponseEntity<Void> deleteTipoRacc(@PathVariable Long id) {
        log.debug("REST request to delete TipoRacc : {}", id);
        tipoRaccService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
