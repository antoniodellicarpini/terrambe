package com.terrambe.web.rest;

import com.terrambe.domain.RecAziRapCli;
import com.terrambe.service.RecAziRapCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecAziRapCliCriteria;
import com.terrambe.service.RecAziRapCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecAziRapCli}.
 */
@RestController
@RequestMapping("/api")
public class RecAziRapCliResource {

    private final Logger log = LoggerFactory.getLogger(RecAziRapCliResource.class);

    private static final String ENTITY_NAME = "recAziRapCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecAziRapCliService recAziRapCliService;

    private final RecAziRapCliQueryService recAziRapCliQueryService;

    public RecAziRapCliResource(RecAziRapCliService recAziRapCliService, RecAziRapCliQueryService recAziRapCliQueryService) {
        this.recAziRapCliService = recAziRapCliService;
        this.recAziRapCliQueryService = recAziRapCliQueryService;
    }

    /**
     * {@code POST  /rec-azi-rap-clis} : Create a new recAziRapCli.
     *
     * @param recAziRapCli the recAziRapCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recAziRapCli, or with status {@code 400 (Bad Request)} if the recAziRapCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-azi-rap-clis")
    public ResponseEntity<RecAziRapCli> createRecAziRapCli(@RequestBody RecAziRapCli recAziRapCli) throws URISyntaxException {
        log.debug("REST request to save RecAziRapCli : {}", recAziRapCli);
        if (recAziRapCli.getId() != null) {
            throw new BadRequestAlertException("A new recAziRapCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecAziRapCli result = recAziRapCliService.save(recAziRapCli);
        return ResponseEntity.created(new URI("/api/rec-azi-rap-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-azi-rap-clis} : Updates an existing recAziRapCli.
     *
     * @param recAziRapCli the recAziRapCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recAziRapCli,
     * or with status {@code 400 (Bad Request)} if the recAziRapCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recAziRapCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-azi-rap-clis")
    public ResponseEntity<RecAziRapCli> updateRecAziRapCli(@RequestBody RecAziRapCli recAziRapCli) throws URISyntaxException {
        log.debug("REST request to update RecAziRapCli : {}", recAziRapCli);
        if (recAziRapCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecAziRapCli result = recAziRapCliService.save(recAziRapCli);
        RecAziRapCli result2 = recAziRapCliService.save(recAziRapCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recAziRapCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-azi-rap-clis} : get all the recAziRapClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recAziRapClis in body.
     */
    @GetMapping("/rec-azi-rap-clis")
    public ResponseEntity<List<RecAziRapCli>> getAllRecAziRapClis(RecAziRapCliCriteria criteria) {
        log.debug("REST request to get RecAziRapClis by criteria: {}", criteria);
        List<RecAziRapCli> entityList = recAziRapCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-azi-rap-clis/count} : count all the recAziRapClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-azi-rap-clis/count")
    public ResponseEntity<Long> countRecAziRapClis(RecAziRapCliCriteria criteria) {
        log.debug("REST request to count RecAziRapClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(recAziRapCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-azi-rap-clis/:id} : get the "id" recAziRapCli.
     *
     * @param id the id of the recAziRapCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recAziRapCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-azi-rap-clis/{id}")
    public ResponseEntity<RecAziRapCli> getRecAziRapCli(@PathVariable Long id) {
        log.debug("REST request to get RecAziRapCli : {}", id);
        Optional<RecAziRapCli> recAziRapCli = recAziRapCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recAziRapCli);
    }

    /**
     * {@code DELETE  /rec-azi-rap-clis/:id} : delete the "id" recAziRapCli.
     *
     * @param id the id of the recAziRapCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-azi-rap-clis/{id}")
    public ResponseEntity<Void> deleteRecAziRapCli(@PathVariable Long id) {
        log.debug("REST request to delete RecAziRapCli : {}", id);
        recAziRapCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
