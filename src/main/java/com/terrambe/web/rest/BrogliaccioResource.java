package com.terrambe.web.rest;

import com.terrambe.domain.Brogliaccio;
import com.terrambe.service.BrogliaccioService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.BrogliaccioCriteria;
import com.terrambe.service.BrogliaccioQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Brogliaccio}.
 */
@RestController
@RequestMapping("/api")
public class BrogliaccioResource {

    private final Logger log = LoggerFactory.getLogger(BrogliaccioResource.class);

    private static final String ENTITY_NAME = "brogliaccio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BrogliaccioService brogliaccioService;

    private final BrogliaccioQueryService brogliaccioQueryService;

    public BrogliaccioResource(BrogliaccioService brogliaccioService, BrogliaccioQueryService brogliaccioQueryService) {
        this.brogliaccioService = brogliaccioService;
        this.brogliaccioQueryService = brogliaccioQueryService;
    }

    /**
     * {@code POST  /brogliaccios} : Create a new brogliaccio.
     *
     * @param brogliaccio the brogliaccio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new brogliaccio, or with status {@code 400 (Bad Request)} if the brogliaccio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/brogliaccios")
    public ResponseEntity<Brogliaccio> createBrogliaccio(@Valid @RequestBody Brogliaccio brogliaccio) throws URISyntaxException {
        log.debug("REST request to save Brogliaccio : {}", brogliaccio);
        if (brogliaccio.getId() != null) {
            throw new BadRequestAlertException("A new brogliaccio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Brogliaccio result = brogliaccioService.save(brogliaccio);
        return ResponseEntity.created(new URI("/api/brogliaccios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /brogliaccios} : Updates an existing brogliaccio.
     *
     * @param brogliaccio the brogliaccio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated brogliaccio,
     * or with status {@code 400 (Bad Request)} if the brogliaccio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the brogliaccio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/brogliaccios")
    public ResponseEntity<Brogliaccio> updateBrogliaccio(@Valid @RequestBody Brogliaccio brogliaccio) throws URISyntaxException {
        log.debug("REST request to update Brogliaccio : {}", brogliaccio);
        if (brogliaccio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Brogliaccio result = brogliaccioService.save(brogliaccio);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, brogliaccio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /brogliaccios} : get all the brogliaccios.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of brogliaccios in body.
     */
    @GetMapping("/brogliaccios")
    public ResponseEntity<List<Brogliaccio>> getAllBrogliaccios(BrogliaccioCriteria criteria) {
        log.debug("REST request to get Brogliaccios by criteria: {}", criteria);
        List<Brogliaccio> entityList = brogliaccioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /brogliaccios/count} : count all the brogliaccios.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/brogliaccios/count")
    public ResponseEntity<Long> countBrogliaccios(BrogliaccioCriteria criteria) {
        log.debug("REST request to count Brogliaccios by criteria: {}", criteria);
        return ResponseEntity.ok().body(brogliaccioQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /brogliaccios/:id} : get the "id" brogliaccio.
     *
     * @param id the id of the brogliaccio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the brogliaccio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/brogliaccios/{id}")
    public ResponseEntity<Brogliaccio> getBrogliaccio(@PathVariable Long id) {
        log.debug("REST request to get Brogliaccio : {}", id);
        Optional<Brogliaccio> brogliaccio = brogliaccioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(brogliaccio);
    }

    /**
     * {@code DELETE  /brogliaccios/:id} : delete the "id" brogliaccio.
     *
     * @param id the id of the brogliaccio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/brogliaccios/{id}")
    public ResponseEntity<Void> deleteBrogliaccio(@PathVariable Long id) {
        log.debug("REST request to delete Brogliaccio : {}", id);
        brogliaccioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
