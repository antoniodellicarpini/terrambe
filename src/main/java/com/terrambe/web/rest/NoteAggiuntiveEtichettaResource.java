package com.terrambe.web.rest;

import com.terrambe.domain.NoteAggiuntiveEtichetta;
import com.terrambe.service.NoteAggiuntiveEtichettaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.NoteAggiuntiveEtichettaCriteria;
import com.terrambe.service.NoteAggiuntiveEtichettaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.NoteAggiuntiveEtichetta}.
 */
@RestController
@RequestMapping("/api")
public class NoteAggiuntiveEtichettaResource {

    private final Logger log = LoggerFactory.getLogger(NoteAggiuntiveEtichettaResource.class);

    private static final String ENTITY_NAME = "noteAggiuntiveEtichetta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NoteAggiuntiveEtichettaService noteAggiuntiveEtichettaService;

    private final NoteAggiuntiveEtichettaQueryService noteAggiuntiveEtichettaQueryService;

    public NoteAggiuntiveEtichettaResource(NoteAggiuntiveEtichettaService noteAggiuntiveEtichettaService, NoteAggiuntiveEtichettaQueryService noteAggiuntiveEtichettaQueryService) {
        this.noteAggiuntiveEtichettaService = noteAggiuntiveEtichettaService;
        this.noteAggiuntiveEtichettaQueryService = noteAggiuntiveEtichettaQueryService;
    }

    /**
     * {@code POST  /note-aggiuntive-etichettas} : Create a new noteAggiuntiveEtichetta.
     *
     * @param noteAggiuntiveEtichetta the noteAggiuntiveEtichetta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new noteAggiuntiveEtichetta, or with status {@code 400 (Bad Request)} if the noteAggiuntiveEtichetta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/note-aggiuntive-etichettas")
    public ResponseEntity<NoteAggiuntiveEtichetta> createNoteAggiuntiveEtichetta(@RequestBody NoteAggiuntiveEtichetta noteAggiuntiveEtichetta) throws URISyntaxException {
        log.debug("REST request to save NoteAggiuntiveEtichetta : {}", noteAggiuntiveEtichetta);
        if (noteAggiuntiveEtichetta.getId() != null) {
            throw new BadRequestAlertException("A new noteAggiuntiveEtichetta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NoteAggiuntiveEtichetta result = noteAggiuntiveEtichettaService.save(noteAggiuntiveEtichetta);
        return ResponseEntity.created(new URI("/api/note-aggiuntive-etichettas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /note-aggiuntive-etichettas} : Updates an existing noteAggiuntiveEtichetta.
     *
     * @param noteAggiuntiveEtichetta the noteAggiuntiveEtichetta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated noteAggiuntiveEtichetta,
     * or with status {@code 400 (Bad Request)} if the noteAggiuntiveEtichetta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the noteAggiuntiveEtichetta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/note-aggiuntive-etichettas")
    public ResponseEntity<NoteAggiuntiveEtichetta> updateNoteAggiuntiveEtichetta(@RequestBody NoteAggiuntiveEtichetta noteAggiuntiveEtichetta) throws URISyntaxException {
        log.debug("REST request to update NoteAggiuntiveEtichetta : {}", noteAggiuntiveEtichetta);
        if (noteAggiuntiveEtichetta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NoteAggiuntiveEtichetta result = noteAggiuntiveEtichettaService.save(noteAggiuntiveEtichetta);
        NoteAggiuntiveEtichetta result2 = noteAggiuntiveEtichettaService.save(noteAggiuntiveEtichetta);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, noteAggiuntiveEtichetta.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /note-aggiuntive-etichettas} : get all the noteAggiuntiveEtichettas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of noteAggiuntiveEtichettas in body.
     */
    @GetMapping("/note-aggiuntive-etichettas")
    public ResponseEntity<List<NoteAggiuntiveEtichetta>> getAllNoteAggiuntiveEtichettas(NoteAggiuntiveEtichettaCriteria criteria) {
        log.debug("REST request to get NoteAggiuntiveEtichettas by criteria: {}", criteria);
        List<NoteAggiuntiveEtichetta> entityList = noteAggiuntiveEtichettaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /note-aggiuntive-etichettas/count} : count all the noteAggiuntiveEtichettas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/note-aggiuntive-etichettas/count")
    public ResponseEntity<Long> countNoteAggiuntiveEtichettas(NoteAggiuntiveEtichettaCriteria criteria) {
        log.debug("REST request to count NoteAggiuntiveEtichettas by criteria: {}", criteria);
        return ResponseEntity.ok().body(noteAggiuntiveEtichettaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /note-aggiuntive-etichettas/:id} : get the "id" noteAggiuntiveEtichetta.
     *
     * @param id the id of the noteAggiuntiveEtichetta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the noteAggiuntiveEtichetta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/note-aggiuntive-etichettas/{id}")
    public ResponseEntity<NoteAggiuntiveEtichetta> getNoteAggiuntiveEtichetta(@PathVariable Long id) {
        log.debug("REST request to get NoteAggiuntiveEtichetta : {}", id);
        Optional<NoteAggiuntiveEtichetta> noteAggiuntiveEtichetta = noteAggiuntiveEtichettaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(noteAggiuntiveEtichetta);
    }

    /**
     * {@code DELETE  /note-aggiuntive-etichettas/:id} : delete the "id" noteAggiuntiveEtichetta.
     *
     * @param id the id of the noteAggiuntiveEtichetta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/note-aggiuntive-etichettas/{id}")
    public ResponseEntity<Void> deleteNoteAggiuntiveEtichetta(@PathVariable Long id) {
        log.debug("REST request to delete NoteAggiuntiveEtichetta : {}", id);
        noteAggiuntiveEtichettaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
