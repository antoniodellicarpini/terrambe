package com.terrambe.web.rest;

import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.service.Agea20152020BDFService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.Agea20152020BDFCriteria;
import com.terrambe.service.Agea20152020BDFQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Agea20152020BDF}.
 */
@RestController
@RequestMapping("/api")
public class Agea20152020BDFResource {

    private final Logger log = LoggerFactory.getLogger(Agea20152020BDFResource.class);

    private static final String ENTITY_NAME = "agea20152020BDF";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Agea20152020BDFService agea20152020BDFService;

    private final Agea20152020BDFQueryService agea20152020BDFQueryService;

    public Agea20152020BDFResource(Agea20152020BDFService agea20152020BDFService, Agea20152020BDFQueryService agea20152020BDFQueryService) {
        this.agea20152020BDFService = agea20152020BDFService;
        this.agea20152020BDFQueryService = agea20152020BDFQueryService;
    }

    /**
     * {@code POST  /agea-20152020-bdfs} : Create a new agea20152020BDF.
     *
     * @param agea20152020BDF the agea20152020BDF to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agea20152020BDF, or with status {@code 400 (Bad Request)} if the agea20152020BDF has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agea-20152020-bdfs")
    public ResponseEntity<Agea20152020BDF> createAgea20152020BDF(@RequestBody Agea20152020BDF agea20152020BDF) throws URISyntaxException {
        log.debug("REST request to save Agea20152020BDF : {}", agea20152020BDF);
        if (agea20152020BDF.getId() != null) {
            throw new BadRequestAlertException("A new agea20152020BDF cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Agea20152020BDF result = agea20152020BDFService.save(agea20152020BDF);
        return ResponseEntity.created(new URI("/api/agea-20152020-bdfs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agea-20152020-bdfs} : Updates an existing agea20152020BDF.
     *
     * @param agea20152020BDF the agea20152020BDF to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agea20152020BDF,
     * or with status {@code 400 (Bad Request)} if the agea20152020BDF is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agea20152020BDF couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agea-20152020-bdfs")
    public ResponseEntity<Agea20152020BDF> updateAgea20152020BDF(@RequestBody Agea20152020BDF agea20152020BDF) throws URISyntaxException {
        log.debug("REST request to update Agea20152020BDF : {}", agea20152020BDF);
        if (agea20152020BDF.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Agea20152020BDF result = agea20152020BDFService.save(agea20152020BDF);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agea20152020BDF.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agea-20152020-bdfs} : get all the agea20152020BDFS.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agea20152020BDFS in body.
     */
    @GetMapping("/agea-20152020-bdfs")
    public ResponseEntity<List<Agea20152020BDF>> getAllAgea20152020BDFS(Agea20152020BDFCriteria criteria) {
        log.debug("REST request to get Agea20152020BDFS by criteria: {}", criteria);
        List<Agea20152020BDF> entityList = agea20152020BDFQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /agea-20152020-bdfs/count} : count all the agea20152020BDFS.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/agea-20152020-bdfs/count")
    public ResponseEntity<Long> countAgea20152020BDFS(Agea20152020BDFCriteria criteria) {
        log.debug("REST request to count Agea20152020BDFS by criteria: {}", criteria);
        return ResponseEntity.ok().body(agea20152020BDFQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /agea-20152020-bdfs/:id} : get the "id" agea20152020BDF.
     *
     * @param id the id of the agea20152020BDF to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agea20152020BDF, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agea-20152020-bdfs/{id}")
    public ResponseEntity<Agea20152020BDF> getAgea20152020BDF(@PathVariable Long id) {
        log.debug("REST request to get Agea20152020BDF : {}", id);
        Optional<Agea20152020BDF> agea20152020BDF = agea20152020BDFService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agea20152020BDF);
    }

    /**
     * {@code DELETE  /agea-20152020-bdfs/:id} : delete the "id" agea20152020BDF.
     *
     * @param id the id of the agea20152020BDF to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agea-20152020-bdfs/{id}")
    public ResponseEntity<Void> deleteAgea20152020BDF(@PathVariable Long id) {
        log.debug("REST request to delete Agea20152020BDF : {}", id);
        agea20152020BDFService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
