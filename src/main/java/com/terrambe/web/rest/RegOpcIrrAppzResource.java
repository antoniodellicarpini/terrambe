package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcIrrAppz;
import com.terrambe.service.RegOpcIrrAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcIrrAppzCriteria;
import com.terrambe.service.RegOpcIrrAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcIrrAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcIrrAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcIrrAppzResource.class);

    private static final String ENTITY_NAME = "regOpcIrrAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcIrrAppzService regOpcIrrAppzService;

    private final RegOpcIrrAppzQueryService regOpcIrrAppzQueryService;

    public RegOpcIrrAppzResource(RegOpcIrrAppzService regOpcIrrAppzService, RegOpcIrrAppzQueryService regOpcIrrAppzQueryService) {
        this.regOpcIrrAppzService = regOpcIrrAppzService;
        this.regOpcIrrAppzQueryService = regOpcIrrAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-irr-appzs} : Create a new regOpcIrrAppz.
     *
     * @param regOpcIrrAppz the regOpcIrrAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcIrrAppz, or with status {@code 400 (Bad Request)} if the regOpcIrrAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-irr-appzs")
    public ResponseEntity<RegOpcIrrAppz> createRegOpcIrrAppz(@Valid @RequestBody RegOpcIrrAppz regOpcIrrAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcIrrAppz : {}", regOpcIrrAppz);
        if (regOpcIrrAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcIrrAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcIrrAppz result = regOpcIrrAppzService.save(regOpcIrrAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-irr-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-irr-appzs} : Updates an existing regOpcIrrAppz.
     *
     * @param regOpcIrrAppz the regOpcIrrAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcIrrAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcIrrAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcIrrAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-irr-appzs")
    public ResponseEntity<RegOpcIrrAppz> updateRegOpcIrrAppz(@Valid @RequestBody RegOpcIrrAppz regOpcIrrAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcIrrAppz : {}", regOpcIrrAppz);
        if (regOpcIrrAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcIrrAppz result = regOpcIrrAppzService.save(regOpcIrrAppz);
        RegOpcIrrAppz result2 = regOpcIrrAppzService.save(regOpcIrrAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcIrrAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-irr-appzs} : get all the regOpcIrrAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcIrrAppzs in body.
     */
    @GetMapping("/reg-opc-irr-appzs")
    public ResponseEntity<List<RegOpcIrrAppz>> getAllRegOpcIrrAppzs(RegOpcIrrAppzCriteria criteria) {
        log.debug("REST request to get RegOpcIrrAppzs by criteria: {}", criteria);
        List<RegOpcIrrAppz> entityList = regOpcIrrAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-irr-appzs/count} : count all the regOpcIrrAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-irr-appzs/count")
    public ResponseEntity<Long> countRegOpcIrrAppzs(RegOpcIrrAppzCriteria criteria) {
        log.debug("REST request to count RegOpcIrrAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcIrrAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-irr-appzs/:id} : get the "id" regOpcIrrAppz.
     *
     * @param id the id of the regOpcIrrAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcIrrAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-irr-appzs/{id}")
    public ResponseEntity<RegOpcIrrAppz> getRegOpcIrrAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcIrrAppz : {}", id);
        Optional<RegOpcIrrAppz> regOpcIrrAppz = regOpcIrrAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcIrrAppz);
    }

    /**
     * {@code DELETE  /reg-opc-irr-appzs/:id} : delete the "id" regOpcIrrAppz.
     *
     * @param id the id of the regOpcIrrAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-irr-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcIrrAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcIrrAppz : {}", id);
        regOpcIrrAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
