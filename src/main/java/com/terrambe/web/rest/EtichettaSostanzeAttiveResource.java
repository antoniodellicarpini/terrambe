package com.terrambe.web.rest;

import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.service.EtichettaSostanzeAttiveService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.EtichettaSostanzeAttiveCriteria;
import com.terrambe.service.EtichettaSostanzeAttiveQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.EtichettaSostanzeAttive}.
 */
@RestController
@RequestMapping("/api")
public class EtichettaSostanzeAttiveResource {

    private final Logger log = LoggerFactory.getLogger(EtichettaSostanzeAttiveResource.class);

    private static final String ENTITY_NAME = "etichettaSostanzeAttive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtichettaSostanzeAttiveService etichettaSostanzeAttiveService;

    private final EtichettaSostanzeAttiveQueryService etichettaSostanzeAttiveQueryService;

    public EtichettaSostanzeAttiveResource(EtichettaSostanzeAttiveService etichettaSostanzeAttiveService, EtichettaSostanzeAttiveQueryService etichettaSostanzeAttiveQueryService) {
        this.etichettaSostanzeAttiveService = etichettaSostanzeAttiveService;
        this.etichettaSostanzeAttiveQueryService = etichettaSostanzeAttiveQueryService;
    }

    /**
     * {@code POST  /etichetta-sostanze-attives} : Create a new etichettaSostanzeAttive.
     *
     * @param etichettaSostanzeAttive the etichettaSostanzeAttive to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etichettaSostanzeAttive, or with status {@code 400 (Bad Request)} if the etichettaSostanzeAttive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etichetta-sostanze-attives")
    public ResponseEntity<EtichettaSostanzeAttive> createEtichettaSostanzeAttive(@RequestBody EtichettaSostanzeAttive etichettaSostanzeAttive) throws URISyntaxException {
        log.debug("REST request to save EtichettaSostanzeAttive : {}", etichettaSostanzeAttive);
        if (etichettaSostanzeAttive.getId() != null) {
            throw new BadRequestAlertException("A new etichettaSostanzeAttive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtichettaSostanzeAttive result = etichettaSostanzeAttiveService.save(etichettaSostanzeAttive);
        return ResponseEntity.created(new URI("/api/etichetta-sostanze-attives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etichetta-sostanze-attives} : Updates an existing etichettaSostanzeAttive.
     *
     * @param etichettaSostanzeAttive the etichettaSostanzeAttive to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etichettaSostanzeAttive,
     * or with status {@code 400 (Bad Request)} if the etichettaSostanzeAttive is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etichettaSostanzeAttive couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etichetta-sostanze-attives")
    public ResponseEntity<EtichettaSostanzeAttive> updateEtichettaSostanzeAttive(@RequestBody EtichettaSostanzeAttive etichettaSostanzeAttive) throws URISyntaxException {
        log.debug("REST request to update EtichettaSostanzeAttive : {}", etichettaSostanzeAttive);
        if (etichettaSostanzeAttive.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EtichettaSostanzeAttive result = etichettaSostanzeAttiveService.save(etichettaSostanzeAttive);
        EtichettaSostanzeAttive result2 = etichettaSostanzeAttiveService.save(etichettaSostanzeAttive);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, etichettaSostanzeAttive.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etichetta-sostanze-attives} : get all the etichettaSostanzeAttives.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etichettaSostanzeAttives in body.
     */
    @GetMapping("/etichetta-sostanze-attives")
    public ResponseEntity<List<EtichettaSostanzeAttive>> getAllEtichettaSostanzeAttives(EtichettaSostanzeAttiveCriteria criteria) {
        log.debug("REST request to get EtichettaSostanzeAttives by criteria: {}", criteria);
        List<EtichettaSostanzeAttive> entityList = etichettaSostanzeAttiveQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /etichetta-sostanze-attives/count} : count all the etichettaSostanzeAttives.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/etichetta-sostanze-attives/count")
    public ResponseEntity<Long> countEtichettaSostanzeAttives(EtichettaSostanzeAttiveCriteria criteria) {
        log.debug("REST request to count EtichettaSostanzeAttives by criteria: {}", criteria);
        return ResponseEntity.ok().body(etichettaSostanzeAttiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etichetta-sostanze-attives/:id} : get the "id" etichettaSostanzeAttive.
     *
     * @param id the id of the etichettaSostanzeAttive to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etichettaSostanzeAttive, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etichetta-sostanze-attives/{id}")
    public ResponseEntity<EtichettaSostanzeAttive> getEtichettaSostanzeAttive(@PathVariable Long id) {
        log.debug("REST request to get EtichettaSostanzeAttive : {}", id);
        Optional<EtichettaSostanzeAttive> etichettaSostanzeAttive = etichettaSostanzeAttiveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etichettaSostanzeAttive);
    }

    /**
     * {@code DELETE  /etichetta-sostanze-attives/:id} : delete the "id" etichettaSostanzeAttive.
     *
     * @param id the id of the etichettaSostanzeAttive to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etichetta-sostanze-attives/{id}")
    public ResponseEntity<Void> deleteEtichettaSostanzeAttive(@PathVariable Long id) {
        log.debug("REST request to delete EtichettaSostanzeAttive : {}", id);
        etichettaSostanzeAttiveService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
