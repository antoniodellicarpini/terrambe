package com.terrambe.web.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import javax.sql.DataSource;

import com.sun.mail.iap.Response;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;

@RestController
@RequestMapping("/api")
public class StampaQDCController {

    @Autowired
    private DataSource dataSource;

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /QDC/} : Creazione Stampa completa QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/QDC/")
    public void tuttoQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* ANAGRAFICA - PAGE 1*/
            JasperPrint jasperPrint1 = JasperFillManager.fillReport( ((File) ResourceUtils.getFile("classpath:report/anagraficaQDCv2.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint1);

            /* Piano Colturale - PAGE 2*/
            JasperPrint jasperPrint2 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/pianoColturaleQDCv2.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint2);

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint jasperPrint3 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regTrattaFitoQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint3);

            /* Registro trattamenti Fitofarmaci - PAGE 4 doppio*/
            //           JasperPrint  jasperPrint4 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regTrattaFitoQDC.jasper")).getAbsolutePath(), parameters,conn);
            //           jprintlist.add(jasperPrint4);

            /* Operazioni Colturali - PAGE 5*/
            JasperPrint  jasperPrint5 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/operazioniColturaliQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint5);

            /* Operazioni Semina - PAGE 6*/
            JasperPrint  jasperPrint6 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/operazioniSeminaQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint6);

            /* Irrigazione Pioggia - PAGE 7*/
            JasperPrint  jasperPrint7 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/irrigazionePioggia.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint7);

            /* Registro Raccolta - PAGE 8*/
            JasperPrint  jasperPrint8 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regRaccolta.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint8);

            /* Registro Concimazione - PAGE 9*/
            JasperPrint  jasperPrint9 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regConcimazione.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint9);

            /* Magazzino Agrofarmaci - PAGE 10*/
            JasperPrint  jasperPrint10 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/magaAgrofarmaci.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint10);

            /* Magazzino Fertilizzanti - PAGE 11*/
            JasperPrint  jasperPrint11 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/magaFertilizzanti.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint11);

            /* Dichiarazione - PAGE 12*/
            JasperPrint  jasperPrint12 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/moviMagazzini.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint12);

            /* Dichiarazione - PAGE 13*/
            JasperPrint  jasperPrint13 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/dichiarazioneQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint13);


            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);


            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");



            exporter.setConfiguration(exportConfig);

            exporter.exportReport();


        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/anagraficaQDC/} : Creazione Stampa Anagrafica.}
     * @throws Exception
     */
    @PostMapping("/stampa/anagraficaQDC/")
    public byte[] testStampa(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));
        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* ANAGRAFICA - PAGE 1*/
            JasperPrint jasperPrint1 = JasperFillManager.fillReport( ((File) ResourceUtils.getFile("classpath:report/anagraficaQDCv2.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint1);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint1);


        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/pianoColturaleQDC/} : Creazione Stampa piano colturale QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/pianoColturaleQDC/")
    public byte[] pianoColturaleQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));
        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Piano Colturale - PAGE 2*/
            JasperPrint jasperPrint2 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/pianoColturaleQDCv2.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint2);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint2);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }


    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/regTrattaFitoQDC/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/regTrattaFitoQDC/")
    public byte[] regTrattaFitoQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint jasperPrint3 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regTrattaFitoQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint3);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint3);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/operazioniColturaliQDC/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/operazioniColturaliQDC/")
    public byte[] operazioniColturaliQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint5 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/operazioniColturaliQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint5);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint5);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/operazioniSeminaQDC/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/operazioniSeminaQDC/")
    public byte[] operazioniSeminaQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint6 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/operazioniSeminaQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint6);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint6);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/irrigazionePioggia/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/irrigazionePioggia/")
    public byte[] irrigazionePioggia(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint7 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/irrigazionePioggia.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint7);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint7);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/regRaccolta/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/regRaccolta/")
    public byte[] regRaccolta(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint8 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regRaccolta.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint8);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint8);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/regConcimazione/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/regConcimazione/")
    public byte[] regConcimazione(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint9 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/regConcimazione.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint9);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint9);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/magaAgrofarmaci/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/magaAgrofarmaci/")
    public byte[] magaAgrofarmaci(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint10 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/magaAgrofarmaci.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint10);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint10);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/magaFertilizzanti/} : Creazione Stampa registo fitofarmaci QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/magaFertilizzanti/")
    public byte[] magaFertilizzanti(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint11 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/magaFertilizzanti.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint11);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint11);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }


    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/moviMagazzini/} : Creazione Stampa movimentazione Magazzini QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/moviMagazzini/")
    public byte[] moviMagazzini(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint12 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/moviMagazzini.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint12);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");


            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint12);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * @author aferrante
     * @category Stampe QDC
     * {@code  POST  /stampa/dichiarazioneQDC/} : Creazione Stampa movimentazione Magazzini QDC.}
     * @throws Exception
     */
    @PostMapping("/stampa/dichiarazioneQDC/")
    public byte[] dichiarazioneQDC(@RequestParam Map<String,String> allParams) throws Exception {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idAzienda", Integer.parseInt(allParams.get("idAzienda")));
        parameters.put("dataDa", allParams.get("dataInizio"));
        parameters.put("dataAl", allParams.get("dataFine"));

        byte[] bytes = null;
        try {
            List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            Connection conn = dataSource.getConnection();

            /* Registro trattamenti Fitofarmaci - PAGE 3*/
            JasperPrint  jasperPrint13 = JasperFillManager.fillReport(((File) ResourceUtils.getFile("classpath:report/dichiarazioneQDC.jasper")).getAbsolutePath(), parameters,conn);
            jprintlist.add(jasperPrint13);

            conn.close();

            /* inizio archivio file */
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jprintlist));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("quadernoqdc.pdf"));

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            bytes  = JasperExportManager.exportReportToPdf(jasperPrint13);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

}
