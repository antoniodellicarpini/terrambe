package com.terrambe.web.rest;

import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.service.Agea20152020MatriceService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.Agea20152020MatriceCriteria;
import com.terrambe.service.Agea20152020MatriceQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Agea20152020Matrice}.
 */
@RestController
@RequestMapping("/api")
public class Agea20152020MatriceResource {

    private final Logger log = LoggerFactory.getLogger(Agea20152020MatriceResource.class);

    private static final String ENTITY_NAME = "agea20152020Matrice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Agea20152020MatriceService agea20152020MatriceService;

    private final Agea20152020MatriceQueryService agea20152020MatriceQueryService;

    public Agea20152020MatriceResource(Agea20152020MatriceService agea20152020MatriceService, Agea20152020MatriceQueryService agea20152020MatriceQueryService) {
        this.agea20152020MatriceService = agea20152020MatriceService;
        this.agea20152020MatriceQueryService = agea20152020MatriceQueryService;
    }

    /**
     * {@code POST  /agea-20152020-matrices} : Create a new agea20152020Matrice.
     *
     * @param agea20152020Matrice the agea20152020Matrice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agea20152020Matrice, or with status {@code 400 (Bad Request)} if the agea20152020Matrice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agea-20152020-matrices")
    public ResponseEntity<Agea20152020Matrice> createAgea20152020Matrice(@RequestBody Agea20152020Matrice agea20152020Matrice) throws URISyntaxException {
        log.debug("REST request to save Agea20152020Matrice : {}", agea20152020Matrice);
        if (agea20152020Matrice.getId() != null) {
            throw new BadRequestAlertException("A new agea20152020Matrice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Agea20152020Matrice result = agea20152020MatriceService.save(agea20152020Matrice);
        return ResponseEntity.created(new URI("/api/agea-20152020-matrices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agea-20152020-matrices} : Updates an existing agea20152020Matrice.
     *
     * @param agea20152020Matrice the agea20152020Matrice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agea20152020Matrice,
     * or with status {@code 400 (Bad Request)} if the agea20152020Matrice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agea20152020Matrice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agea-20152020-matrices")
    public ResponseEntity<Agea20152020Matrice> updateAgea20152020Matrice(@RequestBody Agea20152020Matrice agea20152020Matrice) throws URISyntaxException {
        log.debug("REST request to update Agea20152020Matrice : {}", agea20152020Matrice);
        if (agea20152020Matrice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Agea20152020Matrice result = agea20152020MatriceService.save(agea20152020Matrice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agea20152020Matrice.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agea-20152020-matrices} : get all the agea20152020Matrices.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agea20152020Matrices in body.
     */
    @GetMapping("/agea-20152020-matrices")
    public ResponseEntity<List<Agea20152020Matrice>> getAllAgea20152020Matrices(Agea20152020MatriceCriteria criteria) {
        log.debug("REST request to get Agea20152020Matrices by criteria: {}", criteria);
        List<Agea20152020Matrice> entityList = agea20152020MatriceQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /agea-20152020-matrices/count} : count all the agea20152020Matrices.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/agea-20152020-matrices/count")
    public ResponseEntity<Long> countAgea20152020Matrices(Agea20152020MatriceCriteria criteria) {
        log.debug("REST request to count Agea20152020Matrices by criteria: {}", criteria);
        return ResponseEntity.ok().body(agea20152020MatriceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /agea-20152020-matrices/:id} : get the "id" agea20152020Matrice.
     *
     * @param id the id of the agea20152020Matrice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agea20152020Matrice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agea-20152020-matrices/{id}")
    public ResponseEntity<Agea20152020Matrice> getAgea20152020Matrice(@PathVariable Long id) {
        log.debug("REST request to get Agea20152020Matrice : {}", id);
        Optional<Agea20152020Matrice> agea20152020Matrice = agea20152020MatriceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agea20152020Matrice);
    }

    /**
     * {@code DELETE  /agea-20152020-matrices/:id} : delete the "id" agea20152020Matrice.
     *
     * @param id the id of the agea20152020Matrice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agea-20152020-matrices/{id}")
    public ResponseEntity<Void> deleteAgea20152020Matrice(@PathVariable Long id) {
        log.debug("REST request to delete Agea20152020Matrice : {}", id);
        agea20152020MatriceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
