package com.terrambe.web.rest;

import com.terrambe.domain.TipologieOpc;
import com.terrambe.service.TipologieOpcService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipologieOpcCriteria;
import com.terrambe.service.TipologieOpcQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipologieOpc}.
 */
@RestController
@RequestMapping("/api")
public class TipologieOpcResource {

    private final Logger log = LoggerFactory.getLogger(TipologieOpcResource.class);

    private static final String ENTITY_NAME = "tipologieOpc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipologieOpcService tipologieOpcService;

    private final TipologieOpcQueryService tipologieOpcQueryService;

    public TipologieOpcResource(TipologieOpcService tipologieOpcService, TipologieOpcQueryService tipologieOpcQueryService) {
        this.tipologieOpcService = tipologieOpcService;
        this.tipologieOpcQueryService = tipologieOpcQueryService;
    }

    /**
     * {@code POST  /tipologie-opcs} : Create a new tipologieOpc.
     *
     * @param tipologieOpc the tipologieOpc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipologieOpc, or with status {@code 400 (Bad Request)} if the tipologieOpc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipologie-opcs")
    public ResponseEntity<TipologieOpc> createTipologieOpc(@RequestBody TipologieOpc tipologieOpc) throws URISyntaxException {
        log.debug("REST request to save TipologieOpc : {}", tipologieOpc);
        if (tipologieOpc.getId() != null) {
            throw new BadRequestAlertException("A new tipologieOpc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipologieOpc result = tipologieOpcService.save(tipologieOpc);
        return ResponseEntity.created(new URI("/api/tipologie-opcs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipologie-opcs} : Updates an existing tipologieOpc.
     *
     * @param tipologieOpc the tipologieOpc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipologieOpc,
     * or with status {@code 400 (Bad Request)} if the tipologieOpc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipologieOpc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipologie-opcs")
    public ResponseEntity<TipologieOpc> updateTipologieOpc(@RequestBody TipologieOpc tipologieOpc) throws URISyntaxException {
        log.debug("REST request to update TipologieOpc : {}", tipologieOpc);
        if (tipologieOpc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipologieOpc result = tipologieOpcService.save(tipologieOpc);
        TipologieOpc result2 = tipologieOpcService.save(tipologieOpc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipologieOpc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipologie-opcs} : get all the tipologieOpcs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipologieOpcs in body.
     */
    @GetMapping("/tipologie-opcs")
    public ResponseEntity<List<TipologieOpc>> getAllTipologieOpcs(TipologieOpcCriteria criteria) {
        log.debug("REST request to get TipologieOpcs by criteria: {}", criteria);
        List<TipologieOpc> entityList = tipologieOpcQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipologie-opcs/count} : count all the tipologieOpcs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipologie-opcs/count")
    public ResponseEntity<Long> countTipologieOpcs(TipologieOpcCriteria criteria) {
        log.debug("REST request to count TipologieOpcs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipologieOpcQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipologie-opcs/:id} : get the "id" tipologieOpc.
     *
     * @param id the id of the tipologieOpc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipologieOpc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipologie-opcs/{id}")
    public ResponseEntity<TipologieOpc> getTipologieOpc(@PathVariable Long id) {
        log.debug("REST request to get TipologieOpc : {}", id);
        Optional<TipologieOpc> tipologieOpc = tipologieOpcService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipologieOpc);
    }

    /**
     * {@code DELETE  /tipologie-opcs/:id} : delete the "id" tipologieOpc.
     *
     * @param id the id of the tipologieOpc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipologie-opcs/{id}")
    public ResponseEntity<Void> deleteTipologieOpc(@PathVariable Long id) {
        log.debug("REST request to delete TipologieOpc : {}", id);
        tipologieOpcService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
