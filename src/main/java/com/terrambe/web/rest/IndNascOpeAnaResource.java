package com.terrambe.web.rest;

import com.terrambe.domain.IndNascOpeAna;
import com.terrambe.service.IndNascOpeAnaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndNascOpeAnaCriteria;
import com.terrambe.service.IndNascOpeAnaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.terrambe.domain.IndNascOpeAna}.
 */
@RestController
@RequestMapping("/api")
public class IndNascOpeAnaResource {

    private final Logger log = LoggerFactory.getLogger(IndNascOpeAnaResource.class);

    private static final String ENTITY_NAME = "indNascOpeAna";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndNascOpeAnaService indNascOpeAnaService;

    private final IndNascOpeAnaQueryService indNascOpeAnaQueryService;

    public IndNascOpeAnaResource(IndNascOpeAnaService indNascOpeAnaService, IndNascOpeAnaQueryService indNascOpeAnaQueryService) {
        this.indNascOpeAnaService = indNascOpeAnaService;
        this.indNascOpeAnaQueryService = indNascOpeAnaQueryService;
    }

    /**
     * {@code POST  /ind-nasc-ope-anas} : Create a new indNascOpeAna.
     *
     * @param indNascOpeAna the indNascOpeAna to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indNascOpeAna, or with status {@code 400 (Bad Request)} if the indNascOpeAna has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-nasc-ope-anas")
    public ResponseEntity<IndNascOpeAna> createIndNascOpeAna(@Valid @RequestBody IndNascOpeAna indNascOpeAna) throws URISyntaxException {
        log.debug("REST request to save IndNascOpeAna : {}", indNascOpeAna);
        if (indNascOpeAna.getId() != null) {
            throw new BadRequestAlertException("A new indNascOpeAna cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndNascOpeAna result = indNascOpeAnaService.save(indNascOpeAna);
        return ResponseEntity.created(new URI("/api/ind-nasc-ope-anas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-nasc-ope-anas} : Updates an existing indNascOpeAna.
     *
     * @param indNascOpeAna the indNascOpeAna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indNascOpeAna,
     * or with status {@code 400 (Bad Request)} if the indNascOpeAna is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indNascOpeAna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-nasc-ope-anas")
    public ResponseEntity<IndNascOpeAna> updateIndNascOpeAna(@Valid @RequestBody IndNascOpeAna indNascOpeAna) throws URISyntaxException {
        log.debug("REST request to update IndNascOpeAna : {}", indNascOpeAna);
        if (indNascOpeAna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndNascOpeAna result = indNascOpeAnaService.save(indNascOpeAna);
        IndNascOpeAna result2 = indNascOpeAnaService.save(indNascOpeAna);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indNascOpeAna.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-nasc-ope-anas} : get all the indNascOpeAnas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indNascOpeAnas in body.
     */
    @GetMapping("/ind-nasc-ope-anas")
    public ResponseEntity<List<IndNascOpeAna>> getAllIndNascOpeAnas(IndNascOpeAnaCriteria criteria) {
        log.debug("REST request to get IndNascOpeAnas by criteria: {}", criteria);
        List<IndNascOpeAna> entityList = indNascOpeAnaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-nasc-ope-anas/count} : count all the indNascOpeAnas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-nasc-ope-anas/count")
    public ResponseEntity<Long> countIndNascOpeAnas(IndNascOpeAnaCriteria criteria) {
        log.debug("REST request to count IndNascOpeAnas by criteria: {}", criteria);
        return ResponseEntity.ok().body(indNascOpeAnaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-nasc-ope-anas/:id} : get the "id" indNascOpeAna.
     *
     * @param id the id of the indNascOpeAna to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indNascOpeAna, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-nasc-ope-anas/{id}")
    public ResponseEntity<IndNascOpeAna> getIndNascOpeAna(@PathVariable Long id) {
        log.debug("REST request to get IndNascOpeAna : {}", id);
        Optional<IndNascOpeAna> indNascOpeAna = indNascOpeAnaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indNascOpeAna);
    }

    /**
     * {@code DELETE  /ind-nasc-ope-anas/:id} : delete the "id" indNascOpeAna.
     *
     * @param id the id of the indNascOpeAna to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-nasc-ope-anas/{id}")
    public ResponseEntity<Void> deleteIndNascOpeAna(@PathVariable Long id) {
        log.debug("REST request to delete IndNascOpeAna : {}", id);
        indNascOpeAnaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
