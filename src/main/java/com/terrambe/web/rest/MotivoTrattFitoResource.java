package com.terrambe.web.rest;

import com.terrambe.domain.MotivoTrattFito;
import com.terrambe.service.MotivoTrattFitoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.MotivoTrattFitoCriteria;
import com.terrambe.service.MotivoTrattFitoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.MotivoTrattFito}.
 */
@RestController
@RequestMapping("/api")
public class MotivoTrattFitoResource {

    private final Logger log = LoggerFactory.getLogger(MotivoTrattFitoResource.class);

    private static final String ENTITY_NAME = "motivoTrattFito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MotivoTrattFitoService motivoTrattFitoService;

    private final MotivoTrattFitoQueryService motivoTrattFitoQueryService;

    public MotivoTrattFitoResource(MotivoTrattFitoService motivoTrattFitoService, MotivoTrattFitoQueryService motivoTrattFitoQueryService) {
        this.motivoTrattFitoService = motivoTrattFitoService;
        this.motivoTrattFitoQueryService = motivoTrattFitoQueryService;
    }

    /**
     * {@code POST  /motivo-tratt-fitos} : Create a new motivoTrattFito.
     *
     * @param motivoTrattFito the motivoTrattFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new motivoTrattFito, or with status {@code 400 (Bad Request)} if the motivoTrattFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/motivo-tratt-fitos")
    public ResponseEntity<MotivoTrattFito> createMotivoTrattFito(@RequestBody MotivoTrattFito motivoTrattFito) throws URISyntaxException {
        log.debug("REST request to save MotivoTrattFito : {}", motivoTrattFito);
        if (motivoTrattFito.getId() != null) {
            throw new BadRequestAlertException("A new motivoTrattFito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MotivoTrattFito result = motivoTrattFitoService.save(motivoTrattFito);
        return ResponseEntity.created(new URI("/api/motivo-tratt-fitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /motivo-tratt-fitos} : Updates an existing motivoTrattFito.
     *
     * @param motivoTrattFito the motivoTrattFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated motivoTrattFito,
     * or with status {@code 400 (Bad Request)} if the motivoTrattFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the motivoTrattFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/motivo-tratt-fitos")
    public ResponseEntity<MotivoTrattFito> updateMotivoTrattFito(@RequestBody MotivoTrattFito motivoTrattFito) throws URISyntaxException {
        log.debug("REST request to update MotivoTrattFito : {}", motivoTrattFito);
        if (motivoTrattFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MotivoTrattFito result = motivoTrattFitoService.save(motivoTrattFito);
        MotivoTrattFito result2 = motivoTrattFitoService.save(motivoTrattFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, motivoTrattFito.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /motivo-tratt-fitos} : get all the motivoTrattFitos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of motivoTrattFitos in body.
     */
    @GetMapping("/motivo-tratt-fitos")
    public ResponseEntity<List<MotivoTrattFito>> getAllMotivoTrattFitos(MotivoTrattFitoCriteria criteria) {
        log.debug("REST request to get MotivoTrattFitos by criteria: {}", criteria);
        List<MotivoTrattFito> entityList = motivoTrattFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /motivo-tratt-fitos/count} : count all the motivoTrattFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/motivo-tratt-fitos/count")
    public ResponseEntity<Long> countMotivoTrattFitos(MotivoTrattFitoCriteria criteria) {
        log.debug("REST request to count MotivoTrattFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(motivoTrattFitoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /motivo-tratt-fitos/:id} : get the "id" motivoTrattFito.
     *
     * @param id the id of the motivoTrattFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the motivoTrattFito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/motivo-tratt-fitos/{id}")
    public ResponseEntity<MotivoTrattFito> getMotivoTrattFito(@PathVariable Long id) {
        log.debug("REST request to get MotivoTrattFito : {}", id);
        Optional<MotivoTrattFito> motivoTrattFito = motivoTrattFitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(motivoTrattFito);
    }

    /**
     * {@code DELETE  /motivo-tratt-fitos/:id} : delete the "id" motivoTrattFito.
     *
     * @param id the id of the motivoTrattFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/motivo-tratt-fitos/{id}")
    public ResponseEntity<Void> deleteMotivoTrattFito(@PathVariable Long id) {
        log.debug("REST request to delete MotivoTrattFito : {}", id);
        motivoTrattFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
