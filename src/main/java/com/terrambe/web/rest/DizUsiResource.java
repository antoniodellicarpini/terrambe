package com.terrambe.web.rest;

import com.terrambe.domain.DizUsi;
import com.terrambe.service.DizUsiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizUsiCriteria;
import com.terrambe.service.DizUsiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizUsi}.
 */
@RestController
@RequestMapping("/api")
public class DizUsiResource {

    private final Logger log = LoggerFactory.getLogger(DizUsiResource.class);

    private static final String ENTITY_NAME = "dizUsi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizUsiService dizUsiService;

    private final DizUsiQueryService dizUsiQueryService;

    public DizUsiResource(DizUsiService dizUsiService, DizUsiQueryService dizUsiQueryService) {
        this.dizUsiService = dizUsiService;
        this.dizUsiQueryService = dizUsiQueryService;
    }

    /**
     * {@code POST  /diz-usis} : Create a new dizUsi.
     *
     * @param dizUsi the dizUsi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizUsi, or with status {@code 400 (Bad Request)} if the dizUsi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-usis")
    public ResponseEntity<DizUsi> createDizUsi(@RequestBody DizUsi dizUsi) throws URISyntaxException {
        log.debug("REST request to save DizUsi : {}", dizUsi);
        if (dizUsi.getId() != null) {
            throw new BadRequestAlertException("A new dizUsi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizUsi result = dizUsiService.save(dizUsi);
        return ResponseEntity.created(new URI("/api/diz-usis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-usis} : Updates an existing dizUsi.
     *
     * @param dizUsi the dizUsi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizUsi,
     * or with status {@code 400 (Bad Request)} if the dizUsi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizUsi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-usis")
    public ResponseEntity<DizUsi> updateDizUsi(@RequestBody DizUsi dizUsi) throws URISyntaxException {
        log.debug("REST request to update DizUsi : {}", dizUsi);
        if (dizUsi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizUsi result = dizUsiService.save(dizUsi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizUsi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-usis} : get all the dizUsis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizUsis in body.
     */
    @GetMapping("/diz-usis")
    public ResponseEntity<List<DizUsi>> getAllDizUsis(DizUsiCriteria criteria) {
        log.debug("REST request to get DizUsis by criteria: {}", criteria);
        List<DizUsi> entityList = dizUsiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-usis/count} : count all the dizUsis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-usis/count")
    public ResponseEntity<Long> countDizUsis(DizUsiCriteria criteria) {
        log.debug("REST request to count DizUsis by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizUsiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-usis/:id} : get the "id" dizUsi.
     *
     * @param id the id of the dizUsi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizUsi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-usis/{id}")
    public ResponseEntity<DizUsi> getDizUsi(@PathVariable Long id) {
        log.debug("REST request to get DizUsi : {}", id);
        Optional<DizUsi> dizUsi = dizUsiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizUsi);
    }

    /**
     * {@code DELETE  /diz-usis/:id} : delete the "id" dizUsi.
     *
     * @param id the id of the dizUsi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-usis/{id}")
    public ResponseEntity<Void> deleteDizUsi(@PathVariable Long id) {
        log.debug("REST request to delete DizUsi : {}", id);
        dizUsiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
