package com.terrambe.web.rest;

import com.terrambe.domain.Avversita;
import com.terrambe.service.AvversitaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AvversitaCriteria;
import com.terrambe.service.AvversitaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Avversita}.
 */
@RestController
@RequestMapping("/api")
public class AvversitaResource {

    private final Logger log = LoggerFactory.getLogger(AvversitaResource.class);

    private static final String ENTITY_NAME = "avversita";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AvversitaService avversitaService;

    private final AvversitaQueryService avversitaQueryService;

    public AvversitaResource(AvversitaService avversitaService, AvversitaQueryService avversitaQueryService) {
        this.avversitaService = avversitaService;
        this.avversitaQueryService = avversitaQueryService;
    }

    /**
     * {@code POST  /avversitas} : Create a new avversita.
     *
     * @param avversita the avversita to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new avversita, or with status {@code 400 (Bad Request)} if the avversita has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/avversitas")
    public ResponseEntity<Avversita> createAvversita(@RequestBody Avversita avversita) throws URISyntaxException {
        log.debug("REST request to save Avversita : {}", avversita);
        if (avversita.getId() != null) {
            throw new BadRequestAlertException("A new avversita cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Avversita result = avversitaService.save(avversita);
        return ResponseEntity.created(new URI("/api/avversitas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /avversitas} : Updates an existing avversita.
     *
     * @param avversita the avversita to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avversita,
     * or with status {@code 400 (Bad Request)} if the avversita is not valid,
     * or with status {@code 500 (Internal Server Error)} if the avversita couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/avversitas")
    public ResponseEntity<Avversita> updateAvversita(@RequestBody Avversita avversita) throws URISyntaxException {
        log.debug("REST request to update Avversita : {}", avversita);
        if (avversita.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Avversita result = avversitaService.save(avversita);
        Avversita result2 = avversitaService.save(avversita);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, avversita.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /avversitas} : get all the avversitas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of avversitas in body.
     */
    @GetMapping("/avversitas")
    public ResponseEntity<List<Avversita>> getAllAvversitas(AvversitaCriteria criteria) {
        log.debug("REST request to get Avversitas by criteria: {}", criteria);
        List<Avversita> entityList = avversitaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /avversitas/count} : count all the avversitas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/avversitas/count")
    public ResponseEntity<Long> countAvversitas(AvversitaCriteria criteria) {
        log.debug("REST request to count Avversitas by criteria: {}", criteria);
        return ResponseEntity.ok().body(avversitaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /avversitas/:id} : get the "id" avversita.
     *
     * @param id the id of the avversita to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the avversita, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/avversitas/{id}")
    public ResponseEntity<Avversita> getAvversita(@PathVariable Long id) {
        log.debug("REST request to get Avversita : {}", id);
        Optional<Avversita> avversita = avversitaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(avversita);
    }

    /**
     * {@code DELETE  /avversitas/:id} : delete the "id" avversita.
     *
     * @param id the id of the avversita to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/avversitas/{id}")
    public ResponseEntity<Void> deleteAvversita(@PathVariable Long id) {
        log.debug("REST request to delete Avversita : {}", id);
        avversitaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
