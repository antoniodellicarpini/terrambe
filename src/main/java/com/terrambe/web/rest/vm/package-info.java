/**
 * View Models used by Spring MVC REST controllers.
 */
package com.terrambe.web.rest.vm;
