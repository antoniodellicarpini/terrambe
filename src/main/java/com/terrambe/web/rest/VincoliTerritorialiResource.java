package com.terrambe.web.rest;

import com.terrambe.domain.VincoliTerritoriali;
import com.terrambe.service.VincoliTerritorialiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.VincoliTerritorialiCriteria;
import com.terrambe.service.VincoliTerritorialiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.VincoliTerritoriali}.
 */
@RestController
@RequestMapping("/api")
public class VincoliTerritorialiResource {

    private final Logger log = LoggerFactory.getLogger(VincoliTerritorialiResource.class);

    private static final String ENTITY_NAME = "vincoliTerritoriali";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VincoliTerritorialiService vincoliTerritorialiService;

    private final VincoliTerritorialiQueryService vincoliTerritorialiQueryService;

    public VincoliTerritorialiResource(VincoliTerritorialiService vincoliTerritorialiService, VincoliTerritorialiQueryService vincoliTerritorialiQueryService) {
        this.vincoliTerritorialiService = vincoliTerritorialiService;
        this.vincoliTerritorialiQueryService = vincoliTerritorialiQueryService;
    }

    /**
     * {@code POST  /vincoli-territorialis} : Create a new vincoliTerritoriali.
     *
     * @param vincoliTerritoriali the vincoliTerritoriali to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vincoliTerritoriali, or with status {@code 400 (Bad Request)} if the vincoliTerritoriali has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vincoli-territorialis")
    public ResponseEntity<VincoliTerritoriali> createVincoliTerritoriali(@Valid @RequestBody VincoliTerritoriali vincoliTerritoriali) throws URISyntaxException {
        log.debug("REST request to save VincoliTerritoriali : {}", vincoliTerritoriali);
        if (vincoliTerritoriali.getId() != null) {
            throw new BadRequestAlertException("A new vincoliTerritoriali cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VincoliTerritoriali result = vincoliTerritorialiService.save(vincoliTerritoriali);
        return ResponseEntity.created(new URI("/api/vincoli-territorialis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vincoli-territorialis} : Updates an existing vincoliTerritoriali.
     *
     * @param vincoliTerritoriali the vincoliTerritoriali to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vincoliTerritoriali,
     * or with status {@code 400 (Bad Request)} if the vincoliTerritoriali is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vincoliTerritoriali couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vincoli-territorialis")
    public ResponseEntity<VincoliTerritoriali> updateVincoliTerritoriali(@Valid @RequestBody VincoliTerritoriali vincoliTerritoriali) throws URISyntaxException {
        log.debug("REST request to update VincoliTerritoriali : {}", vincoliTerritoriali);
        if (vincoliTerritoriali.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VincoliTerritoriali result = vincoliTerritorialiService.save(vincoliTerritoriali);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vincoliTerritoriali.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vincoli-territorialis} : get all the vincoliTerritorialis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vincoliTerritorialis in body.
     */
    @GetMapping("/vincoli-territorialis")
    public ResponseEntity<List<VincoliTerritoriali>> getAllVincoliTerritorialis(VincoliTerritorialiCriteria criteria) {
        log.debug("REST request to get VincoliTerritorialis by criteria: {}", criteria);
        List<VincoliTerritoriali> entityList = vincoliTerritorialiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /vincoli-territorialis/count} : count all the vincoliTerritorialis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/vincoli-territorialis/count")
    public ResponseEntity<Long> countVincoliTerritorialis(VincoliTerritorialiCriteria criteria) {
        log.debug("REST request to count VincoliTerritorialis by criteria: {}", criteria);
        return ResponseEntity.ok().body(vincoliTerritorialiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /vincoli-territorialis/:id} : get the "id" vincoliTerritoriali.
     *
     * @param id the id of the vincoliTerritoriali to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vincoliTerritoriali, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vincoli-territorialis/{id}")
    public ResponseEntity<VincoliTerritoriali> getVincoliTerritoriali(@PathVariable Long id) {
        log.debug("REST request to get VincoliTerritoriali : {}", id);
        Optional<VincoliTerritoriali> vincoliTerritoriali = vincoliTerritorialiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vincoliTerritoriali);
    }

    /**
     * {@code DELETE  /vincoli-territorialis/:id} : delete the "id" vincoliTerritoriali.
     *
     * @param id the id of the vincoliTerritoriali to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vincoli-territorialis/{id}")
    public ResponseEntity<Void> deleteVincoliTerritoriali(@PathVariable Long id) {
        log.debug("REST request to delete VincoliTerritoriali : {}", id);
        vincoliTerritorialiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
