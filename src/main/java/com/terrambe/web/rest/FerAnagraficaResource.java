package com.terrambe.web.rest;

import com.terrambe.domain.FerAnagrafica;
import com.terrambe.service.FerAnagraficaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FerAnagraficaCriteria;
import com.terrambe.service.FerAnagraficaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.FerAnagrafica}.
 */
@RestController
@RequestMapping("/api")
public class FerAnagraficaResource {

    private final Logger log = LoggerFactory.getLogger(FerAnagraficaResource.class);

    private static final String ENTITY_NAME = "ferAnagrafica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FerAnagraficaService ferAnagraficaService;

    private final FerAnagraficaQueryService ferAnagraficaQueryService;

    public FerAnagraficaResource(FerAnagraficaService ferAnagraficaService, FerAnagraficaQueryService ferAnagraficaQueryService) {
        this.ferAnagraficaService = ferAnagraficaService;
        this.ferAnagraficaQueryService = ferAnagraficaQueryService;
    }

    /**
     * {@code POST  /fer-anagraficas} : Create a new ferAnagrafica.
     *
     * @param ferAnagrafica the ferAnagrafica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ferAnagrafica, or with status {@code 400 (Bad Request)} if the ferAnagrafica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fer-anagraficas")
    public ResponseEntity<FerAnagrafica> createFerAnagrafica(@Valid @RequestBody FerAnagrafica ferAnagrafica) throws URISyntaxException {
        log.debug("REST request to save FerAnagrafica : {}", ferAnagrafica);
        if (ferAnagrafica.getId() != null) {
            throw new BadRequestAlertException("A new ferAnagrafica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FerAnagrafica result = ferAnagraficaService.save(ferAnagrafica);
        return ResponseEntity.created(new URI("/api/fer-anagraficas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fer-anagraficas} : Updates an existing ferAnagrafica.
     *
     * @param ferAnagrafica the ferAnagrafica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ferAnagrafica,
     * or with status {@code 400 (Bad Request)} if the ferAnagrafica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ferAnagrafica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fer-anagraficas")
    public ResponseEntity<FerAnagrafica> updateFerAnagrafica(@Valid @RequestBody FerAnagrafica ferAnagrafica) throws URISyntaxException {
        log.debug("REST request to update FerAnagrafica : {}", ferAnagrafica);
        if (ferAnagrafica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FerAnagrafica result = ferAnagraficaService.save(ferAnagrafica);
        FerAnagrafica result2 = ferAnagraficaService.save(ferAnagrafica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ferAnagrafica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fer-anagraficas} : get all the ferAnagraficas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ferAnagraficas in body.
     */
    @GetMapping("/fer-anagraficas")
    public ResponseEntity<List<FerAnagrafica>> getAllFerAnagraficas(FerAnagraficaCriteria criteria) {
        log.debug("REST request to get FerAnagraficas by criteria: {}", criteria);
        List<FerAnagrafica> entityList = ferAnagraficaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /fer-anagraficas/count} : count all the ferAnagraficas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fer-anagraficas/count")
    public ResponseEntity<Long> countFerAnagraficas(FerAnagraficaCriteria criteria) {
        log.debug("REST request to count FerAnagraficas by criteria: {}", criteria);
        return ResponseEntity.ok().body(ferAnagraficaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fer-anagraficas/:id} : get the "id" ferAnagrafica.
     *
     * @param id the id of the ferAnagrafica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ferAnagrafica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fer-anagraficas/{id}")
    public ResponseEntity<FerAnagrafica> getFerAnagrafica(@PathVariable Long id) {
        log.debug("REST request to get FerAnagrafica : {}", id);
        Optional<FerAnagrafica> ferAnagrafica = ferAnagraficaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ferAnagrafica);
    }

    /**
     * {@code DELETE  /fer-anagraficas/:id} : delete the "id" ferAnagrafica.
     *
     * @param id the id of the ferAnagrafica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fer-anagraficas/{id}")
    public ResponseEntity<Void> deleteFerAnagrafica(@PathVariable Long id) {
        log.debug("REST request to delete FerAnagrafica : {}", id);
        ferAnagraficaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
