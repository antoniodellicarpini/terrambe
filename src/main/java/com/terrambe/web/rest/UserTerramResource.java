package com.terrambe.web.rest;

import com.terrambe.domain.UserTerram;
import com.terrambe.service.UserTerramService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.UserTerramCriteria;
import com.terrambe.service.UserTerramQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.UserTerram}.
 */
@RestController
@RequestMapping("/api")
public class UserTerramResource {

    private final Logger log = LoggerFactory.getLogger(UserTerramResource.class);

    private static final String ENTITY_NAME = "userTerram";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserTerramService userTerramService;

    private final UserTerramQueryService userTerramQueryService;

    public UserTerramResource(UserTerramService userTerramService, UserTerramQueryService userTerramQueryService) {
        this.userTerramService = userTerramService;
        this.userTerramQueryService = userTerramQueryService;
    }

    /**
     * {@code POST  /user-terrams} : Create a new userTerram.
     *
     * @param userTerram the userTerram to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userTerram, or with status {@code 400 (Bad Request)} if the userTerram has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-terrams")
    public ResponseEntity<UserTerram> createUserTerram(@RequestBody UserTerram userTerram) throws URISyntaxException {
        log.debug("REST request to save UserTerram : {}", userTerram);
        if (userTerram.getId() != null) {
            throw new BadRequestAlertException("A new userTerram cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserTerram result = userTerramService.save(userTerram);
        return ResponseEntity.created(new URI("/api/user-terrams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-terrams} : Updates an existing userTerram.
     *
     * @param userTerram the userTerram to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userTerram,
     * or with status {@code 400 (Bad Request)} if the userTerram is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userTerram couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-terrams")
    public ResponseEntity<UserTerram> updateUserTerram(@RequestBody UserTerram userTerram) throws URISyntaxException {
        log.debug("REST request to update UserTerram : {}", userTerram);
        if (userTerram.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserTerram result = userTerramService.save(userTerram);
        UserTerram result2 = userTerramService.save(userTerram);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userTerram.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-terrams} : get all the userTerrams.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userTerrams in body.
     */
    @GetMapping("/user-terrams")
    public ResponseEntity<List<UserTerram>> getAllUserTerrams(UserTerramCriteria criteria) {
        log.debug("REST request to get UserTerrams by criteria: {}", criteria);
        List<UserTerram> entityList = userTerramQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /user-terrams/count} : count all the userTerrams.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-terrams/count")
    public ResponseEntity<Long> countUserTerrams(UserTerramCriteria criteria) {
        log.debug("REST request to count UserTerrams by criteria: {}", criteria);
        return ResponseEntity.ok().body(userTerramQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-terrams/:id} : get the "id" userTerram.
     *
     * @param id the id of the userTerram to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userTerram, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-terrams/{id}")
    public ResponseEntity<UserTerram> getUserTerram(@PathVariable Long id) {
        log.debug("REST request to get UserTerram : {}", id);
        Optional<UserTerram> userTerram = userTerramService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userTerram);
    }

    /**
     * {@code DELETE  /user-terrams/:id} : delete the "id" userTerram.
     *
     * @param id the id of the userTerram to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-terrams/{id}")
    public ResponseEntity<Void> deleteUserTerram(@PathVariable Long id) {
        log.debug("REST request to delete UserTerram : {}", id);
        userTerramService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
