package com.terrambe.web.rest;

import com.terrambe.domain.TipoContratto;
import com.terrambe.service.TipoContrattoService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoContrattoCriteria;
import com.terrambe.service.TipoContrattoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoContratto}.
 */
@RestController
@RequestMapping("/api")
public class TipoContrattoResource {

    private final Logger log = LoggerFactory.getLogger(TipoContrattoResource.class);

    private static final String ENTITY_NAME = "tipoContratto";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoContrattoService tipoContrattoService;

    private final TipoContrattoQueryService tipoContrattoQueryService;

    public TipoContrattoResource(TipoContrattoService tipoContrattoService, TipoContrattoQueryService tipoContrattoQueryService) {
        this.tipoContrattoService = tipoContrattoService;
        this.tipoContrattoQueryService = tipoContrattoQueryService;
    }

    /**
     * {@code POST  /tipo-contrattos} : Create a new tipoContratto.
     *
     * @param tipoContratto the tipoContratto to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoContratto, or with status {@code 400 (Bad Request)} if the tipoContratto has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-contrattos")
    public ResponseEntity<TipoContratto> createTipoContratto(@RequestBody TipoContratto tipoContratto) throws URISyntaxException {
        log.debug("REST request to save TipoContratto : {}", tipoContratto);
        if (tipoContratto.getId() != null) {
            throw new BadRequestAlertException("A new tipoContratto cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoContratto result = tipoContrattoService.save(tipoContratto);
        return ResponseEntity.created(new URI("/api/tipo-contrattos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-contrattos} : Updates an existing tipoContratto.
     *
     * @param tipoContratto the tipoContratto to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoContratto,
     * or with status {@code 400 (Bad Request)} if the tipoContratto is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoContratto couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-contrattos")
    public ResponseEntity<TipoContratto> updateTipoContratto(@RequestBody TipoContratto tipoContratto) throws URISyntaxException {
        log.debug("REST request to update TipoContratto : {}", tipoContratto);
        if (tipoContratto.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoContratto result = tipoContrattoService.save(tipoContratto);
        TipoContratto result2 = tipoContrattoService.save(tipoContratto);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoContratto.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-contrattos} : get all the tipoContrattos.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoContrattos in body.
     */
    @GetMapping("/tipo-contrattos")
    public ResponseEntity<List<TipoContratto>> getAllTipoContrattos(TipoContrattoCriteria criteria) {
        log.debug("REST request to get TipoContrattos by criteria: {}", criteria);
        List<TipoContratto> entityList = tipoContrattoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-contrattos/count} : count all the tipoContrattos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-contrattos/count")
    public ResponseEntity<Long> countTipoContrattos(TipoContrattoCriteria criteria) {
        log.debug("REST request to count TipoContrattos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoContrattoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-contrattos/:id} : get the "id" tipoContratto.
     *
     * @param id the id of the tipoContratto to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoContratto, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-contrattos/{id}")
    public ResponseEntity<TipoContratto> getTipoContratto(@PathVariable Long id) {
        log.debug("REST request to get TipoContratto : {}", id);
        Optional<TipoContratto> tipoContratto = tipoContrattoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoContratto);
    }

    /**
     * {@code DELETE  /tipo-contrattos/:id} : delete the "id" tipoContratto.
     *
     * @param id the id of the tipoContratto to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-contrattos/{id}")
    public ResponseEntity<Void> deleteTipoContratto(@PathVariable Long id) {
        log.debug("REST request to delete TipoContratto : {}", id);
        tipoContrattoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
