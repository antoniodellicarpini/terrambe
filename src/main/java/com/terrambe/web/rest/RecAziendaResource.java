package com.terrambe.web.rest;

import com.terrambe.domain.RecAzienda;
import com.terrambe.service.RecAziendaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecAziendaCriteria;
import com.terrambe.service.RecAziendaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecAzienda}.
 */
@RestController
@RequestMapping("/api")
public class RecAziendaResource {

    private final Logger log = LoggerFactory.getLogger(RecAziendaResource.class);

    private static final String ENTITY_NAME = "recAzienda";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecAziendaService recAziendaService;

    private final RecAziendaQueryService recAziendaQueryService;

    public RecAziendaResource(RecAziendaService recAziendaService, RecAziendaQueryService recAziendaQueryService) {
        this.recAziendaService = recAziendaService;
        this.recAziendaQueryService = recAziendaQueryService;
    }

    /**
     * {@code POST  /rec-aziendas} : Create a new recAzienda.
     *
     * @param recAzienda the recAzienda to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recAzienda, or with status {@code 400 (Bad Request)} if the recAzienda has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-aziendas")
    public ResponseEntity<RecAzienda> createRecAzienda(@RequestBody RecAzienda recAzienda) throws URISyntaxException {
        log.debug("REST request to save RecAzienda : {}", recAzienda);
        if (recAzienda.getId() != null) {
            throw new BadRequestAlertException("A new recAzienda cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecAzienda result = recAziendaService.save(recAzienda);
        return ResponseEntity.created(new URI("/api/rec-aziendas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-aziendas} : Updates an existing recAzienda.
     *
     * @param recAzienda the recAzienda to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recAzienda,
     * or with status {@code 400 (Bad Request)} if the recAzienda is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recAzienda couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-aziendas")
    public ResponseEntity<RecAzienda> updateRecAzienda(@RequestBody RecAzienda recAzienda) throws URISyntaxException {
        log.debug("REST request to update RecAzienda : {}", recAzienda);
        if (recAzienda.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecAzienda result = recAziendaService.save(recAzienda);
        RecAzienda result2 = recAziendaService.save(recAzienda);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recAzienda.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-aziendas} : get all the recAziendas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recAziendas in body.
     */
    @GetMapping("/rec-aziendas")
    public ResponseEntity<List<RecAzienda>> getAllRecAziendas(RecAziendaCriteria criteria) {
        log.debug("REST request to get RecAziendas by criteria: {}", criteria);
        List<RecAzienda> entityList = recAziendaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-aziendas/count} : count all the recAziendas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-aziendas/count")
    public ResponseEntity<Long> countRecAziendas(RecAziendaCriteria criteria) {
        log.debug("REST request to count RecAziendas by criteria: {}", criteria);
        return ResponseEntity.ok().body(recAziendaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-aziendas/:id} : get the "id" recAzienda.
     *
     * @param id the id of the recAzienda to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recAzienda, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-aziendas/{id}")
    public ResponseEntity<RecAzienda> getRecAzienda(@PathVariable Long id) {
        log.debug("REST request to get RecAzienda : {}", id);
        Optional<RecAzienda> recAzienda = recAziendaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recAzienda);
    }

    /**
     * {@code DELETE  /rec-aziendas/:id} : delete the "id" recAzienda.
     *
     * @param id the id of the recAzienda to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-aziendas/{id}")
    public ResponseEntity<Void> deleteRecAzienda(@PathVariable Long id) {
        log.debug("REST request to delete RecAzienda : {}", id);
        recAziendaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
