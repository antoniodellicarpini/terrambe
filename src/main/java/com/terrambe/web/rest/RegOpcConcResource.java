package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcConc;
import com.terrambe.service.*;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcConcCriteria;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcConc}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcConcResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcResource.class);

    private static final String ENTITY_NAME = "regOpcConc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcConcService regOpcConcService;

    private final RegOpcConcQueryService regOpcConcQueryService;

    @Autowired
    private Agea20152020MatriceService ageaMatServ;

    @Autowired
    private AtrAnagraficaService atrServ;

    @Autowired
    private OpeAnagraficaService opeAnServ;

    public RegOpcConcResource(RegOpcConcService regOpcConcService, RegOpcConcQueryService regOpcConcQueryService) {
        this.regOpcConcService = regOpcConcService;
        this.regOpcConcQueryService = regOpcConcQueryService;
    }

    /**
     * {@code POST  /reg-opc-concs} : Create a new regOpcConc.
     *
     * @param regOpcConc the regOpcConc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcConc, or with status {@code 400 (Bad Request)} if the regOpcConc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-concs")
    public ResponseEntity<RegOpcConc> createRegOpcConc(@Valid @RequestBody RegOpcConc regOpcConc) throws URISyntaxException {
        log.debug("REST request to save RegOpcConc : {}", regOpcConc);
        if (regOpcConc.getId() != null) {
            throw new BadRequestAlertException("A new regOpcConc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcConc result = regOpcConcService.save(regOpcConc);
        return ResponseEntity.created(new URI("/api/reg-opc-concs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-concs} : Updates an existing regOpcConc.
     *
     * @param regOpcConc the regOpcConc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcConc,
     * or with status {@code 400 (Bad Request)} if the regOpcConc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcConc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-concs")
    public ResponseEntity<RegOpcConc> updateRegOpcConc(@Valid @RequestBody RegOpcConc regOpcConc) throws URISyntaxException {
        log.debug("REST request to update RegOpcConc : {}", regOpcConc);
        if (regOpcConc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcConc result = regOpcConcService.save(regOpcConc);
        RegOpcConc result2 = regOpcConcService.save(regOpcConc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcConc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-concs} : get all the regOpcConcs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcConcs in body.
     */
    @GetMapping("/reg-opc-concs")
    public ResponseEntity<List<RegOpcConc>> getAllRegOpcConcs(RegOpcConcCriteria criteria) {
        log.debug("REST request to get RegOpcConcs by criteria: {}", criteria);
        List<RegOpcConc> entityList = regOpcConcQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-concs/count} : count all the regOpcConcs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-concs/count")
    public ResponseEntity<Long> countRegOpcConcs(RegOpcConcCriteria criteria) {
        log.debug("REST request to count RegOpcConcs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcConcQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-concs/:id} : get the "id" regOpcConc.
     *
     * @param id the id of the regOpcConc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcConc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-concs/{id}")
    public ResponseEntity<RegOpcConc> getRegOpcConc(@PathVariable Long id) {
        log.debug("REST request to get RegOpcConc : {}", id);
        Optional<RegOpcConc> regOpcConc = regOpcConcService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcConc);
    }

    /**
     * {@code DELETE  /reg-opc-concs/:id} : delete the "id" regOpcConc.
     *
     * @param id the id of the regOpcConc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-concs/{id}")
    public ResponseEntity<Void> deleteRegOpcConc(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcConc : {}", id);
        regOpcConcService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
