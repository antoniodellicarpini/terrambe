package com.terrambe.web.rest;

import com.terrambe.domain.TipoPrepterr;
import com.terrambe.service.TipoPrepterrService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoPrepterrCriteria;
import com.terrambe.service.TipoPrepterrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoPrepterr}.
 */
@RestController
@RequestMapping("/api")
public class TipoPrepterrResource {

    private final Logger log = LoggerFactory.getLogger(TipoPrepterrResource.class);

    private static final String ENTITY_NAME = "tipoPrepterr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoPrepterrService tipoPrepterrService;

    private final TipoPrepterrQueryService tipoPrepterrQueryService;

    public TipoPrepterrResource(TipoPrepterrService tipoPrepterrService, TipoPrepterrQueryService tipoPrepterrQueryService) {
        this.tipoPrepterrService = tipoPrepterrService;
        this.tipoPrepterrQueryService = tipoPrepterrQueryService;
    }

    /**
     * {@code POST  /tipo-prepterrs} : Create a new tipoPrepterr.
     *
     * @param tipoPrepterr the tipoPrepterr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoPrepterr, or with status {@code 400 (Bad Request)} if the tipoPrepterr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-prepterrs")
    public ResponseEntity<TipoPrepterr> createTipoPrepterr(@RequestBody TipoPrepterr tipoPrepterr) throws URISyntaxException {
        log.debug("REST request to save TipoPrepterr : {}", tipoPrepterr);
        if (tipoPrepterr.getId() != null) {
            throw new BadRequestAlertException("A new tipoPrepterr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoPrepterr result = tipoPrepterrService.save(tipoPrepterr);
        return ResponseEntity.created(new URI("/api/tipo-prepterrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-prepterrs} : Updates an existing tipoPrepterr.
     *
     * @param tipoPrepterr the tipoPrepterr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoPrepterr,
     * or with status {@code 400 (Bad Request)} if the tipoPrepterr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoPrepterr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-prepterrs")
    public ResponseEntity<TipoPrepterr> updateTipoPrepterr(@RequestBody TipoPrepterr tipoPrepterr) throws URISyntaxException {
        log.debug("REST request to update TipoPrepterr : {}", tipoPrepterr);
        if (tipoPrepterr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoPrepterr result = tipoPrepterrService.save(tipoPrepterr);
        TipoPrepterr result2 = tipoPrepterrService.save(tipoPrepterr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoPrepterr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-prepterrs} : get all the tipoPrepterrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoPrepterrs in body.
     */
    @GetMapping("/tipo-prepterrs")
    public ResponseEntity<List<TipoPrepterr>> getAllTipoPrepterrs(TipoPrepterrCriteria criteria) {
        log.debug("REST request to get TipoPrepterrs by criteria: {}", criteria);
        List<TipoPrepterr> entityList = tipoPrepterrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-prepterrs/count} : count all the tipoPrepterrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-prepterrs/count")
    public ResponseEntity<Long> countTipoPrepterrs(TipoPrepterrCriteria criteria) {
        log.debug("REST request to count TipoPrepterrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoPrepterrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-prepterrs/:id} : get the "id" tipoPrepterr.
     *
     * @param id the id of the tipoPrepterr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoPrepterr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-prepterrs/{id}")
    public ResponseEntity<TipoPrepterr> getTipoPrepterr(@PathVariable Long id) {
        log.debug("REST request to get TipoPrepterr : {}", id);
        Optional<TipoPrepterr> tipoPrepterr = tipoPrepterrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoPrepterr);
    }

    /**
     * {@code DELETE  /tipo-prepterrs/:id} : delete the "id" tipoPrepterr.
     *
     * @param id the id of the tipoPrepterr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-prepterrs/{id}")
    public ResponseEntity<Void> deleteTipoPrepterr(@PathVariable Long id) {
        log.debug("REST request to delete TipoPrepterr : {}", id);
        tipoPrepterrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
