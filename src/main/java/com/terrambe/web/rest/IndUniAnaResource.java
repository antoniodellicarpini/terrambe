package com.terrambe.web.rest;

import com.terrambe.domain.IndUniAna;
import com.terrambe.service.IndUniAnaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndUniAnaCriteria;
import com.terrambe.service.IndUniAnaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.IndUniAna}.
 */
@RestController
@RequestMapping("/api")
public class IndUniAnaResource {

    private final Logger log = LoggerFactory.getLogger(IndUniAnaResource.class);

    private static final String ENTITY_NAME = "indUniAna";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndUniAnaService indUniAnaService;

    private final IndUniAnaQueryService indUniAnaQueryService;

    public IndUniAnaResource(IndUniAnaService indUniAnaService, IndUniAnaQueryService indUniAnaQueryService) {
        this.indUniAnaService = indUniAnaService;
        this.indUniAnaQueryService = indUniAnaQueryService;
    }

    /**
     * {@code POST  /ind-uni-anas} : Create a new indUniAna.
     *
     * @param indUniAna the indUniAna to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indUniAna, or with status {@code 400 (Bad Request)} if the indUniAna has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-uni-anas")
    public ResponseEntity<IndUniAna> createIndUniAna(@Valid @RequestBody IndUniAna indUniAna) throws URISyntaxException {
        log.debug("REST request to save IndUniAna : {}", indUniAna);
        if (indUniAna.getId() != null) {
            throw new BadRequestAlertException("A new indUniAna cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndUniAna result = indUniAnaService.save(indUniAna);
        return ResponseEntity.created(new URI("/api/ind-uni-anas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-uni-anas} : Updates an existing indUniAna.
     *
     * @param indUniAna the indUniAna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indUniAna,
     * or with status {@code 400 (Bad Request)} if the indUniAna is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indUniAna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-uni-anas")
    public ResponseEntity<IndUniAna> updateIndUniAna(@Valid @RequestBody IndUniAna indUniAna) throws URISyntaxException {
        log.debug("REST request to update IndUniAna : {}", indUniAna);
        if (indUniAna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndUniAna result = indUniAnaService.save(indUniAna);
        IndUniAna result2 = indUniAnaService.save(indUniAna);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indUniAna.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-uni-anas} : get all the indUniAnas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indUniAnas in body.
     */
    @GetMapping("/ind-uni-anas")
    public ResponseEntity<List<IndUniAna>> getAllIndUniAnas(IndUniAnaCriteria criteria) {
        log.debug("REST request to get IndUniAnas by criteria: {}", criteria);
        List<IndUniAna> entityList = indUniAnaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-uni-anas/count} : count all the indUniAnas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-uni-anas/count")
    public ResponseEntity<Long> countIndUniAnas(IndUniAnaCriteria criteria) {
        log.debug("REST request to count IndUniAnas by criteria: {}", criteria);
        return ResponseEntity.ok().body(indUniAnaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-uni-anas/:id} : get the "id" indUniAna.
     *
     * @param id the id of the indUniAna to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indUniAna, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-uni-anas/{id}")
    public ResponseEntity<IndUniAna> getIndUniAna(@PathVariable Long id) {
        log.debug("REST request to get IndUniAna : {}", id);
        Optional<IndUniAna> indUniAna = indUniAnaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indUniAna);
    }

    /**
     * {@code DELETE  /ind-uni-anas/:id} : delete the "id" indUniAna.
     *
     * @param id the id of the indUniAna to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-uni-anas/{id}")
    public ResponseEntity<Void> deleteIndUniAna(@PathVariable Long id) {
        log.debug("REST request to delete IndUniAna : {}", id);
        indUniAnaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
