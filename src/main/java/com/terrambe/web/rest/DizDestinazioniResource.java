package com.terrambe.web.rest;

import com.terrambe.domain.DizDestinazioni;
import com.terrambe.service.DizDestinazioniService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizDestinazioniCriteria;
import com.terrambe.service.DizDestinazioniQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizDestinazioni}.
 */
@RestController
@RequestMapping("/api")
public class DizDestinazioniResource {

    private final Logger log = LoggerFactory.getLogger(DizDestinazioniResource.class);

    private static final String ENTITY_NAME = "dizDestinazioni";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizDestinazioniService dizDestinazioniService;

    private final DizDestinazioniQueryService dizDestinazioniQueryService;

    public DizDestinazioniResource(DizDestinazioniService dizDestinazioniService, DizDestinazioniQueryService dizDestinazioniQueryService) {
        this.dizDestinazioniService = dizDestinazioniService;
        this.dizDestinazioniQueryService = dizDestinazioniQueryService;
    }

    /**
     * {@code POST  /diz-destinazionis} : Create a new dizDestinazioni.
     *
     * @param dizDestinazioni the dizDestinazioni to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizDestinazioni, or with status {@code 400 (Bad Request)} if the dizDestinazioni has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-destinazionis")
    public ResponseEntity<DizDestinazioni> createDizDestinazioni(@RequestBody DizDestinazioni dizDestinazioni) throws URISyntaxException {
        log.debug("REST request to save DizDestinazioni : {}", dizDestinazioni);
        if (dizDestinazioni.getId() != null) {
            throw new BadRequestAlertException("A new dizDestinazioni cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizDestinazioni result = dizDestinazioniService.save(dizDestinazioni);
        return ResponseEntity.created(new URI("/api/diz-destinazionis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-destinazionis} : Updates an existing dizDestinazioni.
     *
     * @param dizDestinazioni the dizDestinazioni to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizDestinazioni,
     * or with status {@code 400 (Bad Request)} if the dizDestinazioni is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizDestinazioni couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-destinazionis")
    public ResponseEntity<DizDestinazioni> updateDizDestinazioni(@RequestBody DizDestinazioni dizDestinazioni) throws URISyntaxException {
        log.debug("REST request to update DizDestinazioni : {}", dizDestinazioni);
        if (dizDestinazioni.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizDestinazioni result = dizDestinazioniService.save(dizDestinazioni);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizDestinazioni.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-destinazionis} : get all the dizDestinazionis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizDestinazionis in body.
     */
    @GetMapping("/diz-destinazionis")
    public ResponseEntity<List<DizDestinazioni>> getAllDizDestinazionis(DizDestinazioniCriteria criteria) {
        log.debug("REST request to get DizDestinazionis by criteria: {}", criteria);
        List<DizDestinazioni> entityList = dizDestinazioniQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-destinazionis/count} : count all the dizDestinazionis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-destinazionis/count")
    public ResponseEntity<Long> countDizDestinazionis(DizDestinazioniCriteria criteria) {
        log.debug("REST request to count DizDestinazionis by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizDestinazioniQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-destinazionis/:id} : get the "id" dizDestinazioni.
     *
     * @param id the id of the dizDestinazioni to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizDestinazioni, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-destinazionis/{id}")
    public ResponseEntity<DizDestinazioni> getDizDestinazioni(@PathVariable Long id) {
        log.debug("REST request to get DizDestinazioni : {}", id);
        Optional<DizDestinazioni> dizDestinazioni = dizDestinazioniService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizDestinazioni);
    }

    /**
     * {@code DELETE  /diz-destinazionis/:id} : delete the "id" dizDestinazioni.
     *
     * @param id the id of the dizDestinazioni to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-destinazionis/{id}")
    public ResponseEntity<Void> deleteDizDestinazioni(@PathVariable Long id) {
        log.debug("REST request to delete DizDestinazioni : {}", id);
        dizDestinazioniService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
