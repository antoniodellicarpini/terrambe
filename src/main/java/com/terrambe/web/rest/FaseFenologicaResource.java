package com.terrambe.web.rest;

import com.terrambe.domain.FaseFenologica;
import com.terrambe.service.FaseFenologicaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FaseFenologicaCriteria;
import com.terrambe.service.FaseFenologicaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.FaseFenologica}.
 */
@RestController
@RequestMapping("/api")
public class FaseFenologicaResource {

    private final Logger log = LoggerFactory.getLogger(FaseFenologicaResource.class);

    private static final String ENTITY_NAME = "faseFenologica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FaseFenologicaService faseFenologicaService;

    private final FaseFenologicaQueryService faseFenologicaQueryService;

    public FaseFenologicaResource(FaseFenologicaService faseFenologicaService, FaseFenologicaQueryService faseFenologicaQueryService) {
        this.faseFenologicaService = faseFenologicaService;
        this.faseFenologicaQueryService = faseFenologicaQueryService;
    }

    /**
     * {@code POST  /fase-fenologicas} : Create a new faseFenologica.
     *
     * @param faseFenologica the faseFenologica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new faseFenologica, or with status {@code 400 (Bad Request)} if the faseFenologica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fase-fenologicas")
    public ResponseEntity<FaseFenologica> createFaseFenologica(@RequestBody FaseFenologica faseFenologica) throws URISyntaxException {
        log.debug("REST request to save FaseFenologica : {}", faseFenologica);
        if (faseFenologica.getId() != null) {
            throw new BadRequestAlertException("A new faseFenologica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FaseFenologica result = faseFenologicaService.save(faseFenologica);
        return ResponseEntity.created(new URI("/api/fase-fenologicas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fase-fenologicas} : Updates an existing faseFenologica.
     *
     * @param faseFenologica the faseFenologica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated faseFenologica,
     * or with status {@code 400 (Bad Request)} if the faseFenologica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the faseFenologica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fase-fenologicas")
    public ResponseEntity<FaseFenologica> updateFaseFenologica(@RequestBody FaseFenologica faseFenologica) throws URISyntaxException {
        log.debug("REST request to update FaseFenologica : {}", faseFenologica);
        if (faseFenologica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FaseFenologica result = faseFenologicaService.save(faseFenologica);
        FaseFenologica result2 = faseFenologicaService.save(faseFenologica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, faseFenologica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fase-fenologicas} : get all the faseFenologicas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of faseFenologicas in body.
     */
    @GetMapping("/fase-fenologicas")
    public ResponseEntity<List<FaseFenologica>> getAllFaseFenologicas(FaseFenologicaCriteria criteria) {
        log.debug("REST request to get FaseFenologicas by criteria: {}", criteria);
        List<FaseFenologica> entityList = faseFenologicaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /fase-fenologicas/count} : count all the faseFenologicas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fase-fenologicas/count")
    public ResponseEntity<Long> countFaseFenologicas(FaseFenologicaCriteria criteria) {
        log.debug("REST request to count FaseFenologicas by criteria: {}", criteria);
        return ResponseEntity.ok().body(faseFenologicaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fase-fenologicas/:id} : get the "id" faseFenologica.
     *
     * @param id the id of the faseFenologica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the faseFenologica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fase-fenologicas/{id}")
    public ResponseEntity<FaseFenologica> getFaseFenologica(@PathVariable Long id) {
        log.debug("REST request to get FaseFenologica : {}", id);
        Optional<FaseFenologica> faseFenologica = faseFenologicaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(faseFenologica);
    }

    /**
     * {@code DELETE  /fase-fenologicas/:id} : delete the "id" faseFenologica.
     *
     * @param id the id of the faseFenologica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fase-fenologicas/{id}")
    public ResponseEntity<Void> deleteFaseFenologica(@PathVariable Long id) {
        log.debug("REST request to delete FaseFenologica : {}", id);
        faseFenologicaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
