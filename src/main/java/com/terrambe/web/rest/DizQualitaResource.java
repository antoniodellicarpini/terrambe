package com.terrambe.web.rest;

import com.terrambe.domain.DizQualita;
import com.terrambe.service.DizQualitaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizQualitaCriteria;
import com.terrambe.service.DizQualitaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizQualita}.
 */
@RestController
@RequestMapping("/api")
public class DizQualitaResource {

    private final Logger log = LoggerFactory.getLogger(DizQualitaResource.class);

    private static final String ENTITY_NAME = "dizQualita";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizQualitaService dizQualitaService;

    private final DizQualitaQueryService dizQualitaQueryService;

    public DizQualitaResource(DizQualitaService dizQualitaService, DizQualitaQueryService dizQualitaQueryService) {
        this.dizQualitaService = dizQualitaService;
        this.dizQualitaQueryService = dizQualitaQueryService;
    }

    /**
     * {@code POST  /diz-qualitas} : Create a new dizQualita.
     *
     * @param dizQualita the dizQualita to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizQualita, or with status {@code 400 (Bad Request)} if the dizQualita has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-qualitas")
    public ResponseEntity<DizQualita> createDizQualita(@RequestBody DizQualita dizQualita) throws URISyntaxException {
        log.debug("REST request to save DizQualita : {}", dizQualita);
        if (dizQualita.getId() != null) {
            throw new BadRequestAlertException("A new dizQualita cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizQualita result = dizQualitaService.save(dizQualita);
        return ResponseEntity.created(new URI("/api/diz-qualitas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-qualitas} : Updates an existing dizQualita.
     *
     * @param dizQualita the dizQualita to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizQualita,
     * or with status {@code 400 (Bad Request)} if the dizQualita is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizQualita couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-qualitas")
    public ResponseEntity<DizQualita> updateDizQualita(@RequestBody DizQualita dizQualita) throws URISyntaxException {
        log.debug("REST request to update DizQualita : {}", dizQualita);
        if (dizQualita.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizQualita result = dizQualitaService.save(dizQualita);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizQualita.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-qualitas} : get all the dizQualitas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizQualitas in body.
     */
    @GetMapping("/diz-qualitas")
    public ResponseEntity<List<DizQualita>> getAllDizQualitas(DizQualitaCriteria criteria) {
        log.debug("REST request to get DizQualitas by criteria: {}", criteria);
        List<DizQualita> entityList = dizQualitaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-qualitas/count} : count all the dizQualitas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-qualitas/count")
    public ResponseEntity<Long> countDizQualitas(DizQualitaCriteria criteria) {
        log.debug("REST request to count DizQualitas by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizQualitaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-qualitas/:id} : get the "id" dizQualita.
     *
     * @param id the id of the dizQualita to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizQualita, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-qualitas/{id}")
    public ResponseEntity<DizQualita> getDizQualita(@PathVariable Long id) {
        log.debug("REST request to get DizQualita : {}", id);
        Optional<DizQualita> dizQualita = dizQualitaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizQualita);
    }

    /**
     * {@code DELETE  /diz-qualitas/:id} : delete the "id" dizQualita.
     *
     * @param id the id of the dizQualita to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-qualitas/{id}")
    public ResponseEntity<Void> deleteDizQualita(@PathVariable Long id) {
        log.debug("REST request to delete DizQualita : {}", id);
        dizQualitaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
