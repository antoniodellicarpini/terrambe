package com.terrambe.web.rest;

import com.terrambe.domain.RecAziRap;
import com.terrambe.service.RecAziRapService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecAziRapCriteria;
import com.terrambe.service.RecAziRapQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecAziRap}.
 */
@RestController
@RequestMapping("/api")
public class RecAziRapResource {

    private final Logger log = LoggerFactory.getLogger(RecAziRapResource.class);

    private static final String ENTITY_NAME = "recAziRap";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecAziRapService recAziRapService;

    private final RecAziRapQueryService recAziRapQueryService;

    public RecAziRapResource(RecAziRapService recAziRapService, RecAziRapQueryService recAziRapQueryService) {
        this.recAziRapService = recAziRapService;
        this.recAziRapQueryService = recAziRapQueryService;
    }

    /**
     * {@code POST  /rec-azi-raps} : Create a new recAziRap.
     *
     * @param recAziRap the recAziRap to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recAziRap, or with status {@code 400 (Bad Request)} if the recAziRap has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-azi-raps")
    public ResponseEntity<RecAziRap> createRecAziRap(@RequestBody RecAziRap recAziRap) throws URISyntaxException {
        log.debug("REST request to save RecAziRap : {}", recAziRap);
        if (recAziRap.getId() != null) {
            throw new BadRequestAlertException("A new recAziRap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecAziRap result = recAziRapService.save(recAziRap);
        return ResponseEntity.created(new URI("/api/rec-azi-raps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-azi-raps} : Updates an existing recAziRap.
     *
     * @param recAziRap the recAziRap to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recAziRap,
     * or with status {@code 400 (Bad Request)} if the recAziRap is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recAziRap couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-azi-raps")
    public ResponseEntity<RecAziRap> updateRecAziRap(@RequestBody RecAziRap recAziRap) throws URISyntaxException {
        log.debug("REST request to update RecAziRap : {}", recAziRap);
        if (recAziRap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecAziRap result = recAziRapService.save(recAziRap);
        RecAziRap result2 = recAziRapService.save(recAziRap);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recAziRap.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-azi-raps} : get all the recAziRaps.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recAziRaps in body.
     */
    @GetMapping("/rec-azi-raps")
    public ResponseEntity<List<RecAziRap>> getAllRecAziRaps(RecAziRapCriteria criteria) {
        log.debug("REST request to get RecAziRaps by criteria: {}", criteria);
        List<RecAziRap> entityList = recAziRapQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-azi-raps/count} : count all the recAziRaps.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-azi-raps/count")
    public ResponseEntity<Long> countRecAziRaps(RecAziRapCriteria criteria) {
        log.debug("REST request to count RecAziRaps by criteria: {}", criteria);
        return ResponseEntity.ok().body(recAziRapQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-azi-raps/:id} : get the "id" recAziRap.
     *
     * @param id the id of the recAziRap to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recAziRap, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-azi-raps/{id}")
    public ResponseEntity<RecAziRap> getRecAziRap(@PathVariable Long id) {
        log.debug("REST request to get RecAziRap : {}", id);
        Optional<RecAziRap> recAziRap = recAziRapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recAziRap);
    }

    /**
     * {@code DELETE  /rec-azi-raps/:id} : delete the "id" recAziRap.
     *
     * @param id the id of the recAziRap to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-azi-raps/{id}")
    public ResponseEntity<Void> deleteRecAziRap(@PathVariable Long id) {
        log.debug("REST request to delete RecAziRap : {}", id);
        recAziRapService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
