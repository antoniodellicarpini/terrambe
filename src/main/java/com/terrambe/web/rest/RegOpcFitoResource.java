package com.terrambe.web.rest;
import com.google.gson.Gson;
import com.terrambe.domain.*;
import com.terrambe.service.*;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcFitoCriteria;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.swing.text.html.Option;
import javax.validation.Payload;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import javax.sql.DataSource;
/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcFito}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcFitoResource {
    @Autowired
    private DataSource dataSource;
    private final Logger log = LoggerFactory.getLogger(RegOpcFitoResource.class);
    private static final String ENTITY_NAME = "regOpcFito";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final RegOpcFitoService regOpcFitoService;
    private final RegOpcFitoQueryService regOpcFitoQueryService;
    UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
    @Autowired
    private Agea20152020MatriceService ageaMatServ;
    @Autowired
    private AtrAnagraficaService atrServ;
    @Autowired
    private OpeAnagraficaService opeAnServ;
    @Autowired
    private AziAnagraficaService aziAnaServ;
    @Autowired
    private UniAnagraficaService uniAnaServ;
    @Autowired
    private FaseFenologicaService faseFenServ;
    public RegOpcFitoResource(RegOpcFitoService regOpcFitoService, RegOpcFitoQueryService regOpcFitoQueryService) {
        this.regOpcFitoService = regOpcFitoService;
        this.regOpcFitoQueryService = regOpcFitoQueryService;
    }
    /**
     * {@code POST  /reg-opc-fitos} : Create a new regOpcFito.
     *
     * @param regOpcFito the regOpcFito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcFito, or with status {@code 400 (Bad Request)} if the regOpcFito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-fitos")
    public ResponseEntity<?>  createRegOpcFito(@Valid @RequestBody RegOpcFito regOpcFito) throws URISyntaxException {
        // log.debug("REST request to save RegOpcFito : {}", regOpcFito);
        //     if (regOpcFito.getId() != null) {
        //         throw new BadRequestAlertException("A new regOpcFito cannot already have an ID", ENTITY_NAME, "idexists");
        //     }
        //     RegOpcFito result = regOpcFitoService.save(regOpcFito);
        //     Optional<RegOpcFito> storedRegOpcFito = regOpcFitoService.findOne(result.getId());
        // return ResponseUtil.wrapOrNotFound(storedRegOpcFito);
        List<ErrorTerram> list_errorTerram = new ArrayList<>();
        list_errorTerram = utility.controlliFito(regOpcFito, dataSource);
        // list_errorTerram.add(new ErrorTerram(null, null, "testtt 1"));
        if(list_errorTerram.size()==0){
             log.debug("REST request to save RegOpcFito : {}", regOpcFito);
            if (regOpcFito.getId() != null) {
                throw new BadRequestAlertException("A new regOpcFito cannot already have an ID", ENTITY_NAME, "idexists");
            }
            RegOpcFito result = regOpcFitoService.save(regOpcFito);
            Optional<RegOpcFito> storedRegOpcFito = regOpcFitoService.findOne(result.getId());
            return ResponseUtil.wrapOrNotFound(storedRegOpcFito);
        }else{
            return new ResponseEntity<>(list_errorTerram, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    /**
     * {@code PUT  /reg-opc-fitos} : Updates an existing regOpcFito.
     *
     * @param regOpcFito the regOpcFito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcFito,
     * or with status {@code 400 (Bad Request)} if the regOpcFito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcFito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-fitos")
    public ResponseEntity<RegOpcFito> updateRegOpcFito(@Valid @RequestBody RegOpcFito regOpcFito) throws URISyntaxException {
        log.debug("REST request to update RegOpcFito : {}", regOpcFito);
        if (regOpcFito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcFito result = regOpcFitoService.save(regOpcFito);
        RegOpcFito result2 = regOpcFitoService.save(regOpcFito);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcFito.getId().toString()))
            .body(result);
    }
    /**
     * {@code GET  /opcfitos} : get all the opcfitos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regopcfitos in body.
     */
    @GetMapping("/reg-opc-fitos")
    public ResponseEntity<List<RegOpcFito>> getAllRegOpcFito(RegOpcFitoCriteria criteria) throws Exception {
        log.debug("REST request to get RegOpcFitos by criteria: {}", criteria);
        List<RegOpcFito> entityList = regOpcFitoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }
    /**
    * {@code GET  /reg-opc-fitos/count} : count all the regOpcFitos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-fitos/count")
    public ResponseEntity<Long> countRegOpcFitos(RegOpcFitoCriteria criteria) {
        log.debug("REST request to count RegOpcFitos by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcFitoQueryService.countByCriteria(criteria));
    }
    /**
     * Mod2
     * {@code GET  /reg-opc-fitos/:id} : get the "id" regOpcFito.
     *
     * @param id the id of the regOpcFito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcFito, or with status {@code 404 (Not Found)}.
     */
    //public ResponseEntity<ListRegOpcFito> getRegOpcFito(@PathVariable Long id) throws Exception {
//test prova restituzione json in formato string
        @GetMapping("/reg-opc-fitos/{id}")
        public ResponseEntity<RegOpcFito> getRegOpcFito(@PathVariable Long id) throws Exception {
        List result = new ArrayList();
        log.debug("REST request to get single RegOpcFito : {}", id);
        ListRegOpcFito singlelistregopcfito= utility.getRegOpcFito(id, dataSource);
        Optional<RegOpcFito> regOpcFito = regOpcFitoService.findOne(id);
        /*if(regOpcFito.get().getIdAzienda() != null) {
            Optional<AziAnagrafica> aziAnagrafica = aziAnaServ.findOne(regOpcFito.get().getIdAzienda());
            result.add(aziAnagrafica);
        }
        if(regOpcFito.get().getIdUnitaProd()!= null) {
            Optional<UniAnagrafica> uniAnagrafica = uniAnaServ.findOne(regOpcFito.get().getIdUnitaProd());
            result.add(uniAnagrafica);
        }
        if(regOpcFito.get().getIdColtura() != null) {
            Optional<Agea20152020Matrice> ageaMat = ageaMatServ.findOne(regOpcFito.get().getIdColtura());
            result.add(ageaMat);
        }
        if(regOpcFito.get().getIdFaseFenologica() != null) {
            Optional<FaseFenologica> faseFen = faseFenServ.findOne(regOpcFito.get().getIdFaseFenologica());
            result.add(faseFen);
        }
        if(regOpcFito.get().getIdOperatore() != null) {
            Optional<OpeAnagrafica> opeAn = opeAnServ.findOne(regOpcFito.get().getIdOperatore());
            result.add(opeAn);
        }
        if(regOpcFito.get().getIdMezzo() != null ) {
            Optional<AtrAnagrafica> atrAn = atrServ.findOne(regOpcFito.get().getIdMezzo());
            result.add(atrAn);
        }
        */
        //result.add(regOpcFito);
        //result.add(singlelistregopcfito);
        //return ResponseUtil.wrapOrNotFound(regOpcFito);
        //return result
        //return ResponseEntity.ok().body(regOpcFito);
        return ResponseUtil.wrapOrNotFound(regOpcFito);
        }
    /**
     * {@code DELETE  /reg-opc-fitos/:id} : delete the "id" regOpcFito.
     *
     * @param id the id of the regOpcFito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-fitos/{id}")
    public ResponseEntity<Void> deleteRegOpcFito(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcFito : {}", id);
        regOpcFitoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}