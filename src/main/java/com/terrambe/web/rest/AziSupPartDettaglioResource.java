package com.terrambe.web.rest;

import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.service.AziSupPartDettaglioService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziSupPartDettaglioCriteria;
import com.terrambe.service.AziSupPartDettaglioQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziSupPartDettaglio}.
 */
@RestController
@RequestMapping("/api")
public class AziSupPartDettaglioResource {

    private final Logger log = LoggerFactory.getLogger(AziSupPartDettaglioResource.class);

    private static final String ENTITY_NAME = "aziSupPartDettaglio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziSupPartDettaglioService aziSupPartDettaglioService;

    private final AziSupPartDettaglioQueryService aziSupPartDettaglioQueryService;

    public AziSupPartDettaglioResource(AziSupPartDettaglioService aziSupPartDettaglioService, AziSupPartDettaglioQueryService aziSupPartDettaglioQueryService) {
        this.aziSupPartDettaglioService = aziSupPartDettaglioService;
        this.aziSupPartDettaglioQueryService = aziSupPartDettaglioQueryService;
    }

    /**
     * {@code POST  /azi-sup-part-dettaglios} : Create a new aziSupPartDettaglio.
     *
     * @param aziSupPartDettaglio the aziSupPartDettaglio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziSupPartDettaglio, or with status {@code 400 (Bad Request)} if the aziSupPartDettaglio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-sup-part-dettaglios")
    public ResponseEntity<AziSupPartDettaglio> createAziSupPartDettaglio(@Valid @RequestBody AziSupPartDettaglio aziSupPartDettaglio) throws URISyntaxException {
        log.debug("REST request to save AziSupPartDettaglio : {}", aziSupPartDettaglio);
        if (aziSupPartDettaglio.getId() != null) {
            throw new BadRequestAlertException("A new aziSupPartDettaglio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziSupPartDettaglio result = aziSupPartDettaglioService.save(aziSupPartDettaglio);
        return ResponseEntity.created(new URI("/api/azi-sup-part-dettaglios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-sup-part-dettaglios} : Updates an existing aziSupPartDettaglio.
     *
     * @param aziSupPartDettaglio the aziSupPartDettaglio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziSupPartDettaglio,
     * or with status {@code 400 (Bad Request)} if the aziSupPartDettaglio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziSupPartDettaglio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-sup-part-dettaglios")
    public ResponseEntity<AziSupPartDettaglio> updateAziSupPartDettaglio(@Valid @RequestBody AziSupPartDettaglio aziSupPartDettaglio) throws URISyntaxException {
        log.debug("REST request to update AziSupPartDettaglio : {}", aziSupPartDettaglio);
        if (aziSupPartDettaglio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziSupPartDettaglio result = aziSupPartDettaglioService.save(aziSupPartDettaglio);
        AziSupPartDettaglio result2 = aziSupPartDettaglioService.save(aziSupPartDettaglio);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziSupPartDettaglio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-sup-part-dettaglios} : get all the aziSupPartDettaglios.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziSupPartDettaglios in body.
     */
    @GetMapping("/azi-sup-part-dettaglios")
    public ResponseEntity<List<AziSupPartDettaglio>> getAllAziSupPartDettaglios(AziSupPartDettaglioCriteria criteria) {
        log.debug("REST request to get AziSupPartDettaglios by criteria: {}", criteria);
        List<AziSupPartDettaglio> entityList = aziSupPartDettaglioQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-sup-part-dettaglios/count} : count all the aziSupPartDettaglios.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-sup-part-dettaglios/count")
    public ResponseEntity<Long> countAziSupPartDettaglios(AziSupPartDettaglioCriteria criteria) {
        log.debug("REST request to count AziSupPartDettaglios by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziSupPartDettaglioQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-sup-part-dettaglios/:id} : get the "id" aziSupPartDettaglio.
     *
     * @param id the id of the aziSupPartDettaglio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziSupPartDettaglio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-sup-part-dettaglios/{id}")
    public ResponseEntity<AziSupPartDettaglio> getAziSupPartDettaglio(@PathVariable Long id) {
        log.debug("REST request to get AziSupPartDettaglio : {}", id);
        Optional<AziSupPartDettaglio> aziSupPartDettaglio = aziSupPartDettaglioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziSupPartDettaglio);
    }

    /**
     * {@code DELETE  /azi-sup-part-dettaglios/:id} : delete the "id" aziSupPartDettaglio.
     *
     * @param id the id of the aziSupPartDettaglio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-sup-part-dettaglios/{id}")
    public ResponseEntity<Void> deleteAziSupPartDettaglio(@PathVariable Long id) {
        log.debug("REST request to delete AziSupPartDettaglio : {}", id);
        aziSupPartDettaglioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
