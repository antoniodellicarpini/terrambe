package com.terrambe.web.rest;

import com.terrambe.domain.IndNascRap;
import com.terrambe.service.IndNascRapService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndNascRapCriteria;
import com.terrambe.service.IndNascRapQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.terrambe.domain.IndNascRap}.
 */
@RestController
@RequestMapping("/api")
public class IndNascRapResource {

    private final Logger log = LoggerFactory.getLogger(IndNascRapResource.class);

    private static final String ENTITY_NAME = "indNascRap";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndNascRapService indNascRapService;

    private final IndNascRapQueryService indNascRapQueryService;

    public IndNascRapResource(IndNascRapService indNascRapService, IndNascRapQueryService indNascRapQueryService) {
        this.indNascRapService = indNascRapService;
        this.indNascRapQueryService = indNascRapQueryService;
    }

    /**
     * {@code POST  /ind-nasc-raps} : Create a new indNascRap.
     *
     * @param indNascRap the indNascRap to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indNascRap, or with status {@code 400 (Bad Request)} if the indNascRap has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-nasc-raps")
    public ResponseEntity<IndNascRap> createIndNascRap(@Valid @RequestBody IndNascRap indNascRap) throws URISyntaxException {
        log.debug("REST request to save IndNascRap : {}", indNascRap);
        if (indNascRap.getId() != null) {
            throw new BadRequestAlertException("A new indNascRap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndNascRap result = indNascRapService.save(indNascRap);
        return ResponseEntity.created(new URI("/api/ind-nasc-raps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-nasc-raps} : Updates an existing indNascRap.
     *
     * @param indNascRap the indNascRap to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indNascRap,
     * or with status {@code 400 (Bad Request)} if the indNascRap is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indNascRap couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-nasc-raps")
    public ResponseEntity<IndNascRap> updateIndNascRap(@Valid @RequestBody IndNascRap indNascRap) throws URISyntaxException {
        log.debug("REST request to update IndNascRap : {}", indNascRap);
        if (indNascRap.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndNascRap result = indNascRapService.save(indNascRap);
        IndNascRap result2 = indNascRapService.save(indNascRap);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indNascRap.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-nasc-raps} : get all the indNascRaps.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indNascRaps in body.
     */
    @GetMapping("/ind-nasc-raps")
    public ResponseEntity<List<IndNascRap>> getAllIndNascRaps(IndNascRapCriteria criteria) {
        log.debug("REST request to get IndNascRaps by criteria: {}", criteria);
        List<IndNascRap> entityList = indNascRapQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-nasc-raps/count} : count all the indNascRaps.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-nasc-raps/count")
    public ResponseEntity<Long> countIndNascRaps(IndNascRapCriteria criteria) {
        log.debug("REST request to count IndNascRaps by criteria: {}", criteria);
        return ResponseEntity.ok().body(indNascRapQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-nasc-raps/:id} : get the "id" indNascRap.
     *
     * @param id the id of the indNascRap to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indNascRap, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-nasc-raps/{id}")
    public ResponseEntity<IndNascRap> getIndNascRap(@PathVariable Long id) {
        log.debug("REST request to get IndNascRap : {}", id);
        Optional<IndNascRap> indNascRap = indNascRapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indNascRap);
    }

    /**
     * {@code DELETE  /ind-nasc-raps/:id} : delete the "id" indNascRap.
     *
     * @param id the id of the indNascRap to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-nasc-raps/{id}")
    public ResponseEntity<Void> deleteIndNascRap(@PathVariable Long id) {
        log.debug("REST request to delete IndNascRap : {}", id);
        indNascRapService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
