package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcConcProd;
import com.terrambe.service.RegOpcConcProdService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcConcProdCriteria;
import com.terrambe.service.RegOpcConcProdQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcConcProd}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcConcProdResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcProdResource.class);

    private static final String ENTITY_NAME = "regOpcConcProd";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcConcProdService regOpcConcProdService;

    private final RegOpcConcProdQueryService regOpcConcProdQueryService;

    public RegOpcConcProdResource(RegOpcConcProdService regOpcConcProdService, RegOpcConcProdQueryService regOpcConcProdQueryService) {
        this.regOpcConcProdService = regOpcConcProdService;
        this.regOpcConcProdQueryService = regOpcConcProdQueryService;
    }

    /**
     * {@code POST  /reg-opc-conc-prods} : Create a new regOpcConcProd.
     *
     * @param regOpcConcProd the regOpcConcProd to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcConcProd, or with status {@code 400 (Bad Request)} if the regOpcConcProd has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-conc-prods")
    public ResponseEntity<RegOpcConcProd> createRegOpcConcProd(@Valid @RequestBody RegOpcConcProd regOpcConcProd) throws URISyntaxException {
        log.debug("REST request to save RegOpcConcProd : {}", regOpcConcProd);
        if (regOpcConcProd.getId() != null) {
            throw new BadRequestAlertException("A new regOpcConcProd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcConcProd result = regOpcConcProdService.save(regOpcConcProd);
        return ResponseEntity.created(new URI("/api/reg-opc-conc-prods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-conc-prods} : Updates an existing regOpcConcProd.
     *
     * @param regOpcConcProd the regOpcConcProd to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcConcProd,
     * or with status {@code 400 (Bad Request)} if the regOpcConcProd is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcConcProd couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-conc-prods")
    public ResponseEntity<RegOpcConcProd> updateRegOpcConcProd(@Valid @RequestBody RegOpcConcProd regOpcConcProd) throws URISyntaxException {
        log.debug("REST request to update RegOpcConcProd : {}", regOpcConcProd);
        if (regOpcConcProd.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcConcProd result = regOpcConcProdService.save(regOpcConcProd);
        RegOpcConcProd result2 = regOpcConcProdService.save(regOpcConcProd);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcConcProd.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-conc-prods} : get all the regOpcConcProds.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcConcProds in body.
     */
    @GetMapping("/reg-opc-conc-prods")
    public ResponseEntity<List<RegOpcConcProd>> getAllRegOpcConcProds(RegOpcConcProdCriteria criteria) {
        log.debug("REST request to get RegOpcConcProds by criteria: {}", criteria);
        List<RegOpcConcProd> entityList = regOpcConcProdQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-conc-prods/count} : count all the regOpcConcProds.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-conc-prods/count")
    public ResponseEntity<Long> countRegOpcConcProds(RegOpcConcProdCriteria criteria) {
        log.debug("REST request to count RegOpcConcProds by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcConcProdQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-conc-prods/:id} : get the "id" regOpcConcProd.
     *
     * @param id the id of the regOpcConcProd to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcConcProd, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-conc-prods/{id}")
    public ResponseEntity<RegOpcConcProd> getRegOpcConcProd(@PathVariable Long id) {
        log.debug("REST request to get RegOpcConcProd : {}", id);
        Optional<RegOpcConcProd> regOpcConcProd = regOpcConcProdService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcConcProd);
    }

    /**
     * {@code DELETE  /reg-opc-conc-prods/:id} : delete the "id" regOpcConcProd.
     *
     * @param id the id of the regOpcConcProd to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-conc-prods/{id}")
    public ResponseEntity<Void> deleteRegOpcConcProd(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcConcProd : {}", id);
        regOpcConcProdService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
