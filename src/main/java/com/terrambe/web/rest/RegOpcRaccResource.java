package com.terrambe.web.rest;

import com.terrambe.domain.ListRegOpcRaccolta;
import com.terrambe.domain.RegOpcRacc;
import com.terrambe.service.RegOpcRaccService;
import com.terrambe.service.UtilityJpaQueriesService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcRaccCriteria;
import com.terrambe.service.RegOpcRaccQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;


/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcRacc}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcRaccResource {

    @Autowired
    private DataSource dataSource;
    
    private final Logger log = LoggerFactory.getLogger(RegOpcRaccResource.class);

    private static final String ENTITY_NAME = "regOpcRacc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcRaccService regOpcRaccService;

    private final RegOpcRaccQueryService regOpcRaccQueryService;

    public RegOpcRaccResource(RegOpcRaccService regOpcRaccService, RegOpcRaccQueryService regOpcRaccQueryService) {
        this.regOpcRaccService = regOpcRaccService;
        this.regOpcRaccQueryService = regOpcRaccQueryService;
    }

    /**
     * {@code POST  /reg-opc-raccs} : Create a new regOpcRacc.
     *
     * @param regOpcRacc the regOpcRacc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcRacc, or with status {@code 400 (Bad Request)} if the regOpcRacc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-raccs")
    public ResponseEntity<RegOpcRacc> createRegOpcRacc(@Valid @RequestBody RegOpcRacc regOpcRacc) throws URISyntaxException {
        log.debug("REST request to save RegOpcRacc : {}", regOpcRacc);
        if (regOpcRacc.getId() != null) {
            throw new BadRequestAlertException("A new regOpcRacc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcRacc result = regOpcRaccService.save(regOpcRacc);
        return ResponseEntity.created(new URI("/api/reg-opc-raccs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-raccs} : Updates an existing regOpcRacc.
     *
     * @param regOpcRacc the regOpcRacc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcRacc,
     * or with status {@code 400 (Bad Request)} if the regOpcRacc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcRacc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-raccs")
    public ResponseEntity<RegOpcRacc> updateRegOpcRacc(@Valid @RequestBody RegOpcRacc regOpcRacc) throws URISyntaxException {
        log.debug("REST request to update RegOpcRacc : {}", regOpcRacc);
        if (regOpcRacc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcRacc result = regOpcRaccService.save(regOpcRacc);
        RegOpcRacc result2 = regOpcRaccService.save(regOpcRacc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcRacc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-raccs} : get all the regOpcRaccs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcRaccs in body.
     */
    @GetMapping("/reg-opc-raccs")
    public ResponseEntity<List<RegOpcRacc>> getAllRegOpcRaccs(RegOpcRaccCriteria criteria) {
        log.debug("REST request to get RegOpcRaccs by criteria: {}", criteria);
        List<RegOpcRacc> entityList = regOpcRaccQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }


    @GetMapping("/reg-opc-raccolta-list")
    public ResponseEntity<List<ListRegOpcRaccolta>> getListRegOpcRaccolta(@RequestParam(value = "azienda_id.equals") Long azienda_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<ListRegOpcRaccolta> result_listregopcfito= utility.getListRegOpcRaccolta(azienda_id, dataSource);
        return ResponseEntity.ok().body(result_listregopcfito);
    }

    /**
    * {@code GET  /reg-opc-raccs/count} : count all the regOpcRaccs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-raccs/count")
    public ResponseEntity<Long> countRegOpcRaccs(RegOpcRaccCriteria criteria) {
        log.debug("REST request to count RegOpcRaccs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcRaccQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-raccs/:id} : get the "id" regOpcRacc.
     *
     * @param id the id of the regOpcRacc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcRacc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-raccs/{id}")
    public ResponseEntity<RegOpcRacc> getRegOpcRacc(@PathVariable Long id) {
        log.debug("REST request to get RegOpcRacc : {}", id);
        Optional<RegOpcRacc> regOpcRacc = regOpcRaccService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcRacc);
    }

    /**
     * {@code DELETE  /reg-opc-raccs/:id} : delete the "id" regOpcRacc.
     *
     * @param id the id of the regOpcRacc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-raccs/{id}")
    public ResponseEntity<Void> deleteRegOpcRacc(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcRacc : {}", id);
        regOpcRaccService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
