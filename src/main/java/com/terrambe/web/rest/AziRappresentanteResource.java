package com.terrambe.web.rest;

import com.terrambe.domain.AziRappresentante;
import com.terrambe.service.AziRappresentanteService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziRappresentanteCriteria;
import com.terrambe.service.AziRappresentanteQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziRappresentante}.
 */
@RestController
@RequestMapping("/api")
public class AziRappresentanteResource {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteResource.class);

    private static final String ENTITY_NAME = "aziRappresentante";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziRappresentanteService aziRappresentanteService;

    private final AziRappresentanteQueryService aziRappresentanteQueryService;

    public AziRappresentanteResource(AziRappresentanteService aziRappresentanteService, AziRappresentanteQueryService aziRappresentanteQueryService) {
        this.aziRappresentanteService = aziRappresentanteService;
        this.aziRappresentanteQueryService = aziRappresentanteQueryService;
    }

    /**
     * {@code POST  /azi-rappresentantes} : Create a new aziRappresentante.
     *
     * @param aziRappresentante the aziRappresentante to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziRappresentante, or with status {@code 400 (Bad Request)} if the aziRappresentante has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-rappresentantes")
    public ResponseEntity<AziRappresentante> createAziRappresentante(@Valid @RequestBody AziRappresentante aziRappresentante) throws URISyntaxException {
        log.debug("REST request to save AziRappresentante : {}", aziRappresentante);
        if (aziRappresentante.getId() != null) {
            throw new BadRequestAlertException("A new aziRappresentante cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziRappresentante result = aziRappresentanteService.save(aziRappresentante);
        return ResponseEntity.created(new URI("/api/azi-rappresentantes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-rappresentantes} : Updates an existing aziRappresentante.
     *
     * @param aziRappresentante the aziRappresentante to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziRappresentante,
     * or with status {@code 400 (Bad Request)} if the aziRappresentante is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziRappresentante couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-rappresentantes")
    public ResponseEntity<AziRappresentante> updateAziRappresentante(@Valid @RequestBody AziRappresentante aziRappresentante) throws URISyntaxException {
        log.debug("REST request to update AziRappresentante : {}", aziRappresentante);
        if (aziRappresentante.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziRappresentante result = aziRappresentanteService.save(aziRappresentante);
        AziRappresentante result2 = aziRappresentanteService.save(aziRappresentante);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziRappresentante.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-rappresentantes} : get all the aziRappresentantes.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziRappresentantes in body.
     */
    @GetMapping("/azi-rappresentantes")
    public ResponseEntity<List<AziRappresentante>> getAllAziRappresentantes(AziRappresentanteCriteria criteria) {
        log.debug("REST request to get AziRappresentantes by criteria: {}", criteria);
        List<AziRappresentante> entityList = aziRappresentanteQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-rappresentantes/count} : count all the aziRappresentantes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-rappresentantes/count")
    public ResponseEntity<Long> countAziRappresentantes(AziRappresentanteCriteria criteria) {
        log.debug("REST request to count AziRappresentantes by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziRappresentanteQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-rappresentantes/:id} : get the "id" aziRappresentante.
     *
     * @param id the id of the aziRappresentante to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziRappresentante, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-rappresentantes/{id}")
    public ResponseEntity<AziRappresentante> getAziRappresentante(@PathVariable Long id) {
        log.debug("REST request to get AziRappresentante : {}", id);
        Optional<AziRappresentante> aziRappresentante = aziRappresentanteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziRappresentante);
    }

    /**
     * {@code DELETE  /azi-rappresentantes/:id} : delete the "id" aziRappresentante.
     *
     * @param id the id of the aziRappresentante to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-rappresentantes/{id}")
    public ResponseEntity<Void> deleteAziRappresentante(@PathVariable Long id) {
        log.debug("REST request to delete AziRappresentante : {}", id);
        aziRappresentanteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
