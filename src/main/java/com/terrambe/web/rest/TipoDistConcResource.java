package com.terrambe.web.rest;

import com.terrambe.domain.TipoDistConc;
import com.terrambe.service.TipoDistConcService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.TipoDistConcCriteria;
import com.terrambe.service.TipoDistConcQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.TipoDistConc}.
 */
@RestController
@RequestMapping("/api")
public class TipoDistConcResource {

    private final Logger log = LoggerFactory.getLogger(TipoDistConcResource.class);

    private static final String ENTITY_NAME = "tipoDistConc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoDistConcService tipoDistConcService;

    private final TipoDistConcQueryService tipoDistConcQueryService;

    public TipoDistConcResource(TipoDistConcService tipoDistConcService, TipoDistConcQueryService tipoDistConcQueryService) {
        this.tipoDistConcService = tipoDistConcService;
        this.tipoDistConcQueryService = tipoDistConcQueryService;
    }

    /**
     * {@code POST  /tipo-dist-concs} : Create a new tipoDistConc.
     *
     * @param tipoDistConc the tipoDistConc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoDistConc, or with status {@code 400 (Bad Request)} if the tipoDistConc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-dist-concs")
    public ResponseEntity<TipoDistConc> createTipoDistConc(@RequestBody TipoDistConc tipoDistConc) throws URISyntaxException {
        log.debug("REST request to save TipoDistConc : {}", tipoDistConc);
        if (tipoDistConc.getId() != null) {
            throw new BadRequestAlertException("A new tipoDistConc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoDistConc result = tipoDistConcService.save(tipoDistConc);
        return ResponseEntity.created(new URI("/api/tipo-dist-concs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-dist-concs} : Updates an existing tipoDistConc.
     *
     * @param tipoDistConc the tipoDistConc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoDistConc,
     * or with status {@code 400 (Bad Request)} if the tipoDistConc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoDistConc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-dist-concs")
    public ResponseEntity<TipoDistConc> updateTipoDistConc(@RequestBody TipoDistConc tipoDistConc) throws URISyntaxException {
        log.debug("REST request to update TipoDistConc : {}", tipoDistConc);
        if (tipoDistConc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoDistConc result = tipoDistConcService.save(tipoDistConc);
        TipoDistConc result2 = tipoDistConcService.save(tipoDistConc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoDistConc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-dist-concs} : get all the tipoDistConcs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoDistConcs in body.
     */
    @GetMapping("/tipo-dist-concs")
    public ResponseEntity<List<TipoDistConc>> getAllTipoDistConcs(TipoDistConcCriteria criteria) {
        log.debug("REST request to get TipoDistConcs by criteria: {}", criteria);
        List<TipoDistConc> entityList = tipoDistConcQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /tipo-dist-concs/count} : count all the tipoDistConcs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipo-dist-concs/count")
    public ResponseEntity<Long> countTipoDistConcs(TipoDistConcCriteria criteria) {
        log.debug("REST request to count TipoDistConcs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoDistConcQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipo-dist-concs/:id} : get the "id" tipoDistConc.
     *
     * @param id the id of the tipoDistConc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoDistConc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-dist-concs/{id}")
    public ResponseEntity<TipoDistConc> getTipoDistConc(@PathVariable Long id) {
        log.debug("REST request to get TipoDistConc : {}", id);
        Optional<TipoDistConc> tipoDistConc = tipoDistConcService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoDistConc);
    }

    /**
     * {@code DELETE  /tipo-dist-concs/:id} : delete the "id" tipoDistConc.
     *
     * @param id the id of the tipoDistConc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-dist-concs/{id}")
    public ResponseEntity<Void> deleteTipoDistConc(@PathVariable Long id) {
        log.debug("REST request to delete TipoDistConc : {}", id);
        tipoDistConcService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
