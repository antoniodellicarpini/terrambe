package com.terrambe.web.rest;

import com.terrambe.domain.DizMacrousi;
import com.terrambe.service.DizMacrousiService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizMacrousiCriteria;
import com.terrambe.service.DizMacrousiQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizMacrousi}.
 */
@RestController
@RequestMapping("/api")
public class DizMacrousiResource {

    private final Logger log = LoggerFactory.getLogger(DizMacrousiResource.class);

    private static final String ENTITY_NAME = "dizMacrousi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizMacrousiService dizMacrousiService;

    private final DizMacrousiQueryService dizMacrousiQueryService;

    public DizMacrousiResource(DizMacrousiService dizMacrousiService, DizMacrousiQueryService dizMacrousiQueryService) {
        this.dizMacrousiService = dizMacrousiService;
        this.dizMacrousiQueryService = dizMacrousiQueryService;
    }

    /**
     * {@code POST  /diz-macrousis} : Create a new dizMacrousi.
     *
     * @param dizMacrousi the dizMacrousi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizMacrousi, or with status {@code 400 (Bad Request)} if the dizMacrousi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-macrousis")
    public ResponseEntity<DizMacrousi> createDizMacrousi(@RequestBody DizMacrousi dizMacrousi) throws URISyntaxException {
        log.debug("REST request to save DizMacrousi : {}", dizMacrousi);
        if (dizMacrousi.getId() != null) {
            throw new BadRequestAlertException("A new dizMacrousi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizMacrousi result = dizMacrousiService.save(dizMacrousi);
        return ResponseEntity.created(new URI("/api/diz-macrousis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-macrousis} : Updates an existing dizMacrousi.
     *
     * @param dizMacrousi the dizMacrousi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizMacrousi,
     * or with status {@code 400 (Bad Request)} if the dizMacrousi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizMacrousi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-macrousis")
    public ResponseEntity<DizMacrousi> updateDizMacrousi(@RequestBody DizMacrousi dizMacrousi) throws URISyntaxException {
        log.debug("REST request to update DizMacrousi : {}", dizMacrousi);
        if (dizMacrousi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizMacrousi result = dizMacrousiService.save(dizMacrousi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizMacrousi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-macrousis} : get all the dizMacrousis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizMacrousis in body.
     */
    @GetMapping("/diz-macrousis")
    public ResponseEntity<List<DizMacrousi>> getAllDizMacrousis(DizMacrousiCriteria criteria) {
        log.debug("REST request to get DizMacrousis by criteria: {}", criteria);
        List<DizMacrousi> entityList = dizMacrousiQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-macrousis/count} : count all the dizMacrousis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-macrousis/count")
    public ResponseEntity<Long> countDizMacrousis(DizMacrousiCriteria criteria) {
        log.debug("REST request to count DizMacrousis by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizMacrousiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-macrousis/:id} : get the "id" dizMacrousi.
     *
     * @param id the id of the dizMacrousi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizMacrousi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-macrousis/{id}")
    public ResponseEntity<DizMacrousi> getDizMacrousi(@PathVariable Long id) {
        log.debug("REST request to get DizMacrousi : {}", id);
        Optional<DizMacrousi> dizMacrousi = dizMacrousiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizMacrousi);
    }

    /**
     * {@code DELETE  /diz-macrousis/:id} : delete the "id" dizMacrousi.
     *
     * @param id the id of the dizMacrousi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-macrousis/{id}")
    public ResponseEntity<Void> deleteDizMacrousi(@PathVariable Long id) {
        log.debug("REST request to delete DizMacrousi : {}", id);
        dizMacrousiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
