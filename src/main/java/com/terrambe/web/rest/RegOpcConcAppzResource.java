package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcConcAppz;
import com.terrambe.service.RegOpcConcAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcConcAppzCriteria;
import com.terrambe.service.RegOpcConcAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcConcAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcConcAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcAppzResource.class);

    private static final String ENTITY_NAME = "regOpcConcAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcConcAppzService regOpcConcAppzService;

    private final RegOpcConcAppzQueryService regOpcConcAppzQueryService;

    public RegOpcConcAppzResource(RegOpcConcAppzService regOpcConcAppzService, RegOpcConcAppzQueryService regOpcConcAppzQueryService) {
        this.regOpcConcAppzService = regOpcConcAppzService;
        this.regOpcConcAppzQueryService = regOpcConcAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-conc-appzs} : Create a new regOpcConcAppz.
     *
     * @param regOpcConcAppz the regOpcConcAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcConcAppz, or with status {@code 400 (Bad Request)} if the regOpcConcAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-conc-appzs")
    public ResponseEntity<RegOpcConcAppz> createRegOpcConcAppz(@Valid @RequestBody RegOpcConcAppz regOpcConcAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcConcAppz : {}", regOpcConcAppz);
        if (regOpcConcAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcConcAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcConcAppz result = regOpcConcAppzService.save(regOpcConcAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-conc-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-conc-appzs} : Updates an existing regOpcConcAppz.
     *
     * @param regOpcConcAppz the regOpcConcAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcConcAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcConcAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcConcAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-conc-appzs")
    public ResponseEntity<RegOpcConcAppz> updateRegOpcConcAppz(@Valid @RequestBody RegOpcConcAppz regOpcConcAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcConcAppz : {}", regOpcConcAppz);
        if (regOpcConcAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcConcAppz result = regOpcConcAppzService.save(regOpcConcAppz);
        RegOpcConcAppz result2 = regOpcConcAppzService.save(regOpcConcAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcConcAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-conc-appzs} : get all the regOpcConcAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcConcAppzs in body.
     */
    @GetMapping("/reg-opc-conc-appzs")
    public ResponseEntity<List<RegOpcConcAppz>> getAllRegOpcConcAppzs(RegOpcConcAppzCriteria criteria) {
        log.debug("REST request to get RegOpcConcAppzs by criteria: {}", criteria);
        List<RegOpcConcAppz> entityList = regOpcConcAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-conc-appzs/count} : count all the regOpcConcAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-conc-appzs/count")
    public ResponseEntity<Long> countRegOpcConcAppzs(RegOpcConcAppzCriteria criteria) {
        log.debug("REST request to count RegOpcConcAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcConcAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-conc-appzs/:id} : get the "id" regOpcConcAppz.
     *
     * @param id the id of the regOpcConcAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcConcAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-conc-appzs/{id}")
    public ResponseEntity<RegOpcConcAppz> getRegOpcConcAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcConcAppz : {}", id);
        Optional<RegOpcConcAppz> regOpcConcAppz = regOpcConcAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcConcAppz);
    }

    /**
     * {@code DELETE  /reg-opc-conc-appzs/:id} : delete the "id" regOpcConcAppz.
     *
     * @param id the id of the regOpcConcAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-conc-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcConcAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcConcAppz : {}", id);
        regOpcConcAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
