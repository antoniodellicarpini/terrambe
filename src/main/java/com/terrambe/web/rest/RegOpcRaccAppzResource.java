package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcRaccAppz;
import com.terrambe.service.RegOpcRaccAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcRaccAppzCriteria;
import com.terrambe.service.RegOpcRaccAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcRaccAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcRaccAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcRaccAppzResource.class);

    private static final String ENTITY_NAME = "regOpcRaccAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcRaccAppzService regOpcRaccAppzService;

    private final RegOpcRaccAppzQueryService regOpcRaccAppzQueryService;

    public RegOpcRaccAppzResource(RegOpcRaccAppzService regOpcRaccAppzService, RegOpcRaccAppzQueryService regOpcRaccAppzQueryService) {
        this.regOpcRaccAppzService = regOpcRaccAppzService;
        this.regOpcRaccAppzQueryService = regOpcRaccAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-racc-appzs} : Create a new regOpcRaccAppz.
     *
     * @param regOpcRaccAppz the regOpcRaccAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcRaccAppz, or with status {@code 400 (Bad Request)} if the regOpcRaccAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-racc-appzs")
    public ResponseEntity<RegOpcRaccAppz> createRegOpcRaccAppz(@Valid @RequestBody RegOpcRaccAppz regOpcRaccAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcRaccAppz : {}", regOpcRaccAppz);
        if (regOpcRaccAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcRaccAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcRaccAppz result = regOpcRaccAppzService.save(regOpcRaccAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-racc-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-racc-appzs} : Updates an existing regOpcRaccAppz.
     *
     * @param regOpcRaccAppz the regOpcRaccAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcRaccAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcRaccAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcRaccAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-racc-appzs")
    public ResponseEntity<RegOpcRaccAppz> updateRegOpcRaccAppz(@Valid @RequestBody RegOpcRaccAppz regOpcRaccAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcRaccAppz : {}", regOpcRaccAppz);
        if (regOpcRaccAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcRaccAppz result = regOpcRaccAppzService.save(regOpcRaccAppz);
        RegOpcRaccAppz result2 = regOpcRaccAppzService.save(regOpcRaccAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcRaccAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-racc-appzs} : get all the regOpcRaccAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcRaccAppzs in body.
     */
    @GetMapping("/reg-opc-racc-appzs")
    public ResponseEntity<List<RegOpcRaccAppz>> getAllRegOpcRaccAppzs(RegOpcRaccAppzCriteria criteria) {
        log.debug("REST request to get RegOpcRaccAppzs by criteria: {}", criteria);
        List<RegOpcRaccAppz> entityList = regOpcRaccAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-racc-appzs/count} : count all the regOpcRaccAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-racc-appzs/count")
    public ResponseEntity<Long> countRegOpcRaccAppzs(RegOpcRaccAppzCriteria criteria) {
        log.debug("REST request to count RegOpcRaccAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcRaccAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-racc-appzs/:id} : get the "id" regOpcRaccAppz.
     *
     * @param id the id of the regOpcRaccAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcRaccAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-racc-appzs/{id}")
    public ResponseEntity<RegOpcRaccAppz> getRegOpcRaccAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcRaccAppz : {}", id);
        Optional<RegOpcRaccAppz> regOpcRaccAppz = regOpcRaccAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcRaccAppz);
    }

    /**
     * {@code DELETE  /reg-opc-racc-appzs/:id} : delete the "id" regOpcRaccAppz.
     *
     * @param id the id of the regOpcRaccAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-racc-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcRaccAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcRaccAppz : {}", id);
        regOpcRaccAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
