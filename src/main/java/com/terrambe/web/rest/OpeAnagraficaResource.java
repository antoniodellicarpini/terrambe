package com.terrambe.web.rest;

import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.domain.OpePatentino;
import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.service.OpeAnagraficaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.OpeAnagraficaCriteria;
import com.terrambe.service.OpeAnagraficaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link com.terrambe.domain.OpeAnagrafica}.
 */
@RestController
@RequestMapping("/api")
public class OpeAnagraficaResource {

    private final Logger log = LoggerFactory.getLogger(OpeAnagraficaResource.class);

    private static final String ENTITY_NAME = "opeAnagrafica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OpeAnagraficaService opeAnagraficaService;

    private final OpeAnagraficaQueryService opeAnagraficaQueryService;

    public OpeAnagraficaResource(OpeAnagraficaService opeAnagraficaService, OpeAnagraficaQueryService opeAnagraficaQueryService) {
        this.opeAnagraficaService = opeAnagraficaService;
        this.opeAnagraficaQueryService = opeAnagraficaQueryService;
    }

    /**
     * {@code POST  /ope-anagraficas} : Create a new opeAnagrafica.
     *
     * @param opeAnagrafica the opeAnagrafica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new opeAnagrafica, or with status {@code 400 (Bad Request)} if the opeAnagrafica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ope-anagraficas")
    public ResponseEntity<OpeAnagrafica> createOpeAnagrafica(@Valid @RequestBody OpeAnagrafica opeAnagrafica) throws URISyntaxException {
        
        OpePatentinoTipologia tipo = new OpePatentinoTipologia();
        tipo.setId(999L);
        Iterator<OpePatentino> itr = opeAnagrafica.getOpeAnagToPates().iterator();
        while(itr.hasNext()){
              itr.next().setOpepatentinotipologia(tipo);
        }

        log.debug("REST request to save OpeAnagrafica : {}", opeAnagrafica);
        if (opeAnagrafica.getId() != null) {
            throw new BadRequestAlertException("A new opeAnagrafica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpeAnagrafica result = opeAnagraficaService.save(opeAnagrafica);

        return ResponseEntity.created(new URI("/api/ope-anagraficas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ope-anagraficas} : Updates an existing opeAnagrafica.
     *
     * @param opeAnagrafica the opeAnagrafica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated opeAnagrafica,
     * or with status {@code 400 (Bad Request)} if the opeAnagrafica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the opeAnagrafica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ope-anagraficas")
    public ResponseEntity<OpeAnagrafica> updateOpeAnagrafica(@Valid @RequestBody OpeAnagrafica opeAnagrafica) throws URISyntaxException {
        log.debug("REST request to update OpeAnagrafica : {}", opeAnagrafica);
        if (opeAnagrafica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OpeAnagrafica result = opeAnagraficaService.save(opeAnagrafica);
        OpeAnagrafica result2 = opeAnagraficaService.save(opeAnagrafica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, opeAnagrafica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ope-anagraficas} : get all the opeAnagraficas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of opeAnagraficas in body.
     */
    @GetMapping("/ope-anagraficas")
    public ResponseEntity<List<OpeAnagrafica>> getAllOpeAnagraficas(OpeAnagraficaCriteria criteria) {
        log.debug("REST request to get OpeAnagraficas by criteria: {}", criteria);
        List<OpeAnagrafica> entityList = opeAnagraficaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ope-anagraficas/count} : count all the opeAnagraficas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ope-anagraficas/count")
    public ResponseEntity<Long> countOpeAnagraficas(OpeAnagraficaCriteria criteria) {
        log.debug("REST request to count OpeAnagraficas by criteria: {}", criteria);
        return ResponseEntity.ok().body(opeAnagraficaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ope-anagraficas/:id} : get the "id" opeAnagrafica.
     *
     * @param id the id of the opeAnagrafica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the opeAnagrafica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ope-anagraficas/{id}")
    public ResponseEntity<OpeAnagrafica> getOpeAnagrafica(@PathVariable Long id) {
        log.debug("REST request to get OpeAnagrafica : {}", id);
        Optional<OpeAnagrafica> opeAnagrafica = opeAnagraficaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(opeAnagrafica);
    }

    /**
     * {@code DELETE  /ope-anagraficas/:id} : delete the "id" opeAnagrafica.
     *
     * @param id the id of the opeAnagrafica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ope-anagraficas/{id}")
    public ResponseEntity<Void> deleteOpeAnagrafica(@PathVariable Long id) {
        log.debug("REST request to delete OpeAnagrafica : {}", id);
        opeAnagraficaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
