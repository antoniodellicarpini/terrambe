package com.terrambe.web.rest;

import com.terrambe.domain.*;
import com.terrambe.service.*;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcPrepterrCriteria;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.sql.DataSource;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcPrepterr}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcPrepterrResource {

    @Autowired
    private DataSource dataSource;
    
    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrResource.class);

    private static final String ENTITY_NAME = "regOpcPrepterr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcPrepterrService regOpcPrepterrService;

    private final RegOpcPrepterrQueryService regOpcPrepterrQueryService;

    @Autowired
    private Agea20152020MatriceService ageaMatServ;

    @Autowired
    private AtrAnagraficaService atrServ;

    @Autowired
    private OpeAnagraficaService opeAnServ;

    public RegOpcPrepterrResource(RegOpcPrepterrService regOpcPrepterrService, RegOpcPrepterrQueryService regOpcPrepterrQueryService) {
        this.regOpcPrepterrService = regOpcPrepterrService;
        this.regOpcPrepterrQueryService = regOpcPrepterrQueryService;
    }

    /**
     * {@code POST  /reg-opc-prepterrs} : Create a new regOpcPrepterr.
     *
     * @param regOpcPrepterr the regOpcPrepterr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcPrepterr, or with status {@code 400 (Bad Request)} if the regOpcPrepterr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-prepterrs")
    public ResponseEntity<RegOpcPrepterr> createRegOpcPrepterr(@Valid @RequestBody RegOpcPrepterr regOpcPrepterr) throws URISyntaxException {
        log.debug("REST request to save RegOpcPrepterr : {}", regOpcPrepterr);
        if (regOpcPrepterr.getId() != null) {
            throw new BadRequestAlertException("A new regOpcPrepterr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcPrepterr result = regOpcPrepterrService.save(regOpcPrepterr);
        return ResponseEntity.created(new URI("/api/reg-opc-prepterrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-prepterrs} : Updates an existing regOpcPrepterr.
     *
     * @param regOpcPrepterr the regOpcPrepterr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcPrepterr,
     * or with status {@code 400 (Bad Request)} if the regOpcPrepterr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcPrepterr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-prepterrs")
    public ResponseEntity<RegOpcPrepterr> updateRegOpcPrepterr(@Valid @RequestBody RegOpcPrepterr regOpcPrepterr) throws URISyntaxException {
        log.debug("REST request to update RegOpcPrepterr : {}", regOpcPrepterr);
        if (regOpcPrepterr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcPrepterr result = regOpcPrepterrService.save(regOpcPrepterr);
        RegOpcPrepterr result2 = regOpcPrepterrService.save(regOpcPrepterr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcPrepterr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-prepterrs} : get all the regOpcPrepterrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcPrepterrs in body.
     */
    @GetMapping("/reg-opc-prepterrs")
    public ResponseEntity<List<RegOpcPrepterr>> getAllRegOpcPrepterrs(RegOpcPrepterrCriteria criteria) {
        log.debug("REST request to get RegOpcPrepterrs by criteria: {}", criteria);
        List<RegOpcPrepterr> entityList = regOpcPrepterrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    @GetMapping("/reg-opc-prepterr-list")
    public ResponseEntity<List<ListRegOpcPrepterreno>> getListRegOpcPrepterreno(@RequestParam(value = "azienda_id.equals") Long azienda_id) throws Exception {
        UtilityJpaQueriesService utility = new UtilityJpaQueriesService();
        List<ListRegOpcPrepterreno> result_listregopc = utility.getListRegOpcPrepterreno(azienda_id, dataSource);
        return ResponseEntity.ok().body(result_listregopc);
    }


    // @GetMapping("/reg-opc-prepterrs-spec")
    // public ResponseEntity<List<RegOpcSpec>> getAllRegOpcPrepterrsSpec(RegOpcPrepterrCriteria criteria) {
    //     log.debug("REST request to get RegOpcPrepterrs by criteria: {}", criteria);
    //     List<RegOpcPrepterr> entityList = regOpcPrepterrQueryService.findByCriteria(criteria);

    //     List<RegOpcSpec> regOpcPrepTerr = new ArrayList<>();

    //     for (RegOpcPrepterr entity: entityList){

    //         RegOpcSpec spec = new RegOpcSpec();
    //         spec.setId(entity.getId());
    //         spec.setDataInizOpc(entity.getDataInizOpc());
    //         spec.setTipoTratt(entity.getOpcPrepterrToTipo().getDescrizione());
    //         //ottengo i dati dal dal db
    //         Optional<Agea20152020Matrice> ageMat = ageaMatServ.findOne(entity.getIdColtura());
    //         spec.setDescColtura(ageMat.get().getDescColtura());

    //         Optional<OpeAnagrafica> ope = opeAnServ.findOne(entity.getIdOperatore());
    //         spec.setNominativoOperatore(ope.get().getCognome() + " " + ope.get().getNome());

    //         Optional<AtrAnagrafica> atr = atrServ.findOne(entity.getIdMezzo());
    //         spec.setDescMezzo(atr.get().getModello());

    //         regOpcPrepTerr.add(spec);

    //     }
    //     return ResponseEntity.ok().body(regOpcPrepTerr);
    // }


    /**
    * {@code GET  /reg-opc-prepterrs/count} : count all the regOpcPrepterrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-prepterrs/count")
    public ResponseEntity<Long> countRegOpcPrepterrs(RegOpcPrepterrCriteria criteria) {
        log.debug("REST request to count RegOpcPrepterrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcPrepterrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-prepterrs/:id} : get the "id" regOpcPrepterr.
     *
     * @param id the id of the regOpcPrepterr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcPrepterr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-prepterrs/{id}")
    public ResponseEntity<RegOpcPrepterr> getRegOpcPrepterr(@PathVariable Long id) {
        log.debug("REST request to get RegOpcPrepterr : {}", id);
        Optional<RegOpcPrepterr> regOpcPrepterr = regOpcPrepterrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcPrepterr);
    }

    /**
     * {@code DELETE  /reg-opc-prepterrs/:id} : delete the "id" regOpcPrepterr.
     *
     * @param id the id of the regOpcPrepterr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-prepterrs/{id}")
    public ResponseEntity<Void> deleteRegOpcPrepterr(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcPrepterr : {}", id);
        regOpcPrepterrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
