package com.terrambe.web.rest;

import com.terrambe.domain.UniAnagrafica;
import com.terrambe.service.UniAnagraficaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.UniAnagraficaCriteria;
import com.terrambe.service.UniAnagraficaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.UniAnagrafica}.
 */
@RestController
@RequestMapping("/api")
public class UniAnagraficaResource {

    private final Logger log = LoggerFactory.getLogger(UniAnagraficaResource.class);

    private static final String ENTITY_NAME = "uniAnagrafica";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UniAnagraficaService uniAnagraficaService;

    private final UniAnagraficaQueryService uniAnagraficaQueryService;

    public UniAnagraficaResource(UniAnagraficaService uniAnagraficaService, UniAnagraficaQueryService uniAnagraficaQueryService) {
        this.uniAnagraficaService = uniAnagraficaService;
        this.uniAnagraficaQueryService = uniAnagraficaQueryService;
    }

    /**
     * {@code POST  /uni-anagraficas} : Create a new uniAnagrafica.
     *
     * @param uniAnagrafica the uniAnagrafica to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uniAnagrafica, or with status {@code 400 (Bad Request)} if the uniAnagrafica has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/uni-anagraficas")
    public ResponseEntity<UniAnagrafica> createUniAnagrafica(@Valid @RequestBody UniAnagrafica uniAnagrafica) throws URISyntaxException {
        log.debug("REST request to save UniAnagrafica : {}", uniAnagrafica);
        if (uniAnagrafica.getId() != null) {
            throw new BadRequestAlertException("A new uniAnagrafica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UniAnagrafica result = uniAnagraficaService.save(uniAnagrafica);
        return ResponseEntity.created(new URI("/api/uni-anagraficas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /uni-anagraficas} : Updates an existing uniAnagrafica.
     *
     * @param uniAnagrafica the uniAnagrafica to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uniAnagrafica,
     * or with status {@code 400 (Bad Request)} if the uniAnagrafica is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uniAnagrafica couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/uni-anagraficas")
    public ResponseEntity<UniAnagrafica> updateUniAnagrafica(@Valid @RequestBody UniAnagrafica uniAnagrafica) throws URISyntaxException {
        log.debug("REST request to update UniAnagrafica : {}", uniAnagrafica);
        if (uniAnagrafica.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UniAnagrafica result = uniAnagraficaService.save(uniAnagrafica);
        UniAnagrafica result2 = uniAnagraficaService.save(uniAnagrafica);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, uniAnagrafica.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /uni-anagraficas} : get all the uniAnagraficas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uniAnagraficas in body.
     */
    @GetMapping("/uni-anagraficas")
    public ResponseEntity<List<UniAnagrafica>> getAllUniAnagraficas(UniAnagraficaCriteria criteria) {
        log.debug("REST request to get UniAnagraficas by criteria: {}", criteria);
        List<UniAnagrafica> entityList = uniAnagraficaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /uni-anagraficas/count} : count all the uniAnagraficas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/uni-anagraficas/count")
    public ResponseEntity<Long> countUniAnagraficas(UniAnagraficaCriteria criteria) {
        log.debug("REST request to count UniAnagraficas by criteria: {}", criteria);
        return ResponseEntity.ok().body(uniAnagraficaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /uni-anagraficas/:id} : get the "id" uniAnagrafica.
     *
     * @param id the id of the uniAnagrafica to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uniAnagrafica, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/uni-anagraficas/{id}")
    public ResponseEntity<UniAnagrafica> getUniAnagrafica(@PathVariable Long id) {
        log.debug("REST request to get UniAnagrafica : {}", id);
        Optional<UniAnagrafica> uniAnagrafica = uniAnagraficaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uniAnagrafica);
    }

    /**
     * {@code DELETE  /uni-anagraficas/:id} : delete the "id" uniAnagrafica.
     *
     * @param id the id of the uniAnagrafica to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/uni-anagraficas/{id}")
    public ResponseEntity<Void> deleteUniAnagrafica(@PathVariable Long id) {
        log.debug("REST request to delete UniAnagrafica : {}", id);
        uniAnagraficaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
