package com.terrambe.web.rest;

import com.terrambe.domain.MotivoIrr;
import com.terrambe.service.MotivoIrrService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.MotivoIrrCriteria;
import com.terrambe.service.MotivoIrrQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.MotivoIrr}.
 */
@RestController
@RequestMapping("/api")
public class MotivoIrrResource {

    private final Logger log = LoggerFactory.getLogger(MotivoIrrResource.class);

    private static final String ENTITY_NAME = "motivoIrr";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MotivoIrrService motivoIrrService;

    private final MotivoIrrQueryService motivoIrrQueryService;

    public MotivoIrrResource(MotivoIrrService motivoIrrService, MotivoIrrQueryService motivoIrrQueryService) {
        this.motivoIrrService = motivoIrrService;
        this.motivoIrrQueryService = motivoIrrQueryService;
    }

    /**
     * {@code POST  /motivo-irrs} : Create a new motivoIrr.
     *
     * @param motivoIrr the motivoIrr to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new motivoIrr, or with status {@code 400 (Bad Request)} if the motivoIrr has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/motivo-irrs")
    public ResponseEntity<MotivoIrr> createMotivoIrr(@RequestBody MotivoIrr motivoIrr) throws URISyntaxException {
        log.debug("REST request to save MotivoIrr : {}", motivoIrr);
        if (motivoIrr.getId() != null) {
            throw new BadRequestAlertException("A new motivoIrr cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MotivoIrr result = motivoIrrService.save(motivoIrr);
        return ResponseEntity.created(new URI("/api/motivo-irrs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /motivo-irrs} : Updates an existing motivoIrr.
     *
     * @param motivoIrr the motivoIrr to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated motivoIrr,
     * or with status {@code 400 (Bad Request)} if the motivoIrr is not valid,
     * or with status {@code 500 (Internal Server Error)} if the motivoIrr couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/motivo-irrs")
    public ResponseEntity<MotivoIrr> updateMotivoIrr(@RequestBody MotivoIrr motivoIrr) throws URISyntaxException {
        log.debug("REST request to update MotivoIrr : {}", motivoIrr);
        if (motivoIrr.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MotivoIrr result = motivoIrrService.save(motivoIrr);
        MotivoIrr result2 = motivoIrrService.save(motivoIrr);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, motivoIrr.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /motivo-irrs} : get all the motivoIrrs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of motivoIrrs in body.
     */
    @GetMapping("/motivo-irrs")
    public ResponseEntity<List<MotivoIrr>> getAllMotivoIrrs(MotivoIrrCriteria criteria) {
        log.debug("REST request to get MotivoIrrs by criteria: {}", criteria);
        List<MotivoIrr> entityList = motivoIrrQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /motivo-irrs/count} : count all the motivoIrrs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/motivo-irrs/count")
    public ResponseEntity<Long> countMotivoIrrs(MotivoIrrCriteria criteria) {
        log.debug("REST request to count MotivoIrrs by criteria: {}", criteria);
        return ResponseEntity.ok().body(motivoIrrQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /motivo-irrs/:id} : get the "id" motivoIrr.
     *
     * @param id the id of the motivoIrr to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the motivoIrr, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/motivo-irrs/{id}")
    public ResponseEntity<MotivoIrr> getMotivoIrr(@PathVariable Long id) {
        log.debug("REST request to get MotivoIrr : {}", id);
        Optional<MotivoIrr> motivoIrr = motivoIrrService.findOne(id);
        return ResponseUtil.wrapOrNotFound(motivoIrr);
    }

    /**
     * {@code DELETE  /motivo-irrs/:id} : delete the "id" motivoIrr.
     *
     * @param id the id of the motivoIrr to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/motivo-irrs/{id}")
    public ResponseEntity<Void> deleteMotivoIrr(@PathVariable Long id) {
        log.debug("REST request to delete MotivoIrr : {}", id);
        motivoIrrService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
