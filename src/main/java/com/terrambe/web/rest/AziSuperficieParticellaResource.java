package com.terrambe.web.rest;

import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.service.AziSuperficieParticellaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziSuperficieParticellaCriteria;
import com.terrambe.service.AziSuperficieParticellaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziSuperficieParticella}.
 */
@RestController
@RequestMapping("/api")
public class AziSuperficieParticellaResource {

    private final Logger log = LoggerFactory.getLogger(AziSuperficieParticellaResource.class);

    private static final String ENTITY_NAME = "aziSuperficieParticella";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziSuperficieParticellaService aziSuperficieParticellaService;

    private final AziSuperficieParticellaQueryService aziSuperficieParticellaQueryService;

    public AziSuperficieParticellaResource(AziSuperficieParticellaService aziSuperficieParticellaService, AziSuperficieParticellaQueryService aziSuperficieParticellaQueryService) {
        this.aziSuperficieParticellaService = aziSuperficieParticellaService;
        this.aziSuperficieParticellaQueryService = aziSuperficieParticellaQueryService;
    }

    /**
     * {@code POST  /azi-superficie-particellas} : Create a new aziSuperficieParticella.
     *
     * @param aziSuperficieParticella the aziSuperficieParticella to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziSuperficieParticella, or with status {@code 400 (Bad Request)} if the aziSuperficieParticella has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-superficie-particellas")
    public ResponseEntity<AziSuperficieParticella> createAziSuperficieParticella(@Valid @RequestBody AziSuperficieParticella aziSuperficieParticella) throws URISyntaxException {
        log.debug("REST request to save AziSuperficieParticella : {}", aziSuperficieParticella);
        if (aziSuperficieParticella.getId() != null) {
            throw new BadRequestAlertException("A new aziSuperficieParticella cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziSuperficieParticella result = aziSuperficieParticellaService.save(aziSuperficieParticella);
        return ResponseEntity.created(new URI("/api/azi-superficie-particellas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-superficie-particellas} : Updates an existing aziSuperficieParticella.
     *
     * @param aziSuperficieParticella the aziSuperficieParticella to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziSuperficieParticella,
     * or with status {@code 400 (Bad Request)} if the aziSuperficieParticella is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziSuperficieParticella couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-superficie-particellas")
    public ResponseEntity<AziSuperficieParticella> updateAziSuperficieParticella(@Valid @RequestBody AziSuperficieParticella aziSuperficieParticella) throws URISyntaxException {
        log.debug("REST request to update AziSuperficieParticella : {}", aziSuperficieParticella);
        if (aziSuperficieParticella.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziSuperficieParticella result = aziSuperficieParticellaService.save(aziSuperficieParticella);
        AziSuperficieParticella result2 = aziSuperficieParticellaService.save(aziSuperficieParticella);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziSuperficieParticella.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-superficie-particellas} : get all the aziSuperficieParticellas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziSuperficieParticellas in body.
     */
    @GetMapping("/azi-superficie-particellas")
    public ResponseEntity<List<AziSuperficieParticella>> getAllAziSuperficieParticellas(AziSuperficieParticellaCriteria criteria) {
        log.debug("REST request to get AziSuperficieParticellas by criteria: {}", criteria);
        List<AziSuperficieParticella> entityList = aziSuperficieParticellaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-superficie-particellas/count} : count all the aziSuperficieParticellas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-superficie-particellas/count")
    public ResponseEntity<Long> countAziSuperficieParticellas(AziSuperficieParticellaCriteria criteria) {
        log.debug("REST request to count AziSuperficieParticellas by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziSuperficieParticellaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-superficie-particellas/:id} : get the "id" aziSuperficieParticella.
     *
     * @param id the id of the aziSuperficieParticella to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziSuperficieParticella, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-superficie-particellas/{id}")
    public ResponseEntity<AziSuperficieParticella> getAziSuperficieParticella(@PathVariable Long id) {
        log.debug("REST request to get AziSuperficieParticella : {}", id);
        Optional<AziSuperficieParticella> aziSuperficieParticella = aziSuperficieParticellaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziSuperficieParticella);
    }

    /**
     * {@code DELETE  /azi-superficie-particellas/:id} : delete the "id" aziSuperficieParticella.
     *
     * @param id the id of the aziSuperficieParticella to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-superficie-particellas/{id}")
    public ResponseEntity<Void> deleteAziSuperficieParticella(@PathVariable Long id) {
        log.debug("REST request to delete AziSuperficieParticella : {}", id);
                Optional<AziSuperficieParticella> aziSuperficieParticella = aziSuperficieParticellaService.findOne(id);

        if (aziSuperficieParticella.isPresent()){

           if ((aziSuperficieParticella.get().getAziSupToCampi().getId() != 0) && aziSuperficieParticella.get().getAziSupToCampi().getId() != null){

               throw new BadRequestAlertException("La particella che si sta cercando di eliminare ha un campo associato, non è possibile completare l'operazione ", ENTITY_NAME, "Campo presente eliminazione negata");
           }

        }else{

            throw new BadRequestAlertException("La particella che si sta cercando di eliminare non esiste", ENTITY_NAME, "idnull");
        }
        aziSuperficieParticellaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
