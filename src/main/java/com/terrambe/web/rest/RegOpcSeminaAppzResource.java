package com.terrambe.web.rest;

import com.terrambe.domain.RegOpcSeminaAppz;
import com.terrambe.service.RegOpcSeminaAppzService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RegOpcSeminaAppzCriteria;
import com.terrambe.service.RegOpcSeminaAppzQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RegOpcSeminaAppz}.
 */
@RestController
@RequestMapping("/api")
public class RegOpcSeminaAppzResource {

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaAppzResource.class);

    private static final String ENTITY_NAME = "regOpcSeminaAppz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegOpcSeminaAppzService regOpcSeminaAppzService;

    private final RegOpcSeminaAppzQueryService regOpcSeminaAppzQueryService;

    public RegOpcSeminaAppzResource(RegOpcSeminaAppzService regOpcSeminaAppzService, RegOpcSeminaAppzQueryService regOpcSeminaAppzQueryService) {
        this.regOpcSeminaAppzService = regOpcSeminaAppzService;
        this.regOpcSeminaAppzQueryService = regOpcSeminaAppzQueryService;
    }

    /**
     * {@code POST  /reg-opc-semina-appzs} : Create a new regOpcSeminaAppz.
     *
     * @param regOpcSeminaAppz the regOpcSeminaAppz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new regOpcSeminaAppz, or with status {@code 400 (Bad Request)} if the regOpcSeminaAppz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reg-opc-semina-appzs")
    public ResponseEntity<RegOpcSeminaAppz> createRegOpcSeminaAppz(@Valid @RequestBody RegOpcSeminaAppz regOpcSeminaAppz) throws URISyntaxException {
        log.debug("REST request to save RegOpcSeminaAppz : {}", regOpcSeminaAppz);
        if (regOpcSeminaAppz.getId() != null) {
            throw new BadRequestAlertException("A new regOpcSeminaAppz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegOpcSeminaAppz result = regOpcSeminaAppzService.save(regOpcSeminaAppz);
        return ResponseEntity.created(new URI("/api/reg-opc-semina-appzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reg-opc-semina-appzs} : Updates an existing regOpcSeminaAppz.
     *
     * @param regOpcSeminaAppz the regOpcSeminaAppz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated regOpcSeminaAppz,
     * or with status {@code 400 (Bad Request)} if the regOpcSeminaAppz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the regOpcSeminaAppz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reg-opc-semina-appzs")
    public ResponseEntity<RegOpcSeminaAppz> updateRegOpcSeminaAppz(@Valid @RequestBody RegOpcSeminaAppz regOpcSeminaAppz) throws URISyntaxException {
        log.debug("REST request to update RegOpcSeminaAppz : {}", regOpcSeminaAppz);
        if (regOpcSeminaAppz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegOpcSeminaAppz result = regOpcSeminaAppzService.save(regOpcSeminaAppz);
        RegOpcSeminaAppz result2 = regOpcSeminaAppzService.save(regOpcSeminaAppz);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, regOpcSeminaAppz.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reg-opc-semina-appzs} : get all the regOpcSeminaAppzs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of regOpcSeminaAppzs in body.
     */
    @GetMapping("/reg-opc-semina-appzs")
    public ResponseEntity<List<RegOpcSeminaAppz>> getAllRegOpcSeminaAppzs(RegOpcSeminaAppzCriteria criteria) {
        log.debug("REST request to get RegOpcSeminaAppzs by criteria: {}", criteria);
        List<RegOpcSeminaAppz> entityList = regOpcSeminaAppzQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /reg-opc-semina-appzs/count} : count all the regOpcSeminaAppzs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reg-opc-semina-appzs/count")
    public ResponseEntity<Long> countRegOpcSeminaAppzs(RegOpcSeminaAppzCriteria criteria) {
        log.debug("REST request to count RegOpcSeminaAppzs by criteria: {}", criteria);
        return ResponseEntity.ok().body(regOpcSeminaAppzQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reg-opc-semina-appzs/:id} : get the "id" regOpcSeminaAppz.
     *
     * @param id the id of the regOpcSeminaAppz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the regOpcSeminaAppz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reg-opc-semina-appzs/{id}")
    public ResponseEntity<RegOpcSeminaAppz> getRegOpcSeminaAppz(@PathVariable Long id) {
        log.debug("REST request to get RegOpcSeminaAppz : {}", id);
        Optional<RegOpcSeminaAppz> regOpcSeminaAppz = regOpcSeminaAppzService.findOne(id);
        return ResponseUtil.wrapOrNotFound(regOpcSeminaAppz);
    }

    /**
     * {@code DELETE  /reg-opc-semina-appzs/:id} : delete the "id" regOpcSeminaAppz.
     *
     * @param id the id of the regOpcSeminaAppz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reg-opc-semina-appzs/{id}")
    public ResponseEntity<Void> deleteRegOpcSeminaAppz(@PathVariable Long id) {
        log.debug("REST request to delete RegOpcSeminaAppz : {}", id);
        regOpcSeminaAppzService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
