package com.terrambe.web.rest;

import com.terrambe.domain.DizColture;
import com.terrambe.service.DizColtureService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.DizColtureCriteria;
import com.terrambe.service.DizColtureQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.DizColture}.
 */
@RestController
@RequestMapping("/api")
public class DizColtureResource {

    private final Logger log = LoggerFactory.getLogger(DizColtureResource.class);

    private static final String ENTITY_NAME = "dizColture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DizColtureService dizColtureService;

    private final DizColtureQueryService dizColtureQueryService;

    public DizColtureResource(DizColtureService dizColtureService, DizColtureQueryService dizColtureQueryService) {
        this.dizColtureService = dizColtureService;
        this.dizColtureQueryService = dizColtureQueryService;
    }

    /**
     * {@code POST  /diz-coltures} : Create a new dizColture.
     *
     * @param dizColture the dizColture to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dizColture, or with status {@code 400 (Bad Request)} if the dizColture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diz-coltures")
    public ResponseEntity<DizColture> createDizColture(@RequestBody DizColture dizColture) throws URISyntaxException {
        log.debug("REST request to save DizColture : {}", dizColture);
        if (dizColture.getId() != null) {
            throw new BadRequestAlertException("A new dizColture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DizColture result = dizColtureService.save(dizColture);
        return ResponseEntity.created(new URI("/api/diz-coltures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diz-coltures} : Updates an existing dizColture.
     *
     * @param dizColture the dizColture to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dizColture,
     * or with status {@code 400 (Bad Request)} if the dizColture is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dizColture couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diz-coltures")
    public ResponseEntity<DizColture> updateDizColture(@RequestBody DizColture dizColture) throws URISyntaxException {
        log.debug("REST request to update DizColture : {}", dizColture);
        if (dizColture.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DizColture result = dizColtureService.save(dizColture);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dizColture.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diz-coltures} : get all the dizColtures.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dizColtures in body.
     */
    @GetMapping("/diz-coltures")
    public ResponseEntity<List<DizColture>> getAllDizColtures(DizColtureCriteria criteria) {
        log.debug("REST request to get DizColtures by criteria: {}", criteria);
        List<DizColture> entityList = dizColtureQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /diz-coltures/count} : count all the dizColtures.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/diz-coltures/count")
    public ResponseEntity<Long> countDizColtures(DizColtureCriteria criteria) {
        log.debug("REST request to count DizColtures by criteria: {}", criteria);
        return ResponseEntity.ok().body(dizColtureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /diz-coltures/:id} : get the "id" dizColture.
     *
     * @param id the id of the dizColture to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dizColture, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diz-coltures/{id}")
    public ResponseEntity<DizColture> getDizColture(@PathVariable Long id) {
        log.debug("REST request to get DizColture : {}", id);
        Optional<DizColture> dizColture = dizColtureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dizColture);
    }

    /**
     * {@code DELETE  /diz-coltures/:id} : delete the "id" dizColture.
     *
     * @param id the id of the dizColture to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diz-coltures/{id}")
    public ResponseEntity<Void> deleteDizColture(@PathVariable Long id) {
        log.debug("REST request to delete DizColture : {}", id);
        dizColtureService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
