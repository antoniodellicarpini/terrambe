package com.terrambe.web.rest;

import com.terrambe.domain.RecOpeAna;
import com.terrambe.service.RecOpeAnaService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.RecOpeAnaCriteria;
import com.terrambe.service.RecOpeAnaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.RecOpeAna}.
 */
@RestController
@RequestMapping("/api")
public class RecOpeAnaResource {

    private final Logger log = LoggerFactory.getLogger(RecOpeAnaResource.class);

    private static final String ENTITY_NAME = "recOpeAna";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecOpeAnaService recOpeAnaService;

    private final RecOpeAnaQueryService recOpeAnaQueryService;

    public RecOpeAnaResource(RecOpeAnaService recOpeAnaService, RecOpeAnaQueryService recOpeAnaQueryService) {
        this.recOpeAnaService = recOpeAnaService;
        this.recOpeAnaQueryService = recOpeAnaQueryService;
    }

    /**
     * {@code POST  /rec-ope-anas} : Create a new recOpeAna.
     *
     * @param recOpeAna the recOpeAna to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recOpeAna, or with status {@code 400 (Bad Request)} if the recOpeAna has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rec-ope-anas")
    public ResponseEntity<RecOpeAna> createRecOpeAna(@RequestBody RecOpeAna recOpeAna) throws URISyntaxException {
        log.debug("REST request to save RecOpeAna : {}", recOpeAna);
        if (recOpeAna.getId() != null) {
            throw new BadRequestAlertException("A new recOpeAna cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecOpeAna result = recOpeAnaService.save(recOpeAna);
        return ResponseEntity.created(new URI("/api/rec-ope-anas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rec-ope-anas} : Updates an existing recOpeAna.
     *
     * @param recOpeAna the recOpeAna to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recOpeAna,
     * or with status {@code 400 (Bad Request)} if the recOpeAna is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recOpeAna couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rec-ope-anas")
    public ResponseEntity<RecOpeAna> updateRecOpeAna(@RequestBody RecOpeAna recOpeAna) throws URISyntaxException {
        log.debug("REST request to update RecOpeAna : {}", recOpeAna);
        if (recOpeAna.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecOpeAna result = recOpeAnaService.save(recOpeAna);
        RecOpeAna result2 = recOpeAnaService.save(recOpeAna);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, recOpeAna.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rec-ope-anas} : get all the recOpeAnas.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recOpeAnas in body.
     */
    @GetMapping("/rec-ope-anas")
    public ResponseEntity<List<RecOpeAna>> getAllRecOpeAnas(RecOpeAnaCriteria criteria) {
        log.debug("REST request to get RecOpeAnas by criteria: {}", criteria);
        List<RecOpeAna> entityList = recOpeAnaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /rec-ope-anas/count} : count all the recOpeAnas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/rec-ope-anas/count")
    public ResponseEntity<Long> countRecOpeAnas(RecOpeAnaCriteria criteria) {
        log.debug("REST request to count RecOpeAnas by criteria: {}", criteria);
        return ResponseEntity.ok().body(recOpeAnaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /rec-ope-anas/:id} : get the "id" recOpeAna.
     *
     * @param id the id of the recOpeAna to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recOpeAna, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rec-ope-anas/{id}")
    public ResponseEntity<RecOpeAna> getRecOpeAna(@PathVariable Long id) {
        log.debug("REST request to get RecOpeAna : {}", id);
        Optional<RecOpeAna> recOpeAna = recOpeAnaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recOpeAna);
    }

    /**
     * {@code DELETE  /rec-ope-anas/:id} : delete the "id" recOpeAna.
     *
     * @param id the id of the recOpeAna to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rec-ope-anas/{id}")
    public ResponseEntity<Void> deleteRecOpeAna(@PathVariable Long id) {
        log.debug("REST request to delete RecOpeAna : {}", id);
        recOpeAnaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
