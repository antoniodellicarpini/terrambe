package com.terrambe.web.rest;

import com.terrambe.domain.IndNascRapCli;
import com.terrambe.service.IndNascRapCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.IndNascRapCliCriteria;
import com.terrambe.service.IndNascRapCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.terrambe.domain.IndNascRapCli}.
 */
@RestController
@RequestMapping("/api")
public class IndNascRapCliResource {

    private final Logger log = LoggerFactory.getLogger(IndNascRapCliResource.class);

    private static final String ENTITY_NAME = "indNascRapCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndNascRapCliService indNascRapCliService;

    private final IndNascRapCliQueryService indNascRapCliQueryService;

    public IndNascRapCliResource(IndNascRapCliService indNascRapCliService, IndNascRapCliQueryService indNascRapCliQueryService) {
        this.indNascRapCliService = indNascRapCliService;
        this.indNascRapCliQueryService = indNascRapCliQueryService;
    }

    /**
     * {@code POST  /ind-nasc-rap-clis} : Create a new indNascRapCli.
     *
     * @param indNascRapCli the indNascRapCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indNascRapCli, or with status {@code 400 (Bad Request)} if the indNascRapCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ind-nasc-rap-clis")
    public ResponseEntity<IndNascRapCli> createIndNascRapCli(@Valid @RequestBody IndNascRapCli indNascRapCli) throws URISyntaxException {
        log.debug("REST request to save IndNascRapCli : {}", indNascRapCli);
        if (indNascRapCli.getId() != null) {
            throw new BadRequestAlertException("A new indNascRapCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndNascRapCli result = indNascRapCliService.save(indNascRapCli);
        return ResponseEntity.created(new URI("/api/ind-nasc-rap-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ind-nasc-rap-clis} : Updates an existing indNascRapCli.
     *
     * @param indNascRapCli the indNascRapCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indNascRapCli,
     * or with status {@code 400 (Bad Request)} if the indNascRapCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indNascRapCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ind-nasc-rap-clis")
    public ResponseEntity<IndNascRapCli> updateIndNascRapCli(@Valid @RequestBody IndNascRapCli indNascRapCli) throws URISyntaxException {
        log.debug("REST request to update IndNascRapCli : {}", indNascRapCli);
        if (indNascRapCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndNascRapCli result = indNascRapCliService.save(indNascRapCli);
        IndNascRapCli result2 = indNascRapCliService.save(indNascRapCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, indNascRapCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ind-nasc-rap-clis} : get all the indNascRapClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indNascRapClis in body.
     */
    @GetMapping("/ind-nasc-rap-clis")
    public ResponseEntity<List<IndNascRapCli>> getAllIndNascRapClis(IndNascRapCliCriteria criteria) {
        log.debug("REST request to get IndNascRapClis by criteria: {}", criteria);
        List<IndNascRapCli> entityList = indNascRapCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /ind-nasc-rap-clis/count} : count all the indNascRapClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ind-nasc-rap-clis/count")
    public ResponseEntity<Long> countIndNascRapClis(IndNascRapCliCriteria criteria) {
        log.debug("REST request to count IndNascRapClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(indNascRapCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ind-nasc-rap-clis/:id} : get the "id" indNascRapCli.
     *
     * @param id the id of the indNascRapCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indNascRapCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ind-nasc-rap-clis/{id}")
    public ResponseEntity<IndNascRapCli> getIndNascRapCli(@PathVariable Long id) {
        log.debug("REST request to get IndNascRapCli : {}", id);
        Optional<IndNascRapCli> indNascRapCli = indNascRapCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indNascRapCli);
    }

    /**
     * {@code DELETE  /ind-nasc-rap-clis/:id} : delete the "id" indNascRapCli.
     *
     * @param id the id of the indNascRapCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ind-nasc-rap-clis/{id}")
    public ResponseEntity<Void> deleteIndNascRapCli(@PathVariable Long id) {
        log.debug("REST request to delete IndNascRapCli : {}", id);
        indNascRapCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
