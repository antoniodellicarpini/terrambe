package com.terrambe.web.rest;

import com.terrambe.domain.PartAppez;
import com.terrambe.service.PartAppezService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.PartAppezCriteria;
import com.terrambe.service.PartAppezQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.PartAppez}.
 */
@RestController
@RequestMapping("/api")
public class PartAppezResource {

    private final Logger log = LoggerFactory.getLogger(PartAppezResource.class);

    private static final String ENTITY_NAME = "partAppez";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PartAppezService partAppezService;

    private final PartAppezQueryService partAppezQueryService;

    public PartAppezResource(PartAppezService partAppezService, PartAppezQueryService partAppezQueryService) {
        this.partAppezService = partAppezService;
        this.partAppezQueryService = partAppezQueryService;
    }

    /**
     * {@code POST  /part-appezs} : Create a new partAppez.
     *
     * @param partAppez the partAppez to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new partAppez, or with status {@code 400 (Bad Request)} if the partAppez has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/part-appezs")
    public ResponseEntity<PartAppez> createPartAppez(@Valid @RequestBody PartAppez partAppez) throws URISyntaxException {
        log.debug("REST request to save PartAppez : {}", partAppez);
        if (partAppez.getId() != null) {
            throw new BadRequestAlertException("A new partAppez cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartAppez result = partAppezService.save(partAppez);
        return ResponseEntity.created(new URI("/api/part-appezs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /part-appezs} : Updates an existing partAppez.
     *
     * @param partAppez the partAppez to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partAppez,
     * or with status {@code 400 (Bad Request)} if the partAppez is not valid,
     * or with status {@code 500 (Internal Server Error)} if the partAppez couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/part-appezs")
    public ResponseEntity<PartAppez> updatePartAppez(@Valid @RequestBody PartAppez partAppez) throws URISyntaxException {
        log.debug("REST request to update PartAppez : {}", partAppez);
        if (partAppez.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PartAppez result = partAppezService.save(partAppez);
        PartAppez result2 = partAppezService.save(partAppez);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, partAppez.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /part-appezs} : get all the partAppezs.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of partAppezs in body.
     */
    @GetMapping("/part-appezs")
    public ResponseEntity<List<PartAppez>> getAllPartAppezs(PartAppezCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PartAppezs by criteria: {}", criteria);
        Page<PartAppez> page = partAppezQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /part-appezs/count} : count all the partAppezs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/part-appezs/count")
    public ResponseEntity<Long> countPartAppezs(PartAppezCriteria criteria) {
        log.debug("REST request to count PartAppezs by criteria: {}", criteria);
        return ResponseEntity.ok().body(partAppezQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /part-appezs/:id} : get the "id" partAppez.
     *
     * @param id the id of the partAppez to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the partAppez, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/part-appezs/{id}")
    public ResponseEntity<PartAppez> getPartAppez(@PathVariable Long id) {
        log.debug("REST request to get PartAppez : {}", id);
        Optional<PartAppez> partAppez = partAppezService.findOne(id);
        return ResponseUtil.wrapOrNotFound(partAppez);
    }

    /**
     * {@code DELETE  /part-appezs/:id} : delete the "id" partAppez.
     *
     * @param id the id of the partAppez to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/part-appezs/{id}")
    public ResponseEntity<Void> deletePartAppez(@PathVariable Long id) {
        log.debug("REST request to delete PartAppez : {}", id);
        partAppezService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
