package com.terrambe.web.rest;

import com.terrambe.domain.Fornitori;
import com.terrambe.service.FornitoriService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.FornitoriCriteria;
import com.terrambe.service.FornitoriQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.Fornitori}.
 */
@RestController
@RequestMapping("/api")
public class FornitoriResource {

    private final Logger log = LoggerFactory.getLogger(FornitoriResource.class);

    private static final String ENTITY_NAME = "fornitori";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FornitoriService fornitoriService;

    private final FornitoriQueryService fornitoriQueryService;

    public FornitoriResource(FornitoriService fornitoriService, FornitoriQueryService fornitoriQueryService) {
        this.fornitoriService = fornitoriService;
        this.fornitoriQueryService = fornitoriQueryService;
    }

    /**
     * {@code POST  /fornitoris} : Create a new fornitori.
     *
     * @param fornitori the fornitori to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fornitori, or with status {@code 400 (Bad Request)} if the fornitori has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fornitoris")
    public ResponseEntity<Fornitori> createFornitori(@Valid @RequestBody Fornitori fornitori) throws URISyntaxException {
        log.debug("REST request to save Fornitori : {}", fornitori);
        if (fornitori.getId() != null) {
            throw new BadRequestAlertException("A new fornitori cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fornitori result = fornitoriService.save(fornitori);
        return ResponseEntity.created(new URI("/api/fornitoris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fornitoris} : Updates an existing fornitori.
     *
     * @param fornitori the fornitori to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fornitori,
     * or with status {@code 400 (Bad Request)} if the fornitori is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fornitori couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fornitoris")
    public ResponseEntity<Fornitori> updateFornitori(@Valid @RequestBody Fornitori fornitori) throws URISyntaxException {
        log.debug("REST request to update Fornitori : {}", fornitori);
        if (fornitori.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fornitori result = fornitoriService.save(fornitori);
        Fornitori result2 = fornitoriService.save(fornitori);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fornitori.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fornitoris} : get all the fornitoris.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fornitoris in body.
     */
    @GetMapping("/fornitoris")
    public ResponseEntity<List<Fornitori>> getAllFornitoris(FornitoriCriteria criteria) {
        log.debug("REST request to get Fornitoris by criteria: {}", criteria);
        List<Fornitori> entityList = fornitoriQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /fornitoris/count} : count all the fornitoris.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fornitoris/count")
    public ResponseEntity<Long> countFornitoris(FornitoriCriteria criteria) {
        log.debug("REST request to count Fornitoris by criteria: {}", criteria);
        return ResponseEntity.ok().body(fornitoriQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fornitoris/:id} : get the "id" fornitori.
     *
     * @param id the id of the fornitori to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fornitori, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fornitoris/{id}")
    public ResponseEntity<Fornitori> getFornitori(@PathVariable Long id) {
        log.debug("REST request to get Fornitori : {}", id);
        Optional<Fornitori> fornitori = fornitoriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fornitori);
    }

    /**
     * {@code DELETE  /fornitoris/:id} : delete the "id" fornitori.
     *
     * @param id the id of the fornitori to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fornitoris/{id}")
    public ResponseEntity<Void> deleteFornitori(@PathVariable Long id) {
        log.debug("REST request to delete Fornitori : {}", id);
        fornitoriService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
