package com.terrambe.web.rest;

import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.service.AziRappresentanteCliService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.AziRappresentanteCliCriteria;
import com.terrambe.service.AziRappresentanteCliQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.AziRappresentanteCli}.
 */
@RestController
@RequestMapping("/api")
public class AziRappresentanteCliResource {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteCliResource.class);

    private static final String ENTITY_NAME = "aziRappresentanteCli";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AziRappresentanteCliService aziRappresentanteCliService;

    private final AziRappresentanteCliQueryService aziRappresentanteCliQueryService;

    public AziRappresentanteCliResource(AziRappresentanteCliService aziRappresentanteCliService, AziRappresentanteCliQueryService aziRappresentanteCliQueryService) {
        this.aziRappresentanteCliService = aziRappresentanteCliService;
        this.aziRappresentanteCliQueryService = aziRappresentanteCliQueryService;
    }

    /**
     * {@code POST  /azi-rappresentante-clis} : Create a new aziRappresentanteCli.
     *
     * @param aziRappresentanteCli the aziRappresentanteCli to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aziRappresentanteCli, or with status {@code 400 (Bad Request)} if the aziRappresentanteCli has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/azi-rappresentante-clis")
    public ResponseEntity<AziRappresentanteCli> createAziRappresentanteCli(@Valid @RequestBody AziRappresentanteCli aziRappresentanteCli) throws URISyntaxException {
        log.debug("REST request to save AziRappresentanteCli : {}", aziRappresentanteCli);
        if (aziRappresentanteCli.getId() != null) {
            throw new BadRequestAlertException("A new aziRappresentanteCli cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AziRappresentanteCli result = aziRappresentanteCliService.save(aziRappresentanteCli);
        return ResponseEntity.created(new URI("/api/azi-rappresentante-clis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /azi-rappresentante-clis} : Updates an existing aziRappresentanteCli.
     *
     * @param aziRappresentanteCli the aziRappresentanteCli to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aziRappresentanteCli,
     * or with status {@code 400 (Bad Request)} if the aziRappresentanteCli is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aziRappresentanteCli couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/azi-rappresentante-clis")
    public ResponseEntity<AziRappresentanteCli> updateAziRappresentanteCli(@Valid @RequestBody AziRappresentanteCli aziRappresentanteCli) throws URISyntaxException {
        log.debug("REST request to update AziRappresentanteCli : {}", aziRappresentanteCli);
        if (aziRappresentanteCli.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AziRappresentanteCli result = aziRappresentanteCliService.save(aziRappresentanteCli);
        AziRappresentanteCli result2 = aziRappresentanteCliService.save(aziRappresentanteCli);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aziRappresentanteCli.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /azi-rappresentante-clis} : get all the aziRappresentanteClis.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aziRappresentanteClis in body.
     */
    @GetMapping("/azi-rappresentante-clis")
    public ResponseEntity<List<AziRappresentanteCli>> getAllAziRappresentanteClis(AziRappresentanteCliCriteria criteria) {
        log.debug("REST request to get AziRappresentanteClis by criteria: {}", criteria);
        List<AziRappresentanteCli> entityList = aziRappresentanteCliQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /azi-rappresentante-clis/count} : count all the aziRappresentanteClis.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/azi-rappresentante-clis/count")
    public ResponseEntity<Long> countAziRappresentanteClis(AziRappresentanteCliCriteria criteria) {
        log.debug("REST request to count AziRappresentanteClis by criteria: {}", criteria);
        return ResponseEntity.ok().body(aziRappresentanteCliQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /azi-rappresentante-clis/:id} : get the "id" aziRappresentanteCli.
     *
     * @param id the id of the aziRappresentanteCli to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aziRappresentanteCli, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/azi-rappresentante-clis/{id}")
    public ResponseEntity<AziRappresentanteCli> getAziRappresentanteCli(@PathVariable Long id) {
        log.debug("REST request to get AziRappresentanteCli : {}", id);
        Optional<AziRappresentanteCli> aziRappresentanteCli = aziRappresentanteCliService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aziRappresentanteCli);
    }

    /**
     * {@code DELETE  /azi-rappresentante-clis/:id} : delete the "id" aziRappresentanteCli.
     *
     * @param id the id of the aziRappresentanteCli to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/azi-rappresentante-clis/{id}")
    public ResponseEntity<Void> deleteAziRappresentanteCli(@PathVariable Long id) {
        log.debug("REST request to delete AziRappresentanteCli : {}", id);
        aziRappresentanteCliService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
