package com.terrambe.web.rest;

import com.terrambe.domain.UnitaMisura;
import com.terrambe.service.UnitaMisuraService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.UnitaMisuraCriteria;
import com.terrambe.service.UnitaMisuraQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.UnitaMisura}.
 */
@RestController
@RequestMapping("/api")
public class UnitaMisuraResource {

    private final Logger log = LoggerFactory.getLogger(UnitaMisuraResource.class);

    private static final String ENTITY_NAME = "unitaMisura";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UnitaMisuraService unitaMisuraService;

    private final UnitaMisuraQueryService unitaMisuraQueryService;

    public UnitaMisuraResource(UnitaMisuraService unitaMisuraService, UnitaMisuraQueryService unitaMisuraQueryService) {
        this.unitaMisuraService = unitaMisuraService;
        this.unitaMisuraQueryService = unitaMisuraQueryService;
    }

    /**
     * {@code POST  /unita-misuras} : Create a new unitaMisura.
     *
     * @param unitaMisura the unitaMisura to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new unitaMisura, or with status {@code 400 (Bad Request)} if the unitaMisura has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/unita-misuras")
    public ResponseEntity<UnitaMisura> createUnitaMisura(@RequestBody UnitaMisura unitaMisura) throws URISyntaxException {
        log.debug("REST request to save UnitaMisura : {}", unitaMisura);
        if (unitaMisura.getId() != null) {
            throw new BadRequestAlertException("A new unitaMisura cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitaMisura result = unitaMisuraService.save(unitaMisura);
        return ResponseEntity.created(new URI("/api/unita-misuras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /unita-misuras} : Updates an existing unitaMisura.
     *
     * @param unitaMisura the unitaMisura to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unitaMisura,
     * or with status {@code 400 (Bad Request)} if the unitaMisura is not valid,
     * or with status {@code 500 (Internal Server Error)} if the unitaMisura couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/unita-misuras")
    public ResponseEntity<UnitaMisura> updateUnitaMisura(@RequestBody UnitaMisura unitaMisura) throws URISyntaxException {
        log.debug("REST request to update UnitaMisura : {}", unitaMisura);
        if (unitaMisura.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UnitaMisura result = unitaMisuraService.save(unitaMisura);
        UnitaMisura result2 = unitaMisuraService.save(unitaMisura);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, unitaMisura.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /unita-misuras} : get all the unitaMisuras.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of unitaMisuras in body.
     */
    @GetMapping("/unita-misuras")
    public ResponseEntity<List<UnitaMisura>> getAllUnitaMisuras(UnitaMisuraCriteria criteria) {
        log.debug("REST request to get UnitaMisuras by criteria: {}", criteria);
        List<UnitaMisura> entityList = unitaMisuraQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /unita-misuras/count} : count all the unitaMisuras.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/unita-misuras/count")
    public ResponseEntity<Long> countUnitaMisuras(UnitaMisuraCriteria criteria) {
        log.debug("REST request to count UnitaMisuras by criteria: {}", criteria);
        return ResponseEntity.ok().body(unitaMisuraQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /unita-misuras/:id} : get the "id" unitaMisura.
     *
     * @param id the id of the unitaMisura to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the unitaMisura, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/unita-misuras/{id}")
    public ResponseEntity<UnitaMisura> getUnitaMisura(@PathVariable Long id) {
        log.debug("REST request to get UnitaMisura : {}", id);
        Optional<UnitaMisura> unitaMisura = unitaMisuraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(unitaMisura);
    }

    /**
     * {@code DELETE  /unita-misuras/:id} : delete the "id" unitaMisura.
     *
     * @param id the id of the unitaMisura to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/unita-misuras/{id}")
    public ResponseEntity<Void> deleteUnitaMisura(@PathVariable Long id) {
        log.debug("REST request to delete UnitaMisura : {}", id);
        unitaMisuraService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
