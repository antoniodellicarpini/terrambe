package com.terrambe.web.rest;

import com.terrambe.domain.MotivoConc;
import com.terrambe.service.MotivoConcService;
import com.terrambe.web.rest.errors.BadRequestAlertException;
import com.terrambe.service.dto.MotivoConcCriteria;
import com.terrambe.service.MotivoConcQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.terrambe.domain.MotivoConc}.
 */
@RestController
@RequestMapping("/api")
public class MotivoConcResource {

    private final Logger log = LoggerFactory.getLogger(MotivoConcResource.class);

    private static final String ENTITY_NAME = "motivoConc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MotivoConcService motivoConcService;

    private final MotivoConcQueryService motivoConcQueryService;

    public MotivoConcResource(MotivoConcService motivoConcService, MotivoConcQueryService motivoConcQueryService) {
        this.motivoConcService = motivoConcService;
        this.motivoConcQueryService = motivoConcQueryService;
    }

    /**
     * {@code POST  /motivo-concs} : Create a new motivoConc.
     *
     * @param motivoConc the motivoConc to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new motivoConc, or with status {@code 400 (Bad Request)} if the motivoConc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/motivo-concs")
    public ResponseEntity<MotivoConc> createMotivoConc(@RequestBody MotivoConc motivoConc) throws URISyntaxException {
        log.debug("REST request to save MotivoConc : {}", motivoConc);
        if (motivoConc.getId() != null) {
            throw new BadRequestAlertException("A new motivoConc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MotivoConc result = motivoConcService.save(motivoConc);
        return ResponseEntity.created(new URI("/api/motivo-concs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /motivo-concs} : Updates an existing motivoConc.
     *
     * @param motivoConc the motivoConc to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated motivoConc,
     * or with status {@code 400 (Bad Request)} if the motivoConc is not valid,
     * or with status {@code 500 (Internal Server Error)} if the motivoConc couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/motivo-concs")
    public ResponseEntity<MotivoConc> updateMotivoConc(@RequestBody MotivoConc motivoConc) throws URISyntaxException {
        log.debug("REST request to update MotivoConc : {}", motivoConc);
        if (motivoConc.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MotivoConc result = motivoConcService.save(motivoConc);
        MotivoConc result2 = motivoConcService.save(motivoConc);
        log.debug("Fine del secondo save : {}", result2.getId().toString());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, motivoConc.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /motivo-concs} : get all the motivoConcs.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of motivoConcs in body.
     */
    @GetMapping("/motivo-concs")
    public ResponseEntity<List<MotivoConc>> getAllMotivoConcs(MotivoConcCriteria criteria) {
        log.debug("REST request to get MotivoConcs by criteria: {}", criteria);
        List<MotivoConc> entityList = motivoConcQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /motivo-concs/count} : count all the motivoConcs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/motivo-concs/count")
    public ResponseEntity<Long> countMotivoConcs(MotivoConcCriteria criteria) {
        log.debug("REST request to count MotivoConcs by criteria: {}", criteria);
        return ResponseEntity.ok().body(motivoConcQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /motivo-concs/:id} : get the "id" motivoConc.
     *
     * @param id the id of the motivoConc to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the motivoConc, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/motivo-concs/{id}")
    public ResponseEntity<MotivoConc> getMotivoConc(@PathVariable Long id) {
        log.debug("REST request to get MotivoConc : {}", id);
        Optional<MotivoConc> motivoConc = motivoConcService.findOne(id);
        return ResponseUtil.wrapOrNotFound(motivoConc);
    }

    /**
     * {@code DELETE  /motivo-concs/:id} : delete the "id" motivoConc.
     *
     * @param id the id of the motivoConc to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/motivo-concs/{id}")
    public ResponseEntity<Void> deleteMotivoConc(@PathVariable Long id) {
        log.debug("REST request to delete MotivoConc : {}", id);
        motivoConcService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
