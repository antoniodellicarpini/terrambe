package com.terrambe.repository;
import com.terrambe.domain.AziAnagrafica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziAnagrafica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziAnagraficaRepository extends JpaRepository<AziAnagrafica, Long>, JpaSpecificationExecutor<AziAnagrafica> {

}
