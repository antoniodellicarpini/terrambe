package com.terrambe.repository;
import com.terrambe.domain.Fornitori;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Fornitori entity.
 */
@Repository
public interface FornitoriRepository extends JpaRepository<Fornitori, Long>, JpaSpecificationExecutor<Fornitori> {

    @Query(value = "select distinct fornitori from Fornitori fornitori left join fetch fornitori.fornitoritipologias",
        countQuery = "select count(distinct fornitori) from Fornitori fornitori")
    Page<Fornitori> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct fornitori from Fornitori fornitori left join fetch fornitori.fornitoritipologias")
    List<Fornitori> findAllWithEagerRelationships();

    @Query("select fornitori from Fornitori fornitori left join fetch fornitori.fornitoritipologias where fornitori.id =:id")
    Optional<Fornitori> findOneWithEagerRelationships(@Param("id") Long id);

}
