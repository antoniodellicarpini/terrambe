package com.terrambe.repository;
import com.terrambe.domain.AziSupPartConduzione;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziSupPartConduzione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziSupPartConduzioneRepository extends JpaRepository<AziSupPartConduzione, Long>, JpaSpecificationExecutor<AziSupPartConduzione> {

}
