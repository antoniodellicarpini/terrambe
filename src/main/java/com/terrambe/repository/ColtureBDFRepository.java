package com.terrambe.repository;
import com.terrambe.domain.ColtureBDF;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ColtureBDF entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ColtureBDFRepository extends JpaRepository<ColtureBDF, Long>, JpaSpecificationExecutor<ColtureBDF> {

}
