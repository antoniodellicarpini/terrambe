package com.terrambe.repository;
import com.terrambe.domain.ProbabileFitoTossicita;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProbabileFitoTossicita entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProbabileFitoTossicitaRepository extends JpaRepository<ProbabileFitoTossicita, Long>, JpaSpecificationExecutor<ProbabileFitoTossicita> {

}
