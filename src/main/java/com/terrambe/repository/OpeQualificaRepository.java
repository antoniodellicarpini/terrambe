package com.terrambe.repository;
import com.terrambe.domain.OpeQualifica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OpeQualifica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OpeQualificaRepository extends JpaRepository<OpeQualifica, Long>, JpaSpecificationExecutor<OpeQualifica> {

}
