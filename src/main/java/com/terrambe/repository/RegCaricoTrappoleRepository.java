package com.terrambe.repository;
import com.terrambe.domain.RegCaricoTrappole;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegCaricoTrappole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegCaricoTrappoleRepository extends JpaRepository<RegCaricoTrappole, Long>, JpaSpecificationExecutor<RegCaricoTrappole> {

}
