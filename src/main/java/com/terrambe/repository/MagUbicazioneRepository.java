package com.terrambe.repository;
import com.terrambe.domain.MagUbicazione;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MagUbicazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MagUbicazioneRepository extends JpaRepository<MagUbicazione, Long>, JpaSpecificationExecutor<MagUbicazione> {

}
