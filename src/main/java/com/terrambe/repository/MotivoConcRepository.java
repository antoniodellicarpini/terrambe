package com.terrambe.repository;
import com.terrambe.domain.MotivoConc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MotivoConc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotivoConcRepository extends JpaRepository<MotivoConc, Long>, JpaSpecificationExecutor<MotivoConc> {

}
