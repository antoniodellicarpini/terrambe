package com.terrambe.repository;
import com.terrambe.domain.RegOpcTrap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcTrap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcTrapRepository extends JpaRepository<RegOpcTrap, Long>, JpaSpecificationExecutor<RegOpcTrap> {

}
