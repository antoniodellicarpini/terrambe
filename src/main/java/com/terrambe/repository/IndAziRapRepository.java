package com.terrambe.repository;
import com.terrambe.domain.IndAziRap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndAziRap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndAziRapRepository extends JpaRepository<IndAziRap, Long>, JpaSpecificationExecutor<IndAziRap> {

}
