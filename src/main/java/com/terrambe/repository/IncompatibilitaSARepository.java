package com.terrambe.repository;
import com.terrambe.domain.IncompatibilitaSA;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IncompatibilitaSA entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncompatibilitaSARepository extends JpaRepository<IncompatibilitaSA, Long>, JpaSpecificationExecutor<IncompatibilitaSA> {

}
