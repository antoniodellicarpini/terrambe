package com.terrambe.repository;
import com.terrambe.domain.RecAzienda;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecAzienda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecAziendaRepository extends JpaRepository<RecAzienda, Long>, JpaSpecificationExecutor<RecAzienda> {

}
