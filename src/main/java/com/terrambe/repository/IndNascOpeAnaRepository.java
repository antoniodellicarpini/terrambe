package com.terrambe.repository;
import com.terrambe.domain.IndNascOpeAna;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndNascOpeAna entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndNascOpeAnaRepository extends JpaRepository<IndNascOpeAna, Long>, JpaSpecificationExecutor<IndNascOpeAna> {

}
