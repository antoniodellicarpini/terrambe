package com.terrambe.repository;
import com.terrambe.domain.TipoRacc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoRacc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoRaccRepository extends JpaRepository<TipoRacc, Long>, JpaSpecificationExecutor<TipoRacc> {

}
