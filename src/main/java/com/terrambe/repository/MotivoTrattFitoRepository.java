package com.terrambe.repository;
import com.terrambe.domain.MotivoTrattFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MotivoTrattFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotivoTrattFitoRepository extends JpaRepository<MotivoTrattFito, Long>, JpaSpecificationExecutor<MotivoTrattFito> {

}
