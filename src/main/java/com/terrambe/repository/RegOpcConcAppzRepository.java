package com.terrambe.repository;
import com.terrambe.domain.RegOpcConcAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcConcAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcConcAppzRepository extends JpaRepository<RegOpcConcAppz, Long>, JpaSpecificationExecutor<RegOpcConcAppz> {

}
