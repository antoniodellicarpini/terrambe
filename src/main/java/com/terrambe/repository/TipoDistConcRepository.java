package com.terrambe.repository;
import com.terrambe.domain.TipoDistConc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoDistConc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoDistConcRepository extends JpaRepository<TipoDistConc, Long>, JpaSpecificationExecutor<TipoDistConc> {

}
