package com.terrambe.repository;
import com.terrambe.domain.Dose;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Dose entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoseRepository extends JpaRepository<Dose, Long>, JpaSpecificationExecutor<Dose> {

}
