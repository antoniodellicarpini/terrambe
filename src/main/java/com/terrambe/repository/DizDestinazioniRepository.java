package com.terrambe.repository;
import com.terrambe.domain.DizDestinazioni;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizDestinazioni entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizDestinazioniRepository extends JpaRepository<DizDestinazioni, Long>, JpaSpecificationExecutor<DizDestinazioni> {

}
