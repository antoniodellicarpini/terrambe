package com.terrambe.repository;
import com.terrambe.domain.DizMacrousi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizMacrousi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizMacrousiRepository extends JpaRepository<DizMacrousi, Long>, JpaSpecificationExecutor<DizMacrousi> {

}
