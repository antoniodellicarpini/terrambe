package com.terrambe.repository;
import com.terrambe.domain.Agea20152020Matrice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Agea20152020Matrice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Agea20152020MatriceRepository extends JpaRepository<Agea20152020Matrice, Long>, JpaSpecificationExecutor<Agea20152020Matrice> {

}
