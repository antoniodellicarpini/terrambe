package com.terrambe.repository;
import com.terrambe.domain.RegCaricoFertilizzante;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegCaricoFertilizzante entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegCaricoFertilizzanteRepository extends JpaRepository<RegCaricoFertilizzante, Long>, JpaSpecificationExecutor<RegCaricoFertilizzante> {

}
