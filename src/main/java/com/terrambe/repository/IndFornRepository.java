package com.terrambe.repository;
import com.terrambe.domain.IndForn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndForn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndFornRepository extends JpaRepository<IndForn, Long>, JpaSpecificationExecutor<IndForn> {

}
