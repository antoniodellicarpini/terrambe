package com.terrambe.repository;
import com.terrambe.domain.BioAppzTipo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BioAppzTipo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BioAppzTipoRepository extends JpaRepository<BioAppzTipo, Long>, JpaSpecificationExecutor<BioAppzTipo> {

}
