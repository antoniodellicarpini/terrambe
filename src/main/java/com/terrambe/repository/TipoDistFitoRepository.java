package com.terrambe.repository;
import com.terrambe.domain.TipoDistFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoDistFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoDistFitoRepository extends JpaRepository<TipoDistFito, Long>, JpaSpecificationExecutor<TipoDistFito> {

}
