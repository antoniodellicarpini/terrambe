package com.terrambe.repository;
import com.terrambe.domain.RegOpcConc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcConc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcConcRepository extends JpaRepository<RegOpcConc, Long>, JpaSpecificationExecutor<RegOpcConc> {

}
