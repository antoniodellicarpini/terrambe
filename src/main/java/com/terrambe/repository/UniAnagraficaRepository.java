package com.terrambe.repository;
import com.terrambe.domain.UniAnagrafica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UniAnagrafica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UniAnagraficaRepository extends JpaRepository<UniAnagrafica, Long>, JpaSpecificationExecutor<UniAnagrafica> {

}
