package com.terrambe.repository;
import com.terrambe.domain.ModIrr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ModIrr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModIrrRepository extends JpaRepository<ModIrr, Long>, JpaSpecificationExecutor<ModIrr> {

}
