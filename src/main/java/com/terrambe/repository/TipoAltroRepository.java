package com.terrambe.repository;
import com.terrambe.domain.TipoAltro;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoAltro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoAltroRepository extends JpaRepository<TipoAltro, Long>, JpaSpecificationExecutor<TipoAltro> {

}
