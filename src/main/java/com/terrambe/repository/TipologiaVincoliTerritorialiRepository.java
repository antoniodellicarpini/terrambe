package com.terrambe.repository;
import com.terrambe.domain.TipologiaVincoliTerritoriali;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipologiaVincoliTerritoriali entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipologiaVincoliTerritorialiRepository extends JpaRepository<TipologiaVincoliTerritoriali, Long>, JpaSpecificationExecutor<TipologiaVincoliTerritoriali> {

}
