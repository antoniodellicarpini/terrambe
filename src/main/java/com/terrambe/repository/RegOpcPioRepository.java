package com.terrambe.repository;
import com.terrambe.domain.RegOpcPio;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcPio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcPioRepository extends JpaRepository<RegOpcPio, Long>, JpaSpecificationExecutor<RegOpcPio> {

}
