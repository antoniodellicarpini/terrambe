package com.terrambe.repository;
import com.terrambe.domain.AziSupPartDettaglio;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziSupPartDettaglio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziSupPartDettaglioRepository extends JpaRepository<AziSupPartDettaglio, Long>, JpaSpecificationExecutor<AziSupPartDettaglio> {

}
