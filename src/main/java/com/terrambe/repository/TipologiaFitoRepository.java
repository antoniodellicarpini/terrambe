package com.terrambe.repository;
import com.terrambe.domain.TipologiaFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipologiaFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipologiaFitoRepository extends JpaRepository<TipologiaFito, Long>, JpaSpecificationExecutor<TipologiaFito> {

}
