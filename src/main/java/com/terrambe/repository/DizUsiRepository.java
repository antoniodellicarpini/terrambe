package com.terrambe.repository;
import com.terrambe.domain.DizUsi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizUsi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizUsiRepository extends JpaRepository<DizUsi, Long>, JpaSpecificationExecutor<DizUsi> {

}
