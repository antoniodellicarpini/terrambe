package com.terrambe.repository;
import com.terrambe.domain.DizQualita;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizQualita entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizQualitaRepository extends JpaRepository<DizQualita, Long>, JpaSpecificationExecutor<DizQualita> {

}
