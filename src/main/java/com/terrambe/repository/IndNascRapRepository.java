package com.terrambe.repository;
import com.terrambe.domain.IndNascRap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndNascRap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndNascRapRepository extends JpaRepository<IndNascRap, Long>, JpaSpecificationExecutor<IndNascRap> {

}
