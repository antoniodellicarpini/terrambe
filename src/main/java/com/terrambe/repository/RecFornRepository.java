package com.terrambe.repository;
import com.terrambe.domain.RecForn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecForn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecFornRepository extends JpaRepository<RecForn, Long>, JpaSpecificationExecutor<RecForn> {

}
