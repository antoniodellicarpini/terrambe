package com.terrambe.repository;
import com.terrambe.domain.TipologieOpc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipologieOpc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipologieOpcRepository extends JpaRepository<TipologieOpc, Long>, JpaSpecificationExecutor<TipologieOpc> {

}
