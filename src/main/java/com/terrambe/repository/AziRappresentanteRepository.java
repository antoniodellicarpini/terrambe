package com.terrambe.repository;
import com.terrambe.domain.AziRappresentante;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziRappresentante entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziRappresentanteRepository extends JpaRepository<AziRappresentante, Long>, JpaSpecificationExecutor<AziRappresentante> {

}
