package com.terrambe.repository;
import com.terrambe.domain.RegOpcPrepterr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcPrepterr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcPrepterrRepository extends JpaRepository<RegOpcPrepterr, Long>, JpaSpecificationExecutor<RegOpcPrepterr> {

}
