package com.terrambe.repository;
import com.terrambe.domain.RecAziRapCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecAziRapCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecAziRapCliRepository extends JpaRepository<RecAziRapCli, Long>, JpaSpecificationExecutor<RecAziRapCli> {

}
