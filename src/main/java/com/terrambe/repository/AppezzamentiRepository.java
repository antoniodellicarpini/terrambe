package com.terrambe.repository;
import com.terrambe.domain.Appezzamenti;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Appezzamenti entity.
 */
@Repository
public interface AppezzamentiRepository extends JpaRepository<Appezzamenti, Long>, JpaSpecificationExecutor<Appezzamenti> {

    @Query(value = "select distinct appezzamenti from Appezzamenti appezzamenti left join fetch appezzamenti.appzToParts left join fetch appezzamenti.appezToVincoliTers",
        countQuery = "select count(distinct appezzamenti) from Appezzamenti appezzamenti")
    Page<Appezzamenti> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct appezzamenti from Appezzamenti appezzamenti left join fetch appezzamenti.appzToParts left join fetch appezzamenti.appezToVincoliTers")
    List<Appezzamenti> findAllWithEagerRelationships();

    @Query("select appezzamenti from Appezzamenti appezzamenti left join fetch appezzamenti.appzToParts left join fetch appezzamenti.appezToVincoliTers where appezzamenti.id =:id")
    Optional<Appezzamenti> findOneWithEagerRelationships(@Param("id") Long id);

}
