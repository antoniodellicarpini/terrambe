package com.terrambe.repository;
import com.terrambe.domain.EtiTipologiaFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EtiTipologiaFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtiTipologiaFitoRepository extends JpaRepository<EtiTipologiaFito, Long>, JpaSpecificationExecutor<EtiTipologiaFito> {

}
