package com.terrambe.repository;
import com.terrambe.domain.TrapContrTipo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TrapContrTipo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrapContrTipoRepository extends JpaRepository<TrapContrTipo, Long>, JpaSpecificationExecutor<TrapContrTipo> {

}
