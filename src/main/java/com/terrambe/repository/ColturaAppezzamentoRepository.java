package com.terrambe.repository;
import com.terrambe.domain.ColturaAppezzamento;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ColturaAppezzamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ColturaAppezzamentoRepository extends JpaRepository<ColturaAppezzamento, Long>, JpaSpecificationExecutor<ColturaAppezzamento> {

}
