package com.terrambe.repository;
import com.terrambe.domain.RegOpcPrepterrAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcPrepterrAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcPrepterrAppzRepository extends JpaRepository<RegOpcPrepterrAppz, Long>, JpaSpecificationExecutor<RegOpcPrepterrAppz> {

}
