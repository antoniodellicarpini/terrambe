package com.terrambe.repository;
import com.terrambe.domain.TipoIrr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoIrr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoIrrRepository extends JpaRepository<TipoIrr, Long>, JpaSpecificationExecutor<TipoIrr> {

}
