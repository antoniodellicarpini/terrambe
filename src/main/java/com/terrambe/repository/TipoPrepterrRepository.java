package com.terrambe.repository;
import com.terrambe.domain.TipoPrepterr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoPrepterr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoPrepterrRepository extends JpaRepository<TipoPrepterr, Long>, JpaSpecificationExecutor<TipoPrepterr> {

}
