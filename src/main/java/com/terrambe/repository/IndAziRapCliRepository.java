package com.terrambe.repository;
import com.terrambe.domain.IndAziRapCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndAziRapCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndAziRapCliRepository extends JpaRepository<IndAziRapCli, Long>, JpaSpecificationExecutor<IndAziRapCli> {

}
