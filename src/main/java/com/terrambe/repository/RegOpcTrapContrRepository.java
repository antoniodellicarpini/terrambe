package com.terrambe.repository;
import com.terrambe.domain.RegOpcTrapContr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcTrapContr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcTrapContrRepository extends JpaRepository<RegOpcTrapContr, Long>, JpaSpecificationExecutor<RegOpcTrapContr> {

}
