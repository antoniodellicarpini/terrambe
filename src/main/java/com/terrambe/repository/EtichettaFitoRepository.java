package com.terrambe.repository;
import com.terrambe.domain.EtichettaFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EtichettaFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtichettaFitoRepository extends JpaRepository<EtichettaFito, Long>, JpaSpecificationExecutor<EtichettaFito> {

}
