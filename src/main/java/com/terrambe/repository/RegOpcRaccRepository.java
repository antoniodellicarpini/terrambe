package com.terrambe.repository;
import com.terrambe.domain.RegOpcRacc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcRacc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcRaccRepository extends JpaRepository<RegOpcRacc, Long>, JpaSpecificationExecutor<RegOpcRacc> {

}
