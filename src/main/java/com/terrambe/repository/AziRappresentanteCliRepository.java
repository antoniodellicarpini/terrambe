package com.terrambe.repository;
import com.terrambe.domain.AziRappresentanteCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziRappresentanteCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziRappresentanteCliRepository extends JpaRepository<AziRappresentanteCli, Long>, JpaSpecificationExecutor<AziRappresentanteCli> {

}
