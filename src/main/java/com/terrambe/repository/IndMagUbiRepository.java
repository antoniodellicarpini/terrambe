package com.terrambe.repository;
import com.terrambe.domain.IndMagUbi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndMagUbi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndMagUbiRepository extends JpaRepository<IndMagUbi, Long>, JpaSpecificationExecutor<IndMagUbi> {

}
