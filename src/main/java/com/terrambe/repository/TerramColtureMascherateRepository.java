package com.terrambe.repository;
import com.terrambe.domain.TerramColtureMascherate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TerramColtureMascherate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TerramColtureMascherateRepository extends JpaRepository<TerramColtureMascherate, Long>, JpaSpecificationExecutor<TerramColtureMascherate> {

}
