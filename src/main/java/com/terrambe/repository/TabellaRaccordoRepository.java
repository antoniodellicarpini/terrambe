package com.terrambe.repository;
import com.terrambe.domain.TabellaRaccordo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TabellaRaccordo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TabellaRaccordoRepository extends JpaRepository<TabellaRaccordo, Long>, JpaSpecificationExecutor<TabellaRaccordo> {

}
