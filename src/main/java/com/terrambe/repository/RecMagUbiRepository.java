package com.terrambe.repository;
import com.terrambe.domain.RecMagUbi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecMagUbi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecMagUbiRepository extends JpaRepository<RecMagUbi, Long>, JpaSpecificationExecutor<RecMagUbi> {

}
