package com.terrambe.repository;
import com.terrambe.domain.TipoContratto;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoContratto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoContrattoRepository extends JpaRepository<TipoContratto, Long>, JpaSpecificationExecutor<TipoContratto> {

}
