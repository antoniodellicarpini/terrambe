package com.terrambe.repository;
import com.terrambe.domain.EtichettaFrasi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EtichettaFrasi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtichettaFrasiRepository extends JpaRepository<EtichettaFrasi, Long>, JpaSpecificationExecutor<EtichettaFrasi> {

}
