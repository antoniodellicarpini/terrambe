package com.terrambe.repository;
import com.terrambe.domain.AziFormaGiuridica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziFormaGiuridica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziFormaGiuridicaRepository extends JpaRepository<AziFormaGiuridica, Long>, JpaSpecificationExecutor<AziFormaGiuridica> {

}
