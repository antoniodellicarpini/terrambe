package com.terrambe.repository;
import com.terrambe.domain.Formulazione;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Formulazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormulazioneRepository extends JpaRepository<Formulazione, Long>, JpaSpecificationExecutor<Formulazione> {

}
