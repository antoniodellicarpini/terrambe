package com.terrambe.repository;
import com.terrambe.domain.MotivoIrr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MotivoIrr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotivoIrrRepository extends JpaRepository<MotivoIrr, Long>, JpaSpecificationExecutor<MotivoIrr> {

}
