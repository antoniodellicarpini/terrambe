package com.terrambe.repository;
import com.terrambe.domain.RegOpcAltroAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcAltroAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcAltroAppzRepository extends JpaRepository<RegOpcAltroAppz, Long>, JpaSpecificationExecutor<RegOpcAltroAppz> {

}
