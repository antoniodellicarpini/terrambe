package com.terrambe.repository;
import com.terrambe.domain.TipologiaPermessoOperatore;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipologiaPermessoOperatore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipologiaPermessoOperatoreRepository extends JpaRepository<TipologiaPermessoOperatore, Long>, JpaSpecificationExecutor<TipologiaPermessoOperatore> {

}
