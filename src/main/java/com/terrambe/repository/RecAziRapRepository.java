package com.terrambe.repository;
import com.terrambe.domain.RecAziRap;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecAziRap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecAziRapRepository extends JpaRepository<RecAziRap, Long>, JpaSpecificationExecutor<RecAziRap> {

}
