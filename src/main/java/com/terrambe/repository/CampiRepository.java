package com.terrambe.repository;
import com.terrambe.domain.Campi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Campi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampiRepository extends JpaRepository<Campi, Long>, JpaSpecificationExecutor<Campi> {

}
