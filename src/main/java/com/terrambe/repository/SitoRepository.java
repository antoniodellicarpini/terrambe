package com.terrambe.repository;
import com.terrambe.domain.Sito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SitoRepository extends JpaRepository<Sito, Long>, JpaSpecificationExecutor<Sito> {

}
