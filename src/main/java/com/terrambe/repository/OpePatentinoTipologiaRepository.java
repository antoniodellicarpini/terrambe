package com.terrambe.repository;
import com.terrambe.domain.OpePatentinoTipologia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OpePatentinoTipologia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OpePatentinoTipologiaRepository extends JpaRepository<OpePatentinoTipologia, Long>, JpaSpecificationExecutor<OpePatentinoTipologia> {

}
