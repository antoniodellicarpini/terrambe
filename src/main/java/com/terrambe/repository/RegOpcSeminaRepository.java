package com.terrambe.repository;
import com.terrambe.domain.RegOpcSemina;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcSemina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcSeminaRepository extends JpaRepository<RegOpcSemina, Long>, JpaSpecificationExecutor<RegOpcSemina> {

}
