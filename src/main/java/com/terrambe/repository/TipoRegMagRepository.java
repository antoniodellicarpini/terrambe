package com.terrambe.repository;
import com.terrambe.domain.TipoRegMag;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoRegMag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoRegMagRepository extends JpaRepository<TipoRegMag, Long>, JpaSpecificationExecutor<TipoRegMag> {

}
