package com.terrambe.repository;
import com.terrambe.domain.RegOpcIrrAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcIrrAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcIrrAppzRepository extends JpaRepository<RegOpcIrrAppz, Long>, JpaSpecificationExecutor<RegOpcIrrAppz> {

}
