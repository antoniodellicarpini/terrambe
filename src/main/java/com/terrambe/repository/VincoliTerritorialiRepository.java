package com.terrambe.repository;
import com.terrambe.domain.VincoliTerritoriali;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the VincoliTerritoriali entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VincoliTerritorialiRepository extends JpaRepository<VincoliTerritoriali, Long>, JpaSpecificationExecutor<VincoliTerritoriali> {

}
