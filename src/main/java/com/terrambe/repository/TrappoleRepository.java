package com.terrambe.repository;
import com.terrambe.domain.Trappole;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Trappole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrappoleRepository extends JpaRepository<Trappole, Long>, JpaSpecificationExecutor<Trappole> {

}
