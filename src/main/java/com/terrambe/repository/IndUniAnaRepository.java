package com.terrambe.repository;
import com.terrambe.domain.IndUniAna;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndUniAna entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndUniAnaRepository extends JpaRepository<IndUniAna, Long>, JpaSpecificationExecutor<IndUniAna> {

}
