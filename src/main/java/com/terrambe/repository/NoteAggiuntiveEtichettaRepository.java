package com.terrambe.repository;
import com.terrambe.domain.NoteAggiuntiveEtichetta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NoteAggiuntiveEtichetta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NoteAggiuntiveEtichettaRepository extends JpaRepository<NoteAggiuntiveEtichetta, Long>, JpaSpecificationExecutor<NoteAggiuntiveEtichetta> {

}
