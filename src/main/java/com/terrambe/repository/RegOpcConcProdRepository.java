package com.terrambe.repository;
import com.terrambe.domain.RegOpcConcProd;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcConcProd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcConcProdRepository extends JpaRepository<RegOpcConcProd, Long>, JpaSpecificationExecutor<RegOpcConcProd> {

}
