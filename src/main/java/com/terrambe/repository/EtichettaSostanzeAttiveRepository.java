package com.terrambe.repository;
import com.terrambe.domain.EtichettaSostanzeAttive;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EtichettaSostanzeAttive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtichettaSostanzeAttiveRepository extends JpaRepository<EtichettaSostanzeAttive, Long>, JpaSpecificationExecutor<EtichettaSostanzeAttive> {

}
