package com.terrambe.repository;
import com.terrambe.domain.RegOpcFito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcFito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcFitoRepository extends JpaRepository<RegOpcFito, Long>, JpaSpecificationExecutor<RegOpcFito> {

}
