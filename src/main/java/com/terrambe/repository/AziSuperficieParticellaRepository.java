package com.terrambe.repository;
import com.terrambe.domain.AziSuperficieParticella;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AziSuperficieParticella entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AziSuperficieParticellaRepository extends JpaRepository<AziSuperficieParticella, Long>, JpaSpecificationExecutor<AziSuperficieParticella> {

    //Nel repository jpa creo un metodo con parametri id dell'oggetto lato one e id di uno degli oggetti lato many
    // e scrivo la query nativa per eseguire l'aggiormanento
    @Modifying
    @Query(value = "update azi_superficie_particella  set azi_sup_to_campi_id = ? where id = ?", nativeQuery = true)
    int addCampiIdToExistingParticella(String campoId, String particellaId);
}
