package com.terrambe.repository;
import com.terrambe.domain.RegOpcRaccAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcRaccAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcRaccAppzRepository extends JpaRepository<RegOpcRaccAppz, Long>, JpaSpecificationExecutor<RegOpcRaccAppz> {

}
