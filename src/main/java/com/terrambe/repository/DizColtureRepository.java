package com.terrambe.repository;
import com.terrambe.domain.DizColture;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizColture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizColtureRepository extends JpaRepository<DizColture, Long>, JpaSpecificationExecutor<DizColture> {

}
