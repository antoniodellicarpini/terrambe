package com.terrambe.repository;
import com.terrambe.domain.FerAnagrafica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FerAnagrafica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FerAnagraficaRepository extends JpaRepository<FerAnagrafica, Long>, JpaSpecificationExecutor<FerAnagrafica> {

}
