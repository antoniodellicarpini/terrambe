package com.terrambe.repository;
import com.terrambe.domain.IndAziendaCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndAziendaCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndAziendaCliRepository extends JpaRepository<IndAziendaCli, Long>, JpaSpecificationExecutor<IndAziendaCli> {

}
