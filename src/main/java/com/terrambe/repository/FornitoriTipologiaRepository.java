package com.terrambe.repository;
import com.terrambe.domain.FornitoriTipologia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FornitoriTipologia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FornitoriTipologiaRepository extends JpaRepository<FornitoriTipologia, Long>, JpaSpecificationExecutor<FornitoriTipologia> {

}
