package com.terrambe.repository;
import com.terrambe.domain.DizVarieta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DizVarieta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DizVarietaRepository extends JpaRepository<DizVarieta, Long>, JpaSpecificationExecutor<DizVarieta> {

}
