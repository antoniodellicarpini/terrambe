package com.terrambe.repository;
import com.terrambe.domain.UserTerram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UserTerram entity.
 */
@Repository
public interface UserTerramRepository extends JpaRepository<UserTerram, Long>, JpaSpecificationExecutor<UserTerram> {

    @Query(value = "select distinct userTerram from UserTerram userTerram left join fetch userTerram.usrTerToAziAnas",
        countQuery = "select count(distinct userTerram) from UserTerram userTerram")
    Page<UserTerram> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct userTerram from UserTerram userTerram left join fetch userTerram.usrTerToAziAnas")
    List<UserTerram> findAllWithEagerRelationships();

    @Query("select userTerram from UserTerram userTerram left join fetch userTerram.usrTerToAziAnas where userTerram.id =:id")
    Optional<UserTerram> findOneWithEagerRelationships(@Param("id") Long id);

}
