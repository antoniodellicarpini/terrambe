package com.terrambe.repository;
import com.terrambe.domain.SostanzeAttive;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SostanzeAttive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SostanzeAttiveRepository extends JpaRepository<SostanzeAttive, Long>, JpaSpecificationExecutor<SostanzeAttive> {

}
