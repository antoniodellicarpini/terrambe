package com.terrambe.repository;
import com.terrambe.domain.RecOpeAna;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecOpeAna entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecOpeAnaRepository extends JpaRepository<RecOpeAna, Long>, JpaSpecificationExecutor<RecOpeAna> {

}
