package com.terrambe.repository;
import com.terrambe.domain.RegOpcFitoProd;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcFitoProd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcFitoProdRepository extends JpaRepository<RegOpcFitoProd, Long>, JpaSpecificationExecutor<RegOpcFitoProd> {

}
