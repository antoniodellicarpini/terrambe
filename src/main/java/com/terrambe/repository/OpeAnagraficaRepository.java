package com.terrambe.repository;
import com.terrambe.domain.OpeAnagrafica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the OpeAnagrafica entity.
 */
@Repository
public interface OpeAnagraficaRepository extends JpaRepository<OpeAnagrafica, Long>, JpaSpecificationExecutor<OpeAnagrafica> {

    @Query(value = "select distinct opeAnagrafica from OpeAnagrafica opeAnagrafica left join fetch opeAnagrafica.opeAnaToForns",
        countQuery = "select count(distinct opeAnagrafica) from OpeAnagrafica opeAnagrafica")
    Page<OpeAnagrafica> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct opeAnagrafica from OpeAnagrafica opeAnagrafica left join fetch opeAnagrafica.opeAnaToForns")
    List<OpeAnagrafica> findAllWithEagerRelationships();

    @Query("select opeAnagrafica from OpeAnagrafica opeAnagrafica left join fetch opeAnagrafica.opeAnaToForns where opeAnagrafica.id =:id")
    Optional<OpeAnagrafica> findOneWithEagerRelationships(@Param("id") Long id);

}
