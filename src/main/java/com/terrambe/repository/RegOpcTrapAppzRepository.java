package com.terrambe.repository;
import com.terrambe.domain.RegOpcTrapAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcTrapAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcTrapAppzRepository extends JpaRepository<RegOpcTrapAppz, Long>, JpaSpecificationExecutor<RegOpcTrapAppz> {

}
