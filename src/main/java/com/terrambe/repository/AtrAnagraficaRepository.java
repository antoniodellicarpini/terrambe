package com.terrambe.repository;
import com.terrambe.domain.AtrAnagrafica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AtrAnagrafica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtrAnagraficaRepository extends JpaRepository<AtrAnagrafica, Long>, JpaSpecificationExecutor<AtrAnagrafica> {

}
