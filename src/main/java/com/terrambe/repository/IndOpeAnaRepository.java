package com.terrambe.repository;
import com.terrambe.domain.IndOpeAna;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndOpeAna entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndOpeAnaRepository extends JpaRepository<IndOpeAna, Long>, JpaSpecificationExecutor<IndOpeAna> {

}
