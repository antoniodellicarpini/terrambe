package com.terrambe.repository;
import com.terrambe.domain.IndAzienda;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndAzienda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndAziendaRepository extends JpaRepository<IndAzienda, Long>, JpaSpecificationExecutor<IndAzienda> {

}
