package com.terrambe.repository;
import com.terrambe.domain.TipoTrappola;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoTrappola entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoTrappolaRepository extends JpaRepository<TipoTrappola, Long>, JpaSpecificationExecutor<TipoTrappola> {

}
