package com.terrambe.repository;
import com.terrambe.domain.Frasi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Frasi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FrasiRepository extends JpaRepository<Frasi, Long>, JpaSpecificationExecutor<Frasi> {

}
