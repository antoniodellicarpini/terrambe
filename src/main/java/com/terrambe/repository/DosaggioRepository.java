package com.terrambe.repository;
import com.terrambe.domain.Dosaggio;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Dosaggio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DosaggioRepository extends JpaRepository<Dosaggio, Long>, JpaSpecificationExecutor<Dosaggio> {

}
