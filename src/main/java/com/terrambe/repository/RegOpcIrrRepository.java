package com.terrambe.repository;
import com.terrambe.domain.RegOpcIrr;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcIrr entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcIrrRepository extends JpaRepository<RegOpcIrr, Long>, JpaSpecificationExecutor<RegOpcIrr> {

}
