package com.terrambe.repository;
import com.terrambe.domain.Brogliaccio;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Brogliaccio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BrogliaccioRepository extends JpaRepository<Brogliaccio, Long>, JpaSpecificationExecutor<Brogliaccio> {

}
