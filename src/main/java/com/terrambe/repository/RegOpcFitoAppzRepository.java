package com.terrambe.repository;
import com.terrambe.domain.RegOpcFitoAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcFitoAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcFitoAppzRepository extends JpaRepository<RegOpcFitoAppz, Long>, JpaSpecificationExecutor<RegOpcFitoAppz> {

}
