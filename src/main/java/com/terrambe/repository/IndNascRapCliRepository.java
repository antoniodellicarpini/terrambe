package com.terrambe.repository;
import com.terrambe.domain.IndNascRapCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndNascRapCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndNascRapCliRepository extends JpaRepository<IndNascRapCli, Long>, JpaSpecificationExecutor<IndNascRapCli> {

}
