package com.terrambe.repository;
import com.terrambe.domain.Agea20152020BDF;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Agea20152020BDF entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Agea20152020BDFRepository extends JpaRepository<Agea20152020BDF, Long>, JpaSpecificationExecutor<Agea20152020BDF> {

}
