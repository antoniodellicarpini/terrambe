package com.terrambe.repository;
import com.terrambe.domain.RegOpcAltro;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcAltro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcAltroRepository extends JpaRepository<RegOpcAltro, Long>, JpaSpecificationExecutor<RegOpcAltro> {

}
