package com.terrambe.repository;
import com.terrambe.domain.FaseFenologica;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FaseFenologica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FaseFenologicaRepository extends JpaRepository<FaseFenologica, Long>, JpaSpecificationExecutor<FaseFenologica> {

}
