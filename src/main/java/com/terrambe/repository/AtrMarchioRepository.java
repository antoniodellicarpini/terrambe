package com.terrambe.repository;
import com.terrambe.domain.AtrMarchio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the AtrMarchio entity.
 */
@Repository
public interface AtrMarchioRepository extends JpaRepository<AtrMarchio, Long>, JpaSpecificationExecutor<AtrMarchio> {

    @Query(value = "select distinct atrMarchio from AtrMarchio atrMarchio left join fetch atrMarchio.atrTipologias",
        countQuery = "select count(distinct atrMarchio) from AtrMarchio atrMarchio")
    Page<AtrMarchio> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct atrMarchio from AtrMarchio atrMarchio left join fetch atrMarchio.atrTipologias")
    List<AtrMarchio> findAllWithEagerRelationships();

    @Query("select atrMarchio from AtrMarchio atrMarchio left join fetch atrMarchio.atrTipologias where atrMarchio.id =:id")
    Optional<AtrMarchio> findOneWithEagerRelationships(@Param("id") Long id);

}
