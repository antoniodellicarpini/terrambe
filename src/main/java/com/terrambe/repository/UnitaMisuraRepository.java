package com.terrambe.repository;
import com.terrambe.domain.UnitaMisura;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UnitaMisura entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnitaMisuraRepository extends JpaRepository<UnitaMisura, Long>, JpaSpecificationExecutor<UnitaMisura> {

}
