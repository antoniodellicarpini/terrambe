package com.terrambe.repository;
import com.terrambe.domain.IndNascForn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IndNascForn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndNascFornRepository extends JpaRepository<IndNascForn, Long>, JpaSpecificationExecutor<IndNascForn> {

}
