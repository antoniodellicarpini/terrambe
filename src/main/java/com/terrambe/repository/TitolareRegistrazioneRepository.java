package com.terrambe.repository;
import com.terrambe.domain.TitolareRegistrazione;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TitolareRegistrazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TitolareRegistrazioneRepository extends JpaRepository<TitolareRegistrazione, Long>, JpaSpecificationExecutor<TitolareRegistrazione> {

}
