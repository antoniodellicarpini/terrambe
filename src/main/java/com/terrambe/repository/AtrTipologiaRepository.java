package com.terrambe.repository;
import com.terrambe.domain.AtrTipologia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AtrTipologia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtrTipologiaRepository extends JpaRepository<AtrTipologia, Long>, JpaSpecificationExecutor<AtrTipologia> {

}
