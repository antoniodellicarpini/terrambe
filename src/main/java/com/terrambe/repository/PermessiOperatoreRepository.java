package com.terrambe.repository;
import com.terrambe.domain.PermessiOperatore;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PermessiOperatore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PermessiOperatoreRepository extends JpaRepository<PermessiOperatore, Long>, JpaSpecificationExecutor<PermessiOperatore> {

}
