package com.terrambe.repository;
import com.terrambe.domain.Syslog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Syslog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SyslogRepository extends JpaRepository<Syslog, Long>, JpaSpecificationExecutor<Syslog> {

}
