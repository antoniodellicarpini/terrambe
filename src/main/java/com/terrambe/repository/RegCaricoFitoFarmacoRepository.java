package com.terrambe.repository;
import com.terrambe.domain.RegCaricoFitoFarmaco;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegCaricoFitoFarmaco entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegCaricoFitoFarmacoRepository extends JpaRepository<RegCaricoFitoFarmaco, Long>, JpaSpecificationExecutor<RegCaricoFitoFarmaco> {

}
