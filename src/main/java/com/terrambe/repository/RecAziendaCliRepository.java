package com.terrambe.repository;
import com.terrambe.domain.RecAziendaCli;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RecAziendaCli entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecAziendaCliRepository extends JpaRepository<RecAziendaCli, Long>, JpaSpecificationExecutor<RecAziendaCli> {

}
