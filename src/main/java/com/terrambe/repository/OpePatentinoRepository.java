package com.terrambe.repository;
import com.terrambe.domain.OpePatentino;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OpePatentino entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OpePatentinoRepository extends JpaRepository<OpePatentino, Long>, JpaSpecificationExecutor<OpePatentino> {

}
