package com.terrambe.repository;
import com.terrambe.domain.PartAppez;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PartAppez entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartAppezRepository extends JpaRepository<PartAppez, Long>, JpaSpecificationExecutor<PartAppez> {

}
