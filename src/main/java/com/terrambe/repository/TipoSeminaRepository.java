package com.terrambe.repository;
import com.terrambe.domain.TipoSemina;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoSemina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoSeminaRepository extends JpaRepository<TipoSemina, Long>, JpaSpecificationExecutor<TipoSemina> {

}
