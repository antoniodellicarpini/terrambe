package com.terrambe.repository;
import com.terrambe.domain.DosaggioTrattamento;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DosaggioTrattamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DosaggioTrattamentoRepository extends JpaRepository<DosaggioTrattamento, Long>, JpaSpecificationExecutor<DosaggioTrattamento> {

}
