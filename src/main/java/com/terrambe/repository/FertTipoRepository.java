package com.terrambe.repository;
import com.terrambe.domain.FertTipo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FertTipo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FertTipoRepository extends JpaRepository<FertTipo, Long>, JpaSpecificationExecutor<FertTipo> {

}
