package com.terrambe.repository;
import com.terrambe.domain.RegOpcSeminaAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcSeminaAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcSeminaAppzRepository extends JpaRepository<RegOpcSeminaAppz, Long>, JpaSpecificationExecutor<RegOpcSeminaAppz> {

}
