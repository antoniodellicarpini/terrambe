package com.terrambe.repository;
import com.terrambe.domain.Avversita;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Avversita entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AvversitaRepository extends JpaRepository<Avversita, Long>, JpaSpecificationExecutor<Avversita> {

}
