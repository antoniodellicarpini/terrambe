package com.terrambe.repository;
import com.terrambe.domain.EtichettaPittogrammi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EtichettaPittogrammi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtichettaPittogrammiRepository extends JpaRepository<EtichettaPittogrammi, Long>, JpaSpecificationExecutor<EtichettaPittogrammi> {

}
