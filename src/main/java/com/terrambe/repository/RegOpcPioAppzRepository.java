package com.terrambe.repository;
import com.terrambe.domain.RegOpcPioAppz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RegOpcPioAppz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegOpcPioAppzRepository extends JpaRepository<RegOpcPioAppz, Long>, JpaSpecificationExecutor<RegOpcPioAppz> {

}
