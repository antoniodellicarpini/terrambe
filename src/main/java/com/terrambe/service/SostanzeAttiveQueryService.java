package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.SostanzeAttive;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.SostanzeAttiveRepository;
import com.terrambe.service.dto.SostanzeAttiveCriteria;

/**
 * Service for executing complex queries for {@link SostanzeAttive} entities in the database.
 * The main input is a {@link SostanzeAttiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SostanzeAttive} or a {@link Page} of {@link SostanzeAttive} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SostanzeAttiveQueryService extends QueryService<SostanzeAttive> {

    private final Logger log = LoggerFactory.getLogger(SostanzeAttiveQueryService.class);

    private final SostanzeAttiveRepository sostanzeAttiveRepository;

    public SostanzeAttiveQueryService(SostanzeAttiveRepository sostanzeAttiveRepository) {
        this.sostanzeAttiveRepository = sostanzeAttiveRepository;
    }

    /**
     * Return a {@link List} of {@link SostanzeAttive} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SostanzeAttive> findByCriteria(SostanzeAttiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SostanzeAttive> specification = createSpecification(criteria);
        return sostanzeAttiveRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link SostanzeAttive} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SostanzeAttive> findByCriteria(SostanzeAttiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SostanzeAttive> specification = createSpecification(criteria);
        return sostanzeAttiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SostanzeAttiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SostanzeAttive> specification = createSpecification(criteria);
        return sostanzeAttiveRepository.count(specification);
    }

    /**
     * Function to convert {@link SostanzeAttiveCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<SostanzeAttive> createSpecification(SostanzeAttiveCriteria criteria) {
        Specification<SostanzeAttive> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SostanzeAttive_.id));
            }
            if (criteria.getIdSostanza() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIdSostanza(), SostanzeAttive_.idSostanza));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), SostanzeAttive_.descrizione));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), SostanzeAttive_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), SostanzeAttive_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), SostanzeAttive_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), SostanzeAttive_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), SostanzeAttive_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), SostanzeAttive_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), SostanzeAttive_.userIdLastMod));
            }
            if (criteria.getSostAttToIncompId() != null) {
                specification = specification.and(buildSpecification(criteria.getSostAttToIncompId(),
                    root -> root.join(SostanzeAttive_.sostAttToIncomps, JoinType.LEFT).get(IncompatibilitaSA_.id)));
            }
            if (criteria.getSostAttToEtiSostAttId() != null) {
                specification = specification.and(buildSpecification(criteria.getSostAttToEtiSostAttId(),
                    root -> root.join(SostanzeAttive_.sostAttToEtiSostAtts, JoinType.LEFT).get(EtichettaSostanzeAttive_.id)));
            }
        }
        return specification;
    }
}
