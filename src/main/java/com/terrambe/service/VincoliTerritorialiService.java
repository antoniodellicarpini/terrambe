package com.terrambe.service;

import com.terrambe.domain.VincoliTerritoriali;
import com.terrambe.repository.VincoliTerritorialiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link VincoliTerritoriali}.
 */
@Service
@Transactional
public class VincoliTerritorialiService {

    private final Logger log = LoggerFactory.getLogger(VincoliTerritorialiService.class);

    private final VincoliTerritorialiRepository vincoliTerritorialiRepository;

    public VincoliTerritorialiService(VincoliTerritorialiRepository vincoliTerritorialiRepository) {
        this.vincoliTerritorialiRepository = vincoliTerritorialiRepository;
    }

    /**
     * Save a vincoliTerritoriali.
     *
     * @param vincoliTerritoriali the entity to save.
     * @return the persisted entity.
     */
    public VincoliTerritoriali save(VincoliTerritoriali vincoliTerritoriali) {
        log.debug("Request to save VincoliTerritoriali : {}", vincoliTerritoriali);
        return vincoliTerritorialiRepository.save(vincoliTerritoriali);
    }

    /**
     * Get all the vincoliTerritorialis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<VincoliTerritoriali> findAll() {
        log.debug("Request to get all VincoliTerritorialis");
        return vincoliTerritorialiRepository.findAll();
    }


    /**
     * Get one vincoliTerritoriali by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VincoliTerritoriali> findOne(Long id) {
        log.debug("Request to get VincoliTerritoriali : {}", id);
        return vincoliTerritorialiRepository.findById(id);
    }

    /**
     * Delete the vincoliTerritoriali by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete VincoliTerritoriali : {}", id);
        vincoliTerritorialiRepository.deleteById(id);
    }
}
