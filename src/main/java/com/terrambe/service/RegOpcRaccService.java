package com.terrambe.service;

import com.terrambe.domain.RegOpcRacc;
import com.terrambe.repository.RegOpcRaccRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcRacc}.
 */
@Service
@Transactional
public class RegOpcRaccService {

    private final Logger log = LoggerFactory.getLogger(RegOpcRaccService.class);

    private final RegOpcRaccRepository regOpcRaccRepository;

    public RegOpcRaccService(RegOpcRaccRepository regOpcRaccRepository) {
        this.regOpcRaccRepository = regOpcRaccRepository;
    }

    /**
     * Save a regOpcRacc.
     *
     * @param regOpcRacc the entity to save.
     * @return the persisted entity.
     */
    public RegOpcRacc save(RegOpcRacc regOpcRacc) {
        log.debug("Request to save RegOpcRacc : {}", regOpcRacc);
        return regOpcRaccRepository.save(regOpcRacc);
    }

    /**
     * Get all the regOpcRaccs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcRacc> findAll() {
        log.debug("Request to get all RegOpcRaccs");
        return regOpcRaccRepository.findAll();
    }


    /**
     * Get one regOpcRacc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcRacc> findOne(Long id) {
        log.debug("Request to get RegOpcRacc : {}", id);
        return regOpcRaccRepository.findById(id);
    }

    /**
     * Delete the regOpcRacc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcRacc : {}", id);
        regOpcRaccRepository.deleteById(id);
    }
}
