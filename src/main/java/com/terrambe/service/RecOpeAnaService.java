package com.terrambe.service;

import com.terrambe.domain.RecOpeAna;
import com.terrambe.repository.RecOpeAnaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecOpeAna}.
 */
@Service
@Transactional
public class RecOpeAnaService {

    private final Logger log = LoggerFactory.getLogger(RecOpeAnaService.class);

    private final RecOpeAnaRepository recOpeAnaRepository;

    public RecOpeAnaService(RecOpeAnaRepository recOpeAnaRepository) {
        this.recOpeAnaRepository = recOpeAnaRepository;
    }

    /**
     * Save a recOpeAna.
     *
     * @param recOpeAna the entity to save.
     * @return the persisted entity.
     */
    public RecOpeAna save(RecOpeAna recOpeAna) {
        log.debug("Request to save RecOpeAna : {}", recOpeAna);
        return recOpeAnaRepository.save(recOpeAna);
    }

    /**
     * Get all the recOpeAnas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecOpeAna> findAll() {
        log.debug("Request to get all RecOpeAnas");
        return recOpeAnaRepository.findAll();
    }


    /**
     * Get one recOpeAna by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecOpeAna> findOne(Long id) {
        log.debug("Request to get RecOpeAna : {}", id);
        return recOpeAnaRepository.findById(id);
    }

    /**
     * Delete the recOpeAna by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecOpeAna : {}", id);
        recOpeAnaRepository.deleteById(id);
    }
}
