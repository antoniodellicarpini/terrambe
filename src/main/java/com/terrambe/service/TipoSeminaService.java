package com.terrambe.service;

import com.terrambe.domain.TipoSemina;
import com.terrambe.repository.TipoSeminaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoSemina}.
 */
@Service
@Transactional
public class TipoSeminaService {

    private final Logger log = LoggerFactory.getLogger(TipoSeminaService.class);

    private final TipoSeminaRepository tipoSeminaRepository;

    public TipoSeminaService(TipoSeminaRepository tipoSeminaRepository) {
        this.tipoSeminaRepository = tipoSeminaRepository;
    }

    /**
     * Save a tipoSemina.
     *
     * @param tipoSemina the entity to save.
     * @return the persisted entity.
     */
    public TipoSemina save(TipoSemina tipoSemina) {
        log.debug("Request to save TipoSemina : {}", tipoSemina);
        return tipoSeminaRepository.save(tipoSemina);
    }

    /**
     * Get all the tipoSeminas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoSemina> findAll() {
        log.debug("Request to get all TipoSeminas");
        return tipoSeminaRepository.findAll();
    }


    /**
     * Get one tipoSemina by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoSemina> findOne(Long id) {
        log.debug("Request to get TipoSemina : {}", id);
        return tipoSeminaRepository.findById(id);
    }

    /**
     * Delete the tipoSemina by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoSemina : {}", id);
        tipoSeminaRepository.deleteById(id);
    }
}
