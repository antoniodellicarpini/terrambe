package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.NoteAggiuntiveEtichetta;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.NoteAggiuntiveEtichettaRepository;
import com.terrambe.service.dto.NoteAggiuntiveEtichettaCriteria;

/**
 * Service for executing complex queries for {@link NoteAggiuntiveEtichetta} entities in the database.
 * The main input is a {@link NoteAggiuntiveEtichettaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NoteAggiuntiveEtichetta} or a {@link Page} of {@link NoteAggiuntiveEtichetta} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NoteAggiuntiveEtichettaQueryService extends QueryService<NoteAggiuntiveEtichetta> {

    private final Logger log = LoggerFactory.getLogger(NoteAggiuntiveEtichettaQueryService.class);

    private final NoteAggiuntiveEtichettaRepository noteAggiuntiveEtichettaRepository;

    public NoteAggiuntiveEtichettaQueryService(NoteAggiuntiveEtichettaRepository noteAggiuntiveEtichettaRepository) {
        this.noteAggiuntiveEtichettaRepository = noteAggiuntiveEtichettaRepository;
    }

    /**
     * Return a {@link List} of {@link NoteAggiuntiveEtichetta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NoteAggiuntiveEtichetta> findByCriteria(NoteAggiuntiveEtichettaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NoteAggiuntiveEtichetta> specification = createSpecification(criteria);
        return noteAggiuntiveEtichettaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link NoteAggiuntiveEtichetta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NoteAggiuntiveEtichetta> findByCriteria(NoteAggiuntiveEtichettaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NoteAggiuntiveEtichetta> specification = createSpecification(criteria);
        return noteAggiuntiveEtichettaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NoteAggiuntiveEtichettaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NoteAggiuntiveEtichetta> specification = createSpecification(criteria);
        return noteAggiuntiveEtichettaRepository.count(specification);
    }

    /**
     * Function to convert {@link NoteAggiuntiveEtichettaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NoteAggiuntiveEtichetta> createSpecification(NoteAggiuntiveEtichettaCriteria criteria) {
        Specification<NoteAggiuntiveEtichetta> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NoteAggiuntiveEtichetta_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), NoteAggiuntiveEtichetta_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), NoteAggiuntiveEtichetta_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), NoteAggiuntiveEtichetta_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), NoteAggiuntiveEtichetta_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), NoteAggiuntiveEtichetta_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), NoteAggiuntiveEtichetta_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), NoteAggiuntiveEtichetta_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(NoteAggiuntiveEtichetta_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
        }
        return specification;
    }
}
