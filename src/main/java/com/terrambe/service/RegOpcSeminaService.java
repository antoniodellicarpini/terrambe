package com.terrambe.service;

import com.terrambe.domain.RegOpcSemina;
import com.terrambe.repository.RegOpcSeminaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcSemina}.
 */
@Service
@Transactional
public class RegOpcSeminaService {

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaService.class);

    private final RegOpcSeminaRepository regOpcSeminaRepository;

    public RegOpcSeminaService(RegOpcSeminaRepository regOpcSeminaRepository) {
        this.regOpcSeminaRepository = regOpcSeminaRepository;
    }

    /**
     * Save a regOpcSemina.
     *
     * @param regOpcSemina the entity to save.
     * @return the persisted entity.
     */
    public RegOpcSemina save(RegOpcSemina regOpcSemina) {
        log.debug("Request to save RegOpcSemina : {}", regOpcSemina);
        return regOpcSeminaRepository.save(regOpcSemina);
    }

    /**
     * Get all the regOpcSeminas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcSemina> findAll() {
        log.debug("Request to get all RegOpcSeminas");
        return regOpcSeminaRepository.findAll();
    }


    /**
     * Get one regOpcSemina by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcSemina> findOne(Long id) {
        log.debug("Request to get RegOpcSemina : {}", id);
        return regOpcSeminaRepository.findById(id);
    }

    /**
     * Delete the regOpcSemina by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcSemina : {}", id);
        regOpcSeminaRepository.deleteById(id);
    }
}
