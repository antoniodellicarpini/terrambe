package com.terrambe.service;

import com.terrambe.domain.Brogliaccio;
import com.terrambe.repository.BrogliaccioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Brogliaccio}.
 */
@Service
@Transactional
public class BrogliaccioService {

    private final Logger log = LoggerFactory.getLogger(BrogliaccioService.class);

    private final BrogliaccioRepository brogliaccioRepository;

    public BrogliaccioService(BrogliaccioRepository brogliaccioRepository) {
        this.brogliaccioRepository = brogliaccioRepository;
    }

    /**
     * Save a brogliaccio.
     *
     * @param brogliaccio the entity to save.
     * @return the persisted entity.
     */
    public Brogliaccio save(Brogliaccio brogliaccio) {
        log.debug("Request to save Brogliaccio : {}", brogliaccio);
        return brogliaccioRepository.save(brogliaccio);
    }

    /**
     * Get all the brogliaccios.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Brogliaccio> findAll() {
        log.debug("Request to get all Brogliaccios");
        return brogliaccioRepository.findAll();
    }


    /**
     * Get one brogliaccio by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Brogliaccio> findOne(Long id) {
        log.debug("Request to get Brogliaccio : {}", id);
        return brogliaccioRepository.findById(id);
    }

    /**
     * Delete the brogliaccio by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Brogliaccio : {}", id);
        brogliaccioRepository.deleteById(id);
    }
}
