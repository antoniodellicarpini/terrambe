package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipologieOpc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipologieOpcRepository;
import com.terrambe.service.dto.TipologieOpcCriteria;

/**
 * Service for executing complex queries for {@link TipologieOpc} entities in the database.
 * The main input is a {@link TipologieOpcCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipologieOpc} or a {@link Page} of {@link TipologieOpc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipologieOpcQueryService extends QueryService<TipologieOpc> {

    private final Logger log = LoggerFactory.getLogger(TipologieOpcQueryService.class);

    private final TipologieOpcRepository tipologieOpcRepository;

    public TipologieOpcQueryService(TipologieOpcRepository tipologieOpcRepository) {
        this.tipologieOpcRepository = tipologieOpcRepository;
    }

    /**
     * Return a {@link List} of {@link TipologieOpc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipologieOpc> findByCriteria(TipologieOpcCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipologieOpc> specification = createSpecification(criteria);
        return tipologieOpcRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipologieOpc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipologieOpc> findByCriteria(TipologieOpcCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipologieOpc> specification = createSpecification(criteria);
        return tipologieOpcRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipologieOpcCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipologieOpc> specification = createSpecification(criteria);
        return tipologieOpcRepository.count(specification);
    }

    /**
     * Function to convert {@link TipologieOpcCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipologieOpc> createSpecification(TipologieOpcCriteria criteria) {
        Specification<TipologieOpc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipologieOpc_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipologieOpc_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipologieOpc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipologieOpc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipologieOpc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipologieOpc_.userIdLastMod));
            }
        }
        return specification;
    }
}
