package com.terrambe.service;

import com.terrambe.domain.MotivoIrr;
import com.terrambe.repository.MotivoIrrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MotivoIrr}.
 */
@Service
@Transactional
public class MotivoIrrService {

    private final Logger log = LoggerFactory.getLogger(MotivoIrrService.class);

    private final MotivoIrrRepository motivoIrrRepository;

    public MotivoIrrService(MotivoIrrRepository motivoIrrRepository) {
        this.motivoIrrRepository = motivoIrrRepository;
    }

    /**
     * Save a motivoIrr.
     *
     * @param motivoIrr the entity to save.
     * @return the persisted entity.
     */
    public MotivoIrr save(MotivoIrr motivoIrr) {
        log.debug("Request to save MotivoIrr : {}", motivoIrr);
        return motivoIrrRepository.save(motivoIrr);
    }

    /**
     * Get all the motivoIrrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoIrr> findAll() {
        log.debug("Request to get all MotivoIrrs");
        return motivoIrrRepository.findAll();
    }


    /**
     * Get one motivoIrr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MotivoIrr> findOne(Long id) {
        log.debug("Request to get MotivoIrr : {}", id);
        return motivoIrrRepository.findById(id);
    }

    /**
     * Delete the motivoIrr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MotivoIrr : {}", id);
        motivoIrrRepository.deleteById(id);
    }
}
