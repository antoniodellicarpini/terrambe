package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.ModIrr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ModIrrRepository;
import com.terrambe.service.dto.ModIrrCriteria;

/**
 * Service for executing complex queries for {@link ModIrr} entities in the database.
 * The main input is a {@link ModIrrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ModIrr} or a {@link Page} of {@link ModIrr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ModIrrQueryService extends QueryService<ModIrr> {

    private final Logger log = LoggerFactory.getLogger(ModIrrQueryService.class);

    private final ModIrrRepository modIrrRepository;

    public ModIrrQueryService(ModIrrRepository modIrrRepository) {
        this.modIrrRepository = modIrrRepository;
    }

    /**
     * Return a {@link List} of {@link ModIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ModIrr> findByCriteria(ModIrrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ModIrr> specification = createSpecification(criteria);
        return modIrrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ModIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ModIrr> findByCriteria(ModIrrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ModIrr> specification = createSpecification(criteria);
        return modIrrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ModIrrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ModIrr> specification = createSpecification(criteria);
        return modIrrRepository.count(specification);
    }

    /**
     * Function to convert {@link ModIrrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ModIrr> createSpecification(ModIrrCriteria criteria) {
        Specification<ModIrr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ModIrr_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), ModIrr_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), ModIrr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), ModIrr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), ModIrr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), ModIrr_.userIdLastMod));
            }
        }
        return specification;
    }
}
