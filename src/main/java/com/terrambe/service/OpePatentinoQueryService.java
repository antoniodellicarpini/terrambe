package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.OpePatentino;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.OpePatentinoRepository;
import com.terrambe.service.dto.OpePatentinoCriteria;

/**
 * Service for executing complex queries for {@link OpePatentino} entities in the database.
 * The main input is a {@link OpePatentinoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OpePatentino} or a {@link Page} of {@link OpePatentino} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OpePatentinoQueryService extends QueryService<OpePatentino> {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoQueryService.class);

    private final OpePatentinoRepository opePatentinoRepository;

    public OpePatentinoQueryService(OpePatentinoRepository opePatentinoRepository) {
        this.opePatentinoRepository = opePatentinoRepository;
    }

    /**
     * Return a {@link List} of {@link OpePatentino} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OpePatentino> findByCriteria(OpePatentinoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OpePatentino> specification = createSpecification(criteria);
        return opePatentinoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OpePatentino} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OpePatentino> findByCriteria(OpePatentinoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OpePatentino> specification = createSpecification(criteria);
        return opePatentinoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OpePatentinoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OpePatentino> specification = createSpecification(criteria);
        return opePatentinoRepository.count(specification);
    }

    /**
     * Function to convert {@link OpePatentinoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OpePatentino> createSpecification(OpePatentinoCriteria criteria) {
        Specification<OpePatentino> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OpePatentino_.id));
            }
            if (criteria.getNumero() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumero(), OpePatentino_.numero));
            }
            if (criteria.getDataRilascio() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataRilascio(), OpePatentino_.dataRilascio));
            }
            if (criteria.getDataScadenza() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataScadenza(), OpePatentino_.dataScadenza));
            }
            if (criteria.getEnteRilasciato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnteRilasciato(), OpePatentino_.enteRilasciato));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), OpePatentino_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), OpePatentino_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), OpePatentino_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), OpePatentino_.userIdLastMod));
            }
            if (criteria.getPateToOpeAnagId() != null) {
                specification = specification.and(buildSpecification(criteria.getPateToOpeAnagId(),
                    root -> root.join(OpePatentino_.pateToOpeAnag, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
            if (criteria.getOpepatentinotipologiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpepatentinotipologiaId(),
                    root -> root.join(OpePatentino_.opepatentinotipologia, JoinType.LEFT).get(OpePatentinoTipologia_.id)));
            }
        }
        return specification;
    }
}
