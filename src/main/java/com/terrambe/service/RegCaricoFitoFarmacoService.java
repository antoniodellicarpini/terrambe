package com.terrambe.service;

import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.repository.RegCaricoFitoFarmacoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegCaricoFitoFarmaco}.
 */
@Service
@Transactional
public class RegCaricoFitoFarmacoService {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFitoFarmacoService.class);

    private final RegCaricoFitoFarmacoRepository regCaricoFitoFarmacoRepository;

    public RegCaricoFitoFarmacoService(RegCaricoFitoFarmacoRepository regCaricoFitoFarmacoRepository) {
        this.regCaricoFitoFarmacoRepository = regCaricoFitoFarmacoRepository;
    }

    /**
     * Save a regCaricoFitoFarmaco.
     *
     * @param regCaricoFitoFarmaco the entity to save.
     * @return the persisted entity.
     */
    public RegCaricoFitoFarmaco save(RegCaricoFitoFarmaco regCaricoFitoFarmaco) {
        log.debug("Request to save RegCaricoFitoFarmaco : {}", regCaricoFitoFarmaco);
        return regCaricoFitoFarmacoRepository.save(regCaricoFitoFarmaco);
    }

    /**
     * Get all the regCaricoFitoFarmacos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoFitoFarmaco> findAll() {
        log.debug("Request to get all RegCaricoFitoFarmacos");
        return regCaricoFitoFarmacoRepository.findAll();
    }


    /**
     * Get one regCaricoFitoFarmaco by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegCaricoFitoFarmaco> findOne(Long id) {
        log.debug("Request to get RegCaricoFitoFarmaco : {}", id);
        return regCaricoFitoFarmacoRepository.findById(id);
    }

    /**
     * Delete the regCaricoFitoFarmaco by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegCaricoFitoFarmaco : {}", id);
        regCaricoFitoFarmacoRepository.deleteById(id);
    }
}
