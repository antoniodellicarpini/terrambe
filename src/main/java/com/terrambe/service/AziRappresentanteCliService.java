package com.terrambe.service;

import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.repository.AziRappresentanteCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziRappresentanteCli}.
 */
@Service
@Transactional
public class AziRappresentanteCliService {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteCliService.class);

    private final AziRappresentanteCliRepository aziRappresentanteCliRepository;

    public AziRappresentanteCliService(AziRappresentanteCliRepository aziRappresentanteCliRepository) {
        this.aziRappresentanteCliRepository = aziRappresentanteCliRepository;
    }

    /**
     * Save a aziRappresentanteCli.
     *
     * @param aziRappresentanteCli the entity to save.
     * @return the persisted entity.
     */
    public AziRappresentanteCli save(AziRappresentanteCli aziRappresentanteCli) {
        log.debug("Request to save AziRappresentanteCli : {}", aziRappresentanteCli);
        return aziRappresentanteCliRepository.save(aziRappresentanteCli);
    }

    /**
     * Get all the aziRappresentanteClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziRappresentanteCli> findAll() {
        log.debug("Request to get all AziRappresentanteClis");
        return aziRappresentanteCliRepository.findAll();
    }


    /**
     * Get one aziRappresentanteCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziRappresentanteCli> findOne(Long id) {
        log.debug("Request to get AziRappresentanteCli : {}", id);
        return aziRappresentanteCliRepository.findById(id);
    }

    /**
     * Delete the aziRappresentanteCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziRappresentanteCli : {}", id);
        aziRappresentanteCliRepository.deleteById(id);
    }
}
