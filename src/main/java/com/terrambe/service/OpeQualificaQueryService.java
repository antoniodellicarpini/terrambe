package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.OpeQualifica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.OpeQualificaRepository;
import com.terrambe.service.dto.OpeQualificaCriteria;

/**
 * Service for executing complex queries for {@link OpeQualifica} entities in the database.
 * The main input is a {@link OpeQualificaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OpeQualifica} or a {@link Page} of {@link OpeQualifica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OpeQualificaQueryService extends QueryService<OpeQualifica> {

    private final Logger log = LoggerFactory.getLogger(OpeQualificaQueryService.class);

    private final OpeQualificaRepository opeQualificaRepository;

    public OpeQualificaQueryService(OpeQualificaRepository opeQualificaRepository) {
        this.opeQualificaRepository = opeQualificaRepository;
    }

    /**
     * Return a {@link List} of {@link OpeQualifica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OpeQualifica> findByCriteria(OpeQualificaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OpeQualifica> specification = createSpecification(criteria);
        return opeQualificaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OpeQualifica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OpeQualifica> findByCriteria(OpeQualificaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OpeQualifica> specification = createSpecification(criteria);
        return opeQualificaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OpeQualificaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OpeQualifica> specification = createSpecification(criteria);
        return opeQualificaRepository.count(specification);
    }

    /**
     * Function to convert {@link OpeQualificaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OpeQualifica> createSpecification(OpeQualificaCriteria criteria) {
        Specification<OpeQualifica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OpeQualifica_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), OpeQualifica_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), OpeQualifica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), OpeQualifica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), OpeQualifica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), OpeQualifica_.userIdLastMod));
            }
            if (criteria.getOpeQualToOpeAnagId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeQualToOpeAnagId(),
                    root -> root.join(OpeQualifica_.opeQualToOpeAnags, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
