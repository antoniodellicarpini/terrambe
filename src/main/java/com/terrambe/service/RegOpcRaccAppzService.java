package com.terrambe.service;

import com.terrambe.domain.RegOpcRaccAppz;
import com.terrambe.repository.RegOpcRaccAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcRaccAppz}.
 */
@Service
@Transactional
public class RegOpcRaccAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcRaccAppzService.class);

    private final RegOpcRaccAppzRepository regOpcRaccAppzRepository;

    public RegOpcRaccAppzService(RegOpcRaccAppzRepository regOpcRaccAppzRepository) {
        this.regOpcRaccAppzRepository = regOpcRaccAppzRepository;
    }

    /**
     * Save a regOpcRaccAppz.
     *
     * @param regOpcRaccAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcRaccAppz save(RegOpcRaccAppz regOpcRaccAppz) {
        log.debug("Request to save RegOpcRaccAppz : {}", regOpcRaccAppz);
        return regOpcRaccAppzRepository.save(regOpcRaccAppz);
    }

    /**
     * Get all the regOpcRaccAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcRaccAppz> findAll() {
        log.debug("Request to get all RegOpcRaccAppzs");
        return regOpcRaccAppzRepository.findAll();
    }


    /**
     * Get one regOpcRaccAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcRaccAppz> findOne(Long id) {
        log.debug("Request to get RegOpcRaccAppz : {}", id);
        return regOpcRaccAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcRaccAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcRaccAppz : {}", id);
        regOpcRaccAppzRepository.deleteById(id);
    }
}
