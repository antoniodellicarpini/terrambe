package com.terrambe.service;

import com.terrambe.domain.ModIrr;
import com.terrambe.repository.ModIrrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ModIrr}.
 */
@Service
@Transactional
public class ModIrrService {

    private final Logger log = LoggerFactory.getLogger(ModIrrService.class);

    private final ModIrrRepository modIrrRepository;

    public ModIrrService(ModIrrRepository modIrrRepository) {
        this.modIrrRepository = modIrrRepository;
    }

    /**
     * Save a modIrr.
     *
     * @param modIrr the entity to save.
     * @return the persisted entity.
     */
    public ModIrr save(ModIrr modIrr) {
        log.debug("Request to save ModIrr : {}", modIrr);
        return modIrrRepository.save(modIrr);
    }

    /**
     * Get all the modIrrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ModIrr> findAll() {
        log.debug("Request to get all ModIrrs");
        return modIrrRepository.findAll();
    }


    /**
     * Get one modIrr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ModIrr> findOne(Long id) {
        log.debug("Request to get ModIrr : {}", id);
        return modIrrRepository.findById(id);
    }

    /**
     * Delete the modIrr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ModIrr : {}", id);
        modIrrRepository.deleteById(id);
    }
}
