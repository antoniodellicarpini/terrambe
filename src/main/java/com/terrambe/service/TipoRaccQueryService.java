package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoRacc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoRaccRepository;
import com.terrambe.service.dto.TipoRaccCriteria;

/**
 * Service for executing complex queries for {@link TipoRacc} entities in the database.
 * The main input is a {@link TipoRaccCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoRacc} or a {@link Page} of {@link TipoRacc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoRaccQueryService extends QueryService<TipoRacc> {

    private final Logger log = LoggerFactory.getLogger(TipoRaccQueryService.class);

    private final TipoRaccRepository tipoRaccRepository;

    public TipoRaccQueryService(TipoRaccRepository tipoRaccRepository) {
        this.tipoRaccRepository = tipoRaccRepository;
    }

    /**
     * Return a {@link List} of {@link TipoRacc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoRacc> findByCriteria(TipoRaccCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoRacc> specification = createSpecification(criteria);
        return tipoRaccRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoRacc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoRacc> findByCriteria(TipoRaccCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoRacc> specification = createSpecification(criteria);
        return tipoRaccRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoRaccCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoRacc> specification = createSpecification(criteria);
        return tipoRaccRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoRaccCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoRacc> createSpecification(TipoRaccCriteria criteria) {
        Specification<TipoRacc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoRacc_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoRacc_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoRacc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoRacc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoRacc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoRacc_.userIdLastMod));
            }
            if (criteria.getTipoToOpcRaccId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoToOpcRaccId(),
                    root -> root.join(TipoRacc_.tipoToOpcRaccs, JoinType.LEFT).get(RegOpcRacc_.id)));
            }
        }
        return specification;
    }
}
