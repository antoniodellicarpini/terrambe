package com.terrambe.service;

import com.terrambe.domain.IndUniAna;
import com.terrambe.repository.IndUniAnaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndUniAna}.
 */
@Service
@Transactional
public class IndUniAnaService {

    private final Logger log = LoggerFactory.getLogger(IndUniAnaService.class);

    private final IndUniAnaRepository indUniAnaRepository;

    public IndUniAnaService(IndUniAnaRepository indUniAnaRepository) {
        this.indUniAnaRepository = indUniAnaRepository;
    }

    /**
     * Save a indUniAna.
     *
     * @param indUniAna the entity to save.
     * @return the persisted entity.
     */
    public IndUniAna save(IndUniAna indUniAna) {
        log.debug("Request to save IndUniAna : {}", indUniAna);
        return indUniAnaRepository.save(indUniAna);
    }

    /**
     * Get all the indUniAnas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndUniAna> findAll() {
        log.debug("Request to get all IndUniAnas");
        return indUniAnaRepository.findAll();
    }


    /**
     * Get one indUniAna by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndUniAna> findOne(Long id) {
        log.debug("Request to get IndUniAna : {}", id);
        return indUniAnaRepository.findById(id);
    }

    /**
     * Delete the indUniAna by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndUniAna : {}", id);
        indUniAnaRepository.deleteById(id);
    }
}
