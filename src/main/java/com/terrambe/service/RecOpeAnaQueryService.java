package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecOpeAna;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecOpeAnaRepository;
import com.terrambe.service.dto.RecOpeAnaCriteria;

/**
 * Service for executing complex queries for {@link RecOpeAna} entities in the database.
 * The main input is a {@link RecOpeAnaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecOpeAna} or a {@link Page} of {@link RecOpeAna} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecOpeAnaQueryService extends QueryService<RecOpeAna> {

    private final Logger log = LoggerFactory.getLogger(RecOpeAnaQueryService.class);

    private final RecOpeAnaRepository recOpeAnaRepository;

    public RecOpeAnaQueryService(RecOpeAnaRepository recOpeAnaRepository) {
        this.recOpeAnaRepository = recOpeAnaRepository;
    }

    /**
     * Return a {@link List} of {@link RecOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecOpeAna> findByCriteria(RecOpeAnaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecOpeAna> specification = createSpecification(criteria);
        return recOpeAnaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecOpeAna> findByCriteria(RecOpeAnaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecOpeAna> specification = createSpecification(criteria);
        return recOpeAnaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecOpeAnaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecOpeAna> specification = createSpecification(criteria);
        return recOpeAnaRepository.count(specification);
    }

    /**
     * Function to convert {@link RecOpeAnaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecOpeAna> createSpecification(RecOpeAnaCriteria criteria) {
        Specification<RecOpeAna> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecOpeAna_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecOpeAna_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecOpeAna_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecOpeAna_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecOpeAna_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecOpeAna_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecOpeAna_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecOpeAna_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecOpeAna_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecOpeAna_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecOpeAna_.userIdLastMod));
            }
            if (criteria.getRecToOpeAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToOpeAnaId(),
                    root -> root.join(RecOpeAna_.recToOpeAna, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
