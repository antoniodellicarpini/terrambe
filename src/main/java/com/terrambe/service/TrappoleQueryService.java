package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Trappole;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TrappoleRepository;
import com.terrambe.service.dto.TrappoleCriteria;

/**
 * Service for executing complex queries for {@link Trappole} entities in the database.
 * The main input is a {@link TrappoleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Trappole} or a {@link Page} of {@link Trappole} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TrappoleQueryService extends QueryService<Trappole> {

    private final Logger log = LoggerFactory.getLogger(TrappoleQueryService.class);

    private final TrappoleRepository trappoleRepository;

    public TrappoleQueryService(TrappoleRepository trappoleRepository) {
        this.trappoleRepository = trappoleRepository;
    }

    /**
     * Return a {@link List} of {@link Trappole} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Trappole> findByCriteria(TrappoleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Trappole> specification = createSpecification(criteria);
        return trappoleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Trappole} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Trappole> findByCriteria(TrappoleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Trappole> specification = createSpecification(criteria);
        return trappoleRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TrappoleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Trappole> specification = createSpecification(criteria);
        return trappoleRepository.count(specification);
    }

    /**
     * Function to convert {@link TrappoleCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Trappole> createSpecification(TrappoleCriteria criteria) {
        Specification<Trappole> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Trappole_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), Trappole_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), Trappole_.idUnitaProd));
            }
            if (criteria.getMarchio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMarchio(), Trappole_.marchio));
            }
            if (criteria.getNomeTrappola() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeTrappola(), Trappole_.nomeTrappola));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), Trappole_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Trappole_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Trappole_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Trappole_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Trappole_.userIdLastMod));
            }
            if (criteria.getTrapToTipoTrapId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrapToTipoTrapId(),
                    root -> root.join(Trappole_.trapToTipoTrap, JoinType.LEFT).get(TipoTrappola_.id)));
            }
        }
        return specification;
    }
}
