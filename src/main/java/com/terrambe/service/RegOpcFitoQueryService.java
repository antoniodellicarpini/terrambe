package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcFitoRepository;
import com.terrambe.service.dto.RegOpcFitoCriteria;

/**
 * Service for executing complex queries for {@link RegOpcFito} entities in the database.
 * The main input is a {@link RegOpcFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcFito} or a {@link Page} of {@link RegOpcFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcFitoQueryService extends QueryService<RegOpcFito> {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoQueryService.class);

    private final RegOpcFitoRepository regOpcFitoRepository;

    public RegOpcFitoQueryService(RegOpcFitoRepository regOpcFitoRepository) {
        this.regOpcFitoRepository = regOpcFitoRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcFito> findByCriteria(RegOpcFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcFito> specification = createSpecification(criteria);
        return regOpcFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcFito> findByCriteria(RegOpcFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcFito> specification = createSpecification(criteria);
        return regOpcFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcFito> specification = createSpecification(criteria);
        return regOpcFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcFito> createSpecification(RegOpcFitoCriteria criteria) {
        Specification<RegOpcFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcFito_.id));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcFito_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcFito_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcFito_.tempoImpiegato));
            }
            if (criteria.getVolumeAcqua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVolumeAcqua(), RegOpcFito_.volumeAcqua));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcFito_.userIdLastMod));
            }
            if (criteria.getOpcFitoToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcFitoToAppzId(),
                    root -> root.join(RegOpcFito_.opcFitoToAppzs, JoinType.LEFT).get(RegOpcFitoAppz_.id)));
            }
            if (criteria.getOpcFitoToProdId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcFitoToProdId(),
                    root -> root.join(RegOpcFito_.opcFitoToProds, JoinType.LEFT).get(RegOpcFitoProd_.id)));
            }
            if (criteria.getRegOpcFitoToDosTrattId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegOpcFitoToDosTrattId(),
                    root -> root.join(RegOpcFito_.regOpcFitoToDosTratt, JoinType.LEFT).get(DosaggioTrattamento_.id)));
            }
            if (criteria.getTrattFitoToMotivoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrattFitoToMotivoId(),
                    root -> root.join(RegOpcFito_.trattFitoToMotivo, JoinType.LEFT).get(MotivoTrattFito_.id)));
            }
        }
        return specification;
    }
}
