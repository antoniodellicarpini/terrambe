package com.terrambe.service;

import com.terrambe.domain.IndNascRapCli;
import com.terrambe.repository.IndNascRapCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link IndNascRapCli}.
 */
@Service
@Transactional
public class IndNascRapCliService {

    private final Logger log = LoggerFactory.getLogger(IndNascRapCliService.class);

    private final IndNascRapCliRepository indNascRapCliRepository;

    public IndNascRapCliService(IndNascRapCliRepository indNascRapCliRepository) {
        this.indNascRapCliRepository = indNascRapCliRepository;
    }

    /**
     * Save a indNascRapCli.
     *
     * @param indNascRapCli the entity to save.
     * @return the persisted entity.
     */
    public IndNascRapCli save(IndNascRapCli indNascRapCli) {
        log.debug("Request to save IndNascRapCli : {}", indNascRapCli);
        return indNascRapCliRepository.save(indNascRapCli);
    }

    /**
     * Get all the indNascRapClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascRapCli> findAll() {
        log.debug("Request to get all IndNascRapClis");
        return indNascRapCliRepository.findAll();
    }



    /**
    *  Get all the indNascRapClis where IndNascToCliRap is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<IndNascRapCli> findAllWhereIndNascToCliRapIsNull() {
        log.debug("Request to get all indNascRapClis where IndNascToCliRap is null");
        return StreamSupport
            .stream(indNascRapCliRepository.findAll().spliterator(), false)
            .filter(indNascRapCli -> indNascRapCli.getIndNascToCliRap() == null)
            .collect(Collectors.toList());
    }

    /**
     * Get one indNascRapCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndNascRapCli> findOne(Long id) {
        log.debug("Request to get IndNascRapCli : {}", id);
        return indNascRapCliRepository.findById(id);
    }

    /**
     * Delete the indNascRapCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndNascRapCli : {}", id);
        indNascRapCliRepository.deleteById(id);
    }
}
