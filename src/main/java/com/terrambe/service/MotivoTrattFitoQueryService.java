package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.MotivoTrattFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.MotivoTrattFitoRepository;
import com.terrambe.service.dto.MotivoTrattFitoCriteria;

/**
 * Service for executing complex queries for {@link MotivoTrattFito} entities in the database.
 * The main input is a {@link MotivoTrattFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MotivoTrattFito} or a {@link Page} of {@link MotivoTrattFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MotivoTrattFitoQueryService extends QueryService<MotivoTrattFito> {

    private final Logger log = LoggerFactory.getLogger(MotivoTrattFitoQueryService.class);

    private final MotivoTrattFitoRepository motivoTrattFitoRepository;

    public MotivoTrattFitoQueryService(MotivoTrattFitoRepository motivoTrattFitoRepository) {
        this.motivoTrattFitoRepository = motivoTrattFitoRepository;
    }

    /**
     * Return a {@link List} of {@link MotivoTrattFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoTrattFito> findByCriteria(MotivoTrattFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MotivoTrattFito> specification = createSpecification(criteria);
        return motivoTrattFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MotivoTrattFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MotivoTrattFito> findByCriteria(MotivoTrattFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MotivoTrattFito> specification = createSpecification(criteria);
        return motivoTrattFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MotivoTrattFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MotivoTrattFito> specification = createSpecification(criteria);
        return motivoTrattFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link MotivoTrattFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MotivoTrattFito> createSpecification(MotivoTrattFitoCriteria criteria) {
        Specification<MotivoTrattFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MotivoTrattFito_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), MotivoTrattFito_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), MotivoTrattFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), MotivoTrattFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), MotivoTrattFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), MotivoTrattFito_.userIdLastMod));
            }
            if (criteria.getMotivoToTrattFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getMotivoToTrattFitoId(),
                    root -> root.join(MotivoTrattFito_.motivoToTrattFitos, JoinType.LEFT).get(RegOpcFito_.id)));
            }
        }
        return specification;
    }
}
