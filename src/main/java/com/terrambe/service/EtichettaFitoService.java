package com.terrambe.service;

import com.terrambe.domain.EtichettaFito;
import com.terrambe.repository.EtichettaFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EtichettaFito}.
 */
@Service
@Transactional
public class EtichettaFitoService {

    private final Logger log = LoggerFactory.getLogger(EtichettaFitoService.class);

    private final EtichettaFitoRepository etichettaFitoRepository;

    public EtichettaFitoService(EtichettaFitoRepository etichettaFitoRepository) {
        this.etichettaFitoRepository = etichettaFitoRepository;
    }

    /**
     * Save a etichettaFito.
     *
     * @param etichettaFito the entity to save.
     * @return the persisted entity.
     */
    public EtichettaFito save(EtichettaFito etichettaFito) {
        log.debug("Request to save EtichettaFito : {}", etichettaFito);
        return etichettaFitoRepository.save(etichettaFito);
    }

    /**
     * Get all the etichettaFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaFito> findAll() {
        log.debug("Request to get all EtichettaFitos");
        return etichettaFitoRepository.findAll();
    }


    /**
     * Get one etichettaFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EtichettaFito> findOne(Long id) {
        log.debug("Request to get EtichettaFito : {}", id);
        return etichettaFitoRepository.findById(id);
    }

    /**
     * Delete the etichettaFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EtichettaFito : {}", id);
        etichettaFitoRepository.deleteById(id);
    }
}
