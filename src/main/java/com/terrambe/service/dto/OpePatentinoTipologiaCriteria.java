package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.OpePatentinoTipologia} entity. This class is used
 * in {@link com.terrambe.web.rest.OpePatentinoTipologiaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ope-patentino-tipologias?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OpePatentinoTipologiaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tpologia;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opePatTipToOpePateId;

    public OpePatentinoTipologiaCriteria(){
    }

    public OpePatentinoTipologiaCriteria(OpePatentinoTipologiaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tpologia = other.tpologia == null ? null : other.tpologia.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opePatTipToOpePateId = other.opePatTipToOpePateId == null ? null : other.opePatTipToOpePateId.copy();
    }

    @Override
    public OpePatentinoTipologiaCriteria copy() {
        return new OpePatentinoTipologiaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTpologia() {
        return tpologia;
    }

    public void setTpologia(StringFilter tpologia) {
        this.tpologia = tpologia;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpePatTipToOpePateId() {
        return opePatTipToOpePateId;
    }

    public void setOpePatTipToOpePateId(LongFilter opePatTipToOpePateId) {
        this.opePatTipToOpePateId = opePatTipToOpePateId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OpePatentinoTipologiaCriteria that = (OpePatentinoTipologiaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tpologia, that.tpologia) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opePatTipToOpePateId, that.opePatTipToOpePateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tpologia,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opePatTipToOpePateId
        );
    }

    @Override
    public String toString() {
        return "OpePatentinoTipologiaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tpologia != null ? "tpologia=" + tpologia + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opePatTipToOpePateId != null ? "opePatTipToOpePateId=" + opePatTipToOpePateId + ", " : "") +
            "}";
    }

}
