package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegCaricoFertilizzante} entity. This class is used
 * in {@link com.terrambe.web.rest.RegCaricoFertilizzanteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-carico-fertilizzantes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegCaricoFertilizzanteCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private DoubleFilter quantita;

    private FloatFilter prezzoUnitario;

    private LocalDateFilter dataCarico;

    private StringFilter ddt;

    private LocalDateFilter dataScadenza;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter magUbicazioneId;

    private LongFilter feranagraficaId;

    private LongFilter caricoFertToFornitoriId;

    private LongFilter regFertToTipoRegId;

    public RegCaricoFertilizzanteCriteria(){
    }

    public RegCaricoFertilizzanteCriteria(RegCaricoFertilizzanteCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.quantita = other.quantita == null ? null : other.quantita.copy();
        this.prezzoUnitario = other.prezzoUnitario == null ? null : other.prezzoUnitario.copy();
        this.dataCarico = other.dataCarico == null ? null : other.dataCarico.copy();
        this.ddt = other.ddt == null ? null : other.ddt.copy();
        this.dataScadenza = other.dataScadenza == null ? null : other.dataScadenza.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.magUbicazioneId = other.magUbicazioneId == null ? null : other.magUbicazioneId.copy();
        this.feranagraficaId = other.feranagraficaId == null ? null : other.feranagraficaId.copy();
        this.caricoFertToFornitoriId = other.caricoFertToFornitoriId == null ? null : other.caricoFertToFornitoriId.copy();
        this.regFertToTipoRegId = other.regFertToTipoRegId == null ? null : other.regFertToTipoRegId.copy();
    }

    @Override
    public RegCaricoFertilizzanteCriteria copy() {
        return new RegCaricoFertilizzanteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public DoubleFilter getQuantita() {
        return quantita;
    }

    public void setQuantita(DoubleFilter quantita) {
        this.quantita = quantita;
    }

    public FloatFilter getPrezzoUnitario() {
        return prezzoUnitario;
    }

    public void setPrezzoUnitario(FloatFilter prezzoUnitario) {
        this.prezzoUnitario = prezzoUnitario;
    }

    public LocalDateFilter getDataCarico() {
        return dataCarico;
    }

    public void setDataCarico(LocalDateFilter dataCarico) {
        this.dataCarico = dataCarico;
    }

    public StringFilter getDdt() {
        return ddt;
    }

    public void setDdt(StringFilter ddt) {
        this.ddt = ddt;
    }

    public LocalDateFilter getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(LocalDateFilter dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getMagUbicazioneId() {
        return magUbicazioneId;
    }

    public void setMagUbicazioneId(LongFilter magUbicazioneId) {
        this.magUbicazioneId = magUbicazioneId;
    }

    public LongFilter getFeranagraficaId() {
        return feranagraficaId;
    }

    public void setFeranagraficaId(LongFilter feranagraficaId) {
        this.feranagraficaId = feranagraficaId;
    }

    public LongFilter getCaricoFertToFornitoriId() {
        return caricoFertToFornitoriId;
    }

    public void setCaricoFertToFornitoriId(LongFilter caricoFertToFornitoriId) {
        this.caricoFertToFornitoriId = caricoFertToFornitoriId;
    }

    public LongFilter getRegFertToTipoRegId() {
        return regFertToTipoRegId;
    }

    public void setRegFertToTipoRegId(LongFilter regFertToTipoRegId) {
        this.regFertToTipoRegId = regFertToTipoRegId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegCaricoFertilizzanteCriteria that = (RegCaricoFertilizzanteCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(quantita, that.quantita) &&
            Objects.equals(prezzoUnitario, that.prezzoUnitario) &&
            Objects.equals(dataCarico, that.dataCarico) &&
            Objects.equals(ddt, that.ddt) &&
            Objects.equals(dataScadenza, that.dataScadenza) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(magUbicazioneId, that.magUbicazioneId) &&
            Objects.equals(feranagraficaId, that.feranagraficaId) &&
            Objects.equals(caricoFertToFornitoriId, that.caricoFertToFornitoriId) &&
            Objects.equals(regFertToTipoRegId, that.regFertToTipoRegId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        quantita,
        prezzoUnitario,
        dataCarico,
        ddt,
        dataScadenza,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        magUbicazioneId,
        feranagraficaId,
        caricoFertToFornitoriId,
        regFertToTipoRegId
        );
    }

    @Override
    public String toString() {
        return "RegCaricoFertilizzanteCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (quantita != null ? "quantita=" + quantita + ", " : "") +
                (prezzoUnitario != null ? "prezzoUnitario=" + prezzoUnitario + ", " : "") +
                (dataCarico != null ? "dataCarico=" + dataCarico + ", " : "") +
                (ddt != null ? "ddt=" + ddt + ", " : "") +
                (dataScadenza != null ? "dataScadenza=" + dataScadenza + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (magUbicazioneId != null ? "magUbicazioneId=" + magUbicazioneId + ", " : "") +
                (feranagraficaId != null ? "feranagraficaId=" + feranagraficaId + ", " : "") +
                (caricoFertToFornitoriId != null ? "caricoFertToFornitoriId=" + caricoFertToFornitoriId + ", " : "") +
                (regFertToTipoRegId != null ? "regFertToTipoRegId=" + regFertToTipoRegId + ", " : "") +
            "}";
    }

}
