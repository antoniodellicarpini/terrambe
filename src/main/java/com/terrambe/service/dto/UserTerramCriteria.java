package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.terrambe.domain.enumeration.TipologiaUtente;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.UserTerram} entity. This class is used
 * in {@link com.terrambe.web.rest.UserTerramResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-terrams?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserTerramCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TipologiaUtente
     */
    public static class TipologiaUtenteFilter extends Filter<TipologiaUtente> {

        public TipologiaUtenteFilter() {
        }

        public TipologiaUtenteFilter(TipologiaUtenteFilter filter) {
            super(filter);
        }

        @Override
        public TipologiaUtenteFilter copy() {
            return new TipologiaUtenteFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TipologiaUtenteFilter tipoUtente;

    private IntegerFilter idJuser;

    private LongFilter usrTerToAziAnaId;

    public UserTerramCriteria(){
    }

    public UserTerramCriteria(UserTerramCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tipoUtente = other.tipoUtente == null ? null : other.tipoUtente.copy();
        this.idJuser = other.idJuser == null ? null : other.idJuser.copy();
        this.usrTerToAziAnaId = other.usrTerToAziAnaId == null ? null : other.usrTerToAziAnaId.copy();
    }

    @Override
    public UserTerramCriteria copy() {
        return new UserTerramCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TipologiaUtenteFilter getTipoUtente() {
        return tipoUtente;
    }

    public void setTipoUtente(TipologiaUtenteFilter tipoUtente) {
        this.tipoUtente = tipoUtente;
    }

    public IntegerFilter getIdJuser() {
        return idJuser;
    }

    public void setIdJuser(IntegerFilter idJuser) {
        this.idJuser = idJuser;
    }

    public LongFilter getUsrTerToAziAnaId() {
        return usrTerToAziAnaId;
    }

    public void setUsrTerToAziAnaId(LongFilter usrTerToAziAnaId) {
        this.usrTerToAziAnaId = usrTerToAziAnaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserTerramCriteria that = (UserTerramCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tipoUtente, that.tipoUtente) &&
            Objects.equals(idJuser, that.idJuser) &&
            Objects.equals(usrTerToAziAnaId, that.usrTerToAziAnaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tipoUtente,
        idJuser,
        usrTerToAziAnaId
        );
    }

    @Override
    public String toString() {
        return "UserTerramCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tipoUtente != null ? "tipoUtente=" + tipoUtente + ", " : "") +
                (idJuser != null ? "idJuser=" + idJuser + ", " : "") +
                (usrTerToAziAnaId != null ? "usrTerToAziAnaId=" + usrTerToAziAnaId + ", " : "") +
            "}";
    }

}
