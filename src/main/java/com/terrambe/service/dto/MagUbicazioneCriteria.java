package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.MagUbicazione} entity. This class is used
 * in {@link com.terrambe.web.rest.MagUbicazioneResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /mag-ubicaziones?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MagUbicazioneCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private StringFilter descrizione;

    private LocalDateFilter dataInizioMag;

    private LocalDateFilter dataFineMag;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter magUbiToIndId;

    private LongFilter magUbiToRecId;

    private LongFilter regcaricotrappoleId;

    private LongFilter regcaricofitofarmacoId;

    private LongFilter regcaricofertilizzanteId;

    private LongFilter unianagraficaId;

    public MagUbicazioneCriteria(){
    }

    public MagUbicazioneCriteria(MagUbicazioneCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.descrizione = other.descrizione == null ? null : other.descrizione.copy();
        this.dataInizioMag = other.dataInizioMag == null ? null : other.dataInizioMag.copy();
        this.dataFineMag = other.dataFineMag == null ? null : other.dataFineMag.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.magUbiToIndId = other.magUbiToIndId == null ? null : other.magUbiToIndId.copy();
        this.magUbiToRecId = other.magUbiToRecId == null ? null : other.magUbiToRecId.copy();
        this.regcaricotrappoleId = other.regcaricotrappoleId == null ? null : other.regcaricotrappoleId.copy();
        this.regcaricofitofarmacoId = other.regcaricofitofarmacoId == null ? null : other.regcaricofitofarmacoId.copy();
        this.regcaricofertilizzanteId = other.regcaricofertilizzanteId == null ? null : other.regcaricofertilizzanteId.copy();
        this.unianagraficaId = other.unianagraficaId == null ? null : other.unianagraficaId.copy();
    }

    @Override
    public MagUbicazioneCriteria copy() {
        return new MagUbicazioneCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public StringFilter getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(StringFilter descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDateFilter getDataInizioMag() {
        return dataInizioMag;
    }

    public void setDataInizioMag(LocalDateFilter dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
    }

    public LocalDateFilter getDataFineMag() {
        return dataFineMag;
    }

    public void setDataFineMag(LocalDateFilter dataFineMag) {
        this.dataFineMag = dataFineMag;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getMagUbiToIndId() {
        return magUbiToIndId;
    }

    public void setMagUbiToIndId(LongFilter magUbiToIndId) {
        this.magUbiToIndId = magUbiToIndId;
    }

    public LongFilter getMagUbiToRecId() {
        return magUbiToRecId;
    }

    public void setMagUbiToRecId(LongFilter magUbiToRecId) {
        this.magUbiToRecId = magUbiToRecId;
    }

    public LongFilter getRegcaricotrappoleId() {
        return regcaricotrappoleId;
    }

    public void setRegcaricotrappoleId(LongFilter regcaricotrappoleId) {
        this.regcaricotrappoleId = regcaricotrappoleId;
    }

    public LongFilter getRegcaricofitofarmacoId() {
        return regcaricofitofarmacoId;
    }

    public void setRegcaricofitofarmacoId(LongFilter regcaricofitofarmacoId) {
        this.regcaricofitofarmacoId = regcaricofitofarmacoId;
    }

    public LongFilter getRegcaricofertilizzanteId() {
        return regcaricofertilizzanteId;
    }

    public void setRegcaricofertilizzanteId(LongFilter regcaricofertilizzanteId) {
        this.regcaricofertilizzanteId = regcaricofertilizzanteId;
    }

    public LongFilter getUnianagraficaId() {
        return unianagraficaId;
    }

    public void setUnianagraficaId(LongFilter unianagraficaId) {
        this.unianagraficaId = unianagraficaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MagUbicazioneCriteria that = (MagUbicazioneCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(descrizione, that.descrizione) &&
            Objects.equals(dataInizioMag, that.dataInizioMag) &&
            Objects.equals(dataFineMag, that.dataFineMag) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(magUbiToIndId, that.magUbiToIndId) &&
            Objects.equals(magUbiToRecId, that.magUbiToRecId) &&
            Objects.equals(regcaricotrappoleId, that.regcaricotrappoleId) &&
            Objects.equals(regcaricofitofarmacoId, that.regcaricofitofarmacoId) &&
            Objects.equals(regcaricofertilizzanteId, that.regcaricofertilizzanteId) &&
            Objects.equals(unianagraficaId, that.unianagraficaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        descrizione,
        dataInizioMag,
        dataFineMag,
        userIdCreator,
        userIdLastMod,
        magUbiToIndId,
        magUbiToRecId,
        regcaricotrappoleId,
        regcaricofitofarmacoId,
        regcaricofertilizzanteId,
        unianagraficaId
        );
    }

    @Override
    public String toString() {
        return "MagUbicazioneCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (descrizione != null ? "descrizione=" + descrizione + ", " : "") +
                (dataInizioMag != null ? "dataInizioMag=" + dataInizioMag + ", " : "") +
                (dataFineMag != null ? "dataFineMag=" + dataFineMag + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (magUbiToIndId != null ? "magUbiToIndId=" + magUbiToIndId + ", " : "") +
                (magUbiToRecId != null ? "magUbiToRecId=" + magUbiToRecId + ", " : "") +
                (regcaricotrappoleId != null ? "regcaricotrappoleId=" + regcaricotrappoleId + ", " : "") +
                (regcaricofitofarmacoId != null ? "regcaricofitofarmacoId=" + regcaricofitofarmacoId + ", " : "") +
                (regcaricofertilizzanteId != null ? "regcaricofertilizzanteId=" + regcaricofertilizzanteId + ", " : "") +
                (unianagraficaId != null ? "unianagraficaId=" + unianagraficaId + ", " : "") +
            "}";
    }

}
