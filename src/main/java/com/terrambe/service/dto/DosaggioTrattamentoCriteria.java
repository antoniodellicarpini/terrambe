package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.DosaggioTrattamento} entity. This class is used
 * in {@link com.terrambe.web.rest.DosaggioTrattamentoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dosaggio-trattamentos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DosaggioTrattamentoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter totaleProdottoUtilizzato;

    private LongFilter etichettafitoId;

    private LongFilter dosTrattToRegOpcFitoId;

    public DosaggioTrattamentoCriteria(){
    }

    public DosaggioTrattamentoCriteria(DosaggioTrattamentoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.totaleProdottoUtilizzato = other.totaleProdottoUtilizzato == null ? null : other.totaleProdottoUtilizzato.copy();
        this.etichettafitoId = other.etichettafitoId == null ? null : other.etichettafitoId.copy();
        this.dosTrattToRegOpcFitoId = other.dosTrattToRegOpcFitoId == null ? null : other.dosTrattToRegOpcFitoId.copy();
    }

    @Override
    public DosaggioTrattamentoCriteria copy() {
        return new DosaggioTrattamentoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getTotaleProdottoUtilizzato() {
        return totaleProdottoUtilizzato;
    }

    public void setTotaleProdottoUtilizzato(DoubleFilter totaleProdottoUtilizzato) {
        this.totaleProdottoUtilizzato = totaleProdottoUtilizzato;
    }

    public LongFilter getEtichettafitoId() {
        return etichettafitoId;
    }

    public void setEtichettafitoId(LongFilter etichettafitoId) {
        this.etichettafitoId = etichettafitoId;
    }

    public LongFilter getDosTrattToRegOpcFitoId() {
        return dosTrattToRegOpcFitoId;
    }

    public void setDosTrattToRegOpcFitoId(LongFilter dosTrattToRegOpcFitoId) {
        this.dosTrattToRegOpcFitoId = dosTrattToRegOpcFitoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DosaggioTrattamentoCriteria that = (DosaggioTrattamentoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(totaleProdottoUtilizzato, that.totaleProdottoUtilizzato) &&
            Objects.equals(etichettafitoId, that.etichettafitoId) &&
            Objects.equals(dosTrattToRegOpcFitoId, that.dosTrattToRegOpcFitoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        totaleProdottoUtilizzato,
        etichettafitoId,
        dosTrattToRegOpcFitoId
        );
    }

    @Override
    public String toString() {
        return "DosaggioTrattamentoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (totaleProdottoUtilizzato != null ? "totaleProdottoUtilizzato=" + totaleProdottoUtilizzato + ", " : "") +
                (etichettafitoId != null ? "etichettafitoId=" + etichettafitoId + ", " : "") +
                (dosTrattToRegOpcFitoId != null ? "dosTrattToRegOpcFitoId=" + dosTrattToRegOpcFitoId + ", " : "") +
            "}";
    }

}
