package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.PermessiOperatore} entity. This class is used
 * in {@link com.terrambe.web.rest.PermessiOperatoreResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /permessi-operatores?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PermessiOperatoreCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter flagModifica;

    private BooleanFilter flagVisualizza;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter permOperToTipoPermId;

    private LongFilter perOperToOpeAnagId;

    public PermessiOperatoreCriteria(){
    }

    public PermessiOperatoreCriteria(PermessiOperatoreCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.flagModifica = other.flagModifica == null ? null : other.flagModifica.copy();
        this.flagVisualizza = other.flagVisualizza == null ? null : other.flagVisualizza.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.permOperToTipoPermId = other.permOperToTipoPermId == null ? null : other.permOperToTipoPermId.copy();
        this.perOperToOpeAnagId = other.perOperToOpeAnagId == null ? null : other.perOperToOpeAnagId.copy();
    }

    @Override
    public PermessiOperatoreCriteria copy() {
        return new PermessiOperatoreCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getFlagModifica() {
        return flagModifica;
    }

    public void setFlagModifica(BooleanFilter flagModifica) {
        this.flagModifica = flagModifica;
    }

    public BooleanFilter getFlagVisualizza() {
        return flagVisualizza;
    }

    public void setFlagVisualizza(BooleanFilter flagVisualizza) {
        this.flagVisualizza = flagVisualizza;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getPermOperToTipoPermId() {
        return permOperToTipoPermId;
    }

    public void setPermOperToTipoPermId(LongFilter permOperToTipoPermId) {
        this.permOperToTipoPermId = permOperToTipoPermId;
    }

    public LongFilter getPerOperToOpeAnagId() {
        return perOperToOpeAnagId;
    }

    public void setPerOperToOpeAnagId(LongFilter perOperToOpeAnagId) {
        this.perOperToOpeAnagId = perOperToOpeAnagId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PermessiOperatoreCriteria that = (PermessiOperatoreCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(flagModifica, that.flagModifica) &&
            Objects.equals(flagVisualizza, that.flagVisualizza) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(permOperToTipoPermId, that.permOperToTipoPermId) &&
            Objects.equals(perOperToOpeAnagId, that.perOperToOpeAnagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        flagModifica,
        flagVisualizza,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        permOperToTipoPermId,
        perOperToOpeAnagId
        );
    }

    @Override
    public String toString() {
        return "PermessiOperatoreCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (flagModifica != null ? "flagModifica=" + flagModifica + ", " : "") +
                (flagVisualizza != null ? "flagVisualizza=" + flagVisualizza + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (permOperToTipoPermId != null ? "permOperToTipoPermId=" + permOperToTipoPermId + ", " : "") +
                (perOperToOpeAnagId != null ? "perOperToOpeAnagId=" + perOperToOpeAnagId + ", " : "") +
            "}";
    }

}
