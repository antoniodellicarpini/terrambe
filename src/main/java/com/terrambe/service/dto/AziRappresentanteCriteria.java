package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.AziRappresentante} entity. This class is used
 * in {@link com.terrambe.web.rest.AziRappresentanteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /azi-rappresentantes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AziRappresentanteCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private StringFilter cognome;

    private StringFilter sesso;

    private LocalDateFilter dataNascita;

    private StringFilter codiceFiscale;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter aziRapToIndNascId;

    private LongFilter aziRapToIndId;

    private LongFilter aziRapToRecId;

    private LongFilter aziRapToAziAnaId;

    public AziRappresentanteCriteria(){
    }

    public AziRappresentanteCriteria(AziRappresentanteCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.cognome = other.cognome == null ? null : other.cognome.copy();
        this.sesso = other.sesso == null ? null : other.sesso.copy();
        this.dataNascita = other.dataNascita == null ? null : other.dataNascita.copy();
        this.codiceFiscale = other.codiceFiscale == null ? null : other.codiceFiscale.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.aziRapToIndNascId = other.aziRapToIndNascId == null ? null : other.aziRapToIndNascId.copy();
        this.aziRapToIndId = other.aziRapToIndId == null ? null : other.aziRapToIndId.copy();
        this.aziRapToRecId = other.aziRapToRecId == null ? null : other.aziRapToRecId.copy();
        this.aziRapToAziAnaId = other.aziRapToAziAnaId == null ? null : other.aziRapToAziAnaId.copy();
    }

    @Override
    public AziRappresentanteCriteria copy() {
        return new AziRappresentanteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public StringFilter getCognome() {
        return cognome;
    }

    public void setCognome(StringFilter cognome) {
        this.cognome = cognome;
    }

    public StringFilter getSesso() {
        return sesso;
    }

    public void setSesso(StringFilter sesso) {
        this.sesso = sesso;
    }

    public LocalDateFilter getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(LocalDateFilter dataNascita) {
        this.dataNascita = dataNascita;
    }

    public StringFilter getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(StringFilter codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAziRapToIndNascId() {
        return aziRapToIndNascId;
    }

    public void setAziRapToIndNascId(LongFilter aziRapToIndNascId) {
        this.aziRapToIndNascId = aziRapToIndNascId;
    }

    public LongFilter getAziRapToIndId() {
        return aziRapToIndId;
    }

    public void setAziRapToIndId(LongFilter aziRapToIndId) {
        this.aziRapToIndId = aziRapToIndId;
    }

    public LongFilter getAziRapToRecId() {
        return aziRapToRecId;
    }

    public void setAziRapToRecId(LongFilter aziRapToRecId) {
        this.aziRapToRecId = aziRapToRecId;
    }

    public LongFilter getAziRapToAziAnaId() {
        return aziRapToAziAnaId;
    }

    public void setAziRapToAziAnaId(LongFilter aziRapToAziAnaId) {
        this.aziRapToAziAnaId = aziRapToAziAnaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AziRappresentanteCriteria that = (AziRappresentanteCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(cognome, that.cognome) &&
            Objects.equals(sesso, that.sesso) &&
            Objects.equals(dataNascita, that.dataNascita) &&
            Objects.equals(codiceFiscale, that.codiceFiscale) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(aziRapToIndNascId, that.aziRapToIndNascId) &&
            Objects.equals(aziRapToIndId, that.aziRapToIndId) &&
            Objects.equals(aziRapToRecId, that.aziRapToRecId) &&
            Objects.equals(aziRapToAziAnaId, that.aziRapToAziAnaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nome,
        cognome,
        sesso,
        dataNascita,
        codiceFiscale,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        aziRapToIndNascId,
        aziRapToIndId,
        aziRapToRecId,
        aziRapToAziAnaId
        );
    }

    @Override
    public String toString() {
        return "AziRappresentanteCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nome != null ? "nome=" + nome + ", " : "") +
                (cognome != null ? "cognome=" + cognome + ", " : "") +
                (sesso != null ? "sesso=" + sesso + ", " : "") +
                (dataNascita != null ? "dataNascita=" + dataNascita + ", " : "") +
                (codiceFiscale != null ? "codiceFiscale=" + codiceFiscale + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (aziRapToIndNascId != null ? "aziRapToIndNascId=" + aziRapToIndNascId + ", " : "") +
                (aziRapToIndId != null ? "aziRapToIndId=" + aziRapToIndId + ", " : "") +
                (aziRapToRecId != null ? "aziRapToRecId=" + aziRapToRecId + ", " : "") +
                (aziRapToAziAnaId != null ? "aziRapToAziAnaId=" + aziRapToAziAnaId + ", " : "") +
            "}";
    }

}
