package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcFitoProd} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcFitoProdResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-fito-prods?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcFitoProdCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private FloatFilter quantitaFito;

    private LongFilter idFitofarmaco;

    private LongFilter idMagazzino;

    private LongFilter idAvversita;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter prodToOpcFitoId;

    public RegOpcFitoProdCriteria(){
    }

    public RegOpcFitoProdCriteria(RegOpcFitoProdCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.quantitaFito = other.quantitaFito == null ? null : other.quantitaFito.copy();
        this.idFitofarmaco = other.idFitofarmaco == null ? null : other.idFitofarmaco.copy();
        this.idMagazzino = other.idMagazzino == null ? null : other.idMagazzino.copy();
        this.idAvversita = other.idAvversita == null ? null : other.idAvversita.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.prodToOpcFitoId = other.prodToOpcFitoId == null ? null : other.prodToOpcFitoId.copy();
    }

    @Override
    public RegOpcFitoProdCriteria copy() {
        return new RegOpcFitoProdCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public FloatFilter getQuantitaFito() {
        return quantitaFito;
    }

    public void setQuantitaFito(FloatFilter quantitaFito) {
        this.quantitaFito = quantitaFito;
    }

    public LongFilter getIdFitofarmaco() {
        return idFitofarmaco;
    }

    public void setIdFitofarmaco(LongFilter idFitofarmaco) {
        this.idFitofarmaco = idFitofarmaco;
    }

    public LongFilter getIdMagazzino() {
        return idMagazzino;
    }

    public void setIdMagazzino(LongFilter idMagazzino) {
        this.idMagazzino = idMagazzino;
    }

    public LongFilter getIdAvversita() {
        return idAvversita;
    }

    public void setIdAvversita(LongFilter idAvversita) {
        this.idAvversita = idAvversita;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getProdToOpcFitoId() {
        return prodToOpcFitoId;
    }

    public void setProdToOpcFitoId(LongFilter prodToOpcFitoId) {
        this.prodToOpcFitoId = prodToOpcFitoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcFitoProdCriteria that = (RegOpcFitoProdCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(quantitaFito, that.quantitaFito) &&
            Objects.equals(idFitofarmaco, that.idFitofarmaco) &&
            Objects.equals(idMagazzino, that.idMagazzino) &&
            Objects.equals(idAvversita, that.idAvversita) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(prodToOpcFitoId, that.prodToOpcFitoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        quantitaFito,
        idFitofarmaco,
        idMagazzino,
        idAvversita,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        prodToOpcFitoId
        );
    }

    @Override
    public String toString() {
        return "RegOpcFitoProdCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (quantitaFito != null ? "quantitaFito=" + quantitaFito + ", " : "") +
                (idFitofarmaco != null ? "idFitofarmaco=" + idFitofarmaco + ", " : "") +
                (idMagazzino != null ? "idMagazzino=" + idMagazzino + ", " : "") +
                (idAvversita != null ? "idAvversita=" + idAvversita + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (prodToOpcFitoId != null ? "prodToOpcFitoId=" + prodToOpcFitoId + ", " : "") +
            "}";
    }

}
