package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcTrapContr} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcTrapContrResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-trap-contrs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcTrapContrCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataControllo;

    private LocalDateFilter dataSostiFerom;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter trapContrToOpcTrapId;

    private LongFilter trapContrTipoToTrapContrId;

    public RegOpcTrapContrCriteria(){
    }

    public RegOpcTrapContrCriteria(RegOpcTrapContrCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataControllo = other.dataControllo == null ? null : other.dataControllo.copy();
        this.dataSostiFerom = other.dataSostiFerom == null ? null : other.dataSostiFerom.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.trapContrToOpcTrapId = other.trapContrToOpcTrapId == null ? null : other.trapContrToOpcTrapId.copy();
        this.trapContrTipoToTrapContrId = other.trapContrTipoToTrapContrId == null ? null : other.trapContrTipoToTrapContrId.copy();
    }

    @Override
    public RegOpcTrapContrCriteria copy() {
        return new RegOpcTrapContrCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataControllo() {
        return dataControllo;
    }

    public void setDataControllo(LocalDateFilter dataControllo) {
        this.dataControllo = dataControllo;
    }

    public LocalDateFilter getDataSostiFerom() {
        return dataSostiFerom;
    }

    public void setDataSostiFerom(LocalDateFilter dataSostiFerom) {
        this.dataSostiFerom = dataSostiFerom;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getTrapContrToOpcTrapId() {
        return trapContrToOpcTrapId;
    }

    public void setTrapContrToOpcTrapId(LongFilter trapContrToOpcTrapId) {
        this.trapContrToOpcTrapId = trapContrToOpcTrapId;
    }

    public LongFilter getTrapContrTipoToTrapContrId() {
        return trapContrTipoToTrapContrId;
    }

    public void setTrapContrTipoToTrapContrId(LongFilter trapContrTipoToTrapContrId) {
        this.trapContrTipoToTrapContrId = trapContrTipoToTrapContrId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcTrapContrCriteria that = (RegOpcTrapContrCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataControllo, that.dataControllo) &&
            Objects.equals(dataSostiFerom, that.dataSostiFerom) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(trapContrToOpcTrapId, that.trapContrToOpcTrapId) &&
            Objects.equals(trapContrTipoToTrapContrId, that.trapContrTipoToTrapContrId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        dataControllo,
        dataSostiFerom,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        trapContrToOpcTrapId,
        trapContrTipoToTrapContrId
        );
    }

    @Override
    public String toString() {
        return "RegOpcTrapContrCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataControllo != null ? "dataControllo=" + dataControllo + ", " : "") +
                (dataSostiFerom != null ? "dataSostiFerom=" + dataSostiFerom + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (trapContrToOpcTrapId != null ? "trapContrToOpcTrapId=" + trapContrToOpcTrapId + ", " : "") +
                (trapContrTipoToTrapContrId != null ? "trapContrTipoToTrapContrId=" + trapContrTipoToTrapContrId + ", " : "") +
            "}";
    }

}
