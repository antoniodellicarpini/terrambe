package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Fornitori} entity. This class is used
 * in {@link com.terrambe.web.rest.FornitoriResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fornitoris?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FornitoriCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter registroImprese;

    private StringFilter numIscrizione;

    private LongFilter idAzienda;

    private StringFilter nomeFornitore;

    private StringFilter piva;

    private StringFilter codiFisc;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter fornToIndId;

    private LongFilter fornToRecId;

    private LongFilter fornitoritipologiaId;

    private LongFilter fornitoriToCaricoFertId;

    private LongFilter fornitoriToCaricoFitoId;

    private LongFilter fornitoriToCaricoTrapId;

    private LongFilter fornToOpeAnaId;

    public FornitoriCriteria(){
    }

    public FornitoriCriteria(FornitoriCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.registroImprese = other.registroImprese == null ? null : other.registroImprese.copy();
        this.numIscrizione = other.numIscrizione == null ? null : other.numIscrizione.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.nomeFornitore = other.nomeFornitore == null ? null : other.nomeFornitore.copy();
        this.piva = other.piva == null ? null : other.piva.copy();
        this.codiFisc = other.codiFisc == null ? null : other.codiFisc.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.fornToIndId = other.fornToIndId == null ? null : other.fornToIndId.copy();
        this.fornToRecId = other.fornToRecId == null ? null : other.fornToRecId.copy();
        this.fornitoritipologiaId = other.fornitoritipologiaId == null ? null : other.fornitoritipologiaId.copy();
        this.fornitoriToCaricoFertId = other.fornitoriToCaricoFertId == null ? null : other.fornitoriToCaricoFertId.copy();
        this.fornitoriToCaricoFitoId = other.fornitoriToCaricoFitoId == null ? null : other.fornitoriToCaricoFitoId.copy();
        this.fornitoriToCaricoTrapId = other.fornitoriToCaricoTrapId == null ? null : other.fornitoriToCaricoTrapId.copy();
        this.fornToOpeAnaId = other.fornToOpeAnaId == null ? null : other.fornToOpeAnaId.copy();
    }

    @Override
    public FornitoriCriteria copy() {
        return new FornitoriCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getRegistroImprese() {
        return registroImprese;
    }

    public void setRegistroImprese(StringFilter registroImprese) {
        this.registroImprese = registroImprese;
    }

    public StringFilter getNumIscrizione() {
        return numIscrizione;
    }

    public void setNumIscrizione(StringFilter numIscrizione) {
        this.numIscrizione = numIscrizione;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public StringFilter getNomeFornitore() {
        return nomeFornitore;
    }

    public void setNomeFornitore(StringFilter nomeFornitore) {
        this.nomeFornitore = nomeFornitore;
    }

    public StringFilter getPiva() {
        return piva;
    }

    public void setPiva(StringFilter piva) {
        this.piva = piva;
    }

    public StringFilter getCodiFisc() {
        return codiFisc;
    }

    public void setCodiFisc(StringFilter codiFisc) {
        this.codiFisc = codiFisc;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getFornToIndId() {
        return fornToIndId;
    }

    public void setFornToIndId(LongFilter fornToIndId) {
        this.fornToIndId = fornToIndId;
    }

    public LongFilter getFornToRecId() {
        return fornToRecId;
    }

    public void setFornToRecId(LongFilter fornToRecId) {
        this.fornToRecId = fornToRecId;
    }

    public LongFilter getFornitoritipologiaId() {
        return fornitoritipologiaId;
    }

    public void setFornitoritipologiaId(LongFilter fornitoritipologiaId) {
        this.fornitoritipologiaId = fornitoritipologiaId;
    }

    public LongFilter getFornitoriToCaricoFertId() {
        return fornitoriToCaricoFertId;
    }

    public void setFornitoriToCaricoFertId(LongFilter fornitoriToCaricoFertId) {
        this.fornitoriToCaricoFertId = fornitoriToCaricoFertId;
    }

    public LongFilter getFornitoriToCaricoFitoId() {
        return fornitoriToCaricoFitoId;
    }

    public void setFornitoriToCaricoFitoId(LongFilter fornitoriToCaricoFitoId) {
        this.fornitoriToCaricoFitoId = fornitoriToCaricoFitoId;
    }

    public LongFilter getFornitoriToCaricoTrapId() {
        return fornitoriToCaricoTrapId;
    }

    public void setFornitoriToCaricoTrapId(LongFilter fornitoriToCaricoTrapId) {
        this.fornitoriToCaricoTrapId = fornitoriToCaricoTrapId;
    }

    public LongFilter getFornToOpeAnaId() {
        return fornToOpeAnaId;
    }

    public void setFornToOpeAnaId(LongFilter fornToOpeAnaId) {
        this.fornToOpeAnaId = fornToOpeAnaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FornitoriCriteria that = (FornitoriCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(registroImprese, that.registroImprese) &&
            Objects.equals(numIscrizione, that.numIscrizione) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(nomeFornitore, that.nomeFornitore) &&
            Objects.equals(piva, that.piva) &&
            Objects.equals(codiFisc, that.codiFisc) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(fornToIndId, that.fornToIndId) &&
            Objects.equals(fornToRecId, that.fornToRecId) &&
            Objects.equals(fornitoritipologiaId, that.fornitoritipologiaId) &&
            Objects.equals(fornitoriToCaricoFertId, that.fornitoriToCaricoFertId) &&
            Objects.equals(fornitoriToCaricoFitoId, that.fornitoriToCaricoFitoId) &&
            Objects.equals(fornitoriToCaricoTrapId, that.fornitoriToCaricoTrapId) &&
            Objects.equals(fornToOpeAnaId, that.fornToOpeAnaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        registroImprese,
        numIscrizione,
        idAzienda,
        nomeFornitore,
        piva,
        codiFisc,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        fornToIndId,
        fornToRecId,
        fornitoritipologiaId,
        fornitoriToCaricoFertId,
        fornitoriToCaricoFitoId,
        fornitoriToCaricoTrapId,
        fornToOpeAnaId
        );
    }

    @Override
    public String toString() {
        return "FornitoriCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (registroImprese != null ? "registroImprese=" + registroImprese + ", " : "") +
                (numIscrizione != null ? "numIscrizione=" + numIscrizione + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (nomeFornitore != null ? "nomeFornitore=" + nomeFornitore + ", " : "") +
                (piva != null ? "piva=" + piva + ", " : "") +
                (codiFisc != null ? "codiFisc=" + codiFisc + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (fornToIndId != null ? "fornToIndId=" + fornToIndId + ", " : "") +
                (fornToRecId != null ? "fornToRecId=" + fornToRecId + ", " : "") +
                (fornitoritipologiaId != null ? "fornitoritipologiaId=" + fornitoritipologiaId + ", " : "") +
                (fornitoriToCaricoFertId != null ? "fornitoriToCaricoFertId=" + fornitoriToCaricoFertId + ", " : "") +
                (fornitoriToCaricoFitoId != null ? "fornitoriToCaricoFitoId=" + fornitoriToCaricoFitoId + ", " : "") +
                (fornitoriToCaricoTrapId != null ? "fornitoriToCaricoTrapId=" + fornitoriToCaricoTrapId + ", " : "") +
                (fornToOpeAnaId != null ? "fornToOpeAnaId=" + fornToOpeAnaId + ", " : "") +
            "}";
    }

}
