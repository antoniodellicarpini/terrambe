package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.PartAppez} entity. This class is used
 * in {@link com.terrambe.web.rest.PartAppezResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /part-appezs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PartAppezCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter foglio;

    private StringFilter part;

    private StringFilter sub;

    private StringFilter sezione;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter partAppzToAppzId;

    private LongFilter partAppzToPartId;

    public PartAppezCriteria(){
    }

    public PartAppezCriteria(PartAppezCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.foglio = other.foglio == null ? null : other.foglio.copy();
        this.part = other.part == null ? null : other.part.copy();
        this.sub = other.sub == null ? null : other.sub.copy();
        this.sezione = other.sezione == null ? null : other.sezione.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.partAppzToAppzId = other.partAppzToAppzId == null ? null : other.partAppzToAppzId.copy();
        this.partAppzToPartId = other.partAppzToPartId == null ? null : other.partAppzToPartId.copy();
    }

    @Override
    public PartAppezCriteria copy() {
        return new PartAppezCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFoglio() {
        return foglio;
    }

    public void setFoglio(StringFilter foglio) {
        this.foglio = foglio;
    }

    public StringFilter getPart() {
        return part;
    }

    public void setPart(StringFilter part) {
        this.part = part;
    }

    public StringFilter getSub() {
        return sub;
    }

    public void setSub(StringFilter sub) {
        this.sub = sub;
    }

    public StringFilter getSezione() {
        return sezione;
    }

    public void setSezione(StringFilter sezione) {
        this.sezione = sezione;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getPartAppzToAppzId() {
        return partAppzToAppzId;
    }

    public void setPartAppzToAppzId(LongFilter partAppzToAppzId) {
        this.partAppzToAppzId = partAppzToAppzId;
    }

    public LongFilter getPartAppzToPartId() {
        return partAppzToPartId;
    }

    public void setPartAppzToPartId(LongFilter partAppzToPartId) {
        this.partAppzToPartId = partAppzToPartId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PartAppezCriteria that = (PartAppezCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(foglio, that.foglio) &&
            Objects.equals(part, that.part) &&
            Objects.equals(sub, that.sub) &&
            Objects.equals(sezione, that.sezione) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(partAppzToAppzId, that.partAppzToAppzId) &&
            Objects.equals(partAppzToPartId, that.partAppzToPartId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        foglio,
        part,
        sub,
        sezione,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        partAppzToAppzId,
        partAppzToPartId
        );
    }

    @Override
    public String toString() {
        return "PartAppezCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (foglio != null ? "foglio=" + foglio + ", " : "") +
                (part != null ? "part=" + part + ", " : "") +
                (sub != null ? "sub=" + sub + ", " : "") +
                (sezione != null ? "sezione=" + sezione + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (partAppzToAppzId != null ? "partAppzToAppzId=" + partAppzToAppzId + ", " : "") +
                (partAppzToPartId != null ? "partAppzToPartId=" + partAppzToPartId + ", " : "") +
            "}";
    }

}
