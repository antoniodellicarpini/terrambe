package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.UniAnagrafica} entity. This class is used
 * in {@link com.terrambe.web.rest.UniAnagraficaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /uni-anagraficas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UniAnagraficaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter descrizione;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter uniAnaToIndId;

    private LongFilter unianaazianaId;

    private LongFilter uniAnaToAtrAnaId;

    public UniAnagraficaCriteria(){
    }

    public UniAnagraficaCriteria(UniAnagraficaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.descrizione = other.descrizione == null ? null : other.descrizione.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.uniAnaToIndId = other.uniAnaToIndId == null ? null : other.uniAnaToIndId.copy();
        this.unianaazianaId = other.unianaazianaId == null ? null : other.unianaazianaId.copy();
        this.uniAnaToAtrAnaId = other.uniAnaToAtrAnaId == null ? null : other.uniAnaToAtrAnaId.copy();
    }

    @Override
    public UniAnagraficaCriteria copy() {
        return new UniAnagraficaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(StringFilter descrizione) {
        this.descrizione = descrizione;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getUniAnaToIndId() {
        return uniAnaToIndId;
    }

    public void setUniAnaToIndId(LongFilter uniAnaToIndId) {
        this.uniAnaToIndId = uniAnaToIndId;
    }

    public LongFilter getUnianaazianaId() {
        return unianaazianaId;
    }

    public void setUnianaazianaId(LongFilter unianaazianaId) {
        this.unianaazianaId = unianaazianaId;
    }

    public LongFilter getUniAnaToAtrAnaId() {
        return uniAnaToAtrAnaId;
    }

    public void setUniAnaToAtrAnaId(LongFilter uniAnaToAtrAnaId) {
        this.uniAnaToAtrAnaId = uniAnaToAtrAnaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UniAnagraficaCriteria that = (UniAnagraficaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(descrizione, that.descrizione) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(uniAnaToIndId, that.uniAnaToIndId) &&
            Objects.equals(unianaazianaId, that.unianaazianaId) &&
            Objects.equals(uniAnaToAtrAnaId, that.uniAnaToAtrAnaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        descrizione,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        uniAnaToIndId,
        unianaazianaId,
        uniAnaToAtrAnaId
        );
    }

    @Override
    public String toString() {
        return "UniAnagraficaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (descrizione != null ? "descrizione=" + descrizione + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (uniAnaToIndId != null ? "uniAnaToIndId=" + uniAnaToIndId + ", " : "") +
                (unianaazianaId != null ? "unianaazianaId=" + unianaazianaId + ", " : "") +
                (uniAnaToAtrAnaId != null ? "uniAnaToAtrAnaId=" + uniAnaToAtrAnaId + ", " : "") +
            "}";
    }

}
