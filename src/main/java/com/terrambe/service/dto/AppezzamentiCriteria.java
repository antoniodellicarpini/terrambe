package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Appezzamenti} entity. This class is used
 * in {@link com.terrambe.web.rest.AppezzamentiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /appezzamentis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AppezzamentiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private StringFilter nomeAppezzamento;

    private FloatFilter superficeHa;

    private BooleanFilter rotazione;

    private BooleanFilter serra;

    private BooleanFilter irrigabile;

    private StringFilter uuidEsri;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter appzToPartId;

    private LongFilter appezToVincoliTerId;

    private LongFilter appezToCampiId;

    private LongFilter appzToBioAppzTipoId;

    private LongFilter appeToColtId;

    public AppezzamentiCriteria(){
    }

    public AppezzamentiCriteria(AppezzamentiCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.nomeAppezzamento = other.nomeAppezzamento == null ? null : other.nomeAppezzamento.copy();
        this.superficeHa = other.superficeHa == null ? null : other.superficeHa.copy();
        this.rotazione = other.rotazione == null ? null : other.rotazione.copy();
        this.serra = other.serra == null ? null : other.serra.copy();
        this.irrigabile = other.irrigabile == null ? null : other.irrigabile.copy();
        this.uuidEsri = other.uuidEsri == null ? null : other.uuidEsri.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.appzToPartId = other.appzToPartId == null ? null : other.appzToPartId.copy();
        this.appezToVincoliTerId = other.appezToVincoliTerId == null ? null : other.appezToVincoliTerId.copy();
        this.appezToCampiId = other.appezToCampiId == null ? null : other.appezToCampiId.copy();
        this.appzToBioAppzTipoId = other.appzToBioAppzTipoId == null ? null : other.appzToBioAppzTipoId.copy();
        this.appeToColtId = other.appeToColtId == null ? null : other.appeToColtId.copy();
    }

    @Override
    public AppezzamentiCriteria copy() {
        return new AppezzamentiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public StringFilter getNomeAppezzamento() {
        return nomeAppezzamento;
    }

    public void setNomeAppezzamento(StringFilter nomeAppezzamento) {
        this.nomeAppezzamento = nomeAppezzamento;
    }

    public FloatFilter getSuperficeHa() {
        return superficeHa;
    }

    public void setSuperficeHa(FloatFilter superficeHa) {
        this.superficeHa = superficeHa;
    }

    public BooleanFilter getRotazione() {
        return rotazione;
    }

    public void setRotazione(BooleanFilter rotazione) {
        this.rotazione = rotazione;
    }

    public BooleanFilter getSerra() {
        return serra;
    }

    public void setSerra(BooleanFilter serra) {
        this.serra = serra;
    }

    public BooleanFilter getIrrigabile() {
        return irrigabile;
    }

    public void setIrrigabile(BooleanFilter irrigabile) {
        this.irrigabile = irrigabile;
    }

    public StringFilter getUuidEsri() {
        return uuidEsri;
    }

    public void setUuidEsri(StringFilter uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAppzToPartId() {
        return appzToPartId;
    }

    public void setAppzToPartId(LongFilter appzToPartId) {
        this.appzToPartId = appzToPartId;
    }

    public LongFilter getAppezToVincoliTerId() {
        return appezToVincoliTerId;
    }

    public void setAppezToVincoliTerId(LongFilter appezToVincoliTerId) {
        this.appezToVincoliTerId = appezToVincoliTerId;
    }

    public LongFilter getAppezToCampiId() {
        return appezToCampiId;
    }

    public void setAppezToCampiId(LongFilter appezToCampiId) {
        this.appezToCampiId = appezToCampiId;
    }

    public LongFilter getAppzToBioAppzTipoId() {
        return appzToBioAppzTipoId;
    }

    public void setAppzToBioAppzTipoId(LongFilter appzToBioAppzTipoId) {
        this.appzToBioAppzTipoId = appzToBioAppzTipoId;
    }

    public LongFilter getAppeToColtId() {
        return appeToColtId;
    }

    public void setAppeToColtId(LongFilter appeToColtId) {
        this.appeToColtId = appeToColtId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AppezzamentiCriteria that = (AppezzamentiCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(nomeAppezzamento, that.nomeAppezzamento) &&
            Objects.equals(superficeHa, that.superficeHa) &&
            Objects.equals(rotazione, that.rotazione) &&
            Objects.equals(serra, that.serra) &&
            Objects.equals(irrigabile, that.irrigabile) &&
            Objects.equals(uuidEsri, that.uuidEsri) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(appzToPartId, that.appzToPartId) &&
            Objects.equals(appezToVincoliTerId, that.appezToVincoliTerId) &&
            Objects.equals(appezToCampiId, that.appezToCampiId) &&
            Objects.equals(appzToBioAppzTipoId, that.appzToBioAppzTipoId) &&
            Objects.equals(appeToColtId, that.appeToColtId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        nomeAppezzamento,
        superficeHa,
        rotazione,
        serra,
        irrigabile,
        uuidEsri,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        appzToPartId,
        appezToVincoliTerId,
        appezToCampiId,
        appzToBioAppzTipoId,
        appeToColtId
        );
    }

    @Override
    public String toString() {
        return "AppezzamentiCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (nomeAppezzamento != null ? "nomeAppezzamento=" + nomeAppezzamento + ", " : "") +
                (superficeHa != null ? "superficeHa=" + superficeHa + ", " : "") +
                (rotazione != null ? "rotazione=" + rotazione + ", " : "") +
                (serra != null ? "serra=" + serra + ", " : "") +
                (irrigabile != null ? "irrigabile=" + irrigabile + ", " : "") +
                (uuidEsri != null ? "uuidEsri=" + uuidEsri + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (appzToPartId != null ? "appzToPartId=" + appzToPartId + ", " : "") +
                (appezToVincoliTerId != null ? "appezToVincoliTerId=" + appezToVincoliTerId + ", " : "") +
                (appezToCampiId != null ? "appezToCampiId=" + appezToCampiId + ", " : "") +
                (appzToBioAppzTipoId != null ? "appzToBioAppzTipoId=" + appzToBioAppzTipoId + ", " : "") +
                (appeToColtId != null ? "appeToColtId=" + appeToColtId + ", " : "") +
            "}";
    }

}
