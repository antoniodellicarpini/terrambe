package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Comuni} entity. This class is used
 * in {@link com.terrambe.web.rest.ComuniResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /comunis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ComuniCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter codiProv;

    private StringFilter codiComu;

    private StringFilter descComu;

    private StringFilter descProv;

    private StringFilter codiSiglProv;

    private StringFilter codiFiscLuna;

    private StringFilter descRegi;

    private StringFilter codiceBelfiore;

    private LocalDateFilter dataInizio;

    private LocalDateFilter dataFine;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    public ComuniCriteria(){
    }

    public ComuniCriteria(ComuniCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.codiProv = other.codiProv == null ? null : other.codiProv.copy();
        this.codiComu = other.codiComu == null ? null : other.codiComu.copy();
        this.descComu = other.descComu == null ? null : other.descComu.copy();
        this.descProv = other.descProv == null ? null : other.descProv.copy();
        this.codiSiglProv = other.codiSiglProv == null ? null : other.codiSiglProv.copy();
        this.codiFiscLuna = other.codiFiscLuna == null ? null : other.codiFiscLuna.copy();
        this.descRegi = other.descRegi == null ? null : other.descRegi.copy();
        this.codiceBelfiore = other.codiceBelfiore == null ? null : other.codiceBelfiore.copy();
        this.dataInizio = other.dataInizio == null ? null : other.dataInizio.copy();
        this.dataFine = other.dataFine == null ? null : other.dataFine.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
    }

    @Override
    public ComuniCriteria copy() {
        return new ComuniCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCodiProv() {
        return codiProv;
    }

    public void setCodiProv(StringFilter codiProv) {
        this.codiProv = codiProv;
    }

    public StringFilter getCodiComu() {
        return codiComu;
    }

    public void setCodiComu(StringFilter codiComu) {
        this.codiComu = codiComu;
    }

    public StringFilter getDescComu() {
        return descComu;
    }

    public void setDescComu(StringFilter descComu) {
        this.descComu = descComu;
    }

    public StringFilter getDescProv() {
        return descProv;
    }

    public void setDescProv(StringFilter descProv) {
        this.descProv = descProv;
    }

    public StringFilter getCodiSiglProv() {
        return codiSiglProv;
    }

    public void setCodiSiglProv(StringFilter codiSiglProv) {
        this.codiSiglProv = codiSiglProv;
    }

    public StringFilter getCodiFiscLuna() {
        return codiFiscLuna;
    }

    public void setCodiFiscLuna(StringFilter codiFiscLuna) {
        this.codiFiscLuna = codiFiscLuna;
    }

    public StringFilter getDescRegi() {
        return descRegi;
    }

    public void setDescRegi(StringFilter descRegi) {
        this.descRegi = descRegi;
    }

    public StringFilter getCodiceBelfiore() {
        return codiceBelfiore;
    }

    public void setCodiceBelfiore(StringFilter codiceBelfiore) {
        this.codiceBelfiore = codiceBelfiore;
    }

    public LocalDateFilter getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(LocalDateFilter dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDateFilter getDataFine() {
        return dataFine;
    }

    public void setDataFine(LocalDateFilter dataFine) {
        this.dataFine = dataFine;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ComuniCriteria that = (ComuniCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(codiProv, that.codiProv) &&
            Objects.equals(codiComu, that.codiComu) &&
            Objects.equals(descComu, that.descComu) &&
            Objects.equals(descProv, that.descProv) &&
            Objects.equals(codiSiglProv, that.codiSiglProv) &&
            Objects.equals(codiFiscLuna, that.codiFiscLuna) &&
            Objects.equals(descRegi, that.descRegi) &&
            Objects.equals(codiceBelfiore, that.codiceBelfiore) &&
            Objects.equals(dataInizio, that.dataInizio) &&
            Objects.equals(dataFine, that.dataFine) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        codiProv,
        codiComu,
        descComu,
        descProv,
        codiSiglProv,
        codiFiscLuna,
        descRegi,
        codiceBelfiore,
        dataInizio,
        dataFine,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod
        );
    }

    @Override
    public String toString() {
        return "ComuniCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (codiProv != null ? "codiProv=" + codiProv + ", " : "") +
                (codiComu != null ? "codiComu=" + codiComu + ", " : "") +
                (descComu != null ? "descComu=" + descComu + ", " : "") +
                (descProv != null ? "descProv=" + descProv + ", " : "") +
                (codiSiglProv != null ? "codiSiglProv=" + codiSiglProv + ", " : "") +
                (codiFiscLuna != null ? "codiFiscLuna=" + codiFiscLuna + ", " : "") +
                (descRegi != null ? "descRegi=" + descRegi + ", " : "") +
                (codiceBelfiore != null ? "codiceBelfiore=" + codiceBelfiore + ", " : "") +
                (dataInizio != null ? "dataInizio=" + dataInizio + ", " : "") +
                (dataFine != null ? "dataFine=" + dataFine + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
            "}";
    }

}
