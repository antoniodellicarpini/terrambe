package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.OpeAnagrafica} entity. This class is used
 * in {@link com.terrambe.web.rest.OpeAnagraficaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ope-anagraficas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OpeAnagraficaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private BooleanFilter attivo;

    private StringFilter nome;

    private StringFilter cognome;

    private StringFilter sesso;

    private LocalDateFilter dataNascita;

    private StringFilter codiceFiscale;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opeAnaToIndNascId;

    private LongFilter opeAnaToIndId;

    private LongFilter opeAnaToRecId;

    private LongFilter permessioperatoreId;

    private LongFilter opeAnagToPateId;

    private LongFilter operatoriaziId;

    private LongFilter opeAnagToOpeQualId;

    private LongFilter opeAnaToFornId;

    private LongFilter opeToTipoContratId;

    public OpeAnagraficaCriteria(){
    }

    public OpeAnagraficaCriteria(OpeAnagraficaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.attivo = other.attivo == null ? null : other.attivo.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.cognome = other.cognome == null ? null : other.cognome.copy();
        this.sesso = other.sesso == null ? null : other.sesso.copy();
        this.dataNascita = other.dataNascita == null ? null : other.dataNascita.copy();
        this.codiceFiscale = other.codiceFiscale == null ? null : other.codiceFiscale.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opeAnaToIndNascId = other.opeAnaToIndNascId == null ? null : other.opeAnaToIndNascId.copy();
        this.opeAnaToIndId = other.opeAnaToIndId == null ? null : other.opeAnaToIndId.copy();
        this.opeAnaToRecId = other.opeAnaToRecId == null ? null : other.opeAnaToRecId.copy();
        this.permessioperatoreId = other.permessioperatoreId == null ? null : other.permessioperatoreId.copy();
        this.opeAnagToPateId = other.opeAnagToPateId == null ? null : other.opeAnagToPateId.copy();
        this.operatoriaziId = other.operatoriaziId == null ? null : other.operatoriaziId.copy();
        this.opeAnagToOpeQualId = other.opeAnagToOpeQualId == null ? null : other.opeAnagToOpeQualId.copy();
        this.opeAnaToFornId = other.opeAnaToFornId == null ? null : other.opeAnaToFornId.copy();
        this.opeToTipoContratId = other.opeToTipoContratId == null ? null : other.opeToTipoContratId.copy();
    }

    @Override
    public OpeAnagraficaCriteria copy() {
        return new OpeAnagraficaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public BooleanFilter getAttivo() {
        return attivo;
    }

    public void setAttivo(BooleanFilter attivo) {
        this.attivo = attivo;
    }

    public StringFilter getNome() {
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public StringFilter getCognome() {
        return cognome;
    }

    public void setCognome(StringFilter cognome) {
        this.cognome = cognome;
    }

    public StringFilter getSesso() {
        return sesso;
    }

    public void setSesso(StringFilter sesso) {
        this.sesso = sesso;
    }

    public LocalDateFilter getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(LocalDateFilter dataNascita) {
        this.dataNascita = dataNascita;
    }

    public StringFilter getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(StringFilter codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpeAnaToIndNascId() {
        return opeAnaToIndNascId;
    }

    public void setOpeAnaToIndNascId(LongFilter opeAnaToIndNascId) {
        this.opeAnaToIndNascId = opeAnaToIndNascId;
    }

    public LongFilter getOpeAnaToIndId() {
        return opeAnaToIndId;
    }

    public void setOpeAnaToIndId(LongFilter opeAnaToIndId) {
        this.opeAnaToIndId = opeAnaToIndId;
    }

    public LongFilter getOpeAnaToRecId() {
        return opeAnaToRecId;
    }

    public void setOpeAnaToRecId(LongFilter opeAnaToRecId) {
        this.opeAnaToRecId = opeAnaToRecId;
    }

    public LongFilter getPermessioperatoreId() {
        return permessioperatoreId;
    }

    public void setPermessioperatoreId(LongFilter permessioperatoreId) {
        this.permessioperatoreId = permessioperatoreId;
    }

    public LongFilter getOpeAnagToPateId() {
        return opeAnagToPateId;
    }

    public void setOpeAnagToPateId(LongFilter opeAnagToPateId) {
        this.opeAnagToPateId = opeAnagToPateId;
    }

    public LongFilter getOperatoriaziId() {
        return operatoriaziId;
    }

    public void setOperatoriaziId(LongFilter operatoriaziId) {
        this.operatoriaziId = operatoriaziId;
    }

    public LongFilter getOpeAnagToOpeQualId() {
        return opeAnagToOpeQualId;
    }

    public void setOpeAnagToOpeQualId(LongFilter opeAnagToOpeQualId) {
        this.opeAnagToOpeQualId = opeAnagToOpeQualId;
    }

    public LongFilter getOpeAnaToFornId() {
        return opeAnaToFornId;
    }

    public void setOpeAnaToFornId(LongFilter opeAnaToFornId) {
        this.opeAnaToFornId = opeAnaToFornId;
    }

    public LongFilter getOpeToTipoContratId() {
        return opeToTipoContratId;
    }

    public void setOpeToTipoContratId(LongFilter opeToTipoContratId) {
        this.opeToTipoContratId = opeToTipoContratId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OpeAnagraficaCriteria that = (OpeAnagraficaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(attivo, that.attivo) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(cognome, that.cognome) &&
            Objects.equals(sesso, that.sesso) &&
            Objects.equals(dataNascita, that.dataNascita) &&
            Objects.equals(codiceFiscale, that.codiceFiscale) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opeAnaToIndNascId, that.opeAnaToIndNascId) &&
            Objects.equals(opeAnaToIndId, that.opeAnaToIndId) &&
            Objects.equals(opeAnaToRecId, that.opeAnaToRecId) &&
            Objects.equals(permessioperatoreId, that.permessioperatoreId) &&
            Objects.equals(opeAnagToPateId, that.opeAnagToPateId) &&
            Objects.equals(operatoriaziId, that.operatoriaziId) &&
            Objects.equals(opeAnagToOpeQualId, that.opeAnagToOpeQualId) &&
            Objects.equals(opeAnaToFornId, that.opeAnaToFornId) &&
            Objects.equals(opeToTipoContratId, that.opeToTipoContratId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        attivo,
        nome,
        cognome,
        sesso,
        dataNascita,
        codiceFiscale,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opeAnaToIndNascId,
        opeAnaToIndId,
        opeAnaToRecId,
        permessioperatoreId,
        opeAnagToPateId,
        operatoriaziId,
        opeAnagToOpeQualId,
        opeAnaToFornId,
        opeToTipoContratId
        );
    }

    @Override
    public String toString() {
        return "OpeAnagraficaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (attivo != null ? "attivo=" + attivo + ", " : "") +
                (nome != null ? "nome=" + nome + ", " : "") +
                (cognome != null ? "cognome=" + cognome + ", " : "") +
                (sesso != null ? "sesso=" + sesso + ", " : "") +
                (dataNascita != null ? "dataNascita=" + dataNascita + ", " : "") +
                (codiceFiscale != null ? "codiceFiscale=" + codiceFiscale + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opeAnaToIndNascId != null ? "opeAnaToIndNascId=" + opeAnaToIndNascId + ", " : "") +
                (opeAnaToIndId != null ? "opeAnaToIndId=" + opeAnaToIndId + ", " : "") +
                (opeAnaToRecId != null ? "opeAnaToRecId=" + opeAnaToRecId + ", " : "") +
                (permessioperatoreId != null ? "permessioperatoreId=" + permessioperatoreId + ", " : "") +
                (opeAnagToPateId != null ? "opeAnagToPateId=" + opeAnagToPateId + ", " : "") +
                (operatoriaziId != null ? "operatoriaziId=" + operatoriaziId + ", " : "") +
                (opeAnagToOpeQualId != null ? "opeAnagToOpeQualId=" + opeAnagToOpeQualId + ", " : "") +
                (opeAnaToFornId != null ? "opeAnaToFornId=" + opeAnaToFornId + ", " : "") +
                (opeToTipoContratId != null ? "opeToTipoContratId=" + opeToTipoContratId + ", " : "") +
            "}";
    }

}
