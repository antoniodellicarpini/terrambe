package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.ColtureBDF} entity. This class is used
 * in {@link com.terrambe.web.rest.ColtureBDFResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /colture-bdfs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ColtureBDFCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter codicePv;

    private StringFilter nomeColtura;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter coltBdfToRaccordoId;

    private LongFilter coltBdfToColtMascId;

    private LongFilter coltBdfToAgeaBdfId;

    public ColtureBDFCriteria(){
    }

    public ColtureBDFCriteria(ColtureBDFCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.codicePv = other.codicePv == null ? null : other.codicePv.copy();
        this.nomeColtura = other.nomeColtura == null ? null : other.nomeColtura.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.coltBdfToRaccordoId = other.coltBdfToRaccordoId == null ? null : other.coltBdfToRaccordoId.copy();
        this.coltBdfToColtMascId = other.coltBdfToColtMascId == null ? null : other.coltBdfToColtMascId.copy();
        this.coltBdfToAgeaBdfId = other.coltBdfToAgeaBdfId == null ? null : other.coltBdfToAgeaBdfId.copy();
    }

    @Override
    public ColtureBDFCriteria copy() {
        return new ColtureBDFCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCodicePv() {
        return codicePv;
    }

    public void setCodicePv(StringFilter codicePv) {
        this.codicePv = codicePv;
    }

    public StringFilter getNomeColtura() {
        return nomeColtura;
    }

    public void setNomeColtura(StringFilter nomeColtura) {
        this.nomeColtura = nomeColtura;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getColtBdfToRaccordoId() {
        return coltBdfToRaccordoId;
    }

    public void setColtBdfToRaccordoId(LongFilter coltBdfToRaccordoId) {
        this.coltBdfToRaccordoId = coltBdfToRaccordoId;
    }

    public LongFilter getColtBdfToColtMascId() {
        return coltBdfToColtMascId;
    }

    public void setColtBdfToColtMascId(LongFilter coltBdfToColtMascId) {
        this.coltBdfToColtMascId = coltBdfToColtMascId;
    }

    public LongFilter getColtBdfToAgeaBdfId() {
        return coltBdfToAgeaBdfId;
    }

    public void setColtBdfToAgeaBdfId(LongFilter coltBdfToAgeaBdfId) {
        this.coltBdfToAgeaBdfId = coltBdfToAgeaBdfId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ColtureBDFCriteria that = (ColtureBDFCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(codicePv, that.codicePv) &&
            Objects.equals(nomeColtura, that.nomeColtura) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(coltBdfToRaccordoId, that.coltBdfToRaccordoId) &&
            Objects.equals(coltBdfToColtMascId, that.coltBdfToColtMascId) &&
            Objects.equals(coltBdfToAgeaBdfId, that.coltBdfToAgeaBdfId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        codicePv,
        nomeColtura,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        coltBdfToRaccordoId,
        coltBdfToColtMascId,
        coltBdfToAgeaBdfId
        );
    }

    @Override
    public String toString() {
        return "ColtureBDFCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (codicePv != null ? "codicePv=" + codicePv + ", " : "") +
                (nomeColtura != null ? "nomeColtura=" + nomeColtura + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (coltBdfToRaccordoId != null ? "coltBdfToRaccordoId=" + coltBdfToRaccordoId + ", " : "") +
                (coltBdfToColtMascId != null ? "coltBdfToColtMascId=" + coltBdfToColtMascId + ", " : "") +
                (coltBdfToAgeaBdfId != null ? "coltBdfToAgeaBdfId=" + coltBdfToAgeaBdfId + ", " : "") +
            "}";
    }

}
