package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.TabellaRaccordo} entity. This class is used
 * in {@link com.terrambe.web.rest.TabellaRaccordoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tabella-raccordos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TabellaRaccordoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter raccordoToSitoId;

    private LongFilter raccordoToColtBdfId;

    private LongFilter raccordoToDosagId;

    private LongFilter raccordoToEtiFitoId;

    private LongFilter raccordoToFaseFenoId;

    private LongFilter raccordoToTipoDistrId;

    private LongFilter raccordoToAvversId;

    private LongFilter avversita_id;
    private LongFilter coltura_id;

    public TabellaRaccordoCriteria(){
    }

    public TabellaRaccordoCriteria(TabellaRaccordoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.raccordoToSitoId = other.raccordoToSitoId == null ? null : other.raccordoToSitoId.copy();
        this.raccordoToColtBdfId = other.raccordoToColtBdfId == null ? null : other.raccordoToColtBdfId.copy();
        this.raccordoToDosagId = other.raccordoToDosagId == null ? null : other.raccordoToDosagId.copy();
        this.raccordoToEtiFitoId = other.raccordoToEtiFitoId == null ? null : other.raccordoToEtiFitoId.copy();
        this.raccordoToFaseFenoId = other.raccordoToFaseFenoId == null ? null : other.raccordoToFaseFenoId.copy();
        this.raccordoToTipoDistrId = other.raccordoToTipoDistrId == null ? null : other.raccordoToTipoDistrId.copy();
        this.raccordoToAvversId = other.raccordoToAvversId == null ? null : other.raccordoToAvversId.copy();

        this.avversita_id = other.avversita_id == null ? null : other.avversita_id.copy();
        this.coltura_id = other.coltura_id == null ? null : other.coltura_id.copy();
    }

    @Override
    public TabellaRaccordoCriteria copy() {
        return new TabellaRaccordoCriteria(this);
    }

    public LongFilter getAvversitaId() {
        return avversita_id;
    }

    public void setAvversitaId(LongFilter avversita_id) {
        this.avversita_id = avversita_id;
    }

    public LongFilter getColturaId() {
        return coltura_id;
    }

    public void setColturaId(LongFilter coltura_id) {
        this.coltura_id = coltura_id;
    }



    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getRaccordoToSitoId() {
        return raccordoToSitoId;
    }

    public void setRaccordoToSitoId(LongFilter raccordoToSitoId) {
        this.raccordoToSitoId = raccordoToSitoId;
    }

    public LongFilter getRaccordoToColtBdfId() {
        return raccordoToColtBdfId;
    }

    public void setRaccordoToColtBdfId(LongFilter raccordoToColtBdfId) {
        this.raccordoToColtBdfId = raccordoToColtBdfId;
    }

    public LongFilter getRaccordoToDosagId() {
        return raccordoToDosagId;
    }

    public void setRaccordoToDosagId(LongFilter raccordoToDosagId) {
        this.raccordoToDosagId = raccordoToDosagId;
    }

    public LongFilter getRaccordoToEtiFitoId() {
        return raccordoToEtiFitoId;
    }

    public void setRaccordoToEtiFitoId(LongFilter raccordoToEtiFitoId) {
        this.raccordoToEtiFitoId = raccordoToEtiFitoId;
    }

    public LongFilter getRaccordoToFaseFenoId() {
        return raccordoToFaseFenoId;
    }

    public void setRaccordoToFaseFenoId(LongFilter raccordoToFaseFenoId) {
        this.raccordoToFaseFenoId = raccordoToFaseFenoId;
    }

    public LongFilter getRaccordoToTipoDistrId() {
        return raccordoToTipoDistrId;
    }

    public void setRaccordoToTipoDistrId(LongFilter raccordoToTipoDistrId) {
        this.raccordoToTipoDistrId = raccordoToTipoDistrId;
    }

    public LongFilter getRaccordoToAvversId() {
        return raccordoToAvversId;
    }

    public void setRaccordoToAvversId(LongFilter raccordoToAvversId) {
        this.raccordoToAvversId = raccordoToAvversId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TabellaRaccordoCriteria that = (TabellaRaccordoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(raccordoToSitoId, that.raccordoToSitoId) &&
            Objects.equals(raccordoToColtBdfId, that.raccordoToColtBdfId) &&
            Objects.equals(raccordoToDosagId, that.raccordoToDosagId) &&
            Objects.equals(raccordoToEtiFitoId, that.raccordoToEtiFitoId) &&
            Objects.equals(raccordoToFaseFenoId, that.raccordoToFaseFenoId) &&
            Objects.equals(raccordoToTipoDistrId, that.raccordoToTipoDistrId) &&
            Objects.equals(raccordoToAvversId, that.raccordoToAvversId) &&

            Objects.equals(avversita_id, that.avversita_id) &&
            Objects.equals(coltura_id, that.coltura_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        raccordoToSitoId,
        raccordoToColtBdfId,
        raccordoToDosagId,
        raccordoToEtiFitoId,
        raccordoToFaseFenoId,
        raccordoToTipoDistrId,
        raccordoToAvversId
        );
    }

    @Override
    public String toString() {
        return "TabellaRaccordoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (raccordoToSitoId != null ? "raccordoToSitoId=" + raccordoToSitoId + ", " : "") +
                (raccordoToColtBdfId != null ? "raccordoToColtBdfId=" + raccordoToColtBdfId + ", " : "") +
                (raccordoToDosagId != null ? "raccordoToDosagId=" + raccordoToDosagId + ", " : "") +
                (raccordoToEtiFitoId != null ? "raccordoToEtiFitoId=" + raccordoToEtiFitoId + ", " : "") +
                (raccordoToFaseFenoId != null ? "raccordoToFaseFenoId=" + raccordoToFaseFenoId + ", " : "") +
                (raccordoToTipoDistrId != null ? "raccordoToTipoDistrId=" + raccordoToTipoDistrId + ", " : "") +
                (raccordoToAvversId != null ? "raccordoToAvversId=" + raccordoToAvversId + ", " : "") +
            "}";
    }

}
