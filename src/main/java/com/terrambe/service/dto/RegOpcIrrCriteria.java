package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcIrr} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcIrrResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-irrs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcIrrCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataOperazione;

    private StringFilter tempoImpiegato;

    private FloatFilter volumeAcqua;

    private LocalDateFilter dataInizioPeriod;

    private LocalDateFilter dataFinePeriod;

    private IntegerFilter frequenzaGiorni;

    private LongFilter idColtura;

    private LongFilter idOperatore;

    private LongFilter idMezzo;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opcIrrToAppzId;

    private LongFilter opcIrrToTipoIrrId;

    private LongFilter opcIrrToMotivoId;

    public RegOpcIrrCriteria(){
    }

    public RegOpcIrrCriteria(RegOpcIrrCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataOperazione = other.dataOperazione == null ? null : other.dataOperazione.copy();
        this.tempoImpiegato = other.tempoImpiegato == null ? null : other.tempoImpiegato.copy();
        this.volumeAcqua = other.volumeAcqua == null ? null : other.volumeAcqua.copy();
        this.dataInizioPeriod = other.dataInizioPeriod == null ? null : other.dataInizioPeriod.copy();
        this.dataFinePeriod = other.dataFinePeriod == null ? null : other.dataFinePeriod.copy();
        this.frequenzaGiorni = other.frequenzaGiorni == null ? null : other.frequenzaGiorni.copy();
        this.idColtura = other.idColtura == null ? null : other.idColtura.copy();
        this.idOperatore = other.idOperatore == null ? null : other.idOperatore.copy();
        this.idMezzo = other.idMezzo == null ? null : other.idMezzo.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opcIrrToAppzId = other.opcIrrToAppzId == null ? null : other.opcIrrToAppzId.copy();
        this.opcIrrToTipoIrrId = other.opcIrrToTipoIrrId == null ? null : other.opcIrrToTipoIrrId.copy();
        this.opcIrrToMotivoId = other.opcIrrToMotivoId == null ? null : other.opcIrrToMotivoId.copy();
    }

    @Override
    public RegOpcIrrCriteria copy() {
        return new RegOpcIrrCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataOperazione() {
        return dataOperazione;
    }

    public void setDataOperazione(LocalDateFilter dataOperazione) {
        this.dataOperazione = dataOperazione;
    }

    public StringFilter getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(StringFilter tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public FloatFilter getVolumeAcqua() {
        return volumeAcqua;
    }

    public void setVolumeAcqua(FloatFilter volumeAcqua) {
        this.volumeAcqua = volumeAcqua;
    }

    public LocalDateFilter getDataInizioPeriod() {
        return dataInizioPeriod;
    }

    public void setDataInizioPeriod(LocalDateFilter dataInizioPeriod) {
        this.dataInizioPeriod = dataInizioPeriod;
    }

    public LocalDateFilter getDataFinePeriod() {
        return dataFinePeriod;
    }

    public void setDataFinePeriod(LocalDateFilter dataFinePeriod) {
        this.dataFinePeriod = dataFinePeriod;
    }

    public IntegerFilter getFrequenzaGiorni() {
        return frequenzaGiorni;
    }

    public void setFrequenzaGiorni(IntegerFilter frequenzaGiorni) {
        this.frequenzaGiorni = frequenzaGiorni;
    }

    public LongFilter getIdColtura() {
        return idColtura;
    }

    public void setIdColtura(LongFilter idColtura) {
        this.idColtura = idColtura;
    }

    public LongFilter getIdOperatore() {
        return idOperatore;
    }

    public void setIdOperatore(LongFilter idOperatore) {
        this.idOperatore = idOperatore;
    }

    public LongFilter getIdMezzo() {
        return idMezzo;
    }

    public void setIdMezzo(LongFilter idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpcIrrToAppzId() {
        return opcIrrToAppzId;
    }

    public void setOpcIrrToAppzId(LongFilter opcIrrToAppzId) {
        this.opcIrrToAppzId = opcIrrToAppzId;
    }

    public LongFilter getOpcIrrToTipoIrrId() {
        return opcIrrToTipoIrrId;
    }

    public void setOpcIrrToTipoIrrId(LongFilter opcIrrToTipoIrrId) {
        this.opcIrrToTipoIrrId = opcIrrToTipoIrrId;
    }

    public LongFilter getOpcIrrToMotivoId() {
        return opcIrrToMotivoId;
    }

    public void setOpcIrrToMotivoId(LongFilter opcIrrToMotivoId) {
        this.opcIrrToMotivoId = opcIrrToMotivoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcIrrCriteria that = (RegOpcIrrCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataOperazione, that.dataOperazione) &&
            Objects.equals(tempoImpiegato, that.tempoImpiegato) &&
            Objects.equals(volumeAcqua, that.volumeAcqua) &&
            Objects.equals(dataInizioPeriod, that.dataInizioPeriod) &&
            Objects.equals(dataFinePeriod, that.dataFinePeriod) &&
            Objects.equals(frequenzaGiorni, that.frequenzaGiorni) &&
            Objects.equals(idColtura, that.idColtura) &&
            Objects.equals(idOperatore, that.idOperatore) &&
            Objects.equals(idMezzo, that.idMezzo) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opcIrrToAppzId, that.opcIrrToAppzId) &&
            Objects.equals(opcIrrToTipoIrrId, that.opcIrrToTipoIrrId) &&
            Objects.equals(opcIrrToMotivoId, that.opcIrrToMotivoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        dataOperazione,
        tempoImpiegato,
        volumeAcqua,
        dataInizioPeriod,
        dataFinePeriod,
        frequenzaGiorni,
        idColtura,
        idOperatore,
        idMezzo,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opcIrrToAppzId,
        opcIrrToTipoIrrId,
        opcIrrToMotivoId
        );
    }

    @Override
    public String toString() {
        return "RegOpcIrrCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataOperazione != null ? "dataOperazione=" + dataOperazione + ", " : "") +
                (tempoImpiegato != null ? "tempoImpiegato=" + tempoImpiegato + ", " : "") +
                (volumeAcqua != null ? "volumeAcqua=" + volumeAcqua + ", " : "") +
                (dataInizioPeriod != null ? "dataInizioPeriod=" + dataInizioPeriod + ", " : "") +
                (dataFinePeriod != null ? "dataFinePeriod=" + dataFinePeriod + ", " : "") +
                (frequenzaGiorni != null ? "frequenzaGiorni=" + frequenzaGiorni + ", " : "") +
                (idColtura != null ? "idColtura=" + idColtura + ", " : "") +
                (idOperatore != null ? "idOperatore=" + idOperatore + ", " : "") +
                (idMezzo != null ? "idMezzo=" + idMezzo + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opcIrrToAppzId != null ? "opcIrrToAppzId=" + opcIrrToAppzId + ", " : "") +
                (opcIrrToTipoIrrId != null ? "opcIrrToTipoIrrId=" + opcIrrToTipoIrrId + ", " : "") +
                (opcIrrToMotivoId != null ? "opcIrrToMotivoId=" + opcIrrToMotivoId + ", " : "") +
            "}";
    }

}
