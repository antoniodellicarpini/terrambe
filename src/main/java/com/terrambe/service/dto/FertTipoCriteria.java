package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.FertTipo} entity. This class is used
 * in {@link com.terrambe.web.rest.FertTipoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fert-tipos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FertTipoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter descrizione;

    private LongFilter ferAnagraficaId;

    public FertTipoCriteria(){
    }

    public FertTipoCriteria(FertTipoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.descrizione = other.descrizione == null ? null : other.descrizione.copy();
        this.ferAnagraficaId = other.ferAnagraficaId == null ? null : other.ferAnagraficaId.copy();
    }

    @Override
    public FertTipoCriteria copy() {
        return new FertTipoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(StringFilter descrizione) {
        this.descrizione = descrizione;
    }

    public LongFilter getFerAnagraficaId() {
        return ferAnagraficaId;
    }

    public void setFerAnagraficaId(LongFilter ferAnagraficaId) {
        this.ferAnagraficaId = ferAnagraficaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FertTipoCriteria that = (FertTipoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(descrizione, that.descrizione) &&
            Objects.equals(ferAnagraficaId, that.ferAnagraficaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        descrizione,
        ferAnagraficaId
        );
    }

    @Override
    public String toString() {
        return "FertTipoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (descrizione != null ? "descrizione=" + descrizione + ", " : "") +
                (ferAnagraficaId != null ? "ferAnagraficaId=" + ferAnagraficaId + ", " : "") +
            "}";
    }

}
