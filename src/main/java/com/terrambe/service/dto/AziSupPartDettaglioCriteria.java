package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.AziSupPartDettaglio} entity. This class is used
 * in {@link com.terrambe.web.rest.AziSupPartDettaglioResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /azi-sup-part-dettaglios?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AziSupPartDettaglioCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private FloatFilter tara;

    private StringFilter uuidEsri;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter dettaglioToPartId;

    private LongFilter dettToAgeaId;

    public AziSupPartDettaglioCriteria(){
    }

    public AziSupPartDettaglioCriteria(AziSupPartDettaglioCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.tara = other.tara == null ? null : other.tara.copy();
        this.uuidEsri = other.uuidEsri == null ? null : other.uuidEsri.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.dettaglioToPartId = other.dettaglioToPartId == null ? null : other.dettaglioToPartId.copy();
        this.dettToAgeaId = other.dettToAgeaId == null ? null : other.dettToAgeaId.copy();
    }

    @Override
    public AziSupPartDettaglioCriteria copy() {
        return new AziSupPartDettaglioCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public FloatFilter getTara() {
        return tara;
    }

    public void setTara(FloatFilter tara) {
        this.tara = tara;
    }

    public StringFilter getUuidEsri() {
        return uuidEsri;
    }

    public void setUuidEsri(StringFilter uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getDettaglioToPartId() {
        return dettaglioToPartId;
    }

    public void setDettaglioToPartId(LongFilter dettaglioToPartId) {
        this.dettaglioToPartId = dettaglioToPartId;
    }

    public LongFilter getDettToAgeaId() {
        return dettToAgeaId;
    }

    public void setDettToAgeaId(LongFilter dettToAgeaId) {
        this.dettToAgeaId = dettToAgeaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AziSupPartDettaglioCriteria that = (AziSupPartDettaglioCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(tara, that.tara) &&
            Objects.equals(uuidEsri, that.uuidEsri) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(dettaglioToPartId, that.dettaglioToPartId) &&
            Objects.equals(dettToAgeaId, that.dettToAgeaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        tara,
        uuidEsri,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        dettaglioToPartId,
        dettToAgeaId
        );
    }

    @Override
    public String toString() {
        return "AziSupPartDettaglioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (tara != null ? "tara=" + tara + ", " : "") +
                (uuidEsri != null ? "uuidEsri=" + uuidEsri + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (dettaglioToPartId != null ? "dettaglioToPartId=" + dettaglioToPartId + ", " : "") +
                (dettToAgeaId != null ? "dettToAgeaId=" + dettToAgeaId + ", " : "") +
            "}";
    }

}
