package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.OpePatentino} entity. This class is used
 * in {@link com.terrambe.web.rest.OpePatentinoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ope-patentinos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OpePatentinoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter numero;

    private LocalDateFilter dataRilascio;

    private LocalDateFilter dataScadenza;

    private StringFilter enteRilasciato;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter pateToOpeAnagId;

    private LongFilter opepatentinotipologiaId;

    public OpePatentinoCriteria(){
    }

    public OpePatentinoCriteria(OpePatentinoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.numero = other.numero == null ? null : other.numero.copy();
        this.dataRilascio = other.dataRilascio == null ? null : other.dataRilascio.copy();
        this.dataScadenza = other.dataScadenza == null ? null : other.dataScadenza.copy();
        this.enteRilasciato = other.enteRilasciato == null ? null : other.enteRilasciato.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.pateToOpeAnagId = other.pateToOpeAnagId == null ? null : other.pateToOpeAnagId.copy();
        this.opepatentinotipologiaId = other.opepatentinotipologiaId == null ? null : other.opepatentinotipologiaId.copy();
    }

    @Override
    public OpePatentinoCriteria copy() {
        return new OpePatentinoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNumero() {
        return numero;
    }

    public void setNumero(StringFilter numero) {
        this.numero = numero;
    }

    public LocalDateFilter getDataRilascio() {
        return dataRilascio;
    }

    public void setDataRilascio(LocalDateFilter dataRilascio) {
        this.dataRilascio = dataRilascio;
    }

    public LocalDateFilter getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(LocalDateFilter dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public StringFilter getEnteRilasciato() {
        return enteRilasciato;
    }

    public void setEnteRilasciato(StringFilter enteRilasciato) {
        this.enteRilasciato = enteRilasciato;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getPateToOpeAnagId() {
        return pateToOpeAnagId;
    }

    public void setPateToOpeAnagId(LongFilter pateToOpeAnagId) {
        this.pateToOpeAnagId = pateToOpeAnagId;
    }

    public LongFilter getOpepatentinotipologiaId() {
        return opepatentinotipologiaId;
    }

    public void setOpepatentinotipologiaId(LongFilter opepatentinotipologiaId) {
        this.opepatentinotipologiaId = opepatentinotipologiaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OpePatentinoCriteria that = (OpePatentinoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(numero, that.numero) &&
            Objects.equals(dataRilascio, that.dataRilascio) &&
            Objects.equals(dataScadenza, that.dataScadenza) &&
            Objects.equals(enteRilasciato, that.enteRilasciato) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(pateToOpeAnagId, that.pateToOpeAnagId) &&
            Objects.equals(opepatentinotipologiaId, that.opepatentinotipologiaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        numero,
        dataRilascio,
        dataScadenza,
        enteRilasciato,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        pateToOpeAnagId,
        opepatentinotipologiaId
        );
    }

    @Override
    public String toString() {
        return "OpePatentinoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numero != null ? "numero=" + numero + ", " : "") +
                (dataRilascio != null ? "dataRilascio=" + dataRilascio + ", " : "") +
                (dataScadenza != null ? "dataScadenza=" + dataScadenza + ", " : "") +
                (enteRilasciato != null ? "enteRilasciato=" + enteRilasciato + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (pateToOpeAnagId != null ? "pateToOpeAnagId=" + pateToOpeAnagId + ", " : "") +
                (opepatentinotipologiaId != null ? "opepatentinotipologiaId=" + opepatentinotipologiaId + ", " : "") +
            "}";
    }

}
