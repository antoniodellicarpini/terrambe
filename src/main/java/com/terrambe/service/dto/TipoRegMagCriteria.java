package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.TipoRegMag} entity. This class is used
 * in {@link com.terrambe.web.rest.TipoRegMagResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tipo-reg-mags?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TipoRegMagCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter descrizione;

    private IntegerFilter moltiplicatore;

    private LocalDateFilter dataInizioMag;

    private LocalDateFilter dataFineMag;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter tipoRegToRegFitoId;

    private LongFilter tipoRegToRegFertId;

    private LongFilter tipoRegToRegTrapId;

    public TipoRegMagCriteria(){
    }

    public TipoRegMagCriteria(TipoRegMagCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.descrizione = other.descrizione == null ? null : other.descrizione.copy();
        this.moltiplicatore = other.moltiplicatore == null ? null : other.moltiplicatore.copy();
        this.dataInizioMag = other.dataInizioMag == null ? null : other.dataInizioMag.copy();
        this.dataFineMag = other.dataFineMag == null ? null : other.dataFineMag.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.tipoRegToRegFitoId = other.tipoRegToRegFitoId == null ? null : other.tipoRegToRegFitoId.copy();
        this.tipoRegToRegFertId = other.tipoRegToRegFertId == null ? null : other.tipoRegToRegFertId.copy();
        this.tipoRegToRegTrapId = other.tipoRegToRegTrapId == null ? null : other.tipoRegToRegTrapId.copy();
    }

    @Override
    public TipoRegMagCriteria copy() {
        return new TipoRegMagCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(StringFilter descrizione) {
        this.descrizione = descrizione;
    }

    public IntegerFilter getMoltiplicatore() {
        return moltiplicatore;
    }

    public void setMoltiplicatore(IntegerFilter moltiplicatore) {
        this.moltiplicatore = moltiplicatore;
    }

    public LocalDateFilter getDataInizioMag() {
        return dataInizioMag;
    }

    public void setDataInizioMag(LocalDateFilter dataInizioMag) {
        this.dataInizioMag = dataInizioMag;
    }

    public LocalDateFilter getDataFineMag() {
        return dataFineMag;
    }

    public void setDataFineMag(LocalDateFilter dataFineMag) {
        this.dataFineMag = dataFineMag;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getTipoRegToRegFitoId() {
        return tipoRegToRegFitoId;
    }

    public void setTipoRegToRegFitoId(LongFilter tipoRegToRegFitoId) {
        this.tipoRegToRegFitoId = tipoRegToRegFitoId;
    }

    public LongFilter getTipoRegToRegFertId() {
        return tipoRegToRegFertId;
    }

    public void setTipoRegToRegFertId(LongFilter tipoRegToRegFertId) {
        this.tipoRegToRegFertId = tipoRegToRegFertId;
    }

    public LongFilter getTipoRegToRegTrapId() {
        return tipoRegToRegTrapId;
    }

    public void setTipoRegToRegTrapId(LongFilter tipoRegToRegTrapId) {
        this.tipoRegToRegTrapId = tipoRegToRegTrapId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TipoRegMagCriteria that = (TipoRegMagCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(descrizione, that.descrizione) &&
            Objects.equals(moltiplicatore, that.moltiplicatore) &&
            Objects.equals(dataInizioMag, that.dataInizioMag) &&
            Objects.equals(dataFineMag, that.dataFineMag) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(tipoRegToRegFitoId, that.tipoRegToRegFitoId) &&
            Objects.equals(tipoRegToRegFertId, that.tipoRegToRegFertId) &&
            Objects.equals(tipoRegToRegTrapId, that.tipoRegToRegTrapId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        descrizione,
        moltiplicatore,
        dataInizioMag,
        dataFineMag,
        userIdCreator,
        userIdLastMod,
        tipoRegToRegFitoId,
        tipoRegToRegFertId,
        tipoRegToRegTrapId
        );
    }

    @Override
    public String toString() {
        return "TipoRegMagCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (descrizione != null ? "descrizione=" + descrizione + ", " : "") +
                (moltiplicatore != null ? "moltiplicatore=" + moltiplicatore + ", " : "") +
                (dataInizioMag != null ? "dataInizioMag=" + dataInizioMag + ", " : "") +
                (dataFineMag != null ? "dataFineMag=" + dataFineMag + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (tipoRegToRegFitoId != null ? "tipoRegToRegFitoId=" + tipoRegToRegFitoId + ", " : "") +
                (tipoRegToRegFertId != null ? "tipoRegToRegFertId=" + tipoRegToRegFertId + ", " : "") +
                (tipoRegToRegTrapId != null ? "tipoRegToRegTrapId=" + tipoRegToRegTrapId + ", " : "") +
            "}";
    }

}
