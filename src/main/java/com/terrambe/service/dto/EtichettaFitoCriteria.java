package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.EtichettaFito} entity. This class is used
 * in {@link com.terrambe.web.rest.EtichettaFitoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /etichetta-fitos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EtichettaFitoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomeCommerciale;

    private LocalDateFilter dataReg;

    private StringFilter registrazioneMinisteriale;

    private StringFilter sigla;

    private StringFilter bio;

    private StringFilter ppo;

    private StringFilter revocato;

    private StringFilter revocaAutorizzazione;

    private StringFilter scadenzaCommercio;

    private StringFilter scadenzaUtilizzo;

    private StringFilter avvClp;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter incompatibilitaetichettaId;

    private LongFilter probfitotossicitaId;

    private LongFilter noteagguntiveetiId;

    private LongFilter etisostanzeattiveId;

    private LongFilter etichettafrasiId;

    private LongFilter etichettapittogrammiId;

    private LongFilter etichettatipofitofarmacoId;

    private LongFilter etiFitoToRaccordoId;

    private LongFilter titolareRegistrazioneId;

    private LongFilter formulazioneId;

    public EtichettaFitoCriteria(){
    }

    public EtichettaFitoCriteria(EtichettaFitoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nomeCommerciale = other.nomeCommerciale == null ? null : other.nomeCommerciale.copy();
        this.dataReg = other.dataReg == null ? null : other.dataReg.copy();
        this.registrazioneMinisteriale = other.registrazioneMinisteriale == null ? null : other.registrazioneMinisteriale.copy();
        this.sigla = other.sigla == null ? null : other.sigla.copy();
        this.bio = other.bio == null ? null : other.bio.copy();
        this.ppo = other.ppo == null ? null : other.ppo.copy();
        this.revocato = other.revocato == null ? null : other.revocato.copy();
        this.revocaAutorizzazione = other.revocaAutorizzazione == null ? null : other.revocaAutorizzazione.copy();
        this.scadenzaCommercio = other.scadenzaCommercio == null ? null : other.scadenzaCommercio.copy();
        this.scadenzaUtilizzo = other.scadenzaUtilizzo == null ? null : other.scadenzaUtilizzo.copy();
        this.avvClp = other.avvClp == null ? null : other.avvClp.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.incompatibilitaetichettaId = other.incompatibilitaetichettaId == null ? null : other.incompatibilitaetichettaId.copy();
        this.probfitotossicitaId = other.probfitotossicitaId == null ? null : other.probfitotossicitaId.copy();
        this.noteagguntiveetiId = other.noteagguntiveetiId == null ? null : other.noteagguntiveetiId.copy();
        this.etisostanzeattiveId = other.etisostanzeattiveId == null ? null : other.etisostanzeattiveId.copy();
        this.etichettafrasiId = other.etichettafrasiId == null ? null : other.etichettafrasiId.copy();
        this.etichettapittogrammiId = other.etichettapittogrammiId == null ? null : other.etichettapittogrammiId.copy();
        this.etichettatipofitofarmacoId = other.etichettatipofitofarmacoId == null ? null : other.etichettatipofitofarmacoId.copy();
        this.etiFitoToRaccordoId = other.etiFitoToRaccordoId == null ? null : other.etiFitoToRaccordoId.copy();
        this.titolareRegistrazioneId = other.titolareRegistrazioneId == null ? null : other.titolareRegistrazioneId.copy();
        this.formulazioneId = other.formulazioneId == null ? null : other.formulazioneId.copy();
    }

    @Override
    public EtichettaFitoCriteria copy() {
        return new EtichettaFitoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomeCommerciale() {
        return nomeCommerciale;
    }

    public void setNomeCommerciale(StringFilter nomeCommerciale) {
        this.nomeCommerciale = nomeCommerciale;
    }

    public LocalDateFilter getDataReg() {
        return dataReg;
    }

    public void setDataReg(LocalDateFilter dataReg) {
        this.dataReg = dataReg;
    }

    public StringFilter getRegistrazioneMinisteriale() {
        return registrazioneMinisteriale;
    }

    public void setRegistrazioneMinisteriale(StringFilter registrazioneMinisteriale) {
        this.registrazioneMinisteriale = registrazioneMinisteriale;
    }

    public StringFilter getSigla() {
        return sigla;
    }

    public void setSigla(StringFilter sigla) {
        this.sigla = sigla;
    }

    public StringFilter getBio() {
        return bio;
    }

    public void setBio(StringFilter bio) {
        this.bio = bio;
    }

    public StringFilter getPpo() {
        return ppo;
    }

    public void setPpo(StringFilter ppo) {
        this.ppo = ppo;
    }

    public StringFilter getRevocato() {
        return revocato;
    }

    public void setRevocato(StringFilter revocato) {
        this.revocato = revocato;
    }

    public StringFilter getRevocaAutorizzazione() {
        return revocaAutorizzazione;
    }

    public void setRevocaAutorizzazione(StringFilter revocaAutorizzazione) {
        this.revocaAutorizzazione = revocaAutorizzazione;
    }

    public StringFilter getScadenzaCommercio() {
        return scadenzaCommercio;
    }

    public void setScadenzaCommercio(StringFilter scadenzaCommercio) {
        this.scadenzaCommercio = scadenzaCommercio;
    }

    public StringFilter getScadenzaUtilizzo() {
        return scadenzaUtilizzo;
    }

    public void setScadenzaUtilizzo(StringFilter scadenzaUtilizzo) {
        this.scadenzaUtilizzo = scadenzaUtilizzo;
    }

    public StringFilter getAvvClp() {
        return avvClp;
    }

    public void setAvvClp(StringFilter avvClp) {
        this.avvClp = avvClp;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getIncompatibilitaetichettaId() {
        return incompatibilitaetichettaId;
    }

    public void setIncompatibilitaetichettaId(LongFilter incompatibilitaetichettaId) {
        this.incompatibilitaetichettaId = incompatibilitaetichettaId;
    }

    public LongFilter getProbfitotossicitaId() {
        return probfitotossicitaId;
    }

    public void setProbfitotossicitaId(LongFilter probfitotossicitaId) {
        this.probfitotossicitaId = probfitotossicitaId;
    }

    public LongFilter getNoteagguntiveetiId() {
        return noteagguntiveetiId;
    }

    public void setNoteagguntiveetiId(LongFilter noteagguntiveetiId) {
        this.noteagguntiveetiId = noteagguntiveetiId;
    }

    public LongFilter getEtisostanzeattiveId() {
        return etisostanzeattiveId;
    }

    public void setEtisostanzeattiveId(LongFilter etisostanzeattiveId) {
        this.etisostanzeattiveId = etisostanzeattiveId;
    }

    public LongFilter getEtichettafrasiId() {
        return etichettafrasiId;
    }

    public void setEtichettafrasiId(LongFilter etichettafrasiId) {
        this.etichettafrasiId = etichettafrasiId;
    }

    public LongFilter getEtichettapittogrammiId() {
        return etichettapittogrammiId;
    }

    public void setEtichettapittogrammiId(LongFilter etichettapittogrammiId) {
        this.etichettapittogrammiId = etichettapittogrammiId;
    }

    public LongFilter getEtichettatipofitofarmacoId() {
        return etichettatipofitofarmacoId;
    }

    public void setEtichettatipofitofarmacoId(LongFilter etichettatipofitofarmacoId) {
        this.etichettatipofitofarmacoId = etichettatipofitofarmacoId;
    }

    public LongFilter getEtiFitoToRaccordoId() {
        return etiFitoToRaccordoId;
    }

    public void setEtiFitoToRaccordoId(LongFilter etiFitoToRaccordoId) {
        this.etiFitoToRaccordoId = etiFitoToRaccordoId;
    }

    public LongFilter getTitolareRegistrazioneId() {
        return titolareRegistrazioneId;
    }

    public void setTitolareRegistrazioneId(LongFilter titolareRegistrazioneId) {
        this.titolareRegistrazioneId = titolareRegistrazioneId;
    }

    public LongFilter getFormulazioneId() {
        return formulazioneId;
    }

    public void setFormulazioneId(LongFilter formulazioneId) {
        this.formulazioneId = formulazioneId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EtichettaFitoCriteria that = (EtichettaFitoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomeCommerciale, that.nomeCommerciale) &&
            Objects.equals(dataReg, that.dataReg) &&
            Objects.equals(registrazioneMinisteriale, that.registrazioneMinisteriale) &&
            Objects.equals(sigla, that.sigla) &&
            Objects.equals(bio, that.bio) &&
            Objects.equals(ppo, that.ppo) &&
            Objects.equals(revocato, that.revocato) &&
            Objects.equals(revocaAutorizzazione, that.revocaAutorizzazione) &&
            Objects.equals(scadenzaCommercio, that.scadenzaCommercio) &&
            Objects.equals(scadenzaUtilizzo, that.scadenzaUtilizzo) &&
            Objects.equals(avvClp, that.avvClp) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(incompatibilitaetichettaId, that.incompatibilitaetichettaId) &&
            Objects.equals(probfitotossicitaId, that.probfitotossicitaId) &&
            Objects.equals(noteagguntiveetiId, that.noteagguntiveetiId) &&
            Objects.equals(etisostanzeattiveId, that.etisostanzeattiveId) &&
            Objects.equals(etichettafrasiId, that.etichettafrasiId) &&
            Objects.equals(etichettapittogrammiId, that.etichettapittogrammiId) &&
            Objects.equals(etichettatipofitofarmacoId, that.etichettatipofitofarmacoId) &&
            Objects.equals(etiFitoToRaccordoId, that.etiFitoToRaccordoId) &&
            Objects.equals(titolareRegistrazioneId, that.titolareRegistrazioneId) &&
            Objects.equals(formulazioneId, that.formulazioneId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomeCommerciale,
        dataReg,
        registrazioneMinisteriale,
        sigla,
        bio,
        ppo,
        revocato,
        revocaAutorizzazione,
        scadenzaCommercio,
        scadenzaUtilizzo,
        avvClp,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        incompatibilitaetichettaId,
        probfitotossicitaId,
        noteagguntiveetiId,
        etisostanzeattiveId,
        etichettafrasiId,
        etichettapittogrammiId,
        etichettatipofitofarmacoId,
        etiFitoToRaccordoId,
        titolareRegistrazioneId,
        formulazioneId
        );
    }

    @Override
    public String toString() {
        return "EtichettaFitoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomeCommerciale != null ? "nomeCommerciale=" + nomeCommerciale + ", " : "") +
                (dataReg != null ? "dataReg=" + dataReg + ", " : "") +
                (registrazioneMinisteriale != null ? "registrazioneMinisteriale=" + registrazioneMinisteriale + ", " : "") +
                (sigla != null ? "sigla=" + sigla + ", " : "") +
                (bio != null ? "bio=" + bio + ", " : "") +
                (ppo != null ? "ppo=" + ppo + ", " : "") +
                (revocato != null ? "revocato=" + revocato + ", " : "") +
                (revocaAutorizzazione != null ? "revocaAutorizzazione=" + revocaAutorizzazione + ", " : "") +
                (scadenzaCommercio != null ? "scadenzaCommercio=" + scadenzaCommercio + ", " : "") +
                (scadenzaUtilizzo != null ? "scadenzaUtilizzo=" + scadenzaUtilizzo + ", " : "") +
                (avvClp != null ? "avvClp=" + avvClp + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (incompatibilitaetichettaId != null ? "incompatibilitaetichettaId=" + incompatibilitaetichettaId + ", " : "") +
                (probfitotossicitaId != null ? "probfitotossicitaId=" + probfitotossicitaId + ", " : "") +
                (noteagguntiveetiId != null ? "noteagguntiveetiId=" + noteagguntiveetiId + ", " : "") +
                (etisostanzeattiveId != null ? "etisostanzeattiveId=" + etisostanzeattiveId + ", " : "") +
                (etichettafrasiId != null ? "etichettafrasiId=" + etichettafrasiId + ", " : "") +
                (etichettapittogrammiId != null ? "etichettapittogrammiId=" + etichettapittogrammiId + ", " : "") +
                (etichettatipofitofarmacoId != null ? "etichettatipofitofarmacoId=" + etichettatipofitofarmacoId + ", " : "") +
                (etiFitoToRaccordoId != null ? "etiFitoToRaccordoId=" + etiFitoToRaccordoId + ", " : "") +
                (titolareRegistrazioneId != null ? "titolareRegistrazioneId=" + titolareRegistrazioneId + ", " : "") +
                (formulazioneId != null ? "formulazioneId=" + formulazioneId + ", " : "") +
            "}";
    }

}
