package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Brogliaccio} entity. This class is used
 * in {@link com.terrambe.web.rest.BrogliaccioResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /brogliaccios?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BrogliaccioCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomeComune;

    private StringFilter codNaz;

    private StringFilter sez;

    private StringFilter foglio;

    private StringFilter part;

    private StringFilter sub;

    private StringFilter supCat;

    private StringFilter supGra;

    private StringFilter conduz;

    private StringFilter cP;

    private LocalDateFilter dataInizio;

    private LocalDateFilter dataFine;

    private StringFilter supUtil;

    private StringFilter supEleg;

    private StringFilter macrouso;

    private StringFilter occupazione;

    private StringFilter destinazione;

    private StringFilter uso;

    private StringFilter qualita;

    private StringFilter varieta;

    private InstantFilter dataImportazione;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter broglToAziAnaId;

    public BrogliaccioCriteria(){
    }

    public BrogliaccioCriteria(BrogliaccioCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nomeComune = other.nomeComune == null ? null : other.nomeComune.copy();
        this.codNaz = other.codNaz == null ? null : other.codNaz.copy();
        this.sez = other.sez == null ? null : other.sez.copy();
        this.foglio = other.foglio == null ? null : other.foglio.copy();
        this.part = other.part == null ? null : other.part.copy();
        this.sub = other.sub == null ? null : other.sub.copy();
        this.supCat = other.supCat == null ? null : other.supCat.copy();
        this.supGra = other.supGra == null ? null : other.supGra.copy();
        this.conduz = other.conduz == null ? null : other.conduz.copy();
        this.cP = other.cP == null ? null : other.cP.copy();
        this.dataInizio = other.dataInizio == null ? null : other.dataInizio.copy();
        this.dataFine = other.dataFine == null ? null : other.dataFine.copy();
        this.supUtil = other.supUtil == null ? null : other.supUtil.copy();
        this.supEleg = other.supEleg == null ? null : other.supEleg.copy();
        this.macrouso = other.macrouso == null ? null : other.macrouso.copy();
        this.occupazione = other.occupazione == null ? null : other.occupazione.copy();
        this.destinazione = other.destinazione == null ? null : other.destinazione.copy();
        this.uso = other.uso == null ? null : other.uso.copy();
        this.qualita = other.qualita == null ? null : other.qualita.copy();
        this.varieta = other.varieta == null ? null : other.varieta.copy();
        this.dataImportazione = other.dataImportazione == null ? null : other.dataImportazione.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.broglToAziAnaId = other.broglToAziAnaId == null ? null : other.broglToAziAnaId.copy();
    }

    @Override
    public BrogliaccioCriteria copy() {
        return new BrogliaccioCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomeComune() {
        return nomeComune;
    }

    public void setNomeComune(StringFilter nomeComune) {
        this.nomeComune = nomeComune;
    }

    public StringFilter getCodNaz() {
        return codNaz;
    }

    public void setCodNaz(StringFilter codNaz) {
        this.codNaz = codNaz;
    }

    public StringFilter getSez() {
        return sez;
    }

    public void setSez(StringFilter sez) {
        this.sez = sez;
    }

    public StringFilter getFoglio() {
        return foglio;
    }

    public void setFoglio(StringFilter foglio) {
        this.foglio = foglio;
    }

    public StringFilter getPart() {
        return part;
    }

    public void setPart(StringFilter part) {
        this.part = part;
    }

    public StringFilter getSub() {
        return sub;
    }

    public void setSub(StringFilter sub) {
        this.sub = sub;
    }

    public StringFilter getSupCat() {
        return supCat;
    }

    public void setSupCat(StringFilter supCat) {
        this.supCat = supCat;
    }

    public StringFilter getSupGra() {
        return supGra;
    }

    public void setSupGra(StringFilter supGra) {
        this.supGra = supGra;
    }

    public StringFilter getConduz() {
        return conduz;
    }

    public void setConduz(StringFilter conduz) {
        this.conduz = conduz;
    }

    public StringFilter getcP() {
        return cP;
    }

    public void setcP(StringFilter cP) {
        this.cP = cP;
    }

    public LocalDateFilter getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(LocalDateFilter dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDateFilter getDataFine() {
        return dataFine;
    }

    public void setDataFine(LocalDateFilter dataFine) {
        this.dataFine = dataFine;
    }

    public StringFilter getSupUtil() {
        return supUtil;
    }

    public void setSupUtil(StringFilter supUtil) {
        this.supUtil = supUtil;
    }

    public StringFilter getSupEleg() {
        return supEleg;
    }

    public void setSupEleg(StringFilter supEleg) {
        this.supEleg = supEleg;
    }

    public StringFilter getMacrouso() {
        return macrouso;
    }

    public void setMacrouso(StringFilter macrouso) {
        this.macrouso = macrouso;
    }

    public StringFilter getOccupazione() {
        return occupazione;
    }

    public void setOccupazione(StringFilter occupazione) {
        this.occupazione = occupazione;
    }

    public StringFilter getDestinazione() {
        return destinazione;
    }

    public void setDestinazione(StringFilter destinazione) {
        this.destinazione = destinazione;
    }

    public StringFilter getUso() {
        return uso;
    }

    public void setUso(StringFilter uso) {
        this.uso = uso;
    }

    public StringFilter getQualita() {
        return qualita;
    }

    public void setQualita(StringFilter qualita) {
        this.qualita = qualita;
    }

    public StringFilter getVarieta() {
        return varieta;
    }

    public void setVarieta(StringFilter varieta) {
        this.varieta = varieta;
    }

    public InstantFilter getDataImportazione() {
        return dataImportazione;
    }

    public void setDataImportazione(InstantFilter dataImportazione) {
        this.dataImportazione = dataImportazione;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getBroglToAziAnaId() {
        return broglToAziAnaId;
    }

    public void setBroglToAziAnaId(LongFilter broglToAziAnaId) {
        this.broglToAziAnaId = broglToAziAnaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BrogliaccioCriteria that = (BrogliaccioCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomeComune, that.nomeComune) &&
            Objects.equals(codNaz, that.codNaz) &&
            Objects.equals(sez, that.sez) &&
            Objects.equals(foglio, that.foglio) &&
            Objects.equals(part, that.part) &&
            Objects.equals(sub, that.sub) &&
            Objects.equals(supCat, that.supCat) &&
            Objects.equals(supGra, that.supGra) &&
            Objects.equals(conduz, that.conduz) &&
            Objects.equals(cP, that.cP) &&
            Objects.equals(dataInizio, that.dataInizio) &&
            Objects.equals(dataFine, that.dataFine) &&
            Objects.equals(supUtil, that.supUtil) &&
            Objects.equals(supEleg, that.supEleg) &&
            Objects.equals(macrouso, that.macrouso) &&
            Objects.equals(occupazione, that.occupazione) &&
            Objects.equals(destinazione, that.destinazione) &&
            Objects.equals(uso, that.uso) &&
            Objects.equals(qualita, that.qualita) &&
            Objects.equals(varieta, that.varieta) &&
            Objects.equals(dataImportazione, that.dataImportazione) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(broglToAziAnaId, that.broglToAziAnaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomeComune,
        codNaz,
        sez,
        foglio,
        part,
        sub,
        supCat,
        supGra,
        conduz,
        cP,
        dataInizio,
        dataFine,
        supUtil,
        supEleg,
        macrouso,
        occupazione,
        destinazione,
        uso,
        qualita,
        varieta,
        dataImportazione,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        broglToAziAnaId
        );
    }

    @Override
    public String toString() {
        return "BrogliaccioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomeComune != null ? "nomeComune=" + nomeComune + ", " : "") +
                (codNaz != null ? "codNaz=" + codNaz + ", " : "") +
                (sez != null ? "sez=" + sez + ", " : "") +
                (foglio != null ? "foglio=" + foglio + ", " : "") +
                (part != null ? "part=" + part + ", " : "") +
                (sub != null ? "sub=" + sub + ", " : "") +
                (supCat != null ? "supCat=" + supCat + ", " : "") +
                (supGra != null ? "supGra=" + supGra + ", " : "") +
                (conduz != null ? "conduz=" + conduz + ", " : "") +
                (cP != null ? "cP=" + cP + ", " : "") +
                (dataInizio != null ? "dataInizio=" + dataInizio + ", " : "") +
                (dataFine != null ? "dataFine=" + dataFine + ", " : "") +
                (supUtil != null ? "supUtil=" + supUtil + ", " : "") +
                (supEleg != null ? "supEleg=" + supEleg + ", " : "") +
                (macrouso != null ? "macrouso=" + macrouso + ", " : "") +
                (occupazione != null ? "occupazione=" + occupazione + ", " : "") +
                (destinazione != null ? "destinazione=" + destinazione + ", " : "") +
                (uso != null ? "uso=" + uso + ", " : "") +
                (qualita != null ? "qualita=" + qualita + ", " : "") +
                (varieta != null ? "varieta=" + varieta + ", " : "") +
                (dataImportazione != null ? "dataImportazione=" + dataImportazione + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (broglToAziAnaId != null ? "broglToAziAnaId=" + broglToAziAnaId + ", " : "") +
            "}";
    }

}
