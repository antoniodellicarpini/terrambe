package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Campi} entity. This class is used
 * in {@link com.terrambe.web.rest.CampiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /campis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private StringFilter nomeCampo;

    private FloatFilter superficieUtilizzata;

    private FloatFilter supCatastale;

    private FloatFilter supUtilizzabile;

    private StringFilter uuidEsri;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter campiToAziSupId;

    private LongFilter campiToAppezId;

    private LongFilter campiuniId;

    public CampiCriteria(){
    }

    public CampiCriteria(CampiCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.nomeCampo = other.nomeCampo == null ? null : other.nomeCampo.copy();
        this.superficieUtilizzata = other.superficieUtilizzata == null ? null : other.superficieUtilizzata.copy();
        this.supCatastale = other.supCatastale == null ? null : other.supCatastale.copy();
        this.supUtilizzabile = other.supUtilizzabile == null ? null : other.supUtilizzabile.copy();
        this.uuidEsri = other.uuidEsri == null ? null : other.uuidEsri.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.campiToAziSupId = other.campiToAziSupId == null ? null : other.campiToAziSupId.copy();
        this.campiToAppezId = other.campiToAppezId == null ? null : other.campiToAppezId.copy();
        this.campiuniId = other.campiuniId == null ? null : other.campiuniId.copy();
    }

    @Override
    public CampiCriteria copy() {
        return new CampiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public StringFilter getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(StringFilter nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public FloatFilter getSuperficieUtilizzata() {
        return superficieUtilizzata;
    }

    public void setSuperficieUtilizzata(FloatFilter superficieUtilizzata) {
        this.superficieUtilizzata = superficieUtilizzata;
    }

    public FloatFilter getSupCatastale() {
        return supCatastale;
    }

    public void setSupCatastale(FloatFilter supCatastale) {
        this.supCatastale = supCatastale;
    }

    public FloatFilter getSupUtilizzabile() {
        return supUtilizzabile;
    }

    public void setSupUtilizzabile(FloatFilter supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
    }

    public StringFilter getUuidEsri() {
        return uuidEsri;
    }

    public void setUuidEsri(StringFilter uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getCampiToAziSupId() {
        return campiToAziSupId;
    }

    public void setCampiToAziSupId(LongFilter campiToAziSupId) {
        this.campiToAziSupId = campiToAziSupId;
    }

    public LongFilter getCampiToAppezId() {
        return campiToAppezId;
    }

    public void setCampiToAppezId(LongFilter campiToAppezId) {
        this.campiToAppezId = campiToAppezId;
    }

    public LongFilter getCampiuniId() {
        return campiuniId;
    }

    public void setCampiuniId(LongFilter campiuniId) {
        this.campiuniId = campiuniId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CampiCriteria that = (CampiCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(nomeCampo, that.nomeCampo) &&
            Objects.equals(superficieUtilizzata, that.superficieUtilizzata) &&
            Objects.equals(supCatastale, that.supCatastale) &&
            Objects.equals(supUtilizzabile, that.supUtilizzabile) &&
            Objects.equals(uuidEsri, that.uuidEsri) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(campiToAziSupId, that.campiToAziSupId) &&
            Objects.equals(campiToAppezId, that.campiToAppezId) &&
            Objects.equals(campiuniId, that.campiuniId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        nomeCampo,
        superficieUtilizzata,
        supCatastale,
        supUtilizzabile,
        uuidEsri,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        campiToAziSupId,
        campiToAppezId,
        campiuniId
        );
    }

    @Override
    public String toString() {
        return "CampiCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (nomeCampo != null ? "nomeCampo=" + nomeCampo + ", " : "") +
                (superficieUtilizzata != null ? "superficieUtilizzata=" + superficieUtilizzata + ", " : "") +
                (supCatastale != null ? "supCatastale=" + supCatastale + ", " : "") +
                (supUtilizzabile != null ? "supUtilizzabile=" + supUtilizzabile + ", " : "") +
                (uuidEsri != null ? "uuidEsri=" + uuidEsri + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (campiToAziSupId != null ? "campiToAziSupId=" + campiToAziSupId + ", " : "") +
                (campiToAppezId != null ? "campiToAppezId=" + campiToAppezId + ", " : "") +
                (campiuniId != null ? "campiuniId=" + campiuniId + ", " : "") +
            "}";
    }

}
