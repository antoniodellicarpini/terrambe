package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Syslog} entity. This class is used
 * in {@link com.terrambe.web.rest.SyslogResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /syslogs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SyslogCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomeTabella;

    private IntegerFilter idJuser;

    private LocalDateFilter dataModifica;

    private StringFilter nomeCampo;

    private StringFilter vecchioValore;

    private StringFilter nuovoValore;

    public SyslogCriteria(){
    }

    public SyslogCriteria(SyslogCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nomeTabella = other.nomeTabella == null ? null : other.nomeTabella.copy();
        this.idJuser = other.idJuser == null ? null : other.idJuser.copy();
        this.dataModifica = other.dataModifica == null ? null : other.dataModifica.copy();
        this.nomeCampo = other.nomeCampo == null ? null : other.nomeCampo.copy();
        this.vecchioValore = other.vecchioValore == null ? null : other.vecchioValore.copy();
        this.nuovoValore = other.nuovoValore == null ? null : other.nuovoValore.copy();
    }

    @Override
    public SyslogCriteria copy() {
        return new SyslogCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomeTabella() {
        return nomeTabella;
    }

    public void setNomeTabella(StringFilter nomeTabella) {
        this.nomeTabella = nomeTabella;
    }

    public IntegerFilter getIdJuser() {
        return idJuser;
    }

    public void setIdJuser(IntegerFilter idJuser) {
        this.idJuser = idJuser;
    }

    public LocalDateFilter getDataModifica() {
        return dataModifica;
    }

    public void setDataModifica(LocalDateFilter dataModifica) {
        this.dataModifica = dataModifica;
    }

    public StringFilter getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(StringFilter nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public StringFilter getVecchioValore() {
        return vecchioValore;
    }

    public void setVecchioValore(StringFilter vecchioValore) {
        this.vecchioValore = vecchioValore;
    }

    public StringFilter getNuovoValore() {
        return nuovoValore;
    }

    public void setNuovoValore(StringFilter nuovoValore) {
        this.nuovoValore = nuovoValore;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SyslogCriteria that = (SyslogCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomeTabella, that.nomeTabella) &&
            Objects.equals(idJuser, that.idJuser) &&
            Objects.equals(dataModifica, that.dataModifica) &&
            Objects.equals(nomeCampo, that.nomeCampo) &&
            Objects.equals(vecchioValore, that.vecchioValore) &&
            Objects.equals(nuovoValore, that.nuovoValore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomeTabella,
        idJuser,
        dataModifica,
        nomeCampo,
        vecchioValore,
        nuovoValore
        );
    }

    @Override
    public String toString() {
        return "SyslogCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomeTabella != null ? "nomeTabella=" + nomeTabella + ", " : "") +
                (idJuser != null ? "idJuser=" + idJuser + ", " : "") +
                (dataModifica != null ? "dataModifica=" + dataModifica + ", " : "") +
                (nomeCampo != null ? "nomeCampo=" + nomeCampo + ", " : "") +
                (vecchioValore != null ? "vecchioValore=" + vecchioValore + ", " : "") +
                (nuovoValore != null ? "nuovoValore=" + nuovoValore + ", " : "") +
            "}";
    }

}
