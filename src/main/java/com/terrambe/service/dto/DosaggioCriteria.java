package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Dosaggio} entity. This class is used
 * in {@link com.terrambe.web.rest.DosaggioResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dosaggios?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DosaggioCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter carenzaC;

    private StringFilter carenzaP;

    private StringFilter carenzaD;

    private StringFilter carenzaO;

    private StringFilter carenzaT;

    private StringFilter carenzaNd;

    private StringFilter acquaHaMin;

    private StringFilter acquaHaMax;

    private StringFilter numMaxInt;

    private StringFilter rifMaxTratt;

    private StringFilter intervTratt;

    private StringFilter intervTrattMax;

    private StringFilter scadenzaDosi;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter dosagToDoseId;

    private LongFilter dosagToRaccordoId;

    public DosaggioCriteria(){
    }

    public DosaggioCriteria(DosaggioCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.carenzaC = other.carenzaC == null ? null : other.carenzaC.copy();
        this.carenzaP = other.carenzaP == null ? null : other.carenzaP.copy();
        this.carenzaD = other.carenzaD == null ? null : other.carenzaD.copy();
        this.carenzaO = other.carenzaO == null ? null : other.carenzaO.copy();
        this.carenzaT = other.carenzaT == null ? null : other.carenzaT.copy();
        this.carenzaNd = other.carenzaNd == null ? null : other.carenzaNd.copy();
        this.acquaHaMin = other.acquaHaMin == null ? null : other.acquaHaMin.copy();
        this.acquaHaMax = other.acquaHaMax == null ? null : other.acquaHaMax.copy();
        this.numMaxInt = other.numMaxInt == null ? null : other.numMaxInt.copy();
        this.rifMaxTratt = other.rifMaxTratt == null ? null : other.rifMaxTratt.copy();
        this.intervTratt = other.intervTratt == null ? null : other.intervTratt.copy();
        this.intervTrattMax = other.intervTrattMax == null ? null : other.intervTrattMax.copy();
        this.scadenzaDosi = other.scadenzaDosi == null ? null : other.scadenzaDosi.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.dosagToDoseId = other.dosagToDoseId == null ? null : other.dosagToDoseId.copy();
        this.dosagToRaccordoId = other.dosagToRaccordoId == null ? null : other.dosagToRaccordoId.copy();
    }

    @Override
    public DosaggioCriteria copy() {
        return new DosaggioCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCarenzaC() {
        return carenzaC;
    }

    public void setCarenzaC(StringFilter carenzaC) {
        this.carenzaC = carenzaC;
    }

    public StringFilter getCarenzaP() {
        return carenzaP;
    }

    public void setCarenzaP(StringFilter carenzaP) {
        this.carenzaP = carenzaP;
    }

    public StringFilter getCarenzaD() {
        return carenzaD;
    }

    public void setCarenzaD(StringFilter carenzaD) {
        this.carenzaD = carenzaD;
    }

    public StringFilter getCarenzaO() {
        return carenzaO;
    }

    public void setCarenzaO(StringFilter carenzaO) {
        this.carenzaO = carenzaO;
    }

    public StringFilter getCarenzaT() {
        return carenzaT;
    }

    public void setCarenzaT(StringFilter carenzaT) {
        this.carenzaT = carenzaT;
    }

    public StringFilter getCarenzaNd() {
        return carenzaNd;
    }

    public void setCarenzaNd(StringFilter carenzaNd) {
        this.carenzaNd = carenzaNd;
    }

    public StringFilter getAcquaHaMin() {
        return acquaHaMin;
    }

    public void setAcquaHaMin(StringFilter acquaHaMin) {
        this.acquaHaMin = acquaHaMin;
    }

    public StringFilter getAcquaHaMax() {
        return acquaHaMax;
    }

    public void setAcquaHaMax(StringFilter acquaHaMax) {
        this.acquaHaMax = acquaHaMax;
    }

    public StringFilter getNumMaxInt() {
        return numMaxInt;
    }

    public void setNumMaxInt(StringFilter numMaxInt) {
        this.numMaxInt = numMaxInt;
    }

    public StringFilter getRifMaxTratt() {
        return rifMaxTratt;
    }

    public void setRifMaxTratt(StringFilter rifMaxTratt) {
        this.rifMaxTratt = rifMaxTratt;
    }

    public StringFilter getIntervTratt() {
        return intervTratt;
    }

    public void setIntervTratt(StringFilter intervTratt) {
        this.intervTratt = intervTratt;
    }

    public StringFilter getIntervTrattMax() {
        return intervTrattMax;
    }

    public void setIntervTrattMax(StringFilter intervTrattMax) {
        this.intervTrattMax = intervTrattMax;
    }

    public StringFilter getScadenzaDosi() {
        return scadenzaDosi;
    }

    public void setScadenzaDosi(StringFilter scadenzaDosi) {
        this.scadenzaDosi = scadenzaDosi;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getDosagToDoseId() {
        return dosagToDoseId;
    }

    public void setDosagToDoseId(LongFilter dosagToDoseId) {
        this.dosagToDoseId = dosagToDoseId;
    }

    public LongFilter getDosagToRaccordoId() {
        return dosagToRaccordoId;
    }

    public void setDosagToRaccordoId(LongFilter dosagToRaccordoId) {
        this.dosagToRaccordoId = dosagToRaccordoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DosaggioCriteria that = (DosaggioCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(carenzaC, that.carenzaC) &&
            Objects.equals(carenzaP, that.carenzaP) &&
            Objects.equals(carenzaD, that.carenzaD) &&
            Objects.equals(carenzaO, that.carenzaO) &&
            Objects.equals(carenzaT, that.carenzaT) &&
            Objects.equals(carenzaNd, that.carenzaNd) &&
            Objects.equals(acquaHaMin, that.acquaHaMin) &&
            Objects.equals(acquaHaMax, that.acquaHaMax) &&
            Objects.equals(numMaxInt, that.numMaxInt) &&
            Objects.equals(rifMaxTratt, that.rifMaxTratt) &&
            Objects.equals(intervTratt, that.intervTratt) &&
            Objects.equals(intervTrattMax, that.intervTrattMax) &&
            Objects.equals(scadenzaDosi, that.scadenzaDosi) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(dosagToDoseId, that.dosagToDoseId) &&
            Objects.equals(dosagToRaccordoId, that.dosagToRaccordoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        carenzaC,
        carenzaP,
        carenzaD,
        carenzaO,
        carenzaT,
        carenzaNd,
        acquaHaMin,
        acquaHaMax,
        numMaxInt,
        rifMaxTratt,
        intervTratt,
        intervTrattMax,
        scadenzaDosi,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        dosagToDoseId,
        dosagToRaccordoId
        );
    }

    @Override
    public String toString() {
        return "DosaggioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (carenzaC != null ? "carenzaC=" + carenzaC + ", " : "") +
                (carenzaP != null ? "carenzaP=" + carenzaP + ", " : "") +
                (carenzaD != null ? "carenzaD=" + carenzaD + ", " : "") +
                (carenzaO != null ? "carenzaO=" + carenzaO + ", " : "") +
                (carenzaT != null ? "carenzaT=" + carenzaT + ", " : "") +
                (carenzaNd != null ? "carenzaNd=" + carenzaNd + ", " : "") +
                (acquaHaMin != null ? "acquaHaMin=" + acquaHaMin + ", " : "") +
                (acquaHaMax != null ? "acquaHaMax=" + acquaHaMax + ", " : "") +
                (numMaxInt != null ? "numMaxInt=" + numMaxInt + ", " : "") +
                (rifMaxTratt != null ? "rifMaxTratt=" + rifMaxTratt + ", " : "") +
                (intervTratt != null ? "intervTratt=" + intervTratt + ", " : "") +
                (intervTrattMax != null ? "intervTrattMax=" + intervTrattMax + ", " : "") +
                (scadenzaDosi != null ? "scadenzaDosi=" + scadenzaDosi + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (dosagToDoseId != null ? "dosagToDoseId=" + dosagToDoseId + ", " : "") +
                (dosagToRaccordoId != null ? "dosagToRaccordoId=" + dosagToRaccordoId + ", " : "") +
            "}";
    }

}
