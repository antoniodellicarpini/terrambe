package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcPrepterr} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcPrepterrResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-prepterrs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcPrepterrCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataInizOpc;

    private LocalDateFilter dataFineOpc;

    private StringFilter tempoImpiegato;

    private LongFilter idColtura;

    private LongFilter idOperatore;

    private LongFilter idMezzo;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opcPrepterrToAppzId;

    private LongFilter opcPrepterrToTipoId;

    public RegOpcPrepterrCriteria(){
    }

    public RegOpcPrepterrCriteria(RegOpcPrepterrCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataInizOpc = other.dataInizOpc == null ? null : other.dataInizOpc.copy();
        this.dataFineOpc = other.dataFineOpc == null ? null : other.dataFineOpc.copy();
        this.tempoImpiegato = other.tempoImpiegato == null ? null : other.tempoImpiegato.copy();
        this.idColtura = other.idColtura == null ? null : other.idColtura.copy();
        this.idOperatore = other.idOperatore == null ? null : other.idOperatore.copy();
        this.idMezzo = other.idMezzo == null ? null : other.idMezzo.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opcPrepterrToAppzId = other.opcPrepterrToAppzId == null ? null : other.opcPrepterrToAppzId.copy();
        this.opcPrepterrToTipoId = other.opcPrepterrToTipoId == null ? null : other.opcPrepterrToTipoId.copy();
    }

    @Override
    public RegOpcPrepterrCriteria copy() {
        return new RegOpcPrepterrCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataInizOpc() {
        return dataInizOpc;
    }

    public void setDataInizOpc(LocalDateFilter dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDateFilter getDataFineOpc() {
        return dataFineOpc;
    }

    public void setDataFineOpc(LocalDateFilter dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public StringFilter getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(StringFilter tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public LongFilter getIdColtura() {
        return idColtura;
    }

    public void setIdColtura(LongFilter idColtura) {
        this.idColtura = idColtura;
    }

    public LongFilter getIdOperatore() {
        return idOperatore;
    }

    public void setIdOperatore(LongFilter idOperatore) {
        this.idOperatore = idOperatore;
    }

    public LongFilter getIdMezzo() {
        return idMezzo;
    }

    public void setIdMezzo(LongFilter idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpcPrepterrToAppzId() {
        return opcPrepterrToAppzId;
    }

    public void setOpcPrepterrToAppzId(LongFilter opcPrepterrToAppzId) {
        this.opcPrepterrToAppzId = opcPrepterrToAppzId;
    }

    public LongFilter getOpcPrepterrToTipoId() {
        return opcPrepterrToTipoId;
    }

    public void setOpcPrepterrToTipoId(LongFilter opcPrepterrToTipoId) {
        this.opcPrepterrToTipoId = opcPrepterrToTipoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcPrepterrCriteria that = (RegOpcPrepterrCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataInizOpc, that.dataInizOpc) &&
            Objects.equals(dataFineOpc, that.dataFineOpc) &&
            Objects.equals(tempoImpiegato, that.tempoImpiegato) &&
            Objects.equals(idColtura, that.idColtura) &&
            Objects.equals(idOperatore, that.idOperatore) &&
            Objects.equals(idMezzo, that.idMezzo) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opcPrepterrToAppzId, that.opcPrepterrToAppzId) &&
            Objects.equals(opcPrepterrToTipoId, that.opcPrepterrToTipoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        dataInizOpc,
        dataFineOpc,
        tempoImpiegato,
        idColtura,
        idOperatore,
        idMezzo,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opcPrepterrToAppzId,
        opcPrepterrToTipoId
        );
    }

    @Override
    public String toString() {
        return "RegOpcPrepterrCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataInizOpc != null ? "dataInizOpc=" + dataInizOpc + ", " : "") +
                (dataFineOpc != null ? "dataFineOpc=" + dataFineOpc + ", " : "") +
                (tempoImpiegato != null ? "tempoImpiegato=" + tempoImpiegato + ", " : "") +
                (idColtura != null ? "idColtura=" + idColtura + ", " : "") +
                (idOperatore != null ? "idOperatore=" + idOperatore + ", " : "") +
                (idMezzo != null ? "idMezzo=" + idMezzo + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opcPrepterrToAppzId != null ? "opcPrepterrToAppzId=" + opcPrepterrToAppzId + ", " : "") +
                (opcPrepterrToTipoId != null ? "opcPrepterrToTipoId=" + opcPrepterrToTipoId + ", " : "") +
            "}";
    }

}
