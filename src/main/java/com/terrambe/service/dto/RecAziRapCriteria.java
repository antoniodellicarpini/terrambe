package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RecAziRap} entity. This class is used
 * in {@link com.terrambe.web.rest.RecAziRapResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /rec-azi-raps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RecAziRapCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter email;

    private StringFilter pec;

    private StringFilter telefono;

    private StringFilter fax;

    private StringFilter cell;

    private StringFilter cell2;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter recToAziRapId;

    public RecAziRapCriteria(){
    }

    public RecAziRapCriteria(RecAziRapCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.pec = other.pec == null ? null : other.pec.copy();
        this.telefono = other.telefono == null ? null : other.telefono.copy();
        this.fax = other.fax == null ? null : other.fax.copy();
        this.cell = other.cell == null ? null : other.cell.copy();
        this.cell2 = other.cell2 == null ? null : other.cell2.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.recToAziRapId = other.recToAziRapId == null ? null : other.recToAziRapId.copy();
    }

    @Override
    public RecAziRapCriteria copy() {
        return new RecAziRapCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPec() {
        return pec;
    }

    public void setPec(StringFilter pec) {
        this.pec = pec;
    }

    public StringFilter getTelefono() {
        return telefono;
    }

    public void setTelefono(StringFilter telefono) {
        this.telefono = telefono;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getCell() {
        return cell;
    }

    public void setCell(StringFilter cell) {
        this.cell = cell;
    }

    public StringFilter getCell2() {
        return cell2;
    }

    public void setCell2(StringFilter cell2) {
        this.cell2 = cell2;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getRecToAziRapId() {
        return recToAziRapId;
    }

    public void setRecToAziRapId(LongFilter recToAziRapId) {
        this.recToAziRapId = recToAziRapId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RecAziRapCriteria that = (RecAziRapCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(email, that.email) &&
            Objects.equals(pec, that.pec) &&
            Objects.equals(telefono, that.telefono) &&
            Objects.equals(fax, that.fax) &&
            Objects.equals(cell, that.cell) &&
            Objects.equals(cell2, that.cell2) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(recToAziRapId, that.recToAziRapId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        email,
        pec,
        telefono,
        fax,
        cell,
        cell2,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        recToAziRapId
        );
    }

    @Override
    public String toString() {
        return "RecAziRapCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (pec != null ? "pec=" + pec + ", " : "") +
                (telefono != null ? "telefono=" + telefono + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (cell != null ? "cell=" + cell + ", " : "") +
                (cell2 != null ? "cell2=" + cell2 + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (recToAziRapId != null ? "recToAziRapId=" + recToAziRapId + ", " : "") +
            "}";
    }

}
