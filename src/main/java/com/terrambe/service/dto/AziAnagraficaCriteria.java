package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.AziAnagrafica} entity. This class is used
 * in {@link com.terrambe.web.rest.AziAnagraficaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /azi-anagraficas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AziAnagraficaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cuaa;

    private StringFilter partitaiva;

    private StringFilter codicefiscale;

    private StringFilter codiceAteco;

    private StringFilter ragioneSociale;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private BooleanFilter attivo;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter aziAnaToCliId;

    private LongFilter aziToIndId;

    private LongFilter aziToRecId;

    private LongFilter aziAnaToBroglId;

    private LongFilter naturaGiuridicaId;

    private LongFilter aziAnaToAziRapId;

    private LongFilter aziAnaToUniAnaId;

    private LongFilter aziAnaToOpeAnaId;

    private LongFilter aziAnaToUsrTerId;

    public AziAnagraficaCriteria(){
    }

    public AziAnagraficaCriteria(AziAnagraficaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.cuaa = other.cuaa == null ? null : other.cuaa.copy();
        this.partitaiva = other.partitaiva == null ? null : other.partitaiva.copy();
        this.codicefiscale = other.codicefiscale == null ? null : other.codicefiscale.copy();
        this.codiceAteco = other.codiceAteco == null ? null : other.codiceAteco.copy();
        this.ragioneSociale = other.ragioneSociale == null ? null : other.ragioneSociale.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.attivo = other.attivo == null ? null : other.attivo.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.aziAnaToCliId = other.aziAnaToCliId == null ? null : other.aziAnaToCliId.copy();
        this.aziToIndId = other.aziToIndId == null ? null : other.aziToIndId.copy();
        this.aziToRecId = other.aziToRecId == null ? null : other.aziToRecId.copy();
        this.aziAnaToBroglId = other.aziAnaToBroglId == null ? null : other.aziAnaToBroglId.copy();
        this.naturaGiuridicaId = other.naturaGiuridicaId == null ? null : other.naturaGiuridicaId.copy();
        this.aziAnaToAziRapId = other.aziAnaToAziRapId == null ? null : other.aziAnaToAziRapId.copy();
        this.aziAnaToUniAnaId = other.aziAnaToUniAnaId == null ? null : other.aziAnaToUniAnaId.copy();
        this.aziAnaToOpeAnaId = other.aziAnaToOpeAnaId == null ? null : other.aziAnaToOpeAnaId.copy();
        this.aziAnaToUsrTerId = other.aziAnaToUsrTerId == null ? null : other.aziAnaToUsrTerId.copy();
    }

    @Override
    public AziAnagraficaCriteria copy() {
        return new AziAnagraficaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCuaa() {
        return cuaa;
    }

    public void setCuaa(StringFilter cuaa) {
        this.cuaa = cuaa;
    }

    public StringFilter getPartitaiva() {
        return partitaiva;
    }

    public void setPartitaiva(StringFilter partitaiva) {
        this.partitaiva = partitaiva;
    }

    public StringFilter getCodicefiscale() {
        return codicefiscale;
    }

    public void setCodicefiscale(StringFilter codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public StringFilter getCodiceAteco() {
        return codiceAteco;
    }

    public void setCodiceAteco(StringFilter codiceAteco) {
        this.codiceAteco = codiceAteco;
    }

    public StringFilter getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(StringFilter ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public BooleanFilter getAttivo() {
        return attivo;
    }

    public void setAttivo(BooleanFilter attivo) {
        this.attivo = attivo;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAziAnaToCliId() {
        return aziAnaToCliId;
    }

    public void setAziAnaToCliId(LongFilter aziAnaToCliId) {
        this.aziAnaToCliId = aziAnaToCliId;
    }

    public LongFilter getAziToIndId() {
        return aziToIndId;
    }

    public void setAziToIndId(LongFilter aziToIndId) {
        this.aziToIndId = aziToIndId;
    }

    public LongFilter getAziToRecId() {
        return aziToRecId;
    }

    public void setAziToRecId(LongFilter aziToRecId) {
        this.aziToRecId = aziToRecId;
    }

    public LongFilter getAziAnaToBroglId() {
        return aziAnaToBroglId;
    }

    public void setAziAnaToBroglId(LongFilter aziAnaToBroglId) {
        this.aziAnaToBroglId = aziAnaToBroglId;
    }

    public LongFilter getNaturaGiuridicaId() {
        return naturaGiuridicaId;
    }

    public void setNaturaGiuridicaId(LongFilter naturaGiuridicaId) {
        this.naturaGiuridicaId = naturaGiuridicaId;
    }

    public LongFilter getAziAnaToAziRapId() {
        return aziAnaToAziRapId;
    }

    public void setAziAnaToAziRapId(LongFilter aziAnaToAziRapId) {
        this.aziAnaToAziRapId = aziAnaToAziRapId;
    }

    public LongFilter getAziAnaToUniAnaId() {
        return aziAnaToUniAnaId;
    }

    public void setAziAnaToUniAnaId(LongFilter aziAnaToUniAnaId) {
        this.aziAnaToUniAnaId = aziAnaToUniAnaId;
    }

    public LongFilter getAziAnaToOpeAnaId() {
        return aziAnaToOpeAnaId;
    }

    public void setAziAnaToOpeAnaId(LongFilter aziAnaToOpeAnaId) {
        this.aziAnaToOpeAnaId = aziAnaToOpeAnaId;
    }

    public LongFilter getAziAnaToUsrTerId() {
        return aziAnaToUsrTerId;
    }

    public void setAziAnaToUsrTerId(LongFilter aziAnaToUsrTerId) {
        this.aziAnaToUsrTerId = aziAnaToUsrTerId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AziAnagraficaCriteria that = (AziAnagraficaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cuaa, that.cuaa) &&
            Objects.equals(partitaiva, that.partitaiva) &&
            Objects.equals(codicefiscale, that.codicefiscale) &&
            Objects.equals(codiceAteco, that.codiceAteco) &&
            Objects.equals(ragioneSociale, that.ragioneSociale) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(attivo, that.attivo) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(aziAnaToCliId, that.aziAnaToCliId) &&
            Objects.equals(aziToIndId, that.aziToIndId) &&
            Objects.equals(aziToRecId, that.aziToRecId) &&
            Objects.equals(aziAnaToBroglId, that.aziAnaToBroglId) &&
            Objects.equals(naturaGiuridicaId, that.naturaGiuridicaId) &&
            Objects.equals(aziAnaToAziRapId, that.aziAnaToAziRapId) &&
            Objects.equals(aziAnaToUniAnaId, that.aziAnaToUniAnaId) &&
            Objects.equals(aziAnaToOpeAnaId, that.aziAnaToOpeAnaId) &&
            Objects.equals(aziAnaToUsrTerId, that.aziAnaToUsrTerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cuaa,
        partitaiva,
        codicefiscale,
        codiceAteco,
        ragioneSociale,
        dataInizVali,
        dataFineVali,
        attivo,
        userIdCreator,
        userIdLastMod,
        aziAnaToCliId,
        aziToIndId,
        aziToRecId,
        aziAnaToBroglId,
        naturaGiuridicaId,
        aziAnaToAziRapId,
        aziAnaToUniAnaId,
        aziAnaToOpeAnaId,
        aziAnaToUsrTerId
        );
    }

    @Override
    public String toString() {
        return "AziAnagraficaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cuaa != null ? "cuaa=" + cuaa + ", " : "") +
                (partitaiva != null ? "partitaiva=" + partitaiva + ", " : "") +
                (codicefiscale != null ? "codicefiscale=" + codicefiscale + ", " : "") +
                (codiceAteco != null ? "codiceAteco=" + codiceAteco + ", " : "") +
                (ragioneSociale != null ? "ragioneSociale=" + ragioneSociale + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (attivo != null ? "attivo=" + attivo + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (aziAnaToCliId != null ? "aziAnaToCliId=" + aziAnaToCliId + ", " : "") +
                (aziToIndId != null ? "aziToIndId=" + aziToIndId + ", " : "") +
                (aziToRecId != null ? "aziToRecId=" + aziToRecId + ", " : "") +
                (aziAnaToBroglId != null ? "aziAnaToBroglId=" + aziAnaToBroglId + ", " : "") +
                (naturaGiuridicaId != null ? "naturaGiuridicaId=" + naturaGiuridicaId + ", " : "") +
                (aziAnaToAziRapId != null ? "aziAnaToAziRapId=" + aziAnaToAziRapId + ", " : "") +
                (aziAnaToUniAnaId != null ? "aziAnaToUniAnaId=" + aziAnaToUniAnaId + ", " : "") +
                (aziAnaToOpeAnaId != null ? "aziAnaToOpeAnaId=" + aziAnaToOpeAnaId + ", " : "") +
                (aziAnaToUsrTerId != null ? "aziAnaToUsrTerId=" + aziAnaToUsrTerId + ", " : "") +
            "}";
    }

}
