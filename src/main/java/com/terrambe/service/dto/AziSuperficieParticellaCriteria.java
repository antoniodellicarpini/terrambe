package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.AziSuperficieParticella} entity. This class is used
 * in {@link com.terrambe.web.rest.AziSuperficieParticellaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /azi-superficie-particellas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AziSuperficieParticellaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter foglio;

    private StringFilter part;

    private StringFilter sub;

    private StringFilter sezione;

    private FloatFilter superficie;

    private FloatFilter supCatastale;

    private FloatFilter supUtilizzabile;

    private FloatFilter supTara;

    private StringFilter uuidEsri;

    private StringFilter nazione;

    private StringFilter regione;

    private StringFilter provincia;

    private StringFilter comune;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter partToDettaglioId;

    private LongFilter conduzioneId;

    private LongFilter partToAziAnaId;

    private LongFilter aziSupToCampiId;

    private LongFilter partToAppzId;

    public AziSuperficieParticellaCriteria(){
    }

    public AziSuperficieParticellaCriteria(AziSuperficieParticellaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.foglio = other.foglio == null ? null : other.foglio.copy();
        this.part = other.part == null ? null : other.part.copy();
        this.sub = other.sub == null ? null : other.sub.copy();
        this.sezione = other.sezione == null ? null : other.sezione.copy();
        this.superficie = other.superficie == null ? null : other.superficie.copy();
        this.supCatastale = other.supCatastale == null ? null : other.supCatastale.copy();
        this.supUtilizzabile = other.supUtilizzabile == null ? null : other.supUtilizzabile.copy();
        this.supTara = other.supTara == null ? null : other.supTara.copy();
        this.uuidEsri = other.uuidEsri == null ? null : other.uuidEsri.copy();
        this.nazione = other.nazione == null ? null : other.nazione.copy();
        this.regione = other.regione == null ? null : other.regione.copy();
        this.provincia = other.provincia == null ? null : other.provincia.copy();
        this.comune = other.comune == null ? null : other.comune.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.partToDettaglioId = other.partToDettaglioId == null ? null : other.partToDettaglioId.copy();
        this.conduzioneId = other.conduzioneId == null ? null : other.conduzioneId.copy();
        this.partToAziAnaId = other.partToAziAnaId == null ? null : other.partToAziAnaId.copy();
        this.aziSupToCampiId = other.aziSupToCampiId == null ? null : other.aziSupToCampiId.copy();
        this.partToAppzId = other.partToAppzId == null ? null : other.partToAppzId.copy();
    }

    @Override
    public AziSuperficieParticellaCriteria copy() {
        return new AziSuperficieParticellaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFoglio() {
        return foglio;
    }

    public void setFoglio(StringFilter foglio) {
        this.foglio = foglio;
    }

    public StringFilter getPart() {
        return part;
    }

    public void setPart(StringFilter part) {
        this.part = part;
    }

    public StringFilter getSub() {
        return sub;
    }

    public void setSub(StringFilter sub) {
        this.sub = sub;
    }

    public StringFilter getSezione() {
        return sezione;
    }

    public void setSezione(StringFilter sezione) {
        this.sezione = sezione;
    }

    public FloatFilter getSuperficie() {
        return superficie;
    }

    public void setSuperficie(FloatFilter superficie) {
        this.superficie = superficie;
    }

    public FloatFilter getSupCatastale() {
        return supCatastale;
    }

    public void setSupCatastale(FloatFilter supCatastale) {
        this.supCatastale = supCatastale;
    }

    public FloatFilter getSupUtilizzabile() {
        return supUtilizzabile;
    }

    public void setSupUtilizzabile(FloatFilter supUtilizzabile) {
        this.supUtilizzabile = supUtilizzabile;
    }

    public FloatFilter getSupTara() {
        return supTara;
    }

    public void setSupTara(FloatFilter supTara) {
        this.supTara = supTara;
    }

    public StringFilter getUuidEsri() {
        return uuidEsri;
    }

    public void setUuidEsri(StringFilter uuidEsri) {
        this.uuidEsri = uuidEsri;
    }

    public StringFilter getNazione() {
        return nazione;
    }

    public void setNazione(StringFilter nazione) {
        this.nazione = nazione;
    }

    public StringFilter getRegione() {
        return regione;
    }

    public void setRegione(StringFilter regione) {
        this.regione = regione;
    }

    public StringFilter getProvincia() {
        return provincia;
    }

    public void setProvincia(StringFilter provincia) {
        this.provincia = provincia;
    }

    public StringFilter getComune() {
        return comune;
    }

    public void setComune(StringFilter comune) {
        this.comune = comune;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getPartToDettaglioId() {
        return partToDettaglioId;
    }

    public void setPartToDettaglioId(LongFilter partToDettaglioId) {
        this.partToDettaglioId = partToDettaglioId;
    }

    public LongFilter getConduzioneId() {
        return conduzioneId;
    }

    public void setConduzioneId(LongFilter conduzioneId) {
        this.conduzioneId = conduzioneId;
    }

    public LongFilter getPartToAziAnaId() {
        return partToAziAnaId;
    }

    public void setPartToAziAnaId(LongFilter partToAziAnaId) {
        this.partToAziAnaId = partToAziAnaId;
    }

    public LongFilter getAziSupToCampiId() {
        return aziSupToCampiId;
    }

    public void setAziSupToCampiId(LongFilter aziSupToCampiId) {
        this.aziSupToCampiId = aziSupToCampiId;
    }

    public LongFilter getPartToAppzId() {
        return partToAppzId;
    }

    public void setPartToAppzId(LongFilter partToAppzId) {
        this.partToAppzId = partToAppzId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AziSuperficieParticellaCriteria that = (AziSuperficieParticellaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(foglio, that.foglio) &&
            Objects.equals(part, that.part) &&
            Objects.equals(sub, that.sub) &&
            Objects.equals(sezione, that.sezione) &&
            Objects.equals(superficie, that.superficie) &&
            Objects.equals(supCatastale, that.supCatastale) &&
            Objects.equals(supUtilizzabile, that.supUtilizzabile) &&
            Objects.equals(supTara, that.supTara) &&
            Objects.equals(uuidEsri, that.uuidEsri) &&
            Objects.equals(nazione, that.nazione) &&
            Objects.equals(regione, that.regione) &&
            Objects.equals(provincia, that.provincia) &&
            Objects.equals(comune, that.comune) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(partToDettaglioId, that.partToDettaglioId) &&
            Objects.equals(conduzioneId, that.conduzioneId) &&
            Objects.equals(partToAziAnaId, that.partToAziAnaId) &&
            Objects.equals(aziSupToCampiId, that.aziSupToCampiId) &&
            Objects.equals(partToAppzId, that.partToAppzId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        foglio,
        part,
        sub,
        sezione,
        superficie,
        supCatastale,
        supUtilizzabile,
        supTara,
        uuidEsri,
        nazione,
        regione,
        provincia,
        comune,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        partToDettaglioId,
        conduzioneId,
        partToAziAnaId,
        aziSupToCampiId,
        partToAppzId
        );
    }

    @Override
    public String toString() {
        return "AziSuperficieParticellaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (foglio != null ? "foglio=" + foglio + ", " : "") +
                (part != null ? "part=" + part + ", " : "") +
                (sub != null ? "sub=" + sub + ", " : "") +
                (sezione != null ? "sezione=" + sezione + ", " : "") +
                (superficie != null ? "superficie=" + superficie + ", " : "") +
                (supCatastale != null ? "supCatastale=" + supCatastale + ", " : "") +
                (supUtilizzabile != null ? "supUtilizzabile=" + supUtilizzabile + ", " : "") +
                (supTara != null ? "supTara=" + supTara + ", " : "") +
                (uuidEsri != null ? "uuidEsri=" + uuidEsri + ", " : "") +
                (nazione != null ? "nazione=" + nazione + ", " : "") +
                (regione != null ? "regione=" + regione + ", " : "") +
                (provincia != null ? "provincia=" + provincia + ", " : "") +
                (comune != null ? "comune=" + comune + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (partToDettaglioId != null ? "partToDettaglioId=" + partToDettaglioId + ", " : "") +
                (conduzioneId != null ? "conduzioneId=" + conduzioneId + ", " : "") +
                (partToAziAnaId != null ? "partToAziAnaId=" + partToAziAnaId + ", " : "") +
                (aziSupToCampiId != null ? "aziSupToCampiId=" + aziSupToCampiId + ", " : "") +
                (partToAppzId != null ? "partToAppzId=" + partToAppzId + ", " : "") +
            "}";
    }

}
