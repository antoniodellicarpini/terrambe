package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Agea20152020Matrice} entity. This class is used
 * in {@link com.terrambe.web.rest.Agea20152020MatriceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /agea-20152020-matrices?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Agea20152020MatriceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter descColtura;

    private StringFilter codMacroUso;

    private StringFilter macroUso;

    private StringFilter codOccupazioneSuolo;

    private StringFilter occupazioneSuolo;

    private StringFilter codDestinazione;

    private StringFilter destinazione;

    private StringFilter codUso;

    private StringFilter uso;

    private StringFilter codQualita;

    private StringFilter qualita;

    private StringFilter codVarieta;

    private StringFilter varieta;

    private StringFilter codFamiglia;

    private StringFilter famiglia;

    private StringFilter codGenere;

    private StringFilter genere;

    private StringFilter codSpecie;

    private StringFilter specie;

    private StringFilter versioneInserimento;

    private StringFilter codiceCompostoAgea;

    private LongFilter ageaToDettId;

    private LongFilter ageaToColtId;

    private LongFilter matriceToBdfId;

    public Agea20152020MatriceCriteria(){
    }

    public Agea20152020MatriceCriteria(Agea20152020MatriceCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.descColtura = other.descColtura == null ? null : other.descColtura.copy();
        this.codMacroUso = other.codMacroUso == null ? null : other.codMacroUso.copy();
        this.macroUso = other.macroUso == null ? null : other.macroUso.copy();
        this.codOccupazioneSuolo = other.codOccupazioneSuolo == null ? null : other.codOccupazioneSuolo.copy();
        this.occupazioneSuolo = other.occupazioneSuolo == null ? null : other.occupazioneSuolo.copy();
        this.codDestinazione = other.codDestinazione == null ? null : other.codDestinazione.copy();
        this.destinazione = other.destinazione == null ? null : other.destinazione.copy();
        this.codUso = other.codUso == null ? null : other.codUso.copy();
        this.uso = other.uso == null ? null : other.uso.copy();
        this.codQualita = other.codQualita == null ? null : other.codQualita.copy();
        this.qualita = other.qualita == null ? null : other.qualita.copy();
        this.codVarieta = other.codVarieta == null ? null : other.codVarieta.copy();
        this.varieta = other.varieta == null ? null : other.varieta.copy();
        this.codFamiglia = other.codFamiglia == null ? null : other.codFamiglia.copy();
        this.famiglia = other.famiglia == null ? null : other.famiglia.copy();
        this.codGenere = other.codGenere == null ? null : other.codGenere.copy();
        this.genere = other.genere == null ? null : other.genere.copy();
        this.codSpecie = other.codSpecie == null ? null : other.codSpecie.copy();
        this.specie = other.specie == null ? null : other.specie.copy();
        this.versioneInserimento = other.versioneInserimento == null ? null : other.versioneInserimento.copy();
        this.codiceCompostoAgea = other.codiceCompostoAgea == null ? null : other.codiceCompostoAgea.copy();
        this.ageaToDettId = other.ageaToDettId == null ? null : other.ageaToDettId.copy();
        this.ageaToColtId = other.ageaToColtId == null ? null : other.ageaToColtId.copy();
        this.matriceToBdfId = other.matriceToBdfId == null ? null : other.matriceToBdfId.copy();
    }

    @Override
    public Agea20152020MatriceCriteria copy() {
        return new Agea20152020MatriceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescColtura() {
        return descColtura;
    }

    public void setDescColtura(StringFilter descColtura) {
        this.descColtura = descColtura;
    }

    public StringFilter getCodMacroUso() {
        return codMacroUso;
    }

    public void setCodMacroUso(StringFilter codMacroUso) {
        this.codMacroUso = codMacroUso;
    }

    public StringFilter getMacroUso() {
        return macroUso;
    }

    public void setMacroUso(StringFilter macroUso) {
        this.macroUso = macroUso;
    }

    public StringFilter getCodOccupazioneSuolo() {
        return codOccupazioneSuolo;
    }

    public void setCodOccupazioneSuolo(StringFilter codOccupazioneSuolo) {
        this.codOccupazioneSuolo = codOccupazioneSuolo;
    }

    public StringFilter getOccupazioneSuolo() {
        return occupazioneSuolo;
    }

    public void setOccupazioneSuolo(StringFilter occupazioneSuolo) {
        this.occupazioneSuolo = occupazioneSuolo;
    }

    public StringFilter getCodDestinazione() {
        return codDestinazione;
    }

    public void setCodDestinazione(StringFilter codDestinazione) {
        this.codDestinazione = codDestinazione;
    }

    public StringFilter getDestinazione() {
        return destinazione;
    }

    public void setDestinazione(StringFilter destinazione) {
        this.destinazione = destinazione;
    }

    public StringFilter getCodUso() {
        return codUso;
    }

    public void setCodUso(StringFilter codUso) {
        this.codUso = codUso;
    }

    public StringFilter getUso() {
        return uso;
    }

    public void setUso(StringFilter uso) {
        this.uso = uso;
    }

    public StringFilter getCodQualita() {
        return codQualita;
    }

    public void setCodQualita(StringFilter codQualita) {
        this.codQualita = codQualita;
    }

    public StringFilter getQualita() {
        return qualita;
    }

    public void setQualita(StringFilter qualita) {
        this.qualita = qualita;
    }

    public StringFilter getCodVarieta() {
        return codVarieta;
    }

    public void setCodVarieta(StringFilter codVarieta) {
        this.codVarieta = codVarieta;
    }

    public StringFilter getVarieta() {
        return varieta;
    }

    public void setVarieta(StringFilter varieta) {
        this.varieta = varieta;
    }

    public StringFilter getCodFamiglia() {
        return codFamiglia;
    }

    public void setCodFamiglia(StringFilter codFamiglia) {
        this.codFamiglia = codFamiglia;
    }

    public StringFilter getFamiglia() {
        return famiglia;
    }

    public void setFamiglia(StringFilter famiglia) {
        this.famiglia = famiglia;
    }

    public StringFilter getCodGenere() {
        return codGenere;
    }

    public void setCodGenere(StringFilter codGenere) {
        this.codGenere = codGenere;
    }

    public StringFilter getGenere() {
        return genere;
    }

    public void setGenere(StringFilter genere) {
        this.genere = genere;
    }

    public StringFilter getCodSpecie() {
        return codSpecie;
    }

    public void setCodSpecie(StringFilter codSpecie) {
        this.codSpecie = codSpecie;
    }

    public StringFilter getSpecie() {
        return specie;
    }

    public void setSpecie(StringFilter specie) {
        this.specie = specie;
    }

    public StringFilter getVersioneInserimento() {
        return versioneInserimento;
    }

    public void setVersioneInserimento(StringFilter versioneInserimento) {
        this.versioneInserimento = versioneInserimento;
    }

    public StringFilter getCodiceCompostoAgea() {
        return codiceCompostoAgea;
    }

    public void setCodiceCompostoAgea(StringFilter codiceCompostoAgea) {
        this.codiceCompostoAgea = codiceCompostoAgea;
    }

    public LongFilter getAgeaToDettId() {
        return ageaToDettId;
    }

    public void setAgeaToDettId(LongFilter ageaToDettId) {
        this.ageaToDettId = ageaToDettId;
    }

    public LongFilter getAgeaToColtId() {
        return ageaToColtId;
    }

    public void setAgeaToColtId(LongFilter ageaToColtId) {
        this.ageaToColtId = ageaToColtId;
    }

    public LongFilter getMatriceToBdfId() {
        return matriceToBdfId;
    }

    public void setMatriceToBdfId(LongFilter matriceToBdfId) {
        this.matriceToBdfId = matriceToBdfId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Agea20152020MatriceCriteria that = (Agea20152020MatriceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(descColtura, that.descColtura) &&
            Objects.equals(codMacroUso, that.codMacroUso) &&
            Objects.equals(macroUso, that.macroUso) &&
            Objects.equals(codOccupazioneSuolo, that.codOccupazioneSuolo) &&
            Objects.equals(occupazioneSuolo, that.occupazioneSuolo) &&
            Objects.equals(codDestinazione, that.codDestinazione) &&
            Objects.equals(destinazione, that.destinazione) &&
            Objects.equals(codUso, that.codUso) &&
            Objects.equals(uso, that.uso) &&
            Objects.equals(codQualita, that.codQualita) &&
            Objects.equals(qualita, that.qualita) &&
            Objects.equals(codVarieta, that.codVarieta) &&
            Objects.equals(varieta, that.varieta) &&
            Objects.equals(codFamiglia, that.codFamiglia) &&
            Objects.equals(famiglia, that.famiglia) &&
            Objects.equals(codGenere, that.codGenere) &&
            Objects.equals(genere, that.genere) &&
            Objects.equals(codSpecie, that.codSpecie) &&
            Objects.equals(specie, that.specie) &&
            Objects.equals(versioneInserimento, that.versioneInserimento) &&
            Objects.equals(codiceCompostoAgea, that.codiceCompostoAgea) &&
            Objects.equals(ageaToDettId, that.ageaToDettId) &&
            Objects.equals(ageaToColtId, that.ageaToColtId) &&
            Objects.equals(matriceToBdfId, that.matriceToBdfId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        descColtura,
        codMacroUso,
        macroUso,
        codOccupazioneSuolo,
        occupazioneSuolo,
        codDestinazione,
        destinazione,
        codUso,
        uso,
        codQualita,
        qualita,
        codVarieta,
        varieta,
        codFamiglia,
        famiglia,
        codGenere,
        genere,
        codSpecie,
        specie,
        versioneInserimento,
        codiceCompostoAgea,
        ageaToDettId,
        ageaToColtId,
        matriceToBdfId
        );
    }

    @Override
    public String toString() {
        return "Agea20152020MatriceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (descColtura != null ? "descColtura=" + descColtura + ", " : "") +
                (codMacroUso != null ? "codMacroUso=" + codMacroUso + ", " : "") +
                (macroUso != null ? "macroUso=" + macroUso + ", " : "") +
                (codOccupazioneSuolo != null ? "codOccupazioneSuolo=" + codOccupazioneSuolo + ", " : "") +
                (occupazioneSuolo != null ? "occupazioneSuolo=" + occupazioneSuolo + ", " : "") +
                (codDestinazione != null ? "codDestinazione=" + codDestinazione + ", " : "") +
                (destinazione != null ? "destinazione=" + destinazione + ", " : "") +
                (codUso != null ? "codUso=" + codUso + ", " : "") +
                (uso != null ? "uso=" + uso + ", " : "") +
                (codQualita != null ? "codQualita=" + codQualita + ", " : "") +
                (qualita != null ? "qualita=" + qualita + ", " : "") +
                (codVarieta != null ? "codVarieta=" + codVarieta + ", " : "") +
                (varieta != null ? "varieta=" + varieta + ", " : "") +
                (codFamiglia != null ? "codFamiglia=" + codFamiglia + ", " : "") +
                (famiglia != null ? "famiglia=" + famiglia + ", " : "") +
                (codGenere != null ? "codGenere=" + codGenere + ", " : "") +
                (genere != null ? "genere=" + genere + ", " : "") +
                (codSpecie != null ? "codSpecie=" + codSpecie + ", " : "") +
                (specie != null ? "specie=" + specie + ", " : "") +
                (versioneInserimento != null ? "versioneInserimento=" + versioneInserimento + ", " : "") +
                (codiceCompostoAgea != null ? "codiceCompostoAgea=" + codiceCompostoAgea + ", " : "") +
                (ageaToDettId != null ? "ageaToDettId=" + ageaToDettId + ", " : "") +
                (ageaToColtId != null ? "ageaToColtId=" + ageaToColtId + ", " : "") +
                (matriceToBdfId != null ? "matriceToBdfId=" + matriceToBdfId + ", " : "") +
            "}";
    }

}
