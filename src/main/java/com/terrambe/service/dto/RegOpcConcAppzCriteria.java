package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcConcAppz} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcConcAppzResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-conc-appzs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcConcAppzCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private FloatFilter percentLavorata;

    private FloatFilter valN;

    private FloatFilter valP;

    private FloatFilter valK;

    private LongFilter idAppezzamento;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter appzToOpcConcId;

    public RegOpcConcAppzCriteria(){
    }

    public RegOpcConcAppzCriteria(RegOpcConcAppzCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.percentLavorata = other.percentLavorata == null ? null : other.percentLavorata.copy();
        this.valN = other.valN == null ? null : other.valN.copy();
        this.valP = other.valP == null ? null : other.valP.copy();
        this.valK = other.valK == null ? null : other.valK.copy();
        this.idAppezzamento = other.idAppezzamento == null ? null : other.idAppezzamento.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.appzToOpcConcId = other.appzToOpcConcId == null ? null : other.appzToOpcConcId.copy();
    }

    @Override
    public RegOpcConcAppzCriteria copy() {
        return new RegOpcConcAppzCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public FloatFilter getPercentLavorata() {
        return percentLavorata;
    }

    public void setPercentLavorata(FloatFilter percentLavorata) {
        this.percentLavorata = percentLavorata;
    }

    public FloatFilter getValN() {
        return valN;
    }

    public void setValN(FloatFilter valN) {
        this.valN = valN;
    }

    public FloatFilter getValP() {
        return valP;
    }

    public void setValP(FloatFilter valP) {
        this.valP = valP;
    }

    public FloatFilter getValK() {
        return valK;
    }

    public void setValK(FloatFilter valK) {
        this.valK = valK;
    }

    public LongFilter getIdAppezzamento() {
        return idAppezzamento;
    }

    public void setIdAppezzamento(LongFilter idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAppzToOpcConcId() {
        return appzToOpcConcId;
    }

    public void setAppzToOpcConcId(LongFilter appzToOpcConcId) {
        this.appzToOpcConcId = appzToOpcConcId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcConcAppzCriteria that = (RegOpcConcAppzCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(percentLavorata, that.percentLavorata) &&
            Objects.equals(valN, that.valN) &&
            Objects.equals(valP, that.valP) &&
            Objects.equals(valK, that.valK) &&
            Objects.equals(idAppezzamento, that.idAppezzamento) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(appzToOpcConcId, that.appzToOpcConcId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        percentLavorata,
        valN,
        valP,
        valK,
        idAppezzamento,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        appzToOpcConcId
        );
    }

    @Override
    public String toString() {
        return "RegOpcConcAppzCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (percentLavorata != null ? "percentLavorata=" + percentLavorata + ", " : "") +
                (valN != null ? "valN=" + valN + ", " : "") +
                (valP != null ? "valP=" + valP + ", " : "") +
                (valK != null ? "valK=" + valK + ", " : "") +
                (idAppezzamento != null ? "idAppezzamento=" + idAppezzamento + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (appzToOpcConcId != null ? "appzToOpcConcId=" + appzToOpcConcId + ", " : "") +
            "}";
    }

}
