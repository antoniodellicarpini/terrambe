package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcRacc} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcRaccResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-raccs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcRaccCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataInizOpc;

    private LocalDateFilter dataFineOpc;

    private StringFilter tempoImpiegato;

    private StringFilter locazioneConferimento;

    private FloatFilter quantita;

    private LongFilter idColtura;

    private LongFilter idOperatore;

    private LongFilter idMezzo;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opcRaccToAppzId;

    private LongFilter opcRaccToTipoId;

    public RegOpcRaccCriteria(){
    }

    public RegOpcRaccCriteria(RegOpcRaccCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataInizOpc = other.dataInizOpc == null ? null : other.dataInizOpc.copy();
        this.dataFineOpc = other.dataFineOpc == null ? null : other.dataFineOpc.copy();
        this.tempoImpiegato = other.tempoImpiegato == null ? null : other.tempoImpiegato.copy();
        this.locazioneConferimento = other.locazioneConferimento == null ? null : other.locazioneConferimento.copy();
        this.quantita = other.quantita == null ? null : other.quantita.copy();
        this.idColtura = other.idColtura == null ? null : other.idColtura.copy();
        this.idOperatore = other.idOperatore == null ? null : other.idOperatore.copy();
        this.idMezzo = other.idMezzo == null ? null : other.idMezzo.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opcRaccToAppzId = other.opcRaccToAppzId == null ? null : other.opcRaccToAppzId.copy();
        this.opcRaccToTipoId = other.opcRaccToTipoId == null ? null : other.opcRaccToTipoId.copy();
    }

    @Override
    public RegOpcRaccCriteria copy() {
        return new RegOpcRaccCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataInizOpc() {
        return dataInizOpc;
    }

    public void setDataInizOpc(LocalDateFilter dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDateFilter getDataFineOpc() {
        return dataFineOpc;
    }

    public void setDataFineOpc(LocalDateFilter dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public StringFilter getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(StringFilter tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public StringFilter getLocazioneConferimento() {
        return locazioneConferimento;
    }

    public void setLocazioneConferimento(StringFilter locazioneConferimento) {
        this.locazioneConferimento = locazioneConferimento;
    }

    public FloatFilter getQuantita() {
        return quantita;
    }

    public void setQuantita(FloatFilter quantita) {
        this.quantita = quantita;
    }

    public LongFilter getIdColtura() {
        return idColtura;
    }

    public void setIdColtura(LongFilter idColtura) {
        this.idColtura = idColtura;
    }

    public LongFilter getIdOperatore() {
        return idOperatore;
    }

    public void setIdOperatore(LongFilter idOperatore) {
        this.idOperatore = idOperatore;
    }

    public LongFilter getIdMezzo() {
        return idMezzo;
    }

    public void setIdMezzo(LongFilter idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpcRaccToAppzId() {
        return opcRaccToAppzId;
    }

    public void setOpcRaccToAppzId(LongFilter opcRaccToAppzId) {
        this.opcRaccToAppzId = opcRaccToAppzId;
    }

    public LongFilter getOpcRaccToTipoId() {
        return opcRaccToTipoId;
    }

    public void setOpcRaccToTipoId(LongFilter opcRaccToTipoId) {
        this.opcRaccToTipoId = opcRaccToTipoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcRaccCriteria that = (RegOpcRaccCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataInizOpc, that.dataInizOpc) &&
            Objects.equals(dataFineOpc, that.dataFineOpc) &&
            Objects.equals(tempoImpiegato, that.tempoImpiegato) &&
            Objects.equals(locazioneConferimento, that.locazioneConferimento) &&
            Objects.equals(quantita, that.quantita) &&
            Objects.equals(idColtura, that.idColtura) &&
            Objects.equals(idOperatore, that.idOperatore) &&
            Objects.equals(idMezzo, that.idMezzo) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opcRaccToAppzId, that.opcRaccToAppzId) &&
            Objects.equals(opcRaccToTipoId, that.opcRaccToTipoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        dataInizOpc,
        dataFineOpc,
        tempoImpiegato,
        locazioneConferimento,
        quantita,
        idColtura,
        idOperatore,
        idMezzo,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opcRaccToAppzId,
        opcRaccToTipoId
        );
    }

    @Override
    public String toString() {
        return "RegOpcRaccCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataInizOpc != null ? "dataInizOpc=" + dataInizOpc + ", " : "") +
                (dataFineOpc != null ? "dataFineOpc=" + dataFineOpc + ", " : "") +
                (tempoImpiegato != null ? "tempoImpiegato=" + tempoImpiegato + ", " : "") +
                (locazioneConferimento != null ? "locazioneConferimento=" + locazioneConferimento + ", " : "") +
                (quantita != null ? "quantita=" + quantita + ", " : "") +
                (idColtura != null ? "idColtura=" + idColtura + ", " : "") +
                (idOperatore != null ? "idOperatore=" + idOperatore + ", " : "") +
                (idMezzo != null ? "idMezzo=" + idMezzo + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opcRaccToAppzId != null ? "opcRaccToAppzId=" + opcRaccToAppzId + ", " : "") +
                (opcRaccToTipoId != null ? "opcRaccToTipoId=" + opcRaccToTipoId + ", " : "") +
            "}";
    }

}
