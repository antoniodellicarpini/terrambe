package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.IndAziRap} entity. This class is used
 * in {@link com.terrambe.web.rest.IndAziRapResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ind-azi-raps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class IndAziRapCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter indirizzo;

    private StringFilter cap;

    private StringFilter nazione;

    private StringFilter regione;

    private StringFilter provincia;

    private StringFilter comune;

    private StringFilter numeroCiv;

    private DoubleFilter latitude;

    private DoubleFilter longitude;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter indToAziRapId;

    public IndAziRapCriteria(){
    }

    public IndAziRapCriteria(IndAziRapCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.indirizzo = other.indirizzo == null ? null : other.indirizzo.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.nazione = other.nazione == null ? null : other.nazione.copy();
        this.regione = other.regione == null ? null : other.regione.copy();
        this.provincia = other.provincia == null ? null : other.provincia.copy();
        this.comune = other.comune == null ? null : other.comune.copy();
        this.numeroCiv = other.numeroCiv == null ? null : other.numeroCiv.copy();
        this.latitude = other.latitude == null ? null : other.latitude.copy();
        this.longitude = other.longitude == null ? null : other.longitude.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.indToAziRapId = other.indToAziRapId == null ? null : other.indToAziRapId.copy();
    }

    @Override
    public IndAziRapCriteria copy() {
        return new IndAziRapCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(StringFilter indirizzo) {
        this.indirizzo = indirizzo;
    }

    public StringFilter getCap() {
        return cap;
    }

    public void setCap(StringFilter cap) {
        this.cap = cap;
    }

    public StringFilter getNazione() {
        return nazione;
    }

    public void setNazione(StringFilter nazione) {
        this.nazione = nazione;
    }

    public StringFilter getRegione() {
        return regione;
    }

    public void setRegione(StringFilter regione) {
        this.regione = regione;
    }

    public StringFilter getProvincia() {
        return provincia;
    }

    public void setProvincia(StringFilter provincia) {
        this.provincia = provincia;
    }

    public StringFilter getComune() {
        return comune;
    }

    public void setComune(StringFilter comune) {
        this.comune = comune;
    }

    public StringFilter getNumeroCiv() {
        return numeroCiv;
    }

    public void setNumeroCiv(StringFilter numeroCiv) {
        this.numeroCiv = numeroCiv;
    }

    public DoubleFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(DoubleFilter latitude) {
        this.latitude = latitude;
    }

    public DoubleFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(DoubleFilter longitude) {
        this.longitude = longitude;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getIndToAziRapId() {
        return indToAziRapId;
    }

    public void setIndToAziRapId(LongFilter indToAziRapId) {
        this.indToAziRapId = indToAziRapId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final IndAziRapCriteria that = (IndAziRapCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(indirizzo, that.indirizzo) &&
            Objects.equals(cap, that.cap) &&
            Objects.equals(nazione, that.nazione) &&
            Objects.equals(regione, that.regione) &&
            Objects.equals(provincia, that.provincia) &&
            Objects.equals(comune, that.comune) &&
            Objects.equals(numeroCiv, that.numeroCiv) &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(indToAziRapId, that.indToAziRapId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        indirizzo,
        cap,
        nazione,
        regione,
        provincia,
        comune,
        numeroCiv,
        latitude,
        longitude,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        indToAziRapId
        );
    }

    @Override
    public String toString() {
        return "IndAziRapCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (indirizzo != null ? "indirizzo=" + indirizzo + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (nazione != null ? "nazione=" + nazione + ", " : "") +
                (regione != null ? "regione=" + regione + ", " : "") +
                (provincia != null ? "provincia=" + provincia + ", " : "") +
                (comune != null ? "comune=" + comune + ", " : "") +
                (numeroCiv != null ? "numeroCiv=" + numeroCiv + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (indToAziRapId != null ? "indToAziRapId=" + indToAziRapId + ", " : "") +
            "}";
    }

}
