package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Avversita} entity. This class is used
 * in {@link com.terrambe.web.rest.AvversitaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /avversitas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AvversitaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter codAvversita;

    private StringFilter nomeSci;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter avversToRaccordoId;

    public AvversitaCriteria(){
    }

    public AvversitaCriteria(AvversitaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.codAvversita = other.codAvversita == null ? null : other.codAvversita.copy();
        this.nomeSci = other.nomeSci == null ? null : other.nomeSci.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.avversToRaccordoId = other.avversToRaccordoId == null ? null : other.avversToRaccordoId.copy();
    }

    @Override
    public AvversitaCriteria copy() {
        return new AvversitaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCodAvversita() {
        return codAvversita;
    }

    public void setCodAvversita(StringFilter codAvversita) {
        this.codAvversita = codAvversita;
    }

    public StringFilter getNomeSci() {
        return nomeSci;
    }

    public void setNomeSci(StringFilter nomeSci) {
        this.nomeSci = nomeSci;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAvversToRaccordoId() {
        return avversToRaccordoId;
    }

    public void setAvversToRaccordoId(LongFilter avversToRaccordoId) {
        this.avversToRaccordoId = avversToRaccordoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AvversitaCriteria that = (AvversitaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(codAvversita, that.codAvversita) &&
            Objects.equals(nomeSci, that.nomeSci) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(avversToRaccordoId, that.avversToRaccordoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        codAvversita,
        nomeSci,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        avversToRaccordoId
        );
    }

    @Override
    public String toString() {
        return "AvversitaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (codAvversita != null ? "codAvversita=" + codAvversita + ", " : "") +
                (nomeSci != null ? "nomeSci=" + nomeSci + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (avversToRaccordoId != null ? "avversToRaccordoId=" + avversToRaccordoId + ", " : "") +
            "}";
    }

}
