package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcAltro} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcAltroResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-altros?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcAltroCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataInizOpc;

    private LocalDateFilter dataFineOpc;

    private StringFilter tempoImpiegato;

    private LongFilter idColtura;

    private LongFilter idOperatore;

    private LongFilter idMezzo;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter opcAltroToAppzId;

    private LongFilter opcAltroToTipoId;

    public RegOpcAltroCriteria(){
    }

    public RegOpcAltroCriteria(RegOpcAltroCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataInizOpc = other.dataInizOpc == null ? null : other.dataInizOpc.copy();
        this.dataFineOpc = other.dataFineOpc == null ? null : other.dataFineOpc.copy();
        this.tempoImpiegato = other.tempoImpiegato == null ? null : other.tempoImpiegato.copy();
        this.idColtura = other.idColtura == null ? null : other.idColtura.copy();
        this.idOperatore = other.idOperatore == null ? null : other.idOperatore.copy();
        this.idMezzo = other.idMezzo == null ? null : other.idMezzo.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.opcAltroToAppzId = other.opcAltroToAppzId == null ? null : other.opcAltroToAppzId.copy();
        this.opcAltroToTipoId = other.opcAltroToTipoId == null ? null : other.opcAltroToTipoId.copy();
    }

    @Override
    public RegOpcAltroCriteria copy() {
        return new RegOpcAltroCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataInizOpc() {
        return dataInizOpc;
    }

    public void setDataInizOpc(LocalDateFilter dataInizOpc) {
        this.dataInizOpc = dataInizOpc;
    }

    public LocalDateFilter getDataFineOpc() {
        return dataFineOpc;
    }

    public void setDataFineOpc(LocalDateFilter dataFineOpc) {
        this.dataFineOpc = dataFineOpc;
    }

    public StringFilter getTempoImpiegato() {
        return tempoImpiegato;
    }

    public void setTempoImpiegato(StringFilter tempoImpiegato) {
        this.tempoImpiegato = tempoImpiegato;
    }

    public LongFilter getIdColtura() {
        return idColtura;
    }

    public void setIdColtura(LongFilter idColtura) {
        this.idColtura = idColtura;
    }

    public LongFilter getIdOperatore() {
        return idOperatore;
    }

    public void setIdOperatore(LongFilter idOperatore) {
        this.idOperatore = idOperatore;
    }

    public LongFilter getIdMezzo() {
        return idMezzo;
    }

    public void setIdMezzo(LongFilter idMezzo) {
        this.idMezzo = idMezzo;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getOpcAltroToAppzId() {
        return opcAltroToAppzId;
    }

    public void setOpcAltroToAppzId(LongFilter opcAltroToAppzId) {
        this.opcAltroToAppzId = opcAltroToAppzId;
    }

    public LongFilter getOpcAltroToTipoId() {
        return opcAltroToTipoId;
    }

    public void setOpcAltroToTipoId(LongFilter opcAltroToTipoId) {
        this.opcAltroToTipoId = opcAltroToTipoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcAltroCriteria that = (RegOpcAltroCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataInizOpc, that.dataInizOpc) &&
            Objects.equals(dataFineOpc, that.dataFineOpc) &&
            Objects.equals(tempoImpiegato, that.tempoImpiegato) &&
            Objects.equals(idColtura, that.idColtura) &&
            Objects.equals(idOperatore, that.idOperatore) &&
            Objects.equals(idMezzo, that.idMezzo) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(opcAltroToAppzId, that.opcAltroToAppzId) &&
            Objects.equals(opcAltroToTipoId, that.opcAltroToTipoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        dataInizOpc,
        dataFineOpc,
        tempoImpiegato,
        idColtura,
        idOperatore,
        idMezzo,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        opcAltroToAppzId,
        opcAltroToTipoId
        );
    }

    @Override
    public String toString() {
        return "RegOpcAltroCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataInizOpc != null ? "dataInizOpc=" + dataInizOpc + ", " : "") +
                (dataFineOpc != null ? "dataFineOpc=" + dataFineOpc + ", " : "") +
                (tempoImpiegato != null ? "tempoImpiegato=" + tempoImpiegato + ", " : "") +
                (idColtura != null ? "idColtura=" + idColtura + ", " : "") +
                (idOperatore != null ? "idOperatore=" + idOperatore + ", " : "") +
                (idMezzo != null ? "idMezzo=" + idMezzo + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (opcAltroToAppzId != null ? "opcAltroToAppzId=" + opcAltroToAppzId + ", " : "") +
                (opcAltroToTipoId != null ? "opcAltroToTipoId=" + opcAltroToTipoId + ", " : "") +
            "}";
    }

}
