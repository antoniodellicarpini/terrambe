package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.ColturaAppezzamento} entity. This class is used
 * in {@link com.terrambe.web.rest.ColturaAppezzamentoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /coltura-appezzamentos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ColturaAppezzamentoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter primaria;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private LocalDateFilter dataInizioColtura;

    private LocalDateFilter dataFineColtura;

    private LocalDateFilter dataFioritura;

    private LocalDateFilter dataFineFioritura;

    private LocalDateFilter dataRaccolta;

    private IntegerFilter numeroPiante;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter coltToAppeId;

    private LongFilter coltToAgeaId;

    public ColturaAppezzamentoCriteria(){
    }

    public ColturaAppezzamentoCriteria(ColturaAppezzamentoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.primaria = other.primaria == null ? null : other.primaria.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.dataInizioColtura = other.dataInizioColtura == null ? null : other.dataInizioColtura.copy();
        this.dataFineColtura = other.dataFineColtura == null ? null : other.dataFineColtura.copy();
        this.dataFioritura = other.dataFioritura == null ? null : other.dataFioritura.copy();
        this.dataFineFioritura = other.dataFineFioritura == null ? null : other.dataFineFioritura.copy();
        this.dataRaccolta = other.dataRaccolta == null ? null : other.dataRaccolta.copy();
        this.numeroPiante = other.numeroPiante == null ? null : other.numeroPiante.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.coltToAppeId = other.coltToAppeId == null ? null : other.coltToAppeId.copy();
        this.coltToAgeaId = other.coltToAgeaId == null ? null : other.coltToAgeaId.copy();
    }

    @Override
    public ColturaAppezzamentoCriteria copy() {
        return new ColturaAppezzamentoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getPrimaria() {
        return primaria;
    }

    public void setPrimaria(BooleanFilter primaria) {
        this.primaria = primaria;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public LocalDateFilter getDataInizioColtura() {
        return dataInizioColtura;
    }

    public void setDataInizioColtura(LocalDateFilter dataInizioColtura) {
        this.dataInizioColtura = dataInizioColtura;
    }

    public LocalDateFilter getDataFineColtura() {
        return dataFineColtura;
    }

    public void setDataFineColtura(LocalDateFilter dataFineColtura) {
        this.dataFineColtura = dataFineColtura;
    }

    public LocalDateFilter getDataFioritura() {
        return dataFioritura;
    }

    public void setDataFioritura(LocalDateFilter dataFioritura) {
        this.dataFioritura = dataFioritura;
    }

    public LocalDateFilter getDataFineFioritura() {
        return dataFineFioritura;
    }

    public void setDataFineFioritura(LocalDateFilter dataFineFioritura) {
        this.dataFineFioritura = dataFineFioritura;
    }

    public LocalDateFilter getDataRaccolta() {
        return dataRaccolta;
    }

    public void setDataRaccolta(LocalDateFilter dataRaccolta) {
        this.dataRaccolta = dataRaccolta;
    }

    public IntegerFilter getNumeroPiante() {
        return numeroPiante;
    }

    public void setNumeroPiante(IntegerFilter numeroPiante) {
        this.numeroPiante = numeroPiante;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getColtToAppeId() {
        return coltToAppeId;
    }

    public void setColtToAppeId(LongFilter coltToAppeId) {
        this.coltToAppeId = coltToAppeId;
    }

    public LongFilter getColtToAgeaId() {
        return coltToAgeaId;
    }

    public void setColtToAgeaId(LongFilter coltToAgeaId) {
        this.coltToAgeaId = coltToAgeaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ColturaAppezzamentoCriteria that = (ColturaAppezzamentoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(primaria, that.primaria) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(dataInizioColtura, that.dataInizioColtura) &&
            Objects.equals(dataFineColtura, that.dataFineColtura) &&
            Objects.equals(dataFioritura, that.dataFioritura) &&
            Objects.equals(dataFineFioritura, that.dataFineFioritura) &&
            Objects.equals(dataRaccolta, that.dataRaccolta) &&
            Objects.equals(numeroPiante, that.numeroPiante) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(coltToAppeId, that.coltToAppeId) &&
            Objects.equals(coltToAgeaId, that.coltToAgeaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        primaria,
        idAzienda,
        idUnitaProd,
        dataInizioColtura,
        dataFineColtura,
        dataFioritura,
        dataFineFioritura,
        dataRaccolta,
        numeroPiante,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        coltToAppeId,
        coltToAgeaId
        );
    }

    @Override
    public String toString() {
        return "ColturaAppezzamentoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (primaria != null ? "primaria=" + primaria + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (dataInizioColtura != null ? "dataInizioColtura=" + dataInizioColtura + ", " : "") +
                (dataFineColtura != null ? "dataFineColtura=" + dataFineColtura + ", " : "") +
                (dataFioritura != null ? "dataFioritura=" + dataFioritura + ", " : "") +
                (dataFineFioritura != null ? "dataFineFioritura=" + dataFineFioritura + ", " : "") +
                (dataRaccolta != null ? "dataRaccolta=" + dataRaccolta + ", " : "") +
                (numeroPiante != null ? "numeroPiante=" + numeroPiante + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (coltToAppeId != null ? "coltToAppeId=" + coltToAppeId + ", " : "") +
                (coltToAgeaId != null ? "coltToAgeaId=" + coltToAgeaId + ", " : "") +
            "}";
    }

}
