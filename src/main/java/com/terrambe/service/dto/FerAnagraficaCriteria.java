package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.FerAnagrafica} entity. This class is used
 * in {@link com.terrambe.web.rest.FerAnagraficaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fer-anagraficas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FerAnagraficaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter denominazione;

    private StringFilter dittaProduttrice;

    private DoubleFilter datoN;

    private DoubleFilter datoP;

    private DoubleFilter datoK;

    private StringFilter tipoConcime;

    private BooleanFilter bio;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter fertTipoId;

    public FerAnagraficaCriteria(){
    }

    public FerAnagraficaCriteria(FerAnagraficaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.denominazione = other.denominazione == null ? null : other.denominazione.copy();
        this.dittaProduttrice = other.dittaProduttrice == null ? null : other.dittaProduttrice.copy();
        this.datoN = other.datoN == null ? null : other.datoN.copy();
        this.datoP = other.datoP == null ? null : other.datoP.copy();
        this.datoK = other.datoK == null ? null : other.datoK.copy();
        this.tipoConcime = other.tipoConcime == null ? null : other.tipoConcime.copy();
        this.bio = other.bio == null ? null : other.bio.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.fertTipoId = other.fertTipoId == null ? null : other.fertTipoId.copy();
    }

    @Override
    public FerAnagraficaCriteria copy() {
        return new FerAnagraficaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDenominazione() {
        return denominazione;
    }

    public void setDenominazione(StringFilter denominazione) {
        this.denominazione = denominazione;
    }

    public StringFilter getDittaProduttrice() {
        return dittaProduttrice;
    }

    public void setDittaProduttrice(StringFilter dittaProduttrice) {
        this.dittaProduttrice = dittaProduttrice;
    }

    public DoubleFilter getDatoN() {
        return datoN;
    }

    public void setDatoN(DoubleFilter datoN) {
        this.datoN = datoN;
    }

    public DoubleFilter getDatoP() {
        return datoP;
    }

    public void setDatoP(DoubleFilter datoP) {
        this.datoP = datoP;
    }

    public DoubleFilter getDatoK() {
        return datoK;
    }

    public void setDatoK(DoubleFilter datoK) {
        this.datoK = datoK;
    }

    public StringFilter getTipoConcime() {
        return tipoConcime;
    }

    public void setTipoConcime(StringFilter tipoConcime) {
        this.tipoConcime = tipoConcime;
    }

    public BooleanFilter getBio() {
        return bio;
    }

    public void setBio(BooleanFilter bio) {
        this.bio = bio;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getFertTipoId() {
        return fertTipoId;
    }

    public void setFertTipoId(LongFilter fertTipoId) {
        this.fertTipoId = fertTipoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FerAnagraficaCriteria that = (FerAnagraficaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(denominazione, that.denominazione) &&
            Objects.equals(dittaProduttrice, that.dittaProduttrice) &&
            Objects.equals(datoN, that.datoN) &&
            Objects.equals(datoP, that.datoP) &&
            Objects.equals(datoK, that.datoK) &&
            Objects.equals(tipoConcime, that.tipoConcime) &&
            Objects.equals(bio, that.bio) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(fertTipoId, that.fertTipoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        denominazione,
        dittaProduttrice,
        datoN,
        datoP,
        datoK,
        tipoConcime,
        bio,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        fertTipoId
        );
    }

    @Override
    public String toString() {
        return "FerAnagraficaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (denominazione != null ? "denominazione=" + denominazione + ", " : "") +
                (dittaProduttrice != null ? "dittaProduttrice=" + dittaProduttrice + ", " : "") +
                (datoN != null ? "datoN=" + datoN + ", " : "") +
                (datoP != null ? "datoP=" + datoP + ", " : "") +
                (datoK != null ? "datoK=" + datoK + ", " : "") +
                (tipoConcime != null ? "tipoConcime=" + tipoConcime + ", " : "") +
                (bio != null ? "bio=" + bio + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (fertTipoId != null ? "fertTipoId=" + fertTipoId + ", " : "") +
            "}";
    }

}
