package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.AtrAnagrafica} entity. This class is used
 * in {@link com.terrambe.web.rest.AtrAnagraficaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /atr-anagraficas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AtrAnagraficaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private StringFilter modello;

    private LocalDateFilter dataRevisione;

    private IntegerFilter cavalli;

    private IntegerFilter capacita;

    private StringFilter targa;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter atrAnaToUniAnaId;

    private LongFilter atrAnaToAtrTipoId;

    private LongFilter atrAnagToAtrMarcId;

    public AtrAnagraficaCriteria(){
    }

    public AtrAnagraficaCriteria(AtrAnagraficaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.modello = other.modello == null ? null : other.modello.copy();
        this.dataRevisione = other.dataRevisione == null ? null : other.dataRevisione.copy();
        this.cavalli = other.cavalli == null ? null : other.cavalli.copy();
        this.capacita = other.capacita == null ? null : other.capacita.copy();
        this.targa = other.targa == null ? null : other.targa.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.atrAnaToUniAnaId = other.atrAnaToUniAnaId == null ? null : other.atrAnaToUniAnaId.copy();
        this.atrAnaToAtrTipoId = other.atrAnaToAtrTipoId == null ? null : other.atrAnaToAtrTipoId.copy();
        this.atrAnagToAtrMarcId = other.atrAnagToAtrMarcId == null ? null : other.atrAnagToAtrMarcId.copy();
    }

    @Override
    public AtrAnagraficaCriteria copy() {
        return new AtrAnagraficaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public StringFilter getModello() {
        return modello;
    }

    public void setModello(StringFilter modello) {
        this.modello = modello;
    }

    public LocalDateFilter getDataRevisione() {
        return dataRevisione;
    }

    public void setDataRevisione(LocalDateFilter dataRevisione) {
        this.dataRevisione = dataRevisione;
    }

    public IntegerFilter getCavalli() {
        return cavalli;
    }

    public void setCavalli(IntegerFilter cavalli) {
        this.cavalli = cavalli;
    }

    public IntegerFilter getCapacita() {
        return capacita;
    }

    public void setCapacita(IntegerFilter capacita) {
        this.capacita = capacita;
    }

    public StringFilter getTarga() {
        return targa;
    }

    public void setTarga(StringFilter targa) {
        this.targa = targa;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAtrAnaToUniAnaId() {
        return atrAnaToUniAnaId;
    }

    public void setAtrAnaToUniAnaId(LongFilter atrAnaToUniAnaId) {
        this.atrAnaToUniAnaId = atrAnaToUniAnaId;
    }

    public LongFilter getAtrAnaToAtrTipoId() {
        return atrAnaToAtrTipoId;
    }

    public void setAtrAnaToAtrTipoId(LongFilter atrAnaToAtrTipoId) {
        this.atrAnaToAtrTipoId = atrAnaToAtrTipoId;
    }

    public LongFilter getAtrAnagToAtrMarcId() {
        return atrAnagToAtrMarcId;
    }

    public void setAtrAnagToAtrMarcId(LongFilter atrAnagToAtrMarcId) {
        this.atrAnagToAtrMarcId = atrAnagToAtrMarcId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AtrAnagraficaCriteria that = (AtrAnagraficaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(modello, that.modello) &&
            Objects.equals(dataRevisione, that.dataRevisione) &&
            Objects.equals(cavalli, that.cavalli) &&
            Objects.equals(capacita, that.capacita) &&
            Objects.equals(targa, that.targa) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(atrAnaToUniAnaId, that.atrAnaToUniAnaId) &&
            Objects.equals(atrAnaToAtrTipoId, that.atrAnaToAtrTipoId) &&
            Objects.equals(atrAnagToAtrMarcId, that.atrAnagToAtrMarcId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        modello,
        dataRevisione,
        cavalli,
        capacita,
        targa,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        atrAnaToUniAnaId,
        atrAnaToAtrTipoId,
        atrAnagToAtrMarcId
        );
    }

    @Override
    public String toString() {
        return "AtrAnagraficaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (modello != null ? "modello=" + modello + ", " : "") +
                (dataRevisione != null ? "dataRevisione=" + dataRevisione + ", " : "") +
                (cavalli != null ? "cavalli=" + cavalli + ", " : "") +
                (capacita != null ? "capacita=" + capacita + ", " : "") +
                (targa != null ? "targa=" + targa + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (atrAnaToUniAnaId != null ? "atrAnaToUniAnaId=" + atrAnaToUniAnaId + ", " : "") +
                (atrAnaToAtrTipoId != null ? "atrAnaToAtrTipoId=" + atrAnaToAtrTipoId + ", " : "") +
                (atrAnagToAtrMarcId != null ? "atrAnagToAtrMarcId=" + atrAnagToAtrMarcId + ", " : "") +
            "}";
    }

}
