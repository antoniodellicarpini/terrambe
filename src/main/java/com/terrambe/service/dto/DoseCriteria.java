package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Dose} entity. This class is used
 * in {@link com.terrambe.web.rest.DoseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /doses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DoseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter doseMin;

    private DoubleFilter doseMax;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter doseToDosagId;

    private LongFilter doseToUniMisuId;

    public DoseCriteria(){
    }

    public DoseCriteria(DoseCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.doseMin = other.doseMin == null ? null : other.doseMin.copy();
        this.doseMax = other.doseMax == null ? null : other.doseMax.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.doseToDosagId = other.doseToDosagId == null ? null : other.doseToDosagId.copy();
        this.doseToUniMisuId = other.doseToUniMisuId == null ? null : other.doseToUniMisuId.copy();
    }

    @Override
    public DoseCriteria copy() {
        return new DoseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getDoseMin() {
        return doseMin;
    }

    public void setDoseMin(DoubleFilter doseMin) {
        this.doseMin = doseMin;
    }

    public DoubleFilter getDoseMax() {
        return doseMax;
    }

    public void setDoseMax(DoubleFilter doseMax) {
        this.doseMax = doseMax;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getDoseToDosagId() {
        return doseToDosagId;
    }

    public void setDoseToDosagId(LongFilter doseToDosagId) {
        this.doseToDosagId = doseToDosagId;
    }

    public LongFilter getDoseToUniMisuId() {
        return doseToUniMisuId;
    }

    public void setDoseToUniMisuId(LongFilter doseToUniMisuId) {
        this.doseToUniMisuId = doseToUniMisuId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DoseCriteria that = (DoseCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(doseMin, that.doseMin) &&
            Objects.equals(doseMax, that.doseMax) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(doseToDosagId, that.doseToDosagId) &&
            Objects.equals(doseToUniMisuId, that.doseToUniMisuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        doseMin,
        doseMax,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        doseToDosagId,
        doseToUniMisuId
        );
    }

    @Override
    public String toString() {
        return "DoseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (doseMin != null ? "doseMin=" + doseMin + ", " : "") +
                (doseMax != null ? "doseMax=" + doseMax + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (doseToDosagId != null ? "doseToDosagId=" + doseToDosagId + ", " : "") +
                (doseToUniMisuId != null ? "doseToUniMisuId=" + doseToUniMisuId + ", " : "") +
            "}";
    }

}
