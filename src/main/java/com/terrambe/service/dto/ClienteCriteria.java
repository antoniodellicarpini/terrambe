package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Cliente} entity. This class is used
 * in {@link com.terrambe.web.rest.ClienteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /clientes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClienteCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private StringFilter cuaa;

    private StringFilter partitaiva;

    private StringFilter codicefiscale;

    private StringFilter codiceAteco;

    private StringFilter ragioneSociale;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private BooleanFilter attivo;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter cliToIndId;

    private LongFilter cliToRecId;

    private LongFilter naturaGiuridicaId;

    private LongFilter cliToAziAnaId;

    private LongFilter cliAnaToCliRapId;

    public ClienteCriteria(){
    }

    public ClienteCriteria(ClienteCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.cuaa = other.cuaa == null ? null : other.cuaa.copy();
        this.partitaiva = other.partitaiva == null ? null : other.partitaiva.copy();
        this.codicefiscale = other.codicefiscale == null ? null : other.codicefiscale.copy();
        this.codiceAteco = other.codiceAteco == null ? null : other.codiceAteco.copy();
        this.ragioneSociale = other.ragioneSociale == null ? null : other.ragioneSociale.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.attivo = other.attivo == null ? null : other.attivo.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.cliToIndId = other.cliToIndId == null ? null : other.cliToIndId.copy();
        this.cliToRecId = other.cliToRecId == null ? null : other.cliToRecId.copy();
        this.naturaGiuridicaId = other.naturaGiuridicaId == null ? null : other.naturaGiuridicaId.copy();
        this.cliToAziAnaId = other.cliToAziAnaId == null ? null : other.cliToAziAnaId.copy();
        this.cliAnaToCliRapId = other.cliAnaToCliRapId == null ? null : other.cliAnaToCliRapId.copy();
    }

    @Override
    public ClienteCriteria copy() {
        return new ClienteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public StringFilter getCuaa() {
        return cuaa;
    }

    public void setCuaa(StringFilter cuaa) {
        this.cuaa = cuaa;
    }

    public StringFilter getPartitaiva() {
        return partitaiva;
    }

    public void setPartitaiva(StringFilter partitaiva) {
        this.partitaiva = partitaiva;
    }

    public StringFilter getCodicefiscale() {
        return codicefiscale;
    }

    public void setCodicefiscale(StringFilter codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public StringFilter getCodiceAteco() {
        return codiceAteco;
    }

    public void setCodiceAteco(StringFilter codiceAteco) {
        this.codiceAteco = codiceAteco;
    }

    public StringFilter getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(StringFilter ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public BooleanFilter getAttivo() {
        return attivo;
    }

    public void setAttivo(BooleanFilter attivo) {
        this.attivo = attivo;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getCliToIndId() {
        return cliToIndId;
    }

    public void setCliToIndId(LongFilter cliToIndId) {
        this.cliToIndId = cliToIndId;
    }

    public LongFilter getCliToRecId() {
        return cliToRecId;
    }

    public void setCliToRecId(LongFilter cliToRecId) {
        this.cliToRecId = cliToRecId;
    }

    public LongFilter getNaturaGiuridicaId() {
        return naturaGiuridicaId;
    }

    public void setNaturaGiuridicaId(LongFilter naturaGiuridicaId) {
        this.naturaGiuridicaId = naturaGiuridicaId;
    }

    public LongFilter getCliToAziAnaId() {
        return cliToAziAnaId;
    }

    public void setCliToAziAnaId(LongFilter cliToAziAnaId) {
        this.cliToAziAnaId = cliToAziAnaId;
    }

    public LongFilter getCliAnaToCliRapId() {
        return cliAnaToCliRapId;
    }

    public void setCliAnaToCliRapId(LongFilter cliAnaToCliRapId) {
        this.cliAnaToCliRapId = cliAnaToCliRapId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClienteCriteria that = (ClienteCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(cuaa, that.cuaa) &&
            Objects.equals(partitaiva, that.partitaiva) &&
            Objects.equals(codicefiscale, that.codicefiscale) &&
            Objects.equals(codiceAteco, that.codiceAteco) &&
            Objects.equals(ragioneSociale, that.ragioneSociale) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(attivo, that.attivo) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(cliToIndId, that.cliToIndId) &&
            Objects.equals(cliToRecId, that.cliToRecId) &&
            Objects.equals(naturaGiuridicaId, that.naturaGiuridicaId) &&
            Objects.equals(cliToAziAnaId, that.cliToAziAnaId) &&
            Objects.equals(cliAnaToCliRapId, that.cliAnaToCliRapId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        cuaa,
        partitaiva,
        codicefiscale,
        codiceAteco,
        ragioneSociale,
        dataInizVali,
        dataFineVali,
        attivo,
        userIdCreator,
        userIdLastMod,
        cliToIndId,
        cliToRecId,
        naturaGiuridicaId,
        cliToAziAnaId,
        cliAnaToCliRapId
        );
    }

    @Override
    public String toString() {
        return "ClienteCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (cuaa != null ? "cuaa=" + cuaa + ", " : "") +
                (partitaiva != null ? "partitaiva=" + partitaiva + ", " : "") +
                (codicefiscale != null ? "codicefiscale=" + codicefiscale + ", " : "") +
                (codiceAteco != null ? "codiceAteco=" + codiceAteco + ", " : "") +
                (ragioneSociale != null ? "ragioneSociale=" + ragioneSociale + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (attivo != null ? "attivo=" + attivo + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (cliToIndId != null ? "cliToIndId=" + cliToIndId + ", " : "") +
                (cliToRecId != null ? "cliToRecId=" + cliToRecId + ", " : "") +
                (naturaGiuridicaId != null ? "naturaGiuridicaId=" + naturaGiuridicaId + ", " : "") +
                (cliToAziAnaId != null ? "cliToAziAnaId=" + cliToAziAnaId + ", " : "") +
                (cliAnaToCliRapId != null ? "cliAnaToCliRapId=" + cliAnaToCliRapId + ", " : "") +
            "}";
    }

}
