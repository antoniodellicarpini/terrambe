package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.FaseFenologica} entity. This class is used
 * in {@link com.terrambe.web.rest.FaseFenologicaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fase-fenologicas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FaseFenologicaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter codStadioColt;

    private StringFilter desStadioColt;

    private StringFilter tipoImport;

    private StringFilter operatore;

    private InstantFilter ts;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter faseFenoToRaccordoId;

    public FaseFenologicaCriteria(){
    }

    public FaseFenologicaCriteria(FaseFenologicaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.codStadioColt = other.codStadioColt == null ? null : other.codStadioColt.copy();
        this.desStadioColt = other.desStadioColt == null ? null : other.desStadioColt.copy();
        this.tipoImport = other.tipoImport == null ? null : other.tipoImport.copy();
        this.operatore = other.operatore == null ? null : other.operatore.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.faseFenoToRaccordoId = other.faseFenoToRaccordoId == null ? null : other.faseFenoToRaccordoId.copy();
    }

    @Override
    public FaseFenologicaCriteria copy() {
        return new FaseFenologicaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCodStadioColt() {
        return codStadioColt;
    }

    public void setCodStadioColt(StringFilter codStadioColt) {
        this.codStadioColt = codStadioColt;
    }

    public StringFilter getDesStadioColt() {
        return desStadioColt;
    }

    public void setDesStadioColt(StringFilter desStadioColt) {
        this.desStadioColt = desStadioColt;
    }

    public StringFilter getTipoImport() {
        return tipoImport;
    }

    public void setTipoImport(StringFilter tipoImport) {
        this.tipoImport = tipoImport;
    }

    public StringFilter getOperatore() {
        return operatore;
    }

    public void setOperatore(StringFilter operatore) {
        this.operatore = operatore;
    }

    public InstantFilter getTs() {
        return ts;
    }

    public void setTs(InstantFilter ts) {
        this.ts = ts;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getFaseFenoToRaccordoId() {
        return faseFenoToRaccordoId;
    }

    public void setFaseFenoToRaccordoId(LongFilter faseFenoToRaccordoId) {
        this.faseFenoToRaccordoId = faseFenoToRaccordoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FaseFenologicaCriteria that = (FaseFenologicaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(codStadioColt, that.codStadioColt) &&
            Objects.equals(desStadioColt, that.desStadioColt) &&
            Objects.equals(tipoImport, that.tipoImport) &&
            Objects.equals(operatore, that.operatore) &&
            Objects.equals(ts, that.ts) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(faseFenoToRaccordoId, that.faseFenoToRaccordoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        codStadioColt,
        desStadioColt,
        tipoImport,
        operatore,
        ts,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        faseFenoToRaccordoId
        );
    }

    @Override
    public String toString() {
        return "FaseFenologicaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (codStadioColt != null ? "codStadioColt=" + codStadioColt + ", " : "") +
                (desStadioColt != null ? "desStadioColt=" + desStadioColt + ", " : "") +
                (tipoImport != null ? "tipoImport=" + tipoImport + ", " : "") +
                (operatore != null ? "operatore=" + operatore + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (faseFenoToRaccordoId != null ? "faseFenoToRaccordoId=" + faseFenoToRaccordoId + ", " : "") +
            "}";
    }

}
