package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.RegOpcAltroAppz} entity. This class is used
 * in {@link com.terrambe.web.rest.RegOpcAltroAppzResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reg-opc-altro-appzs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RegOpcAltroAppzCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter idAzienda;

    private LongFilter idUnitaProd;

    private FloatFilter percentLavorata;

    private LongFilter idAppezzamento;

    private LocalDateFilter dataInizVali;

    private LocalDateFilter dataFineVali;

    private LongFilter userIdCreator;

    private LongFilter userIdLastMod;

    private LongFilter appzToOpcAltroId;

    public RegOpcAltroAppzCriteria(){
    }

    public RegOpcAltroAppzCriteria(RegOpcAltroAppzCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.idAzienda = other.idAzienda == null ? null : other.idAzienda.copy();
        this.idUnitaProd = other.idUnitaProd == null ? null : other.idUnitaProd.copy();
        this.percentLavorata = other.percentLavorata == null ? null : other.percentLavorata.copy();
        this.idAppezzamento = other.idAppezzamento == null ? null : other.idAppezzamento.copy();
        this.dataInizVali = other.dataInizVali == null ? null : other.dataInizVali.copy();
        this.dataFineVali = other.dataFineVali == null ? null : other.dataFineVali.copy();
        this.userIdCreator = other.userIdCreator == null ? null : other.userIdCreator.copy();
        this.userIdLastMod = other.userIdLastMod == null ? null : other.userIdLastMod.copy();
        this.appzToOpcAltroId = other.appzToOpcAltroId == null ? null : other.appzToOpcAltroId.copy();
    }

    @Override
    public RegOpcAltroAppzCriteria copy() {
        return new RegOpcAltroAppzCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getIdAzienda() {
        return idAzienda;
    }

    public void setIdAzienda(LongFilter idAzienda) {
        this.idAzienda = idAzienda;
    }

    public LongFilter getIdUnitaProd() {
        return idUnitaProd;
    }

    public void setIdUnitaProd(LongFilter idUnitaProd) {
        this.idUnitaProd = idUnitaProd;
    }

    public FloatFilter getPercentLavorata() {
        return percentLavorata;
    }

    public void setPercentLavorata(FloatFilter percentLavorata) {
        this.percentLavorata = percentLavorata;
    }

    public LongFilter getIdAppezzamento() {
        return idAppezzamento;
    }

    public void setIdAppezzamento(LongFilter idAppezzamento) {
        this.idAppezzamento = idAppezzamento;
    }

    public LocalDateFilter getDataInizVali() {
        return dataInizVali;
    }

    public void setDataInizVali(LocalDateFilter dataInizVali) {
        this.dataInizVali = dataInizVali;
    }

    public LocalDateFilter getDataFineVali() {
        return dataFineVali;
    }

    public void setDataFineVali(LocalDateFilter dataFineVali) {
        this.dataFineVali = dataFineVali;
    }

    public LongFilter getUserIdCreator() {
        return userIdCreator;
    }

    public void setUserIdCreator(LongFilter userIdCreator) {
        this.userIdCreator = userIdCreator;
    }

    public LongFilter getUserIdLastMod() {
        return userIdLastMod;
    }

    public void setUserIdLastMod(LongFilter userIdLastMod) {
        this.userIdLastMod = userIdLastMod;
    }

    public LongFilter getAppzToOpcAltroId() {
        return appzToOpcAltroId;
    }

    public void setAppzToOpcAltroId(LongFilter appzToOpcAltroId) {
        this.appzToOpcAltroId = appzToOpcAltroId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RegOpcAltroAppzCriteria that = (RegOpcAltroAppzCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(idAzienda, that.idAzienda) &&
            Objects.equals(idUnitaProd, that.idUnitaProd) &&
            Objects.equals(percentLavorata, that.percentLavorata) &&
            Objects.equals(idAppezzamento, that.idAppezzamento) &&
            Objects.equals(dataInizVali, that.dataInizVali) &&
            Objects.equals(dataFineVali, that.dataFineVali) &&
            Objects.equals(userIdCreator, that.userIdCreator) &&
            Objects.equals(userIdLastMod, that.userIdLastMod) &&
            Objects.equals(appzToOpcAltroId, that.appzToOpcAltroId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        idAzienda,
        idUnitaProd,
        percentLavorata,
        idAppezzamento,
        dataInizVali,
        dataFineVali,
        userIdCreator,
        userIdLastMod,
        appzToOpcAltroId
        );
    }

    @Override
    public String toString() {
        return "RegOpcAltroAppzCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (idAzienda != null ? "idAzienda=" + idAzienda + ", " : "") +
                (idUnitaProd != null ? "idUnitaProd=" + idUnitaProd + ", " : "") +
                (percentLavorata != null ? "percentLavorata=" + percentLavorata + ", " : "") +
                (idAppezzamento != null ? "idAppezzamento=" + idAppezzamento + ", " : "") +
                (dataInizVali != null ? "dataInizVali=" + dataInizVali + ", " : "") +
                (dataFineVali != null ? "dataFineVali=" + dataFineVali + ", " : "") +
                (userIdCreator != null ? "userIdCreator=" + userIdCreator + ", " : "") +
                (userIdLastMod != null ? "userIdLastMod=" + userIdLastMod + ", " : "") +
                (appzToOpcAltroId != null ? "appzToOpcAltroId=" + appzToOpcAltroId + ", " : "") +
            "}";
    }

}
