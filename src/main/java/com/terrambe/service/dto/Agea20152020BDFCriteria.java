package com.terrambe.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.terrambe.domain.Agea20152020BDF} entity. This class is used
 * in {@link com.terrambe.web.rest.Agea20152020BDFResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /agea-20152020-bdfs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class Agea20152020BDFCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter ageaBdfToColtBdfId;

    private LongFilter bdfToMAtriceId;

    public Agea20152020BDFCriteria(){
    }

    public Agea20152020BDFCriteria(Agea20152020BDFCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.ageaBdfToColtBdfId = other.ageaBdfToColtBdfId == null ? null : other.ageaBdfToColtBdfId.copy();
        this.bdfToMAtriceId = other.bdfToMAtriceId == null ? null : other.bdfToMAtriceId.copy();
    }

    @Override
    public Agea20152020BDFCriteria copy() {
        return new Agea20152020BDFCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAgeaBdfToColtBdfId() {
        return ageaBdfToColtBdfId;
    }

    public void setAgeaBdfToColtBdfId(LongFilter ageaBdfToColtBdfId) {
        this.ageaBdfToColtBdfId = ageaBdfToColtBdfId;
    }

    public LongFilter getBdfToMAtriceId() {
        return bdfToMAtriceId;
    }

    public void setBdfToMAtriceId(LongFilter bdfToMAtriceId) {
        this.bdfToMAtriceId = bdfToMAtriceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Agea20152020BDFCriteria that = (Agea20152020BDFCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ageaBdfToColtBdfId, that.ageaBdfToColtBdfId) &&
            Objects.equals(bdfToMAtriceId, that.bdfToMAtriceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ageaBdfToColtBdfId,
        bdfToMAtriceId
        );
    }

    @Override
    public String toString() {
        return "Agea20152020BDFCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ageaBdfToColtBdfId != null ? "ageaBdfToColtBdfId=" + ageaBdfToColtBdfId + ", " : "") +
                (bdfToMAtriceId != null ? "bdfToMAtriceId=" + bdfToMAtriceId + ", " : "") +
            "}";
    }

}
