package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.BioAppzTipo;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.BioAppzTipoRepository;
import com.terrambe.service.dto.BioAppzTipoCriteria;

/**
 * Service for executing complex queries for {@link BioAppzTipo} entities in the database.
 * The main input is a {@link BioAppzTipoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BioAppzTipo} or a {@link Page} of {@link BioAppzTipo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BioAppzTipoQueryService extends QueryService<BioAppzTipo> {

    private final Logger log = LoggerFactory.getLogger(BioAppzTipoQueryService.class);

    private final BioAppzTipoRepository bioAppzTipoRepository;

    public BioAppzTipoQueryService(BioAppzTipoRepository bioAppzTipoRepository) {
        this.bioAppzTipoRepository = bioAppzTipoRepository;
    }

    /**
     * Return a {@link List} of {@link BioAppzTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BioAppzTipo> findByCriteria(BioAppzTipoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BioAppzTipo> specification = createSpecification(criteria);
        return bioAppzTipoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link BioAppzTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BioAppzTipo> findByCriteria(BioAppzTipoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BioAppzTipo> specification = createSpecification(criteria);
        return bioAppzTipoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BioAppzTipoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BioAppzTipo> specification = createSpecification(criteria);
        return bioAppzTipoRepository.count(specification);
    }

    /**
     * Function to convert {@link BioAppzTipoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BioAppzTipo> createSpecification(BioAppzTipoCriteria criteria) {
        Specification<BioAppzTipo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BioAppzTipo_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), BioAppzTipo_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), BioAppzTipo_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), BioAppzTipo_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), BioAppzTipo_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), BioAppzTipo_.userIdLastMod));
            }
            if (criteria.getBioAppzTipoToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getBioAppzTipoToAppzId(),
                    root -> root.join(BioAppzTipo_.bioAppzTipoToAppzs, JoinType.LEFT).get(Appezzamenti_.id)));
            }
        }
        return specification;
    }
}
