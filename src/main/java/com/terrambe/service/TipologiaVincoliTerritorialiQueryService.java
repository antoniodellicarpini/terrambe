package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipologiaVincoliTerritoriali;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipologiaVincoliTerritorialiRepository;
import com.terrambe.service.dto.TipologiaVincoliTerritorialiCriteria;

/**
 * Service for executing complex queries for {@link TipologiaVincoliTerritoriali} entities in the database.
 * The main input is a {@link TipologiaVincoliTerritorialiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipologiaVincoliTerritoriali} or a {@link Page} of {@link TipologiaVincoliTerritoriali} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipologiaVincoliTerritorialiQueryService extends QueryService<TipologiaVincoliTerritoriali> {

    private final Logger log = LoggerFactory.getLogger(TipologiaVincoliTerritorialiQueryService.class);

    private final TipologiaVincoliTerritorialiRepository tipologiaVincoliTerritorialiRepository;

    public TipologiaVincoliTerritorialiQueryService(TipologiaVincoliTerritorialiRepository tipologiaVincoliTerritorialiRepository) {
        this.tipologiaVincoliTerritorialiRepository = tipologiaVincoliTerritorialiRepository;
    }

    /**
     * Return a {@link List} of {@link TipologiaVincoliTerritoriali} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipologiaVincoliTerritoriali> findByCriteria(TipologiaVincoliTerritorialiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipologiaVincoliTerritoriali> specification = createSpecification(criteria);
        return tipologiaVincoliTerritorialiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipologiaVincoliTerritoriali} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipologiaVincoliTerritoriali> findByCriteria(TipologiaVincoliTerritorialiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipologiaVincoliTerritoriali> specification = createSpecification(criteria);
        return tipologiaVincoliTerritorialiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipologiaVincoliTerritorialiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipologiaVincoliTerritoriali> specification = createSpecification(criteria);
        return tipologiaVincoliTerritorialiRepository.count(specification);
    }

    /**
     * Function to convert {@link TipologiaVincoliTerritorialiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipologiaVincoliTerritoriali> createSpecification(TipologiaVincoliTerritorialiCriteria criteria) {
        Specification<TipologiaVincoliTerritoriali> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipologiaVincoliTerritoriali_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipologiaVincoliTerritoriali_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipologiaVincoliTerritoriali_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipologiaVincoliTerritoriali_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipologiaVincoliTerritoriali_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipologiaVincoliTerritoriali_.userIdLastMod));
            }
            if (criteria.getVincoliTerToAppezId() != null) {
                specification = specification.and(buildSpecification(criteria.getVincoliTerToAppezId(),
                    root -> root.join(TipologiaVincoliTerritoriali_.vincoliTerToAppezs, JoinType.LEFT).get(Appezzamenti_.id)));
            }
        }
        return specification;
    }
}
