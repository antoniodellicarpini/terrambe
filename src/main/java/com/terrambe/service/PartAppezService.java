package com.terrambe.service;

import com.terrambe.domain.PartAppez;
import com.terrambe.repository.PartAppezRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PartAppez}.
 */
@Service
@Transactional
public class PartAppezService {

    private final Logger log = LoggerFactory.getLogger(PartAppezService.class);

    private final PartAppezRepository partAppezRepository;

    public PartAppezService(PartAppezRepository partAppezRepository) {
        this.partAppezRepository = partAppezRepository;
    }

    /**
     * Save a partAppez.
     *
     * @param partAppez the entity to save.
     * @return the persisted entity.
     */
    public PartAppez save(PartAppez partAppez) {
        log.debug("Request to save PartAppez : {}", partAppez);
        return partAppezRepository.save(partAppez);
    }

    /**
     * Get all the partAppezs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PartAppez> findAll(Pageable pageable) {
        log.debug("Request to get all PartAppezs");
        return partAppezRepository.findAll(pageable);
    }


    /**
     * Get one partAppez by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PartAppez> findOne(Long id) {
        log.debug("Request to get PartAppez : {}", id);
        return partAppezRepository.findById(id);
    }

    /**
     * Delete the partAppez by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PartAppez : {}", id);
        partAppezRepository.deleteById(id);
    }
}
