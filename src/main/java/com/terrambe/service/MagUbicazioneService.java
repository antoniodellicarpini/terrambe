package com.terrambe.service;

import com.terrambe.domain.MagUbicazione;
import com.terrambe.repository.MagUbicazioneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MagUbicazione}.
 */
@Service
@Transactional
public class MagUbicazioneService {

    private final Logger log = LoggerFactory.getLogger(MagUbicazioneService.class);

    private final MagUbicazioneRepository magUbicazioneRepository;

    public MagUbicazioneService(MagUbicazioneRepository magUbicazioneRepository) {
        this.magUbicazioneRepository = magUbicazioneRepository;
    }

    /**
     * Save a magUbicazione.
     *
     * @param magUbicazione the entity to save.
     * @return the persisted entity.
     */
    public MagUbicazione save(MagUbicazione magUbicazione) {
        log.debug("Request to save MagUbicazione : {}", magUbicazione);
        return magUbicazioneRepository.save(magUbicazione);
    }

    /**
     * Get all the magUbicaziones.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MagUbicazione> findAll() {
        log.debug("Request to get all MagUbicaziones");
        return magUbicazioneRepository.findAll();
    }


    /**
     * Get one magUbicazione by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MagUbicazione> findOne(Long id) {
        log.debug("Request to get MagUbicazione : {}", id);
        return magUbicazioneRepository.findById(id);
    }

    /**
     * Delete the magUbicazione by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MagUbicazione : {}", id);
        magUbicazioneRepository.deleteById(id);
    }
}
