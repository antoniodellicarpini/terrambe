package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcTrapAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcTrapAppzRepository;
import com.terrambe.service.dto.RegOpcTrapAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcTrapAppz} entities in the database.
 * The main input is a {@link RegOpcTrapAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcTrapAppz} or a {@link Page} of {@link RegOpcTrapAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcTrapAppzQueryService extends QueryService<RegOpcTrapAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapAppzQueryService.class);

    private final RegOpcTrapAppzRepository regOpcTrapAppzRepository;

    public RegOpcTrapAppzQueryService(RegOpcTrapAppzRepository regOpcTrapAppzRepository) {
        this.regOpcTrapAppzRepository = regOpcTrapAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcTrapAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrapAppz> findByCriteria(RegOpcTrapAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcTrapAppz> specification = createSpecification(criteria);
        return regOpcTrapAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcTrapAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcTrapAppz> findByCriteria(RegOpcTrapAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcTrapAppz> specification = createSpecification(criteria);
        return regOpcTrapAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcTrapAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcTrapAppz> specification = createSpecification(criteria);
        return regOpcTrapAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcTrapAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcTrapAppz> createSpecification(RegOpcTrapAppzCriteria criteria) {
        Specification<RegOpcTrapAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcTrapAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcTrapAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcTrapAppz_.idUnitaProd));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegOpcTrapAppz_.quantita));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcTrapAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcTrapAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcTrapAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcTrapAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcTrapAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcTrapId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcTrapId(),
                    root -> root.join(RegOpcTrapAppz_.appzToOpcTrap, JoinType.LEFT).get(RegOpcTrap_.id)));
            }
        }
        return specification;
    }
}
