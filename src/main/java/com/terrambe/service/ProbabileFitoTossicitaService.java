package com.terrambe.service;

import com.terrambe.domain.ProbabileFitoTossicita;
import com.terrambe.repository.ProbabileFitoTossicitaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProbabileFitoTossicita}.
 */
@Service
@Transactional
public class ProbabileFitoTossicitaService {

    private final Logger log = LoggerFactory.getLogger(ProbabileFitoTossicitaService.class);

    private final ProbabileFitoTossicitaRepository probabileFitoTossicitaRepository;

    public ProbabileFitoTossicitaService(ProbabileFitoTossicitaRepository probabileFitoTossicitaRepository) {
        this.probabileFitoTossicitaRepository = probabileFitoTossicitaRepository;
    }

    /**
     * Save a probabileFitoTossicita.
     *
     * @param probabileFitoTossicita the entity to save.
     * @return the persisted entity.
     */
    public ProbabileFitoTossicita save(ProbabileFitoTossicita probabileFitoTossicita) {
        log.debug("Request to save ProbabileFitoTossicita : {}", probabileFitoTossicita);
        return probabileFitoTossicitaRepository.save(probabileFitoTossicita);
    }

    /**
     * Get all the probabileFitoTossicitas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProbabileFitoTossicita> findAll() {
        log.debug("Request to get all ProbabileFitoTossicitas");
        return probabileFitoTossicitaRepository.findAll();
    }


    /**
     * Get one probabileFitoTossicita by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProbabileFitoTossicita> findOne(Long id) {
        log.debug("Request to get ProbabileFitoTossicita : {}", id);
        return probabileFitoTossicitaRepository.findById(id);
    }

    /**
     * Delete the probabileFitoTossicita by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProbabileFitoTossicita : {}", id);
        probabileFitoTossicitaRepository.deleteById(id);
    }
}
