package com.terrambe.service;

import com.terrambe.domain.TipologiaPermessoOperatore;
import com.terrambe.repository.TipologiaPermessoOperatoreRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipologiaPermessoOperatore}.
 */
@Service
@Transactional
public class TipologiaPermessoOperatoreService {

    private final Logger log = LoggerFactory.getLogger(TipologiaPermessoOperatoreService.class);

    private final TipologiaPermessoOperatoreRepository tipologiaPermessoOperatoreRepository;

    public TipologiaPermessoOperatoreService(TipologiaPermessoOperatoreRepository tipologiaPermessoOperatoreRepository) {
        this.tipologiaPermessoOperatoreRepository = tipologiaPermessoOperatoreRepository;
    }

    /**
     * Save a tipologiaPermessoOperatore.
     *
     * @param tipologiaPermessoOperatore the entity to save.
     * @return the persisted entity.
     */
    public TipologiaPermessoOperatore save(TipologiaPermessoOperatore tipologiaPermessoOperatore) {
        log.debug("Request to save TipologiaPermessoOperatore : {}", tipologiaPermessoOperatore);
        return tipologiaPermessoOperatoreRepository.save(tipologiaPermessoOperatore);
    }

    /**
     * Get all the tipologiaPermessoOperatores.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipologiaPermessoOperatore> findAll() {
        log.debug("Request to get all TipologiaPermessoOperatores");
        return tipologiaPermessoOperatoreRepository.findAll();
    }


    /**
     * Get one tipologiaPermessoOperatore by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipologiaPermessoOperatore> findOne(Long id) {
        log.debug("Request to get TipologiaPermessoOperatore : {}", id);
        return tipologiaPermessoOperatoreRepository.findById(id);
    }

    /**
     * Delete the tipologiaPermessoOperatore by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipologiaPermessoOperatore : {}", id);
        tipologiaPermessoOperatoreRepository.deleteById(id);
    }
}
