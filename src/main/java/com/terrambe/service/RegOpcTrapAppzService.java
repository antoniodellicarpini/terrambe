package com.terrambe.service;

import com.terrambe.domain.RegOpcTrapAppz;
import com.terrambe.repository.RegOpcTrapAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcTrapAppz}.
 */
@Service
@Transactional
public class RegOpcTrapAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapAppzService.class);

    private final RegOpcTrapAppzRepository regOpcTrapAppzRepository;

    public RegOpcTrapAppzService(RegOpcTrapAppzRepository regOpcTrapAppzRepository) {
        this.regOpcTrapAppzRepository = regOpcTrapAppzRepository;
    }

    /**
     * Save a regOpcTrapAppz.
     *
     * @param regOpcTrapAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcTrapAppz save(RegOpcTrapAppz regOpcTrapAppz) {
        log.debug("Request to save RegOpcTrapAppz : {}", regOpcTrapAppz);
        return regOpcTrapAppzRepository.save(regOpcTrapAppz);
    }

    /**
     * Get all the regOpcTrapAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrapAppz> findAll() {
        log.debug("Request to get all RegOpcTrapAppzs");
        return regOpcTrapAppzRepository.findAll();
    }


    /**
     * Get one regOpcTrapAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcTrapAppz> findOne(Long id) {
        log.debug("Request to get RegOpcTrapAppz : {}", id);
        return regOpcTrapAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcTrapAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcTrapAppz : {}", id);
        regOpcTrapAppzRepository.deleteById(id);
    }
}
