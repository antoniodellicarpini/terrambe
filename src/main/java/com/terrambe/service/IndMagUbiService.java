package com.terrambe.service;

import com.terrambe.domain.IndMagUbi;
import com.terrambe.repository.IndMagUbiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndMagUbi}.
 */
@Service
@Transactional
public class IndMagUbiService {

    private final Logger log = LoggerFactory.getLogger(IndMagUbiService.class);

    private final IndMagUbiRepository indMagUbiRepository;

    public IndMagUbiService(IndMagUbiRepository indMagUbiRepository) {
        this.indMagUbiRepository = indMagUbiRepository;
    }

    /**
     * Save a indMagUbi.
     *
     * @param indMagUbi the entity to save.
     * @return the persisted entity.
     */
    public IndMagUbi save(IndMagUbi indMagUbi) {
        log.debug("Request to save IndMagUbi : {}", indMagUbi);
        return indMagUbiRepository.save(indMagUbi);
    }

    /**
     * Get all the indMagUbis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndMagUbi> findAll() {
        log.debug("Request to get all IndMagUbis");
        return indMagUbiRepository.findAll();
    }


    /**
     * Get one indMagUbi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndMagUbi> findOne(Long id) {
        log.debug("Request to get IndMagUbi : {}", id);
        return indMagUbiRepository.findById(id);
    }

    /**
     * Delete the indMagUbi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndMagUbi : {}", id);
        indMagUbiRepository.deleteById(id);
    }
}
