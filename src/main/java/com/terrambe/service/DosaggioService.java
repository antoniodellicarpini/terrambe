package com.terrambe.service;

import com.terrambe.domain.Dosaggio;
import com.terrambe.repository.DosaggioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Dosaggio}.
 */
@Service
@Transactional
public class DosaggioService {

    private final Logger log = LoggerFactory.getLogger(DosaggioService.class);

    private final DosaggioRepository dosaggioRepository;

    public DosaggioService(DosaggioRepository dosaggioRepository) {
        this.dosaggioRepository = dosaggioRepository;
    }

    /**
     * Save a dosaggio.
     *
     * @param dosaggio the entity to save.
     * @return the persisted entity.
     */
    public Dosaggio save(Dosaggio dosaggio) {
        log.debug("Request to save Dosaggio : {}", dosaggio);
        return dosaggioRepository.save(dosaggio);
    }

    /**
     * Get all the dosaggios.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Dosaggio> findAll() {
        log.debug("Request to get all Dosaggios");
        return dosaggioRepository.findAll();
    }


    /**
     * Get one dosaggio by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Dosaggio> findOne(Long id) {
        log.debug("Request to get Dosaggio : {}", id);
        return dosaggioRepository.findById(id);
    }

    /**
     * Delete the dosaggio by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Dosaggio : {}", id);
        dosaggioRepository.deleteById(id);
    }
}
