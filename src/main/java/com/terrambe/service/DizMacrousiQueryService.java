package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DizMacrousi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DizMacrousiRepository;
import com.terrambe.service.dto.DizMacrousiCriteria;

/**
 * Service for executing complex queries for {@link DizMacrousi} entities in the database.
 * The main input is a {@link DizMacrousiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DizMacrousi} or a {@link Page} of {@link DizMacrousi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DizMacrousiQueryService extends QueryService<DizMacrousi> {

    private final Logger log = LoggerFactory.getLogger(DizMacrousiQueryService.class);

    private final DizMacrousiRepository dizMacrousiRepository;

    public DizMacrousiQueryService(DizMacrousiRepository dizMacrousiRepository) {
        this.dizMacrousiRepository = dizMacrousiRepository;
    }

    /**
     * Return a {@link List} of {@link DizMacrousi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DizMacrousi> findByCriteria(DizMacrousiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DizMacrousi> specification = createSpecification(criteria);
        return dizMacrousiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DizMacrousi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DizMacrousi> findByCriteria(DizMacrousiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DizMacrousi> specification = createSpecification(criteria);
        return dizMacrousiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DizMacrousiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DizMacrousi> specification = createSpecification(criteria);
        return dizMacrousiRepository.count(specification);
    }

    /**
     * Function to convert {@link DizMacrousiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DizMacrousi> createSpecification(DizMacrousiCriteria criteria) {
        Specification<DizMacrousi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DizMacrousi_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), DizMacrousi_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), DizMacrousi_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), DizMacrousi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), DizMacrousi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), DizMacrousi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), DizMacrousi_.userIdLastMod));
            }
        }
        return specification;
    }
}
