package com.terrambe.service;

import com.terrambe.domain.Comuni;
import com.terrambe.repository.ComuniRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Comuni}.
 */
@Service
@Transactional
public class ComuniService {

    private final Logger log = LoggerFactory.getLogger(ComuniService.class);

    private final ComuniRepository comuniRepository;

    public ComuniService(ComuniRepository comuniRepository) {
        this.comuniRepository = comuniRepository;
    }

    /**
     * Save a comuni.
     *
     * @param comuni the entity to save.
     * @return the persisted entity.
     */
    public Comuni save(Comuni comuni) {
        log.debug("Request to save Comuni : {}", comuni);
        return comuniRepository.save(comuni);
    }

    /**
     * Get all the comunis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Comuni> findAll() {
        log.debug("Request to get all Comunis");
        return comuniRepository.findAll();
    }


    /**
     * Get one comuni by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Comuni> findOne(Long id) {
        log.debug("Request to get Comuni : {}", id);
        return comuniRepository.findById(id);
    }

    /**
     * Delete the comuni by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Comuni : {}", id);
        comuniRepository.deleteById(id);
    }
}
