package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecAzienda;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecAziendaRepository;
import com.terrambe.service.dto.RecAziendaCriteria;

/**
 * Service for executing complex queries for {@link RecAzienda} entities in the database.
 * The main input is a {@link RecAziendaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecAzienda} or a {@link Page} of {@link RecAzienda} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecAziendaQueryService extends QueryService<RecAzienda> {

    private final Logger log = LoggerFactory.getLogger(RecAziendaQueryService.class);

    private final RecAziendaRepository recAziendaRepository;

    public RecAziendaQueryService(RecAziendaRepository recAziendaRepository) {
        this.recAziendaRepository = recAziendaRepository;
    }

    /**
     * Return a {@link List} of {@link RecAzienda} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecAzienda> findByCriteria(RecAziendaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecAzienda> specification = createSpecification(criteria);
        return recAziendaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecAzienda} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecAzienda> findByCriteria(RecAziendaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecAzienda> specification = createSpecification(criteria);
        return recAziendaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecAziendaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecAzienda> specification = createSpecification(criteria);
        return recAziendaRepository.count(specification);
    }

    /**
     * Function to convert {@link RecAziendaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecAzienda> createSpecification(RecAziendaCriteria criteria) {
        Specification<RecAzienda> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecAzienda_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecAzienda_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecAzienda_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecAzienda_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecAzienda_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecAzienda_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecAzienda_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecAzienda_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecAzienda_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecAzienda_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecAzienda_.userIdLastMod));
            }
            if (criteria.getRecToAziId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToAziId(),
                    root -> root.join(RecAzienda_.recToAzi, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
        }
        return specification;
    }
}
