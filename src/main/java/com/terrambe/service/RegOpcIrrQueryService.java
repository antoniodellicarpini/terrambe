package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcIrr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcIrrRepository;
import com.terrambe.service.dto.RegOpcIrrCriteria;

/**
 * Service for executing complex queries for {@link RegOpcIrr} entities in the database.
 * The main input is a {@link RegOpcIrrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcIrr} or a {@link Page} of {@link RegOpcIrr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcIrrQueryService extends QueryService<RegOpcIrr> {

    private final Logger log = LoggerFactory.getLogger(RegOpcIrrQueryService.class);

    private final RegOpcIrrRepository regOpcIrrRepository;

    public RegOpcIrrQueryService(RegOpcIrrRepository regOpcIrrRepository) {
        this.regOpcIrrRepository = regOpcIrrRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcIrr> findByCriteria(RegOpcIrrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcIrr> specification = createSpecification(criteria);
        return regOpcIrrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcIrr> findByCriteria(RegOpcIrrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcIrr> specification = createSpecification(criteria);
        return regOpcIrrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcIrrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcIrr> specification = createSpecification(criteria);
        return regOpcIrrRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcIrrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcIrr> createSpecification(RegOpcIrrCriteria criteria) {
        Specification<RegOpcIrr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcIrr_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcIrr_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcIrr_.idUnitaProd));
            }
            if (criteria.getDataOperazione() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataOperazione(), RegOpcIrr_.dataOperazione));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcIrr_.tempoImpiegato));
            }
            if (criteria.getVolumeAcqua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVolumeAcqua(), RegOpcIrr_.volumeAcqua));
            }
            if (criteria.getDataInizioPeriod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizioPeriod(), RegOpcIrr_.dataInizioPeriod));
            }
            if (criteria.getDataFinePeriod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFinePeriod(), RegOpcIrr_.dataFinePeriod));
            }
            if (criteria.getFrequenzaGiorni() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFrequenzaGiorni(), RegOpcIrr_.frequenzaGiorni));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcIrr_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcIrr_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcIrr_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcIrr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcIrr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcIrr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcIrr_.userIdLastMod));
            }
            if (criteria.getOpcIrrToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcIrrToAppzId(),
                    root -> root.join(RegOpcIrr_.opcIrrToAppzs, JoinType.LEFT).get(RegOpcIrrAppz_.id)));
            }
            if (criteria.getOpcIrrToTipoIrrId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcIrrToTipoIrrId(),
                    root -> root.join(RegOpcIrr_.opcIrrToTipoIrr, JoinType.LEFT).get(TipoIrr_.id)));
            }
            if (criteria.getOpcIrrToMotivoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcIrrToMotivoId(),
                    root -> root.join(RegOpcIrr_.opcIrrToMotivo, JoinType.LEFT).get(MotivoIrr_.id)));
            }
        }
        return specification;
    }
}
