package com.terrambe.service;

import com.terrambe.domain.RegOpcFitoAppz;
import com.terrambe.repository.RegOpcFitoAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcFitoAppz}.
 */
@Service
@Transactional
public class RegOpcFitoAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoAppzService.class);

    private final RegOpcFitoAppzRepository regOpcFitoAppzRepository;

    public RegOpcFitoAppzService(RegOpcFitoAppzRepository regOpcFitoAppzRepository) {
        this.regOpcFitoAppzRepository = regOpcFitoAppzRepository;
    }

    /**
     * Save a regOpcFitoAppz.
     *
     * @param regOpcFitoAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcFitoAppz save(RegOpcFitoAppz regOpcFitoAppz) {
        log.debug("Request to save RegOpcFitoAppz : {}", regOpcFitoAppz);
        return regOpcFitoAppzRepository.save(regOpcFitoAppz);
    }

    /**
     * Get all the regOpcFitoAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcFitoAppz> findAll() {
        log.debug("Request to get all RegOpcFitoAppzs");
        return regOpcFitoAppzRepository.findAll();
    }


    /**
     * Get one regOpcFitoAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcFitoAppz> findOne(Long id) {
        log.debug("Request to get RegOpcFitoAppz : {}", id);
        return regOpcFitoAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcFitoAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcFitoAppz : {}", id);
        regOpcFitoAppzRepository.deleteById(id);
    }
}
