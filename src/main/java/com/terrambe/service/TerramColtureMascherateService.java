package com.terrambe.service;

import com.terrambe.domain.TerramColtureMascherate;
import com.terrambe.repository.TerramColtureMascherateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TerramColtureMascherate}.
 */
@Service
@Transactional
public class TerramColtureMascherateService {

    private final Logger log = LoggerFactory.getLogger(TerramColtureMascherateService.class);

    private final TerramColtureMascherateRepository terramColtureMascherateRepository;

    public TerramColtureMascherateService(TerramColtureMascherateRepository terramColtureMascherateRepository) {
        this.terramColtureMascherateRepository = terramColtureMascherateRepository;
    }

    /**
     * Save a terramColtureMascherate.
     *
     * @param terramColtureMascherate the entity to save.
     * @return the persisted entity.
     */
    public TerramColtureMascherate save(TerramColtureMascherate terramColtureMascherate) {
        log.debug("Request to save TerramColtureMascherate : {}", terramColtureMascherate);
        return terramColtureMascherateRepository.save(terramColtureMascherate);
    }

    /**
     * Get all the terramColtureMascherates.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TerramColtureMascherate> findAll() {
        log.debug("Request to get all TerramColtureMascherates");
        return terramColtureMascherateRepository.findAll();
    }


    /**
     * Get one terramColtureMascherate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TerramColtureMascherate> findOne(Long id) {
        log.debug("Request to get TerramColtureMascherate : {}", id);
        return terramColtureMascherateRepository.findById(id);
    }

    /**
     * Delete the terramColtureMascherate by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TerramColtureMascherate : {}", id);
        terramColtureMascherateRepository.deleteById(id);
    }
}
