package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Avversita;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AvversitaRepository;
import com.terrambe.service.dto.AvversitaCriteria;

/**
 * Service for executing complex queries for {@link Avversita} entities in the database.
 * The main input is a {@link AvversitaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Avversita} or a {@link Page} of {@link Avversita} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AvversitaQueryService extends QueryService<Avversita> {

    private final Logger log = LoggerFactory.getLogger(AvversitaQueryService.class);

    private final AvversitaRepository avversitaRepository;

    public AvversitaQueryService(AvversitaRepository avversitaRepository) {
        this.avversitaRepository = avversitaRepository;
    }

    /**
     * Return a {@link List} of {@link Avversita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Avversita> findByCriteria(AvversitaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Avversita> specification = createSpecification(criteria);
        return avversitaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Avversita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Avversita> findByCriteria(AvversitaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Avversita> specification = createSpecification(criteria);
        return avversitaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AvversitaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Avversita> specification = createSpecification(criteria);
        return avversitaRepository.count(specification);
    }

    /**
     * Function to convert {@link AvversitaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Avversita> createSpecification(AvversitaCriteria criteria) {
        Specification<Avversita> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Avversita_.id));
            }
            if (criteria.getCodAvversita() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodAvversita(), Avversita_.codAvversita));
            }
            if (criteria.getNomeSci() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeSci(), Avversita_.nomeSci));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Avversita_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Avversita_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Avversita_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Avversita_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Avversita_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Avversita_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Avversita_.userIdLastMod));
            }
            if (criteria.getAvversToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getAvversToRaccordoId(),
                    root -> root.join(Avversita_.avversToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
        }
        return specification;
    }
}
