package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Appezzamenti;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AppezzamentiRepository;
import com.terrambe.service.dto.AppezzamentiCriteria;

/**
 * Service for executing complex queries for {@link Appezzamenti} entities in the database.
 * The main input is a {@link AppezzamentiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Appezzamenti} or a {@link Page} of {@link Appezzamenti} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AppezzamentiQueryService extends QueryService<Appezzamenti> {

    private final Logger log = LoggerFactory.getLogger(AppezzamentiQueryService.class);

    private final AppezzamentiRepository appezzamentiRepository;

    public AppezzamentiQueryService(AppezzamentiRepository appezzamentiRepository) {
        this.appezzamentiRepository = appezzamentiRepository;
    }

    /**
     * Return a {@link List} of {@link Appezzamenti} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Appezzamenti> findByCriteria(AppezzamentiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Appezzamenti> specification = createSpecification(criteria);
        return appezzamentiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Appezzamenti} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Appezzamenti> findByCriteria(AppezzamentiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Appezzamenti> specification = createSpecification(criteria);
        return appezzamentiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AppezzamentiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Appezzamenti> specification = createSpecification(criteria);
        return appezzamentiRepository.count(specification);
    }

    /**
     * Function to convert {@link AppezzamentiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Appezzamenti> createSpecification(AppezzamentiCriteria criteria) {
        Specification<Appezzamenti> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Appezzamenti_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), Appezzamenti_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), Appezzamenti_.idUnitaProd));
            }
            if (criteria.getNomeAppezzamento() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeAppezzamento(), Appezzamenti_.nomeAppezzamento));
            }
            if (criteria.getSuperficeHa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSuperficeHa(), Appezzamenti_.superficeHa));
            }
            if (criteria.getRotazione() != null) {
                specification = specification.and(buildSpecification(criteria.getRotazione(), Appezzamenti_.rotazione));
            }
            if (criteria.getSerra() != null) {
                specification = specification.and(buildSpecification(criteria.getSerra(), Appezzamenti_.serra));
            }
            if (criteria.getIrrigabile() != null) {
                specification = specification.and(buildSpecification(criteria.getIrrigabile(), Appezzamenti_.irrigabile));
            }
            if (criteria.getUuidEsri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuidEsri(), Appezzamenti_.uuidEsri));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Appezzamenti_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Appezzamenti_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Appezzamenti_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Appezzamenti_.userIdLastMod));
            }
            if (criteria.getAppzToPartId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToPartId(),
                    root -> root.join(Appezzamenti_.appzToParts, JoinType.LEFT).get(AziSuperficieParticella_.id)));
            }
            if (criteria.getAppezToVincoliTerId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppezToVincoliTerId(),
                    root -> root.join(Appezzamenti_.appezToVincoliTers, JoinType.LEFT).get(TipologiaVincoliTerritoriali_.id)));
            }
            if (criteria.getAppezToCampiId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppezToCampiId(),
                    root -> root.join(Appezzamenti_.appezToCampi, JoinType.LEFT).get(Campi_.id)));
            }
            if (criteria.getAppzToBioAppzTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToBioAppzTipoId(),
                    root -> root.join(Appezzamenti_.appzToBioAppzTipo, JoinType.LEFT).get(BioAppzTipo_.id)));
            }
            if (criteria.getAppeToColtId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppeToColtId(),
                    root -> root.join(Appezzamenti_.appeToColts, JoinType.LEFT).get(ColturaAppezzamento_.id)));
            }
        }
        return specification;
    }
}
