package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.OpePatentinoTipologiaRepository;
import com.terrambe.service.dto.OpePatentinoTipologiaCriteria;

/**
 * Service for executing complex queries for {@link OpePatentinoTipologia} entities in the database.
 * The main input is a {@link OpePatentinoTipologiaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OpePatentinoTipologia} or a {@link Page} of {@link OpePatentinoTipologia} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OpePatentinoTipologiaQueryService extends QueryService<OpePatentinoTipologia> {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoTipologiaQueryService.class);

    private final OpePatentinoTipologiaRepository opePatentinoTipologiaRepository;

    public OpePatentinoTipologiaQueryService(OpePatentinoTipologiaRepository opePatentinoTipologiaRepository) {
        this.opePatentinoTipologiaRepository = opePatentinoTipologiaRepository;
    }

    /**
     * Return a {@link List} of {@link OpePatentinoTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OpePatentinoTipologia> findByCriteria(OpePatentinoTipologiaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OpePatentinoTipologia> specification = createSpecification(criteria);
        return opePatentinoTipologiaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OpePatentinoTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OpePatentinoTipologia> findByCriteria(OpePatentinoTipologiaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OpePatentinoTipologia> specification = createSpecification(criteria);
        return opePatentinoTipologiaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OpePatentinoTipologiaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OpePatentinoTipologia> specification = createSpecification(criteria);
        return opePatentinoTipologiaRepository.count(specification);
    }

    /**
     * Function to convert {@link OpePatentinoTipologiaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OpePatentinoTipologia> createSpecification(OpePatentinoTipologiaCriteria criteria) {
        Specification<OpePatentinoTipologia> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OpePatentinoTipologia_.id));
            }
            if (criteria.getTpologia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTpologia(), OpePatentinoTipologia_.tpologia));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), OpePatentinoTipologia_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), OpePatentinoTipologia_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), OpePatentinoTipologia_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), OpePatentinoTipologia_.userIdLastMod));
            }
            if (criteria.getOpePatTipToOpePateId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpePatTipToOpePateId(),
                    root -> root.join(OpePatentinoTipologia_.opePatTipToOpePates, JoinType.LEFT).get(OpePatentino_.id)));
            }
        }
        return specification;
    }
}
