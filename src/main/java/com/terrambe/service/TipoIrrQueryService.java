package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoIrr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoIrrRepository;
import com.terrambe.service.dto.TipoIrrCriteria;

/**
 * Service for executing complex queries for {@link TipoIrr} entities in the database.
 * The main input is a {@link TipoIrrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoIrr} or a {@link Page} of {@link TipoIrr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoIrrQueryService extends QueryService<TipoIrr> {

    private final Logger log = LoggerFactory.getLogger(TipoIrrQueryService.class);

    private final TipoIrrRepository tipoIrrRepository;

    public TipoIrrQueryService(TipoIrrRepository tipoIrrRepository) {
        this.tipoIrrRepository = tipoIrrRepository;
    }

    /**
     * Return a {@link List} of {@link TipoIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoIrr> findByCriteria(TipoIrrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoIrr> specification = createSpecification(criteria);
        return tipoIrrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoIrr> findByCriteria(TipoIrrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoIrr> specification = createSpecification(criteria);
        return tipoIrrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoIrrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoIrr> specification = createSpecification(criteria);
        return tipoIrrRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoIrrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoIrr> createSpecification(TipoIrrCriteria criteria) {
        Specification<TipoIrr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoIrr_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoIrr_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoIrr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoIrr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoIrr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoIrr_.userIdLastMod));
            }
            if (criteria.getTipoIrrToOpcIrrId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoIrrToOpcIrrId(),
                    root -> root.join(TipoIrr_.tipoIrrToOpcIrrs, JoinType.LEFT).get(RegOpcIrr_.id)));
            }
        }
        return specification;
    }
}
