package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DosaggioTrattamento;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DosaggioTrattamentoRepository;
import com.terrambe.service.dto.DosaggioTrattamentoCriteria;

/**
 * Service for executing complex queries for {@link DosaggioTrattamento} entities in the database.
 * The main input is a {@link DosaggioTrattamentoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DosaggioTrattamento} or a {@link Page} of {@link DosaggioTrattamento} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DosaggioTrattamentoQueryService extends QueryService<DosaggioTrattamento> {

    private final Logger log = LoggerFactory.getLogger(DosaggioTrattamentoQueryService.class);

    private final DosaggioTrattamentoRepository dosaggioTrattamentoRepository;

    public DosaggioTrattamentoQueryService(DosaggioTrattamentoRepository dosaggioTrattamentoRepository) {
        this.dosaggioTrattamentoRepository = dosaggioTrattamentoRepository;
    }

    /**
     * Return a {@link List} of {@link DosaggioTrattamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DosaggioTrattamento> findByCriteria(DosaggioTrattamentoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DosaggioTrattamento> specification = createSpecification(criteria);
        return dosaggioTrattamentoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DosaggioTrattamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DosaggioTrattamento> findByCriteria(DosaggioTrattamentoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DosaggioTrattamento> specification = createSpecification(criteria);
        return dosaggioTrattamentoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DosaggioTrattamentoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DosaggioTrattamento> specification = createSpecification(criteria);
        return dosaggioTrattamentoRepository.count(specification);
    }

    /**
     * Function to convert {@link DosaggioTrattamentoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DosaggioTrattamento> createSpecification(DosaggioTrattamentoCriteria criteria) {
        Specification<DosaggioTrattamento> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DosaggioTrattamento_.id));
            }
            if (criteria.getTotaleProdottoUtilizzato() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotaleProdottoUtilizzato(), DosaggioTrattamento_.totaleProdottoUtilizzato));
            }
            if (criteria.getEtichettafitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettafitoId(),
                    root -> root.join(DosaggioTrattamento_.etichettafito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getDosTrattToRegOpcFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getDosTrattToRegOpcFitoId(),
                    root -> root.join(DosaggioTrattamento_.dosTrattToRegOpcFitos, JoinType.LEFT).get(RegOpcFito_.id)));
            }
        }
        return specification;
    }
}
