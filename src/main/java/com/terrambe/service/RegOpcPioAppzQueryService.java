package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcPioAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcPioAppzRepository;
import com.terrambe.service.dto.RegOpcPioAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcPioAppz} entities in the database.
 * The main input is a {@link RegOpcPioAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcPioAppz} or a {@link Page} of {@link RegOpcPioAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcPioAppzQueryService extends QueryService<RegOpcPioAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioAppzQueryService.class);

    private final RegOpcPioAppzRepository regOpcPioAppzRepository;

    public RegOpcPioAppzQueryService(RegOpcPioAppzRepository regOpcPioAppzRepository) {
        this.regOpcPioAppzRepository = regOpcPioAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcPioAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPioAppz> findByCriteria(RegOpcPioAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcPioAppz> specification = createSpecification(criteria);
        return regOpcPioAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcPioAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcPioAppz> findByCriteria(RegOpcPioAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcPioAppz> specification = createSpecification(criteria);
        return regOpcPioAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcPioAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcPioAppz> specification = createSpecification(criteria);
        return regOpcPioAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcPioAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcPioAppz> createSpecification(RegOpcPioAppzCriteria criteria) {
        Specification<RegOpcPioAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcPioAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcPioAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcPioAppz_.idUnitaProd));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcPioAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcPioAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcPioAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcPioAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcPioAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcPioId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcPioId(),
                    root -> root.join(RegOpcPioAppz_.appzToOpcPio, JoinType.LEFT).get(RegOpcPio_.id)));
            }
        }
        return specification;
    }
}
