package com.terrambe.service;

import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.repository.Agea20152020BDFRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Agea20152020BDF}.
 */
@Service
@Transactional
public class Agea20152020BDFService {

    private final Logger log = LoggerFactory.getLogger(Agea20152020BDFService.class);

    private final Agea20152020BDFRepository agea20152020BDFRepository;

    public Agea20152020BDFService(Agea20152020BDFRepository agea20152020BDFRepository) {
        this.agea20152020BDFRepository = agea20152020BDFRepository;
    }

    /**
     * Save a agea20152020BDF.
     *
     * @param agea20152020BDF the entity to save.
     * @return the persisted entity.
     */
    public Agea20152020BDF save(Agea20152020BDF agea20152020BDF) {
        log.debug("Request to save Agea20152020BDF : {}", agea20152020BDF);
        return agea20152020BDFRepository.save(agea20152020BDF);
    }

    /**
     * Get all the agea20152020BDFS.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Agea20152020BDF> findAll() {
        log.debug("Request to get all Agea20152020BDFS");
        return agea20152020BDFRepository.findAll();
    }


    /**
     * Get one agea20152020BDF by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Agea20152020BDF> findOne(Long id) {
        log.debug("Request to get Agea20152020BDF : {}", id);
        return agea20152020BDFRepository.findById(id);
    }

    /**
     * Delete the agea20152020BDF by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Agea20152020BDF : {}", id);
        agea20152020BDFRepository.deleteById(id);
    }
}
