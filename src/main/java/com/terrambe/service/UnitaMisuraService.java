package com.terrambe.service;

import com.terrambe.domain.UnitaMisura;
import com.terrambe.repository.UnitaMisuraRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UnitaMisura}.
 */
@Service
@Transactional
public class UnitaMisuraService {

    private final Logger log = LoggerFactory.getLogger(UnitaMisuraService.class);

    private final UnitaMisuraRepository unitaMisuraRepository;

    public UnitaMisuraService(UnitaMisuraRepository unitaMisuraRepository) {
        this.unitaMisuraRepository = unitaMisuraRepository;
    }

    /**
     * Save a unitaMisura.
     *
     * @param unitaMisura the entity to save.
     * @return the persisted entity.
     */
    public UnitaMisura save(UnitaMisura unitaMisura) {
        log.debug("Request to save UnitaMisura : {}", unitaMisura);
        return unitaMisuraRepository.save(unitaMisura);
    }

    /**
     * Get all the unitaMisuras.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UnitaMisura> findAll() {
        log.debug("Request to get all UnitaMisuras");
        return unitaMisuraRepository.findAll();
    }


    /**
     * Get one unitaMisura by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UnitaMisura> findOne(Long id) {
        log.debug("Request to get UnitaMisura : {}", id);
        return unitaMisuraRepository.findById(id);
    }

    /**
     * Delete the unitaMisura by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UnitaMisura : {}", id);
        unitaMisuraRepository.deleteById(id);
    }
}
