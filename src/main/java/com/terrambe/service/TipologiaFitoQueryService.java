package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipologiaFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipologiaFitoRepository;
import com.terrambe.service.dto.TipologiaFitoCriteria;

/**
 * Service for executing complex queries for {@link TipologiaFito} entities in the database.
 * The main input is a {@link TipologiaFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipologiaFito} or a {@link Page} of {@link TipologiaFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipologiaFitoQueryService extends QueryService<TipologiaFito> {

    private final Logger log = LoggerFactory.getLogger(TipologiaFitoQueryService.class);

    private final TipologiaFitoRepository tipologiaFitoRepository;

    public TipologiaFitoQueryService(TipologiaFitoRepository tipologiaFitoRepository) {
        this.tipologiaFitoRepository = tipologiaFitoRepository;
    }

    /**
     * Return a {@link List} of {@link TipologiaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipologiaFito> findByCriteria(TipologiaFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipologiaFito> specification = createSpecification(criteria);
        return tipologiaFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipologiaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipologiaFito> findByCriteria(TipologiaFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipologiaFito> specification = createSpecification(criteria);
        return tipologiaFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipologiaFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipologiaFito> specification = createSpecification(criteria);
        return tipologiaFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link TipologiaFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipologiaFito> createSpecification(TipologiaFitoCriteria criteria) {
        Specification<TipologiaFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipologiaFito_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), TipologiaFito_.codice));
            }
            if (criteria.getTipologia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipologia(), TipologiaFito_.tipologia));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), TipologiaFito_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), TipologiaFito_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), TipologiaFito_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipologiaFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipologiaFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipologiaFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipologiaFito_.userIdLastMod));
            }
            if (criteria.getTipofitofarmacoetichettaId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipofitofarmacoetichettaId(),
                    root -> root.join(TipologiaFito_.tipofitofarmacoetichettas, JoinType.LEFT).get(EtiTipologiaFito_.id)));
            }
        }
        return specification;
    }
}
