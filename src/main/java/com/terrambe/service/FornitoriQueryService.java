package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Fornitori;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FornitoriRepository;
import com.terrambe.service.dto.FornitoriCriteria;

/**
 * Service for executing complex queries for {@link Fornitori} entities in the database.
 * The main input is a {@link FornitoriCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Fornitori} or a {@link Page} of {@link Fornitori} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FornitoriQueryService extends QueryService<Fornitori> {

    private final Logger log = LoggerFactory.getLogger(FornitoriQueryService.class);

    private final FornitoriRepository fornitoriRepository;

    public FornitoriQueryService(FornitoriRepository fornitoriRepository) {
        this.fornitoriRepository = fornitoriRepository;
    }

    /**
     * Return a {@link List} of {@link Fornitori} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Fornitori> findByCriteria(FornitoriCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Fornitori> specification = createSpecification(criteria);
        return fornitoriRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Fornitori} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Fornitori> findByCriteria(FornitoriCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Fornitori> specification = createSpecification(criteria);
        return fornitoriRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FornitoriCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Fornitori> specification = createSpecification(criteria);
        return fornitoriRepository.count(specification);
    }

    /**
     * Function to convert {@link FornitoriCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Fornitori> createSpecification(FornitoriCriteria criteria) {
        Specification<Fornitori> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Fornitori_.id));
            }
            if (criteria.getRegistroImprese() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegistroImprese(), Fornitori_.registroImprese));
            }
            if (criteria.getNumIscrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumIscrizione(), Fornitori_.numIscrizione));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), Fornitori_.idAzienda));
            }
            if (criteria.getNomeFornitore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeFornitore(), Fornitori_.nomeFornitore));
            }
            if (criteria.getPiva() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPiva(), Fornitori_.piva));
            }
            if (criteria.getCodiFisc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiFisc(), Fornitori_.codiFisc));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Fornitori_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Fornitori_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Fornitori_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Fornitori_.userIdLastMod));
            }
            if (criteria.getFornToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornToIndId(),
                    root -> root.join(Fornitori_.fornToInds, JoinType.LEFT).get(IndForn_.id)));
            }
            if (criteria.getFornToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornToRecId(),
                    root -> root.join(Fornitori_.fornToRecs, JoinType.LEFT).get(RecForn_.id)));
            }
            if (criteria.getFornitoritipologiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornitoritipologiaId(),
                    root -> root.join(Fornitori_.fornitoritipologias, JoinType.LEFT).get(FornitoriTipologia_.id)));
            }
            if (criteria.getFornitoriToCaricoFertId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornitoriToCaricoFertId(),
                    root -> root.join(Fornitori_.fornitoriToCaricoFerts, JoinType.LEFT).get(RegCaricoFertilizzante_.id)));
            }
            if (criteria.getFornitoriToCaricoFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornitoriToCaricoFitoId(),
                    root -> root.join(Fornitori_.fornitoriToCaricoFitos, JoinType.LEFT).get(RegCaricoFitoFarmaco_.id)));
            }
            if (criteria.getFornitoriToCaricoTrapId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornitoriToCaricoTrapId(),
                    root -> root.join(Fornitori_.fornitoriToCaricoTraps, JoinType.LEFT).get(RegCaricoTrappole_.id)));
            }
            if (criteria.getFornToOpeAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornToOpeAnaId(),
                    root -> root.join(Fornitori_.fornToOpeAnas, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
