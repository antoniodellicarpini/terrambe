package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.MotivoIrr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.MotivoIrrRepository;
import com.terrambe.service.dto.MotivoIrrCriteria;

/**
 * Service for executing complex queries for {@link MotivoIrr} entities in the database.
 * The main input is a {@link MotivoIrrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MotivoIrr} or a {@link Page} of {@link MotivoIrr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MotivoIrrQueryService extends QueryService<MotivoIrr> {

    private final Logger log = LoggerFactory.getLogger(MotivoIrrQueryService.class);

    private final MotivoIrrRepository motivoIrrRepository;

    public MotivoIrrQueryService(MotivoIrrRepository motivoIrrRepository) {
        this.motivoIrrRepository = motivoIrrRepository;
    }

    /**
     * Return a {@link List} of {@link MotivoIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoIrr> findByCriteria(MotivoIrrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MotivoIrr> specification = createSpecification(criteria);
        return motivoIrrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MotivoIrr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MotivoIrr> findByCriteria(MotivoIrrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MotivoIrr> specification = createSpecification(criteria);
        return motivoIrrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MotivoIrrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MotivoIrr> specification = createSpecification(criteria);
        return motivoIrrRepository.count(specification);
    }

    /**
     * Function to convert {@link MotivoIrrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MotivoIrr> createSpecification(MotivoIrrCriteria criteria) {
        Specification<MotivoIrr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MotivoIrr_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), MotivoIrr_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), MotivoIrr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), MotivoIrr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), MotivoIrr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), MotivoIrr_.userIdLastMod));
            }
            if (criteria.getMotivoToOpcIrrId() != null) {
                specification = specification.and(buildSpecification(criteria.getMotivoToOpcIrrId(),
                    root -> root.join(MotivoIrr_.motivoToOpcIrrs, JoinType.LEFT).get(RegOpcIrr_.id)));
            }
        }
        return specification;
    }
}
