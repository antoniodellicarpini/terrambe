package com.terrambe.service;

import com.terrambe.domain.TipoRacc;
import com.terrambe.repository.TipoRaccRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoRacc}.
 */
@Service
@Transactional
public class TipoRaccService {

    private final Logger log = LoggerFactory.getLogger(TipoRaccService.class);

    private final TipoRaccRepository tipoRaccRepository;

    public TipoRaccService(TipoRaccRepository tipoRaccRepository) {
        this.tipoRaccRepository = tipoRaccRepository;
    }

    /**
     * Save a tipoRacc.
     *
     * @param tipoRacc the entity to save.
     * @return the persisted entity.
     */
    public TipoRacc save(TipoRacc tipoRacc) {
        log.debug("Request to save TipoRacc : {}", tipoRacc);
        return tipoRaccRepository.save(tipoRacc);
    }

    /**
     * Get all the tipoRaccs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoRacc> findAll() {
        log.debug("Request to get all TipoRaccs");
        return tipoRaccRepository.findAll();
    }


    /**
     * Get one tipoRacc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoRacc> findOne(Long id) {
        log.debug("Request to get TipoRacc : {}", id);
        return tipoRaccRepository.findById(id);
    }

    /**
     * Delete the tipoRacc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoRacc : {}", id);
        tipoRaccRepository.deleteById(id);
    }
}
