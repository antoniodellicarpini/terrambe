package com.terrambe.service;

import com.terrambe.domain.RegOpcFito;
import com.terrambe.repository.RegOpcFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcFito}.
 */
@Service
@Transactional
public class RegOpcFitoService {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoService.class);

    private final RegOpcFitoRepository regOpcFitoRepository;

    public RegOpcFitoService(RegOpcFitoRepository regOpcFitoRepository) {
        this.regOpcFitoRepository = regOpcFitoRepository;
    }

    /**
     * Save a regOpcFito.
     *
     * @param regOpcFito the entity to save.
     * @return the persisted entity.
     */
    public RegOpcFito save(RegOpcFito regOpcFito) {
        log.debug("Request to save RegOpcFito : {}", regOpcFito);
        return regOpcFitoRepository.save(regOpcFito);
    }

    /**
     * Get all the regOpcFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcFito> findAll() {
        log.debug("Request to get all RegOpcFitos");
        return regOpcFitoRepository.findAll();
    }


    /**
     * Get one regOpcFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcFito> findOne(Long id) {
        log.debug("Request to get RegOpcFito : {}", id);
        return regOpcFitoRepository.findById(id);
    }

    /**
     * Delete the regOpcFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcFito : {}", id);
        regOpcFitoRepository.deleteById(id);
    }
}
