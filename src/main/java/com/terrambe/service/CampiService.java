package com.terrambe.service;

import com.terrambe.domain.Campi;
import com.terrambe.repository.CampiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Campi}.
 */
@Service
@Transactional
public class CampiService {

    private final Logger log = LoggerFactory.getLogger(CampiService.class);

    private final CampiRepository campiRepository;

    public CampiService(CampiRepository campiRepository) {
        this.campiRepository = campiRepository;
    }

    /**
     * Save a campi.
     *
     * @param campi the entity to save.
     * @return the persisted entity.
     */
    public Campi save(Campi campi) {
        log.debug("Request to save Campi : {}", campi);
        return campiRepository.save(campi);
    }

    /**
     * Get all the campis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Campi> findAll() {
        log.debug("Request to get all Campis");
        return campiRepository.findAll();
    }


    /**
     * Get one campi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Campi> findOne(Long id) {
        log.debug("Request to get Campi : {}", id);
        return campiRepository.findById(id);
    }

    /**
     * Delete the campi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Campi : {}", id);
        campiRepository.deleteById(id);
    }
}
