package com.terrambe.service;

import com.terrambe.domain.IndOpeAna;
import com.terrambe.repository.IndOpeAnaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndOpeAna}.
 */
@Service
@Transactional
public class IndOpeAnaService {

    private final Logger log = LoggerFactory.getLogger(IndOpeAnaService.class);

    private final IndOpeAnaRepository indOpeAnaRepository;

    public IndOpeAnaService(IndOpeAnaRepository indOpeAnaRepository) {
        this.indOpeAnaRepository = indOpeAnaRepository;
    }

    /**
     * Save a indOpeAna.
     *
     * @param indOpeAna the entity to save.
     * @return the persisted entity.
     */
    public IndOpeAna save(IndOpeAna indOpeAna) {
        log.debug("Request to save IndOpeAna : {}", indOpeAna);
        return indOpeAnaRepository.save(indOpeAna);
    }

    /**
     * Get all the indOpeAnas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndOpeAna> findAll() {
        log.debug("Request to get all IndOpeAnas");
        return indOpeAnaRepository.findAll();
    }


    /**
     * Get one indOpeAna by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndOpeAna> findOne(Long id) {
        log.debug("Request to get IndOpeAna : {}", id);
        return indOpeAnaRepository.findById(id);
    }

    /**
     * Delete the indOpeAna by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndOpeAna : {}", id);
        indOpeAnaRepository.deleteById(id);
    }
}
