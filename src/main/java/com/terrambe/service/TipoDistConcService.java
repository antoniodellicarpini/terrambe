package com.terrambe.service;

import com.terrambe.domain.TipoDistConc;
import com.terrambe.repository.TipoDistConcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoDistConc}.
 */
@Service
@Transactional
public class TipoDistConcService {

    private final Logger log = LoggerFactory.getLogger(TipoDistConcService.class);

    private final TipoDistConcRepository tipoDistConcRepository;

    public TipoDistConcService(TipoDistConcRepository tipoDistConcRepository) {
        this.tipoDistConcRepository = tipoDistConcRepository;
    }

    /**
     * Save a tipoDistConc.
     *
     * @param tipoDistConc the entity to save.
     * @return the persisted entity.
     */
    public TipoDistConc save(TipoDistConc tipoDistConc) {
        log.debug("Request to save TipoDistConc : {}", tipoDistConc);
        return tipoDistConcRepository.save(tipoDistConc);
    }

    /**
     * Get all the tipoDistConcs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoDistConc> findAll() {
        log.debug("Request to get all TipoDistConcs");
        return tipoDistConcRepository.findAll();
    }


    /**
     * Get one tipoDistConc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoDistConc> findOne(Long id) {
        log.debug("Request to get TipoDistConc : {}", id);
        return tipoDistConcRepository.findById(id);
    }

    /**
     * Delete the tipoDistConc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoDistConc : {}", id);
        tipoDistConcRepository.deleteById(id);
    }
}
