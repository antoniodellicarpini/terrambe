package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AtrMarchio;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AtrMarchioRepository;
import com.terrambe.service.dto.AtrMarchioCriteria;

/**
 * Service for executing complex queries for {@link AtrMarchio} entities in the database.
 * The main input is a {@link AtrMarchioCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AtrMarchio} or a {@link Page} of {@link AtrMarchio} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AtrMarchioQueryService extends QueryService<AtrMarchio> {

    private final Logger log = LoggerFactory.getLogger(AtrMarchioQueryService.class);

    private final AtrMarchioRepository atrMarchioRepository;

    public AtrMarchioQueryService(AtrMarchioRepository atrMarchioRepository) {
        this.atrMarchioRepository = atrMarchioRepository;
    }

    /**
     * Return a {@link List} of {@link AtrMarchio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AtrMarchio> findByCriteria(AtrMarchioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AtrMarchio> specification = createSpecification(criteria);
        return atrMarchioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AtrMarchio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AtrMarchio> findByCriteria(AtrMarchioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AtrMarchio> specification = createSpecification(criteria);
        return atrMarchioRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AtrMarchioCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AtrMarchio> specification = createSpecification(criteria);
        return atrMarchioRepository.count(specification);
    }

    /**
     * Function to convert {@link AtrMarchioCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AtrMarchio> createSpecification(AtrMarchioCriteria criteria) {
        Specification<AtrMarchio> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AtrMarchio_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), AtrMarchio_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AtrMarchio_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AtrMarchio_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AtrMarchio_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AtrMarchio_.userIdLastMod));
            }
            if (criteria.getAtrTipologiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAtrTipologiaId(),
                    root -> root.join(AtrMarchio_.atrTipologias, JoinType.LEFT).get(AtrTipologia_.id)));
            }
        }
        return specification;
    }
}
