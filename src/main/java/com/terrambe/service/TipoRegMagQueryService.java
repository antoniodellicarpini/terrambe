package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoRegMag;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoRegMagRepository;
import com.terrambe.service.dto.TipoRegMagCriteria;

/**
 * Service for executing complex queries for {@link TipoRegMag} entities in the database.
 * The main input is a {@link TipoRegMagCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoRegMag} or a {@link Page} of {@link TipoRegMag} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoRegMagQueryService extends QueryService<TipoRegMag> {

    private final Logger log = LoggerFactory.getLogger(TipoRegMagQueryService.class);

    private final TipoRegMagRepository tipoRegMagRepository;

    public TipoRegMagQueryService(TipoRegMagRepository tipoRegMagRepository) {
        this.tipoRegMagRepository = tipoRegMagRepository;
    }

    /**
     * Return a {@link List} of {@link TipoRegMag} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoRegMag> findByCriteria(TipoRegMagCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoRegMag> specification = createSpecification(criteria);
        return tipoRegMagRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoRegMag} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoRegMag> findByCriteria(TipoRegMagCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoRegMag> specification = createSpecification(criteria);
        return tipoRegMagRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoRegMagCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoRegMag> specification = createSpecification(criteria);
        return tipoRegMagRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoRegMagCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoRegMag> createSpecification(TipoRegMagCriteria criteria) {
        Specification<TipoRegMag> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoRegMag_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoRegMag_.descrizione));
            }
            if (criteria.getMoltiplicatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMoltiplicatore(), TipoRegMag_.moltiplicatore));
            }
            if (criteria.getDataInizioMag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizioMag(), TipoRegMag_.dataInizioMag));
            }
            if (criteria.getDataFineMag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineMag(), TipoRegMag_.dataFineMag));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoRegMag_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoRegMag_.userIdLastMod));
            }
            if (criteria.getTipoRegToRegFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoRegToRegFitoId(),
                    root -> root.join(TipoRegMag_.tipoRegToRegFitos, JoinType.LEFT).get(RegCaricoFitoFarmaco_.id)));
            }
            if (criteria.getTipoRegToRegFertId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoRegToRegFertId(),
                    root -> root.join(TipoRegMag_.tipoRegToRegFerts, JoinType.LEFT).get(RegCaricoFertilizzante_.id)));
            }
            if (criteria.getTipoRegToRegTrapId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoRegToRegTrapId(),
                    root -> root.join(TipoRegMag_.tipoRegToRegTraps, JoinType.LEFT).get(RegCaricoTrappole_.id)));
            }
        }
        return specification;
    }
}
