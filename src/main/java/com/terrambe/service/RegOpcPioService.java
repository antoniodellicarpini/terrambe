package com.terrambe.service;

import com.terrambe.domain.RegOpcPio;
import com.terrambe.repository.RegOpcPioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcPio}.
 */
@Service
@Transactional
public class RegOpcPioService {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioService.class);

    private final RegOpcPioRepository regOpcPioRepository;

    public RegOpcPioService(RegOpcPioRepository regOpcPioRepository) {
        this.regOpcPioRepository = regOpcPioRepository;
    }

    /**
     * Save a regOpcPio.
     *
     * @param regOpcPio the entity to save.
     * @return the persisted entity.
     */
    public RegOpcPio save(RegOpcPio regOpcPio) {
        log.debug("Request to save RegOpcPio : {}", regOpcPio);
        return regOpcPioRepository.save(regOpcPio);
    }

    /**
     * Get all the regOpcPios.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPio> findAll() {
        log.debug("Request to get all RegOpcPios");
        return regOpcPioRepository.findAll();
    }


    /**
     * Get one regOpcPio by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcPio> findOne(Long id) {
        log.debug("Request to get RegOpcPio : {}", id);
        return regOpcPioRepository.findById(id);
    }

    /**
     * Delete the regOpcPio by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcPio : {}", id);
        regOpcPioRepository.deleteById(id);
    }
}
