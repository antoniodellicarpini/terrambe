package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndAziendaCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndAziendaCliRepository;
import com.terrambe.service.dto.IndAziendaCliCriteria;

/**
 * Service for executing complex queries for {@link IndAziendaCli} entities in the database.
 * The main input is a {@link IndAziendaCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndAziendaCli} or a {@link Page} of {@link IndAziendaCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndAziendaCliQueryService extends QueryService<IndAziendaCli> {

    private final Logger log = LoggerFactory.getLogger(IndAziendaCliQueryService.class);

    private final IndAziendaCliRepository indAziendaCliRepository;

    public IndAziendaCliQueryService(IndAziendaCliRepository indAziendaCliRepository) {
        this.indAziendaCliRepository = indAziendaCliRepository;
    }

    /**
     * Return a {@link List} of {@link IndAziendaCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziendaCli> findByCriteria(IndAziendaCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndAziendaCli> specification = createSpecification(criteria);
        return indAziendaCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndAziendaCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndAziendaCli> findByCriteria(IndAziendaCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndAziendaCli> specification = createSpecification(criteria);
        return indAziendaCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndAziendaCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndAziendaCli> specification = createSpecification(criteria);
        return indAziendaCliRepository.count(specification);
    }

    /**
     * Function to convert {@link IndAziendaCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndAziendaCli> createSpecification(IndAziendaCliCriteria criteria) {
        Specification<IndAziendaCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndAziendaCli_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndAziendaCli_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndAziendaCli_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndAziendaCli_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndAziendaCli_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndAziendaCli_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndAziendaCli_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndAziendaCli_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndAziendaCli_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndAziendaCli_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndAziendaCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndAziendaCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndAziendaCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndAziendaCli_.userIdLastMod));
            }
            if (criteria.getIndToCliId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToCliId(),
                    root -> root.join(IndAziendaCli_.indToCli, JoinType.LEFT).get(Cliente_.id)));
            }
        }
        return specification;
    }
}
