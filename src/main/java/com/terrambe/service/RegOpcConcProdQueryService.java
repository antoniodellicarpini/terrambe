package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcConcProd;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcConcProdRepository;
import com.terrambe.service.dto.RegOpcConcProdCriteria;

/**
 * Service for executing complex queries for {@link RegOpcConcProd} entities in the database.
 * The main input is a {@link RegOpcConcProdCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcConcProd} or a {@link Page} of {@link RegOpcConcProd} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcConcProdQueryService extends QueryService<RegOpcConcProd> {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcProdQueryService.class);

    private final RegOpcConcProdRepository regOpcConcProdRepository;

    public RegOpcConcProdQueryService(RegOpcConcProdRepository regOpcConcProdRepository) {
        this.regOpcConcProdRepository = regOpcConcProdRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcConcProd} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConcProd> findByCriteria(RegOpcConcProdCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcConcProd> specification = createSpecification(criteria);
        return regOpcConcProdRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcConcProd} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcConcProd> findByCriteria(RegOpcConcProdCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcConcProd> specification = createSpecification(criteria);
        return regOpcConcProdRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcConcProdCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcConcProd> specification = createSpecification(criteria);
        return regOpcConcProdRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcConcProdCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcConcProd> createSpecification(RegOpcConcProdCriteria criteria) {
        Specification<RegOpcConcProd> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcConcProd_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcConcProd_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcConcProd_.idUnitaProd));
            }
            if (criteria.getIdConcime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdConcime(), RegOpcConcProd_.idConcime));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegOpcConcProd_.quantita));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcConcProd_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcConcProd_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcConcProd_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcConcProd_.userIdLastMod));
            }
            if (criteria.getProdToOpcConcId() != null) {
                specification = specification.and(buildSpecification(criteria.getProdToOpcConcId(),
                    root -> root.join(RegOpcConcProd_.prodToOpcConc, JoinType.LEFT).get(RegOpcConc_.id)));
            }
        }
        return specification;
    }
}
