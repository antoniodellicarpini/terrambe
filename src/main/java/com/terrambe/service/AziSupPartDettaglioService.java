package com.terrambe.service;

import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.repository.AziSupPartDettaglioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziSupPartDettaglio}.
 */
@Service
@Transactional
public class AziSupPartDettaglioService {

    private final Logger log = LoggerFactory.getLogger(AziSupPartDettaglioService.class);

    private final AziSupPartDettaglioRepository aziSupPartDettaglioRepository;

    public AziSupPartDettaglioService(AziSupPartDettaglioRepository aziSupPartDettaglioRepository) {
        this.aziSupPartDettaglioRepository = aziSupPartDettaglioRepository;
    }

    /**
     * Save a aziSupPartDettaglio.
     *
     * @param aziSupPartDettaglio the entity to save.
     * @return the persisted entity.
     */
    public AziSupPartDettaglio save(AziSupPartDettaglio aziSupPartDettaglio) {
        log.debug("Request to save AziSupPartDettaglio : {}", aziSupPartDettaglio);
        return aziSupPartDettaglioRepository.save(aziSupPartDettaglio);
    }

    /**
     * Get all the aziSupPartDettaglios.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziSupPartDettaglio> findAll() {
        log.debug("Request to get all AziSupPartDettaglios");
        return aziSupPartDettaglioRepository.findAll();
    }


    /**
     * Get one aziSupPartDettaglio by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziSupPartDettaglio> findOne(Long id) {
        log.debug("Request to get AziSupPartDettaglio : {}", id);
        return aziSupPartDettaglioRepository.findById(id);
    }

    /**
     * Delete the aziSupPartDettaglio by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziSupPartDettaglio : {}", id);
        aziSupPartDettaglioRepository.deleteById(id);
    }
}
