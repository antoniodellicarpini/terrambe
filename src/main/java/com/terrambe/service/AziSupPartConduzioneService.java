package com.terrambe.service;

import com.terrambe.domain.AziSupPartConduzione;
import com.terrambe.repository.AziSupPartConduzioneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziSupPartConduzione}.
 */
@Service
@Transactional
public class AziSupPartConduzioneService {

    private final Logger log = LoggerFactory.getLogger(AziSupPartConduzioneService.class);

    private final AziSupPartConduzioneRepository aziSupPartConduzioneRepository;

    public AziSupPartConduzioneService(AziSupPartConduzioneRepository aziSupPartConduzioneRepository) {
        this.aziSupPartConduzioneRepository = aziSupPartConduzioneRepository;
    }

    /**
     * Save a aziSupPartConduzione.
     *
     * @param aziSupPartConduzione the entity to save.
     * @return the persisted entity.
     */
    public AziSupPartConduzione save(AziSupPartConduzione aziSupPartConduzione) {
        log.debug("Request to save AziSupPartConduzione : {}", aziSupPartConduzione);
        return aziSupPartConduzioneRepository.save(aziSupPartConduzione);
    }

    /**
     * Get all the aziSupPartConduziones.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziSupPartConduzione> findAll() {
        log.debug("Request to get all AziSupPartConduziones");
        return aziSupPartConduzioneRepository.findAll();
    }


    /**
     * Get one aziSupPartConduzione by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziSupPartConduzione> findOne(Long id) {
        log.debug("Request to get AziSupPartConduzione : {}", id);
        return aziSupPartConduzioneRepository.findById(id);
    }

    /**
     * Delete the aziSupPartConduzione by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziSupPartConduzione : {}", id);
        aziSupPartConduzioneRepository.deleteById(id);
    }
}
