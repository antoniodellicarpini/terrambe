package com.terrambe.service;

import com.terrambe.domain.RegOpcPioAppz;
import com.terrambe.repository.RegOpcPioAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcPioAppz}.
 */
@Service
@Transactional
public class RegOpcPioAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioAppzService.class);

    private final RegOpcPioAppzRepository regOpcPioAppzRepository;

    public RegOpcPioAppzService(RegOpcPioAppzRepository regOpcPioAppzRepository) {
        this.regOpcPioAppzRepository = regOpcPioAppzRepository;
    }

    /**
     * Save a regOpcPioAppz.
     *
     * @param regOpcPioAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcPioAppz save(RegOpcPioAppz regOpcPioAppz) {
        log.debug("Request to save RegOpcPioAppz : {}", regOpcPioAppz);
        return regOpcPioAppzRepository.save(regOpcPioAppz);
    }

    /**
     * Get all the regOpcPioAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPioAppz> findAll() {
        log.debug("Request to get all RegOpcPioAppzs");
        return regOpcPioAppzRepository.findAll();
    }


    /**
     * Get one regOpcPioAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcPioAppz> findOne(Long id) {
        log.debug("Request to get RegOpcPioAppz : {}", id);
        return regOpcPioAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcPioAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcPioAppz : {}", id);
        regOpcPioAppzRepository.deleteById(id);
    }
}
