package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcTrap;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcTrapRepository;
import com.terrambe.service.dto.RegOpcTrapCriteria;

/**
 * Service for executing complex queries for {@link RegOpcTrap} entities in the database.
 * The main input is a {@link RegOpcTrapCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcTrap} or a {@link Page} of {@link RegOpcTrap} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcTrapQueryService extends QueryService<RegOpcTrap> {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapQueryService.class);

    private final RegOpcTrapRepository regOpcTrapRepository;

    public RegOpcTrapQueryService(RegOpcTrapRepository regOpcTrapRepository) {
        this.regOpcTrapRepository = regOpcTrapRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcTrap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrap> findByCriteria(RegOpcTrapCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcTrap> specification = createSpecification(criteria);
        return regOpcTrapRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcTrap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcTrap> findByCriteria(RegOpcTrapCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcTrap> specification = createSpecification(criteria);
        return regOpcTrapRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcTrapCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcTrap> specification = createSpecification(criteria);
        return regOpcTrapRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcTrapCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcTrap> createSpecification(RegOpcTrapCriteria criteria) {
        Specification<RegOpcTrap> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcTrap_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcTrap_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcTrap_.idUnitaProd));
            }
            if (criteria.getDataOperazione() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataOperazione(), RegOpcTrap_.dataOperazione));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcTrap_.tempoImpiegato));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcTrap_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcTrap_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcTrap_.idMezzo));
            }
            if (criteria.getIdTrappola() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdTrappola(), RegOpcTrap_.idTrappola));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcTrap_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcTrap_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcTrap_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcTrap_.userIdLastMod));
            }
            if (criteria.getOpcTrapToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcTrapToAppzId(),
                    root -> root.join(RegOpcTrap_.opcTrapToAppzs, JoinType.LEFT).get(RegOpcTrapAppz_.id)));
            }
            if (criteria.getOpcTrapToTrapContrId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcTrapToTrapContrId(),
                    root -> root.join(RegOpcTrap_.opcTrapToTrapContrs, JoinType.LEFT).get(RegOpcTrapContr_.id)));
            }
        }
        return specification;
    }
}
