package com.terrambe.service;

import com.terrambe.domain.EtichettaPittogrammi;
import com.terrambe.repository.EtichettaPittogrammiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EtichettaPittogrammi}.
 */
@Service
@Transactional
public class EtichettaPittogrammiService {

    private final Logger log = LoggerFactory.getLogger(EtichettaPittogrammiService.class);

    private final EtichettaPittogrammiRepository etichettaPittogrammiRepository;

    public EtichettaPittogrammiService(EtichettaPittogrammiRepository etichettaPittogrammiRepository) {
        this.etichettaPittogrammiRepository = etichettaPittogrammiRepository;
    }

    /**
     * Save a etichettaPittogrammi.
     *
     * @param etichettaPittogrammi the entity to save.
     * @return the persisted entity.
     */
    public EtichettaPittogrammi save(EtichettaPittogrammi etichettaPittogrammi) {
        log.debug("Request to save EtichettaPittogrammi : {}", etichettaPittogrammi);
        return etichettaPittogrammiRepository.save(etichettaPittogrammi);
    }

    /**
     * Get all the etichettaPittogrammis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaPittogrammi> findAll() {
        log.debug("Request to get all EtichettaPittogrammis");
        return etichettaPittogrammiRepository.findAll();
    }


    /**
     * Get one etichettaPittogrammi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EtichettaPittogrammi> findOne(Long id) {
        log.debug("Request to get EtichettaPittogrammi : {}", id);
        return etichettaPittogrammiRepository.findById(id);
    }

    /**
     * Delete the etichettaPittogrammi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EtichettaPittogrammi : {}", id);
        etichettaPittogrammiRepository.deleteById(id);
    }
}
