package com.terrambe.service;

import com.terrambe.domain.RegOpcAltro;
import com.terrambe.repository.RegOpcAltroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcAltro}.
 */
@Service
@Transactional
public class RegOpcAltroService {

    private final Logger log = LoggerFactory.getLogger(RegOpcAltroService.class);

    private final RegOpcAltroRepository regOpcAltroRepository;

    public RegOpcAltroService(RegOpcAltroRepository regOpcAltroRepository) {
        this.regOpcAltroRepository = regOpcAltroRepository;
    }

    /**
     * Save a regOpcAltro.
     *
     * @param regOpcAltro the entity to save.
     * @return the persisted entity.
     */
    public RegOpcAltro save(RegOpcAltro regOpcAltro) {
        log.debug("Request to save RegOpcAltro : {}", regOpcAltro);
        return regOpcAltroRepository.save(regOpcAltro);
    }

    /**
     * Get all the regOpcAltros.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcAltro> findAll() {
        log.debug("Request to get all RegOpcAltros");
        return regOpcAltroRepository.findAll();
    }


    /**
     * Get one regOpcAltro by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcAltro> findOne(Long id) {
        log.debug("Request to get RegOpcAltro : {}", id);
        return regOpcAltroRepository.findById(id);
    }

    /**
     * Delete the regOpcAltro by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcAltro : {}", id);
        regOpcAltroRepository.deleteById(id);
    }
}
