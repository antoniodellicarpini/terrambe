package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcSeminaAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcSeminaAppzRepository;
import com.terrambe.service.dto.RegOpcSeminaAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcSeminaAppz} entities in the database.
 * The main input is a {@link RegOpcSeminaAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcSeminaAppz} or a {@link Page} of {@link RegOpcSeminaAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcSeminaAppzQueryService extends QueryService<RegOpcSeminaAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaAppzQueryService.class);

    private final RegOpcSeminaAppzRepository regOpcSeminaAppzRepository;

    public RegOpcSeminaAppzQueryService(RegOpcSeminaAppzRepository regOpcSeminaAppzRepository) {
        this.regOpcSeminaAppzRepository = regOpcSeminaAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcSeminaAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcSeminaAppz> findByCriteria(RegOpcSeminaAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcSeminaAppz> specification = createSpecification(criteria);
        return regOpcSeminaAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcSeminaAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcSeminaAppz> findByCriteria(RegOpcSeminaAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcSeminaAppz> specification = createSpecification(criteria);
        return regOpcSeminaAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcSeminaAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcSeminaAppz> specification = createSpecification(criteria);
        return regOpcSeminaAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcSeminaAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcSeminaAppz> createSpecification(RegOpcSeminaAppzCriteria criteria) {
        Specification<RegOpcSeminaAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcSeminaAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcSeminaAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcSeminaAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcSeminaAppz_.percentLavorata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcSeminaAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcSeminaAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcSeminaAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcSeminaAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcSeminaAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcSeminaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcSeminaId(),
                    root -> root.join(RegOpcSeminaAppz_.appzToOpcSemina, JoinType.LEFT).get(RegOpcSemina_.id)));
            }
        }
        return specification;
    }
}
