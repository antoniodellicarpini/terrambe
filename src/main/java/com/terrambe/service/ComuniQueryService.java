package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Comuni;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ComuniRepository;
import com.terrambe.service.dto.ComuniCriteria;

/**
 * Service for executing complex queries for {@link Comuni} entities in the database.
 * The main input is a {@link ComuniCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Comuni} or a {@link Page} of {@link Comuni} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ComuniQueryService extends QueryService<Comuni> {

    private final Logger log = LoggerFactory.getLogger(ComuniQueryService.class);

    private final ComuniRepository comuniRepository;

    public ComuniQueryService(ComuniRepository comuniRepository) {
        this.comuniRepository = comuniRepository;
    }

    /**
     * Return a {@link List} of {@link Comuni} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Comuni> findByCriteria(ComuniCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Comuni> specification = createSpecification(criteria);
        return comuniRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Comuni} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Comuni> findByCriteria(ComuniCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Comuni> specification = createSpecification(criteria);
        return comuniRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ComuniCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Comuni> specification = createSpecification(criteria);
        return comuniRepository.count(specification);
    }

    /**
     * Function to convert {@link ComuniCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Comuni> createSpecification(ComuniCriteria criteria) {
        Specification<Comuni> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Comuni_.id));
            }
            if (criteria.getCodiProv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiProv(), Comuni_.codiProv));
            }
            if (criteria.getCodiComu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiComu(), Comuni_.codiComu));
            }
            if (criteria.getDescComu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescComu(), Comuni_.descComu));
            }
            if (criteria.getDescProv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescProv(), Comuni_.descProv));
            }
            if (criteria.getCodiSiglProv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiSiglProv(), Comuni_.codiSiglProv));
            }
            if (criteria.getCodiFiscLuna() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiFiscLuna(), Comuni_.codiFiscLuna));
            }
            if (criteria.getDescRegi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescRegi(), Comuni_.descRegi));
            }
            if (criteria.getCodiceBelfiore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceBelfiore(), Comuni_.codiceBelfiore));
            }
            if (criteria.getDataInizio() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizio(), Comuni_.dataInizio));
            }
            if (criteria.getDataFine() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFine(), Comuni_.dataFine));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Comuni_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Comuni_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Comuni_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Comuni_.userIdLastMod));
            }
        }
        return specification;
    }
}
