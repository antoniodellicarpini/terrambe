package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Formulazione;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FormulazioneRepository;
import com.terrambe.service.dto.FormulazioneCriteria;

/**
 * Service for executing complex queries for {@link Formulazione} entities in the database.
 * The main input is a {@link FormulazioneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Formulazione} or a {@link Page} of {@link Formulazione} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FormulazioneQueryService extends QueryService<Formulazione> {

    private final Logger log = LoggerFactory.getLogger(FormulazioneQueryService.class);

    private final FormulazioneRepository formulazioneRepository;

    public FormulazioneQueryService(FormulazioneRepository formulazioneRepository) {
        this.formulazioneRepository = formulazioneRepository;
    }

    /**
     * Return a {@link List} of {@link Formulazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Formulazione> findByCriteria(FormulazioneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Formulazione> specification = createSpecification(criteria);
        return formulazioneRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Formulazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Formulazione> findByCriteria(FormulazioneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Formulazione> specification = createSpecification(criteria);
        return formulazioneRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FormulazioneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Formulazione> specification = createSpecification(criteria);
        return formulazioneRepository.count(specification);
    }

    /**
     * Function to convert {@link FormulazioneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Formulazione> createSpecification(FormulazioneCriteria criteria) {
        Specification<Formulazione> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Formulazione_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), Formulazione_.descrizione));
            }
            if (criteria.getSiglaFormulazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSiglaFormulazione(), Formulazione_.siglaFormulazione));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Formulazione_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Formulazione_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Formulazione_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Formulazione_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Formulazione_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Formulazione_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Formulazione_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(Formulazione_.etichettaFitos, JoinType.LEFT).get(EtichettaFito_.id)));
            }
        }
        return specification;
    }
}
