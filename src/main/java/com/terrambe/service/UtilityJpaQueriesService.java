package com.terrambe.service;

import com.terrambe.domain.*;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author egiovanni
 * @version 1.0
 * @since 2019-12-13 Questa classe di servizio contiene tutti i metodi che
 *        permettono la manipolazione attraverso query parametrizzate ed in cui
 *        Ã¨ necessario referenziare Entity generiche
 */

@Service
@Transactional
public class UtilityJpaQueriesService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private EntityManager em;

    @Autowired
    private Agea20152020MatriceService ageaMatService;

    @Autowired
    private DataSource dataSource;

    /**
     * Aggiunge l'utente corrente al campo user_id_creator di qualsiasi entity
     *
     * @param nomeEntity il nome della entity (classe del domain che mappa la
     *                   tabella del db).
     * @param id         l'id del record da modificare in formato stringa.
     * @return n con n = numero di entity aggiornate.
     */
    public int addCurrentUserToEntityCreate(String nomeEntity, String id) {
        // Ottengo l'utente attualmente loggato
        final Optional<User> isUser = userService.getUserWithAuthorities();
        if (!isUser.isPresent()) {
            Logger log = LoggerFactory.getLogger("L'utente non Ã¨ loggato");
        }
        final User user = isUser.get();
        // eseguo la query con i parametri ricevuti e lo user corrente
        Query q1 = em.createQuery(
            String.format("update %s set user_id_creator = '%s', user_id_last_mod = '&s' where id = %s", nomeEntity,
                user.getId().toString(), user.getId().toString(), id));
        return q1.executeUpdate();
    }

    /**
     * Aggiunge l'utente corrente al campo user_last_mod di qualsiasi entity
     *
     * @param nomeEntity il nome della entity (classe del domain che mappa la
     *                   tabella del db).
     * @param id         l'id del record da modificare in formato stringa.
     * @return n con n = numero di entity aggiornate.
     */
    public int addCurrentUserToEntityMod(String nomeEntity, String id) {
        // Ottengo l'utente attualmente loggato
        final Optional<User> isUser = userService.getUserWithAuthorities();
        if (!isUser.isPresent()) {
            Logger log = LoggerFactory.getLogger("L'utente non Ã¨ loggato");
        }
        final User user = isUser.get();
        // eseguo la query con i parametri ricevuti e lo user corrente
        Query q1 = em.createQuery(
            String.format("update %s set user_id_last_mod = %s where id = %s", nomeEntity, user.getId(), id));
        return q1.executeUpdate();
    }

    /**
     * Cambia la data di fine validitÃ del record di qualsiasi entity con la data
     * corrente
     *
     * @param nomeEntity il nome della entity (classe del domain che mappa la
     *                   tabella del db).
     * @param id         l'id del record da modificare in formato stringa.
     * @return n con n = numero di entity aggiornate.
     */

    public int addClosingDateToEntity(String nomeEntity, String id) {
        // Ottengo la data corrente
        LocalDateTime now = LocalDateTime.now();
        // eseguo la query con i parametri ricevuti e la data corrente
        // cambiare data_modifica con data_fine_vali
        Query q1 = em
            .createQuery(String.format("update %s set data_fine_vali = '%s' where id = %s", nomeEntity, now, id));
        return q1.executeUpdate();
    }

    /**
     * Cambia la data di fine validitÃ del record di qualsiasi entity con la data
     * corrente
     *
     * @return L'id dell'utente attualmente loggato.
     */

    public Long getUser() {
        // Ottengo l'utente attualmente loggato
        final Optional<User> isUser = userService.getUserWithAuthorities();
        if (!isUser.isPresent()) {
            Logger log = LoggerFactory.getLogger("L'utente non Ã¨ loggato");
        }
        final User user = isUser.get();
        return user.getId();
    }

    public boolean isADeadRecord(String nomeEntity, String id) {

        boolean result = false;

        // ottengo data_fine_vali del record dell'entity passata.
        Query q1 = em.createQuery(String.format("select dataFineVali from %s where id = %s", nomeEntity, id));
        LocalDate data_fine_vali = (LocalDate) q1.getSingleResult();
        // controllo se la data Ã¨ antecendente al 31/12/9999 oppure vuota
        if (data_fine_vali.isBefore(LocalDate.parse("9999-12-31")) || data_fine_vali == null) {
            result = true;
        }
        return result;
    }

    public RegOpcSpec riempiRegOpcFito(RegOpcFito reg, RegOpcSpec spec) {

        final String ENTITY_NAME = "agea20152020matrice";
        // ottengo la descrizione della coltura

        Query q1 = em.createQuery(
            String.format("select desc_coltura from %s where id = %s", ENTITY_NAME, reg.getAgea20152020Matrice().getId()));
        spec.setDescColtura(q1.getSingleResult().toString());

        // ottengo il nominativo dalla

        Query q2 = em.createQuery(String.format("select nome from ope_anagrafica where id = %s", reg.getOpeAnagrafica().getId()));
        Query q3 = em
            .createQuery(String.format("select cognome from ope_anagrafica where id = %s", reg.getOpeAnagrafica().getId()));
        String nome = q2.getSingleResult().toString();
        String cognome = q3.getSingleResult().toString();
        String nominativo = nome + ' ' + cognome;
        spec.setNominativo(nominativo);

        Query q4 = em.createQuery(String.format("select modello from atr_anagrafica where id = %s", reg.getAtrAnagrafica().getId()));

        spec.setDescMezzo(q4.getSingleResult().toString());

        return spec;
    }

    public List<EtichettaFitoFilter> getFitoFromAvversitaAndColtura(Long coltura_id, Long avversita_id,DataSource dataSource) {

        List<EtichettaFitoFilter> array_etichettafitofilter = new ArrayList<>();
        ResultSet rs = null;

        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){

            // conn = dataSource.getConnection();
            // st = conn.createStatement();
            rs = st.executeQuery(String.format(
                "SELECT DISTINCT etichetta_fito.id,etichetta_fito.nome_commerciale  FROM public.agea_20152020_matrice LEFT JOIN  agea_20152020_bdf ON agea_20152020_matrice.id =  agea_20152020_bdf.bdf_tomatrice_id LEFT JOIN colture_bdf ON colture_bdf.id = agea_20152020_bdf.agea_bdf_to_colt_bdf_id LEFT JOIN tabella_raccordo ON tabella_raccordo.raccordo_to_colt_bdf_id = colture_bdf.id LEFT JOIN etichetta_fito ON tabella_raccordo.raccordo_to_eti_fito_id = etichetta_fito.id where raccordo_to_avvers_id= "
                    + avversita_id + " AND agea_20152020_matrice.id = " + coltura_id));

            while (rs.next()) {
                EtichettaFitoFilter etichettafito = new EtichettaFitoFilter();

                etichettafito.setId(Long.valueOf(rs.getLong("id")));
                etichettafito.setNomeCommerciale(rs.getString("nome_commerciale"));

                array_etichettafitofilter.add(etichettafito);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }

        List<EtichettaFitoFilter> return_eti = array_etichettafitofilter;

        return return_eti;
    }


    public OpeAnagrafica getInfoOperatoreById(Long operatore_id, DataSource dataSource){
        ResultSet rs = null;
        OpeAnagrafica opeAnagrafica = new OpeAnagrafica();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM ope_anagrafica where id = " + operatore_id));
            if(rs.next()) {
                opeAnagrafica.setId(Long.valueOf(rs.getLong("id")));
                opeAnagrafica.setNome(rs.getString("nome"));
                opeAnagrafica.setCognome(rs.getString("cognome"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return opeAnagrafica;
    }

    public TipoIrr getInfoTipoIrrigazioneById(Long tipo_id, DataSource dataSource){
        ResultSet rs = null;
        TipoIrr tipoIrr = new TipoIrr();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM tipo_irr where id = " + tipo_id));
            if(rs.next()) {
                tipoIrr.setId(Long.valueOf(rs.getLong("id")));
                tipoIrr.setDescrizione(rs.getString("descrizione"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return tipoIrr;
    }

    public TipoSemina getInfoTipoSeminaById(Long tipo_id, DataSource dataSource){
        ResultSet rs = null;
        TipoSemina tipoSemina = new TipoSemina();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM tipo_semina where id = " + tipo_id));
            if(rs.next()) {
                tipoSemina.setId(Long.valueOf(rs.getLong("id")));
                tipoSemina.setDescrizione(rs.getString("descrizione"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return tipoSemina;
    }

    public TipoRacc getInfoRaccoltaById(Long raccolta_id, DataSource dataSource){
        ResultSet rs = null;
        TipoRacc tipoRacc = new TipoRacc();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM tipo_racc where id = " + raccolta_id));
            if(rs.next()) {
                tipoRacc.setId(Long.valueOf(rs.getLong("id")));
                tipoRacc.setDescrizione(rs.getString("descrizione"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return tipoRacc;
    }

    public TipoAltro getInfoAltroById(Long altro_id, DataSource dataSource){
        ResultSet rs = null;
        TipoAltro tipoAltro = new TipoAltro();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM tipo_altro where id = " + altro_id));
            if(rs.next()) {
                tipoAltro.setId(Long.valueOf(rs.getLong("id")));
                tipoAltro.setDescrizione(rs.getString("descrizione"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return tipoAltro;
    }

    public EtichettaFito getInfoFitofarmacoById(Long fitofarmaco_id, DataSource dataSource){
        ResultSet rs = null;
        EtichettaFito etichettaFito = new EtichettaFito();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM etichetta_fito where id = " + fitofarmaco_id));
            if(rs.next()) {
                etichettaFito.setId(Long.valueOf(rs.getLong("id")));
                etichettaFito.setNomeCommerciale(rs.getString("nome_commerciale"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return etichettaFito;
    }

    public Avversita getInfoAvversitaById(Long avversita_id, DataSource dataSource){
        ResultSet rs = null;
        Avversita avversita = new Avversita();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM avversita where id = " + avversita_id));
            if(rs.next()) {
                avversita.setId(Long.valueOf(rs.getLong("id")));
                avversita.setNomeSci(rs.getString("nome_sci"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return avversita;
    }

    public Agea20152020Matrice getInfoColturaById(Long coltura_id, DataSource dataSource){
        ResultSet rs = null;
        Agea20152020Matrice agea_coltura = new Agea20152020Matrice();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM agea_20152020_matrice where id = " + coltura_id));
            if(rs.next()) {
                agea_coltura.setId(Long.valueOf(rs.getLong("id")));
                agea_coltura.setDescColtura(rs.getString("desc_coltura"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return agea_coltura;
    }

    public TipoPrepterr getInfoPrepterrById(Long prepterr_id, DataSource dataSource){
        ResultSet rs = null;
        TipoPrepterr TipoPrepterr = new TipoPrepterr();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM tipo_prepterr where id = " + prepterr_id));
            if(rs.next()) {
                TipoPrepterr.setId(Long.valueOf(rs.getLong("id")));
                TipoPrepterr.setDescrizione(rs.getString("descrizione"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return TipoPrepterr;
    }

    public float getPercAppezzamentoById(Long id_appezzamento, Float percentuale, DataSource dataSource){
        ResultSet rs = null;
        Float result = (float) 0;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT (superfice_ha*"+ percentuale +")/100 AS ha, nome_appezzamento FROM appezzamenti WHERE ID= "+ id_appezzamento));
            if (rs.next()) {
                result = rs.getFloat("id");
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return result;
    }

    public AtrAnagrafica getInfoAtrAnagraficaById(Long mezzo_id, DataSource dataSource){
        ResultSet rs = null;
        AtrAnagrafica AtrAnagrafica = new AtrAnagrafica();
        try(Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()){
            rs = st.executeQuery(String.format("SELECT * FROM atr_anagrafica where id = " + mezzo_id));
            if(rs.next()) {
                AtrAnagrafica.setId(Long.valueOf(rs.getLong("id")));
                AtrAnagrafica.setModello(rs.getString("modello"));
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return AtrAnagrafica;
    }

    //Mod2
    public List<ListRegOpcFito> getListRegOpcFito(Long azienda_id, DataSource dataSource) throws SQLException{

        List<ListRegOpcFito> list_regopcfito = new ArrayList<>();
        ResultSet rs = null;

        System.out.println("--->   SELECT * FROM reg_opc_fito LEFT JOIN reg_opc_fito_prod ON reg_opc_fito_prod.prod_to_opc_fito_id = reg_opc_fito.id WHERE reg_opc_fito.id_azienda = " + azienda_id);

        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_fito LEFT JOIN reg_opc_fito_prod ON reg_opc_fito_prod.prod_to_opc_fito_id = reg_opc_fito.id WHERE reg_opc_fito.id_azienda = " + azienda_id));
            while (rs.next()) {
                ListRegOpcFito item_regopcfito = new ListRegOpcFito();
                // OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                // EtichettaFito EtichettaFito = new EtichettaFito();
                // Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();
                // Avversita Avversita = new Avversita();

                item_regopcfito.setId(Long.valueOf(rs.getLong("id")));
                // item_regopcfito.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());
                // OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                // item_regopcfito.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());
                // EtichettaFito = this.getInfoFitofarmacoById(rs.getLong("id_fitofarmaco"), dataSource);
                // item_regopcfito.setDescProdotto(EtichettaFito.getNomeCommerciale());
                // Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                // item_regopcfito.setDescColtura(Agea20152020Matrice.getDescColtura());
                // Avversita = this.getInfoAvversitaById(rs.getLong("id_avversita"), dataSource);
                // item_regopcfito.setDescAvversita(Avversita.getNomeSci());

                
                // item_regopcfito.setDataOperazione("test operazione");
                item_regopcfito.setDescOperatore("test operatore");
                item_regopcfito.setDescProdotto("Test prodotto");
                item_regopcfito.setDescColtura("desc coltura");
                item_regopcfito.setDescAvversita("avversita");


                list_regopcfito.add(item_regopcfito);
            }
        } catch (SQLException ex) {
            System.out.println("---> ErroreErroreErroreErrore");
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List< ListRegOpcFito> return_list = list_regopcfito;
        return return_list;
    }
//modifica1
    public ListRegOpcFito getRegOpcFito(Long id, DataSource dataSource) throws SQLException{

        ResultSet rs = null;
        ResultSet rs2 = null;

        ListRegOpcFito single_regopcfito = new ListRegOpcFito();

        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_fito LEFT JOIN reg_opc_fito_prod ON reg_opc_fito_prod.prod_to_opc_fito_id = reg_opc_fito.id WHERE reg_opc_fito.id = " + id));
            Avversita Avversita = new Avversita();

            if (rs.next()) {

                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                EtichettaFito EtichettaFito = new EtichettaFito();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();

                single_regopcfito.setId(Long.valueOf(rs.getLong("id")));
                single_regopcfito.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                single_regopcfito.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                EtichettaFito = this.getInfoFitofarmacoById(rs.getLong("id_fitofarmaco"), dataSource);
                single_regopcfito.setDescProdotto(EtichettaFito.getNomeCommerciale());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                single_regopcfito.setDescColtura(Agea20152020Matrice.getDescColtura());

                Avversita = this.getInfoAvversitaById(rs.getLong("id_avversita"), dataSource);
                single_regopcfito.setDescAvversita(Avversita.getNomeSci());

                single_regopcfito.setDataInizioOperazione(rs.getDate("data_iniz_opc").toLocalDate());
                single_regopcfito.setDataFineOperazione(rs.getDate("data_fine_opc").toLocalDate());
                single_regopcfito.setTempoImpiegato(rs.getString("tempo_impiegato"));
                single_regopcfito.setVolumeAcqua(rs.getFloat("volume_acqua"));

                try (Statement st2 = conn.createStatement()) {
                    rs2 = st2.executeQuery(String.format("SELECT descrizione FROM motivo_tratt_fito WHERE id = " +rs.getString("tratt_fito_to_motivo_id")));
                    single_regopcfito.setMotiTrattamenti(rs2.getString("descrizione"));

                } catch (SQLException ex) {
                    // log.debug("Errore getFitoFromAvversitaAndColtura");
                }

                single_regopcfito.setDescMezzo(this.getInfoAtrAnagraficaById(rs.getLong("id_mezzo"), dataSource).getModello());

            }

        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        return single_regopcfito;
    }

    public List<ListRegOpcPrepterreno> getListRegOpcPrepterreno(Long azienda_id, DataSource dataSource) throws SQLException {
        List<ListRegOpcPrepterreno> list_regopcprepterr = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_prepterr WHERE id_azienda = "+ azienda_id));
            while (rs.next()) {
                ListRegOpcPrepterreno item_regopc = new ListRegOpcPrepterreno();
                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                TipoPrepterr TipoPrepterr = new TipoPrepterr();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();
                AtrAnagrafica AtrAnagrafica = new AtrAnagrafica();

                item_regopc.setId(Long.valueOf(rs.getLong("id")));
                item_regopc.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                item_regopc.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                TipoPrepterr = this.getInfoPrepterrById(rs.getLong("opc_prepterr_to_tipo_id"), dataSource);
                item_regopc.setDescPrepterr(TipoPrepterr.getDescrizione());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                item_regopc.setDescColtura(Agea20152020Matrice.getDescColtura());

                AtrAnagrafica = this.getInfoAtrAnagraficaById(rs.getLong("id_mezzo"), dataSource);
                item_regopc.setDescMezzo(AtrAnagrafica.getModello());

                list_regopcprepterr.add(item_regopc);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List<ListRegOpcPrepterreno> return_list = list_regopcprepterr;
        return return_list;
    }

    public List<ListRegOpcIrr> getListRegOpcIrrigazione(Long azienda_id, DataSource dataSource) throws SQLException {
        List<ListRegOpcIrr> list_regopcirr = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_irr WHERE id_azienda = "+ azienda_id));
            while (rs.next()) {
                ListRegOpcIrr item_regopc = new ListRegOpcIrr();
                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                TipoIrr TipoIrr = new TipoIrr();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();

                item_regopc.setId(Long.valueOf(rs.getLong("id")));
                item_regopc.setDataOperazione(rs.getDate("data_operazione").toLocalDate());

                item_regopc.setVolumeacqua(rs.getFloat("volume_acqua"));

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                item_regopc.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                TipoIrr = this.getInfoTipoIrrigazioneById(rs.getLong("opc_irr_to_tipo_irr_id"), dataSource);
                item_regopc.setDescTipo(TipoIrr.getDescrizione());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                item_regopc.setDescColtura(Agea20152020Matrice.getDescColtura());

                list_regopcirr.add(item_regopc);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List<ListRegOpcIrr> return_list = list_regopcirr;
        return return_list;
    }

    public List<ListRegOpcSemina> getListRegOpcSemina(Long azienda_id, DataSource dataSource) throws SQLException {
        List<ListRegOpcSemina> list_regopcsemina = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_semina WHERE id_azienda = "+ azienda_id));
            while (rs.next()) {
                ListRegOpcSemina item_regopcsemina = new ListRegOpcSemina();
                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                TipoSemina TipoSemina = new TipoSemina();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();
                AtrAnagrafica AtrAnagrafica = new AtrAnagrafica();

                item_regopcsemina.setId(Long.valueOf(rs.getLong("id")));
                item_regopcsemina.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                item_regopcsemina.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                TipoSemina = this.getInfoTipoSeminaById(rs.getLong("opc_semina_to_tipo_id"), dataSource);
                item_regopcsemina.setDescTipo(TipoSemina.getDescrizione());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                item_regopcsemina.setDescColtura(Agea20152020Matrice.getDescColtura());

                AtrAnagrafica = this.getInfoAtrAnagraficaById(rs.getLong("id_mezzo"), dataSource);
                item_regopcsemina.setDescMezzo(AtrAnagrafica.getModello());

                list_regopcsemina.add(item_regopcsemina);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List<ListRegOpcSemina> return_list = list_regopcsemina;
        return return_list;
    }

    public List<ListRegOpcRaccolta> getListRegOpcRaccolta(Long azienda_id, DataSource dataSource) throws SQLException {
        List<ListRegOpcRaccolta> list_regopcraccolta = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_racc WHERE id_azienda = "+ azienda_id));
            while (rs.next()) {
                ListRegOpcRaccolta item_regopcraccolta = new ListRegOpcRaccolta();
                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                TipoRacc TipoRacc = new TipoRacc();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();
                AtrAnagrafica AtrAnagrafica = new AtrAnagrafica();

                item_regopcraccolta.setId(Long.valueOf(rs.getLong("id")));
                item_regopcraccolta.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                item_regopcraccolta.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                TipoRacc = this.getInfoRaccoltaById(rs.getLong("opc_racc_to_tipo_id"), dataSource);
                item_regopcraccolta.setDescTipo(TipoRacc.getDescrizione());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                item_regopcraccolta.setDescColtura(Agea20152020Matrice.getDescColtura());

                AtrAnagrafica = this.getInfoAtrAnagraficaById(rs.getLong("id_mezzo"), dataSource);
                item_regopcraccolta.setDescMezzo(AtrAnagrafica.getModello());

                item_regopcraccolta.setQuantita(rs.getFloat("quantita"));

                list_regopcraccolta.add(item_regopcraccolta);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List<ListRegOpcRaccolta> return_list = list_regopcraccolta;
        return return_list;
    }

    public List<ListRegOpcAltro> getListRegOpcAltro(Long azienda_id, DataSource dataSource) throws SQLException {
        List<ListRegOpcAltro> list_regopcaltro = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            rs = st.executeQuery(String.format("SELECT * FROM reg_opc_altro WHERE id_azienda = "+ azienda_id));
            while (rs.next()) {
                ListRegOpcAltro item_regopcaltro = new ListRegOpcAltro();
                OpeAnagrafica OpeAnagrafica = new OpeAnagrafica();
                TipoAltro TipoAltro = new TipoAltro();
                Agea20152020Matrice Agea20152020Matrice = new Agea20152020Matrice();
                AtrAnagrafica AtrAnagrafica = new AtrAnagrafica();

                item_regopcaltro.setId(Long.valueOf(rs.getLong("id")));
                item_regopcaltro.setDataOperazione(rs.getDate("data_iniz_opc").toLocalDate());

                OpeAnagrafica = this.getInfoOperatoreById(rs.getLong("id_operatore"), dataSource);
                item_regopcaltro.setDescOperatore(OpeAnagrafica.getNome() + " " + OpeAnagrafica.getCognome());

                TipoAltro = this.getInfoAltroById(rs.getLong("opc_altro_to_tipo_id"), dataSource);
                item_regopcaltro.setDescTipo(TipoAltro.getDescrizione());

                Agea20152020Matrice = this.getInfoColturaById(rs.getLong("id_coltura"), dataSource);
                item_regopcaltro.setDescColtura(Agea20152020Matrice.getDescColtura());

                AtrAnagrafica = this.getInfoAtrAnagraficaById(rs.getLong("id_mezzo"), dataSource);
                item_regopcaltro.setDescMezzo(AtrAnagrafica.getModello());


                list_regopcaltro.add(item_regopcaltro);
            }
            conn.close();
        } catch (SQLException ex) {
            // log.debug("Errore getFitoFromAvversitaAndColtura");
        }
        List<ListRegOpcAltro> return_list = list_regopcaltro;
        return return_list;
    }

    public List<ErrorTerram> controlliFito(RegOpcFito regOpcFito, DataSource dataSource){
        List<ErrorTerram> list_errorTerram = new ArrayList<>();
        for (ErrorTerram errorTerram : checkRegOpcFito(regOpcFito, dataSource)) {
            list_errorTerram.add(errorTerram);
        }
        for (ErrorTerram errorTerram : controllovolumesuperficie(regOpcFito, dataSource)) {
            list_errorTerram.add(errorTerram);
        }
        return list_errorTerram;
    }

    public List<ErrorTerram> controllovolumesuperficie(RegOpcFito regOpcFito, DataSource dataSource){

        // item_superfice = getPercAppezzamentoById(Long id_appezzamento, Float percentuale, DataSource dataSource);

        List<ErrorTerram> list_errorTerram = new ArrayList<>();

        Long item_id_appezzamento = null;
        Float item_percentuale = null;
        Long item_id_fito = null;
        Float item_volume_acqua = null;
        Float item_superfice = null;

        String return_ = "";

        item_volume_acqua = regOpcFito.getVolumeAcqua().floatValue();

        for (RegOpcFitoAppz item_regOpcFitoAppz : regOpcFito.getOpcFitoToAppzs()) {
            item_id_appezzamento = item_regOpcFitoAppz.getIdAppezzamento();

            for (RegOpcFitoProd item_regOpcFitoProd : regOpcFito.getOpcFitoToProds()) {
                item_id_fito = item_regOpcFitoProd.getIdFitofarmaco();

                try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {

                    item_superfice = getPercAppezzamentoById(item_id_appezzamento, item_percentuale, dataSource);
                    if(item_superfice==0) item_superfice= (float) 1;
                    
                    System.out.println("controllovolumesuperficie("+ item_id_fito +","+ Math.round(item_volume_acqua) + "," + Math.round(item_superfice) +")");
                    CallableStatement cstmt = conn.prepareCall("{CALL controllovolumesuperficie(?,?,?,?)}");
                    cstmt.setInt(1, item_id_fito.intValue());
                    cstmt.setInt(2, Math.round(item_volume_acqua));
                    cstmt.setInt(3, Math.round(item_superfice));
                    cstmt.registerOutParameter(4, Types.VARCHAR);
                    cstmt.executeUpdate();
                    return_ = cstmt.getString(4);
                    conn.close();
                    String[] array_return_ = return_.split(";");

                    if (!return_.isEmpty() && !return_.equals("OK")) {
                        String str[] = return_.split(";");
                        List<String> al = new ArrayList<String>();
                        al = Arrays.asList(str);
                        for (String s : al) {
                            list_errorTerram.add(new ErrorTerram(null, null, s.trim()));
                        }
                    }

                } catch (SQLException ex) {
                    // log.debug("Errore procedura ");
                    System.out.println("Errore procedura controllovolumesuperfice");
                }

            }
        }




        return list_errorTerram;
    }


    public List<ErrorTerram> checkRegOpcFito(RegOpcFito regOpcFito, DataSource dataSource){
        List<ErrorTerram> list_errorTerram = new ArrayList<>();

        Long item_id_appezzamento = null;
        Long item_id_coltura = null;
        Long item_id_operatore = null;
        Long item_id_avversita = null;
        Long item_id_fito = null;
        Float item_qta= null;
        Long item_id_magazzino= null;
        Date item_datain = null;
        Date item_datafine = null;
        Long item_id_mezzo = null;

        String return_ = "";

        for (RegOpcFitoAppz item_regOpcFitoAppz : regOpcFito.getOpcFitoToAppzs()) {
            item_id_appezzamento = item_regOpcFitoAppz.getIdAppezzamento();

            for (RegOpcFitoProd item_regOpcFitoProd : regOpcFito.getOpcFitoToProds()) {
                item_id_fito = item_regOpcFitoProd.getIdFitofarmaco();
                item_id_magazzino = item_regOpcFitoProd.getIdMagazzino();
                item_id_avversita = item_regOpcFitoProd.getIdAvversita();

                item_id_coltura = regOpcFito.getAgea20152020Matrice().getId();
                item_id_operatore = regOpcFito.getOpeAnagrafica().getId();
                item_qta = item_regOpcFitoProd.getQuantitaFito();
                item_datain = Date.valueOf(regOpcFito.getDataInizOpc());
                item_datafine = Date.valueOf(regOpcFito.getDataFineOpc());
                item_id_mezzo = regOpcFito.getAtrAnagrafica().getId();

                try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {

                    CallableStatement cstmt = conn.prepareCall("{CALL controllifito(?,?,?,?,?,?,?,?,?,?,?)}");
                    cstmt.setInt(1, item_id_appezzamento.intValue());
                    cstmt.setInt(2, item_id_coltura.intValue());
                    cstmt.setInt(3, item_id_operatore.intValue());
                    cstmt.setInt(4, item_id_avversita.intValue());
                    cstmt.setInt(5, item_id_fito.intValue());
                    cstmt.setFloat(6, item_qta.floatValue());
                    cstmt.setInt(7, item_id_magazzino.intValue());
                    cstmt.setDate(8, item_datain);
                    cstmt.setDate(9, item_datafine);
                    cstmt.setInt(10, item_id_mezzo.intValue());
                    cstmt.registerOutParameter(11, Types.VARCHAR);
                    cstmt.executeUpdate();
                    return_ = cstmt.getString(11);
                    conn.close();

                    if(!return_.isEmpty() && !return_.equals("OK")){
                        String str[] = return_.split(";");
                        List<String> al = new ArrayList<String>();
                        al = Arrays.asList(str);
                        for (String s : al) {
                            list_errorTerram.add(new ErrorTerram(null, null, s.trim()));
                        }
                    }
                } catch (SQLException ex) {
                    System.out.println("Errore procedura controllifito()" );
                    // log.debug("Errore getFitoFromAvversitaAndColtura");
                }
            }
        }
        return list_errorTerram;
    }



    // public String getCallProcedurePostgres(String nomeMetodo, DataSource dataSource) {
    //     ResultSet rs = null;
    //     String return_ = "asdasdasd";
    //     // TipoPrepterr TipoPrepterr = new TipoPrepterr();

    //     try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {

    //         CallableStatement cstmt = conn.prepareCall("{CALL controllifito(?,?,?,?,?,?,?,?,?,?,?)}");
    //         cstmt.setInt(1, 1);
    //         cstmt.setInt(2, 1);
    //         cstmt.setInt(3, 1);
    //         cstmt.setInt(4, 1);
    //         cstmt.setInt(5, 1);
    //         cstmt.setInt(6, 1);
    //         cstmt.setInt(7, 1);
    //         cstmt.setDate(8, null);
    //         cstmt.setDate(9, null);
    //         cstmt.setInt(10, 1);
    //         cstmt.registerOutParameter(11, Types.VARCHAR);
    //         cstmt.executeUpdate();
    //         return_ = cstmt.getString(11);
    //         // rs = st.executeQuery(String.format("SELECT * FROM test(1)"));
    //         // if (rs.next()) {
    //         // return_ = rs.getString("test").toString();
    //         // // TipoPrepterr.setDescrizione(rs.getString("descrizione"));

    //         System.out.println("antonioooooooooooooooooooooooooooooooooooo");
    //         // }
    //         conn.close();
    //     } catch (SQLException ex) {
    //         // log.debug("Errore getFitoFromAvversitaAndColtura");
    //     }
    //     return return_;
    // }


    // public List<ListRegOpcFito> getListRegOpcFito(Long azienda_id, DataSource dataSource) {

    //     List<ListRegOpcFito> list_regopcfito = new ArrayList<>();

    //     Connection conn = null;
    //     Statement st = null;
    //     ResultSet rs = null;

    //     try {

    //         conn = dataSource.getConnection();
    //         st = conn.createStatement();
    //         rs = st.executeQuery(String.format(""));

    //         while (rs.next()) {
    //         }
    //     } catch (SQLException ex) {
    //         // log.debug("Errore getFitoFromAvversitaAndColtura");
    //     }

    //     return list_regopcfito;
    // }

}
