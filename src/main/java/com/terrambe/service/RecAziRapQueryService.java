package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecAziRap;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecAziRapRepository;
import com.terrambe.service.dto.RecAziRapCriteria;

/**
 * Service for executing complex queries for {@link RecAziRap} entities in the database.
 * The main input is a {@link RecAziRapCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecAziRap} or a {@link Page} of {@link RecAziRap} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecAziRapQueryService extends QueryService<RecAziRap> {

    private final Logger log = LoggerFactory.getLogger(RecAziRapQueryService.class);

    private final RecAziRapRepository recAziRapRepository;

    public RecAziRapQueryService(RecAziRapRepository recAziRapRepository) {
        this.recAziRapRepository = recAziRapRepository;
    }

    /**
     * Return a {@link List} of {@link RecAziRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziRap> findByCriteria(RecAziRapCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecAziRap> specification = createSpecification(criteria);
        return recAziRapRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecAziRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecAziRap> findByCriteria(RecAziRapCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecAziRap> specification = createSpecification(criteria);
        return recAziRapRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecAziRapCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecAziRap> specification = createSpecification(criteria);
        return recAziRapRepository.count(specification);
    }

    /**
     * Function to convert {@link RecAziRapCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecAziRap> createSpecification(RecAziRapCriteria criteria) {
        Specification<RecAziRap> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecAziRap_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecAziRap_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecAziRap_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecAziRap_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecAziRap_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecAziRap_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecAziRap_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecAziRap_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecAziRap_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecAziRap_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecAziRap_.userIdLastMod));
            }
            if (criteria.getRecToAziRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToAziRapId(),
                    root -> root.join(RecAziRap_.recToAziRap, JoinType.LEFT).get(AziRappresentante_.id)));
            }
        }
        return specification;
    }
}
