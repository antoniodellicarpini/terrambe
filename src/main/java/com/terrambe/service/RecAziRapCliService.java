package com.terrambe.service;

import com.terrambe.domain.RecAziRapCli;
import com.terrambe.repository.RecAziRapCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecAziRapCli}.
 */
@Service
@Transactional
public class RecAziRapCliService {

    private final Logger log = LoggerFactory.getLogger(RecAziRapCliService.class);

    private final RecAziRapCliRepository recAziRapCliRepository;

    public RecAziRapCliService(RecAziRapCliRepository recAziRapCliRepository) {
        this.recAziRapCliRepository = recAziRapCliRepository;
    }

    /**
     * Save a recAziRapCli.
     *
     * @param recAziRapCli the entity to save.
     * @return the persisted entity.
     */
    public RecAziRapCli save(RecAziRapCli recAziRapCli) {
        log.debug("Request to save RecAziRapCli : {}", recAziRapCli);
        return recAziRapCliRepository.save(recAziRapCli);
    }

    /**
     * Get all the recAziRapClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziRapCli> findAll() {
        log.debug("Request to get all RecAziRapClis");
        return recAziRapCliRepository.findAll();
    }


    /**
     * Get one recAziRapCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecAziRapCli> findOne(Long id) {
        log.debug("Request to get RecAziRapCli : {}", id);
        return recAziRapCliRepository.findById(id);
    }

    /**
     * Delete the recAziRapCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecAziRapCli : {}", id);
        recAziRapCliRepository.deleteById(id);
    }
}
