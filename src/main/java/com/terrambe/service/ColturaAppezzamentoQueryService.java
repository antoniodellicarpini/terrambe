package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ColturaAppezzamentoRepository;
import com.terrambe.service.dto.ColturaAppezzamentoCriteria;

/**
 * Service for executing complex queries for {@link ColturaAppezzamento} entities in the database.
 * The main input is a {@link ColturaAppezzamentoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ColturaAppezzamento} or a {@link Page} of {@link ColturaAppezzamento} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ColturaAppezzamentoQueryService extends QueryService<ColturaAppezzamento> {

    private final Logger log = LoggerFactory.getLogger(ColturaAppezzamentoQueryService.class);

    private final ColturaAppezzamentoRepository colturaAppezzamentoRepository;

    public ColturaAppezzamentoQueryService(ColturaAppezzamentoRepository colturaAppezzamentoRepository) {
        this.colturaAppezzamentoRepository = colturaAppezzamentoRepository;
    }

    /**
     * Return a {@link List} of {@link ColturaAppezzamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ColturaAppezzamento> findByCriteria(ColturaAppezzamentoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ColturaAppezzamento> specification = createSpecification(criteria);
        return colturaAppezzamentoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ColturaAppezzamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ColturaAppezzamento> findByCriteria(ColturaAppezzamentoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ColturaAppezzamento> specification = createSpecification(criteria);
        return colturaAppezzamentoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ColturaAppezzamentoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ColturaAppezzamento> specification = createSpecification(criteria);
        return colturaAppezzamentoRepository.count(specification);
    }

    /**
     * Function to convert {@link ColturaAppezzamentoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ColturaAppezzamento> createSpecification(ColturaAppezzamentoCriteria criteria) {
        Specification<ColturaAppezzamento> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ColturaAppezzamento_.id));
            }
            if (criteria.getPrimaria() != null) {
                specification = specification.and(buildSpecification(criteria.getPrimaria(), ColturaAppezzamento_.primaria));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), ColturaAppezzamento_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), ColturaAppezzamento_.idUnitaProd));
            }
            if (criteria.getDataInizioColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizioColtura(), ColturaAppezzamento_.dataInizioColtura));
            }
            if (criteria.getDataFineColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineColtura(), ColturaAppezzamento_.dataFineColtura));
            }
            if (criteria.getDataFioritura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFioritura(), ColturaAppezzamento_.dataFioritura));
            }
            if (criteria.getDataFineFioritura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineFioritura(), ColturaAppezzamento_.dataFineFioritura));
            }
            if (criteria.getDataRaccolta() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataRaccolta(), ColturaAppezzamento_.dataRaccolta));
            }
            if (criteria.getNumeroPiante() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumeroPiante(), ColturaAppezzamento_.numeroPiante));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), ColturaAppezzamento_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), ColturaAppezzamento_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), ColturaAppezzamento_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), ColturaAppezzamento_.userIdLastMod));
            }
            if (criteria.getColtToAppeId() != null) {
                specification = specification.and(buildSpecification(criteria.getColtToAppeId(),
                    root -> root.join(ColturaAppezzamento_.coltToAppe, JoinType.LEFT).get(Appezzamenti_.id)));
            }
            if (criteria.getColtToAgeaId() != null) {
                specification = specification.and(buildSpecification(criteria.getColtToAgeaId(),
                    root -> root.join(ColturaAppezzamento_.coltToAgea, JoinType.LEFT).get(Agea20152020Matrice_.id)));
            }
        }
        return specification;
    }
}
