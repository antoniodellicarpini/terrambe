package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.ColtureBDF;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ColtureBDFRepository;
import com.terrambe.service.dto.ColtureBDFCriteria;

/**
 * Service for executing complex queries for {@link ColtureBDF} entities in the database.
 * The main input is a {@link ColtureBDFCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ColtureBDF} or a {@link Page} of {@link ColtureBDF} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ColtureBDFQueryService extends QueryService<ColtureBDF> {

    private final Logger log = LoggerFactory.getLogger(ColtureBDFQueryService.class);

    private final ColtureBDFRepository coltureBDFRepository;

    public ColtureBDFQueryService(ColtureBDFRepository coltureBDFRepository) {
        this.coltureBDFRepository = coltureBDFRepository;
    }

    /**
     * Return a {@link List} of {@link ColtureBDF} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ColtureBDF> findByCriteria(ColtureBDFCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ColtureBDF> specification = createSpecification(criteria);
        return coltureBDFRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ColtureBDF} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ColtureBDF> findByCriteria(ColtureBDFCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ColtureBDF> specification = createSpecification(criteria);
        return coltureBDFRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ColtureBDFCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ColtureBDF> specification = createSpecification(criteria);
        return coltureBDFRepository.count(specification);
    }

    /**
     * Function to convert {@link ColtureBDFCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ColtureBDF> createSpecification(ColtureBDFCriteria criteria) {
        Specification<ColtureBDF> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ColtureBDF_.id));
            }
            if (criteria.getCodicePv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodicePv(), ColtureBDF_.codicePv));
            }
            if (criteria.getNomeColtura() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeColtura(), ColtureBDF_.nomeColtura));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), ColtureBDF_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), ColtureBDF_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), ColtureBDF_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), ColtureBDF_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), ColtureBDF_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), ColtureBDF_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), ColtureBDF_.userIdLastMod));
            }
            if (criteria.getColtBdfToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getColtBdfToRaccordoId(),
                    root -> root.join(ColtureBDF_.coltBdfToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
            if (criteria.getColtBdfToColtMascId() != null) {
                specification = specification.and(buildSpecification(criteria.getColtBdfToColtMascId(),
                    root -> root.join(ColtureBDF_.coltBdfToColtMascs, JoinType.LEFT).get(TerramColtureMascherate_.id)));
            }
            if (criteria.getColtBdfToAgeaBdfId() != null) {
                specification = specification.and(buildSpecification(criteria.getColtBdfToAgeaBdfId(),
                    root -> root.join(ColtureBDF_.coltBdfToAgeaBdfs, JoinType.LEFT).get(Agea20152020BDF_.id)));
            }
        }
        return specification;
    }
}
