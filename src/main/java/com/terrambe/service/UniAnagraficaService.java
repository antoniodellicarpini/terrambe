package com.terrambe.service;

import com.terrambe.domain.UniAnagrafica;
import com.terrambe.repository.UniAnagraficaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UniAnagrafica}.
 */
@Service
@Transactional
public class UniAnagraficaService {

    private final Logger log = LoggerFactory.getLogger(UniAnagraficaService.class);

    private final UniAnagraficaRepository uniAnagraficaRepository;

    public UniAnagraficaService(UniAnagraficaRepository uniAnagraficaRepository) {
        this.uniAnagraficaRepository = uniAnagraficaRepository;
    }

    /**
     * Save a uniAnagrafica.
     *
     * @param uniAnagrafica the entity to save.
     * @return the persisted entity.
     */
    public UniAnagrafica save(UniAnagrafica uniAnagrafica) {
        log.debug("Request to save UniAnagrafica : {}", uniAnagrafica);
        return uniAnagraficaRepository.save(uniAnagrafica);
    }

    /**
     * Get all the uniAnagraficas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UniAnagrafica> findAll() {
        log.debug("Request to get all UniAnagraficas");
        return uniAnagraficaRepository.findAll();
    }


    /**
     * Get one uniAnagrafica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UniAnagrafica> findOne(Long id) {
        log.debug("Request to get UniAnagrafica : {}", id);
        return uniAnagraficaRepository.findById(id);
    }

    /**
     * Delete the uniAnagrafica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UniAnagrafica : {}", id);
        uniAnagraficaRepository.deleteById(id);
    }
}
