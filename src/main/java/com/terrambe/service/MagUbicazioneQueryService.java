package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.MagUbicazione;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.MagUbicazioneRepository;
import com.terrambe.service.dto.MagUbicazioneCriteria;

/**
 * Service for executing complex queries for {@link MagUbicazione} entities in the database.
 * The main input is a {@link MagUbicazioneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MagUbicazione} or a {@link Page} of {@link MagUbicazione} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MagUbicazioneQueryService extends QueryService<MagUbicazione> {

    private final Logger log = LoggerFactory.getLogger(MagUbicazioneQueryService.class);

    private final MagUbicazioneRepository magUbicazioneRepository;

    public MagUbicazioneQueryService(MagUbicazioneRepository magUbicazioneRepository) {
        this.magUbicazioneRepository = magUbicazioneRepository;
    }

    /**
     * Return a {@link List} of {@link MagUbicazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MagUbicazione> findByCriteria(MagUbicazioneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MagUbicazione> specification = createSpecification(criteria);
        return magUbicazioneRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MagUbicazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MagUbicazione> findByCriteria(MagUbicazioneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MagUbicazione> specification = createSpecification(criteria);
        return magUbicazioneRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MagUbicazioneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MagUbicazione> specification = createSpecification(criteria);
        return magUbicazioneRepository.count(specification);
    }

    /**
     * Function to convert {@link MagUbicazioneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MagUbicazione> createSpecification(MagUbicazioneCriteria criteria) {
        Specification<MagUbicazione> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MagUbicazione_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), MagUbicazione_.idAzienda));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), MagUbicazione_.descrizione));
            }
            if (criteria.getDataInizioMag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizioMag(), MagUbicazione_.dataInizioMag));
            }
            if (criteria.getDataFineMag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineMag(), MagUbicazione_.dataFineMag));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), MagUbicazione_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), MagUbicazione_.userIdLastMod));
            }
            if (criteria.getMagUbiToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getMagUbiToIndId(),
                    root -> root.join(MagUbicazione_.magUbiToInds, JoinType.LEFT).get(IndMagUbi_.id)));
            }
            if (criteria.getMagUbiToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getMagUbiToRecId(),
                    root -> root.join(MagUbicazione_.magUbiToRecs, JoinType.LEFT).get(RecMagUbi_.id)));
            }
            if (criteria.getRegcaricotrappoleId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegcaricotrappoleId(),
                    root -> root.join(MagUbicazione_.regcaricotrappoles, JoinType.LEFT).get(RegCaricoTrappole_.id)));
            }
            if (criteria.getRegcaricofitofarmacoId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegcaricofitofarmacoId(),
                    root -> root.join(MagUbicazione_.regcaricofitofarmacos, JoinType.LEFT).get(RegCaricoFitoFarmaco_.id)));
            }
            if (criteria.getRegcaricofertilizzanteId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegcaricofertilizzanteId(),
                    root -> root.join(MagUbicazione_.regcaricofertilizzantes, JoinType.LEFT).get(RegCaricoFertilizzante_.id)));
            }
            if (criteria.getUnianagraficaId() != null) {
                specification = specification.and(buildSpecification(criteria.getUnianagraficaId(),
                    root -> root.join(MagUbicazione_.unianagrafica, JoinType.LEFT).get(UniAnagrafica_.id)));
            }
        }
        return specification;
    }
}
