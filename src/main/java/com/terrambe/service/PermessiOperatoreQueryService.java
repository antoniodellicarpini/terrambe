package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.PermessiOperatore;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.PermessiOperatoreRepository;
import com.terrambe.service.dto.PermessiOperatoreCriteria;

/**
 * Service for executing complex queries for {@link PermessiOperatore} entities in the database.
 * The main input is a {@link PermessiOperatoreCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PermessiOperatore} or a {@link Page} of {@link PermessiOperatore} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PermessiOperatoreQueryService extends QueryService<PermessiOperatore> {

    private final Logger log = LoggerFactory.getLogger(PermessiOperatoreQueryService.class);

    private final PermessiOperatoreRepository permessiOperatoreRepository;

    public PermessiOperatoreQueryService(PermessiOperatoreRepository permessiOperatoreRepository) {
        this.permessiOperatoreRepository = permessiOperatoreRepository;
    }

    /**
     * Return a {@link List} of {@link PermessiOperatore} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PermessiOperatore> findByCriteria(PermessiOperatoreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PermessiOperatore> specification = createSpecification(criteria);
        return permessiOperatoreRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link PermessiOperatore} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PermessiOperatore> findByCriteria(PermessiOperatoreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PermessiOperatore> specification = createSpecification(criteria);
        return permessiOperatoreRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PermessiOperatoreCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PermessiOperatore> specification = createSpecification(criteria);
        return permessiOperatoreRepository.count(specification);
    }

    /**
     * Function to convert {@link PermessiOperatoreCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PermessiOperatore> createSpecification(PermessiOperatoreCriteria criteria) {
        Specification<PermessiOperatore> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PermessiOperatore_.id));
            }
            if (criteria.getFlagModifica() != null) {
                specification = specification.and(buildSpecification(criteria.getFlagModifica(), PermessiOperatore_.flagModifica));
            }
            if (criteria.getFlagVisualizza() != null) {
                specification = specification.and(buildSpecification(criteria.getFlagVisualizza(), PermessiOperatore_.flagVisualizza));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), PermessiOperatore_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), PermessiOperatore_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), PermessiOperatore_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), PermessiOperatore_.userIdLastMod));
            }
            if (criteria.getPermOperToTipoPermId() != null) {
                specification = specification.and(buildSpecification(criteria.getPermOperToTipoPermId(),
                    root -> root.join(PermessiOperatore_.permOperToTipoPerm, JoinType.LEFT).get(TipologiaPermessoOperatore_.id)));
            }
            if (criteria.getPerOperToOpeAnagId() != null) {
                specification = specification.and(buildSpecification(criteria.getPerOperToOpeAnagId(),
                    root -> root.join(PermessiOperatore_.perOperToOpeAnag, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
