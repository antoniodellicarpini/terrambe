package com.terrambe.service;

import com.terrambe.domain.DizQualita;
import com.terrambe.repository.DizQualitaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizQualita}.
 */
@Service
@Transactional
public class DizQualitaService {

    private final Logger log = LoggerFactory.getLogger(DizQualitaService.class);

    private final DizQualitaRepository dizQualitaRepository;

    public DizQualitaService(DizQualitaRepository dizQualitaRepository) {
        this.dizQualitaRepository = dizQualitaRepository;
    }

    /**
     * Save a dizQualita.
     *
     * @param dizQualita the entity to save.
     * @return the persisted entity.
     */
    public DizQualita save(DizQualita dizQualita) {
        log.debug("Request to save DizQualita : {}", dizQualita);
        return dizQualitaRepository.save(dizQualita);
    }

    /**
     * Get all the dizQualitas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizQualita> findAll() {
        log.debug("Request to get all DizQualitas");
        return dizQualitaRepository.findAll();
    }


    /**
     * Get one dizQualita by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizQualita> findOne(Long id) {
        log.debug("Request to get DizQualita : {}", id);
        return dizQualitaRepository.findById(id);
    }

    /**
     * Delete the dizQualita by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizQualita : {}", id);
        dizQualitaRepository.deleteById(id);
    }
}
