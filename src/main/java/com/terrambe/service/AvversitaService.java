package com.terrambe.service;

import com.terrambe.domain.Avversita;
import com.terrambe.repository.AvversitaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Avversita}.
 */
@Service
@Transactional
public class AvversitaService {

    private final Logger log = LoggerFactory.getLogger(AvversitaService.class);

    private final AvversitaRepository avversitaRepository;

    public AvversitaService(AvversitaRepository avversitaRepository) {
        this.avversitaRepository = avversitaRepository;
    }

    /**
     * Save a avversita.
     *
     * @param avversita the entity to save.
     * @return the persisted entity.
     */
    public Avversita save(Avversita avversita) {
        log.debug("Request to save Avversita : {}", avversita);
        return avversitaRepository.save(avversita);
    }

    /**
     * Get all the avversitas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Avversita> findAll() {
        log.debug("Request to get all Avversitas");
        return avversitaRepository.findAll();
    }


    /**
     * Get one avversita by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Avversita> findOne(Long id) {
        log.debug("Request to get Avversita : {}", id);
        return avversitaRepository.findById(id);
    }

    /**
     * Delete the avversita by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Avversita : {}", id);
        avversitaRepository.deleteById(id);
    }
}
