package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DizQualita;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DizQualitaRepository;
import com.terrambe.service.dto.DizQualitaCriteria;

/**
 * Service for executing complex queries for {@link DizQualita} entities in the database.
 * The main input is a {@link DizQualitaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DizQualita} or a {@link Page} of {@link DizQualita} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DizQualitaQueryService extends QueryService<DizQualita> {

    private final Logger log = LoggerFactory.getLogger(DizQualitaQueryService.class);

    private final DizQualitaRepository dizQualitaRepository;

    public DizQualitaQueryService(DizQualitaRepository dizQualitaRepository) {
        this.dizQualitaRepository = dizQualitaRepository;
    }

    /**
     * Return a {@link List} of {@link DizQualita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DizQualita> findByCriteria(DizQualitaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DizQualita> specification = createSpecification(criteria);
        return dizQualitaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DizQualita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DizQualita> findByCriteria(DizQualitaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DizQualita> specification = createSpecification(criteria);
        return dizQualitaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DizQualitaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DizQualita> specification = createSpecification(criteria);
        return dizQualitaRepository.count(specification);
    }

    /**
     * Function to convert {@link DizQualitaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DizQualita> createSpecification(DizQualitaCriteria criteria) {
        Specification<DizQualita> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DizQualita_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), DizQualita_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), DizQualita_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), DizQualita_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), DizQualita_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), DizQualita_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), DizQualita_.userIdLastMod));
            }
        }
        return specification;
    }
}
