package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndAzienda;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndAziendaRepository;
import com.terrambe.service.dto.IndAziendaCriteria;

/**
 * Service for executing complex queries for {@link IndAzienda} entities in the database.
 * The main input is a {@link IndAziendaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndAzienda} or a {@link Page} of {@link IndAzienda} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndAziendaQueryService extends QueryService<IndAzienda> {

    private final Logger log = LoggerFactory.getLogger(IndAziendaQueryService.class);

    private final IndAziendaRepository indAziendaRepository;

    public IndAziendaQueryService(IndAziendaRepository indAziendaRepository) {
        this.indAziendaRepository = indAziendaRepository;
    }

    /**
     * Return a {@link List} of {@link IndAzienda} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndAzienda> findByCriteria(IndAziendaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndAzienda> specification = createSpecification(criteria);
        return indAziendaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndAzienda} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndAzienda> findByCriteria(IndAziendaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndAzienda> specification = createSpecification(criteria);
        return indAziendaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndAziendaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndAzienda> specification = createSpecification(criteria);
        return indAziendaRepository.count(specification);
    }

    /**
     * Function to convert {@link IndAziendaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndAzienda> createSpecification(IndAziendaCriteria criteria) {
        Specification<IndAzienda> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndAzienda_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndAzienda_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndAzienda_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndAzienda_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndAzienda_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndAzienda_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndAzienda_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndAzienda_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndAzienda_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndAzienda_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndAzienda_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndAzienda_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndAzienda_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndAzienda_.userIdLastMod));
            }
            if (criteria.getIndToAziId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToAziId(),
                    root -> root.join(IndAzienda_.indToAzi, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
        }
        return specification;
    }
}
