package com.terrambe.service;

import com.terrambe.domain.Sito;
import com.terrambe.repository.SitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Sito}.
 */
@Service
@Transactional
public class SitoService {

    private final Logger log = LoggerFactory.getLogger(SitoService.class);

    private final SitoRepository sitoRepository;

    public SitoService(SitoRepository sitoRepository) {
        this.sitoRepository = sitoRepository;
    }

    /**
     * Save a sito.
     *
     * @param sito the entity to save.
     * @return the persisted entity.
     */
    public Sito save(Sito sito) {
        log.debug("Request to save Sito : {}", sito);
        return sitoRepository.save(sito);
    }

    /**
     * Get all the sitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Sito> findAll() {
        log.debug("Request to get all Sitos");
        return sitoRepository.findAll();
    }


    /**
     * Get one sito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Sito> findOne(Long id) {
        log.debug("Request to get Sito : {}", id);
        return sitoRepository.findById(id);
    }

    /**
     * Delete the sito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sito : {}", id);
        sitoRepository.deleteById(id);
    }
}
