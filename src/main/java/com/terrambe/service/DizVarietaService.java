package com.terrambe.service;

import com.terrambe.domain.DizVarieta;
import com.terrambe.repository.DizVarietaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizVarieta}.
 */
@Service
@Transactional
public class DizVarietaService {

    private final Logger log = LoggerFactory.getLogger(DizVarietaService.class);

    private final DizVarietaRepository dizVarietaRepository;

    public DizVarietaService(DizVarietaRepository dizVarietaRepository) {
        this.dizVarietaRepository = dizVarietaRepository;
    }

    /**
     * Save a dizVarieta.
     *
     * @param dizVarieta the entity to save.
     * @return the persisted entity.
     */
    public DizVarieta save(DizVarieta dizVarieta) {
        log.debug("Request to save DizVarieta : {}", dizVarieta);
        return dizVarietaRepository.save(dizVarieta);
    }

    /**
     * Get all the dizVarietas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizVarieta> findAll() {
        log.debug("Request to get all DizVarietas");
        return dizVarietaRepository.findAll();
    }


    /**
     * Get one dizVarieta by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizVarieta> findOne(Long id) {
        log.debug("Request to get DizVarieta : {}", id);
        return dizVarietaRepository.findById(id);
    }

    /**
     * Delete the dizVarieta by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizVarieta : {}", id);
        dizVarietaRepository.deleteById(id);
    }
}
