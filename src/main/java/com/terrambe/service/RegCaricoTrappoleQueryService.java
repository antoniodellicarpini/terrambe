package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegCaricoTrappoleRepository;
import com.terrambe.service.dto.RegCaricoTrappoleCriteria;

/**
 * Service for executing complex queries for {@link RegCaricoTrappole} entities in the database.
 * The main input is a {@link RegCaricoTrappoleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegCaricoTrappole} or a {@link Page} of {@link RegCaricoTrappole} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegCaricoTrappoleQueryService extends QueryService<RegCaricoTrappole> {

    private final Logger log = LoggerFactory.getLogger(RegCaricoTrappoleQueryService.class);

    private final RegCaricoTrappoleRepository regCaricoTrappoleRepository;

    public RegCaricoTrappoleQueryService(RegCaricoTrappoleRepository regCaricoTrappoleRepository) {
        this.regCaricoTrappoleRepository = regCaricoTrappoleRepository;
    }

    /**
     * Return a {@link List} of {@link RegCaricoTrappole} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoTrappole> findByCriteria(RegCaricoTrappoleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegCaricoTrappole> specification = createSpecification(criteria);
        return regCaricoTrappoleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegCaricoTrappole} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegCaricoTrappole> findByCriteria(RegCaricoTrappoleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegCaricoTrappole> specification = createSpecification(criteria);
        return regCaricoTrappoleRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegCaricoTrappoleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegCaricoTrappole> specification = createSpecification(criteria);
        return regCaricoTrappoleRepository.count(specification);
    }

    /**
     * Function to convert {@link RegCaricoTrappoleCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegCaricoTrappole> createSpecification(RegCaricoTrappoleCriteria criteria) {
        Specification<RegCaricoTrappole> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegCaricoTrappole_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegCaricoTrappole_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegCaricoTrappole_.idUnitaProd));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegCaricoTrappole_.quantita));
            }
            if (criteria.getPrezzoUnitario() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrezzoUnitario(), RegCaricoTrappole_.prezzoUnitario));
            }
            if (criteria.getDataCarico() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataCarico(), RegCaricoTrappole_.dataCarico));
            }
            if (criteria.getDdt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDdt(), RegCaricoTrappole_.ddt));
            }
            if (criteria.getDataScadenza() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataScadenza(), RegCaricoTrappole_.dataScadenza));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegCaricoTrappole_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegCaricoTrappole_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegCaricoTrappole_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegCaricoTrappole_.userIdLastMod));
            }
            if (criteria.getMagUbicazioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getMagUbicazioneId(),
                    root -> root.join(RegCaricoTrappole_.magUbicazione, JoinType.LEFT).get(MagUbicazione_.id)));
            }
            if (criteria.getTrappoleId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrappoleId(),
                    root -> root.join(RegCaricoTrappole_.trappole, JoinType.LEFT).get(Trappole_.id)));
            }
            if (criteria.getCaricoTrapToFornitoriId() != null) {
                specification = specification.and(buildSpecification(criteria.getCaricoTrapToFornitoriId(),
                    root -> root.join(RegCaricoTrappole_.caricoTrapToFornitori, JoinType.LEFT).get(Fornitori_.id)));
            }
            if (criteria.getRegTrapToTipoRegId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegTrapToTipoRegId(),
                    root -> root.join(RegCaricoTrappole_.regTrapToTipoReg, JoinType.LEFT).get(TipoRegMag_.id)));
            }
        }
        return specification;
    }
}
