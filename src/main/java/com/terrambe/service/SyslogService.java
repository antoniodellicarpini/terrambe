package com.terrambe.service;

import com.terrambe.domain.Syslog;
import com.terrambe.repository.SyslogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Syslog}.
 */
@Service
@Transactional
public class SyslogService {

    private final Logger log = LoggerFactory.getLogger(SyslogService.class);

    private final SyslogRepository syslogRepository;

    public SyslogService(SyslogRepository syslogRepository) {
        this.syslogRepository = syslogRepository;
    }

    /**
     * Save a syslog.
     *
     * @param syslog the entity to save.
     * @return the persisted entity.
     */
    public Syslog save(Syslog syslog) {
        log.debug("Request to save Syslog : {}", syslog);
        return syslogRepository.save(syslog);
    }

    /**
     * Get all the syslogs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Syslog> findAll() {
        log.debug("Request to get all Syslogs");
        return syslogRepository.findAll();
    }


    /**
     * Get one syslog by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Syslog> findOne(Long id) {
        log.debug("Request to get Syslog : {}", id);
        return syslogRepository.findById(id);
    }

    /**
     * Delete the syslog by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Syslog : {}", id);
        syslogRepository.deleteById(id);
    }
}
