package com.terrambe.service;

import com.terrambe.domain.MotivoTrattFito;
import com.terrambe.repository.MotivoTrattFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MotivoTrattFito}.
 */
@Service
@Transactional
public class MotivoTrattFitoService {

    private final Logger log = LoggerFactory.getLogger(MotivoTrattFitoService.class);

    private final MotivoTrattFitoRepository motivoTrattFitoRepository;

    public MotivoTrattFitoService(MotivoTrattFitoRepository motivoTrattFitoRepository) {
        this.motivoTrattFitoRepository = motivoTrattFitoRepository;
    }

    /**
     * Save a motivoTrattFito.
     *
     * @param motivoTrattFito the entity to save.
     * @return the persisted entity.
     */
    public MotivoTrattFito save(MotivoTrattFito motivoTrattFito) {
        log.debug("Request to save MotivoTrattFito : {}", motivoTrattFito);
        return motivoTrattFitoRepository.save(motivoTrattFito);
    }

    /**
     * Get all the motivoTrattFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoTrattFito> findAll() {
        log.debug("Request to get all MotivoTrattFitos");
        return motivoTrattFitoRepository.findAll();
    }


    /**
     * Get one motivoTrattFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MotivoTrattFito> findOne(Long id) {
        log.debug("Request to get MotivoTrattFito : {}", id);
        return motivoTrattFitoRepository.findById(id);
    }

    /**
     * Delete the motivoTrattFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MotivoTrattFito : {}", id);
        motivoTrattFitoRepository.deleteById(id);
    }
}
