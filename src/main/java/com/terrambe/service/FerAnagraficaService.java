package com.terrambe.service;

import com.terrambe.domain.FerAnagrafica;
import com.terrambe.repository.FerAnagraficaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FerAnagrafica}.
 */
@Service
@Transactional
public class FerAnagraficaService {

    private final Logger log = LoggerFactory.getLogger(FerAnagraficaService.class);

    private final FerAnagraficaRepository ferAnagraficaRepository;

    public FerAnagraficaService(FerAnagraficaRepository ferAnagraficaRepository) {
        this.ferAnagraficaRepository = ferAnagraficaRepository;
    }

    /**
     * Save a ferAnagrafica.
     *
     * @param ferAnagrafica the entity to save.
     * @return the persisted entity.
     */
    public FerAnagrafica save(FerAnagrafica ferAnagrafica) {
        log.debug("Request to save FerAnagrafica : {}", ferAnagrafica);
        return ferAnagraficaRepository.save(ferAnagrafica);
    }

    /**
     * Get all the ferAnagraficas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FerAnagrafica> findAll() {
        log.debug("Request to get all FerAnagraficas");
        return ferAnagraficaRepository.findAll();
    }


    /**
     * Get one ferAnagrafica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FerAnagrafica> findOne(Long id) {
        log.debug("Request to get FerAnagrafica : {}", id);
        return ferAnagraficaRepository.findById(id);
    }

    /**
     * Delete the ferAnagrafica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FerAnagrafica : {}", id);
        ferAnagraficaRepository.deleteById(id);
    }
}
