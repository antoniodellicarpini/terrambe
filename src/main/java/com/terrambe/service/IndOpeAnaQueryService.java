package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndOpeAna;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndOpeAnaRepository;
import com.terrambe.service.dto.IndOpeAnaCriteria;

/**
 * Service for executing complex queries for {@link IndOpeAna} entities in the database.
 * The main input is a {@link IndOpeAnaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndOpeAna} or a {@link Page} of {@link IndOpeAna} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndOpeAnaQueryService extends QueryService<IndOpeAna> {

    private final Logger log = LoggerFactory.getLogger(IndOpeAnaQueryService.class);

    private final IndOpeAnaRepository indOpeAnaRepository;

    public IndOpeAnaQueryService(IndOpeAnaRepository indOpeAnaRepository) {
        this.indOpeAnaRepository = indOpeAnaRepository;
    }

    /**
     * Return a {@link List} of {@link IndOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndOpeAna> findByCriteria(IndOpeAnaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndOpeAna> specification = createSpecification(criteria);
        return indOpeAnaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndOpeAna> findByCriteria(IndOpeAnaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndOpeAna> specification = createSpecification(criteria);
        return indOpeAnaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndOpeAnaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndOpeAna> specification = createSpecification(criteria);
        return indOpeAnaRepository.count(specification);
    }

    /**
     * Function to convert {@link IndOpeAnaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndOpeAna> createSpecification(IndOpeAnaCriteria criteria) {
        Specification<IndOpeAna> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndOpeAna_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndOpeAna_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndOpeAna_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndOpeAna_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndOpeAna_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndOpeAna_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndOpeAna_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndOpeAna_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndOpeAna_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndOpeAna_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndOpeAna_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndOpeAna_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndOpeAna_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndOpeAna_.userIdLastMod));
            }
            if (criteria.getIndToOpeAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToOpeAnaId(),
                    root -> root.join(IndOpeAna_.indToOpeAna, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
