package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TitolareRegistrazione;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TitolareRegistrazioneRepository;
import com.terrambe.service.dto.TitolareRegistrazioneCriteria;

/**
 * Service for executing complex queries for {@link TitolareRegistrazione} entities in the database.
 * The main input is a {@link TitolareRegistrazioneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TitolareRegistrazione} or a {@link Page} of {@link TitolareRegistrazione} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TitolareRegistrazioneQueryService extends QueryService<TitolareRegistrazione> {

    private final Logger log = LoggerFactory.getLogger(TitolareRegistrazioneQueryService.class);

    private final TitolareRegistrazioneRepository titolareRegistrazioneRepository;

    public TitolareRegistrazioneQueryService(TitolareRegistrazioneRepository titolareRegistrazioneRepository) {
        this.titolareRegistrazioneRepository = titolareRegistrazioneRepository;
    }

    /**
     * Return a {@link List} of {@link TitolareRegistrazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TitolareRegistrazione> findByCriteria(TitolareRegistrazioneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TitolareRegistrazione> specification = createSpecification(criteria);
        return titolareRegistrazioneRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TitolareRegistrazione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TitolareRegistrazione> findByCriteria(TitolareRegistrazioneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TitolareRegistrazione> specification = createSpecification(criteria);
        return titolareRegistrazioneRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TitolareRegistrazioneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TitolareRegistrazione> specification = createSpecification(criteria);
        return titolareRegistrazioneRepository.count(specification);
    }

    /**
     * Function to convert {@link TitolareRegistrazioneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TitolareRegistrazione> createSpecification(TitolareRegistrazioneCriteria criteria) {
        Specification<TitolareRegistrazione> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TitolareRegistrazione_.id));
            }
            if (criteria.getDittaProduttrice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDittaProduttrice(), TitolareRegistrazione_.dittaProduttrice));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), TitolareRegistrazione_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), TitolareRegistrazione_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), TitolareRegistrazione_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TitolareRegistrazione_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TitolareRegistrazione_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TitolareRegistrazione_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TitolareRegistrazione_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(TitolareRegistrazione_.etichettaFitos, JoinType.LEFT).get(EtichettaFito_.id)));
            }
        }
        return specification;
    }
}
