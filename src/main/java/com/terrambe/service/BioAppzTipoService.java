package com.terrambe.service;

import com.terrambe.domain.BioAppzTipo;
import com.terrambe.repository.BioAppzTipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BioAppzTipo}.
 */
@Service
@Transactional
public class BioAppzTipoService {

    private final Logger log = LoggerFactory.getLogger(BioAppzTipoService.class);

    private final BioAppzTipoRepository bioAppzTipoRepository;

    public BioAppzTipoService(BioAppzTipoRepository bioAppzTipoRepository) {
        this.bioAppzTipoRepository = bioAppzTipoRepository;
    }

    /**
     * Save a bioAppzTipo.
     *
     * @param bioAppzTipo the entity to save.
     * @return the persisted entity.
     */
    public BioAppzTipo save(BioAppzTipo bioAppzTipo) {
        log.debug("Request to save BioAppzTipo : {}", bioAppzTipo);
        return bioAppzTipoRepository.save(bioAppzTipo);
    }

    /**
     * Get all the bioAppzTipos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BioAppzTipo> findAll() {
        log.debug("Request to get all BioAppzTipos");
        return bioAppzTipoRepository.findAll();
    }


    /**
     * Get one bioAppzTipo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BioAppzTipo> findOne(Long id) {
        log.debug("Request to get BioAppzTipo : {}", id);
        return bioAppzTipoRepository.findById(id);
    }

    /**
     * Delete the bioAppzTipo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BioAppzTipo : {}", id);
        bioAppzTipoRepository.deleteById(id);
    }
}
