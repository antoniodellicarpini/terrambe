package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TerramColtureMascherate;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TerramColtureMascherateRepository;
import com.terrambe.service.dto.TerramColtureMascherateCriteria;

/**
 * Service for executing complex queries for {@link TerramColtureMascherate} entities in the database.
 * The main input is a {@link TerramColtureMascherateCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TerramColtureMascherate} or a {@link Page} of {@link TerramColtureMascherate} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TerramColtureMascherateQueryService extends QueryService<TerramColtureMascherate> {

    private final Logger log = LoggerFactory.getLogger(TerramColtureMascherateQueryService.class);

    private final TerramColtureMascherateRepository terramColtureMascherateRepository;

    public TerramColtureMascherateQueryService(TerramColtureMascherateRepository terramColtureMascherateRepository) {
        this.terramColtureMascherateRepository = terramColtureMascherateRepository;
    }

    /**
     * Return a {@link List} of {@link TerramColtureMascherate} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TerramColtureMascherate> findByCriteria(TerramColtureMascherateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TerramColtureMascherate> specification = createSpecification(criteria);
        return terramColtureMascherateRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TerramColtureMascherate} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TerramColtureMascherate> findByCriteria(TerramColtureMascherateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TerramColtureMascherate> specification = createSpecification(criteria);
        return terramColtureMascherateRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TerramColtureMascherateCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TerramColtureMascherate> specification = createSpecification(criteria);
        return terramColtureMascherateRepository.count(specification);
    }

    /**
     * Function to convert {@link TerramColtureMascherateCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TerramColtureMascherate> createSpecification(TerramColtureMascherateCriteria criteria) {
        Specification<TerramColtureMascherate> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TerramColtureMascherate_.id));
            }
            if (criteria.getCodiceColtureMascherata() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceColtureMascherata(), TerramColtureMascherate_.codiceColtureMascherata));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TerramColtureMascherate_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TerramColtureMascherate_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TerramColtureMascherate_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TerramColtureMascherate_.userIdLastMod));
            }
            if (criteria.getColMascToColtBdfId() != null) {
                specification = specification.and(buildSpecification(criteria.getColMascToColtBdfId(),
                    root -> root.join(TerramColtureMascherate_.colMascToColtBdf, JoinType.LEFT).get(ColtureBDF_.id)));
            }
        }
        return specification;
    }
}
