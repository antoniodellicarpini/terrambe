package com.terrambe.service;

import com.terrambe.domain.IndNascRap;
import com.terrambe.repository.IndNascRapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link IndNascRap}.
 */
@Service
@Transactional
public class IndNascRapService {

    private final Logger log = LoggerFactory.getLogger(IndNascRapService.class);

    private final IndNascRapRepository indNascRapRepository;

    public IndNascRapService(IndNascRapRepository indNascRapRepository) {
        this.indNascRapRepository = indNascRapRepository;
    }

    /**
     * Save a indNascRap.
     *
     * @param indNascRap the entity to save.
     * @return the persisted entity.
     */
    public IndNascRap save(IndNascRap indNascRap) {
        log.debug("Request to save IndNascRap : {}", indNascRap);
        return indNascRapRepository.save(indNascRap);
    }

    /**
     * Get all the indNascRaps.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascRap> findAll() {
        log.debug("Request to get all IndNascRaps");
        return indNascRapRepository.findAll();
    }



    /**
    *  Get all the indNascRaps where IndNascToAziRap is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<IndNascRap> findAllWhereIndNascToAziRapIsNull() {
        log.debug("Request to get all indNascRaps where IndNascToAziRap is null");
        return StreamSupport
            .stream(indNascRapRepository.findAll().spliterator(), false)
            .filter(indNascRap -> indNascRap.getIndNascToAziRap() == null)
            .collect(Collectors.toList());
    }

    /**
     * Get one indNascRap by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndNascRap> findOne(Long id) {
        log.debug("Request to get IndNascRap : {}", id);
        return indNascRapRepository.findById(id);
    }

    /**
     * Delete the indNascRap by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndNascRap : {}", id);
        indNascRapRepository.deleteById(id);
    }
}
