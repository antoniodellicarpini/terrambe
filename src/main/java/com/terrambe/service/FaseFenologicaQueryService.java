package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.FaseFenologica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FaseFenologicaRepository;
import com.terrambe.service.dto.FaseFenologicaCriteria;

/**
 * Service for executing complex queries for {@link FaseFenologica} entities in the database.
 * The main input is a {@link FaseFenologicaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FaseFenologica} or a {@link Page} of {@link FaseFenologica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FaseFenologicaQueryService extends QueryService<FaseFenologica> {

    private final Logger log = LoggerFactory.getLogger(FaseFenologicaQueryService.class);

    private final FaseFenologicaRepository faseFenologicaRepository;

    public FaseFenologicaQueryService(FaseFenologicaRepository faseFenologicaRepository) {
        this.faseFenologicaRepository = faseFenologicaRepository;
    }

    /**
     * Return a {@link List} of {@link FaseFenologica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FaseFenologica> findByCriteria(FaseFenologicaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FaseFenologica> specification = createSpecification(criteria);
        return faseFenologicaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FaseFenologica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FaseFenologica> findByCriteria(FaseFenologicaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FaseFenologica> specification = createSpecification(criteria);
        return faseFenologicaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FaseFenologicaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FaseFenologica> specification = createSpecification(criteria);
        return faseFenologicaRepository.count(specification);
    }

    /**
     * Function to convert {@link FaseFenologicaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FaseFenologica> createSpecification(FaseFenologicaCriteria criteria) {
        Specification<FaseFenologica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FaseFenologica_.id));
            }
            if (criteria.getCodStadioColt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodStadioColt(), FaseFenologica_.codStadioColt));
            }
            if (criteria.getDesStadioColt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesStadioColt(), FaseFenologica_.desStadioColt));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), FaseFenologica_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), FaseFenologica_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), FaseFenologica_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), FaseFenologica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), FaseFenologica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), FaseFenologica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), FaseFenologica_.userIdLastMod));
            }
            if (criteria.getFaseFenoToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getFaseFenoToRaccordoId(),
                    root -> root.join(FaseFenologica_.faseFenoToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
        }
        return specification;
    }
}
