package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcPio;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcPioRepository;
import com.terrambe.service.dto.RegOpcPioCriteria;

/**
 * Service for executing complex queries for {@link RegOpcPio} entities in the database.
 * The main input is a {@link RegOpcPioCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcPio} or a {@link Page} of {@link RegOpcPio} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcPioQueryService extends QueryService<RegOpcPio> {

    private final Logger log = LoggerFactory.getLogger(RegOpcPioQueryService.class);

    private final RegOpcPioRepository regOpcPioRepository;

    public RegOpcPioQueryService(RegOpcPioRepository regOpcPioRepository) {
        this.regOpcPioRepository = regOpcPioRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcPio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPio> findByCriteria(RegOpcPioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcPio> specification = createSpecification(criteria);
        return regOpcPioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcPio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcPio> findByCriteria(RegOpcPioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcPio> specification = createSpecification(criteria);
        return regOpcPioRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcPioCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcPio> specification = createSpecification(criteria);
        return regOpcPioRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcPioCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcPio> createSpecification(RegOpcPioCriteria criteria) {
        Specification<RegOpcPio> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcPio_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcPio_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcPio_.idUnitaProd));
            }
            if (criteria.getDataOperazione() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataOperazione(), RegOpcPio_.dataOperazione));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcPio_.idColtura));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcPio_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcPio_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcPio_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcPio_.userIdLastMod));
            }
            if (criteria.getOpcPioToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcPioToAppzId(),
                    root -> root.join(RegOpcPio_.opcPioToAppzs, JoinType.LEFT).get(RegOpcPioAppz_.id)));
            }
        }
        return specification;
    }
}
