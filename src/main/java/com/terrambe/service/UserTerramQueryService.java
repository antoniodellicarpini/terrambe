package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.UserTerram;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.UserTerramRepository;
import com.terrambe.service.dto.UserTerramCriteria;

/**
 * Service for executing complex queries for {@link UserTerram} entities in the database.
 * The main input is a {@link UserTerramCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserTerram} or a {@link Page} of {@link UserTerram} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserTerramQueryService extends QueryService<UserTerram> {

    private final Logger log = LoggerFactory.getLogger(UserTerramQueryService.class);

    private final UserTerramRepository userTerramRepository;

    public UserTerramQueryService(UserTerramRepository userTerramRepository) {
        this.userTerramRepository = userTerramRepository;
    }

    /**
     * Return a {@link List} of {@link UserTerram} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserTerram> findByCriteria(UserTerramCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserTerram> specification = createSpecification(criteria);
        return userTerramRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link UserTerram} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserTerram> findByCriteria(UserTerramCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserTerram> specification = createSpecification(criteria);
        return userTerramRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserTerramCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserTerram> specification = createSpecification(criteria);
        return userTerramRepository.count(specification);
    }

    /**
     * Function to convert {@link UserTerramCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserTerram> createSpecification(UserTerramCriteria criteria) {
        Specification<UserTerram> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserTerram_.id));
            }
            if (criteria.getTipoUtente() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoUtente(), UserTerram_.tipoUtente));
            }
            if (criteria.getIdJuser() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdJuser(), UserTerram_.idJuser));
            }
            if (criteria.getUsrTerToAziAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getUsrTerToAziAnaId(),
                    root -> root.join(UserTerram_.usrTerToAziAnas, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
        }
        return specification;
    }
}
