package com.terrambe.service;

import com.terrambe.domain.RecAziRap;
import com.terrambe.repository.RecAziRapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecAziRap}.
 */
@Service
@Transactional
public class RecAziRapService {

    private final Logger log = LoggerFactory.getLogger(RecAziRapService.class);

    private final RecAziRapRepository recAziRapRepository;

    public RecAziRapService(RecAziRapRepository recAziRapRepository) {
        this.recAziRapRepository = recAziRapRepository;
    }

    /**
     * Save a recAziRap.
     *
     * @param recAziRap the entity to save.
     * @return the persisted entity.
     */
    public RecAziRap save(RecAziRap recAziRap) {
        log.debug("Request to save RecAziRap : {}", recAziRap);
        return recAziRapRepository.save(recAziRap);
    }

    /**
     * Get all the recAziRaps.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziRap> findAll() {
        log.debug("Request to get all RecAziRaps");
        return recAziRapRepository.findAll();
    }


    /**
     * Get one recAziRap by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecAziRap> findOne(Long id) {
        log.debug("Request to get RecAziRap : {}", id);
        return recAziRapRepository.findById(id);
    }

    /**
     * Delete the recAziRap by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecAziRap : {}", id);
        recAziRapRepository.deleteById(id);
    }
}
