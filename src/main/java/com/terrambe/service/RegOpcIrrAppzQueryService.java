package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcIrrAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcIrrAppzRepository;
import com.terrambe.service.dto.RegOpcIrrAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcIrrAppz} entities in the database.
 * The main input is a {@link RegOpcIrrAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcIrrAppz} or a {@link Page} of {@link RegOpcIrrAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcIrrAppzQueryService extends QueryService<RegOpcIrrAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcIrrAppzQueryService.class);

    private final RegOpcIrrAppzRepository regOpcIrrAppzRepository;

    public RegOpcIrrAppzQueryService(RegOpcIrrAppzRepository regOpcIrrAppzRepository) {
        this.regOpcIrrAppzRepository = regOpcIrrAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcIrrAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcIrrAppz> findByCriteria(RegOpcIrrAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcIrrAppz> specification = createSpecification(criteria);
        return regOpcIrrAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcIrrAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcIrrAppz> findByCriteria(RegOpcIrrAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcIrrAppz> specification = createSpecification(criteria);
        return regOpcIrrAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcIrrAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcIrrAppz> specification = createSpecification(criteria);
        return regOpcIrrAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcIrrAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcIrrAppz> createSpecification(RegOpcIrrAppzCriteria criteria) {
        Specification<RegOpcIrrAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcIrrAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcIrrAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcIrrAppz_.idUnitaProd));
            }
            if (criteria.getPercentIrrigata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentIrrigata(), RegOpcIrrAppz_.percentIrrigata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcIrrAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcIrrAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcIrrAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcIrrAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcIrrAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcIrrId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcIrrId(),
                    root -> root.join(RegOpcIrrAppz_.appzToOpcIrr, JoinType.LEFT).get(RegOpcIrr_.id)));
            }
        }
        return specification;
    }
}
