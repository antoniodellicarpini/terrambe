package com.terrambe.service;

import com.terrambe.domain.OpePatentinoTipologia;
import com.terrambe.repository.OpePatentinoTipologiaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OpePatentinoTipologia}.
 */
@Service
@Transactional
public class OpePatentinoTipologiaService {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoTipologiaService.class);

    private final OpePatentinoTipologiaRepository opePatentinoTipologiaRepository;

    public OpePatentinoTipologiaService(OpePatentinoTipologiaRepository opePatentinoTipologiaRepository) {
        this.opePatentinoTipologiaRepository = opePatentinoTipologiaRepository;
    }

    /**
     * Save a opePatentinoTipologia.
     *
     * @param opePatentinoTipologia the entity to save.
     * @return the persisted entity.
     */
    public OpePatentinoTipologia save(OpePatentinoTipologia opePatentinoTipologia) {
        log.debug("Request to save OpePatentinoTipologia : {}", opePatentinoTipologia);
        return opePatentinoTipologiaRepository.save(opePatentinoTipologia);
    }

    /**
     * Get all the opePatentinoTipologias.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OpePatentinoTipologia> findAll() {
        log.debug("Request to get all OpePatentinoTipologias");
        return opePatentinoTipologiaRepository.findAll();
    }


    /**
     * Get one opePatentinoTipologia by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OpePatentinoTipologia> findOne(Long id) {
        log.debug("Request to get OpePatentinoTipologia : {}", id);
        return opePatentinoTipologiaRepository.findById(id);
    }

    /**
     * Delete the opePatentinoTipologia by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OpePatentinoTipologia : {}", id);
        opePatentinoTipologiaRepository.deleteById(id);
    }
}
