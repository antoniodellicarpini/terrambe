package com.terrambe.service;

import com.terrambe.domain.RegOpcConcAppz;
import com.terrambe.repository.RegOpcConcAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcConcAppz}.
 */
@Service
@Transactional
public class RegOpcConcAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcAppzService.class);

    private final RegOpcConcAppzRepository regOpcConcAppzRepository;

    public RegOpcConcAppzService(RegOpcConcAppzRepository regOpcConcAppzRepository) {
        this.regOpcConcAppzRepository = regOpcConcAppzRepository;
    }

    /**
     * Save a regOpcConcAppz.
     *
     * @param regOpcConcAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcConcAppz save(RegOpcConcAppz regOpcConcAppz) {
        log.debug("Request to save RegOpcConcAppz : {}", regOpcConcAppz);
        return regOpcConcAppzRepository.save(regOpcConcAppz);
    }

    /**
     * Get all the regOpcConcAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConcAppz> findAll() {
        log.debug("Request to get all RegOpcConcAppzs");
        return regOpcConcAppzRepository.findAll();
    }


    /**
     * Get one regOpcConcAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcConcAppz> findOne(Long id) {
        log.debug("Request to get RegOpcConcAppz : {}", id);
        return regOpcConcAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcConcAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcConcAppz : {}", id);
        regOpcConcAppzRepository.deleteById(id);
    }
}
