package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Sito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.SitoRepository;
import com.terrambe.service.dto.SitoCriteria;

/**
 * Service for executing complex queries for {@link Sito} entities in the database.
 * The main input is a {@link SitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Sito} or a {@link Page} of {@link Sito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SitoQueryService extends QueryService<Sito> {

    private final Logger log = LoggerFactory.getLogger(SitoQueryService.class);

    private final SitoRepository sitoRepository;

    public SitoQueryService(SitoRepository sitoRepository) {
        this.sitoRepository = sitoRepository;
    }

    /**
     * Return a {@link List} of {@link Sito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Sito> findByCriteria(SitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sito> specification = createSpecification(criteria);
        return sitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Sito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Sito> findByCriteria(SitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sito> specification = createSpecification(criteria);
        return sitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sito> specification = createSpecification(criteria);
        return sitoRepository.count(specification);
    }

    /**
     * Function to convert {@link SitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Sito> createSpecification(SitoCriteria criteria) {
        Specification<Sito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Sito_.id));
            }
            if (criteria.getCodSito() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodSito(), Sito_.codSito));
            }
            if (criteria.getDesSito() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesSito(), Sito_.desSito));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Sito_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Sito_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Sito_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Sito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Sito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Sito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Sito_.userIdLastMod));
            }
            if (criteria.getSitoToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getSitoToRaccordoId(),
                    root -> root.join(Sito_.sitoToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
        }
        return specification;
    }
}
