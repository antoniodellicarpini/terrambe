package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndNascForn;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndNascFornRepository;
import com.terrambe.service.dto.IndNascFornCriteria;

/**
 * Service for executing complex queries for {@link IndNascForn} entities in the database.
 * The main input is a {@link IndNascFornCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndNascForn} or a {@link Page} of {@link IndNascForn} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndNascFornQueryService extends QueryService<IndNascForn> {

    private final Logger log = LoggerFactory.getLogger(IndNascFornQueryService.class);

    private final IndNascFornRepository indNascFornRepository;

    public IndNascFornQueryService(IndNascFornRepository indNascFornRepository) {
        this.indNascFornRepository = indNascFornRepository;
    }

    /**
     * Return a {@link List} of {@link IndNascForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascForn> findByCriteria(IndNascFornCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndNascForn> specification = createSpecification(criteria);
        return indNascFornRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndNascForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndNascForn> findByCriteria(IndNascFornCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndNascForn> specification = createSpecification(criteria);
        return indNascFornRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndNascFornCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndNascForn> specification = createSpecification(criteria);
        return indNascFornRepository.count(specification);
    }

    /**
     * Function to convert {@link IndNascFornCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndNascForn> createSpecification(IndNascFornCriteria criteria) {
        Specification<IndNascForn> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndNascForn_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndNascForn_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndNascForn_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndNascForn_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndNascForn_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndNascForn_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndNascForn_.comune));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndNascForn_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndNascForn_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndNascForn_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndNascForn_.userIdLastMod));
            }
        }
        return specification;
    }
}
