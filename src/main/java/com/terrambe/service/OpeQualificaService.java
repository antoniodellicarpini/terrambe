package com.terrambe.service;

import com.terrambe.domain.OpeQualifica;
import com.terrambe.repository.OpeQualificaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OpeQualifica}.
 */
@Service
@Transactional
public class OpeQualificaService {

    private final Logger log = LoggerFactory.getLogger(OpeQualificaService.class);

    private final OpeQualificaRepository opeQualificaRepository;

    public OpeQualificaService(OpeQualificaRepository opeQualificaRepository) {
        this.opeQualificaRepository = opeQualificaRepository;
    }

    /**
     * Save a opeQualifica.
     *
     * @param opeQualifica the entity to save.
     * @return the persisted entity.
     */
    public OpeQualifica save(OpeQualifica opeQualifica) {
        log.debug("Request to save OpeQualifica : {}", opeQualifica);
        return opeQualificaRepository.save(opeQualifica);
    }

    /**
     * Get all the opeQualificas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OpeQualifica> findAll() {
        log.debug("Request to get all OpeQualificas");
        return opeQualificaRepository.findAll();
    }


    /**
     * Get one opeQualifica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OpeQualifica> findOne(Long id) {
        log.debug("Request to get OpeQualifica : {}", id);
        return opeQualificaRepository.findById(id);
    }

    /**
     * Delete the opeQualifica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OpeQualifica : {}", id);
        opeQualificaRepository.deleteById(id);
    }
}
