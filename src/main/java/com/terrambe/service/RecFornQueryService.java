package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecForn;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecFornRepository;
import com.terrambe.service.dto.RecFornCriteria;

/**
 * Service for executing complex queries for {@link RecForn} entities in the database.
 * The main input is a {@link RecFornCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecForn} or a {@link Page} of {@link RecForn} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecFornQueryService extends QueryService<RecForn> {

    private final Logger log = LoggerFactory.getLogger(RecFornQueryService.class);

    private final RecFornRepository recFornRepository;

    public RecFornQueryService(RecFornRepository recFornRepository) {
        this.recFornRepository = recFornRepository;
    }

    /**
     * Return a {@link List} of {@link RecForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecForn> findByCriteria(RecFornCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecForn> specification = createSpecification(criteria);
        return recFornRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecForn> findByCriteria(RecFornCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecForn> specification = createSpecification(criteria);
        return recFornRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecFornCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecForn> specification = createSpecification(criteria);
        return recFornRepository.count(specification);
    }

    /**
     * Function to convert {@link RecFornCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecForn> createSpecification(RecFornCriteria criteria) {
        Specification<RecForn> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecForn_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecForn_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecForn_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecForn_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecForn_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecForn_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecForn_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecForn_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecForn_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecForn_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecForn_.userIdLastMod));
            }
            if (criteria.getRecToFornId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToFornId(),
                    root -> root.join(RecForn_.recToForn, JoinType.LEFT).get(Fornitori_.id)));
            }
        }
        return specification;
    }
}
