package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcPrepterrAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcPrepterrAppzRepository;
import com.terrambe.service.dto.RegOpcPrepterrAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcPrepterrAppz} entities in the database.
 * The main input is a {@link RegOpcPrepterrAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcPrepterrAppz} or a {@link Page} of {@link RegOpcPrepterrAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcPrepterrAppzQueryService extends QueryService<RegOpcPrepterrAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrAppzQueryService.class);

    private final RegOpcPrepterrAppzRepository regOpcPrepterrAppzRepository;

    public RegOpcPrepterrAppzQueryService(RegOpcPrepterrAppzRepository regOpcPrepterrAppzRepository) {
        this.regOpcPrepterrAppzRepository = regOpcPrepterrAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcPrepterrAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPrepterrAppz> findByCriteria(RegOpcPrepterrAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcPrepterrAppz> specification = createSpecification(criteria);
        return regOpcPrepterrAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcPrepterrAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcPrepterrAppz> findByCriteria(RegOpcPrepterrAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcPrepterrAppz> specification = createSpecification(criteria);
        return regOpcPrepterrAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcPrepterrAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcPrepterrAppz> specification = createSpecification(criteria);
        return regOpcPrepterrAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcPrepterrAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcPrepterrAppz> createSpecification(RegOpcPrepterrAppzCriteria criteria) {
        Specification<RegOpcPrepterrAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcPrepterrAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcPrepterrAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcPrepterrAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcPrepterrAppz_.percentLavorata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcPrepterrAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcPrepterrAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcPrepterrAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcPrepterrAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcPrepterrAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcPrepterrId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcPrepterrId(),
                    root -> root.join(RegOpcPrepterrAppz_.appzToOpcPrepterr, JoinType.LEFT).get(RegOpcPrepterr_.id)));
            }
        }
        return specification;
    }
}
