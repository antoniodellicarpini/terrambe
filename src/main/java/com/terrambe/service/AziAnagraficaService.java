package com.terrambe.service;

import com.terrambe.domain.AziAnagrafica;
import com.terrambe.repository.AziAnagraficaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziAnagrafica}.
 */
@Service
@Transactional
public class AziAnagraficaService {

    private final Logger log = LoggerFactory.getLogger(AziAnagraficaService.class);

    private final AziAnagraficaRepository aziAnagraficaRepository;

    public AziAnagraficaService(AziAnagraficaRepository aziAnagraficaRepository) {
        this.aziAnagraficaRepository = aziAnagraficaRepository;
    }

    /**
     * Save a aziAnagrafica.
     *
     * @param aziAnagrafica the entity to save.
     * @return the persisted entity.
     */
    public AziAnagrafica save(AziAnagrafica aziAnagrafica) {
        log.debug("Request to save AziAnagrafica : {}", aziAnagrafica);
        return aziAnagraficaRepository.save(aziAnagrafica);
    }

    /**
     * Get all the aziAnagraficas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziAnagrafica> findAll() {
        log.debug("Request to get all AziAnagraficas");
        return aziAnagraficaRepository.findAll();
    }


    /**
     * Get one aziAnagrafica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziAnagrafica> findOne(Long id) {
        log.debug("Request to get AziAnagrafica : {}", id);
        return aziAnagraficaRepository.findById(id);
    }

    /**
     * Delete the aziAnagrafica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziAnagrafica : {}", id);
        aziAnagraficaRepository.deleteById(id);
    }
}
