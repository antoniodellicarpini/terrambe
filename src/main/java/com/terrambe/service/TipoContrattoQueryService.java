package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoContratto;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoContrattoRepository;
import com.terrambe.service.dto.TipoContrattoCriteria;

/**
 * Service for executing complex queries for {@link TipoContratto} entities in the database.
 * The main input is a {@link TipoContrattoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoContratto} or a {@link Page} of {@link TipoContratto} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoContrattoQueryService extends QueryService<TipoContratto> {

    private final Logger log = LoggerFactory.getLogger(TipoContrattoQueryService.class);

    private final TipoContrattoRepository tipoContrattoRepository;

    public TipoContrattoQueryService(TipoContrattoRepository tipoContrattoRepository) {
        this.tipoContrattoRepository = tipoContrattoRepository;
    }

    /**
     * Return a {@link List} of {@link TipoContratto} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoContratto> findByCriteria(TipoContrattoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoContratto> specification = createSpecification(criteria);
        return tipoContrattoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoContratto} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoContratto> findByCriteria(TipoContrattoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoContratto> specification = createSpecification(criteria);
        return tipoContrattoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoContrattoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoContratto> specification = createSpecification(criteria);
        return tipoContrattoRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoContrattoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoContratto> createSpecification(TipoContrattoCriteria criteria) {
        Specification<TipoContratto> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoContratto_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoContratto_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoContratto_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoContratto_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoContratto_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoContratto_.userIdLastMod));
            }
            if (criteria.getTipoContratToOpeId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoContratToOpeId(),
                    root -> root.join(TipoContratto_.tipoContratToOpes, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
