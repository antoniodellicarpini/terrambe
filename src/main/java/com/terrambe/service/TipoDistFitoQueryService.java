package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoDistFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoDistFitoRepository;
import com.terrambe.service.dto.TipoDistFitoCriteria;

/**
 * Service for executing complex queries for {@link TipoDistFito} entities in the database.
 * The main input is a {@link TipoDistFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoDistFito} or a {@link Page} of {@link TipoDistFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoDistFitoQueryService extends QueryService<TipoDistFito> {

    private final Logger log = LoggerFactory.getLogger(TipoDistFitoQueryService.class);

    private final TipoDistFitoRepository tipoDistFitoRepository;

    public TipoDistFitoQueryService(TipoDistFitoRepository tipoDistFitoRepository) {
        this.tipoDistFitoRepository = tipoDistFitoRepository;
    }

    /**
     * Return a {@link List} of {@link TipoDistFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoDistFito> findByCriteria(TipoDistFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoDistFito> specification = createSpecification(criteria);
        return tipoDistFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoDistFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoDistFito> findByCriteria(TipoDistFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoDistFito> specification = createSpecification(criteria);
        return tipoDistFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoDistFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoDistFito> specification = createSpecification(criteria);
        return tipoDistFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoDistFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoDistFito> createSpecification(TipoDistFitoCriteria criteria) {
        Specification<TipoDistFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoDistFito_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoDistFito_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoDistFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoDistFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoDistFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoDistFito_.userIdLastMod));
            }
        }
        return specification;
    }
}
