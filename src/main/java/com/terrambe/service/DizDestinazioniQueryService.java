package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DizDestinazioni;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DizDestinazioniRepository;
import com.terrambe.service.dto.DizDestinazioniCriteria;

/**
 * Service for executing complex queries for {@link DizDestinazioni} entities in the database.
 * The main input is a {@link DizDestinazioniCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DizDestinazioni} or a {@link Page} of {@link DizDestinazioni} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DizDestinazioniQueryService extends QueryService<DizDestinazioni> {

    private final Logger log = LoggerFactory.getLogger(DizDestinazioniQueryService.class);

    private final DizDestinazioniRepository dizDestinazioniRepository;

    public DizDestinazioniQueryService(DizDestinazioniRepository dizDestinazioniRepository) {
        this.dizDestinazioniRepository = dizDestinazioniRepository;
    }

    /**
     * Return a {@link List} of {@link DizDestinazioni} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DizDestinazioni> findByCriteria(DizDestinazioniCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DizDestinazioni> specification = createSpecification(criteria);
        return dizDestinazioniRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DizDestinazioni} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DizDestinazioni> findByCriteria(DizDestinazioniCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DizDestinazioni> specification = createSpecification(criteria);
        return dizDestinazioniRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DizDestinazioniCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DizDestinazioni> specification = createSpecification(criteria);
        return dizDestinazioniRepository.count(specification);
    }

    /**
     * Function to convert {@link DizDestinazioniCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DizDestinazioni> createSpecification(DizDestinazioniCriteria criteria) {
        Specification<DizDestinazioni> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DizDestinazioni_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), DizDestinazioni_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), DizDestinazioni_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), DizDestinazioni_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), DizDestinazioni_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), DizDestinazioni_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), DizDestinazioni_.userIdLastMod));
            }
        }
        return specification;
    }
}
