package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Campi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.CampiRepository;
import com.terrambe.service.dto.CampiCriteria;

/**
 * Service for executing complex queries for {@link Campi} entities in the database.
 * The main input is a {@link CampiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Campi} or a {@link Page} of {@link Campi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampiQueryService extends QueryService<Campi> {

    private final Logger log = LoggerFactory.getLogger(CampiQueryService.class);

    private final CampiRepository campiRepository;

    public CampiQueryService(CampiRepository campiRepository) {
        this.campiRepository = campiRepository;
    }

    /**
     * Return a {@link List} of {@link Campi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Campi> findByCriteria(CampiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Campi> specification = createSpecification(criteria);
        return campiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Campi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Campi> findByCriteria(CampiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Campi> specification = createSpecification(criteria);
        return campiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CampiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Campi> specification = createSpecification(criteria);
        return campiRepository.count(specification);
    }

    /**
     * Function to convert {@link CampiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Campi> createSpecification(CampiCriteria criteria) {
        Specification<Campi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Campi_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), Campi_.idAzienda));
            }
            if (criteria.getNomeCampo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeCampo(), Campi_.nomeCampo));
            }
            if (criteria.getSuperficieUtilizzata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSuperficieUtilizzata(), Campi_.superficieUtilizzata));
            }
            if (criteria.getSupCatastale() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSupCatastale(), Campi_.supCatastale));
            }
            if (criteria.getSupUtilizzabile() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSupUtilizzabile(), Campi_.supUtilizzabile));
            }
            if (criteria.getUuidEsri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuidEsri(), Campi_.uuidEsri));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Campi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Campi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Campi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Campi_.userIdLastMod));
            }
            if (criteria.getCampiToAziSupId() != null) {
                specification = specification.and(buildSpecification(criteria.getCampiToAziSupId(),
                    root -> root.join(Campi_.campiToAziSups, JoinType.LEFT).get(AziSuperficieParticella_.id)));
            }
            if (criteria.getCampiToAppezId() != null) {
                specification = specification.and(buildSpecification(criteria.getCampiToAppezId(),
                    root -> root.join(Campi_.campiToAppezs, JoinType.LEFT).get(Appezzamenti_.id)));
            }
            if (criteria.getCampiuniId() != null) {
                specification = specification.and(buildSpecification(criteria.getCampiuniId(),
                    root -> root.join(Campi_.campiuni, JoinType.LEFT).get(UniAnagrafica_.id)));
            }
        }
        return specification;
    }
}
