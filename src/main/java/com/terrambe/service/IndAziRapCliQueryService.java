package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndAziRapCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndAziRapCliRepository;
import com.terrambe.service.dto.IndAziRapCliCriteria;

/**
 * Service for executing complex queries for {@link IndAziRapCli} entities in the database.
 * The main input is a {@link IndAziRapCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndAziRapCli} or a {@link Page} of {@link IndAziRapCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndAziRapCliQueryService extends QueryService<IndAziRapCli> {

    private final Logger log = LoggerFactory.getLogger(IndAziRapCliQueryService.class);

    private final IndAziRapCliRepository indAziRapCliRepository;

    public IndAziRapCliQueryService(IndAziRapCliRepository indAziRapCliRepository) {
        this.indAziRapCliRepository = indAziRapCliRepository;
    }

    /**
     * Return a {@link List} of {@link IndAziRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziRapCli> findByCriteria(IndAziRapCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndAziRapCli> specification = createSpecification(criteria);
        return indAziRapCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndAziRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndAziRapCli> findByCriteria(IndAziRapCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndAziRapCli> specification = createSpecification(criteria);
        return indAziRapCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndAziRapCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndAziRapCli> specification = createSpecification(criteria);
        return indAziRapCliRepository.count(specification);
    }

    /**
     * Function to convert {@link IndAziRapCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndAziRapCli> createSpecification(IndAziRapCliCriteria criteria) {
        Specification<IndAziRapCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndAziRapCli_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndAziRapCli_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndAziRapCli_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndAziRapCli_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndAziRapCli_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndAziRapCli_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndAziRapCli_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndAziRapCli_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndAziRapCli_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndAziRapCli_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndAziRapCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndAziRapCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndAziRapCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndAziRapCli_.userIdLastMod));
            }
            if (criteria.getIndToCliRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToCliRapId(),
                    root -> root.join(IndAziRapCli_.indToCliRap, JoinType.LEFT).get(AziRappresentanteCli_.id)));
            }
        }
        return specification;
    }
}
