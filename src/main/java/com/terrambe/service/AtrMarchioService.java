package com.terrambe.service;

import com.terrambe.domain.AtrMarchio;
import com.terrambe.repository.AtrMarchioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AtrMarchio}.
 */
@Service
@Transactional
public class AtrMarchioService {

    private final Logger log = LoggerFactory.getLogger(AtrMarchioService.class);

    private final AtrMarchioRepository atrMarchioRepository;

    public AtrMarchioService(AtrMarchioRepository atrMarchioRepository) {
        this.atrMarchioRepository = atrMarchioRepository;
    }

    /**
     * Save a atrMarchio.
     *
     * @param atrMarchio the entity to save.
     * @return the persisted entity.
     */
    public AtrMarchio save(AtrMarchio atrMarchio) {
        log.debug("Request to save AtrMarchio : {}", atrMarchio);
        return atrMarchioRepository.save(atrMarchio);
    }

    /**
     * Get all the atrMarchios.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AtrMarchio> findAll() {
        log.debug("Request to get all AtrMarchios");
        return atrMarchioRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the atrMarchios with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<AtrMarchio> findAllWithEagerRelationships(Pageable pageable) {
        return atrMarchioRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one atrMarchio by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AtrMarchio> findOne(Long id) {
        log.debug("Request to get AtrMarchio : {}", id);
        return atrMarchioRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the atrMarchio by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AtrMarchio : {}", id);
        atrMarchioRepository.deleteById(id);
    }
}
