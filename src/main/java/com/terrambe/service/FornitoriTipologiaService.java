package com.terrambe.service;

import com.terrambe.domain.FornitoriTipologia;
import com.terrambe.repository.FornitoriTipologiaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FornitoriTipologia}.
 */
@Service
@Transactional
public class FornitoriTipologiaService {

    private final Logger log = LoggerFactory.getLogger(FornitoriTipologiaService.class);

    private final FornitoriTipologiaRepository fornitoriTipologiaRepository;

    public FornitoriTipologiaService(FornitoriTipologiaRepository fornitoriTipologiaRepository) {
        this.fornitoriTipologiaRepository = fornitoriTipologiaRepository;
    }

    /**
     * Save a fornitoriTipologia.
     *
     * @param fornitoriTipologia the entity to save.
     * @return the persisted entity.
     */
    public FornitoriTipologia save(FornitoriTipologia fornitoriTipologia) {
        log.debug("Request to save FornitoriTipologia : {}", fornitoriTipologia);
        return fornitoriTipologiaRepository.save(fornitoriTipologia);
    }

    /**
     * Get all the fornitoriTipologias.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FornitoriTipologia> findAll() {
        log.debug("Request to get all FornitoriTipologias");
        return fornitoriTipologiaRepository.findAll();
    }


    /**
     * Get one fornitoriTipologia by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FornitoriTipologia> findOne(Long id) {
        log.debug("Request to get FornitoriTipologia : {}", id);
        return fornitoriTipologiaRepository.findById(id);
    }

    /**
     * Delete the fornitoriTipologia by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FornitoriTipologia : {}", id);
        fornitoriTipologiaRepository.deleteById(id);
    }
}
