package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.VincoliTerritoriali;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.VincoliTerritorialiRepository;
import com.terrambe.service.dto.VincoliTerritorialiCriteria;

/**
 * Service for executing complex queries for {@link VincoliTerritoriali} entities in the database.
 * The main input is a {@link VincoliTerritorialiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VincoliTerritoriali} or a {@link Page} of {@link VincoliTerritoriali} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VincoliTerritorialiQueryService extends QueryService<VincoliTerritoriali> {

    private final Logger log = LoggerFactory.getLogger(VincoliTerritorialiQueryService.class);

    private final VincoliTerritorialiRepository vincoliTerritorialiRepository;

    public VincoliTerritorialiQueryService(VincoliTerritorialiRepository vincoliTerritorialiRepository) {
        this.vincoliTerritorialiRepository = vincoliTerritorialiRepository;
    }

    /**
     * Return a {@link List} of {@link VincoliTerritoriali} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VincoliTerritoriali> findByCriteria(VincoliTerritorialiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<VincoliTerritoriali> specification = createSpecification(criteria);
        return vincoliTerritorialiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link VincoliTerritoriali} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VincoliTerritoriali> findByCriteria(VincoliTerritorialiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<VincoliTerritoriali> specification = createSpecification(criteria);
        return vincoliTerritorialiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(VincoliTerritorialiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<VincoliTerritoriali> specification = createSpecification(criteria);
        return vincoliTerritorialiRepository.count(specification);
    }

    /**
     * Function to convert {@link VincoliTerritorialiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<VincoliTerritoriali> createSpecification(VincoliTerritorialiCriteria criteria) {
        Specification<VincoliTerritoriali> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), VincoliTerritoriali_.id));
            }
            if (criteria.getFlagAttivo() != null) {
                specification = specification.and(buildSpecification(criteria.getFlagAttivo(), VincoliTerritoriali_.flagAttivo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), VincoliTerritoriali_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), VincoliTerritoriali_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), VincoliTerritoriali_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), VincoliTerritoriali_.userIdLastMod));
            }
        }
        return specification;
    }
}
