package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.MotivoConc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.MotivoConcRepository;
import com.terrambe.service.dto.MotivoConcCriteria;

/**
 * Service for executing complex queries for {@link MotivoConc} entities in the database.
 * The main input is a {@link MotivoConcCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MotivoConc} or a {@link Page} of {@link MotivoConc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MotivoConcQueryService extends QueryService<MotivoConc> {

    private final Logger log = LoggerFactory.getLogger(MotivoConcQueryService.class);

    private final MotivoConcRepository motivoConcRepository;

    public MotivoConcQueryService(MotivoConcRepository motivoConcRepository) {
        this.motivoConcRepository = motivoConcRepository;
    }

    /**
     * Return a {@link List} of {@link MotivoConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoConc> findByCriteria(MotivoConcCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MotivoConc> specification = createSpecification(criteria);
        return motivoConcRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MotivoConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MotivoConc> findByCriteria(MotivoConcCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MotivoConc> specification = createSpecification(criteria);
        return motivoConcRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MotivoConcCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MotivoConc> specification = createSpecification(criteria);
        return motivoConcRepository.count(specification);
    }

    /**
     * Function to convert {@link MotivoConcCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MotivoConc> createSpecification(MotivoConcCriteria criteria) {
        Specification<MotivoConc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MotivoConc_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), MotivoConc_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), MotivoConc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), MotivoConc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), MotivoConc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), MotivoConc_.userIdLastMod));
            }
            if (criteria.getMotivoToOpcConcId() != null) {
                specification = specification.and(buildSpecification(criteria.getMotivoToOpcConcId(),
                    root -> root.join(MotivoConc_.motivoToOpcConcs, JoinType.LEFT).get(RegOpcConc_.id)));
            }
        }
        return specification;
    }
}
