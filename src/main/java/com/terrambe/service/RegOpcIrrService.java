package com.terrambe.service;

import com.terrambe.domain.RegOpcIrr;
import com.terrambe.repository.RegOpcIrrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcIrr}.
 */
@Service
@Transactional
public class RegOpcIrrService {

    private final Logger log = LoggerFactory.getLogger(RegOpcIrrService.class);

    private final RegOpcIrrRepository regOpcIrrRepository;

    public RegOpcIrrService(RegOpcIrrRepository regOpcIrrRepository) {
        this.regOpcIrrRepository = regOpcIrrRepository;
    }

    /**
     * Save a regOpcIrr.
     *
     * @param regOpcIrr the entity to save.
     * @return the persisted entity.
     */
    public RegOpcIrr save(RegOpcIrr regOpcIrr) {
        log.debug("Request to save RegOpcIrr : {}", regOpcIrr);
        return regOpcIrrRepository.save(regOpcIrr);
    }

    /**
     * Get all the regOpcIrrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcIrr> findAll() {
        log.debug("Request to get all RegOpcIrrs");
        return regOpcIrrRepository.findAll();
    }


    /**
     * Get one regOpcIrr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcIrr> findOne(Long id) {
        log.debug("Request to get RegOpcIrr : {}", id);
        return regOpcIrrRepository.findById(id);
    }

    /**
     * Delete the regOpcIrr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcIrr : {}", id);
        regOpcIrrRepository.deleteById(id);
    }
}
