package com.terrambe.service;

import com.terrambe.domain.RegOpcTrap;
import com.terrambe.repository.RegOpcTrapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcTrap}.
 */
@Service
@Transactional
public class RegOpcTrapService {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapService.class);

    private final RegOpcTrapRepository regOpcTrapRepository;

    public RegOpcTrapService(RegOpcTrapRepository regOpcTrapRepository) {
        this.regOpcTrapRepository = regOpcTrapRepository;
    }

    /**
     * Save a regOpcTrap.
     *
     * @param regOpcTrap the entity to save.
     * @return the persisted entity.
     */
    public RegOpcTrap save(RegOpcTrap regOpcTrap) {
        log.debug("Request to save RegOpcTrap : {}", regOpcTrap);
        return regOpcTrapRepository.save(regOpcTrap);
    }

    /**
     * Get all the regOpcTraps.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrap> findAll() {
        log.debug("Request to get all RegOpcTraps");
        return regOpcTrapRepository.findAll();
    }


    /**
     * Get one regOpcTrap by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcTrap> findOne(Long id) {
        log.debug("Request to get RegOpcTrap : {}", id);
        return regOpcTrapRepository.findById(id);
    }

    /**
     * Delete the regOpcTrap by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcTrap : {}", id);
        regOpcTrapRepository.deleteById(id);
    }
}
