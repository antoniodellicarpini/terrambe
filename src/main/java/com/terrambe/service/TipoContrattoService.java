package com.terrambe.service;

import com.terrambe.domain.TipoContratto;
import com.terrambe.repository.TipoContrattoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoContratto}.
 */
@Service
@Transactional
public class TipoContrattoService {

    private final Logger log = LoggerFactory.getLogger(TipoContrattoService.class);

    private final TipoContrattoRepository tipoContrattoRepository;

    public TipoContrattoService(TipoContrattoRepository tipoContrattoRepository) {
        this.tipoContrattoRepository = tipoContrattoRepository;
    }

    /**
     * Save a tipoContratto.
     *
     * @param tipoContratto the entity to save.
     * @return the persisted entity.
     */
    public TipoContratto save(TipoContratto tipoContratto) {
        log.debug("Request to save TipoContratto : {}", tipoContratto);
        return tipoContrattoRepository.save(tipoContratto);
    }

    /**
     * Get all the tipoContrattos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoContratto> findAll() {
        log.debug("Request to get all TipoContrattos");
        return tipoContrattoRepository.findAll();
    }


    /**
     * Get one tipoContratto by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoContratto> findOne(Long id) {
        log.debug("Request to get TipoContratto : {}", id);
        return tipoContrattoRepository.findById(id);
    }

    /**
     * Delete the tipoContratto by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoContratto : {}", id);
        tipoContrattoRepository.deleteById(id);
    }
}
