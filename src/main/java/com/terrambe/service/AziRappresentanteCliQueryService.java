package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziRappresentanteCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziRappresentanteCliRepository;
import com.terrambe.service.dto.AziRappresentanteCliCriteria;

/**
 * Service for executing complex queries for {@link AziRappresentanteCli} entities in the database.
 * The main input is a {@link AziRappresentanteCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziRappresentanteCli} or a {@link Page} of {@link AziRappresentanteCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziRappresentanteCliQueryService extends QueryService<AziRappresentanteCli> {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteCliQueryService.class);

    private final AziRappresentanteCliRepository aziRappresentanteCliRepository;

    public AziRappresentanteCliQueryService(AziRappresentanteCliRepository aziRappresentanteCliRepository) {
        this.aziRappresentanteCliRepository = aziRappresentanteCliRepository;
    }

    /**
     * Return a {@link List} of {@link AziRappresentanteCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziRappresentanteCli> findByCriteria(AziRappresentanteCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziRappresentanteCli> specification = createSpecification(criteria);
        return aziRappresentanteCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziRappresentanteCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziRappresentanteCli> findByCriteria(AziRappresentanteCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziRappresentanteCli> specification = createSpecification(criteria);
        return aziRappresentanteCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziRappresentanteCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziRappresentanteCli> specification = createSpecification(criteria);
        return aziRappresentanteCliRepository.count(specification);
    }

    /**
     * Function to convert {@link AziRappresentanteCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziRappresentanteCli> createSpecification(AziRappresentanteCliCriteria criteria) {
        Specification<AziRappresentanteCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziRappresentanteCli_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), AziRappresentanteCli_.nome));
            }
            if (criteria.getCognome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCognome(), AziRappresentanteCli_.cognome));
            }
            if (criteria.getSesso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSesso(), AziRappresentanteCli_.sesso));
            }
            if (criteria.getDataNascita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataNascita(), AziRappresentanteCli_.dataNascita));
            }
            if (criteria.getCodiceFiscale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceFiscale(), AziRappresentanteCli_.codiceFiscale));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziRappresentanteCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziRappresentanteCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziRappresentanteCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziRappresentanteCli_.userIdLastMod));
            }
            if (criteria.getCliRapToIndNascId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliRapToIndNascId(),
                    root -> root.join(AziRappresentanteCli_.cliRapToIndNasc, JoinType.LEFT).get(IndNascRapCli_.id)));
            }
            if (criteria.getCliRapToCliAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliRapToCliAnaId(),
                    root -> root.join(AziRappresentanteCli_.cliRapToCliAnas, JoinType.LEFT).get(Cliente_.id)));
            }
            if (criteria.getCliRapToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliRapToIndId(),
                    root -> root.join(AziRappresentanteCli_.cliRapToInds, JoinType.LEFT).get(IndAziRapCli_.id)));
            }
            if (criteria.getCliRapToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliRapToRecId(),
                    root -> root.join(AziRappresentanteCli_.cliRapToRecs, JoinType.LEFT).get(RecAziRapCli_.id)));
            }
        }
        return specification;
    }
}
