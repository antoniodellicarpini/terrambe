package com.terrambe.service;

import com.terrambe.domain.TipoIrr;
import com.terrambe.repository.TipoIrrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoIrr}.
 */
@Service
@Transactional
public class TipoIrrService {

    private final Logger log = LoggerFactory.getLogger(TipoIrrService.class);

    private final TipoIrrRepository tipoIrrRepository;

    public TipoIrrService(TipoIrrRepository tipoIrrRepository) {
        this.tipoIrrRepository = tipoIrrRepository;
    }

    /**
     * Save a tipoIrr.
     *
     * @param tipoIrr the entity to save.
     * @return the persisted entity.
     */
    public TipoIrr save(TipoIrr tipoIrr) {
        log.debug("Request to save TipoIrr : {}", tipoIrr);
        return tipoIrrRepository.save(tipoIrr);
    }

    /**
     * Get all the tipoIrrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoIrr> findAll() {
        log.debug("Request to get all TipoIrrs");
        return tipoIrrRepository.findAll();
    }


    /**
     * Get one tipoIrr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoIrr> findOne(Long id) {
        log.debug("Request to get TipoIrr : {}", id);
        return tipoIrrRepository.findById(id);
    }

    /**
     * Delete the tipoIrr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoIrr : {}", id);
        tipoIrrRepository.deleteById(id);
    }
}
