package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcSemina;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcSeminaRepository;
import com.terrambe.service.dto.RegOpcSeminaCriteria;

/**
 * Service for executing complex queries for {@link RegOpcSemina} entities in the database.
 * The main input is a {@link RegOpcSeminaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcSemina} or a {@link Page} of {@link RegOpcSemina} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcSeminaQueryService extends QueryService<RegOpcSemina> {

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaQueryService.class);

    private final RegOpcSeminaRepository regOpcSeminaRepository;

    public RegOpcSeminaQueryService(RegOpcSeminaRepository regOpcSeminaRepository) {
        this.regOpcSeminaRepository = regOpcSeminaRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcSemina} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcSemina> findByCriteria(RegOpcSeminaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcSemina> specification = createSpecification(criteria);
        return regOpcSeminaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcSemina} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcSemina> findByCriteria(RegOpcSeminaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcSemina> specification = createSpecification(criteria);
        return regOpcSeminaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcSeminaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcSemina> specification = createSpecification(criteria);
        return regOpcSeminaRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcSeminaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcSemina> createSpecification(RegOpcSeminaCriteria criteria) {
        Specification<RegOpcSemina> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcSemina_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcSemina_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcSemina_.idUnitaProd));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcSemina_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcSemina_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcSemina_.tempoImpiegato));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcSemina_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcSemina_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcSemina_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcSemina_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcSemina_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcSemina_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcSemina_.userIdLastMod));
            }
            if (criteria.getOpcSeminaToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcSeminaToAppzId(),
                    root -> root.join(RegOpcSemina_.opcSeminaToAppzs, JoinType.LEFT).get(RegOpcSeminaAppz_.id)));
            }
            if (criteria.getOpcSeminaToTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcSeminaToTipoId(),
                    root -> root.join(RegOpcSemina_.opcSeminaToTipo, JoinType.LEFT).get(TipoSemina_.id)));
            }
        }
        return specification;
    }
}
