package com.terrambe.service;

import com.terrambe.domain.DizUsi;
import com.terrambe.repository.DizUsiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizUsi}.
 */
@Service
@Transactional
public class DizUsiService {

    private final Logger log = LoggerFactory.getLogger(DizUsiService.class);

    private final DizUsiRepository dizUsiRepository;

    public DizUsiService(DizUsiRepository dizUsiRepository) {
        this.dizUsiRepository = dizUsiRepository;
    }

    /**
     * Save a dizUsi.
     *
     * @param dizUsi the entity to save.
     * @return the persisted entity.
     */
    public DizUsi save(DizUsi dizUsi) {
        log.debug("Request to save DizUsi : {}", dizUsi);
        return dizUsiRepository.save(dizUsi);
    }

    /**
     * Get all the dizUsis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizUsi> findAll() {
        log.debug("Request to get all DizUsis");
        return dizUsiRepository.findAll();
    }


    /**
     * Get one dizUsi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizUsi> findOne(Long id) {
        log.debug("Request to get DizUsi : {}", id);
        return dizUsiRepository.findById(id);
    }

    /**
     * Delete the dizUsi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizUsi : {}", id);
        dizUsiRepository.deleteById(id);
    }
}
