package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.FornitoriTipologia;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FornitoriTipologiaRepository;
import com.terrambe.service.dto.FornitoriTipologiaCriteria;

/**
 * Service for executing complex queries for {@link FornitoriTipologia} entities in the database.
 * The main input is a {@link FornitoriTipologiaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FornitoriTipologia} or a {@link Page} of {@link FornitoriTipologia} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FornitoriTipologiaQueryService extends QueryService<FornitoriTipologia> {

    private final Logger log = LoggerFactory.getLogger(FornitoriTipologiaQueryService.class);

    private final FornitoriTipologiaRepository fornitoriTipologiaRepository;

    public FornitoriTipologiaQueryService(FornitoriTipologiaRepository fornitoriTipologiaRepository) {
        this.fornitoriTipologiaRepository = fornitoriTipologiaRepository;
    }

    /**
     * Return a {@link List} of {@link FornitoriTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FornitoriTipologia> findByCriteria(FornitoriTipologiaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FornitoriTipologia> specification = createSpecification(criteria);
        return fornitoriTipologiaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FornitoriTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FornitoriTipologia> findByCriteria(FornitoriTipologiaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FornitoriTipologia> specification = createSpecification(criteria);
        return fornitoriTipologiaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FornitoriTipologiaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FornitoriTipologia> specification = createSpecification(criteria);
        return fornitoriTipologiaRepository.count(specification);
    }

    /**
     * Function to convert {@link FornitoriTipologiaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FornitoriTipologia> createSpecification(FornitoriTipologiaCriteria criteria) {
        Specification<FornitoriTipologia> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FornitoriTipologia_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), FornitoriTipologia_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), FornitoriTipologia_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), FornitoriTipologia_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), FornitoriTipologia_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), FornitoriTipologia_.userIdLastMod));
            }
            if (criteria.getFornitoriId() != null) {
                specification = specification.and(buildSpecification(criteria.getFornitoriId(),
                    root -> root.join(FornitoriTipologia_.fornitoris, JoinType.LEFT).get(Fornitori_.id)));
            }
        }
        return specification;
    }
}
