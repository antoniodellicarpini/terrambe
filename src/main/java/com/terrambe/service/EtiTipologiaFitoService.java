package com.terrambe.service;

import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.repository.EtiTipologiaFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EtiTipologiaFito}.
 */
@Service
@Transactional
public class EtiTipologiaFitoService {

    private final Logger log = LoggerFactory.getLogger(EtiTipologiaFitoService.class);

    private final EtiTipologiaFitoRepository etiTipologiaFitoRepository;

    public EtiTipologiaFitoService(EtiTipologiaFitoRepository etiTipologiaFitoRepository) {
        this.etiTipologiaFitoRepository = etiTipologiaFitoRepository;
    }

    /**
     * Save a etiTipologiaFito.
     *
     * @param etiTipologiaFito the entity to save.
     * @return the persisted entity.
     */
    public EtiTipologiaFito save(EtiTipologiaFito etiTipologiaFito) {
        log.debug("Request to save EtiTipologiaFito : {}", etiTipologiaFito);
        return etiTipologiaFitoRepository.save(etiTipologiaFito);
    }

    /**
     * Get all the etiTipologiaFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EtiTipologiaFito> findAll() {
        log.debug("Request to get all EtiTipologiaFitos");
        return etiTipologiaFitoRepository.findAll();
    }


    /**
     * Get one etiTipologiaFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EtiTipologiaFito> findOne(Long id) {
        log.debug("Request to get EtiTipologiaFito : {}", id);
        return etiTipologiaFitoRepository.findById(id);
    }

    /**
     * Delete the etiTipologiaFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EtiTipologiaFito : {}", id);
        etiTipologiaFitoRepository.deleteById(id);
    }
}
