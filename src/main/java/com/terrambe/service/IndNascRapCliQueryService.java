package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndNascRapCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndNascRapCliRepository;
import com.terrambe.service.dto.IndNascRapCliCriteria;

/**
 * Service for executing complex queries for {@link IndNascRapCli} entities in the database.
 * The main input is a {@link IndNascRapCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndNascRapCli} or a {@link Page} of {@link IndNascRapCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndNascRapCliQueryService extends QueryService<IndNascRapCli> {

    private final Logger log = LoggerFactory.getLogger(IndNascRapCliQueryService.class);

    private final IndNascRapCliRepository indNascRapCliRepository;

    public IndNascRapCliQueryService(IndNascRapCliRepository indNascRapCliRepository) {
        this.indNascRapCliRepository = indNascRapCliRepository;
    }

    /**
     * Return a {@link List} of {@link IndNascRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascRapCli> findByCriteria(IndNascRapCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndNascRapCli> specification = createSpecification(criteria);
        return indNascRapCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndNascRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndNascRapCli> findByCriteria(IndNascRapCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndNascRapCli> specification = createSpecification(criteria);
        return indNascRapCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndNascRapCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndNascRapCli> specification = createSpecification(criteria);
        return indNascRapCliRepository.count(specification);
    }

    /**
     * Function to convert {@link IndNascRapCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndNascRapCli> createSpecification(IndNascRapCliCriteria criteria) {
        Specification<IndNascRapCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndNascRapCli_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndNascRapCli_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndNascRapCli_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndNascRapCli_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndNascRapCli_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndNascRapCli_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndNascRapCli_.comune));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndNascRapCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndNascRapCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndNascRapCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndNascRapCli_.userIdLastMod));
            }
            if (criteria.getIndNascToCliRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndNascToCliRapId(),
                    root -> root.join(IndNascRapCli_.indNascToCliRap, JoinType.LEFT).get(AziRappresentanteCli_.id)));
            }
        }
        return specification;
    }
}
