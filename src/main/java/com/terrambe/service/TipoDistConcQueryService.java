package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoDistConc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoDistConcRepository;
import com.terrambe.service.dto.TipoDistConcCriteria;

/**
 * Service for executing complex queries for {@link TipoDistConc} entities in the database.
 * The main input is a {@link TipoDistConcCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoDistConc} or a {@link Page} of {@link TipoDistConc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoDistConcQueryService extends QueryService<TipoDistConc> {

    private final Logger log = LoggerFactory.getLogger(TipoDistConcQueryService.class);

    private final TipoDistConcRepository tipoDistConcRepository;

    public TipoDistConcQueryService(TipoDistConcRepository tipoDistConcRepository) {
        this.tipoDistConcRepository = tipoDistConcRepository;
    }

    /**
     * Return a {@link List} of {@link TipoDistConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoDistConc> findByCriteria(TipoDistConcCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoDistConc> specification = createSpecification(criteria);
        return tipoDistConcRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoDistConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoDistConc> findByCriteria(TipoDistConcCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoDistConc> specification = createSpecification(criteria);
        return tipoDistConcRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoDistConcCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoDistConc> specification = createSpecification(criteria);
        return tipoDistConcRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoDistConcCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoDistConc> createSpecification(TipoDistConcCriteria criteria) {
        Specification<TipoDistConc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoDistConc_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoDistConc_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoDistConc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoDistConc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoDistConc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoDistConc_.userIdLastMod));
            }
            if (criteria.getTipoDistToOpcConcId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoDistToOpcConcId(),
                    root -> root.join(TipoDistConc_.tipoDistToOpcConcs, JoinType.LEFT).get(RegOpcConc_.id)));
            }
            if (criteria.getTipoDistrToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoDistrToRaccordoId(),
                    root -> root.join(TipoDistConc_.tipoDistrToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
        }
        return specification;
    }
}
