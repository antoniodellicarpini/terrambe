package com.terrambe.service;

import com.terrambe.domain.RegOpcPrepterr;
import com.terrambe.repository.RegOpcPrepterrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcPrepterr}.
 */
@Service
@Transactional
public class RegOpcPrepterrService {

    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrService.class);

    private final RegOpcPrepterrRepository regOpcPrepterrRepository;

    public RegOpcPrepterrService(RegOpcPrepterrRepository regOpcPrepterrRepository) {
        this.regOpcPrepterrRepository = regOpcPrepterrRepository;
    }

    /**
     * Save a regOpcPrepterr.
     *
     * @param regOpcPrepterr the entity to save.
     * @return the persisted entity.
     */
    public RegOpcPrepterr save(RegOpcPrepterr regOpcPrepterr) {
        log.debug("Request to save RegOpcPrepterr : {}", regOpcPrepterr);
        return regOpcPrepterrRepository.save(regOpcPrepterr);
    }

    /**
     * Get all the regOpcPrepterrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPrepterr> findAll() {
        log.debug("Request to get all RegOpcPrepterrs");
        return regOpcPrepterrRepository.findAll();
    }


    /**
     * Get one regOpcPrepterr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcPrepterr> findOne(Long id) {
        log.debug("Request to get RegOpcPrepterr : {}", id);
        return regOpcPrepterrRepository.findById(id);
    }

    /**
     * Delete the regOpcPrepterr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcPrepterr : {}", id);
        regOpcPrepterrRepository.deleteById(id);
    }
}
