package com.terrambe.service;

import com.terrambe.domain.RecAzienda;
import com.terrambe.repository.RecAziendaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecAzienda}.
 */
@Service
@Transactional
public class RecAziendaService {

    private final Logger log = LoggerFactory.getLogger(RecAziendaService.class);

    private final RecAziendaRepository recAziendaRepository;

    public RecAziendaService(RecAziendaRepository recAziendaRepository) {
        this.recAziendaRepository = recAziendaRepository;
    }

    /**
     * Save a recAzienda.
     *
     * @param recAzienda the entity to save.
     * @return the persisted entity.
     */
    public RecAzienda save(RecAzienda recAzienda) {
        log.debug("Request to save RecAzienda : {}", recAzienda);
        return recAziendaRepository.save(recAzienda);
    }

    /**
     * Get all the recAziendas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecAzienda> findAll() {
        log.debug("Request to get all RecAziendas");
        return recAziendaRepository.findAll();
    }


    /**
     * Get one recAzienda by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecAzienda> findOne(Long id) {
        log.debug("Request to get RecAzienda : {}", id);
        return recAziendaRepository.findById(id);
    }

    /**
     * Delete the recAzienda by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecAzienda : {}", id);
        recAziendaRepository.deleteById(id);
    }
}
