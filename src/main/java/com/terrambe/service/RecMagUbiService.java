package com.terrambe.service;

import com.terrambe.domain.RecMagUbi;
import com.terrambe.repository.RecMagUbiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecMagUbi}.
 */
@Service
@Transactional
public class RecMagUbiService {

    private final Logger log = LoggerFactory.getLogger(RecMagUbiService.class);

    private final RecMagUbiRepository recMagUbiRepository;

    public RecMagUbiService(RecMagUbiRepository recMagUbiRepository) {
        this.recMagUbiRepository = recMagUbiRepository;
    }

    /**
     * Save a recMagUbi.
     *
     * @param recMagUbi the entity to save.
     * @return the persisted entity.
     */
    public RecMagUbi save(RecMagUbi recMagUbi) {
        log.debug("Request to save RecMagUbi : {}", recMagUbi);
        return recMagUbiRepository.save(recMagUbi);
    }

    /**
     * Get all the recMagUbis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecMagUbi> findAll() {
        log.debug("Request to get all RecMagUbis");
        return recMagUbiRepository.findAll();
    }


    /**
     * Get one recMagUbi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecMagUbi> findOne(Long id) {
        log.debug("Request to get RecMagUbi : {}", id);
        return recMagUbiRepository.findById(id);
    }

    /**
     * Delete the recMagUbi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecMagUbi : {}", id);
        recMagUbiRepository.deleteById(id);
    }
}
