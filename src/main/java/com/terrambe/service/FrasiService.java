package com.terrambe.service;

import com.terrambe.domain.Frasi;
import com.terrambe.repository.FrasiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Frasi}.
 */
@Service
@Transactional
public class FrasiService {

    private final Logger log = LoggerFactory.getLogger(FrasiService.class);

    private final FrasiRepository frasiRepository;

    public FrasiService(FrasiRepository frasiRepository) {
        this.frasiRepository = frasiRepository;
    }

    /**
     * Save a frasi.
     *
     * @param frasi the entity to save.
     * @return the persisted entity.
     */
    public Frasi save(Frasi frasi) {
        log.debug("Request to save Frasi : {}", frasi);
        return frasiRepository.save(frasi);
    }

    /**
     * Get all the frasis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Frasi> findAll() {
        log.debug("Request to get all Frasis");
        return frasiRepository.findAll();
    }


    /**
     * Get one frasi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Frasi> findOne(Long id) {
        log.debug("Request to get Frasi : {}", id);
        return frasiRepository.findById(id);
    }

    /**
     * Delete the frasi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Frasi : {}", id);
        frasiRepository.deleteById(id);
    }
}
