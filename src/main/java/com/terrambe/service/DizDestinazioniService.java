package com.terrambe.service;

import com.terrambe.domain.DizDestinazioni;
import com.terrambe.repository.DizDestinazioniRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizDestinazioni}.
 */
@Service
@Transactional
public class DizDestinazioniService {

    private final Logger log = LoggerFactory.getLogger(DizDestinazioniService.class);

    private final DizDestinazioniRepository dizDestinazioniRepository;

    public DizDestinazioniService(DizDestinazioniRepository dizDestinazioniRepository) {
        this.dizDestinazioniRepository = dizDestinazioniRepository;
    }

    /**
     * Save a dizDestinazioni.
     *
     * @param dizDestinazioni the entity to save.
     * @return the persisted entity.
     */
    public DizDestinazioni save(DizDestinazioni dizDestinazioni) {
        log.debug("Request to save DizDestinazioni : {}", dizDestinazioni);
        return dizDestinazioniRepository.save(dizDestinazioni);
    }

    /**
     * Get all the dizDestinazionis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizDestinazioni> findAll() {
        log.debug("Request to get all DizDestinazionis");
        return dizDestinazioniRepository.findAll();
    }


    /**
     * Get one dizDestinazioni by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizDestinazioni> findOne(Long id) {
        log.debug("Request to get DizDestinazioni : {}", id);
        return dizDestinazioniRepository.findById(id);
    }

    /**
     * Delete the dizDestinazioni by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizDestinazioni : {}", id);
        dizDestinazioniRepository.deleteById(id);
    }
}
