package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziSupPartConduzione;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziSupPartConduzioneRepository;
import com.terrambe.service.dto.AziSupPartConduzioneCriteria;

/**
 * Service for executing complex queries for {@link AziSupPartConduzione} entities in the database.
 * The main input is a {@link AziSupPartConduzioneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziSupPartConduzione} or a {@link Page} of {@link AziSupPartConduzione} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziSupPartConduzioneQueryService extends QueryService<AziSupPartConduzione> {

    private final Logger log = LoggerFactory.getLogger(AziSupPartConduzioneQueryService.class);

    private final AziSupPartConduzioneRepository aziSupPartConduzioneRepository;

    public AziSupPartConduzioneQueryService(AziSupPartConduzioneRepository aziSupPartConduzioneRepository) {
        this.aziSupPartConduzioneRepository = aziSupPartConduzioneRepository;
    }

    /**
     * Return a {@link List} of {@link AziSupPartConduzione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziSupPartConduzione> findByCriteria(AziSupPartConduzioneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziSupPartConduzione> specification = createSpecification(criteria);
        return aziSupPartConduzioneRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziSupPartConduzione} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziSupPartConduzione> findByCriteria(AziSupPartConduzioneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziSupPartConduzione> specification = createSpecification(criteria);
        return aziSupPartConduzioneRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziSupPartConduzioneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziSupPartConduzione> specification = createSpecification(criteria);
        return aziSupPartConduzioneRepository.count(specification);
    }

    /**
     * Function to convert {@link AziSupPartConduzioneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziSupPartConduzione> createSpecification(AziSupPartConduzioneCriteria criteria) {
        Specification<AziSupPartConduzione> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziSupPartConduzione_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), AziSupPartConduzione_.descrizione));
            }
            if (criteria.getDataInizio() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizio(), AziSupPartConduzione_.dataInizio));
            }
            if (criteria.getDataFine() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFine(), AziSupPartConduzione_.dataFine));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziSupPartConduzione_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziSupPartConduzione_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziSupPartConduzione_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziSupPartConduzione_.userIdLastMod));
            }
        }
        return specification;
    }
}
