package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TabellaRaccordoRepository;
import com.terrambe.service.dto.TabellaRaccordoCriteria;

/**
 * Service for executing complex queries for {@link TabellaRaccordo} entities in the database.
 * The main input is a {@link TabellaRaccordoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TabellaRaccordo} or a {@link Page} of {@link TabellaRaccordo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TabellaRaccordoQueryService extends QueryService<TabellaRaccordo> {

    private final Logger log = LoggerFactory.getLogger(TabellaRaccordoQueryService.class);

    private final TabellaRaccordoRepository tabellaRaccordoRepository;

    public TabellaRaccordoQueryService(TabellaRaccordoRepository tabellaRaccordoRepository) {
        this.tabellaRaccordoRepository = tabellaRaccordoRepository;
    }

    /**
     * Return a {@link List} of {@link TabellaRaccordo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TabellaRaccordo> findByCriteria(TabellaRaccordoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TabellaRaccordo> specification = createSpecification(criteria);
        return tabellaRaccordoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TabellaRaccordo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TabellaRaccordo> findByCriteria(TabellaRaccordoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TabellaRaccordo> specification = createSpecification(criteria);
        return tabellaRaccordoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TabellaRaccordoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TabellaRaccordo> specification = createSpecification(criteria);
        return tabellaRaccordoRepository.count(specification);
    }

    /**
     * Function to convert {@link TabellaRaccordoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TabellaRaccordo> createSpecification(TabellaRaccordoCriteria criteria) {
        Specification<TabellaRaccordo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TabellaRaccordo_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), TabellaRaccordo_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), TabellaRaccordo_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), TabellaRaccordo_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TabellaRaccordo_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TabellaRaccordo_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TabellaRaccordo_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TabellaRaccordo_.userIdLastMod));
            }
            if (criteria.getRaccordoToSitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToSitoId(),
                    root -> root.join(TabellaRaccordo_.raccordoToSito, JoinType.LEFT).get(Sito_.id)));
            }
            if (criteria.getRaccordoToColtBdfId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToColtBdfId(),
                    root -> root.join(TabellaRaccordo_.raccordoToColtBdf, JoinType.LEFT).get(ColtureBDF_.id)));
            }
            if (criteria.getRaccordoToDosagId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToDosagId(),
                    root -> root.join(TabellaRaccordo_.raccordoToDosag, JoinType.LEFT).get(Dosaggio_.id)));
            }
            if (criteria.getRaccordoToEtiFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToEtiFitoId(),
                    root -> root.join(TabellaRaccordo_.raccordoToEtiFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getRaccordoToFaseFenoId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToFaseFenoId(),
                    root -> root.join(TabellaRaccordo_.raccordoToFaseFeno, JoinType.LEFT).get(FaseFenologica_.id)));
            }
            if (criteria.getRaccordoToTipoDistrId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToTipoDistrId(),
                    root -> root.join(TabellaRaccordo_.raccordoToTipoDistr, JoinType.LEFT).get(TipoDistConc_.id)));
            }
            if (criteria.getRaccordoToAvversId() != null) {
                specification = specification.and(buildSpecification(criteria.getRaccordoToAvversId(),
                    root -> root.join(TabellaRaccordo_.raccordoToAvvers, JoinType.LEFT).get(Avversita_.id)));
            }
        }
        return specification;
    }
}
