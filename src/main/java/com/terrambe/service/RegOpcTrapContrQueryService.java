package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcTrapContrRepository;
import com.terrambe.service.dto.RegOpcTrapContrCriteria;

/**
 * Service for executing complex queries for {@link RegOpcTrapContr} entities in the database.
 * The main input is a {@link RegOpcTrapContrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcTrapContr} or a {@link Page} of {@link RegOpcTrapContr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcTrapContrQueryService extends QueryService<RegOpcTrapContr> {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapContrQueryService.class);

    private final RegOpcTrapContrRepository regOpcTrapContrRepository;

    public RegOpcTrapContrQueryService(RegOpcTrapContrRepository regOpcTrapContrRepository) {
        this.regOpcTrapContrRepository = regOpcTrapContrRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcTrapContr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrapContr> findByCriteria(RegOpcTrapContrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcTrapContr> specification = createSpecification(criteria);
        return regOpcTrapContrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcTrapContr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcTrapContr> findByCriteria(RegOpcTrapContrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcTrapContr> specification = createSpecification(criteria);
        return regOpcTrapContrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcTrapContrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcTrapContr> specification = createSpecification(criteria);
        return regOpcTrapContrRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcTrapContrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcTrapContr> createSpecification(RegOpcTrapContrCriteria criteria) {
        Specification<RegOpcTrapContr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcTrapContr_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcTrapContr_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcTrapContr_.idUnitaProd));
            }
            if (criteria.getDataControllo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataControllo(), RegOpcTrapContr_.dataControllo));
            }
            if (criteria.getDataSostiFerom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataSostiFerom(), RegOpcTrapContr_.dataSostiFerom));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcTrapContr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcTrapContr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcTrapContr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcTrapContr_.userIdLastMod));
            }
            if (criteria.getTrapContrToOpcTrapId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrapContrToOpcTrapId(),
                    root -> root.join(RegOpcTrapContr_.trapContrToOpcTrap, JoinType.LEFT).get(RegOpcTrap_.id)));
            }
            if (criteria.getTrapContrTipoToTrapContrId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrapContrTipoToTrapContrId(),
                    root -> root.join(RegOpcTrapContr_.trapContrTipoToTrapContr, JoinType.LEFT).get(TrapContrTipo_.id)));
            }
        }
        return specification;
    }
}
