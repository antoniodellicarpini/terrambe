package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegCaricoFitoFarmaco;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegCaricoFitoFarmacoRepository;
import com.terrambe.service.dto.RegCaricoFitoFarmacoCriteria;

/**
 * Service for executing complex queries for {@link RegCaricoFitoFarmaco} entities in the database.
 * The main input is a {@link RegCaricoFitoFarmacoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegCaricoFitoFarmaco} or a {@link Page} of {@link RegCaricoFitoFarmaco} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegCaricoFitoFarmacoQueryService extends QueryService<RegCaricoFitoFarmaco> {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFitoFarmacoQueryService.class);

    private final RegCaricoFitoFarmacoRepository regCaricoFitoFarmacoRepository;

    public RegCaricoFitoFarmacoQueryService(RegCaricoFitoFarmacoRepository regCaricoFitoFarmacoRepository) {
        this.regCaricoFitoFarmacoRepository = regCaricoFitoFarmacoRepository;
    }

    /**
     * Return a {@link List} of {@link RegCaricoFitoFarmaco} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoFitoFarmaco> findByCriteria(RegCaricoFitoFarmacoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegCaricoFitoFarmaco> specification = createSpecification(criteria);
        return regCaricoFitoFarmacoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegCaricoFitoFarmaco} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegCaricoFitoFarmaco> findByCriteria(RegCaricoFitoFarmacoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegCaricoFitoFarmaco> specification = createSpecification(criteria);
        return regCaricoFitoFarmacoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegCaricoFitoFarmacoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegCaricoFitoFarmaco> specification = createSpecification(criteria);
        return regCaricoFitoFarmacoRepository.count(specification);
    }

    /**
     * Function to convert {@link RegCaricoFitoFarmacoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegCaricoFitoFarmaco> createSpecification(RegCaricoFitoFarmacoCriteria criteria) {
        Specification<RegCaricoFitoFarmaco> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegCaricoFitoFarmaco_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegCaricoFitoFarmaco_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegCaricoFitoFarmaco_.idUnitaProd));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegCaricoFitoFarmaco_.quantita));
            }
            if (criteria.getPrezzoUnitario() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrezzoUnitario(), RegCaricoFitoFarmaco_.prezzoUnitario));
            }
            if (criteria.getDataCarico() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataCarico(), RegCaricoFitoFarmaco_.dataCarico));
            }
            if (criteria.getDdt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDdt(), RegCaricoFitoFarmaco_.ddt));
            }
            if (criteria.getDataScadenza() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataScadenza(), RegCaricoFitoFarmaco_.dataScadenza));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegCaricoFitoFarmaco_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegCaricoFitoFarmaco_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegCaricoFitoFarmaco_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegCaricoFitoFarmaco_.userIdLastMod));
            }
            if (criteria.getMagUbicazioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getMagUbicazioneId(),
                    root -> root.join(RegCaricoFitoFarmaco_.magUbicazione, JoinType.LEFT).get(MagUbicazione_.id)));
            }
            if (criteria.getEtichettafitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettafitoId(),
                    root -> root.join(RegCaricoFitoFarmaco_.etichettafito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getCaricoFitoToFornitoriId() != null) {
                specification = specification.and(buildSpecification(criteria.getCaricoFitoToFornitoriId(),
                    root -> root.join(RegCaricoFitoFarmaco_.caricoFitoToFornitori, JoinType.LEFT).get(Fornitori_.id)));
            }
            if (criteria.getRegFitoToTipoRegId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegFitoToTipoRegId(),
                    root -> root.join(RegCaricoFitoFarmaco_.regFitoToTipoReg, JoinType.LEFT).get(TipoRegMag_.id)));
            }
        }
        return specification;
    }
}
