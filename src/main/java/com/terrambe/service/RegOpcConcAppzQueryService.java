package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcConcAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcConcAppzRepository;
import com.terrambe.service.dto.RegOpcConcAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcConcAppz} entities in the database.
 * The main input is a {@link RegOpcConcAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcConcAppz} or a {@link Page} of {@link RegOpcConcAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcConcAppzQueryService extends QueryService<RegOpcConcAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcAppzQueryService.class);

    private final RegOpcConcAppzRepository regOpcConcAppzRepository;

    public RegOpcConcAppzQueryService(RegOpcConcAppzRepository regOpcConcAppzRepository) {
        this.regOpcConcAppzRepository = regOpcConcAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcConcAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConcAppz> findByCriteria(RegOpcConcAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcConcAppz> specification = createSpecification(criteria);
        return regOpcConcAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcConcAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcConcAppz> findByCriteria(RegOpcConcAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcConcAppz> specification = createSpecification(criteria);
        return regOpcConcAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcConcAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcConcAppz> specification = createSpecification(criteria);
        return regOpcConcAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcConcAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcConcAppz> createSpecification(RegOpcConcAppzCriteria criteria) {
        Specification<RegOpcConcAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcConcAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcConcAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcConcAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcConcAppz_.percentLavorata));
            }
            if (criteria.getValN() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValN(), RegOpcConcAppz_.valN));
            }
            if (criteria.getValP() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValP(), RegOpcConcAppz_.valP));
            }
            if (criteria.getValK() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValK(), RegOpcConcAppz_.valK));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcConcAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcConcAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcConcAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcConcAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcConcAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcConcId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcConcId(),
                    root -> root.join(RegOpcConcAppz_.appzToOpcConc, JoinType.LEFT).get(RegOpcConc_.id)));
            }
        }
        return specification;
    }
}
