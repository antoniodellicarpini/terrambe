package com.terrambe.service;

import com.terrambe.domain.RegOpcConc;
import com.terrambe.repository.RegOpcConcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcConc}.
 */
@Service
@Transactional
public class RegOpcConcService {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcService.class);

    private final RegOpcConcRepository regOpcConcRepository;

    public RegOpcConcService(RegOpcConcRepository regOpcConcRepository) {
        this.regOpcConcRepository = regOpcConcRepository;
    }

    /**
     * Save a regOpcConc.
     *
     * @param regOpcConc the entity to save.
     * @return the persisted entity.
     */
    public RegOpcConc save(RegOpcConc regOpcConc) {
        log.debug("Request to save RegOpcConc : {}", regOpcConc);
        return regOpcConcRepository.save(regOpcConc);
    }

    /**
     * Get all the regOpcConcs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConc> findAll() {
        log.debug("Request to get all RegOpcConcs");
        return regOpcConcRepository.findAll();
    }


    /**
     * Get one regOpcConc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcConc> findOne(Long id) {
        log.debug("Request to get RegOpcConc : {}", id);
        return regOpcConcRepository.findById(id);
    }

    /**
     * Delete the regOpcConc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcConc : {}", id);
        regOpcConcRepository.deleteById(id);
    }
}
