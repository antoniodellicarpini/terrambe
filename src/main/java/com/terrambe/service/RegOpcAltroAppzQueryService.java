package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcAltroAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcAltroAppzRepository;
import com.terrambe.service.dto.RegOpcAltroAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcAltroAppz} entities in the database.
 * The main input is a {@link RegOpcAltroAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcAltroAppz} or a {@link Page} of {@link RegOpcAltroAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcAltroAppzQueryService extends QueryService<RegOpcAltroAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcAltroAppzQueryService.class);

    private final RegOpcAltroAppzRepository regOpcAltroAppzRepository;

    public RegOpcAltroAppzQueryService(RegOpcAltroAppzRepository regOpcAltroAppzRepository) {
        this.regOpcAltroAppzRepository = regOpcAltroAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcAltroAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcAltroAppz> findByCriteria(RegOpcAltroAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcAltroAppz> specification = createSpecification(criteria);
        return regOpcAltroAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcAltroAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcAltroAppz> findByCriteria(RegOpcAltroAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcAltroAppz> specification = createSpecification(criteria);
        return regOpcAltroAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcAltroAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcAltroAppz> specification = createSpecification(criteria);
        return regOpcAltroAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcAltroAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcAltroAppz> createSpecification(RegOpcAltroAppzCriteria criteria) {
        Specification<RegOpcAltroAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcAltroAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcAltroAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcAltroAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcAltroAppz_.percentLavorata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcAltroAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcAltroAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcAltroAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcAltroAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcAltroAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcAltroId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcAltroId(),
                    root -> root.join(RegOpcAltroAppz_.appzToOpcAltro, JoinType.LEFT).get(RegOpcAltro_.id)));
            }
        }
        return specification;
    }
}
