package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.FerAnagrafica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FerAnagraficaRepository;
import com.terrambe.service.dto.FerAnagraficaCriteria;

/**
 * Service for executing complex queries for {@link FerAnagrafica} entities in the database.
 * The main input is a {@link FerAnagraficaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FerAnagrafica} or a {@link Page} of {@link FerAnagrafica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FerAnagraficaQueryService extends QueryService<FerAnagrafica> {

    private final Logger log = LoggerFactory.getLogger(FerAnagraficaQueryService.class);

    private final FerAnagraficaRepository ferAnagraficaRepository;

    public FerAnagraficaQueryService(FerAnagraficaRepository ferAnagraficaRepository) {
        this.ferAnagraficaRepository = ferAnagraficaRepository;
    }

    /**
     * Return a {@link List} of {@link FerAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FerAnagrafica> findByCriteria(FerAnagraficaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FerAnagrafica> specification = createSpecification(criteria);
        return ferAnagraficaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FerAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FerAnagrafica> findByCriteria(FerAnagraficaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FerAnagrafica> specification = createSpecification(criteria);
        return ferAnagraficaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FerAnagraficaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FerAnagrafica> specification = createSpecification(criteria);
        return ferAnagraficaRepository.count(specification);
    }

    /**
     * Function to convert {@link FerAnagraficaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FerAnagrafica> createSpecification(FerAnagraficaCriteria criteria) {
        Specification<FerAnagrafica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FerAnagrafica_.id));
            }
            if (criteria.getDenominazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDenominazione(), FerAnagrafica_.denominazione));
            }
            if (criteria.getDittaProduttrice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDittaProduttrice(), FerAnagrafica_.dittaProduttrice));
            }
            if (criteria.getDatoN() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatoN(), FerAnagrafica_.datoN));
            }
            if (criteria.getDatoP() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatoP(), FerAnagrafica_.datoP));
            }
            if (criteria.getDatoK() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatoK(), FerAnagrafica_.datoK));
            }
            if (criteria.getTipoConcime() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoConcime(), FerAnagrafica_.tipoConcime));
            }
            if (criteria.getBio() != null) {
                specification = specification.and(buildSpecification(criteria.getBio(), FerAnagrafica_.bio));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), FerAnagrafica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), FerAnagrafica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), FerAnagrafica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), FerAnagrafica_.userIdLastMod));
            }
            if (criteria.getFertTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getFertTipoId(),
                    root -> root.join(FerAnagrafica_.fertTipo, JoinType.LEFT).get(FertTipo_.id)));
            }
        }
        return specification;
    }
}
