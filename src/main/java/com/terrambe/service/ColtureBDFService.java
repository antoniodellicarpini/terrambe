package com.terrambe.service;

import com.terrambe.domain.ColtureBDF;
import com.terrambe.repository.ColtureBDFRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ColtureBDF}.
 */
@Service
@Transactional
public class ColtureBDFService {

    private final Logger log = LoggerFactory.getLogger(ColtureBDFService.class);

    private final ColtureBDFRepository coltureBDFRepository;

    public ColtureBDFService(ColtureBDFRepository coltureBDFRepository) {
        this.coltureBDFRepository = coltureBDFRepository;
    }

    /**
     * Save a coltureBDF.
     *
     * @param coltureBDF the entity to save.
     * @return the persisted entity.
     */
    public ColtureBDF save(ColtureBDF coltureBDF) {
        log.debug("Request to save ColtureBDF : {}", coltureBDF);
        return coltureBDFRepository.save(coltureBDF);
    }

    /**
     * Get all the coltureBDFS.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ColtureBDF> findAll() {
        log.debug("Request to get all ColtureBDFS");
        return coltureBDFRepository.findAll();
    }


    /**
     * Get one coltureBDF by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ColtureBDF> findOne(Long id) {
        log.debug("Request to get ColtureBDF : {}", id);
        return coltureBDFRepository.findById(id);
    }

    /**
     * Delete the coltureBDF by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ColtureBDF : {}", id);
        coltureBDFRepository.deleteById(id);
    }
}
