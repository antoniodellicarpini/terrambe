package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AtrAnagrafica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AtrAnagraficaRepository;
import com.terrambe.service.dto.AtrAnagraficaCriteria;

/**
 * Service for executing complex queries for {@link AtrAnagrafica} entities in the database.
 * The main input is a {@link AtrAnagraficaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AtrAnagrafica} or a {@link Page} of {@link AtrAnagrafica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AtrAnagraficaQueryService extends QueryService<AtrAnagrafica> {

    private final Logger log = LoggerFactory.getLogger(AtrAnagraficaQueryService.class);

    private final AtrAnagraficaRepository atrAnagraficaRepository;

    public AtrAnagraficaQueryService(AtrAnagraficaRepository atrAnagraficaRepository) {
        this.atrAnagraficaRepository = atrAnagraficaRepository;
    }

    /**
     * Return a {@link List} of {@link AtrAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AtrAnagrafica> findByCriteria(AtrAnagraficaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AtrAnagrafica> specification = createSpecification(criteria);
        return atrAnagraficaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AtrAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AtrAnagrafica> findByCriteria(AtrAnagraficaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AtrAnagrafica> specification = createSpecification(criteria);
        return atrAnagraficaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AtrAnagraficaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AtrAnagrafica> specification = createSpecification(criteria);
        return atrAnagraficaRepository.count(specification);
    }

    /**
     * Function to convert {@link AtrAnagraficaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AtrAnagrafica> createSpecification(AtrAnagraficaCriteria criteria) {
        Specification<AtrAnagrafica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AtrAnagrafica_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), AtrAnagrafica_.idAzienda));
            }
            if (criteria.getModello() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModello(), AtrAnagrafica_.modello));
            }
            if (criteria.getDataRevisione() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataRevisione(), AtrAnagrafica_.dataRevisione));
            }
            if (criteria.getCavalli() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCavalli(), AtrAnagrafica_.cavalli));
            }
            if (criteria.getCapacita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCapacita(), AtrAnagrafica_.capacita));
            }
            if (criteria.getTarga() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTarga(), AtrAnagrafica_.targa));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AtrAnagrafica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AtrAnagrafica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AtrAnagrafica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AtrAnagrafica_.userIdLastMod));
            }
            if (criteria.getAtrAnaToUniAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAtrAnaToUniAnaId(),
                    root -> root.join(AtrAnagrafica_.atrAnaToUniAna, JoinType.LEFT).get(UniAnagrafica_.id)));
            }
            if (criteria.getAtrAnaToAtrTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getAtrAnaToAtrTipoId(),
                    root -> root.join(AtrAnagrafica_.atrAnaToAtrTipo, JoinType.LEFT).get(AtrTipologia_.id)));
            }
            if (criteria.getAtrAnagToAtrMarcId() != null) {
                specification = specification.and(buildSpecification(criteria.getAtrAnagToAtrMarcId(),
                    root -> root.join(AtrAnagrafica_.atrAnagToAtrMarc, JoinType.LEFT).get(AtrMarchio_.id)));
            }
        }
        return specification;
    }
}
