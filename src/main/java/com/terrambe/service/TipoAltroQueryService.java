package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoAltro;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoAltroRepository;
import com.terrambe.service.dto.TipoAltroCriteria;

/**
 * Service for executing complex queries for {@link TipoAltro} entities in the database.
 * The main input is a {@link TipoAltroCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoAltro} or a {@link Page} of {@link TipoAltro} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoAltroQueryService extends QueryService<TipoAltro> {

    private final Logger log = LoggerFactory.getLogger(TipoAltroQueryService.class);

    private final TipoAltroRepository tipoAltroRepository;

    public TipoAltroQueryService(TipoAltroRepository tipoAltroRepository) {
        this.tipoAltroRepository = tipoAltroRepository;
    }

    /**
     * Return a {@link List} of {@link TipoAltro} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoAltro> findByCriteria(TipoAltroCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoAltro> specification = createSpecification(criteria);
        return tipoAltroRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoAltro} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoAltro> findByCriteria(TipoAltroCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoAltro> specification = createSpecification(criteria);
        return tipoAltroRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoAltroCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoAltro> specification = createSpecification(criteria);
        return tipoAltroRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoAltroCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoAltro> createSpecification(TipoAltroCriteria criteria) {
        Specification<TipoAltro> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoAltro_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoAltro_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoAltro_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoAltro_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoAltro_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoAltro_.userIdLastMod));
            }
            if (criteria.getTipoToOpcAltroId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoToOpcAltroId(),
                    root -> root.join(TipoAltro_.tipoToOpcAltros, JoinType.LEFT).get(RegOpcAltro_.id)));
            }
        }
        return specification;
    }
}
