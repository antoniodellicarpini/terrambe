package com.terrambe.service;

import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.repository.EtichettaSostanzeAttiveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EtichettaSostanzeAttive}.
 */
@Service
@Transactional
public class EtichettaSostanzeAttiveService {

    private final Logger log = LoggerFactory.getLogger(EtichettaSostanzeAttiveService.class);

    private final EtichettaSostanzeAttiveRepository etichettaSostanzeAttiveRepository;

    public EtichettaSostanzeAttiveService(EtichettaSostanzeAttiveRepository etichettaSostanzeAttiveRepository) {
        this.etichettaSostanzeAttiveRepository = etichettaSostanzeAttiveRepository;
    }

    /**
     * Save a etichettaSostanzeAttive.
     *
     * @param etichettaSostanzeAttive the entity to save.
     * @return the persisted entity.
     */
    public EtichettaSostanzeAttive save(EtichettaSostanzeAttive etichettaSostanzeAttive) {
        log.debug("Request to save EtichettaSostanzeAttive : {}", etichettaSostanzeAttive);
        return etichettaSostanzeAttiveRepository.save(etichettaSostanzeAttive);
    }

    /**
     * Get all the etichettaSostanzeAttives.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaSostanzeAttive> findAll() {
        log.debug("Request to get all EtichettaSostanzeAttives");
        return etichettaSostanzeAttiveRepository.findAll();
    }


    /**
     * Get one etichettaSostanzeAttive by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EtichettaSostanzeAttive> findOne(Long id) {
        log.debug("Request to get EtichettaSostanzeAttive : {}", id);
        return etichettaSostanzeAttiveRepository.findById(id);
    }

    /**
     * Delete the etichettaSostanzeAttive by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EtichettaSostanzeAttive : {}", id);
        etichettaSostanzeAttiveRepository.deleteById(id);
    }
}
