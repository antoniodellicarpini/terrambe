package com.terrambe.service;

import com.terrambe.domain.OpePatentino;
import com.terrambe.repository.OpePatentinoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OpePatentino}.
 */
@Service
@Transactional
public class OpePatentinoService {

    private final Logger log = LoggerFactory.getLogger(OpePatentinoService.class);

    private final OpePatentinoRepository opePatentinoRepository;

    public OpePatentinoService(OpePatentinoRepository opePatentinoRepository) {
        this.opePatentinoRepository = opePatentinoRepository;
    }

    /**
     * Save a opePatentino.
     *
     * @param opePatentino the entity to save.
     * @return the persisted entity.
     */
    public OpePatentino save(OpePatentino opePatentino) {
        log.debug("Request to save OpePatentino : {}", opePatentino);
        return opePatentinoRepository.save(opePatentino);
    }

    /**
     * Get all the opePatentinos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OpePatentino> findAll() {
        log.debug("Request to get all OpePatentinos");
        return opePatentinoRepository.findAll();
    }


    /**
     * Get one opePatentino by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OpePatentino> findOne(Long id) {
        log.debug("Request to get OpePatentino : {}", id);
        return opePatentinoRepository.findById(id);
    }

    /**
     * Delete the opePatentino by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OpePatentino : {}", id);
        opePatentinoRepository.deleteById(id);
    }
}
