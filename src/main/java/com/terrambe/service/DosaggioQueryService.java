package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Dosaggio;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DosaggioRepository;
import com.terrambe.service.dto.DosaggioCriteria;

/**
 * Service for executing complex queries for {@link Dosaggio} entities in the database.
 * The main input is a {@link DosaggioCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Dosaggio} or a {@link Page} of {@link Dosaggio} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DosaggioQueryService extends QueryService<Dosaggio> {

    private final Logger log = LoggerFactory.getLogger(DosaggioQueryService.class);

    private final DosaggioRepository dosaggioRepository;

    public DosaggioQueryService(DosaggioRepository dosaggioRepository) {
        this.dosaggioRepository = dosaggioRepository;
    }

    /**
     * Return a {@link List} of {@link Dosaggio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Dosaggio> findByCriteria(DosaggioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Dosaggio> specification = createSpecification(criteria);
        return dosaggioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Dosaggio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Dosaggio> findByCriteria(DosaggioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Dosaggio> specification = createSpecification(criteria);
        return dosaggioRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DosaggioCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Dosaggio> specification = createSpecification(criteria);
        return dosaggioRepository.count(specification);
    }

    /**
     * Function to convert {@link DosaggioCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Dosaggio> createSpecification(DosaggioCriteria criteria) {
        Specification<Dosaggio> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Dosaggio_.id));
            }
            if (criteria.getCarenzaC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaC(), Dosaggio_.carenzaC));
            }
            if (criteria.getCarenzaP() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaP(), Dosaggio_.carenzaP));
            }
            if (criteria.getCarenzaD() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaD(), Dosaggio_.carenzaD));
            }
            if (criteria.getCarenzaO() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaO(), Dosaggio_.carenzaO));
            }
            if (criteria.getCarenzaT() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaT(), Dosaggio_.carenzaT));
            }
            if (criteria.getCarenzaNd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCarenzaNd(), Dosaggio_.carenzaNd));
            }
            if (criteria.getAcquaHaMin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAcquaHaMin(), Dosaggio_.acquaHaMin));
            }
            if (criteria.getAcquaHaMax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAcquaHaMax(), Dosaggio_.acquaHaMax));
            }
            if (criteria.getNumMaxInt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumMaxInt(), Dosaggio_.numMaxInt));
            }
            if (criteria.getRifMaxTratt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRifMaxTratt(), Dosaggio_.rifMaxTratt));
            }
            if (criteria.getIntervTratt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIntervTratt(), Dosaggio_.intervTratt));
            }
            if (criteria.getIntervTrattMax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIntervTrattMax(), Dosaggio_.intervTrattMax));
            }
            if (criteria.getScadenzaDosi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScadenzaDosi(), Dosaggio_.scadenzaDosi));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Dosaggio_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Dosaggio_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Dosaggio_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Dosaggio_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Dosaggio_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Dosaggio_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Dosaggio_.userIdLastMod));
            }
            if (criteria.getDosagToDoseId() != null) {
                specification = specification.and(buildSpecification(criteria.getDosagToDoseId(),
                    root -> root.join(Dosaggio_.dosagToDoses, JoinType.LEFT).get(Dose_.id)));
            }
            if (criteria.getDosagToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getDosagToRaccordoId(),
                    root -> root.join(Dosaggio_.dosagToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
        }
        return specification;
    }
}
