package com.terrambe.service;

import com.terrambe.domain.IndAziRapCli;
import com.terrambe.repository.IndAziRapCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndAziRapCli}.
 */
@Service
@Transactional
public class IndAziRapCliService {

    private final Logger log = LoggerFactory.getLogger(IndAziRapCliService.class);

    private final IndAziRapCliRepository indAziRapCliRepository;

    public IndAziRapCliService(IndAziRapCliRepository indAziRapCliRepository) {
        this.indAziRapCliRepository = indAziRapCliRepository;
    }

    /**
     * Save a indAziRapCli.
     *
     * @param indAziRapCli the entity to save.
     * @return the persisted entity.
     */
    public IndAziRapCli save(IndAziRapCli indAziRapCli) {
        log.debug("Request to save IndAziRapCli : {}", indAziRapCli);
        return indAziRapCliRepository.save(indAziRapCli);
    }

    /**
     * Get all the indAziRapClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziRapCli> findAll() {
        log.debug("Request to get all IndAziRapClis");
        return indAziRapCliRepository.findAll();
    }


    /**
     * Get one indAziRapCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndAziRapCli> findOne(Long id) {
        log.debug("Request to get IndAziRapCli : {}", id);
        return indAziRapCliRepository.findById(id);
    }

    /**
     * Delete the indAziRapCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndAziRapCli : {}", id);
        indAziRapCliRepository.deleteById(id);
    }
}
