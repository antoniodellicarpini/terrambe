package com.terrambe.service;

import com.terrambe.domain.TipoRegMag;
import com.terrambe.repository.TipoRegMagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoRegMag}.
 */
@Service
@Transactional
public class TipoRegMagService {

    private final Logger log = LoggerFactory.getLogger(TipoRegMagService.class);

    private final TipoRegMagRepository tipoRegMagRepository;

    public TipoRegMagService(TipoRegMagRepository tipoRegMagRepository) {
        this.tipoRegMagRepository = tipoRegMagRepository;
    }

    /**
     * Save a tipoRegMag.
     *
     * @param tipoRegMag the entity to save.
     * @return the persisted entity.
     */
    public TipoRegMag save(TipoRegMag tipoRegMag) {
        log.debug("Request to save TipoRegMag : {}", tipoRegMag);
        return tipoRegMagRepository.save(tipoRegMag);
    }

    /**
     * Get all the tipoRegMags.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoRegMag> findAll() {
        log.debug("Request to get all TipoRegMags");
        return tipoRegMagRepository.findAll();
    }


    /**
     * Get one tipoRegMag by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoRegMag> findOne(Long id) {
        log.debug("Request to get TipoRegMag : {}", id);
        return tipoRegMagRepository.findById(id);
    }

    /**
     * Delete the tipoRegMag by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoRegMag : {}", id);
        tipoRegMagRepository.deleteById(id);
    }
}
