package com.terrambe.service;

import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.repository.AziFormaGiuridicaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziFormaGiuridica}.
 */
@Service
@Transactional
public class AziFormaGiuridicaService {

    private final Logger log = LoggerFactory.getLogger(AziFormaGiuridicaService.class);

    private final AziFormaGiuridicaRepository aziFormaGiuridicaRepository;

    public AziFormaGiuridicaService(AziFormaGiuridicaRepository aziFormaGiuridicaRepository) {
        this.aziFormaGiuridicaRepository = aziFormaGiuridicaRepository;
    }

    /**
     * Save a aziFormaGiuridica.
     *
     * @param aziFormaGiuridica the entity to save.
     * @return the persisted entity.
     */
    public AziFormaGiuridica save(AziFormaGiuridica aziFormaGiuridica) {
        log.debug("Request to save AziFormaGiuridica : {}", aziFormaGiuridica);
        return aziFormaGiuridicaRepository.save(aziFormaGiuridica);
    }

    /**
     * Get all the aziFormaGiuridicas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziFormaGiuridica> findAll() {
        log.debug("Request to get all AziFormaGiuridicas");
        return aziFormaGiuridicaRepository.findAll();
    }


    /**
     * Get one aziFormaGiuridica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziFormaGiuridica> findOne(Long id) {
        log.debug("Request to get AziFormaGiuridica : {}", id);
        return aziFormaGiuridicaRepository.findById(id);
    }

    /**
     * Delete the aziFormaGiuridica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziFormaGiuridica : {}", id);
        aziFormaGiuridicaRepository.deleteById(id);
    }
}
