package com.terrambe.service;

import com.terrambe.domain.FertTipo;
import com.terrambe.repository.FertTipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FertTipo}.
 */
@Service
@Transactional
public class FertTipoService {

    private final Logger log = LoggerFactory.getLogger(FertTipoService.class);

    private final FertTipoRepository fertTipoRepository;

    public FertTipoService(FertTipoRepository fertTipoRepository) {
        this.fertTipoRepository = fertTipoRepository;
    }

    /**
     * Save a fertTipo.
     *
     * @param fertTipo the entity to save.
     * @return the persisted entity.
     */
    public FertTipo save(FertTipo fertTipo) {
        log.debug("Request to save FertTipo : {}", fertTipo);
        return fertTipoRepository.save(fertTipo);
    }

    /**
     * Get all the fertTipos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FertTipo> findAll() {
        log.debug("Request to get all FertTipos");
        return fertTipoRepository.findAll();
    }


    /**
     * Get one fertTipo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FertTipo> findOne(Long id) {
        log.debug("Request to get FertTipo : {}", id);
        return fertTipoRepository.findById(id);
    }

    /**
     * Delete the fertTipo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FertTipo : {}", id);
        fertTipoRepository.deleteById(id);
    }
}
