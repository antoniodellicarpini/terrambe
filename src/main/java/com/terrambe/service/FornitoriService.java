package com.terrambe.service;

import com.terrambe.domain.Fornitori;
import com.terrambe.repository.FornitoriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Fornitori}.
 */
@Service
@Transactional
public class FornitoriService {

    private final Logger log = LoggerFactory.getLogger(FornitoriService.class);

    private final FornitoriRepository fornitoriRepository;

    public FornitoriService(FornitoriRepository fornitoriRepository) {
        this.fornitoriRepository = fornitoriRepository;
    }

    /**
     * Save a fornitori.
     *
     * @param fornitori the entity to save.
     * @return the persisted entity.
     */
    public Fornitori save(Fornitori fornitori) {
        log.debug("Request to save Fornitori : {}", fornitori);
        return fornitoriRepository.save(fornitori);
    }

    /**
     * Get all the fornitoris.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Fornitori> findAll() {
        log.debug("Request to get all Fornitoris");
        return fornitoriRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the fornitoris with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Fornitori> findAllWithEagerRelationships(Pageable pageable) {
        return fornitoriRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one fornitori by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Fornitori> findOne(Long id) {
        log.debug("Request to get Fornitori : {}", id);
        return fornitoriRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the fornitori by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fornitori : {}", id);
        fornitoriRepository.deleteById(id);
    }
}
