package com.terrambe.service;

import com.terrambe.domain.DizColture;
import com.terrambe.repository.DizColtureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizColture}.
 */
@Service
@Transactional
public class DizColtureService {

    private final Logger log = LoggerFactory.getLogger(DizColtureService.class);

    private final DizColtureRepository dizColtureRepository;

    public DizColtureService(DizColtureRepository dizColtureRepository) {
        this.dizColtureRepository = dizColtureRepository;
    }

    /**
     * Save a dizColture.
     *
     * @param dizColture the entity to save.
     * @return the persisted entity.
     */
    public DizColture save(DizColture dizColture) {
        log.debug("Request to save DizColture : {}", dizColture);
        return dizColtureRepository.save(dizColture);
    }

    /**
     * Get all the dizColtures.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizColture> findAll() {
        log.debug("Request to get all DizColtures");
        return dizColtureRepository.findAll();
    }


    /**
     * Get one dizColture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizColture> findOne(Long id) {
        log.debug("Request to get DizColture : {}", id);
        return dizColtureRepository.findById(id);
    }

    /**
     * Delete the dizColture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizColture : {}", id);
        dizColtureRepository.deleteById(id);
    }
}
