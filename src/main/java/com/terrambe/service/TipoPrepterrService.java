package com.terrambe.service;

import com.terrambe.domain.TipoPrepterr;
import com.terrambe.repository.TipoPrepterrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoPrepterr}.
 */
@Service
@Transactional
public class TipoPrepterrService {

    private final Logger log = LoggerFactory.getLogger(TipoPrepterrService.class);

    private final TipoPrepterrRepository tipoPrepterrRepository;

    public TipoPrepterrService(TipoPrepterrRepository tipoPrepterrRepository) {
        this.tipoPrepterrRepository = tipoPrepterrRepository;
    }

    /**
     * Save a tipoPrepterr.
     *
     * @param tipoPrepterr the entity to save.
     * @return the persisted entity.
     */
    public TipoPrepterr save(TipoPrepterr tipoPrepterr) {
        log.debug("Request to save TipoPrepterr : {}", tipoPrepterr);
        return tipoPrepterrRepository.save(tipoPrepterr);
    }

    /**
     * Get all the tipoPrepterrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoPrepterr> findAll() {
        log.debug("Request to get all TipoPrepterrs");
        return tipoPrepterrRepository.findAll();
    }


    /**
     * Get one tipoPrepterr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoPrepterr> findOne(Long id) {
        log.debug("Request to get TipoPrepterr : {}", id);
        return tipoPrepterrRepository.findById(id);
    }

    /**
     * Delete the tipoPrepterr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoPrepterr : {}", id);
        tipoPrepterrRepository.deleteById(id);
    }
}
