package com.terrambe.service;

import com.terrambe.domain.DizMacrousi;
import com.terrambe.repository.DizMacrousiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DizMacrousi}.
 */
@Service
@Transactional
public class DizMacrousiService {

    private final Logger log = LoggerFactory.getLogger(DizMacrousiService.class);

    private final DizMacrousiRepository dizMacrousiRepository;

    public DizMacrousiService(DizMacrousiRepository dizMacrousiRepository) {
        this.dizMacrousiRepository = dizMacrousiRepository;
    }

    /**
     * Save a dizMacrousi.
     *
     * @param dizMacrousi the entity to save.
     * @return the persisted entity.
     */
    public DizMacrousi save(DizMacrousi dizMacrousi) {
        log.debug("Request to save DizMacrousi : {}", dizMacrousi);
        return dizMacrousiRepository.save(dizMacrousi);
    }

    /**
     * Get all the dizMacrousis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DizMacrousi> findAll() {
        log.debug("Request to get all DizMacrousis");
        return dizMacrousiRepository.findAll();
    }


    /**
     * Get one dizMacrousi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DizMacrousi> findOne(Long id) {
        log.debug("Request to get DizMacrousi : {}", id);
        return dizMacrousiRepository.findById(id);
    }

    /**
     * Delete the dizMacrousi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DizMacrousi : {}", id);
        dizMacrousiRepository.deleteById(id);
    }
}
