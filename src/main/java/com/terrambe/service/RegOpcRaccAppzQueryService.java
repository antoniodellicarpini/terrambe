package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcRaccAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcRaccAppzRepository;
import com.terrambe.service.dto.RegOpcRaccAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcRaccAppz} entities in the database.
 * The main input is a {@link RegOpcRaccAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcRaccAppz} or a {@link Page} of {@link RegOpcRaccAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcRaccAppzQueryService extends QueryService<RegOpcRaccAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcRaccAppzQueryService.class);

    private final RegOpcRaccAppzRepository regOpcRaccAppzRepository;

    public RegOpcRaccAppzQueryService(RegOpcRaccAppzRepository regOpcRaccAppzRepository) {
        this.regOpcRaccAppzRepository = regOpcRaccAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcRaccAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcRaccAppz> findByCriteria(RegOpcRaccAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcRaccAppz> specification = createSpecification(criteria);
        return regOpcRaccAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcRaccAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcRaccAppz> findByCriteria(RegOpcRaccAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcRaccAppz> specification = createSpecification(criteria);
        return regOpcRaccAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcRaccAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcRaccAppz> specification = createSpecification(criteria);
        return regOpcRaccAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcRaccAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcRaccAppz> createSpecification(RegOpcRaccAppzCriteria criteria) {
        Specification<RegOpcRaccAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcRaccAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcRaccAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcRaccAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcRaccAppz_.percentLavorata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcRaccAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcRaccAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcRaccAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcRaccAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcRaccAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcRaccId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcRaccId(),
                    root -> root.join(RegOpcRaccAppz_.appzToOpcRacc, JoinType.LEFT).get(RegOpcRacc_.id)));
            }
        }
        return specification;
    }
}
