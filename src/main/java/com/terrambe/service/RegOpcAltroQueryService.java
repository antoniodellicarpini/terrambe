package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcAltro;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcAltroRepository;
import com.terrambe.service.dto.RegOpcAltroCriteria;

/**
 * Service for executing complex queries for {@link RegOpcAltro} entities in the database.
 * The main input is a {@link RegOpcAltroCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcAltro} or a {@link Page} of {@link RegOpcAltro} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcAltroQueryService extends QueryService<RegOpcAltro> {

    private final Logger log = LoggerFactory.getLogger(RegOpcAltroQueryService.class);

    private final RegOpcAltroRepository regOpcAltroRepository;

    public RegOpcAltroQueryService(RegOpcAltroRepository regOpcAltroRepository) {
        this.regOpcAltroRepository = regOpcAltroRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcAltro} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcAltro> findByCriteria(RegOpcAltroCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcAltro> specification = createSpecification(criteria);
        return regOpcAltroRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcAltro} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcAltro> findByCriteria(RegOpcAltroCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcAltro> specification = createSpecification(criteria);
        return regOpcAltroRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcAltroCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcAltro> specification = createSpecification(criteria);
        return regOpcAltroRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcAltroCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcAltro> createSpecification(RegOpcAltroCriteria criteria) {
        Specification<RegOpcAltro> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcAltro_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcAltro_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcAltro_.idUnitaProd));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcAltro_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcAltro_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcAltro_.tempoImpiegato));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcAltro_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcAltro_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcAltro_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcAltro_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcAltro_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcAltro_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcAltro_.userIdLastMod));
            }
            if (criteria.getOpcAltroToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcAltroToAppzId(),
                    root -> root.join(RegOpcAltro_.opcAltroToAppzs, JoinType.LEFT).get(RegOpcAltroAppz_.id)));
            }
            if (criteria.getOpcAltroToTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcAltroToTipoId(),
                    root -> root.join(RegOpcAltro_.opcAltroToTipo, JoinType.LEFT).get(TipoAltro_.id)));
            }
        }
        return specification;
    }
}
