package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.EtichettaSostanzeAttive;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.EtichettaSostanzeAttiveRepository;
import com.terrambe.service.dto.EtichettaSostanzeAttiveCriteria;

/**
 * Service for executing complex queries for {@link EtichettaSostanzeAttive} entities in the database.
 * The main input is a {@link EtichettaSostanzeAttiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtichettaSostanzeAttive} or a {@link Page} of {@link EtichettaSostanzeAttive} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtichettaSostanzeAttiveQueryService extends QueryService<EtichettaSostanzeAttive> {

    private final Logger log = LoggerFactory.getLogger(EtichettaSostanzeAttiveQueryService.class);

    private final EtichettaSostanzeAttiveRepository etichettaSostanzeAttiveRepository;

    public EtichettaSostanzeAttiveQueryService(EtichettaSostanzeAttiveRepository etichettaSostanzeAttiveRepository) {
        this.etichettaSostanzeAttiveRepository = etichettaSostanzeAttiveRepository;
    }

    /**
     * Return a {@link List} of {@link EtichettaSostanzeAttive} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaSostanzeAttive> findByCriteria(EtichettaSostanzeAttiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EtichettaSostanzeAttive> specification = createSpecification(criteria);
        return etichettaSostanzeAttiveRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EtichettaSostanzeAttive} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtichettaSostanzeAttive> findByCriteria(EtichettaSostanzeAttiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EtichettaSostanzeAttive> specification = createSpecification(criteria);
        return etichettaSostanzeAttiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtichettaSostanzeAttiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EtichettaSostanzeAttive> specification = createSpecification(criteria);
        return etichettaSostanzeAttiveRepository.count(specification);
    }

    /**
     * Function to convert {@link EtichettaSostanzeAttiveCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EtichettaSostanzeAttive> createSpecification(EtichettaSostanzeAttiveCriteria criteria) {
        Specification<EtichettaSostanzeAttive> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EtichettaSostanzeAttive_.id));
            }
            if (criteria.getGrammiLitro() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGrammiLitro(), EtichettaSostanzeAttive_.grammiLitro));
            }
            if (criteria.getPercentuale() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentuale(), EtichettaSostanzeAttive_.percentuale));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), EtichettaSostanzeAttive_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), EtichettaSostanzeAttive_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), EtichettaSostanzeAttive_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), EtichettaSostanzeAttive_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), EtichettaSostanzeAttive_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), EtichettaSostanzeAttive_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), EtichettaSostanzeAttive_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(EtichettaSostanzeAttive_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getEtiSostAttToSostAttId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtiSostAttToSostAttId(),
                    root -> root.join(EtichettaSostanzeAttive_.etiSostAttToSostAtt, JoinType.LEFT).get(SostanzeAttive_.id)));
            }
        }
        return specification;
    }
}
