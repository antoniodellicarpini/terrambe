package com.terrambe.service;

import com.terrambe.domain.Trappole;
import com.terrambe.repository.TrappoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Trappole}.
 */
@Service
@Transactional
public class TrappoleService {

    private final Logger log = LoggerFactory.getLogger(TrappoleService.class);

    private final TrappoleRepository trappoleRepository;

    public TrappoleService(TrappoleRepository trappoleRepository) {
        this.trappoleRepository = trappoleRepository;
    }

    /**
     * Save a trappole.
     *
     * @param trappole the entity to save.
     * @return the persisted entity.
     */
    public Trappole save(Trappole trappole) {
        log.debug("Request to save Trappole : {}", trappole);
        return trappoleRepository.save(trappole);
    }

    /**
     * Get all the trappoles.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Trappole> findAll() {
        log.debug("Request to get all Trappoles");
        return trappoleRepository.findAll();
    }


    /**
     * Get one trappole by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Trappole> findOne(Long id) {
        log.debug("Request to get Trappole : {}", id);
        return trappoleRepository.findById(id);
    }

    /**
     * Delete the trappole by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Trappole : {}", id);
        trappoleRepository.deleteById(id);
    }
}
