package com.terrambe.service;

import com.terrambe.domain.RegOpcFitoProd;
import com.terrambe.repository.RegOpcFitoProdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcFitoProd}.
 */
@Service
@Transactional
public class RegOpcFitoProdService {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoProdService.class);

    private final RegOpcFitoProdRepository regOpcFitoProdRepository;

    public RegOpcFitoProdService(RegOpcFitoProdRepository regOpcFitoProdRepository) {
        this.regOpcFitoProdRepository = regOpcFitoProdRepository;
    }

    /**
     * Save a regOpcFitoProd.
     *
     * @param regOpcFitoProd the entity to save.
     * @return the persisted entity.
     */
    public RegOpcFitoProd save(RegOpcFitoProd regOpcFitoProd) {
        log.debug("Request to save RegOpcFitoProd : {}", regOpcFitoProd);
        return regOpcFitoProdRepository.save(regOpcFitoProd);
    }

    /**
     * Get all the regOpcFitoProds.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcFitoProd> findAll() {
        log.debug("Request to get all RegOpcFitoProds");
        return regOpcFitoProdRepository.findAll();
    }


    /**
     * Get one regOpcFitoProd by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcFitoProd> findOne(Long id) {
        log.debug("Request to get RegOpcFitoProd : {}", id);
        return regOpcFitoProdRepository.findById(id);
    }

    /**
     * Delete the regOpcFitoProd by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcFitoProd : {}", id);
        regOpcFitoProdRepository.deleteById(id);
    }
}
