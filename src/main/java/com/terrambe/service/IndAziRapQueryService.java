package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndAziRap;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndAziRapRepository;
import com.terrambe.service.dto.IndAziRapCriteria;

/**
 * Service for executing complex queries for {@link IndAziRap} entities in the database.
 * The main input is a {@link IndAziRapCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndAziRap} or a {@link Page} of {@link IndAziRap} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndAziRapQueryService extends QueryService<IndAziRap> {

    private final Logger log = LoggerFactory.getLogger(IndAziRapQueryService.class);

    private final IndAziRapRepository indAziRapRepository;

    public IndAziRapQueryService(IndAziRapRepository indAziRapRepository) {
        this.indAziRapRepository = indAziRapRepository;
    }

    /**
     * Return a {@link List} of {@link IndAziRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziRap> findByCriteria(IndAziRapCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndAziRap> specification = createSpecification(criteria);
        return indAziRapRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndAziRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndAziRap> findByCriteria(IndAziRapCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndAziRap> specification = createSpecification(criteria);
        return indAziRapRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndAziRapCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndAziRap> specification = createSpecification(criteria);
        return indAziRapRepository.count(specification);
    }

    /**
     * Function to convert {@link IndAziRapCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndAziRap> createSpecification(IndAziRapCriteria criteria) {
        Specification<IndAziRap> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndAziRap_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndAziRap_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndAziRap_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndAziRap_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndAziRap_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndAziRap_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndAziRap_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndAziRap_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndAziRap_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndAziRap_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndAziRap_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndAziRap_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndAziRap_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndAziRap_.userIdLastMod));
            }
            if (criteria.getIndToAziRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToAziRapId(),
                    root -> root.join(IndAziRap_.indToAziRap, JoinType.LEFT).get(AziRappresentante_.id)));
            }
        }
        return specification;
    }
}
