package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecAziRapCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecAziRapCliRepository;
import com.terrambe.service.dto.RecAziRapCliCriteria;

/**
 * Service for executing complex queries for {@link RecAziRapCli} entities in the database.
 * The main input is a {@link RecAziRapCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecAziRapCli} or a {@link Page} of {@link RecAziRapCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecAziRapCliQueryService extends QueryService<RecAziRapCli> {

    private final Logger log = LoggerFactory.getLogger(RecAziRapCliQueryService.class);

    private final RecAziRapCliRepository recAziRapCliRepository;

    public RecAziRapCliQueryService(RecAziRapCliRepository recAziRapCliRepository) {
        this.recAziRapCliRepository = recAziRapCliRepository;
    }

    /**
     * Return a {@link List} of {@link RecAziRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziRapCli> findByCriteria(RecAziRapCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecAziRapCli> specification = createSpecification(criteria);
        return recAziRapCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecAziRapCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecAziRapCli> findByCriteria(RecAziRapCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecAziRapCli> specification = createSpecification(criteria);
        return recAziRapCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecAziRapCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecAziRapCli> specification = createSpecification(criteria);
        return recAziRapCliRepository.count(specification);
    }

    /**
     * Function to convert {@link RecAziRapCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecAziRapCli> createSpecification(RecAziRapCliCriteria criteria) {
        Specification<RecAziRapCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecAziRapCli_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecAziRapCli_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecAziRapCli_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecAziRapCli_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecAziRapCli_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecAziRapCli_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecAziRapCli_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecAziRapCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecAziRapCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecAziRapCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecAziRapCli_.userIdLastMod));
            }
            if (criteria.getRecToCliRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToCliRapId(),
                    root -> root.join(RecAziRapCli_.recToCliRap, JoinType.LEFT).get(AziRappresentanteCli_.id)));
            }
        }
        return specification;
    }
}
