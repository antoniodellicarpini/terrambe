package com.terrambe.service;

import com.terrambe.domain.TabellaRaccordo;
import com.terrambe.repository.TabellaRaccordoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TabellaRaccordo}.
 */
@Service
@Transactional
public class TabellaRaccordoService {

    private final Logger log = LoggerFactory.getLogger(TabellaRaccordoService.class);

    private final TabellaRaccordoRepository tabellaRaccordoRepository;

    public TabellaRaccordoService(TabellaRaccordoRepository tabellaRaccordoRepository) {
        this.tabellaRaccordoRepository = tabellaRaccordoRepository;
    }

    /**
     * Save a tabellaRaccordo.
     *
     * @param tabellaRaccordo the entity to save.
     * @return the persisted entity.
     */
    public TabellaRaccordo save(TabellaRaccordo tabellaRaccordo) {
        log.debug("Request to save TabellaRaccordo : {}", tabellaRaccordo);
        return tabellaRaccordoRepository.save(tabellaRaccordo);
    }

    /**
     * Get all the tabellaRaccordos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TabellaRaccordo> findAll() {
        log.debug("Request to get all TabellaRaccordos");
        return tabellaRaccordoRepository.findAll();
    }


    /**
     * Get one tabellaRaccordo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TabellaRaccordo> findOne(Long id) {
        log.debug("Request to get TabellaRaccordo : {}", id);
        return tabellaRaccordoRepository.findById(id);
    }

    /**
     * Delete the tabellaRaccordo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TabellaRaccordo : {}", id);
        tabellaRaccordoRepository.deleteById(id);
    }
}
