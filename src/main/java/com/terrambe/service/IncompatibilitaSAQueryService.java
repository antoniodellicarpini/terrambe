package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IncompatibilitaSARepository;
import com.terrambe.service.dto.IncompatibilitaSACriteria;

/**
 * Service for executing complex queries for {@link IncompatibilitaSA} entities in the database.
 * The main input is a {@link IncompatibilitaSACriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IncompatibilitaSA} or a {@link Page} of {@link IncompatibilitaSA} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IncompatibilitaSAQueryService extends QueryService<IncompatibilitaSA> {

    private final Logger log = LoggerFactory.getLogger(IncompatibilitaSAQueryService.class);

    private final IncompatibilitaSARepository incompatibilitaSARepository;

    public IncompatibilitaSAQueryService(IncompatibilitaSARepository incompatibilitaSARepository) {
        this.incompatibilitaSARepository = incompatibilitaSARepository;
    }

    /**
     * Return a {@link List} of {@link IncompatibilitaSA} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IncompatibilitaSA> findByCriteria(IncompatibilitaSACriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IncompatibilitaSA> specification = createSpecification(criteria);
        return incompatibilitaSARepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IncompatibilitaSA} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IncompatibilitaSA> findByCriteria(IncompatibilitaSACriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IncompatibilitaSA> specification = createSpecification(criteria);
        return incompatibilitaSARepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IncompatibilitaSACriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IncompatibilitaSA> specification = createSpecification(criteria);
        return incompatibilitaSARepository.count(specification);
    }

    /**
     * Function to convert {@link IncompatibilitaSACriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IncompatibilitaSA> createSpecification(IncompatibilitaSACriteria criteria) {
        Specification<IncompatibilitaSA> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IncompatibilitaSA_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), IncompatibilitaSA_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), IncompatibilitaSA_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), IncompatibilitaSA_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IncompatibilitaSA_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IncompatibilitaSA_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IncompatibilitaSA_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IncompatibilitaSA_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(IncompatibilitaSA_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getIncompToSostAttId() != null) {
                specification = specification.and(buildSpecification(criteria.getIncompToSostAttId(),
                    root -> root.join(IncompatibilitaSA_.incompToSostAtt, JoinType.LEFT).get(SostanzeAttive_.id)));
            }
        }
        return specification;
    }
}
