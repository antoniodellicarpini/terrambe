package com.terrambe.service;

import com.terrambe.domain.IndNascForn;
import com.terrambe.repository.IndNascFornRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link IndNascForn}.
 */
@Service
@Transactional
public class IndNascFornService {

    private final Logger log = LoggerFactory.getLogger(IndNascFornService.class);

    private final IndNascFornRepository indNascFornRepository;

    public IndNascFornService(IndNascFornRepository indNascFornRepository) {
        this.indNascFornRepository = indNascFornRepository;
    }

    /**
     * Save a indNascForn.
     *
     * @param indNascForn the entity to save.
     * @return the persisted entity.
     */
    public IndNascForn save(IndNascForn indNascForn) {
        log.debug("Request to save IndNascForn : {}", indNascForn);
        return indNascFornRepository.save(indNascForn);
    }

    /**
     * Get all the indNascForns.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IndNascForn> findAll(Pageable pageable) {
        log.debug("Request to get all IndNascForns");
        return indNascFornRepository.findAll(pageable);
    }


    /**
     * Get one indNascForn by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndNascForn> findOne(Long id) {
        log.debug("Request to get IndNascForn : {}", id);
        return indNascFornRepository.findById(id);
    }

    /**
     * Delete the indNascForn by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndNascForn : {}", id);
        indNascFornRepository.deleteById(id);
    }
}
