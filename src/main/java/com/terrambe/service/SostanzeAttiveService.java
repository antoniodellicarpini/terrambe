package com.terrambe.service;

import com.terrambe.domain.SostanzeAttive;
import com.terrambe.repository.SostanzeAttiveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link SostanzeAttive}.
 */
@Service
@Transactional
public class SostanzeAttiveService {

    private final Logger log = LoggerFactory.getLogger(SostanzeAttiveService.class);

    private final SostanzeAttiveRepository sostanzeAttiveRepository;

    public SostanzeAttiveService(SostanzeAttiveRepository sostanzeAttiveRepository) {
        this.sostanzeAttiveRepository = sostanzeAttiveRepository;
    }

    /**
     * Save a sostanzeAttive.
     *
     * @param sostanzeAttive the entity to save.
     * @return the persisted entity.
     */
    public SostanzeAttive save(SostanzeAttive sostanzeAttive) {
        log.debug("Request to save SostanzeAttive : {}", sostanzeAttive);
        return sostanzeAttiveRepository.save(sostanzeAttive);
    }

    /**
     * Get all the sostanzeAttives.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SostanzeAttive> findAll() {
        log.debug("Request to get all SostanzeAttives");
        return sostanzeAttiveRepository.findAll();
    }


    /**
     * Get one sostanzeAttive by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SostanzeAttive> findOne(Long id) {
        log.debug("Request to get SostanzeAttive : {}", id);
        return sostanzeAttiveRepository.findById(id);
    }

    /**
     * Delete the sostanzeAttive by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SostanzeAttive : {}", id);
        sostanzeAttiveRepository.deleteById(id);
    }
}
