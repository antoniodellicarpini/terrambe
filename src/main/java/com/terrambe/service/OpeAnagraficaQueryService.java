package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.OpeAnagraficaRepository;
import com.terrambe.service.dto.OpeAnagraficaCriteria;

/**
 * Service for executing complex queries for {@link OpeAnagrafica} entities in the database.
 * The main input is a {@link OpeAnagraficaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OpeAnagrafica} or a {@link Page} of {@link OpeAnagrafica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OpeAnagraficaQueryService extends QueryService<OpeAnagrafica> {

    private final Logger log = LoggerFactory.getLogger(OpeAnagraficaQueryService.class);

    private final OpeAnagraficaRepository opeAnagraficaRepository;

    public OpeAnagraficaQueryService(OpeAnagraficaRepository opeAnagraficaRepository) {
        this.opeAnagraficaRepository = opeAnagraficaRepository;
    }

    /**
     * Return a {@link List} of {@link OpeAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OpeAnagrafica> findByCriteria(OpeAnagraficaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OpeAnagrafica> specification = createSpecification(criteria);
        return opeAnagraficaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OpeAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OpeAnagrafica> findByCriteria(OpeAnagraficaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OpeAnagrafica> specification = createSpecification(criteria);
        return opeAnagraficaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OpeAnagraficaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OpeAnagrafica> specification = createSpecification(criteria);
        return opeAnagraficaRepository.count(specification);
    }

    /**
     * Function to convert {@link OpeAnagraficaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OpeAnagrafica> createSpecification(OpeAnagraficaCriteria criteria) {
        Specification<OpeAnagrafica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OpeAnagrafica_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), OpeAnagrafica_.idAzienda));
            }
            if (criteria.getAttivo() != null) {
                specification = specification.and(buildSpecification(criteria.getAttivo(), OpeAnagrafica_.attivo));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), OpeAnagrafica_.nome));
            }
            if (criteria.getCognome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCognome(), OpeAnagrafica_.cognome));
            }
            if (criteria.getSesso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSesso(), OpeAnagrafica_.sesso));
            }
            if (criteria.getDataNascita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataNascita(), OpeAnagrafica_.dataNascita));
            }
            if (criteria.getCodiceFiscale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceFiscale(), OpeAnagrafica_.codiceFiscale));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), OpeAnagrafica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), OpeAnagrafica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), OpeAnagrafica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), OpeAnagrafica_.userIdLastMod));
            }
            if (criteria.getOpeAnaToIndNascId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnaToIndNascId(),
                    root -> root.join(OpeAnagrafica_.opeAnaToIndNasc, JoinType.LEFT).get(IndNascOpeAna_.id)));
            }
            if (criteria.getOpeAnaToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnaToIndId(),
                    root -> root.join(OpeAnagrafica_.opeAnaToInds, JoinType.LEFT).get(IndOpeAna_.id)));
            }
            if (criteria.getOpeAnaToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnaToRecId(),
                    root -> root.join(OpeAnagrafica_.opeAnaToRecs, JoinType.LEFT).get(RecOpeAna_.id)));
            }
            if (criteria.getPermessioperatoreId() != null) {
                specification = specification.and(buildSpecification(criteria.getPermessioperatoreId(),
                    root -> root.join(OpeAnagrafica_.permessioperatores, JoinType.LEFT).get(PermessiOperatore_.id)));
            }
            if (criteria.getOpeAnagToPateId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnagToPateId(),
                    root -> root.join(OpeAnagrafica_.opeAnagToPates, JoinType.LEFT).get(OpePatentino_.id)));
            }
            if (criteria.getOperatoriaziId() != null) {
                specification = specification.and(buildSpecification(criteria.getOperatoriaziId(),
                    root -> root.join(OpeAnagrafica_.operatoriazi, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
            if (criteria.getOpeAnagToOpeQualId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnagToOpeQualId(),
                    root -> root.join(OpeAnagrafica_.opeAnagToOpeQual, JoinType.LEFT).get(OpeQualifica_.id)));
            }
            if (criteria.getOpeAnaToFornId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeAnaToFornId(),
                    root -> root.join(OpeAnagrafica_.opeAnaToForns, JoinType.LEFT).get(Fornitori_.id)));
            }
            if (criteria.getOpeToTipoContratId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpeToTipoContratId(),
                    root -> root.join(OpeAnagrafica_.opeToTipoContrat, JoinType.LEFT).get(TipoContratto_.id)));
            }
        }
        return specification;
    }
}
