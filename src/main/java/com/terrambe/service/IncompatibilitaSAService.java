package com.terrambe.service;

import com.terrambe.domain.IncompatibilitaSA;
import com.terrambe.repository.IncompatibilitaSARepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IncompatibilitaSA}.
 */
@Service
@Transactional
public class IncompatibilitaSAService {

    private final Logger log = LoggerFactory.getLogger(IncompatibilitaSAService.class);

    private final IncompatibilitaSARepository incompatibilitaSARepository;

    public IncompatibilitaSAService(IncompatibilitaSARepository incompatibilitaSARepository) {
        this.incompatibilitaSARepository = incompatibilitaSARepository;
    }

    /**
     * Save a incompatibilitaSA.
     *
     * @param incompatibilitaSA the entity to save.
     * @return the persisted entity.
     */
    public IncompatibilitaSA save(IncompatibilitaSA incompatibilitaSA) {
        log.debug("Request to save IncompatibilitaSA : {}", incompatibilitaSA);
        return incompatibilitaSARepository.save(incompatibilitaSA);
    }

    /**
     * Get all the incompatibilitaSAS.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IncompatibilitaSA> findAll() {
        log.debug("Request to get all IncompatibilitaSAS");
        return incompatibilitaSARepository.findAll();
    }


    /**
     * Get one incompatibilitaSA by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IncompatibilitaSA> findOne(Long id) {
        log.debug("Request to get IncompatibilitaSA : {}", id);
        return incompatibilitaSARepository.findById(id);
    }

    /**
     * Delete the incompatibilitaSA by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IncompatibilitaSA : {}", id);
        incompatibilitaSARepository.deleteById(id);
    }
}
