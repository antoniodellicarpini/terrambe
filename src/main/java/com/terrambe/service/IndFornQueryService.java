package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndForn;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndFornRepository;
import com.terrambe.service.dto.IndFornCriteria;

/**
 * Service for executing complex queries for {@link IndForn} entities in the database.
 * The main input is a {@link IndFornCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndForn} or a {@link Page} of {@link IndForn} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndFornQueryService extends QueryService<IndForn> {

    private final Logger log = LoggerFactory.getLogger(IndFornQueryService.class);

    private final IndFornRepository indFornRepository;

    public IndFornQueryService(IndFornRepository indFornRepository) {
        this.indFornRepository = indFornRepository;
    }

    /**
     * Return a {@link List} of {@link IndForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndForn> findByCriteria(IndFornCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndForn> specification = createSpecification(criteria);
        return indFornRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndForn} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndForn> findByCriteria(IndFornCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndForn> specification = createSpecification(criteria);
        return indFornRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndFornCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndForn> specification = createSpecification(criteria);
        return indFornRepository.count(specification);
    }

    /**
     * Function to convert {@link IndFornCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndForn> createSpecification(IndFornCriteria criteria) {
        Specification<IndForn> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndForn_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndForn_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndForn_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndForn_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndForn_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndForn_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndForn_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndForn_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndForn_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndForn_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndForn_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndForn_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndForn_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndForn_.userIdLastMod));
            }
            if (criteria.getIndToFornId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToFornId(),
                    root -> root.join(IndForn_.indToForn, JoinType.LEFT).get(Fornitori_.id)));
            }
        }
        return specification;
    }
}
