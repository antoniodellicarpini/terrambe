package com.terrambe.service;

import com.terrambe.domain.UserTerram;
import com.terrambe.repository.UserTerramRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UserTerram}.
 */
@Service
@Transactional
public class UserTerramService {

    private final Logger log = LoggerFactory.getLogger(UserTerramService.class);

    private final UserTerramRepository userTerramRepository;

    public UserTerramService(UserTerramRepository userTerramRepository) {
        this.userTerramRepository = userTerramRepository;
    }

    /**
     * Save a userTerram.
     *
     * @param userTerram the entity to save.
     * @return the persisted entity.
     */
    public UserTerram save(UserTerram userTerram) {
        log.debug("Request to save UserTerram : {}", userTerram);
        return userTerramRepository.save(userTerram);
    }

    /**
     * Get all the userTerrams.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserTerram> findAll() {
        log.debug("Request to get all UserTerrams");
        return userTerramRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the userTerrams with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<UserTerram> findAllWithEagerRelationships(Pageable pageable) {
        return userTerramRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one userTerram by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserTerram> findOne(Long id) {
        log.debug("Request to get UserTerram : {}", id);
        return userTerramRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the userTerram by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserTerram : {}", id);
        userTerramRepository.deleteById(id);
    }
}
