package com.terrambe.service;

import com.terrambe.domain.RegOpcAltroAppz;
import com.terrambe.repository.RegOpcAltroAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcAltroAppz}.
 */
@Service
@Transactional
public class RegOpcAltroAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcAltroAppzService.class);

    private final RegOpcAltroAppzRepository regOpcAltroAppzRepository;

    public RegOpcAltroAppzService(RegOpcAltroAppzRepository regOpcAltroAppzRepository) {
        this.regOpcAltroAppzRepository = regOpcAltroAppzRepository;
    }

    /**
     * Save a regOpcAltroAppz.
     *
     * @param regOpcAltroAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcAltroAppz save(RegOpcAltroAppz regOpcAltroAppz) {
        log.debug("Request to save RegOpcAltroAppz : {}", regOpcAltroAppz);
        return regOpcAltroAppzRepository.save(regOpcAltroAppz);
    }

    /**
     * Get all the regOpcAltroAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcAltroAppz> findAll() {
        log.debug("Request to get all RegOpcAltroAppzs");
        return regOpcAltroAppzRepository.findAll();
    }


    /**
     * Get one regOpcAltroAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcAltroAppz> findOne(Long id) {
        log.debug("Request to get RegOpcAltroAppz : {}", id);
        return regOpcAltroAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcAltroAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcAltroAppz : {}", id);
        regOpcAltroAppzRepository.deleteById(id);
    }
}
