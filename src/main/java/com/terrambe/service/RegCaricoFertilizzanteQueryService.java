package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegCaricoFertilizzanteRepository;
import com.terrambe.service.dto.RegCaricoFertilizzanteCriteria;

/**
 * Service for executing complex queries for {@link RegCaricoFertilizzante} entities in the database.
 * The main input is a {@link RegCaricoFertilizzanteCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegCaricoFertilizzante} or a {@link Page} of {@link RegCaricoFertilizzante} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegCaricoFertilizzanteQueryService extends QueryService<RegCaricoFertilizzante> {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFertilizzanteQueryService.class);

    private final RegCaricoFertilizzanteRepository regCaricoFertilizzanteRepository;

    public RegCaricoFertilizzanteQueryService(RegCaricoFertilizzanteRepository regCaricoFertilizzanteRepository) {
        this.regCaricoFertilizzanteRepository = regCaricoFertilizzanteRepository;
    }

    /**
     * Return a {@link List} of {@link RegCaricoFertilizzante} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoFertilizzante> findByCriteria(RegCaricoFertilizzanteCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegCaricoFertilizzante> specification = createSpecification(criteria);
        return regCaricoFertilizzanteRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegCaricoFertilizzante} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegCaricoFertilizzante> findByCriteria(RegCaricoFertilizzanteCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegCaricoFertilizzante> specification = createSpecification(criteria);
        return regCaricoFertilizzanteRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegCaricoFertilizzanteCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegCaricoFertilizzante> specification = createSpecification(criteria);
        return regCaricoFertilizzanteRepository.count(specification);
    }

    /**
     * Function to convert {@link RegCaricoFertilizzanteCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegCaricoFertilizzante> createSpecification(RegCaricoFertilizzanteCriteria criteria) {
        Specification<RegCaricoFertilizzante> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegCaricoFertilizzante_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegCaricoFertilizzante_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegCaricoFertilizzante_.idUnitaProd));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegCaricoFertilizzante_.quantita));
            }
            if (criteria.getPrezzoUnitario() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrezzoUnitario(), RegCaricoFertilizzante_.prezzoUnitario));
            }
            if (criteria.getDataCarico() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataCarico(), RegCaricoFertilizzante_.dataCarico));
            }
            if (criteria.getDdt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDdt(), RegCaricoFertilizzante_.ddt));
            }
            if (criteria.getDataScadenza() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataScadenza(), RegCaricoFertilizzante_.dataScadenza));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegCaricoFertilizzante_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegCaricoFertilizzante_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegCaricoFertilizzante_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegCaricoFertilizzante_.userIdLastMod));
            }
            if (criteria.getMagUbicazioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getMagUbicazioneId(),
                    root -> root.join(RegCaricoFertilizzante_.magUbicazione, JoinType.LEFT).get(MagUbicazione_.id)));
            }
            if (criteria.getFeranagraficaId() != null) {
                specification = specification.and(buildSpecification(criteria.getFeranagraficaId(),
                    root -> root.join(RegCaricoFertilizzante_.feranagrafica, JoinType.LEFT).get(FerAnagrafica_.id)));
            }
            if (criteria.getCaricoFertToFornitoriId() != null) {
                specification = specification.and(buildSpecification(criteria.getCaricoFertToFornitoriId(),
                    root -> root.join(RegCaricoFertilizzante_.caricoFertToFornitori, JoinType.LEFT).get(Fornitori_.id)));
            }
            if (criteria.getRegFertToTipoRegId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegFertToTipoRegId(),
                    root -> root.join(RegCaricoFertilizzante_.regFertToTipoReg, JoinType.LEFT).get(TipoRegMag_.id)));
            }
        }
        return specification;
    }
}
