package com.terrambe.service;

import com.terrambe.domain.RegCaricoTrappole;
import com.terrambe.repository.RegCaricoTrappoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegCaricoTrappole}.
 */
@Service
@Transactional
public class RegCaricoTrappoleService {

    private final Logger log = LoggerFactory.getLogger(RegCaricoTrappoleService.class);

    private final RegCaricoTrappoleRepository regCaricoTrappoleRepository;

    public RegCaricoTrappoleService(RegCaricoTrappoleRepository regCaricoTrappoleRepository) {
        this.regCaricoTrappoleRepository = regCaricoTrappoleRepository;
    }

    /**
     * Save a regCaricoTrappole.
     *
     * @param regCaricoTrappole the entity to save.
     * @return the persisted entity.
     */
    public RegCaricoTrappole save(RegCaricoTrappole regCaricoTrappole) {
        log.debug("Request to save RegCaricoTrappole : {}", regCaricoTrappole);
        return regCaricoTrappoleRepository.save(regCaricoTrappole);
    }

    /**
     * Get all the regCaricoTrappoles.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoTrappole> findAll() {
        log.debug("Request to get all RegCaricoTrappoles");
        return regCaricoTrappoleRepository.findAll();
    }


    /**
     * Get one regCaricoTrappole by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegCaricoTrappole> findOne(Long id) {
        log.debug("Request to get RegCaricoTrappole : {}", id);
        return regCaricoTrappoleRepository.findById(id);
    }

    /**
     * Delete the regCaricoTrappole by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegCaricoTrappole : {}", id);
        regCaricoTrappoleRepository.deleteById(id);
    }
}
