package com.terrambe.service;

import com.terrambe.domain.TrapContrTipo;
import com.terrambe.repository.TrapContrTipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TrapContrTipo}.
 */
@Service
@Transactional
public class TrapContrTipoService {

    private final Logger log = LoggerFactory.getLogger(TrapContrTipoService.class);

    private final TrapContrTipoRepository trapContrTipoRepository;

    public TrapContrTipoService(TrapContrTipoRepository trapContrTipoRepository) {
        this.trapContrTipoRepository = trapContrTipoRepository;
    }

    /**
     * Save a trapContrTipo.
     *
     * @param trapContrTipo the entity to save.
     * @return the persisted entity.
     */
    public TrapContrTipo save(TrapContrTipo trapContrTipo) {
        log.debug("Request to save TrapContrTipo : {}", trapContrTipo);
        return trapContrTipoRepository.save(trapContrTipo);
    }

    /**
     * Get all the trapContrTipos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TrapContrTipo> findAll() {
        log.debug("Request to get all TrapContrTipos");
        return trapContrTipoRepository.findAll();
    }


    /**
     * Get one trapContrTipo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TrapContrTipo> findOne(Long id) {
        log.debug("Request to get TrapContrTipo : {}", id);
        return trapContrTipoRepository.findById(id);
    }

    /**
     * Delete the trapContrTipo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TrapContrTipo : {}", id);
        trapContrTipoRepository.deleteById(id);
    }
}
