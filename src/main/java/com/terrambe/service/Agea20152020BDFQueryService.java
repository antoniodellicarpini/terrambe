package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Agea20152020BDF;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.Agea20152020BDFRepository;
import com.terrambe.service.dto.Agea20152020BDFCriteria;

/**
 * Service for executing complex queries for {@link Agea20152020BDF} entities in the database.
 * The main input is a {@link Agea20152020BDFCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Agea20152020BDF} or a {@link Page} of {@link Agea20152020BDF} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class Agea20152020BDFQueryService extends QueryService<Agea20152020BDF> {

    private final Logger log = LoggerFactory.getLogger(Agea20152020BDFQueryService.class);

    private final Agea20152020BDFRepository agea20152020BDFRepository;

    public Agea20152020BDFQueryService(Agea20152020BDFRepository agea20152020BDFRepository) {
        this.agea20152020BDFRepository = agea20152020BDFRepository;
    }

    /**
     * Return a {@link List} of {@link Agea20152020BDF} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Agea20152020BDF> findByCriteria(Agea20152020BDFCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Agea20152020BDF> specification = createSpecification(criteria);
        return agea20152020BDFRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Agea20152020BDF} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Agea20152020BDF> findByCriteria(Agea20152020BDFCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Agea20152020BDF> specification = createSpecification(criteria);
        return agea20152020BDFRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(Agea20152020BDFCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Agea20152020BDF> specification = createSpecification(criteria);
        return agea20152020BDFRepository.count(specification);
    }

    /**
     * Function to convert {@link Agea20152020BDFCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Agea20152020BDF> createSpecification(Agea20152020BDFCriteria criteria) {
        Specification<Agea20152020BDF> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Agea20152020BDF_.id));
            }
            if (criteria.getAgeaBdfToColtBdfId() != null) {
                specification = specification.and(buildSpecification(criteria.getAgeaBdfToColtBdfId(),
                    root -> root.join(Agea20152020BDF_.ageaBdfToColtBdf, JoinType.LEFT).get(ColtureBDF_.id)));
            }
            if (criteria.getBdfToMAtriceId() != null) {
                specification = specification.and(buildSpecification(criteria.getBdfToMAtriceId(),
                    root -> root.join(Agea20152020BDF_.bdfToMAtrice, JoinType.LEFT).get(Agea20152020Matrice_.id)));
            }
        }
        return specification;
    }
}
