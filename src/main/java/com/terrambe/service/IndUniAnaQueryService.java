package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndUniAna;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndUniAnaRepository;
import com.terrambe.service.dto.IndUniAnaCriteria;

/**
 * Service for executing complex queries for {@link IndUniAna} entities in the database.
 * The main input is a {@link IndUniAnaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndUniAna} or a {@link Page} of {@link IndUniAna} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndUniAnaQueryService extends QueryService<IndUniAna> {

    private final Logger log = LoggerFactory.getLogger(IndUniAnaQueryService.class);

    private final IndUniAnaRepository indUniAnaRepository;

    public IndUniAnaQueryService(IndUniAnaRepository indUniAnaRepository) {
        this.indUniAnaRepository = indUniAnaRepository;
    }

    /**
     * Return a {@link List} of {@link IndUniAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndUniAna> findByCriteria(IndUniAnaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndUniAna> specification = createSpecification(criteria);
        return indUniAnaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndUniAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndUniAna> findByCriteria(IndUniAnaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndUniAna> specification = createSpecification(criteria);
        return indUniAnaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndUniAnaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndUniAna> specification = createSpecification(criteria);
        return indUniAnaRepository.count(specification);
    }

    /**
     * Function to convert {@link IndUniAnaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndUniAna> createSpecification(IndUniAnaCriteria criteria) {
        Specification<IndUniAna> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndUniAna_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndUniAna_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndUniAna_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndUniAna_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndUniAna_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndUniAna_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndUniAna_.comune));
            }
            if (criteria.getNumeroCiv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumeroCiv(), IndUniAna_.numeroCiv));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), IndUniAna_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), IndUniAna_.longitude));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndUniAna_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndUniAna_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndUniAna_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndUniAna_.userIdLastMod));
            }
            if (criteria.getIndToUniAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndToUniAnaId(),
                    root -> root.join(IndUniAna_.indToUniAna, JoinType.LEFT).get(UniAnagrafica_.id)));
            }
        }
        return specification;
    }
}
