package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziAnagrafica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziAnagraficaRepository;
import com.terrambe.service.dto.AziAnagraficaCriteria;

/**
 * Service for executing complex queries for {@link AziAnagrafica} entities in the database.
 * The main input is a {@link AziAnagraficaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziAnagrafica} or a {@link Page} of {@link AziAnagrafica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziAnagraficaQueryService extends QueryService<AziAnagrafica> {

    private final Logger log = LoggerFactory.getLogger(AziAnagraficaQueryService.class);

    private final AziAnagraficaRepository aziAnagraficaRepository;

    public AziAnagraficaQueryService(AziAnagraficaRepository aziAnagraficaRepository) {
        this.aziAnagraficaRepository = aziAnagraficaRepository;
    }

    /**
     * Return a {@link List} of {@link AziAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziAnagrafica> findByCriteria(AziAnagraficaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziAnagrafica> specification = createSpecification(criteria);
        return aziAnagraficaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziAnagrafica> findByCriteria(AziAnagraficaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziAnagrafica> specification = createSpecification(criteria);
        return aziAnagraficaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziAnagraficaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziAnagrafica> specification = createSpecification(criteria);
        return aziAnagraficaRepository.count(specification);
    }

    /**
     * Function to convert {@link AziAnagraficaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziAnagrafica> createSpecification(AziAnagraficaCriteria criteria) {
        Specification<AziAnagrafica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziAnagrafica_.id));
            }
            if (criteria.getCuaa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCuaa(), AziAnagrafica_.cuaa));
            }
            if (criteria.getPartitaiva() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPartitaiva(), AziAnagrafica_.partitaiva));
            }
            if (criteria.getCodicefiscale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodicefiscale(), AziAnagrafica_.codicefiscale));
            }
            if (criteria.getCodiceAteco() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceAteco(), AziAnagrafica_.codiceAteco));
            }
            if (criteria.getRagioneSociale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRagioneSociale(), AziAnagrafica_.ragioneSociale));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziAnagrafica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziAnagrafica_.dataFineVali));
            }
            if (criteria.getAttivo() != null) {
                specification = specification.and(buildSpecification(criteria.getAttivo(), AziAnagrafica_.attivo));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziAnagrafica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziAnagrafica_.userIdLastMod));
            }
            if (criteria.getAziAnaToCliId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToCliId(),
                    root -> root.join(AziAnagrafica_.aziAnaToClis, JoinType.LEFT).get(Cliente_.id)));
            }
            if (criteria.getAziToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziToIndId(),
                    root -> root.join(AziAnagrafica_.aziToInds, JoinType.LEFT).get(IndAzienda_.id)));
            }
            if (criteria.getAziToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziToRecId(),
                    root -> root.join(AziAnagrafica_.aziToRecs, JoinType.LEFT).get(RecAzienda_.id)));
            }
            if (criteria.getAziAnaToBroglId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToBroglId(),
                    root -> root.join(AziAnagrafica_.aziAnaToBrogls, JoinType.LEFT).get(Brogliaccio_.id)));
            }
            if (criteria.getNaturaGiuridicaId() != null) {
                specification = specification.and(buildSpecification(criteria.getNaturaGiuridicaId(),
                    root -> root.join(AziAnagrafica_.naturaGiuridica, JoinType.LEFT).get(AziFormaGiuridica_.id)));
            }
            if (criteria.getAziAnaToAziRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToAziRapId(),
                    root -> root.join(AziAnagrafica_.aziAnaToAziRap, JoinType.LEFT).get(AziRappresentante_.id)));
            }
            if (criteria.getAziAnaToUniAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToUniAnaId(),
                    root -> root.join(AziAnagrafica_.aziAnaToUniAnas, JoinType.LEFT).get(UniAnagrafica_.id)));
            }
            if (criteria.getAziAnaToOpeAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToOpeAnaId(),
                    root -> root.join(AziAnagrafica_.aziAnaToOpeAnas, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
            if (criteria.getAziAnaToUsrTerId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziAnaToUsrTerId(),
                    root -> root.join(AziAnagrafica_.aziAnaToUsrTers, JoinType.LEFT).get(UserTerram_.id)));
            }
        }
        return specification;
    }
}
