package com.terrambe.service;

import com.terrambe.domain.TitolareRegistrazione;
import com.terrambe.repository.TitolareRegistrazioneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TitolareRegistrazione}.
 */
@Service
@Transactional
public class TitolareRegistrazioneService {

    private final Logger log = LoggerFactory.getLogger(TitolareRegistrazioneService.class);

    private final TitolareRegistrazioneRepository titolareRegistrazioneRepository;

    public TitolareRegistrazioneService(TitolareRegistrazioneRepository titolareRegistrazioneRepository) {
        this.titolareRegistrazioneRepository = titolareRegistrazioneRepository;
    }

    /**
     * Save a titolareRegistrazione.
     *
     * @param titolareRegistrazione the entity to save.
     * @return the persisted entity.
     */
    public TitolareRegistrazione save(TitolareRegistrazione titolareRegistrazione) {
        log.debug("Request to save TitolareRegistrazione : {}", titolareRegistrazione);
        return titolareRegistrazioneRepository.save(titolareRegistrazione);
    }

    /**
     * Get all the titolareRegistraziones.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TitolareRegistrazione> findAll() {
        log.debug("Request to get all TitolareRegistraziones");
        return titolareRegistrazioneRepository.findAll();
    }


    /**
     * Get one titolareRegistrazione by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TitolareRegistrazione> findOne(Long id) {
        log.debug("Request to get TitolareRegistrazione : {}", id);
        return titolareRegistrazioneRepository.findById(id);
    }

    /**
     * Delete the titolareRegistrazione by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TitolareRegistrazione : {}", id);
        titolareRegistrazioneRepository.deleteById(id);
    }
}
