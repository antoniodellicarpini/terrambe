package com.terrambe.service;

import com.terrambe.domain.TipoTrappola;
import com.terrambe.repository.TipoTrappolaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoTrappola}.
 */
@Service
@Transactional
public class TipoTrappolaService {

    private final Logger log = LoggerFactory.getLogger(TipoTrappolaService.class);

    private final TipoTrappolaRepository tipoTrappolaRepository;

    public TipoTrappolaService(TipoTrappolaRepository tipoTrappolaRepository) {
        this.tipoTrappolaRepository = tipoTrappolaRepository;
    }

    /**
     * Save a tipoTrappola.
     *
     * @param tipoTrappola the entity to save.
     * @return the persisted entity.
     */
    public TipoTrappola save(TipoTrappola tipoTrappola) {
        log.debug("Request to save TipoTrappola : {}", tipoTrappola);
        return tipoTrappolaRepository.save(tipoTrappola);
    }

    /**
     * Get all the tipoTrappolas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoTrappola> findAll() {
        log.debug("Request to get all TipoTrappolas");
        return tipoTrappolaRepository.findAll();
    }


    /**
     * Get one tipoTrappola by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoTrappola> findOne(Long id) {
        log.debug("Request to get TipoTrappola : {}", id);
        return tipoTrappolaRepository.findById(id);
    }

    /**
     * Delete the tipoTrappola by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoTrappola : {}", id);
        tipoTrappolaRepository.deleteById(id);
    }
}
