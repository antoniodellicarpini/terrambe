package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.PartAppez;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.PartAppezRepository;
import com.terrambe.service.dto.PartAppezCriteria;

/**
 * Service for executing complex queries for {@link PartAppez} entities in the database.
 * The main input is a {@link PartAppezCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PartAppez} or a {@link Page} of {@link PartAppez} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PartAppezQueryService extends QueryService<PartAppez> {

    private final Logger log = LoggerFactory.getLogger(PartAppezQueryService.class);

    private final PartAppezRepository partAppezRepository;

    public PartAppezQueryService(PartAppezRepository partAppezRepository) {
        this.partAppezRepository = partAppezRepository;
    }

    /**
     * Return a {@link List} of {@link PartAppez} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PartAppez> findByCriteria(PartAppezCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PartAppez> specification = createSpecification(criteria);
        return partAppezRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link PartAppez} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PartAppez> findByCriteria(PartAppezCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PartAppez> specification = createSpecification(criteria);
        return partAppezRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PartAppezCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PartAppez> specification = createSpecification(criteria);
        return partAppezRepository.count(specification);
    }

    /**
     * Function to convert {@link PartAppezCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PartAppez> createSpecification(PartAppezCriteria criteria) {
        Specification<PartAppez> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PartAppez_.id));
            }
            if (criteria.getFoglio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFoglio(), PartAppez_.foglio));
            }
            if (criteria.getPart() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPart(), PartAppez_.part));
            }
            if (criteria.getSub() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSub(), PartAppez_.sub));
            }
            if (criteria.getSezione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSezione(), PartAppez_.sezione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), PartAppez_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), PartAppez_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), PartAppez_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), PartAppez_.userIdLastMod));
            }
            if (criteria.getPartAppzToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getPartAppzToAppzId(),
                    root -> root.join(PartAppez_.partAppzToAppz, JoinType.LEFT).get(Appezzamenti_.id)));
            }
            if (criteria.getPartAppzToPartId() != null) {
                specification = specification.and(buildSpecification(criteria.getPartAppzToPartId(),
                    root -> root.join(PartAppez_.partAppzToPart, JoinType.LEFT).get(AziSuperficieParticella_.id)));
            }
        }
        return specification;
    }
}
