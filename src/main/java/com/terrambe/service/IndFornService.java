package com.terrambe.service;

import com.terrambe.domain.IndForn;
import com.terrambe.repository.IndFornRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndForn}.
 */
@Service
@Transactional
public class IndFornService {

    private final Logger log = LoggerFactory.getLogger(IndFornService.class);

    private final IndFornRepository indFornRepository;

    public IndFornService(IndFornRepository indFornRepository) {
        this.indFornRepository = indFornRepository;
    }

    /**
     * Save a indForn.
     *
     * @param indForn the entity to save.
     * @return the persisted entity.
     */
    public IndForn save(IndForn indForn) {
        log.debug("Request to save IndForn : {}", indForn);
        return indFornRepository.save(indForn);
    }

    /**
     * Get all the indForns.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndForn> findAll() {
        log.debug("Request to get all IndForns");
        return indFornRepository.findAll();
    }


    /**
     * Get one indForn by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndForn> findOne(Long id) {
        log.debug("Request to get IndForn : {}", id);
        return indFornRepository.findById(id);
    }

    /**
     * Delete the indForn by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndForn : {}", id);
        indFornRepository.deleteById(id);
    }
}
