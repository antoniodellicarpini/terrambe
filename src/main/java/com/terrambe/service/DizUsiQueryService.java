package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DizUsi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DizUsiRepository;
import com.terrambe.service.dto.DizUsiCriteria;

/**
 * Service for executing complex queries for {@link DizUsi} entities in the database.
 * The main input is a {@link DizUsiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DizUsi} or a {@link Page} of {@link DizUsi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DizUsiQueryService extends QueryService<DizUsi> {

    private final Logger log = LoggerFactory.getLogger(DizUsiQueryService.class);

    private final DizUsiRepository dizUsiRepository;

    public DizUsiQueryService(DizUsiRepository dizUsiRepository) {
        this.dizUsiRepository = dizUsiRepository;
    }

    /**
     * Return a {@link List} of {@link DizUsi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DizUsi> findByCriteria(DizUsiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DizUsi> specification = createSpecification(criteria);
        return dizUsiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DizUsi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DizUsi> findByCriteria(DizUsiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DizUsi> specification = createSpecification(criteria);
        return dizUsiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DizUsiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DizUsi> specification = createSpecification(criteria);
        return dizUsiRepository.count(specification);
    }

    /**
     * Function to convert {@link DizUsiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DizUsi> createSpecification(DizUsiCriteria criteria) {
        Specification<DizUsi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DizUsi_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), DizUsi_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), DizUsi_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), DizUsi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), DizUsi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), DizUsi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), DizUsi_.userIdLastMod));
            }
        }
        return specification;
    }
}
