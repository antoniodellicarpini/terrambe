package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziRappresentante;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziRappresentanteRepository;
import com.terrambe.service.dto.AziRappresentanteCriteria;

/**
 * Service for executing complex queries for {@link AziRappresentante} entities in the database.
 * The main input is a {@link AziRappresentanteCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziRappresentante} or a {@link Page} of {@link AziRappresentante} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziRappresentanteQueryService extends QueryService<AziRappresentante> {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteQueryService.class);

    private final AziRappresentanteRepository aziRappresentanteRepository;

    public AziRappresentanteQueryService(AziRappresentanteRepository aziRappresentanteRepository) {
        this.aziRappresentanteRepository = aziRappresentanteRepository;
    }

    /**
     * Return a {@link List} of {@link AziRappresentante} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziRappresentante> findByCriteria(AziRappresentanteCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziRappresentante> specification = createSpecification(criteria);
        return aziRappresentanteRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziRappresentante} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziRappresentante> findByCriteria(AziRappresentanteCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziRappresentante> specification = createSpecification(criteria);
        return aziRappresentanteRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziRappresentanteCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziRappresentante> specification = createSpecification(criteria);
        return aziRappresentanteRepository.count(specification);
    }

    /**
     * Function to convert {@link AziRappresentanteCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziRappresentante> createSpecification(AziRappresentanteCriteria criteria) {
        Specification<AziRappresentante> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziRappresentante_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), AziRappresentante_.nome));
            }
            if (criteria.getCognome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCognome(), AziRappresentante_.cognome));
            }
            if (criteria.getSesso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSesso(), AziRappresentante_.sesso));
            }
            if (criteria.getDataNascita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataNascita(), AziRappresentante_.dataNascita));
            }
            if (criteria.getCodiceFiscale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceFiscale(), AziRappresentante_.codiceFiscale));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziRappresentante_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziRappresentante_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziRappresentante_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziRappresentante_.userIdLastMod));
            }
            if (criteria.getAziRapToIndNascId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziRapToIndNascId(),
                    root -> root.join(AziRappresentante_.aziRapToIndNasc, JoinType.LEFT).get(IndNascRap_.id)));
            }
            if (criteria.getAziRapToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziRapToIndId(),
                    root -> root.join(AziRappresentante_.aziRapToInds, JoinType.LEFT).get(IndAziRap_.id)));
            }
            if (criteria.getAziRapToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziRapToRecId(),
                    root -> root.join(AziRappresentante_.aziRapToRecs, JoinType.LEFT).get(RecAziRap_.id)));
            }
            if (criteria.getAziRapToAziAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziRapToAziAnaId(),
                    root -> root.join(AziRappresentante_.aziRapToAziAnas, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
        }
        return specification;
    }
}
