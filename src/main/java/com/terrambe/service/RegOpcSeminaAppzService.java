package com.terrambe.service;

import com.terrambe.domain.RegOpcSeminaAppz;
import com.terrambe.repository.RegOpcSeminaAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcSeminaAppz}.
 */
@Service
@Transactional
public class RegOpcSeminaAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcSeminaAppzService.class);

    private final RegOpcSeminaAppzRepository regOpcSeminaAppzRepository;

    public RegOpcSeminaAppzService(RegOpcSeminaAppzRepository regOpcSeminaAppzRepository) {
        this.regOpcSeminaAppzRepository = regOpcSeminaAppzRepository;
    }

    /**
     * Save a regOpcSeminaAppz.
     *
     * @param regOpcSeminaAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcSeminaAppz save(RegOpcSeminaAppz regOpcSeminaAppz) {
        log.debug("Request to save RegOpcSeminaAppz : {}", regOpcSeminaAppz);
        return regOpcSeminaAppzRepository.save(regOpcSeminaAppz);
    }

    /**
     * Get all the regOpcSeminaAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcSeminaAppz> findAll() {
        log.debug("Request to get all RegOpcSeminaAppzs");
        return regOpcSeminaAppzRepository.findAll();
    }


    /**
     * Get one regOpcSeminaAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcSeminaAppz> findOne(Long id) {
        log.debug("Request to get RegOpcSeminaAppz : {}", id);
        return regOpcSeminaAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcSeminaAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcSeminaAppz : {}", id);
        regOpcSeminaAppzRepository.deleteById(id);
    }
}
