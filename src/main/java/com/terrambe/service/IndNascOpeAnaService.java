package com.terrambe.service;

import com.terrambe.domain.IndNascOpeAna;
import com.terrambe.repository.IndNascOpeAnaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link IndNascOpeAna}.
 */
@Service
@Transactional
public class IndNascOpeAnaService {

    private final Logger log = LoggerFactory.getLogger(IndNascOpeAnaService.class);

    private final IndNascOpeAnaRepository indNascOpeAnaRepository;

    public IndNascOpeAnaService(IndNascOpeAnaRepository indNascOpeAnaRepository) {
        this.indNascOpeAnaRepository = indNascOpeAnaRepository;
    }

    /**
     * Save a indNascOpeAna.
     *
     * @param indNascOpeAna the entity to save.
     * @return the persisted entity.
     */
    public IndNascOpeAna save(IndNascOpeAna indNascOpeAna) {
        log.debug("Request to save IndNascOpeAna : {}", indNascOpeAna);
        return indNascOpeAnaRepository.save(indNascOpeAna);
    }

    /**
     * Get all the indNascOpeAnas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascOpeAna> findAll() {
        log.debug("Request to get all IndNascOpeAnas");
        return indNascOpeAnaRepository.findAll();
    }



    /**
    *  Get all the indNascOpeAnas where IndNascToOpeAna is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<IndNascOpeAna> findAllWhereIndNascToOpeAnaIsNull() {
        log.debug("Request to get all indNascOpeAnas where IndNascToOpeAna is null");
        return StreamSupport
            .stream(indNascOpeAnaRepository.findAll().spliterator(), false)
            .filter(indNascOpeAna -> indNascOpeAna.getIndNascToOpeAna() == null)
            .collect(Collectors.toList());
    }

    /**
     * Get one indNascOpeAna by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndNascOpeAna> findOne(Long id) {
        log.debug("Request to get IndNascOpeAna : {}", id);
        return indNascOpeAnaRepository.findById(id);
    }

    /**
     * Delete the indNascOpeAna by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndNascOpeAna : {}", id);
        indNascOpeAnaRepository.deleteById(id);
    }
}
