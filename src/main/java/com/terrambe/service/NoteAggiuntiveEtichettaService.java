package com.terrambe.service;

import com.terrambe.domain.NoteAggiuntiveEtichetta;
import com.terrambe.repository.NoteAggiuntiveEtichettaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link NoteAggiuntiveEtichetta}.
 */
@Service
@Transactional
public class NoteAggiuntiveEtichettaService {

    private final Logger log = LoggerFactory.getLogger(NoteAggiuntiveEtichettaService.class);

    private final NoteAggiuntiveEtichettaRepository noteAggiuntiveEtichettaRepository;

    public NoteAggiuntiveEtichettaService(NoteAggiuntiveEtichettaRepository noteAggiuntiveEtichettaRepository) {
        this.noteAggiuntiveEtichettaRepository = noteAggiuntiveEtichettaRepository;
    }

    /**
     * Save a noteAggiuntiveEtichetta.
     *
     * @param noteAggiuntiveEtichetta the entity to save.
     * @return the persisted entity.
     */
    public NoteAggiuntiveEtichetta save(NoteAggiuntiveEtichetta noteAggiuntiveEtichetta) {
        log.debug("Request to save NoteAggiuntiveEtichetta : {}", noteAggiuntiveEtichetta);
        return noteAggiuntiveEtichettaRepository.save(noteAggiuntiveEtichetta);
    }

    /**
     * Get all the noteAggiuntiveEtichettas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NoteAggiuntiveEtichetta> findAll() {
        log.debug("Request to get all NoteAggiuntiveEtichettas");
        return noteAggiuntiveEtichettaRepository.findAll();
    }


    /**
     * Get one noteAggiuntiveEtichetta by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NoteAggiuntiveEtichetta> findOne(Long id) {
        log.debug("Request to get NoteAggiuntiveEtichetta : {}", id);
        return noteAggiuntiveEtichettaRepository.findById(id);
    }

    /**
     * Delete the noteAggiuntiveEtichetta by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NoteAggiuntiveEtichetta : {}", id);
        noteAggiuntiveEtichettaRepository.deleteById(id);
    }
}
