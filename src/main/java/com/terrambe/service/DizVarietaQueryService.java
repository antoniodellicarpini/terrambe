package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.DizVarieta;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DizVarietaRepository;
import com.terrambe.service.dto.DizVarietaCriteria;

/**
 * Service for executing complex queries for {@link DizVarieta} entities in the database.
 * The main input is a {@link DizVarietaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DizVarieta} or a {@link Page} of {@link DizVarieta} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DizVarietaQueryService extends QueryService<DizVarieta> {

    private final Logger log = LoggerFactory.getLogger(DizVarietaQueryService.class);

    private final DizVarietaRepository dizVarietaRepository;

    public DizVarietaQueryService(DizVarietaRepository dizVarietaRepository) {
        this.dizVarietaRepository = dizVarietaRepository;
    }

    /**
     * Return a {@link List} of {@link DizVarieta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DizVarieta> findByCriteria(DizVarietaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DizVarieta> specification = createSpecification(criteria);
        return dizVarietaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DizVarieta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DizVarieta> findByCriteria(DizVarietaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DizVarieta> specification = createSpecification(criteria);
        return dizVarietaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DizVarietaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DizVarieta> specification = createSpecification(criteria);
        return dizVarietaRepository.count(specification);
    }

    /**
     * Function to convert {@link DizVarietaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DizVarieta> createSpecification(DizVarietaCriteria criteria) {
        Specification<DizVarieta> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DizVarieta_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), DizVarieta_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), DizVarieta_.descrizione));
            }
            if (criteria.getCodOccupazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodOccupazione(), DizVarieta_.codOccupazione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), DizVarieta_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), DizVarieta_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), DizVarieta_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), DizVarieta_.userIdLastMod));
            }
        }
        return specification;
    }
}
