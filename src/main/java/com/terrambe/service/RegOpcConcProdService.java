package com.terrambe.service;

import com.terrambe.domain.RegOpcConcProd;
import com.terrambe.repository.RegOpcConcProdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcConcProd}.
 */
@Service
@Transactional
public class RegOpcConcProdService {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcProdService.class);

    private final RegOpcConcProdRepository regOpcConcProdRepository;

    public RegOpcConcProdService(RegOpcConcProdRepository regOpcConcProdRepository) {
        this.regOpcConcProdRepository = regOpcConcProdRepository;
    }

    /**
     * Save a regOpcConcProd.
     *
     * @param regOpcConcProd the entity to save.
     * @return the persisted entity.
     */
    public RegOpcConcProd save(RegOpcConcProd regOpcConcProd) {
        log.debug("Request to save RegOpcConcProd : {}", regOpcConcProd);
        return regOpcConcProdRepository.save(regOpcConcProd);
    }

    /**
     * Get all the regOpcConcProds.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConcProd> findAll() {
        log.debug("Request to get all RegOpcConcProds");
        return regOpcConcProdRepository.findAll();
    }


    /**
     * Get one regOpcConcProd by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcConcProd> findOne(Long id) {
        log.debug("Request to get RegOpcConcProd : {}", id);
        return regOpcConcProdRepository.findById(id);
    }

    /**
     * Delete the regOpcConcProd by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcConcProd : {}", id);
        regOpcConcProdRepository.deleteById(id);
    }
}
