package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Dose;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.DoseRepository;
import com.terrambe.service.dto.DoseCriteria;

/**
 * Service for executing complex queries for {@link Dose} entities in the database.
 * The main input is a {@link DoseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Dose} or a {@link Page} of {@link Dose} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DoseQueryService extends QueryService<Dose> {

    private final Logger log = LoggerFactory.getLogger(DoseQueryService.class);

    private final DoseRepository doseRepository;

    public DoseQueryService(DoseRepository doseRepository) {
        this.doseRepository = doseRepository;
    }

    /**
     * Return a {@link List} of {@link Dose} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Dose> findByCriteria(DoseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Dose> specification = createSpecification(criteria);
        return doseRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Dose} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Dose> findByCriteria(DoseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Dose> specification = createSpecification(criteria);
        return doseRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DoseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Dose> specification = createSpecification(criteria);
        return doseRepository.count(specification);
    }

    /**
     * Function to convert {@link DoseCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Dose> createSpecification(DoseCriteria criteria) {
        Specification<Dose> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Dose_.id));
            }
            if (criteria.getDoseMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoseMin(), Dose_.doseMin));
            }
            if (criteria.getDoseMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoseMax(), Dose_.doseMax));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Dose_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Dose_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Dose_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Dose_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Dose_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Dose_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Dose_.userIdLastMod));
            }
            if (criteria.getDoseToDosagId() != null) {
                specification = specification.and(buildSpecification(criteria.getDoseToDosagId(),
                    root -> root.join(Dose_.doseToDosag, JoinType.LEFT).get(Dosaggio_.id)));
            }
            if (criteria.getDoseToUniMisuId() != null) {
                specification = specification.and(buildSpecification(criteria.getDoseToUniMisuId(),
                    root -> root.join(Dose_.doseToUniMisu, JoinType.LEFT).get(UnitaMisura_.id)));
            }
        }
        return specification;
    }
}
