package com.terrambe.service;

import com.terrambe.domain.MotivoConc;
import com.terrambe.repository.MotivoConcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MotivoConc}.
 */
@Service
@Transactional
public class MotivoConcService {

    private final Logger log = LoggerFactory.getLogger(MotivoConcService.class);

    private final MotivoConcRepository motivoConcRepository;

    public MotivoConcService(MotivoConcRepository motivoConcRepository) {
        this.motivoConcRepository = motivoConcRepository;
    }

    /**
     * Save a motivoConc.
     *
     * @param motivoConc the entity to save.
     * @return the persisted entity.
     */
    public MotivoConc save(MotivoConc motivoConc) {
        log.debug("Request to save MotivoConc : {}", motivoConc);
        return motivoConcRepository.save(motivoConc);
    }

    /**
     * Get all the motivoConcs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MotivoConc> findAll() {
        log.debug("Request to get all MotivoConcs");
        return motivoConcRepository.findAll();
    }


    /**
     * Get one motivoConc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MotivoConc> findOne(Long id) {
        log.debug("Request to get MotivoConc : {}", id);
        return motivoConcRepository.findById(id);
    }

    /**
     * Delete the motivoConc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MotivoConc : {}", id);
        motivoConcRepository.deleteById(id);
    }
}
