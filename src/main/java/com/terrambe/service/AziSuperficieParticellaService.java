package com.terrambe.service;

import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.repository.AziSuperficieParticellaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziSuperficieParticella}.
 */
@Service
@Transactional
public class AziSuperficieParticellaService {

    private final Logger log = LoggerFactory.getLogger(AziSuperficieParticellaService.class);

    private final AziSuperficieParticellaRepository aziSuperficieParticellaRepository;

    public AziSuperficieParticellaService(AziSuperficieParticellaRepository aziSuperficieParticellaRepository) {
        this.aziSuperficieParticellaRepository = aziSuperficieParticellaRepository;
    }

    /**
     * Save a aziSuperficieParticella.
     *
     * @param aziSuperficieParticella the entity to save.
     * @return the persisted entity.
     */
    public AziSuperficieParticella save(AziSuperficieParticella aziSuperficieParticella) {
        log.debug("Request to save AziSuperficieParticella : {}", aziSuperficieParticella);
        return aziSuperficieParticellaRepository.save(aziSuperficieParticella);
    }

    /**
     * Get all the aziSuperficieParticellas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziSuperficieParticella> findAll() {
        log.debug("Request to get all AziSuperficieParticellas");
        return aziSuperficieParticellaRepository.findAll();
    }


    /**
     * Get one aziSuperficieParticella by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziSuperficieParticella> findOne(Long id) {
        log.debug("Request to get AziSuperficieParticella : {}", id);
        return aziSuperficieParticellaRepository.findById(id);
    }

    /**
     * Delete the aziSuperficieParticella by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziSuperficieParticella : {}", id);
        aziSuperficieParticellaRepository.deleteById(id);
    }
        /**
     * Aggiunge l'id della relaziona campi to particelle nell'apoisuta particella.
     *
     * @param  idCampo, idParticella
     * */

    public void addCampiIdToExistingParticella(String idCampo, String idParticella) {
        log.debug("Request to save campi id into particella");
        aziSuperficieParticellaRepository.addCampiIdToExistingParticella(idCampo, idParticella);
    }

}
