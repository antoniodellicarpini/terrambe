package com.terrambe.service;

import com.terrambe.domain.AtrAnagrafica;
import com.terrambe.repository.AtrAnagraficaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AtrAnagrafica}.
 */
@Service
@Transactional
public class AtrAnagraficaService {

    private final Logger log = LoggerFactory.getLogger(AtrAnagraficaService.class);

    private final AtrAnagraficaRepository atrAnagraficaRepository;

    public AtrAnagraficaService(AtrAnagraficaRepository atrAnagraficaRepository) {
        this.atrAnagraficaRepository = atrAnagraficaRepository;
    }

    /**
     * Save a atrAnagrafica.
     *
     * @param atrAnagrafica the entity to save.
     * @return the persisted entity.
     */
    public AtrAnagrafica save(AtrAnagrafica atrAnagrafica) {
        log.debug("Request to save AtrAnagrafica : {}", atrAnagrafica);
        return atrAnagraficaRepository.save(atrAnagrafica);
    }

    /**
     * Get all the atrAnagraficas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AtrAnagrafica> findAll() {
        log.debug("Request to get all AtrAnagraficas");
        return atrAnagraficaRepository.findAll();
    }


    /**
     * Get one atrAnagrafica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AtrAnagrafica> findOne(Long id) {
        log.debug("Request to get AtrAnagrafica : {}", id);
        return atrAnagraficaRepository.findById(id);
    }

    /**
     * Delete the atrAnagrafica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AtrAnagrafica : {}", id);
        atrAnagraficaRepository.deleteById(id);
    }
}
