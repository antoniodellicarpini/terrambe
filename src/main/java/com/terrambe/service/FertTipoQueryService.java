package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.FertTipo;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FertTipoRepository;
import com.terrambe.service.dto.FertTipoCriteria;

/**
 * Service for executing complex queries for {@link FertTipo} entities in the database.
 * The main input is a {@link FertTipoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FertTipo} or a {@link Page} of {@link FertTipo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FertTipoQueryService extends QueryService<FertTipo> {

    private final Logger log = LoggerFactory.getLogger(FertTipoQueryService.class);

    private final FertTipoRepository fertTipoRepository;

    public FertTipoQueryService(FertTipoRepository fertTipoRepository) {
        this.fertTipoRepository = fertTipoRepository;
    }

    /**
     * Return a {@link List} of {@link FertTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FertTipo> findByCriteria(FertTipoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FertTipo> specification = createSpecification(criteria);
        return fertTipoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FertTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FertTipo> findByCriteria(FertTipoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FertTipo> specification = createSpecification(criteria);
        return fertTipoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FertTipoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FertTipo> specification = createSpecification(criteria);
        return fertTipoRepository.count(specification);
    }

    /**
     * Function to convert {@link FertTipoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FertTipo> createSpecification(FertTipoCriteria criteria) {
        Specification<FertTipo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FertTipo_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), FertTipo_.descrizione));
            }
            if (criteria.getFerAnagraficaId() != null) {
                specification = specification.and(buildSpecification(criteria.getFerAnagraficaId(),
                    root -> root.join(FertTipo_.ferAnagraficas, JoinType.LEFT).get(FerAnagrafica_.id)));
            }
        }
        return specification;
    }
}
