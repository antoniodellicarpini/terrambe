package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziFormaGiuridica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziFormaGiuridicaRepository;
import com.terrambe.service.dto.AziFormaGiuridicaCriteria;

/**
 * Service for executing complex queries for {@link AziFormaGiuridica} entities in the database.
 * The main input is a {@link AziFormaGiuridicaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziFormaGiuridica} or a {@link Page} of {@link AziFormaGiuridica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziFormaGiuridicaQueryService extends QueryService<AziFormaGiuridica> {

    private final Logger log = LoggerFactory.getLogger(AziFormaGiuridicaQueryService.class);

    private final AziFormaGiuridicaRepository aziFormaGiuridicaRepository;

    public AziFormaGiuridicaQueryService(AziFormaGiuridicaRepository aziFormaGiuridicaRepository) {
        this.aziFormaGiuridicaRepository = aziFormaGiuridicaRepository;
    }

    /**
     * Return a {@link List} of {@link AziFormaGiuridica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziFormaGiuridica> findByCriteria(AziFormaGiuridicaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziFormaGiuridica> specification = createSpecification(criteria);
        return aziFormaGiuridicaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziFormaGiuridica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziFormaGiuridica> findByCriteria(AziFormaGiuridicaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziFormaGiuridica> specification = createSpecification(criteria);
        return aziFormaGiuridicaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziFormaGiuridicaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziFormaGiuridica> specification = createSpecification(criteria);
        return aziFormaGiuridicaRepository.count(specification);
    }

    /**
     * Function to convert {@link AziFormaGiuridicaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziFormaGiuridica> createSpecification(AziFormaGiuridicaCriteria criteria) {
        Specification<AziFormaGiuridica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziFormaGiuridica_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), AziFormaGiuridica_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziFormaGiuridica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziFormaGiuridica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziFormaGiuridica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziFormaGiuridica_.userIdLastMod));
            }
        }
        return specification;
    }
}
