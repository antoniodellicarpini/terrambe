package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.EtichettaFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.EtichettaFitoRepository;
import com.terrambe.service.dto.EtichettaFitoCriteria;

/**
 * Service for executing complex queries for {@link EtichettaFito} entities in the database.
 * The main input is a {@link EtichettaFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtichettaFito} or a {@link Page} of {@link EtichettaFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtichettaFitoQueryService extends QueryService<EtichettaFito> {

    private final Logger log = LoggerFactory.getLogger(EtichettaFitoQueryService.class);

    private final EtichettaFitoRepository etichettaFitoRepository;

    public EtichettaFitoQueryService(EtichettaFitoRepository etichettaFitoRepository) {
        this.etichettaFitoRepository = etichettaFitoRepository;
    }

    /**
     * Return a {@link List} of {@link EtichettaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaFito> findByCriteria(EtichettaFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EtichettaFito> specification = createSpecification(criteria);
        return etichettaFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EtichettaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtichettaFito> findByCriteria(EtichettaFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EtichettaFito> specification = createSpecification(criteria);
        return etichettaFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtichettaFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EtichettaFito> specification = createSpecification(criteria);
        return etichettaFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link EtichettaFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EtichettaFito> createSpecification(EtichettaFitoCriteria criteria) {
        Specification<EtichettaFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EtichettaFito_.id));
            }
            if (criteria.getNomeCommerciale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeCommerciale(), EtichettaFito_.nomeCommerciale));
            }
            if (criteria.getDataReg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataReg(), EtichettaFito_.dataReg));
            }
            if (criteria.getRegistrazioneMinisteriale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegistrazioneMinisteriale(), EtichettaFito_.registrazioneMinisteriale));
            }
            if (criteria.getSigla() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSigla(), EtichettaFito_.sigla));
            }
            if (criteria.getBio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBio(), EtichettaFito_.bio));
            }
            if (criteria.getPpo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPpo(), EtichettaFito_.ppo));
            }
            if (criteria.getRevocato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRevocato(), EtichettaFito_.revocato));
            }
            if (criteria.getRevocaAutorizzazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRevocaAutorizzazione(), EtichettaFito_.revocaAutorizzazione));
            }
            if (criteria.getScadenzaCommercio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScadenzaCommercio(), EtichettaFito_.scadenzaCommercio));
            }
            if (criteria.getScadenzaUtilizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScadenzaUtilizzo(), EtichettaFito_.scadenzaUtilizzo));
            }
            if (criteria.getAvvClp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAvvClp(), EtichettaFito_.avvClp));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), EtichettaFito_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), EtichettaFito_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), EtichettaFito_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), EtichettaFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), EtichettaFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), EtichettaFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), EtichettaFito_.userIdLastMod));
            }
            if (criteria.getIncompatibilitaetichettaId() != null) {
                specification = specification.and(buildSpecification(criteria.getIncompatibilitaetichettaId(),
                    root -> root.join(EtichettaFito_.incompatibilitaetichettas, JoinType.LEFT).get(IncompatibilitaSA_.id)));
            }
            if (criteria.getProbfitotossicitaId() != null) {
                specification = specification.and(buildSpecification(criteria.getProbfitotossicitaId(),
                    root -> root.join(EtichettaFito_.probfitotossicitas, JoinType.LEFT).get(ProbabileFitoTossicita_.id)));
            }
            if (criteria.getNoteagguntiveetiId() != null) {
                specification = specification.and(buildSpecification(criteria.getNoteagguntiveetiId(),
                    root -> root.join(EtichettaFito_.noteagguntiveetis, JoinType.LEFT).get(NoteAggiuntiveEtichetta_.id)));
            }
            if (criteria.getEtisostanzeattiveId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtisostanzeattiveId(),
                    root -> root.join(EtichettaFito_.etisostanzeattives, JoinType.LEFT).get(EtichettaSostanzeAttive_.id)));
            }
            if (criteria.getEtichettafrasiId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettafrasiId(),
                    root -> root.join(EtichettaFito_.etichettafrasis, JoinType.LEFT).get(EtichettaFrasi_.id)));
            }
            if (criteria.getEtichettapittogrammiId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettapittogrammiId(),
                    root -> root.join(EtichettaFito_.etichettapittogrammis, JoinType.LEFT).get(EtichettaPittogrammi_.id)));
            }
            if (criteria.getEtichettatipofitofarmacoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettatipofitofarmacoId(),
                    root -> root.join(EtichettaFito_.etichettatipofitofarmacos, JoinType.LEFT).get(EtiTipologiaFito_.id)));
            }
            if (criteria.getEtiFitoToRaccordoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtiFitoToRaccordoId(),
                    root -> root.join(EtichettaFito_.etiFitoToRaccordos, JoinType.LEFT).get(TabellaRaccordo_.id)));
            }
            if (criteria.getTitolareRegistrazioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getTitolareRegistrazioneId(),
                    root -> root.join(EtichettaFito_.titolareRegistrazione, JoinType.LEFT).get(TitolareRegistrazione_.id)));
            }
            if (criteria.getFormulazioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getFormulazioneId(),
                    root -> root.join(EtichettaFito_.formulazione, JoinType.LEFT).get(Formulazione_.id)));
            }
        }
        return specification;
    }
}
