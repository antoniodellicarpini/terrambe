package com.terrambe.service;

import com.terrambe.domain.TipologieOpc;
import com.terrambe.repository.TipologieOpcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipologieOpc}.
 */
@Service
@Transactional
public class TipologieOpcService {

    private final Logger log = LoggerFactory.getLogger(TipologieOpcService.class);

    private final TipologieOpcRepository tipologieOpcRepository;

    public TipologieOpcService(TipologieOpcRepository tipologieOpcRepository) {
        this.tipologieOpcRepository = tipologieOpcRepository;
    }

    /**
     * Save a tipologieOpc.
     *
     * @param tipologieOpc the entity to save.
     * @return the persisted entity.
     */
    public TipologieOpc save(TipologieOpc tipologieOpc) {
        log.debug("Request to save TipologieOpc : {}", tipologieOpc);
        return tipologieOpcRepository.save(tipologieOpc);
    }

    /**
     * Get all the tipologieOpcs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipologieOpc> findAll() {
        log.debug("Request to get all TipologieOpcs");
        return tipologieOpcRepository.findAll();
    }


    /**
     * Get one tipologieOpc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipologieOpc> findOne(Long id) {
        log.debug("Request to get TipologieOpc : {}", id);
        return tipologieOpcRepository.findById(id);
    }

    /**
     * Delete the tipologieOpc by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipologieOpc : {}", id);
        tipologieOpcRepository.deleteById(id);
    }
}
