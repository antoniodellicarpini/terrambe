package com.terrambe.service;

import com.terrambe.domain.IndAziendaCli;
import com.terrambe.repository.IndAziendaCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndAziendaCli}.
 */
@Service
@Transactional
public class IndAziendaCliService {

    private final Logger log = LoggerFactory.getLogger(IndAziendaCliService.class);

    private final IndAziendaCliRepository indAziendaCliRepository;

    public IndAziendaCliService(IndAziendaCliRepository indAziendaCliRepository) {
        this.indAziendaCliRepository = indAziendaCliRepository;
    }

    /**
     * Save a indAziendaCli.
     *
     * @param indAziendaCli the entity to save.
     * @return the persisted entity.
     */
    public IndAziendaCli save(IndAziendaCli indAziendaCli) {
        log.debug("Request to save IndAziendaCli : {}", indAziendaCli);
        return indAziendaCliRepository.save(indAziendaCli);
    }

    /**
     * Get all the indAziendaClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziendaCli> findAll() {
        log.debug("Request to get all IndAziendaClis");
        return indAziendaCliRepository.findAll();
    }


    /**
     * Get one indAziendaCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndAziendaCli> findOne(Long id) {
        log.debug("Request to get IndAziendaCli : {}", id);
        return indAziendaCliRepository.findById(id);
    }

    /**
     * Delete the indAziendaCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndAziendaCli : {}", id);
        indAziendaCliRepository.deleteById(id);
    }
}
