package com.terrambe.service;

import com.terrambe.domain.OpeAnagrafica;
import com.terrambe.repository.OpeAnagraficaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OpeAnagrafica}.
 */
@Service
@Transactional
public class OpeAnagraficaService {

    private final Logger log = LoggerFactory.getLogger(OpeAnagraficaService.class);

    private final OpeAnagraficaRepository opeAnagraficaRepository;

    public OpeAnagraficaService(OpeAnagraficaRepository opeAnagraficaRepository) {
        this.opeAnagraficaRepository = opeAnagraficaRepository;
    }

    /**
     * Save a opeAnagrafica.
     *
     * @param opeAnagrafica the entity to save.
     * @return the persisted entity.
     */
    public OpeAnagrafica save(OpeAnagrafica opeAnagrafica) {
        log.debug("Request to save OpeAnagrafica : {}", opeAnagrafica);
        return opeAnagraficaRepository.save(opeAnagrafica);
    }

    /**
     * Get all the opeAnagraficas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OpeAnagrafica> findAll() {
        log.debug("Request to get all OpeAnagraficas");
        return opeAnagraficaRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the opeAnagraficas with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<OpeAnagrafica> findAllWithEagerRelationships(Pageable pageable) {
        return opeAnagraficaRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one opeAnagrafica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OpeAnagrafica> findOne(Long id) {
        log.debug("Request to get OpeAnagrafica : {}", id);
        return opeAnagraficaRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the opeAnagrafica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OpeAnagrafica : {}", id);
        opeAnagraficaRepository.deleteById(id);
    }
}
