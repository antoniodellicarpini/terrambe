package com.terrambe.service;

import com.terrambe.domain.TipoAltro;
import com.terrambe.repository.TipoAltroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoAltro}.
 */
@Service
@Transactional
public class TipoAltroService {

    private final Logger log = LoggerFactory.getLogger(TipoAltroService.class);

    private final TipoAltroRepository tipoAltroRepository;

    public TipoAltroService(TipoAltroRepository tipoAltroRepository) {
        this.tipoAltroRepository = tipoAltroRepository;
    }

    /**
     * Save a tipoAltro.
     *
     * @param tipoAltro the entity to save.
     * @return the persisted entity.
     */
    public TipoAltro save(TipoAltro tipoAltro) {
        log.debug("Request to save TipoAltro : {}", tipoAltro);
        return tipoAltroRepository.save(tipoAltro);
    }

    /**
     * Get all the tipoAltros.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoAltro> findAll() {
        log.debug("Request to get all TipoAltros");
        return tipoAltroRepository.findAll();
    }


    /**
     * Get one tipoAltro by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoAltro> findOne(Long id) {
        log.debug("Request to get TipoAltro : {}", id);
        return tipoAltroRepository.findById(id);
    }

    /**
     * Delete the tipoAltro by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoAltro : {}", id);
        tipoAltroRepository.deleteById(id);
    }
}
