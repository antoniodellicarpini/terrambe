package com.terrambe.service;

import com.terrambe.domain.AziRappresentante;
import com.terrambe.repository.AziRappresentanteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AziRappresentante}.
 */
@Service
@Transactional
public class AziRappresentanteService {

    private final Logger log = LoggerFactory.getLogger(AziRappresentanteService.class);

    private final AziRappresentanteRepository aziRappresentanteRepository;

    public AziRappresentanteService(AziRappresentanteRepository aziRappresentanteRepository) {
        this.aziRappresentanteRepository = aziRappresentanteRepository;
    }

    /**
     * Save a aziRappresentante.
     *
     * @param aziRappresentante the entity to save.
     * @return the persisted entity.
     */
    public AziRappresentante save(AziRappresentante aziRappresentante) {
        log.debug("Request to save AziRappresentante : {}", aziRappresentante);
        return aziRappresentanteRepository.save(aziRappresentante);
    }

    /**
     * Get all the aziRappresentantes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AziRappresentante> findAll() {
        log.debug("Request to get all AziRappresentantes");
        return aziRappresentanteRepository.findAll();
    }


    /**
     * Get one aziRappresentante by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AziRappresentante> findOne(Long id) {
        log.debug("Request to get AziRappresentante : {}", id);
        return aziRappresentanteRepository.findById(id);
    }

    /**
     * Delete the aziRappresentante by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AziRappresentante : {}", id);
        aziRappresentanteRepository.deleteById(id);
    }
}
