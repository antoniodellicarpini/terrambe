package com.terrambe.service;

import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.repository.EtichettaFrasiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EtichettaFrasi}.
 */
@Service
@Transactional
public class EtichettaFrasiService {

    private final Logger log = LoggerFactory.getLogger(EtichettaFrasiService.class);

    private final EtichettaFrasiRepository etichettaFrasiRepository;

    public EtichettaFrasiService(EtichettaFrasiRepository etichettaFrasiRepository) {
        this.etichettaFrasiRepository = etichettaFrasiRepository;
    }

    /**
     * Save a etichettaFrasi.
     *
     * @param etichettaFrasi the entity to save.
     * @return the persisted entity.
     */
    public EtichettaFrasi save(EtichettaFrasi etichettaFrasi) {
        log.debug("Request to save EtichettaFrasi : {}", etichettaFrasi);
        return etichettaFrasiRepository.save(etichettaFrasi);
    }

    /**
     * Get all the etichettaFrasis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaFrasi> findAll() {
        log.debug("Request to get all EtichettaFrasis");
        return etichettaFrasiRepository.findAll();
    }


    /**
     * Get one etichettaFrasi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EtichettaFrasi> findOne(Long id) {
        log.debug("Request to get EtichettaFrasi : {}", id);
        return etichettaFrasiRepository.findById(id);
    }

    /**
     * Delete the etichettaFrasi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EtichettaFrasi : {}", id);
        etichettaFrasiRepository.deleteById(id);
    }
}
