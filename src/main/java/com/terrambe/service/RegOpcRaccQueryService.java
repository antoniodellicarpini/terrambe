package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcRacc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcRaccRepository;
import com.terrambe.service.dto.RegOpcRaccCriteria;

/**
 * Service for executing complex queries for {@link RegOpcRacc} entities in the database.
 * The main input is a {@link RegOpcRaccCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcRacc} or a {@link Page} of {@link RegOpcRacc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcRaccQueryService extends QueryService<RegOpcRacc> {

    private final Logger log = LoggerFactory.getLogger(RegOpcRaccQueryService.class);

    private final RegOpcRaccRepository regOpcRaccRepository;

    public RegOpcRaccQueryService(RegOpcRaccRepository regOpcRaccRepository) {
        this.regOpcRaccRepository = regOpcRaccRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcRacc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcRacc> findByCriteria(RegOpcRaccCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcRacc> specification = createSpecification(criteria);
        return regOpcRaccRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcRacc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcRacc> findByCriteria(RegOpcRaccCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcRacc> specification = createSpecification(criteria);
        return regOpcRaccRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcRaccCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcRacc> specification = createSpecification(criteria);
        return regOpcRaccRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcRaccCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcRacc> createSpecification(RegOpcRaccCriteria criteria) {
        Specification<RegOpcRacc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcRacc_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcRacc_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcRacc_.idUnitaProd));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcRacc_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcRacc_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcRacc_.tempoImpiegato));
            }
            if (criteria.getLocazioneConferimento() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocazioneConferimento(), RegOpcRacc_.locazioneConferimento));
            }
            if (criteria.getQuantita() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantita(), RegOpcRacc_.quantita));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcRacc_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcRacc_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcRacc_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcRacc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcRacc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcRacc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcRacc_.userIdLastMod));
            }
            if (criteria.getOpcRaccToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcRaccToAppzId(),
                    root -> root.join(RegOpcRacc_.opcRaccToAppzs, JoinType.LEFT).get(RegOpcRaccAppz_.id)));
            }
            if (criteria.getOpcRaccToTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcRaccToTipoId(),
                    root -> root.join(RegOpcRacc_.opcRaccToTipo, JoinType.LEFT).get(TipoRacc_.id)));
            }
        }
        return specification;
    }
}
