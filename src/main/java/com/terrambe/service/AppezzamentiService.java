package com.terrambe.service;

import com.terrambe.domain.Appezzamenti;
import com.terrambe.repository.AppezzamentiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Appezzamenti}.
 */
@Service
@Transactional
public class AppezzamentiService {

    private final Logger log = LoggerFactory.getLogger(AppezzamentiService.class);

    private final AppezzamentiRepository appezzamentiRepository;

    public AppezzamentiService(AppezzamentiRepository appezzamentiRepository) {
        this.appezzamentiRepository = appezzamentiRepository;
    }

    /**
     * Save a appezzamenti.
     *
     * @param appezzamenti the entity to save.
     * @return the persisted entity.
     */
    public Appezzamenti save(Appezzamenti appezzamenti) {
        log.debug("Request to save Appezzamenti : {}", appezzamenti);
        return appezzamentiRepository.save(appezzamenti);
    }

    /**
     * Get all the appezzamentis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Appezzamenti> findAll() {
        log.debug("Request to get all Appezzamentis");
        return appezzamentiRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the appezzamentis with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Appezzamenti> findAllWithEagerRelationships(Pageable pageable) {
        return appezzamentiRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one appezzamenti by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Appezzamenti> findOne(Long id) {
        log.debug("Request to get Appezzamenti : {}", id);
        return appezzamentiRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the appezzamenti by id.
     *
     * @param id the id of the entity.
     */
    @Transactional
    public void delete(Long id) {
        log.debug("Request to delete Appezzamenti : {}", id);
        appezzamentiRepository.deleteById(id);
    }
}
