package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TrapContrTipo;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TrapContrTipoRepository;
import com.terrambe.service.dto.TrapContrTipoCriteria;

/**
 * Service for executing complex queries for {@link TrapContrTipo} entities in the database.
 * The main input is a {@link TrapContrTipoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TrapContrTipo} or a {@link Page} of {@link TrapContrTipo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TrapContrTipoQueryService extends QueryService<TrapContrTipo> {

    private final Logger log = LoggerFactory.getLogger(TrapContrTipoQueryService.class);

    private final TrapContrTipoRepository trapContrTipoRepository;

    public TrapContrTipoQueryService(TrapContrTipoRepository trapContrTipoRepository) {
        this.trapContrTipoRepository = trapContrTipoRepository;
    }

    /**
     * Return a {@link List} of {@link TrapContrTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TrapContrTipo> findByCriteria(TrapContrTipoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TrapContrTipo> specification = createSpecification(criteria);
        return trapContrTipoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TrapContrTipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TrapContrTipo> findByCriteria(TrapContrTipoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TrapContrTipo> specification = createSpecification(criteria);
        return trapContrTipoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TrapContrTipoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TrapContrTipo> specification = createSpecification(criteria);
        return trapContrTipoRepository.count(specification);
    }

    /**
     * Function to convert {@link TrapContrTipoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TrapContrTipo> createSpecification(TrapContrTipoCriteria criteria) {
        Specification<TrapContrTipo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TrapContrTipo_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TrapContrTipo_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TrapContrTipo_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TrapContrTipo_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TrapContrTipo_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TrapContrTipo_.userIdLastMod));
            }
            if (criteria.getTrapContrToTrapContrTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTrapContrToTrapContrTipoId(),
                    root -> root.join(TrapContrTipo_.trapContrToTrapContrTipos, JoinType.LEFT).get(RegOpcTrapContr_.id)));
            }
        }
        return specification;
    }
}
