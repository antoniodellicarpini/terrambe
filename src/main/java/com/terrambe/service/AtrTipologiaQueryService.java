package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AtrTipologia;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AtrTipologiaRepository;
import com.terrambe.service.dto.AtrTipologiaCriteria;

/**
 * Service for executing complex queries for {@link AtrTipologia} entities in the database.
 * The main input is a {@link AtrTipologiaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AtrTipologia} or a {@link Page} of {@link AtrTipologia} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AtrTipologiaQueryService extends QueryService<AtrTipologia> {

    private final Logger log = LoggerFactory.getLogger(AtrTipologiaQueryService.class);

    private final AtrTipologiaRepository atrTipologiaRepository;

    public AtrTipologiaQueryService(AtrTipologiaRepository atrTipologiaRepository) {
        this.atrTipologiaRepository = atrTipologiaRepository;
    }

    /**
     * Return a {@link List} of {@link AtrTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AtrTipologia> findByCriteria(AtrTipologiaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AtrTipologia> specification = createSpecification(criteria);
        return atrTipologiaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AtrTipologia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AtrTipologia> findByCriteria(AtrTipologiaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AtrTipologia> specification = createSpecification(criteria);
        return atrTipologiaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AtrTipologiaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AtrTipologia> specification = createSpecification(criteria);
        return atrTipologiaRepository.count(specification);
    }

    /**
     * Function to convert {@link AtrTipologiaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AtrTipologia> createSpecification(AtrTipologiaCriteria criteria) {
        Specification<AtrTipologia> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AtrTipologia_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), AtrTipologia_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AtrTipologia_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AtrTipologia_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AtrTipologia_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AtrTipologia_.userIdLastMod));
            }
            if (criteria.getAtrMarchioId() != null) {
                specification = specification.and(buildSpecification(criteria.getAtrMarchioId(),
                    root -> root.join(AtrTipologia_.atrMarchios, JoinType.LEFT).get(AtrMarchio_.id)));
            }
        }
        return specification;
    }
}
