package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziSuperficieParticella;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziSuperficieParticellaRepository;
import com.terrambe.service.dto.AziSuperficieParticellaCriteria;

/**
 * Service for executing complex queries for {@link AziSuperficieParticella} entities in the database.
 * The main input is a {@link AziSuperficieParticellaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziSuperficieParticella} or a {@link Page} of {@link AziSuperficieParticella} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziSuperficieParticellaQueryService extends QueryService<AziSuperficieParticella> {

    private final Logger log = LoggerFactory.getLogger(AziSuperficieParticellaQueryService.class);

    private final AziSuperficieParticellaRepository aziSuperficieParticellaRepository;

    public AziSuperficieParticellaQueryService(AziSuperficieParticellaRepository aziSuperficieParticellaRepository) {
        this.aziSuperficieParticellaRepository = aziSuperficieParticellaRepository;
    }

    /**
     * Return a {@link List} of {@link AziSuperficieParticella} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziSuperficieParticella> findByCriteria(AziSuperficieParticellaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziSuperficieParticella> specification = createSpecification(criteria);
        return aziSuperficieParticellaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziSuperficieParticella} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziSuperficieParticella> findByCriteria(AziSuperficieParticellaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziSuperficieParticella> specification = createSpecification(criteria);
        return aziSuperficieParticellaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziSuperficieParticellaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziSuperficieParticella> specification = createSpecification(criteria);
        return aziSuperficieParticellaRepository.count(specification);
    }

    /**
     * Function to convert {@link AziSuperficieParticellaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziSuperficieParticella> createSpecification(AziSuperficieParticellaCriteria criteria) {
        Specification<AziSuperficieParticella> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziSuperficieParticella_.id));
            }
            if (criteria.getFoglio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFoglio(), AziSuperficieParticella_.foglio));
            }
            if (criteria.getPart() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPart(), AziSuperficieParticella_.part));
            }
            if (criteria.getSub() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSub(), AziSuperficieParticella_.sub));
            }
            if (criteria.getSezione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSezione(), AziSuperficieParticella_.sezione));
            }
            if (criteria.getSuperficie() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSuperficie(), AziSuperficieParticella_.superficie));
            }
            if (criteria.getSupCatastale() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSupCatastale(), AziSuperficieParticella_.supCatastale));
            }
            if (criteria.getSupUtilizzabile() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSupUtilizzabile(), AziSuperficieParticella_.supUtilizzabile));
            }
            if (criteria.getSupTara() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSupTara(), AziSuperficieParticella_.supTara));
            }
            if (criteria.getUuidEsri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuidEsri(), AziSuperficieParticella_.uuidEsri));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), AziSuperficieParticella_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), AziSuperficieParticella_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), AziSuperficieParticella_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), AziSuperficieParticella_.comune));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziSuperficieParticella_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziSuperficieParticella_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziSuperficieParticella_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziSuperficieParticella_.userIdLastMod));
            }
            if (criteria.getPartToDettaglioId() != null) {
                specification = specification.and(buildSpecification(criteria.getPartToDettaglioId(),
                    root -> root.join(AziSuperficieParticella_.partToDettaglios, JoinType.LEFT).get(AziSupPartDettaglio_.id)));
            }
            if (criteria.getConduzioneId() != null) {
                specification = specification.and(buildSpecification(criteria.getConduzioneId(),
                    root -> root.join(AziSuperficieParticella_.conduzione, JoinType.LEFT).get(AziSupPartConduzione_.id)));
            }
            if (criteria.getPartToAziAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getPartToAziAnaId(),
                    root -> root.join(AziSuperficieParticella_.partToAziAna, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
            if (criteria.getAziSupToCampiId() != null) {
                specification = specification.and(buildSpecification(criteria.getAziSupToCampiId(),
                    root -> root.join(AziSuperficieParticella_.aziSupToCampi, JoinType.LEFT).get(Campi_.id)));
            }
            if (criteria.getPartToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getPartToAppzId(),
                    root -> root.join(AziSuperficieParticella_.partToAppzs, JoinType.LEFT).get(Appezzamenti_.id)));
            }
        }
        return specification;
    }
}
