package com.terrambe.service;

import com.terrambe.domain.DosaggioTrattamento;
import com.terrambe.repository.DosaggioTrattamentoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DosaggioTrattamento}.
 */
@Service
@Transactional
public class DosaggioTrattamentoService {

    private final Logger log = LoggerFactory.getLogger(DosaggioTrattamentoService.class);

    private final DosaggioTrattamentoRepository dosaggioTrattamentoRepository;

    public DosaggioTrattamentoService(DosaggioTrattamentoRepository dosaggioTrattamentoRepository) {
        this.dosaggioTrattamentoRepository = dosaggioTrattamentoRepository;
    }

    /**
     * Save a dosaggioTrattamento.
     *
     * @param dosaggioTrattamento the entity to save.
     * @return the persisted entity.
     */
    public DosaggioTrattamento save(DosaggioTrattamento dosaggioTrattamento) {
        log.debug("Request to save DosaggioTrattamento : {}", dosaggioTrattamento);
        return dosaggioTrattamentoRepository.save(dosaggioTrattamento);
    }

    /**
     * Get all the dosaggioTrattamentos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DosaggioTrattamento> findAll() {
        log.debug("Request to get all DosaggioTrattamentos");
        return dosaggioTrattamentoRepository.findAll();
    }


    /**
     * Get one dosaggioTrattamento by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DosaggioTrattamento> findOne(Long id) {
        log.debug("Request to get DosaggioTrattamento : {}", id);
        return dosaggioTrattamentoRepository.findById(id);
    }

    /**
     * Delete the dosaggioTrattamento by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DosaggioTrattamento : {}", id);
        dosaggioTrattamentoRepository.deleteById(id);
    }
}
