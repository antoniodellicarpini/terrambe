package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Syslog;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.SyslogRepository;
import com.terrambe.service.dto.SyslogCriteria;

/**
 * Service for executing complex queries for {@link Syslog} entities in the database.
 * The main input is a {@link SyslogCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Syslog} or a {@link Page} of {@link Syslog} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SyslogQueryService extends QueryService<Syslog> {

    private final Logger log = LoggerFactory.getLogger(SyslogQueryService.class);

    private final SyslogRepository syslogRepository;

    public SyslogQueryService(SyslogRepository syslogRepository) {
        this.syslogRepository = syslogRepository;
    }

    /**
     * Return a {@link List} of {@link Syslog} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Syslog> findByCriteria(SyslogCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Syslog> specification = createSpecification(criteria);
        return syslogRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Syslog} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Syslog> findByCriteria(SyslogCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Syslog> specification = createSpecification(criteria);
        return syslogRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SyslogCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Syslog> specification = createSpecification(criteria);
        return syslogRepository.count(specification);
    }

    /**
     * Function to convert {@link SyslogCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Syslog> createSpecification(SyslogCriteria criteria) {
        Specification<Syslog> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Syslog_.id));
            }
            if (criteria.getNomeTabella() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeTabella(), Syslog_.nomeTabella));
            }
            if (criteria.getIdJuser() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdJuser(), Syslog_.idJuser));
            }
            if (criteria.getDataModifica() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataModifica(), Syslog_.dataModifica));
            }
            if (criteria.getNomeCampo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeCampo(), Syslog_.nomeCampo));
            }
            if (criteria.getVecchioValore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVecchioValore(), Syslog_.vecchioValore));
            }
            if (criteria.getNuovoValore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNuovoValore(), Syslog_.nuovoValore));
            }
        }
        return specification;
    }
}
