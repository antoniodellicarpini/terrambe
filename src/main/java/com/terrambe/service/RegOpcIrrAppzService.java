package com.terrambe.service;

import com.terrambe.domain.RegOpcIrrAppz;
import com.terrambe.repository.RegOpcIrrAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcIrrAppz}.
 */
@Service
@Transactional
public class RegOpcIrrAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcIrrAppzService.class);

    private final RegOpcIrrAppzRepository regOpcIrrAppzRepository;

    public RegOpcIrrAppzService(RegOpcIrrAppzRepository regOpcIrrAppzRepository) {
        this.regOpcIrrAppzRepository = regOpcIrrAppzRepository;
    }

    /**
     * Save a regOpcIrrAppz.
     *
     * @param regOpcIrrAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcIrrAppz save(RegOpcIrrAppz regOpcIrrAppz) {
        log.debug("Request to save RegOpcIrrAppz : {}", regOpcIrrAppz);
        return regOpcIrrAppzRepository.save(regOpcIrrAppz);
    }

    /**
     * Get all the regOpcIrrAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcIrrAppz> findAll() {
        log.debug("Request to get all RegOpcIrrAppzs");
        return regOpcIrrAppzRepository.findAll();
    }


    /**
     * Get one regOpcIrrAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcIrrAppz> findOne(Long id) {
        log.debug("Request to get RegOpcIrrAppz : {}", id);
        return regOpcIrrAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcIrrAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcIrrAppz : {}", id);
        regOpcIrrAppzRepository.deleteById(id);
    }
}
