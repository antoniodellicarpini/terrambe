package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcPrepterr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcPrepterrRepository;
import com.terrambe.service.dto.RegOpcPrepterrCriteria;

/**
 * Service for executing complex queries for {@link RegOpcPrepterr} entities in the database.
 * The main input is a {@link RegOpcPrepterrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcPrepterr} or a {@link Page} of {@link RegOpcPrepterr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcPrepterrQueryService extends QueryService<RegOpcPrepterr> {

    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrQueryService.class);

    private final RegOpcPrepterrRepository regOpcPrepterrRepository;

    public RegOpcPrepterrQueryService(RegOpcPrepterrRepository regOpcPrepterrRepository) {
        this.regOpcPrepterrRepository = regOpcPrepterrRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcPrepterr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPrepterr> findByCriteria(RegOpcPrepterrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcPrepterr> specification = createSpecification(criteria);
        return regOpcPrepterrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcPrepterr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcPrepterr> findByCriteria(RegOpcPrepterrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcPrepterr> specification = createSpecification(criteria);
        return regOpcPrepterrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcPrepterrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcPrepterr> specification = createSpecification(criteria);
        return regOpcPrepterrRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcPrepterrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcPrepterr> createSpecification(RegOpcPrepterrCriteria criteria) {
        Specification<RegOpcPrepterr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcPrepterr_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcPrepterr_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcPrepterr_.idUnitaProd));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcPrepterr_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcPrepterr_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcPrepterr_.tempoImpiegato));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcPrepterr_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcPrepterr_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcPrepterr_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcPrepterr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcPrepterr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcPrepterr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcPrepterr_.userIdLastMod));
            }
            if (criteria.getOpcPrepterrToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcPrepterrToAppzId(),
                    root -> root.join(RegOpcPrepterr_.opcPrepterrToAppzs, JoinType.LEFT).get(RegOpcPrepterrAppz_.id)));
            }
            if (criteria.getOpcPrepterrToTipoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcPrepterrToTipoId(),
                    root -> root.join(RegOpcPrepterr_.opcPrepterrToTipo, JoinType.LEFT).get(TipoPrepterr_.id)));
            }
        }
        return specification;
    }
}
