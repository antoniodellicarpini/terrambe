package com.terrambe.service;

import com.terrambe.domain.RecForn;
import com.terrambe.repository.RecFornRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecForn}.
 */
@Service
@Transactional
public class RecFornService {

    private final Logger log = LoggerFactory.getLogger(RecFornService.class);

    private final RecFornRepository recFornRepository;

    public RecFornService(RecFornRepository recFornRepository) {
        this.recFornRepository = recFornRepository;
    }

    /**
     * Save a recForn.
     *
     * @param recForn the entity to save.
     * @return the persisted entity.
     */
    public RecForn save(RecForn recForn) {
        log.debug("Request to save RecForn : {}", recForn);
        return recFornRepository.save(recForn);
    }

    /**
     * Get all the recForns.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecForn> findAll() {
        log.debug("Request to get all RecForns");
        return recFornRepository.findAll();
    }


    /**
     * Get one recForn by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecForn> findOne(Long id) {
        log.debug("Request to get RecForn : {}", id);
        return recFornRepository.findById(id);
    }

    /**
     * Delete the recForn by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecForn : {}", id);
        recFornRepository.deleteById(id);
    }
}
