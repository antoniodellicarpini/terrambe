package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcConc;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcConcRepository;
import com.terrambe.service.dto.RegOpcConcCriteria;

/**
 * Service for executing complex queries for {@link RegOpcConc} entities in the database.
 * The main input is a {@link RegOpcConcCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcConc} or a {@link Page} of {@link RegOpcConc} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcConcQueryService extends QueryService<RegOpcConc> {

    private final Logger log = LoggerFactory.getLogger(RegOpcConcQueryService.class);

    private final RegOpcConcRepository regOpcConcRepository;

    public RegOpcConcQueryService(RegOpcConcRepository regOpcConcRepository) {
        this.regOpcConcRepository = regOpcConcRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcConc> findByCriteria(RegOpcConcCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcConc> specification = createSpecification(criteria);
        return regOpcConcRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcConc} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcConc> findByCriteria(RegOpcConcCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcConc> specification = createSpecification(criteria);
        return regOpcConcRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcConcCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcConc> specification = createSpecification(criteria);
        return regOpcConcRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcConcCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcConc> createSpecification(RegOpcConcCriteria criteria) {
        Specification<RegOpcConc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcConc_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcConc_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcConc_.idUnitaProd));
            }
            if (criteria.getDataInizOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizOpc(), RegOpcConc_.dataInizOpc));
            }
            if (criteria.getDataFineOpc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineOpc(), RegOpcConc_.dataFineOpc));
            }
            if (criteria.getTempoImpiegato() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTempoImpiegato(), RegOpcConc_.tempoImpiegato));
            }
            if (criteria.getVolAcquaFertIrr() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVolAcquaFertIrr(), RegOpcConc_.volAcquaFertIrr));
            }
            if (criteria.getIdColtura() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdColtura(), RegOpcConc_.idColtura));
            }
            if (criteria.getIdOperatore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdOperatore(), RegOpcConc_.idOperatore));
            }
            if (criteria.getIdMezzo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdMezzo(), RegOpcConc_.idMezzo));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcConc_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcConc_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcConc_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcConc_.userIdLastMod));
            }
            if (criteria.getOpcConcToAppzId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcConcToAppzId(),
                    root -> root.join(RegOpcConc_.opcConcToAppzs, JoinType.LEFT).get(RegOpcConcAppz_.id)));
            }
            if (criteria.getOpcConcToProdId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcConcToProdId(),
                    root -> root.join(RegOpcConc_.opcConcToProds, JoinType.LEFT).get(RegOpcConcProd_.id)));
            }
            if (criteria.getOpcConcToMotivoId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcConcToMotivoId(),
                    root -> root.join(RegOpcConc_.opcConcToMotivo, JoinType.LEFT).get(MotivoConc_.id)));
            }
            if (criteria.getOpcConcToTipoDistId() != null) {
                specification = specification.and(buildSpecification(criteria.getOpcConcToTipoDistId(),
                    root -> root.join(RegOpcConc_.opcConcToTipoDist, JoinType.LEFT).get(TipoDistConc_.id)));
            }
        }
        return specification;
    }
}
