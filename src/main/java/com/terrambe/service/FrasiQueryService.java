package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Frasi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.FrasiRepository;
import com.terrambe.service.dto.FrasiCriteria;

/**
 * Service for executing complex queries for {@link Frasi} entities in the database.
 * The main input is a {@link FrasiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Frasi} or a {@link Page} of {@link Frasi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FrasiQueryService extends QueryService<Frasi> {

    private final Logger log = LoggerFactory.getLogger(FrasiQueryService.class);

    private final FrasiRepository frasiRepository;

    public FrasiQueryService(FrasiRepository frasiRepository) {
        this.frasiRepository = frasiRepository;
    }

    /**
     * Return a {@link List} of {@link Frasi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Frasi> findByCriteria(FrasiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Frasi> specification = createSpecification(criteria);
        return frasiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Frasi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Frasi> findByCriteria(FrasiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Frasi> specification = createSpecification(criteria);
        return frasiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FrasiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Frasi> specification = createSpecification(criteria);
        return frasiRepository.count(specification);
    }

    /**
     * Function to convert {@link FrasiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Frasi> createSpecification(FrasiCriteria criteria) {
        Specification<Frasi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Frasi_.id));
            }
            if (criteria.getCodice() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodice(), Frasi_.codice));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), Frasi_.descrizione));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), Frasi_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), Frasi_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), Frasi_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Frasi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Frasi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Frasi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Frasi_.userIdLastMod));
            }
            if (criteria.getFrasietichettaId() != null) {
                specification = specification.and(buildSpecification(criteria.getFrasietichettaId(),
                    root -> root.join(Frasi_.frasietichettas, JoinType.LEFT).get(EtichettaFrasi_.id)));
            }
        }
        return specification;
    }
}
