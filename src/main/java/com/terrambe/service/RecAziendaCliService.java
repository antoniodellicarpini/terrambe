package com.terrambe.service;

import com.terrambe.domain.RecAziendaCli;
import com.terrambe.repository.RecAziendaCliRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RecAziendaCli}.
 */
@Service
@Transactional
public class RecAziendaCliService {

    private final Logger log = LoggerFactory.getLogger(RecAziendaCliService.class);

    private final RecAziendaCliRepository recAziendaCliRepository;

    public RecAziendaCliService(RecAziendaCliRepository recAziendaCliRepository) {
        this.recAziendaCliRepository = recAziendaCliRepository;
    }

    /**
     * Save a recAziendaCli.
     *
     * @param recAziendaCli the entity to save.
     * @return the persisted entity.
     */
    public RecAziendaCli save(RecAziendaCli recAziendaCli) {
        log.debug("Request to save RecAziendaCli : {}", recAziendaCli);
        return recAziendaCliRepository.save(recAziendaCli);
    }

    /**
     * Get all the recAziendaClis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziendaCli> findAll() {
        log.debug("Request to get all RecAziendaClis");
        return recAziendaCliRepository.findAll();
    }


    /**
     * Get one recAziendaCli by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecAziendaCli> findOne(Long id) {
        log.debug("Request to get RecAziendaCli : {}", id);
        return recAziendaCliRepository.findById(id);
    }

    /**
     * Delete the recAziendaCli by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecAziendaCli : {}", id);
        recAziendaCliRepository.deleteById(id);
    }
}
