package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RegOpcFitoAppz;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RegOpcFitoAppzRepository;
import com.terrambe.service.dto.RegOpcFitoAppzCriteria;

/**
 * Service for executing complex queries for {@link RegOpcFitoAppz} entities in the database.
 * The main input is a {@link RegOpcFitoAppzCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RegOpcFitoAppz} or a {@link Page} of {@link RegOpcFitoAppz} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RegOpcFitoAppzQueryService extends QueryService<RegOpcFitoAppz> {

    private final Logger log = LoggerFactory.getLogger(RegOpcFitoAppzQueryService.class);

    private final RegOpcFitoAppzRepository regOpcFitoAppzRepository;

    public RegOpcFitoAppzQueryService(RegOpcFitoAppzRepository regOpcFitoAppzRepository) {
        this.regOpcFitoAppzRepository = regOpcFitoAppzRepository;
    }

    /**
     * Return a {@link List} of {@link RegOpcFitoAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcFitoAppz> findByCriteria(RegOpcFitoAppzCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RegOpcFitoAppz> specification = createSpecification(criteria);
        return regOpcFitoAppzRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RegOpcFitoAppz} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RegOpcFitoAppz> findByCriteria(RegOpcFitoAppzCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RegOpcFitoAppz> specification = createSpecification(criteria);
        return regOpcFitoAppzRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RegOpcFitoAppzCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RegOpcFitoAppz> specification = createSpecification(criteria);
        return regOpcFitoAppzRepository.count(specification);
    }

    /**
     * Function to convert {@link RegOpcFitoAppzCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RegOpcFitoAppz> createSpecification(RegOpcFitoAppzCriteria criteria) {
        Specification<RegOpcFitoAppz> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RegOpcFitoAppz_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), RegOpcFitoAppz_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), RegOpcFitoAppz_.idUnitaProd));
            }
            if (criteria.getPercentLavorata() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentLavorata(), RegOpcFitoAppz_.percentLavorata));
            }
            if (criteria.getIdAppezzamento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAppezzamento(), RegOpcFitoAppz_.idAppezzamento));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RegOpcFitoAppz_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RegOpcFitoAppz_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RegOpcFitoAppz_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RegOpcFitoAppz_.userIdLastMod));
            }
            if (criteria.getAppzToOpcFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getAppzToOpcFitoId(),
                    root -> root.join(RegOpcFitoAppz_.appzToOpcFito, JoinType.LEFT).get(RegOpcFito_.id)));
            }
        }
        return specification;
    }
}
