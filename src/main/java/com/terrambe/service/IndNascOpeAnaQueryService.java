package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndNascOpeAna;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndNascOpeAnaRepository;
import com.terrambe.service.dto.IndNascOpeAnaCriteria;

/**
 * Service for executing complex queries for {@link IndNascOpeAna} entities in the database.
 * The main input is a {@link IndNascOpeAnaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndNascOpeAna} or a {@link Page} of {@link IndNascOpeAna} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndNascOpeAnaQueryService extends QueryService<IndNascOpeAna> {

    private final Logger log = LoggerFactory.getLogger(IndNascOpeAnaQueryService.class);

    private final IndNascOpeAnaRepository indNascOpeAnaRepository;

    public IndNascOpeAnaQueryService(IndNascOpeAnaRepository indNascOpeAnaRepository) {
        this.indNascOpeAnaRepository = indNascOpeAnaRepository;
    }

    /**
     * Return a {@link List} of {@link IndNascOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascOpeAna> findByCriteria(IndNascOpeAnaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndNascOpeAna> specification = createSpecification(criteria);
        return indNascOpeAnaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndNascOpeAna} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndNascOpeAna> findByCriteria(IndNascOpeAnaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndNascOpeAna> specification = createSpecification(criteria);
        return indNascOpeAnaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndNascOpeAnaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndNascOpeAna> specification = createSpecification(criteria);
        return indNascOpeAnaRepository.count(specification);
    }

    /**
     * Function to convert {@link IndNascOpeAnaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndNascOpeAna> createSpecification(IndNascOpeAnaCriteria criteria) {
        Specification<IndNascOpeAna> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndNascOpeAna_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndNascOpeAna_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndNascOpeAna_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndNascOpeAna_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndNascOpeAna_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndNascOpeAna_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndNascOpeAna_.comune));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndNascOpeAna_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndNascOpeAna_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndNascOpeAna_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndNascOpeAna_.userIdLastMod));
            }
            if (criteria.getIndNascToOpeAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndNascToOpeAnaId(),
                    root -> root.join(IndNascOpeAna_.indNascToOpeAna, JoinType.LEFT).get(OpeAnagrafica_.id)));
            }
        }
        return specification;
    }
}
