package com.terrambe.service;

import com.terrambe.domain.TipologiaVincoliTerritoriali;
import com.terrambe.repository.TipologiaVincoliTerritorialiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipologiaVincoliTerritoriali}.
 */
@Service
@Transactional
public class TipologiaVincoliTerritorialiService {

    private final Logger log = LoggerFactory.getLogger(TipologiaVincoliTerritorialiService.class);

    private final TipologiaVincoliTerritorialiRepository tipologiaVincoliTerritorialiRepository;

    public TipologiaVincoliTerritorialiService(TipologiaVincoliTerritorialiRepository tipologiaVincoliTerritorialiRepository) {
        this.tipologiaVincoliTerritorialiRepository = tipologiaVincoliTerritorialiRepository;
    }

    /**
     * Save a tipologiaVincoliTerritoriali.
     *
     * @param tipologiaVincoliTerritoriali the entity to save.
     * @return the persisted entity.
     */
    public TipologiaVincoliTerritoriali save(TipologiaVincoliTerritoriali tipologiaVincoliTerritoriali) {
        log.debug("Request to save TipologiaVincoliTerritoriali : {}", tipologiaVincoliTerritoriali);
        return tipologiaVincoliTerritorialiRepository.save(tipologiaVincoliTerritoriali);
    }

    /**
     * Get all the tipologiaVincoliTerritorialis.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipologiaVincoliTerritoriali> findAll() {
        log.debug("Request to get all TipologiaVincoliTerritorialis");
        return tipologiaVincoliTerritorialiRepository.findAll();
    }


    /**
     * Get one tipologiaVincoliTerritoriali by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipologiaVincoliTerritoriali> findOne(Long id) {
        log.debug("Request to get TipologiaVincoliTerritoriali : {}", id);
        return tipologiaVincoliTerritorialiRepository.findById(id);
    }

    /**
     * Delete the tipologiaVincoliTerritoriali by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipologiaVincoliTerritoriali : {}", id);
        tipologiaVincoliTerritorialiRepository.deleteById(id);
    }
}
