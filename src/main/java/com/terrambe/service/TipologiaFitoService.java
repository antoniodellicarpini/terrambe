package com.terrambe.service;

import com.terrambe.domain.TipologiaFito;
import com.terrambe.repository.TipologiaFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipologiaFito}.
 */
@Service
@Transactional
public class TipologiaFitoService {

    private final Logger log = LoggerFactory.getLogger(TipologiaFitoService.class);

    private final TipologiaFitoRepository tipologiaFitoRepository;

    public TipologiaFitoService(TipologiaFitoRepository tipologiaFitoRepository) {
        this.tipologiaFitoRepository = tipologiaFitoRepository;
    }

    /**
     * Save a tipologiaFito.
     *
     * @param tipologiaFito the entity to save.
     * @return the persisted entity.
     */
    public TipologiaFito save(TipologiaFito tipologiaFito) {
        log.debug("Request to save TipologiaFito : {}", tipologiaFito);
        return tipologiaFitoRepository.save(tipologiaFito);
    }

    /**
     * Get all the tipologiaFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipologiaFito> findAll() {
        log.debug("Request to get all TipologiaFitos");
        return tipologiaFitoRepository.findAll();
    }


    /**
     * Get one tipologiaFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipologiaFito> findOne(Long id) {
        log.debug("Request to get TipologiaFito : {}", id);
        return tipologiaFitoRepository.findById(id);
    }

    /**
     * Delete the tipologiaFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipologiaFito : {}", id);
        tipologiaFitoRepository.deleteById(id);
    }
}
