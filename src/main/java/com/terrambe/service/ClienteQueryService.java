package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Cliente;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ClienteRepository;
import com.terrambe.service.dto.ClienteCriteria;

/**
 * Service for executing complex queries for {@link Cliente} entities in the database.
 * The main input is a {@link ClienteCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Cliente} or a {@link Page} of {@link Cliente} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClienteQueryService extends QueryService<Cliente> {

    private final Logger log = LoggerFactory.getLogger(ClienteQueryService.class);

    private final ClienteRepository clienteRepository;

    public ClienteQueryService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    /**
     * Return a {@link List} of {@link Cliente} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Cliente> findByCriteria(ClienteCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cliente> specification = createSpecification(criteria);
        return clienteRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Cliente} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Cliente> findByCriteria(ClienteCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cliente> specification = createSpecification(criteria);
        return clienteRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClienteCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cliente> specification = createSpecification(criteria);
        return clienteRepository.count(specification);
    }

    /**
     * Function to convert {@link ClienteCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Cliente> createSpecification(ClienteCriteria criteria) {
        Specification<Cliente> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Cliente_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), Cliente_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), Cliente_.idUnitaProd));
            }
            if (criteria.getCuaa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCuaa(), Cliente_.cuaa));
            }
            if (criteria.getPartitaiva() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPartitaiva(), Cliente_.partitaiva));
            }
            if (criteria.getCodicefiscale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodicefiscale(), Cliente_.codicefiscale));
            }
            if (criteria.getCodiceAteco() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceAteco(), Cliente_.codiceAteco));
            }
            if (criteria.getRagioneSociale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRagioneSociale(), Cliente_.ragioneSociale));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Cliente_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Cliente_.dataFineVali));
            }
            if (criteria.getAttivo() != null) {
                specification = specification.and(buildSpecification(criteria.getAttivo(), Cliente_.attivo));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Cliente_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Cliente_.userIdLastMod));
            }
            if (criteria.getCliToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliToIndId(),
                    root -> root.join(Cliente_.cliToInds, JoinType.LEFT).get(IndAziendaCli_.id)));
            }
            if (criteria.getCliToRecId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliToRecId(),
                    root -> root.join(Cliente_.cliToRecs, JoinType.LEFT).get(RecAziendaCli_.id)));
            }
            if (criteria.getNaturaGiuridicaId() != null) {
                specification = specification.and(buildSpecification(criteria.getNaturaGiuridicaId(),
                    root -> root.join(Cliente_.naturaGiuridica, JoinType.LEFT).get(AziFormaGiuridica_.id)));
            }
            if (criteria.getCliToAziAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliToAziAnaId(),
                    root -> root.join(Cliente_.cliToAziAna, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
            if (criteria.getCliAnaToCliRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getCliAnaToCliRapId(),
                    root -> root.join(Cliente_.cliAnaToCliRap, JoinType.LEFT).get(AziRappresentanteCli_.id)));
            }
        }
        return specification;
    }
}
