package com.terrambe.service;

import com.terrambe.domain.PermessiOperatore;
import com.terrambe.repository.PermessiOperatoreRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PermessiOperatore}.
 */
@Service
@Transactional
public class PermessiOperatoreService {

    private final Logger log = LoggerFactory.getLogger(PermessiOperatoreService.class);

    private final PermessiOperatoreRepository permessiOperatoreRepository;

    public PermessiOperatoreService(PermessiOperatoreRepository permessiOperatoreRepository) {
        this.permessiOperatoreRepository = permessiOperatoreRepository;
    }

    /**
     * Save a permessiOperatore.
     *
     * @param permessiOperatore the entity to save.
     * @return the persisted entity.
     */
    public PermessiOperatore save(PermessiOperatore permessiOperatore) {
        log.debug("Request to save PermessiOperatore : {}", permessiOperatore);
        return permessiOperatoreRepository.save(permessiOperatore);
    }

    /**
     * Get all the permessiOperatores.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PermessiOperatore> findAll() {
        log.debug("Request to get all PermessiOperatores");
        return permessiOperatoreRepository.findAll();
    }


    /**
     * Get one permessiOperatore by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PermessiOperatore> findOne(Long id) {
        log.debug("Request to get PermessiOperatore : {}", id);
        return permessiOperatoreRepository.findById(id);
    }

    /**
     * Delete the permessiOperatore by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PermessiOperatore : {}", id);
        permessiOperatoreRepository.deleteById(id);
    }
}
