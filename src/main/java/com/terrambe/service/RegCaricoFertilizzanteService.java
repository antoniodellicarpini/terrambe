package com.terrambe.service;

import com.terrambe.domain.RegCaricoFertilizzante;
import com.terrambe.repository.RegCaricoFertilizzanteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegCaricoFertilizzante}.
 */
@Service
@Transactional
public class RegCaricoFertilizzanteService {

    private final Logger log = LoggerFactory.getLogger(RegCaricoFertilizzanteService.class);

    private final RegCaricoFertilizzanteRepository regCaricoFertilizzanteRepository;

    public RegCaricoFertilizzanteService(RegCaricoFertilizzanteRepository regCaricoFertilizzanteRepository) {
        this.regCaricoFertilizzanteRepository = regCaricoFertilizzanteRepository;
    }

    /**
     * Save a regCaricoFertilizzante.
     *
     * @param regCaricoFertilizzante the entity to save.
     * @return the persisted entity.
     */
    public RegCaricoFertilizzante save(RegCaricoFertilizzante regCaricoFertilizzante) {
        log.debug("Request to save RegCaricoFertilizzante : {}", regCaricoFertilizzante);
        return regCaricoFertilizzanteRepository.save(regCaricoFertilizzante);
    }

    /**
     * Get all the regCaricoFertilizzantes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegCaricoFertilizzante> findAll() {
        log.debug("Request to get all RegCaricoFertilizzantes");
        return regCaricoFertilizzanteRepository.findAll();
    }


    /**
     * Get one regCaricoFertilizzante by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegCaricoFertilizzante> findOne(Long id) {
        log.debug("Request to get RegCaricoFertilizzante : {}", id);
        return regCaricoFertilizzanteRepository.findById(id);
    }

    /**
     * Delete the regCaricoFertilizzante by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegCaricoFertilizzante : {}", id);
        regCaricoFertilizzanteRepository.deleteById(id);
    }
}
