package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.EtiTipologiaFito;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.EtiTipologiaFitoRepository;
import com.terrambe.service.dto.EtiTipologiaFitoCriteria;

/**
 * Service for executing complex queries for {@link EtiTipologiaFito} entities in the database.
 * The main input is a {@link EtiTipologiaFitoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtiTipologiaFito} or a {@link Page} of {@link EtiTipologiaFito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtiTipologiaFitoQueryService extends QueryService<EtiTipologiaFito> {

    private final Logger log = LoggerFactory.getLogger(EtiTipologiaFitoQueryService.class);

    private final EtiTipologiaFitoRepository etiTipologiaFitoRepository;

    public EtiTipologiaFitoQueryService(EtiTipologiaFitoRepository etiTipologiaFitoRepository) {
        this.etiTipologiaFitoRepository = etiTipologiaFitoRepository;
    }

    /**
     * Return a {@link List} of {@link EtiTipologiaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtiTipologiaFito> findByCriteria(EtiTipologiaFitoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EtiTipologiaFito> specification = createSpecification(criteria);
        return etiTipologiaFitoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EtiTipologiaFito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtiTipologiaFito> findByCriteria(EtiTipologiaFitoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EtiTipologiaFito> specification = createSpecification(criteria);
        return etiTipologiaFitoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtiTipologiaFitoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EtiTipologiaFito> specification = createSpecification(criteria);
        return etiTipologiaFitoRepository.count(specification);
    }

    /**
     * Function to convert {@link EtiTipologiaFitoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EtiTipologiaFito> createSpecification(EtiTipologiaFitoCriteria criteria) {
        Specification<EtiTipologiaFito> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EtiTipologiaFito_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), EtiTipologiaFito_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), EtiTipologiaFito_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), EtiTipologiaFito_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), EtiTipologiaFito_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), EtiTipologiaFito_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), EtiTipologiaFito_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), EtiTipologiaFito_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(EtiTipologiaFito_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getTipologiaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipologiaFitoId(),
                    root -> root.join(EtiTipologiaFito_.tipologiaFito, JoinType.LEFT).get(TipologiaFito_.id)));
            }
        }
        return specification;
    }
}
