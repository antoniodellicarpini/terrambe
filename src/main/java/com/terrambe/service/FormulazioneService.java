package com.terrambe.service;

import com.terrambe.domain.Formulazione;
import com.terrambe.repository.FormulazioneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Formulazione}.
 */
@Service
@Transactional
public class FormulazioneService {

    private final Logger log = LoggerFactory.getLogger(FormulazioneService.class);

    private final FormulazioneRepository formulazioneRepository;

    public FormulazioneService(FormulazioneRepository formulazioneRepository) {
        this.formulazioneRepository = formulazioneRepository;
    }

    /**
     * Save a formulazione.
     *
     * @param formulazione the entity to save.
     * @return the persisted entity.
     */
    public Formulazione save(Formulazione formulazione) {
        log.debug("Request to save Formulazione : {}", formulazione);
        return formulazioneRepository.save(formulazione);
    }

    /**
     * Get all the formulaziones.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Formulazione> findAll() {
        log.debug("Request to get all Formulaziones");
        return formulazioneRepository.findAll();
    }


    /**
     * Get one formulazione by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Formulazione> findOne(Long id) {
        log.debug("Request to get Formulazione : {}", id);
        return formulazioneRepository.findById(id);
    }

    /**
     * Delete the formulazione by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Formulazione : {}", id);
        formulazioneRepository.deleteById(id);
    }
}
