package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.AziSupPartDettaglio;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.AziSupPartDettaglioRepository;
import com.terrambe.service.dto.AziSupPartDettaglioCriteria;

/**
 * Service for executing complex queries for {@link AziSupPartDettaglio} entities in the database.
 * The main input is a {@link AziSupPartDettaglioCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AziSupPartDettaglio} or a {@link Page} of {@link AziSupPartDettaglio} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AziSupPartDettaglioQueryService extends QueryService<AziSupPartDettaglio> {

    private final Logger log = LoggerFactory.getLogger(AziSupPartDettaglioQueryService.class);

    private final AziSupPartDettaglioRepository aziSupPartDettaglioRepository;

    public AziSupPartDettaglioQueryService(AziSupPartDettaglioRepository aziSupPartDettaglioRepository) {
        this.aziSupPartDettaglioRepository = aziSupPartDettaglioRepository;
    }

    /**
     * Return a {@link List} of {@link AziSupPartDettaglio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AziSupPartDettaglio> findByCriteria(AziSupPartDettaglioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AziSupPartDettaglio> specification = createSpecification(criteria);
        return aziSupPartDettaglioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AziSupPartDettaglio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AziSupPartDettaglio> findByCriteria(AziSupPartDettaglioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AziSupPartDettaglio> specification = createSpecification(criteria);
        return aziSupPartDettaglioRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AziSupPartDettaglioCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AziSupPartDettaglio> specification = createSpecification(criteria);
        return aziSupPartDettaglioRepository.count(specification);
    }

    /**
     * Function to convert {@link AziSupPartDettaglioCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AziSupPartDettaglio> createSpecification(AziSupPartDettaglioCriteria criteria) {
        Specification<AziSupPartDettaglio> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AziSupPartDettaglio_.id));
            }
            if (criteria.getIdAzienda() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdAzienda(), AziSupPartDettaglio_.idAzienda));
            }
            if (criteria.getIdUnitaProd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdUnitaProd(), AziSupPartDettaglio_.idUnitaProd));
            }
            if (criteria.getTara() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTara(), AziSupPartDettaglio_.tara));
            }
            if (criteria.getUuidEsri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuidEsri(), AziSupPartDettaglio_.uuidEsri));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), AziSupPartDettaglio_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), AziSupPartDettaglio_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), AziSupPartDettaglio_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), AziSupPartDettaglio_.userIdLastMod));
            }
            if (criteria.getDettaglioToPartId() != null) {
                specification = specification.and(buildSpecification(criteria.getDettaglioToPartId(),
                    root -> root.join(AziSupPartDettaglio_.dettaglioToPart, JoinType.LEFT).get(AziSuperficieParticella_.id)));
            }
            if (criteria.getDettToAgeaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDettToAgeaId(),
                    root -> root.join(AziSupPartDettaglio_.dettToAgea, JoinType.LEFT).get(Agea20152020Matrice_.id)));
            }
        }
        return specification;
    }
}
