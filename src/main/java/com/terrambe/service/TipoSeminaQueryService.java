package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoSemina;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoSeminaRepository;
import com.terrambe.service.dto.TipoSeminaCriteria;

/**
 * Service for executing complex queries for {@link TipoSemina} entities in the database.
 * The main input is a {@link TipoSeminaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoSemina} or a {@link Page} of {@link TipoSemina} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoSeminaQueryService extends QueryService<TipoSemina> {

    private final Logger log = LoggerFactory.getLogger(TipoSeminaQueryService.class);

    private final TipoSeminaRepository tipoSeminaRepository;

    public TipoSeminaQueryService(TipoSeminaRepository tipoSeminaRepository) {
        this.tipoSeminaRepository = tipoSeminaRepository;
    }

    /**
     * Return a {@link List} of {@link TipoSemina} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoSemina> findByCriteria(TipoSeminaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoSemina> specification = createSpecification(criteria);
        return tipoSeminaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoSemina} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoSemina> findByCriteria(TipoSeminaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoSemina> specification = createSpecification(criteria);
        return tipoSeminaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoSeminaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoSemina> specification = createSpecification(criteria);
        return tipoSeminaRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoSeminaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoSemina> createSpecification(TipoSeminaCriteria criteria) {
        Specification<TipoSemina> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoSemina_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoSemina_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoSemina_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoSemina_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoSemina_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoSemina_.userIdLastMod));
            }
            if (criteria.getTipoToOpcSeminaId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoToOpcSeminaId(),
                    root -> root.join(TipoSemina_.tipoToOpcSeminas, JoinType.LEFT).get(RegOpcSemina_.id)));
            }
        }
        return specification;
    }
}
