package com.terrambe.service;

import com.terrambe.domain.RegOpcTrapContr;
import com.terrambe.repository.RegOpcTrapContrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcTrapContr}.
 */
@Service
@Transactional
public class RegOpcTrapContrService {

    private final Logger log = LoggerFactory.getLogger(RegOpcTrapContrService.class);

    private final RegOpcTrapContrRepository regOpcTrapContrRepository;

    public RegOpcTrapContrService(RegOpcTrapContrRepository regOpcTrapContrRepository) {
        this.regOpcTrapContrRepository = regOpcTrapContrRepository;
    }

    /**
     * Save a regOpcTrapContr.
     *
     * @param regOpcTrapContr the entity to save.
     * @return the persisted entity.
     */
    public RegOpcTrapContr save(RegOpcTrapContr regOpcTrapContr) {
        log.debug("Request to save RegOpcTrapContr : {}", regOpcTrapContr);
        return regOpcTrapContrRepository.save(regOpcTrapContr);
    }

    /**
     * Get all the regOpcTrapContrs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcTrapContr> findAll() {
        log.debug("Request to get all RegOpcTrapContrs");
        return regOpcTrapContrRepository.findAll();
    }


    /**
     * Get one regOpcTrapContr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcTrapContr> findOne(Long id) {
        log.debug("Request to get RegOpcTrapContr : {}", id);
        return regOpcTrapContrRepository.findById(id);
    }

    /**
     * Delete the regOpcTrapContr by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcTrapContr : {}", id);
        regOpcTrapContrRepository.deleteById(id);
    }
}
