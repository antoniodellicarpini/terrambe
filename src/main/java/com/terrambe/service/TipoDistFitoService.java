package com.terrambe.service;

import com.terrambe.domain.TipoDistFito;
import com.terrambe.repository.TipoDistFitoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TipoDistFito}.
 */
@Service
@Transactional
public class TipoDistFitoService {

    private final Logger log = LoggerFactory.getLogger(TipoDistFitoService.class);

    private final TipoDistFitoRepository tipoDistFitoRepository;

    public TipoDistFitoService(TipoDistFitoRepository tipoDistFitoRepository) {
        this.tipoDistFitoRepository = tipoDistFitoRepository;
    }

    /**
     * Save a tipoDistFito.
     *
     * @param tipoDistFito the entity to save.
     * @return the persisted entity.
     */
    public TipoDistFito save(TipoDistFito tipoDistFito) {
        log.debug("Request to save TipoDistFito : {}", tipoDistFito);
        return tipoDistFitoRepository.save(tipoDistFito);
    }

    /**
     * Get all the tipoDistFitos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TipoDistFito> findAll() {
        log.debug("Request to get all TipoDistFitos");
        return tipoDistFitoRepository.findAll();
    }


    /**
     * Get one tipoDistFito by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TipoDistFito> findOne(Long id) {
        log.debug("Request to get TipoDistFito : {}", id);
        return tipoDistFitoRepository.findById(id);
    }

    /**
     * Delete the tipoDistFito by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoDistFito : {}", id);
        tipoDistFitoRepository.deleteById(id);
    }
}
