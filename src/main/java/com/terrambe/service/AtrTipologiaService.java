package com.terrambe.service;

import com.terrambe.domain.AtrTipologia;
import com.terrambe.repository.AtrTipologiaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AtrTipologia}.
 */
@Service
@Transactional
public class AtrTipologiaService {

    private final Logger log = LoggerFactory.getLogger(AtrTipologiaService.class);

    private final AtrTipologiaRepository atrTipologiaRepository;

    public AtrTipologiaService(AtrTipologiaRepository atrTipologiaRepository) {
        this.atrTipologiaRepository = atrTipologiaRepository;
    }

    /**
     * Save a atrTipologia.
     *
     * @param atrTipologia the entity to save.
     * @return the persisted entity.
     */
    public AtrTipologia save(AtrTipologia atrTipologia) {
        log.debug("Request to save AtrTipologia : {}", atrTipologia);
        return atrTipologiaRepository.save(atrTipologia);
    }

    /**
     * Get all the atrTipologias.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AtrTipologia> findAll() {
        log.debug("Request to get all AtrTipologias");
        return atrTipologiaRepository.findAll();
    }


    /**
     * Get one atrTipologia by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AtrTipologia> findOne(Long id) {
        log.debug("Request to get AtrTipologia : {}", id);
        return atrTipologiaRepository.findById(id);
    }

    /**
     * Delete the atrTipologia by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AtrTipologia : {}", id);
        atrTipologiaRepository.deleteById(id);
    }
}
