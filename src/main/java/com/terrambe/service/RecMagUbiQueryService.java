package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecMagUbi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecMagUbiRepository;
import com.terrambe.service.dto.RecMagUbiCriteria;

/**
 * Service for executing complex queries for {@link RecMagUbi} entities in the database.
 * The main input is a {@link RecMagUbiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecMagUbi} or a {@link Page} of {@link RecMagUbi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecMagUbiQueryService extends QueryService<RecMagUbi> {

    private final Logger log = LoggerFactory.getLogger(RecMagUbiQueryService.class);

    private final RecMagUbiRepository recMagUbiRepository;

    public RecMagUbiQueryService(RecMagUbiRepository recMagUbiRepository) {
        this.recMagUbiRepository = recMagUbiRepository;
    }

    /**
     * Return a {@link List} of {@link RecMagUbi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecMagUbi> findByCriteria(RecMagUbiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecMagUbi> specification = createSpecification(criteria);
        return recMagUbiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecMagUbi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecMagUbi> findByCriteria(RecMagUbiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecMagUbi> specification = createSpecification(criteria);
        return recMagUbiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecMagUbiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecMagUbi> specification = createSpecification(criteria);
        return recMagUbiRepository.count(specification);
    }

    /**
     * Function to convert {@link RecMagUbiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecMagUbi> createSpecification(RecMagUbiCriteria criteria) {
        Specification<RecMagUbi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecMagUbi_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecMagUbi_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecMagUbi_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecMagUbi_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecMagUbi_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecMagUbi_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecMagUbi_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecMagUbi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecMagUbi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecMagUbi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecMagUbi_.userIdLastMod));
            }
            if (criteria.getRecToMagUbiId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToMagUbiId(),
                    root -> root.join(RecMagUbi_.recToMagUbi, JoinType.LEFT).get(MagUbicazione_.id)));
            }
        }
        return specification;
    }
}
