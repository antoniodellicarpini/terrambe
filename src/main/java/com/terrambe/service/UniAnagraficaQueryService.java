package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.UniAnagrafica;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.UniAnagraficaRepository;
import com.terrambe.service.dto.UniAnagraficaCriteria;

/**
 * Service for executing complex queries for {@link UniAnagrafica} entities in the database.
 * The main input is a {@link UniAnagraficaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UniAnagrafica} or a {@link Page} of {@link UniAnagrafica} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UniAnagraficaQueryService extends QueryService<UniAnagrafica> {

    private final Logger log = LoggerFactory.getLogger(UniAnagraficaQueryService.class);

    private final UniAnagraficaRepository uniAnagraficaRepository;

    public UniAnagraficaQueryService(UniAnagraficaRepository uniAnagraficaRepository) {
        this.uniAnagraficaRepository = uniAnagraficaRepository;
    }

    /**
     * Return a {@link List} of {@link UniAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UniAnagrafica> findByCriteria(UniAnagraficaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UniAnagrafica> specification = createSpecification(criteria);
        return uniAnagraficaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link UniAnagrafica} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UniAnagrafica> findByCriteria(UniAnagraficaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UniAnagrafica> specification = createSpecification(criteria);
        return uniAnagraficaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UniAnagraficaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UniAnagrafica> specification = createSpecification(criteria);
        return uniAnagraficaRepository.count(specification);
    }

    /**
     * Function to convert {@link UniAnagraficaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UniAnagrafica> createSpecification(UniAnagraficaCriteria criteria) {
        Specification<UniAnagrafica> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UniAnagrafica_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), UniAnagrafica_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), UniAnagrafica_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), UniAnagrafica_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), UniAnagrafica_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), UniAnagrafica_.userIdLastMod));
            }
            if (criteria.getUniAnaToIndId() != null) {
                specification = specification.and(buildSpecification(criteria.getUniAnaToIndId(),
                    root -> root.join(UniAnagrafica_.uniAnaToInds, JoinType.LEFT).get(IndUniAna_.id)));
            }
            if (criteria.getUnianaazianaId() != null) {
                specification = specification.and(buildSpecification(criteria.getUnianaazianaId(),
                    root -> root.join(UniAnagrafica_.unianaaziana, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
            if (criteria.getUniAnaToAtrAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getUniAnaToAtrAnaId(),
                    root -> root.join(UniAnagrafica_.uniAnaToAtrAnas, JoinType.LEFT).get(AtrAnagrafica_.id)));
            }
        }
        return specification;
    }
}
