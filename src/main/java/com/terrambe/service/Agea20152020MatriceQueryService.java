package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.Agea20152020MatriceRepository;
import com.terrambe.service.dto.Agea20152020MatriceCriteria;

/**
 * Service for executing complex queries for {@link Agea20152020Matrice} entities in the database.
 * The main input is a {@link Agea20152020MatriceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Agea20152020Matrice} or a {@link Page} of {@link Agea20152020Matrice} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class Agea20152020MatriceQueryService extends QueryService<Agea20152020Matrice> {

    private final Logger log = LoggerFactory.getLogger(Agea20152020MatriceQueryService.class);

    private final Agea20152020MatriceRepository agea20152020MatriceRepository;

    public Agea20152020MatriceQueryService(Agea20152020MatriceRepository agea20152020MatriceRepository) {
        this.agea20152020MatriceRepository = agea20152020MatriceRepository;
    }

    /**
     * Return a {@link List} of {@link Agea20152020Matrice} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Agea20152020Matrice> findByCriteria(Agea20152020MatriceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Agea20152020Matrice> specification = createSpecification(criteria);
        return agea20152020MatriceRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Agea20152020Matrice} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Agea20152020Matrice> findByCriteria(Agea20152020MatriceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Agea20152020Matrice> specification = createSpecification(criteria);
        return agea20152020MatriceRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(Agea20152020MatriceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Agea20152020Matrice> specification = createSpecification(criteria);
        return agea20152020MatriceRepository.count(specification);
    }

    /**
     * Function to convert {@link Agea20152020MatriceCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Agea20152020Matrice> createSpecification(Agea20152020MatriceCriteria criteria) {
        Specification<Agea20152020Matrice> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Agea20152020Matrice_.id));
            }
            if (criteria.getDescColtura() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescColtura(), Agea20152020Matrice_.descColtura));
            }
            if (criteria.getCodMacroUso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodMacroUso(), Agea20152020Matrice_.codMacroUso));
            }
            if (criteria.getMacroUso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMacroUso(), Agea20152020Matrice_.macroUso));
            }
            if (criteria.getCodOccupazioneSuolo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodOccupazioneSuolo(), Agea20152020Matrice_.codOccupazioneSuolo));
            }
            if (criteria.getOccupazioneSuolo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOccupazioneSuolo(), Agea20152020Matrice_.occupazioneSuolo));
            }
            if (criteria.getCodDestinazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodDestinazione(), Agea20152020Matrice_.codDestinazione));
            }
            if (criteria.getDestinazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDestinazione(), Agea20152020Matrice_.destinazione));
            }
            if (criteria.getCodUso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodUso(), Agea20152020Matrice_.codUso));
            }
            if (criteria.getUso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUso(), Agea20152020Matrice_.uso));
            }
            if (criteria.getCodQualita() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodQualita(), Agea20152020Matrice_.codQualita));
            }
            if (criteria.getQualita() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQualita(), Agea20152020Matrice_.qualita));
            }
            if (criteria.getCodVarieta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodVarieta(), Agea20152020Matrice_.codVarieta));
            }
            if (criteria.getVarieta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVarieta(), Agea20152020Matrice_.varieta));
            }
            if (criteria.getCodFamiglia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodFamiglia(), Agea20152020Matrice_.codFamiglia));
            }
            if (criteria.getFamiglia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFamiglia(), Agea20152020Matrice_.famiglia));
            }
            if (criteria.getCodGenere() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodGenere(), Agea20152020Matrice_.codGenere));
            }
            if (criteria.getGenere() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGenere(), Agea20152020Matrice_.genere));
            }
            if (criteria.getCodSpecie() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodSpecie(), Agea20152020Matrice_.codSpecie));
            }
            if (criteria.getSpecie() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpecie(), Agea20152020Matrice_.specie));
            }
            if (criteria.getVersioneInserimento() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVersioneInserimento(), Agea20152020Matrice_.versioneInserimento));
            }
            if (criteria.getCodiceCompostoAgea() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodiceCompostoAgea(), Agea20152020Matrice_.codiceCompostoAgea));
            }
            if (criteria.getAgeaToDettId() != null) {
                specification = specification.and(buildSpecification(criteria.getAgeaToDettId(),
                    root -> root.join(Agea20152020Matrice_.ageaToDetts, JoinType.LEFT).get(AziSupPartDettaglio_.id)));
            }
            if (criteria.getAgeaToColtId() != null) {
                specification = specification.and(buildSpecification(criteria.getAgeaToColtId(),
                    root -> root.join(Agea20152020Matrice_.ageaToColts, JoinType.LEFT).get(ColturaAppezzamento_.id)));
            }
            if (criteria.getMatriceToBdfId() != null) {
                specification = specification.and(buildSpecification(criteria.getMatriceToBdfId(),
                    root -> root.join(Agea20152020Matrice_.matriceToBdfs, JoinType.LEFT).get(Agea20152020BDF_.id)));
            }
        }
        return specification;
    }
}
