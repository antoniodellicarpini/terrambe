package com.terrambe.service;

import com.terrambe.domain.FaseFenologica;
import com.terrambe.repository.FaseFenologicaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FaseFenologica}.
 */
@Service
@Transactional
public class FaseFenologicaService {

    private final Logger log = LoggerFactory.getLogger(FaseFenologicaService.class);

    private final FaseFenologicaRepository faseFenologicaRepository;

    public FaseFenologicaService(FaseFenologicaRepository faseFenologicaRepository) {
        this.faseFenologicaRepository = faseFenologicaRepository;
    }

    /**
     * Save a faseFenologica.
     *
     * @param faseFenologica the entity to save.
     * @return the persisted entity.
     */
    public FaseFenologica save(FaseFenologica faseFenologica) {
        log.debug("Request to save FaseFenologica : {}", faseFenologica);
        return faseFenologicaRepository.save(faseFenologica);
    }

    /**
     * Get all the faseFenologicas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FaseFenologica> findAll() {
        log.debug("Request to get all FaseFenologicas");
        return faseFenologicaRepository.findAll();
    }


    /**
     * Get one faseFenologica by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FaseFenologica> findOne(Long id) {
        log.debug("Request to get FaseFenologica : {}", id);
        return faseFenologicaRepository.findById(id);
    }

    /**
     * Delete the faseFenologica by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FaseFenologica : {}", id);
        faseFenologicaRepository.deleteById(id);
    }
}
