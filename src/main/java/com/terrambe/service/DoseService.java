package com.terrambe.service;

import com.terrambe.domain.Dose;
import com.terrambe.repository.DoseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Dose}.
 */
@Service
@Transactional
public class DoseService {

    private final Logger log = LoggerFactory.getLogger(DoseService.class);

    private final DoseRepository doseRepository;

    public DoseService(DoseRepository doseRepository) {
        this.doseRepository = doseRepository;
    }

    /**
     * Save a dose.
     *
     * @param dose the entity to save.
     * @return the persisted entity.
     */
    public Dose save(Dose dose) {
        log.debug("Request to save Dose : {}", dose);
        return doseRepository.save(dose);
    }

    /**
     * Get all the doses.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Dose> findAll() {
        log.debug("Request to get all Doses");
        return doseRepository.findAll();
    }


    /**
     * Get one dose by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Dose> findOne(Long id) {
        log.debug("Request to get Dose : {}", id);
        return doseRepository.findById(id);
    }

    /**
     * Delete the dose by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Dose : {}", id);
        doseRepository.deleteById(id);
    }
}
