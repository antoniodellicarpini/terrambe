package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.TipoPrepterr;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.TipoPrepterrRepository;
import com.terrambe.service.dto.TipoPrepterrCriteria;

/**
 * Service for executing complex queries for {@link TipoPrepterr} entities in the database.
 * The main input is a {@link TipoPrepterrCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TipoPrepterr} or a {@link Page} of {@link TipoPrepterr} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoPrepterrQueryService extends QueryService<TipoPrepterr> {

    private final Logger log = LoggerFactory.getLogger(TipoPrepterrQueryService.class);

    private final TipoPrepterrRepository tipoPrepterrRepository;

    public TipoPrepterrQueryService(TipoPrepterrRepository tipoPrepterrRepository) {
        this.tipoPrepterrRepository = tipoPrepterrRepository;
    }

    /**
     * Return a {@link List} of {@link TipoPrepterr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TipoPrepterr> findByCriteria(TipoPrepterrCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TipoPrepterr> specification = createSpecification(criteria);
        return tipoPrepterrRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TipoPrepterr} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TipoPrepterr> findByCriteria(TipoPrepterrCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TipoPrepterr> specification = createSpecification(criteria);
        return tipoPrepterrRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoPrepterrCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TipoPrepterr> specification = createSpecification(criteria);
        return tipoPrepterrRepository.count(specification);
    }

    /**
     * Function to convert {@link TipoPrepterrCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TipoPrepterr> createSpecification(TipoPrepterrCriteria criteria) {
        Specification<TipoPrepterr> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TipoPrepterr_.id));
            }
            if (criteria.getDescrizione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescrizione(), TipoPrepterr_.descrizione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), TipoPrepterr_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), TipoPrepterr_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), TipoPrepterr_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), TipoPrepterr_.userIdLastMod));
            }
            if (criteria.getTipoToOpcPrepterrId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoToOpcPrepterrId(),
                    root -> root.join(TipoPrepterr_.tipoToOpcPrepterrs, JoinType.LEFT).get(RegOpcPrepterr_.id)));
            }
        }
        return specification;
    }
}
