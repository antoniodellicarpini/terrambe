package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.Brogliaccio;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.BrogliaccioRepository;
import com.terrambe.service.dto.BrogliaccioCriteria;

/**
 * Service for executing complex queries for {@link Brogliaccio} entities in the database.
 * The main input is a {@link BrogliaccioCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Brogliaccio} or a {@link Page} of {@link Brogliaccio} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BrogliaccioQueryService extends QueryService<Brogliaccio> {

    private final Logger log = LoggerFactory.getLogger(BrogliaccioQueryService.class);

    private final BrogliaccioRepository brogliaccioRepository;

    public BrogliaccioQueryService(BrogliaccioRepository brogliaccioRepository) {
        this.brogliaccioRepository = brogliaccioRepository;
    }

    /**
     * Return a {@link List} of {@link Brogliaccio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Brogliaccio> findByCriteria(BrogliaccioCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Brogliaccio> specification = createSpecification(criteria);
        return brogliaccioRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Brogliaccio} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Brogliaccio> findByCriteria(BrogliaccioCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Brogliaccio> specification = createSpecification(criteria);
        return brogliaccioRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BrogliaccioCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Brogliaccio> specification = createSpecification(criteria);
        return brogliaccioRepository.count(specification);
    }

    /**
     * Function to convert {@link BrogliaccioCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Brogliaccio> createSpecification(BrogliaccioCriteria criteria) {
        Specification<Brogliaccio> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Brogliaccio_.id));
            }
            if (criteria.getNomeComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNomeComune(), Brogliaccio_.nomeComune));
            }
            if (criteria.getCodNaz() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodNaz(), Brogliaccio_.codNaz));
            }
            if (criteria.getSez() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSez(), Brogliaccio_.sez));
            }
            if (criteria.getFoglio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFoglio(), Brogliaccio_.foglio));
            }
            if (criteria.getPart() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPart(), Brogliaccio_.part));
            }
            if (criteria.getSub() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSub(), Brogliaccio_.sub));
            }
            if (criteria.getSupCat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSupCat(), Brogliaccio_.supCat));
            }
            if (criteria.getSupGra() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSupGra(), Brogliaccio_.supGra));
            }
            if (criteria.getConduz() != null) {
                specification = specification.and(buildStringSpecification(criteria.getConduz(), Brogliaccio_.conduz));
            }
            if (criteria.getcP() != null) {
                specification = specification.and(buildStringSpecification(criteria.getcP(), Brogliaccio_.cP));
            }
            if (criteria.getDataInizio() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizio(), Brogliaccio_.dataInizio));
            }
            if (criteria.getDataFine() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFine(), Brogliaccio_.dataFine));
            }
            if (criteria.getSupUtil() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSupUtil(), Brogliaccio_.supUtil));
            }
            if (criteria.getSupEleg() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSupEleg(), Brogliaccio_.supEleg));
            }
            if (criteria.getMacrouso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMacrouso(), Brogliaccio_.macrouso));
            }
            if (criteria.getOccupazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOccupazione(), Brogliaccio_.occupazione));
            }
            if (criteria.getDestinazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDestinazione(), Brogliaccio_.destinazione));
            }
            if (criteria.getUso() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUso(), Brogliaccio_.uso));
            }
            if (criteria.getQualita() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQualita(), Brogliaccio_.qualita));
            }
            if (criteria.getVarieta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVarieta(), Brogliaccio_.varieta));
            }
            if (criteria.getDataImportazione() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataImportazione(), Brogliaccio_.dataImportazione));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), Brogliaccio_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), Brogliaccio_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), Brogliaccio_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), Brogliaccio_.userIdLastMod));
            }
            if (criteria.getBroglToAziAnaId() != null) {
                specification = specification.and(buildSpecification(criteria.getBroglToAziAnaId(),
                    root -> root.join(Brogliaccio_.broglToAziAna, JoinType.LEFT).get(AziAnagrafica_.id)));
            }
        }
        return specification;
    }
}
