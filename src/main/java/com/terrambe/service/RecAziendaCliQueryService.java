package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.RecAziendaCli;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.RecAziendaCliRepository;
import com.terrambe.service.dto.RecAziendaCliCriteria;

/**
 * Service for executing complex queries for {@link RecAziendaCli} entities in the database.
 * The main input is a {@link RecAziendaCliCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecAziendaCli} or a {@link Page} of {@link RecAziendaCli} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecAziendaCliQueryService extends QueryService<RecAziendaCli> {

    private final Logger log = LoggerFactory.getLogger(RecAziendaCliQueryService.class);

    private final RecAziendaCliRepository recAziendaCliRepository;

    public RecAziendaCliQueryService(RecAziendaCliRepository recAziendaCliRepository) {
        this.recAziendaCliRepository = recAziendaCliRepository;
    }

    /**
     * Return a {@link List} of {@link RecAziendaCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecAziendaCli> findByCriteria(RecAziendaCliCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RecAziendaCli> specification = createSpecification(criteria);
        return recAziendaCliRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RecAziendaCli} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecAziendaCli> findByCriteria(RecAziendaCliCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RecAziendaCli> specification = createSpecification(criteria);
        return recAziendaCliRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecAziendaCliCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RecAziendaCli> specification = createSpecification(criteria);
        return recAziendaCliRepository.count(specification);
    }

    /**
     * Function to convert {@link RecAziendaCliCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RecAziendaCli> createSpecification(RecAziendaCliCriteria criteria) {
        Specification<RecAziendaCli> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RecAziendaCli_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), RecAziendaCli_.email));
            }
            if (criteria.getPec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPec(), RecAziendaCli_.pec));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelefono(), RecAziendaCli_.telefono));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), RecAziendaCli_.fax));
            }
            if (criteria.getCell() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell(), RecAziendaCli_.cell));
            }
            if (criteria.getCell2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCell2(), RecAziendaCli_.cell2));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), RecAziendaCli_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), RecAziendaCli_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), RecAziendaCli_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), RecAziendaCli_.userIdLastMod));
            }
            if (criteria.getRecToCliId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecToCliId(),
                    root -> root.join(RecAziendaCli_.recToCli, JoinType.LEFT).get(Cliente_.id)));
            }
        }
        return specification;
    }
}
