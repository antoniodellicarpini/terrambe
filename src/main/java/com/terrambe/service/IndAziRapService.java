package com.terrambe.service;

import com.terrambe.domain.IndAziRap;
import com.terrambe.repository.IndAziRapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndAziRap}.
 */
@Service
@Transactional
public class IndAziRapService {

    private final Logger log = LoggerFactory.getLogger(IndAziRapService.class);

    private final IndAziRapRepository indAziRapRepository;

    public IndAziRapService(IndAziRapRepository indAziRapRepository) {
        this.indAziRapRepository = indAziRapRepository;
    }

    /**
     * Save a indAziRap.
     *
     * @param indAziRap the entity to save.
     * @return the persisted entity.
     */
    public IndAziRap save(IndAziRap indAziRap) {
        log.debug("Request to save IndAziRap : {}", indAziRap);
        return indAziRapRepository.save(indAziRap);
    }

    /**
     * Get all the indAziRaps.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndAziRap> findAll() {
        log.debug("Request to get all IndAziRaps");
        return indAziRapRepository.findAll();
    }


    /**
     * Get one indAziRap by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndAziRap> findOne(Long id) {
        log.debug("Request to get IndAziRap : {}", id);
        return indAziRapRepository.findById(id);
    }

    /**
     * Delete the indAziRap by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndAziRap : {}", id);
        indAziRapRepository.deleteById(id);
    }
}
