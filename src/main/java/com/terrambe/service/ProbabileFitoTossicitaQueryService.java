package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.ProbabileFitoTossicita;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.ProbabileFitoTossicitaRepository;
import com.terrambe.service.dto.ProbabileFitoTossicitaCriteria;

/**
 * Service for executing complex queries for {@link ProbabileFitoTossicita} entities in the database.
 * The main input is a {@link ProbabileFitoTossicitaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProbabileFitoTossicita} or a {@link Page} of {@link ProbabileFitoTossicita} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProbabileFitoTossicitaQueryService extends QueryService<ProbabileFitoTossicita> {

    private final Logger log = LoggerFactory.getLogger(ProbabileFitoTossicitaQueryService.class);

    private final ProbabileFitoTossicitaRepository probabileFitoTossicitaRepository;

    public ProbabileFitoTossicitaQueryService(ProbabileFitoTossicitaRepository probabileFitoTossicitaRepository) {
        this.probabileFitoTossicitaRepository = probabileFitoTossicitaRepository;
    }

    /**
     * Return a {@link List} of {@link ProbabileFitoTossicita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProbabileFitoTossicita> findByCriteria(ProbabileFitoTossicitaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProbabileFitoTossicita> specification = createSpecification(criteria);
        return probabileFitoTossicitaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ProbabileFitoTossicita} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProbabileFitoTossicita> findByCriteria(ProbabileFitoTossicitaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProbabileFitoTossicita> specification = createSpecification(criteria);
        return probabileFitoTossicitaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProbabileFitoTossicitaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProbabileFitoTossicita> specification = createSpecification(criteria);
        return probabileFitoTossicitaRepository.count(specification);
    }

    /**
     * Function to convert {@link ProbabileFitoTossicitaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProbabileFitoTossicita> createSpecification(ProbabileFitoTossicitaCriteria criteria) {
        Specification<ProbabileFitoTossicita> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProbabileFitoTossicita_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), ProbabileFitoTossicita_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), ProbabileFitoTossicita_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), ProbabileFitoTossicita_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), ProbabileFitoTossicita_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), ProbabileFitoTossicita_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), ProbabileFitoTossicita_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), ProbabileFitoTossicita_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(ProbabileFitoTossicita_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
        }
        return specification;
    }
}
