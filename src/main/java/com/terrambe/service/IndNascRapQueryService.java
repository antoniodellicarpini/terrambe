package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.IndNascRap;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.IndNascRapRepository;
import com.terrambe.service.dto.IndNascRapCriteria;

/**
 * Service for executing complex queries for {@link IndNascRap} entities in the database.
 * The main input is a {@link IndNascRapCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndNascRap} or a {@link Page} of {@link IndNascRap} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndNascRapQueryService extends QueryService<IndNascRap> {

    private final Logger log = LoggerFactory.getLogger(IndNascRapQueryService.class);

    private final IndNascRapRepository indNascRapRepository;

    public IndNascRapQueryService(IndNascRapRepository indNascRapRepository) {
        this.indNascRapRepository = indNascRapRepository;
    }

    /**
     * Return a {@link List} of {@link IndNascRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndNascRap> findByCriteria(IndNascRapCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndNascRap> specification = createSpecification(criteria);
        return indNascRapRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndNascRap} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndNascRap> findByCriteria(IndNascRapCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndNascRap> specification = createSpecification(criteria);
        return indNascRapRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndNascRapCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndNascRap> specification = createSpecification(criteria);
        return indNascRapRepository.count(specification);
    }

    /**
     * Function to convert {@link IndNascRapCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndNascRap> createSpecification(IndNascRapCriteria criteria) {
        Specification<IndNascRap> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndNascRap_.id));
            }
            if (criteria.getIndirizzo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndirizzo(), IndNascRap_.indirizzo));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndNascRap_.cap));
            }
            if (criteria.getNazione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNazione(), IndNascRap_.nazione));
            }
            if (criteria.getRegione() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegione(), IndNascRap_.regione));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndNascRap_.provincia));
            }
            if (criteria.getComune() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComune(), IndNascRap_.comune));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), IndNascRap_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), IndNascRap_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), IndNascRap_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), IndNascRap_.userIdLastMod));
            }
            if (criteria.getIndNascToAziRapId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndNascToAziRapId(),
                    root -> root.join(IndNascRap_.indNascToAziRap, JoinType.LEFT).get(AziRappresentante_.id)));
            }
        }
        return specification;
    }
}
