package com.terrambe.service;

import com.terrambe.domain.Agea20152020Matrice;
import com.terrambe.repository.Agea20152020MatriceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Agea20152020Matrice}.
 */
@Service
@Transactional
public class Agea20152020MatriceService {

    private final Logger log = LoggerFactory.getLogger(Agea20152020MatriceService.class);

    private final Agea20152020MatriceRepository agea20152020MatriceRepository;

    public Agea20152020MatriceService(Agea20152020MatriceRepository agea20152020MatriceRepository) {
        this.agea20152020MatriceRepository = agea20152020MatriceRepository;
    }

    /**
     * Save a agea20152020Matrice.
     *
     * @param agea20152020Matrice the entity to save.
     * @return the persisted entity.
     */
    public Agea20152020Matrice save(Agea20152020Matrice agea20152020Matrice) {
        log.debug("Request to save Agea20152020Matrice : {}", agea20152020Matrice);
        return agea20152020MatriceRepository.save(agea20152020Matrice);
    }

    /**
     * Get all the agea20152020Matrices.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Agea20152020Matrice> findAll() {
        log.debug("Request to get all Agea20152020Matrices");
        return agea20152020MatriceRepository.findAll();
    }


    /**
     * Get one agea20152020Matrice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Agea20152020Matrice> findOne(Long id) {
        log.debug("Request to get Agea20152020Matrice : {}", id);
        return agea20152020MatriceRepository.findById(id);
    }

    /**
     * Delete the agea20152020Matrice by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Agea20152020Matrice : {}", id);
        agea20152020MatriceRepository.deleteById(id);
    }
}
