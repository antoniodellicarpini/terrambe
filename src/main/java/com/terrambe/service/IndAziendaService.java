package com.terrambe.service;

import com.terrambe.domain.IndAzienda;
import com.terrambe.repository.IndAziendaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IndAzienda}.
 */
@Service
@Transactional
public class IndAziendaService {

    private final Logger log = LoggerFactory.getLogger(IndAziendaService.class);

    private final IndAziendaRepository indAziendaRepository;

    public IndAziendaService(IndAziendaRepository indAziendaRepository) {
        this.indAziendaRepository = indAziendaRepository;
    }

    /**
     * Save a indAzienda.
     *
     * @param indAzienda the entity to save.
     * @return the persisted entity.
     */
    public IndAzienda save(IndAzienda indAzienda) {
        log.debug("Request to save IndAzienda : {}", indAzienda);
        return indAziendaRepository.save(indAzienda);
    }

    /**
     * Get all the indAziendas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndAzienda> findAll() {
        log.debug("Request to get all IndAziendas");
        return indAziendaRepository.findAll();
    }


    /**
     * Get one indAzienda by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndAzienda> findOne(Long id) {
        log.debug("Request to get IndAzienda : {}", id);
        return indAziendaRepository.findById(id);
    }

    /**
     * Delete the indAzienda by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndAzienda : {}", id);
        indAziendaRepository.deleteById(id);
    }
}
