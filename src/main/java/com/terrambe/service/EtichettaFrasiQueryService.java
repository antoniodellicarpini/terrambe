package com.terrambe.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.terrambe.domain.EtichettaFrasi;
import com.terrambe.domain.*; // for static metamodels
import com.terrambe.repository.EtichettaFrasiRepository;
import com.terrambe.service.dto.EtichettaFrasiCriteria;

/**
 * Service for executing complex queries for {@link EtichettaFrasi} entities in the database.
 * The main input is a {@link EtichettaFrasiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtichettaFrasi} or a {@link Page} of {@link EtichettaFrasi} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtichettaFrasiQueryService extends QueryService<EtichettaFrasi> {

    private final Logger log = LoggerFactory.getLogger(EtichettaFrasiQueryService.class);

    private final EtichettaFrasiRepository etichettaFrasiRepository;

    public EtichettaFrasiQueryService(EtichettaFrasiRepository etichettaFrasiRepository) {
        this.etichettaFrasiRepository = etichettaFrasiRepository;
    }

    /**
     * Return a {@link List} of {@link EtichettaFrasi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtichettaFrasi> findByCriteria(EtichettaFrasiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EtichettaFrasi> specification = createSpecification(criteria);
        return etichettaFrasiRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EtichettaFrasi} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtichettaFrasi> findByCriteria(EtichettaFrasiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EtichettaFrasi> specification = createSpecification(criteria);
        return etichettaFrasiRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtichettaFrasiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EtichettaFrasi> specification = createSpecification(criteria);
        return etichettaFrasiRepository.count(specification);
    }

    /**
     * Function to convert {@link EtichettaFrasiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EtichettaFrasi> createSpecification(EtichettaFrasiCriteria criteria) {
        Specification<EtichettaFrasi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EtichettaFrasi_.id));
            }
            if (criteria.getTipoImport() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoImport(), EtichettaFrasi_.tipoImport));
            }
            if (criteria.getOperatore() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperatore(), EtichettaFrasi_.operatore));
            }
            if (criteria.getTs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTs(), EtichettaFrasi_.ts));
            }
            if (criteria.getDataInizVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataInizVali(), EtichettaFrasi_.dataInizVali));
            }
            if (criteria.getDataFineVali() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDataFineVali(), EtichettaFrasi_.dataFineVali));
            }
            if (criteria.getUserIdCreator() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdCreator(), EtichettaFrasi_.userIdCreator));
            }
            if (criteria.getUserIdLastMod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserIdLastMod(), EtichettaFrasi_.userIdLastMod));
            }
            if (criteria.getEtichettaFitoId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtichettaFitoId(),
                    root -> root.join(EtichettaFrasi_.etichettaFito, JoinType.LEFT).get(EtichettaFito_.id)));
            }
            if (criteria.getFrasiId() != null) {
                specification = specification.and(buildSpecification(criteria.getFrasiId(),
                    root -> root.join(EtichettaFrasi_.frasi, JoinType.LEFT).get(Frasi_.id)));
            }
        }
        return specification;
    }
}
