package com.terrambe.service;

import com.terrambe.domain.ColturaAppezzamento;
import com.terrambe.repository.ColturaAppezzamentoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ColturaAppezzamento}.
 */
@Service
@Transactional
public class ColturaAppezzamentoService {

    private final Logger log = LoggerFactory.getLogger(ColturaAppezzamentoService.class);

    private final ColturaAppezzamentoRepository colturaAppezzamentoRepository;

    public ColturaAppezzamentoService(ColturaAppezzamentoRepository colturaAppezzamentoRepository) {
        this.colturaAppezzamentoRepository = colturaAppezzamentoRepository;
    }

    /**
     * Save a colturaAppezzamento.
     *
     * @param colturaAppezzamento the entity to save.
     * @return the persisted entity.
     */
    public ColturaAppezzamento save(ColturaAppezzamento colturaAppezzamento) {
        log.debug("Request to save ColturaAppezzamento : {}", colturaAppezzamento);
        return colturaAppezzamentoRepository.save(colturaAppezzamento);
    }

    /**
     * Get all the colturaAppezzamentos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ColturaAppezzamento> findAll() {
        log.debug("Request to get all ColturaAppezzamentos");
        return colturaAppezzamentoRepository.findAll();
    }


    /**
     * Get one colturaAppezzamento by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ColturaAppezzamento> findOne(Long id) {
        log.debug("Request to get ColturaAppezzamento : {}", id);
        return colturaAppezzamentoRepository.findById(id);
    }

    /**
     * Delete the colturaAppezzamento by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ColturaAppezzamento : {}", id);
        colturaAppezzamentoRepository.deleteById(id);
    }
}
