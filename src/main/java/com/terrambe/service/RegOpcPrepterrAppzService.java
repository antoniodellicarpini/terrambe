package com.terrambe.service;

import com.terrambe.domain.RegOpcPrepterrAppz;
import com.terrambe.repository.RegOpcPrepterrAppzRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RegOpcPrepterrAppz}.
 */
@Service
@Transactional
public class RegOpcPrepterrAppzService {

    private final Logger log = LoggerFactory.getLogger(RegOpcPrepterrAppzService.class);

    private final RegOpcPrepterrAppzRepository regOpcPrepterrAppzRepository;

    public RegOpcPrepterrAppzService(RegOpcPrepterrAppzRepository regOpcPrepterrAppzRepository) {
        this.regOpcPrepterrAppzRepository = regOpcPrepterrAppzRepository;
    }

    /**
     * Save a regOpcPrepterrAppz.
     *
     * @param regOpcPrepterrAppz the entity to save.
     * @return the persisted entity.
     */
    public RegOpcPrepterrAppz save(RegOpcPrepterrAppz regOpcPrepterrAppz) {
        log.debug("Request to save RegOpcPrepterrAppz : {}", regOpcPrepterrAppz);
        return regOpcPrepterrAppzRepository.save(regOpcPrepterrAppz);
    }

    /**
     * Get all the regOpcPrepterrAppzs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RegOpcPrepterrAppz> findAll() {
        log.debug("Request to get all RegOpcPrepterrAppzs");
        return regOpcPrepterrAppzRepository.findAll();
    }


    /**
     * Get one regOpcPrepterrAppz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RegOpcPrepterrAppz> findOne(Long id) {
        log.debug("Request to get RegOpcPrepterrAppz : {}", id);
        return regOpcPrepterrAppzRepository.findById(id);
    }

    /**
     * Delete the regOpcPrepterrAppz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RegOpcPrepterrAppz : {}", id);
        regOpcPrepterrAppzRepository.deleteById(id);
    }
}
